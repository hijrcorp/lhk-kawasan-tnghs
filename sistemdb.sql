# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: sistemdb
# Generation Time: 2019-02-12 00:04:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table hijr_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_account`;

CREATE TABLE `hijr_account` (
  `id_account` varchar(50) NOT NULL DEFAULT '',
  `username_account` varchar(30) DEFAULT NULL,
  `password_account` varchar(500) DEFAULT NULL,
  `email_account` varchar(100) DEFAULT NULL,
  `email_status_account` tinyint(1) DEFAULT NULL,
  `first_name_account` varchar(100) NOT NULL DEFAULT '',
  `last_name_account` varchar(100) NOT NULL DEFAULT '',
  `mobile_account` varchar(30) DEFAULT NULL,
  `mobile_status_account` tinyint(1) DEFAULT NULL,
  `male_status_account` tinyint(1) DEFAULT NULL,
  `birth_date_account` date DEFAULT NULL,
  `enabled_account` tinyint(1) NOT NULL,
  `account_non_expired_account` tinyint(1) NOT NULL,
  `credentials_non_expired_account` tinyint(1) NOT NULL,
  `account_non_locked_account` tinyint(1) NOT NULL,
  `person_added_account` varchar(50) DEFAULT NULL,
  `time_added_account` datetime DEFAULT NULL,
  `person_modified_account` varchar(50) DEFAULT NULL,
  `time_modified_account` datetime DEFAULT NULL,
  `login_account` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_account`),
  UNIQUE KEY `username` (`username_account`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_account` WRITE;
/*!40000 ALTER TABLE `hijr_account` DISABLE KEYS */;

INSERT INTO `hijr_account` (`id_account`, `username_account`, `password_account`, `email_account`, `email_status_account`, `first_name_account`, `last_name_account`, `mobile_account`, `mobile_status_account`, `male_status_account`, `birth_date_account`, `enabled_account`, `account_non_expired_account`, `credentials_non_expired_account`, `account_non_locked_account`, `person_added_account`, `time_added_account`, `person_modified_account`, `time_modified_account`, `login_account`)
VALUES
	('1520819042544','admin','$2a$10$/fbRpiHqNXGbxucT6FL9huz6OYvQJAReIKt0sf5MK57.09SNCvRs2','gustaroska@gmail.com',NULL,'Super','Admin','6281281823697',NULL,NULL,NULL,1,1,1,1,NULL,NULL,NULL,NULL,3),
	('1520819042545','gustaroska','$2a$10$XjRiigh1vuwSDhfXuUnlFO9GRlOnyNz.m0nAGkGkBOji1uJh.Vxji','gustaroska@hijr.co.id',NULL,'Refit','Gustaroska','081281823697',NULL,NULL,NULL,1,1,1,1,NULL,NULL,NULL,NULL,0),
	('1520819042546','hamdan','$2a$10$XjRiigh1vuwSDhfXuUnlFO9GRlOnyNz.m0nAGkGkBOji1uJh.Vxji','hasyuba.sys@gmail.com',NULL,'Hamdan','Batubara','08111111202',NULL,NULL,NULL,1,1,1,1,NULL,NULL,'1520819042544','2018-11-17 10:20:29',0),
	('1542070924987','aparaturlhk','$2a$10$MxBZFSTwu1vfmCW.45iKUeGCzHo2FyYEtn5iRk/ijLGKHHG..Q8Vy','',NULL,'Admin','Aparatur','',NULL,NULL,NULL,1,1,1,1,NULL,NULL,'1520819042544','2018-11-16 08:25:41',0),
	('1542881261761','oprsoal',NULL,NULL,NULL,'Operator','Bank Soal',NULL,NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-22 17:07:41',NULL,NULL,0),
	('1543475745102','pelayanan','$2a$10$0EsNDiAZV1Gbjrql.w4kpuNExxKTRORDtF2mYmi8weGutEubCbev6','',NULL,'Seksi','Pelayanan','',NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-29 15:15:44','1543544826379','2018-12-09 15:10:44',0),
	('1543478734417','permohonan','$2a$10$xd/6NHpqB.VCddYqBlqdJ.CYgIHAQKpB/TWcF31xWyfBCbpfc8Kui','',NULL,'Bagian','Permohonan','',NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-29 16:05:34','1543544826379','2018-11-30 17:11:26',0),
	('1543544826379','adminpps','$2a$10$wlk1kO5gtUP7dkbverGsrOsFIoNZQGe51dKWJi0znwvWS8VBsd3qm','admin@hijr.co.id',NULL,'Admin','PPS','081281823697',NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-30 10:27:05','1543544826379','2018-12-06 08:12:34',0),
	('1544058068121','udin',NULL,'',NULL,'Udin','Jaenudin','',NULL,NULL,NULL,0,1,1,1,'1543544826379','2018-12-06 09:01:07',NULL,NULL,0),
	('1544058089624','samsul',NULL,'',NULL,'Samsul','Bahri','',NULL,NULL,NULL,0,1,1,1,'1543544826379','2018-12-06 09:01:29',NULL,NULL,0),
	('1544058156175','agus',NULL,'',NULL,'drh. Agus','Susanto','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-06 09:02:35',NULL,NULL,0),
	('1544058184434','mukidi',NULL,'',NULL,'drh. Mukidi','Jaelani','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-06 09:03:04',NULL,NULL,0),
	('1544578764544','opr','$2a$10$Q92W65aezPINQ8GYeBSsIemLwEXLr4u0tmTET.3WYIleJzTQ8E0xG','',NULL,'Operator','Magang','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-12 09:39:24','1543544826379','2018-12-12 09:39:43',0),
	('1544664228345','tu','$2a$10$ljc8mheFhv/2EI8n5UcZ5.x59QbKmJmElkIop3lFBD37WQeNCEORC','',NULL,'Tata','Usaha','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-13 08:23:47',NULL,NULL,0),
	('1544664544556','kepala','$2a$10$6hqEcJvR65gSxTfXr46..u/8GF3/SpowX/MEzrrGKrt44GJwr.cpy','',NULL,'Kepala','Balai','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-13 08:29:03',NULL,NULL,0),
	('1544790230958','asd',NULL,'asd',NULL,'asd','qwe','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-14 19:23:51','1543544826379','2018-12-14 19:24:01',0);

/*!40000 ALTER TABLE `hijr_account` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_account_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_account_group`;

CREATE TABLE `hijr_account_group` (
  `id_account_group` varchar(50) NOT NULL DEFAULT '',
  `name_account_group` varchar(200) NOT NULL DEFAULT '',
  `description_account_group` varchar(500) DEFAULT NULL,
  `application_id_account_group` varchar(50) NOT NULL DEFAULT '',
  `sequence_account_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_account_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_account_group` WRITE;
/*!40000 ALTER TABLE `hijr_account_group` DISABLE KEYS */;

INSERT INTO `hijr_account_group` (`id_account_group`, `name_account_group`, `description_account_group`, `application_id_account_group`, `sequence_account_group`)
VALUES
	('1001','Administrator',NULL,'001',1),
	('1002','Supervisor',NULL,'001',2),
	('1003','Analis',NULL,'001',3),
	('1004','Operator',NULL,'001',4),
	('1005','Tata Usaha',NULL,'001',5),
	('1543474689117','Kepala Balai','Approval','007',1),
	('1543474707783','Tata Usaha','Verifikasi dan Validasi','007',2),
	('1543474714617','Pelayanan','Penerimaan dan Pengeluaran','007',4),
	('1543474751393','Operator','','007',5),
	('1543475100552','Permohonan','','007',3),
	('1543544803085','Administrator','Full Akses','007',6),
	('1544057998367','Keeper','Operator Log Keeper','007',7),
	('1544058020139','Dokter','Operator Log Dokter','007',8),
	('2001','Administrator',NULL,'002',1),
	('2002','Supervisor',NULL,'002',2),
	('2003','Analis',NULL,'002',3),
	('2004','Operator Satker',NULL,'002',4),
	('3001','Administrator',NULL,'003',1),
	('3002','Inspektur / Kepala Bagian',NULL,'003',2),
	('3003','Supervisor Bagian / RAPIM',NULL,'003',3),
	('3004','Operator / TU Wilayah / Investigasi',NULL,'003',4),
	('4001','Administrator',NULL,'004',1),
	('4002','Seksi P2',NULL,'004',2),
	('4003','Keuangan',NULL,'004',3),
	('4004','Tata Usaha',NULL,'004',4),
	('4005','Bidang Wilayah',NULL,'004',5),
	('4006','Pemegang Izin',NULL,'004',6),
	('5001','Administrator',NULL,'005',1),
	('5002','Pemegang Izin',NULL,'005',2),
	('6001','Administrator',NULL,'006',1),
	('6002','Operator',NULL,'006',2);

/*!40000 ALTER TABLE `hijr_account_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_account_group_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_account_group_role`;

CREATE TABLE `hijr_account_group_role` (
  `group_id_account_group_role` varchar(50) NOT NULL DEFAULT '',
  `role_code_account_group_role` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`group_id_account_group_role`,`role_code_account_group_role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_account_group_role` WRITE;
/*!40000 ALTER TABLE `hijr_account_group_role` DISABLE KEYS */;

INSERT INTO `hijr_account_group_role` (`group_id_account_group_role`, `role_code_account_group_role`)
VALUES
	('1543474689117','KEPALA'),
	('1543474707783','TU'),
	('1543474714617','PELAYANAN'),
	('1543474751393','OPERATOR'),
	('1543475100552','PERMOHONAN'),
	('1543544803085','ADMIN'),
	('1543544803085','KEPALA'),
	('1543544803085','OPERATOR'),
	('1543544803085','PELAYANAN'),
	('1543544803085','PERMOHONAN'),
	('1543544803085','TU'),
	('6001','ADMIN'),
	('6001','OPERATOR'),
	('6002','OPERATOR');

/*!40000 ALTER TABLE `hijr_account_group_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_account_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_account_role`;

CREATE TABLE `hijr_account_role` (
  `id_account_role` varchar(50) DEFAULT NULL,
  `code_account_role` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_account_role` WRITE;
/*!40000 ALTER TABLE `hijr_account_role` DISABLE KEYS */;

INSERT INTO `hijr_account_role` (`id_account_role`, `code_account_role`)
VALUES
	('1520819042544','ROLE_SUPER_ADMIN'),
	('1542070924987','ROLE_LHK_ADMIN');

/*!40000 ALTER TABLE `hijr_account_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_application`;

CREATE TABLE `hijr_application` (
  `id_application` varchar(50) NOT NULL DEFAULT '',
  `code_application` varchar(50) NOT NULL DEFAULT '',
  `name_application` varchar(100) NOT NULL DEFAULT '',
  `prefix_role_application` varchar(20) NOT NULL DEFAULT '',
  `organization_id_application` varchar(50) NOT NULL DEFAULT '',
  `link_application` varchar(500) NOT NULL DEFAULT '',
  `default_role_application` varchar(50) NOT NULL DEFAULT '',
  `person_added_application` varchar(50) DEFAULT NULL,
  `time_added_application` datetime DEFAULT NULL,
  `person_modified_application` varchar(50) DEFAULT NULL,
  `time_modified_application` datetime DEFAULT NULL,
  PRIMARY KEY (`id_application`),
  UNIQUE KEY `code_application` (`code_application`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_application` WRITE;
/*!40000 ALTER TABLE `hijr_application` DISABLE KEYS */;

INSERT INTO `hijr_application` (`id_application`, `code_application`, `name_application`, `prefix_role_application`, `organization_id_application`, `link_application`, `default_role_application`, `person_added_application`, `time_added_application`, `person_modified_application`, `time_modified_application`)
VALUES
	('001','ALHP','Analisis Laporan Hasil Pemeriksaan','ROLE_ALHP_','3110201400001','https://lhk.hijr.co.id/lha/','ROLE_ALHP_USER',NULL,NULL,NULL,NULL),
	('002','PTL','Pemantauan Tindak Lanjut','ROLE_PTL_','3110201400001','https://lhk.hijr.co.id/ptl/','ROLE_PTL_USER',NULL,NULL,NULL,NULL),
	('003','PKPT','Program Kerja Pengawasan Tahunan','ROLE_PKPT_','3110201400001','https://lhk.hijr.co.id/pkpt/','ROLE_PKPT_USER',NULL,NULL,NULL,NULL),
	('004','SATSDN','Surat Angkut TSL Dalam Negeri','ROLE_SATSDN_','3110201400001','https://www.bbksdajabar.com/satsdn/','ROLE_SATSDN_USER',NULL,NULL,NULL,NULL),
	('005','PKR','Database Penangkaran TSL','ROLE_PKR_','3110201400001','https://www.bbksdajabar.com/penangkaran/','ROLE_PKR_USER',NULL,NULL,NULL,NULL),
	('006','APR','Aparatur Survey Jabatan','ROLE_APR_','3110201400001','','ROLE_APR_USER',NULL,NULL,NULL,NULL),
	('007','PPS','Pusat Penyelamatan Satwa','ROLE_PPS_','3110201400001','','ROLE_PPS_USER',NULL,NULL,NULL,NULL),
	('1542357831118','TEST','test','ROLE_TEST_','3110201400000','','ROLE_TEST_USER','1520819042544','2018-11-16 15:43:50',NULL,NULL),
	('1542357885057','CUTI','Aplikasi Cuti Online','ROLE_CUTI_','3110201400000','','ROLE_CUTI_USER','1520819042544','2018-11-16 15:44:44',NULL,NULL),
	('1542358039259','MAAL','Aplikasi Manajemen Keuangan','ROLE_MAAL_','3110201400000','','ROLE_MAAL_USER','1520819042544','2018-11-16 15:47:18',NULL,NULL),
	('1542445618488','QWE','qwe','ROLE_QWE_','3110201400000','','ROLE_QWE_USER','1520819042544','2018-11-17 16:06:58',NULL,NULL),
	('1542445624628','ASD','asd','ROLE_ASD_','3110201400000','','ROLE_ASD_USER','1520819042544','2018-11-17 16:07:03',NULL,NULL);

/*!40000 ALTER TABLE `hijr_application` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_application_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_application_client`;

CREATE TABLE `hijr_application_client` (
  `id_application_client` varchar(50) NOT NULL DEFAULT '',
  `header_id_application_client` varchar(50) NOT NULL DEFAULT '',
  `code_application_client` varchar(20) NOT NULL DEFAULT '',
  `type_application_client` varchar(20) NOT NULL DEFAULT '',
  `resource_application_client` varchar(50) NOT NULL DEFAULT '',
  `organization_id_application_client` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_application_client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table hijr_assignment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_assignment`;

CREATE TABLE `hijr_assignment` (
  `id_assignment` varchar(50) NOT NULL DEFAULT '',
  `account_id_assignment` varchar(50) NOT NULL DEFAULT '',
  `position_id_assignment` varchar(50) NOT NULL DEFAULT '',
  `organization_id_assignment` varchar(50) NOT NULL DEFAULT '',
  `status_assignment` varchar(20) NOT NULL DEFAULT '',
  `start_date_assignment` date DEFAULT NULL,
  `end_date_assignment` date DEFAULT NULL,
  `parent_id_assignment` varchar(50) NOT NULL DEFAULT '',
  `active_assignment` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_assignment`),
  UNIQUE KEY `UNIQUE_KEY` (`account_id_assignment`,`status_assignment`,`position_id_assignment`,`organization_id_assignment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_assignment` WRITE;
/*!40000 ALTER TABLE `hijr_assignment` DISABLE KEYS */;

INSERT INTO `hijr_assignment` (`id_assignment`, `account_id_assignment`, `position_id_assignment`, `organization_id_assignment`, `status_assignment`, `start_date_assignment`, `end_date_assignment`, `parent_id_assignment`, `active_assignment`)
VALUES
	('15208190425440031','1520819042544','003','3110201400000','PERMANENT',NULL,NULL,'3110201400000',1),
	('15208190425441011','1520819042544','101','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('15208190425450011','1520819042545','001','3110201400000','PERMANENT',NULL,NULL,'3110201400000',1),
	('15208190425460021','1520819042546','002','1542358938675','PERMANENT',NULL,NULL,'3110201400000',1),
	('15420709249871031','1542070924987','103','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1542881262177','1542881261761','102','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1543475745150','1543475745102','104','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1543478734241','1543478734417','104','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1543544826575','1543544826379','103','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544058067704','1544058068121','101','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544058089540','1544058089624','102','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544058156723','1544058156175','103','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544058184988','1544058184434','103','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544578764431','1544578764544','101','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544664228372','1544664228345','104','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544664544272','1544664544556','104','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1),
	('1544790231588','1544790230958','102','3110201400001','PERMANENT',NULL,NULL,'3110201400001',1);

/*!40000 ALTER TABLE `hijr_assignment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_authorities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_authorities`;

CREATE TABLE `hijr_authorities` (
  `id_authorities` varchar(50) NOT NULL,
  `account_id_authorities` varchar(50) NOT NULL DEFAULT '',
  `group_id_authorities` varchar(50) NOT NULL DEFAULT '',
  `application_id_authorities` varchar(50) NOT NULL DEFAULT '',
  `person_added_authorities` varchar(50) NOT NULL DEFAULT '',
  `time_added_authorities` datetime NOT NULL,
  PRIMARY KEY (`account_id_authorities`,`group_id_authorities`,`application_id_authorities`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_authorities` WRITE;
/*!40000 ALTER TABLE `hijr_authorities` DISABLE KEYS */;

INSERT INTO `hijr_authorities` (`id_authorities`, `account_id_authorities`, `group_id_authorities`, `application_id_authorities`, `person_added_authorities`, `time_added_authorities`)
VALUES
	('15208190425441001','1520819042544','1001','001','1520819042544','2018-11-17 16:21:57'),
	('15208190425442001','1520819042544','2001','002','1520819042544','2018-11-17 16:21:57'),
	('15208190425443001','1520819042544','3001','003','1520819042544','2018-11-17 16:21:57'),
	('15208190425444001','1520819042544','4001','004','1520819042544','2018-11-17 16:21:57'),
	('15208190425445001','1520819042544','5001','005','1520819042544','2018-11-17 16:21:57'),
	('15208190425446001','1520819042544','6001','006','1520819042544','2018-11-17 16:21:57'),
	('15420709249876001','1542070924987','6001','006','1520819042544','2018-11-17 16:21:57'),
	('1543475744602','1543475745102','1543474714617','007','1543544826379','2018-12-09 15:10:44'),
	('1543478734883','1543478734417','1543475100552','007','1543544826379','2018-11-30 17:11:26'),
	('1543544826615','1543544826379','1543544803085','007','1543544826379','2018-12-06 08:12:34'),
	('1544058068098','1544058068121','1544057998367','007','1543544826379','2018-12-06 09:01:07'),
	('1544058089506','1544058089624','1544057998367','007','1543544826379','2018-12-06 09:01:29'),
	('1544058156758','1544058156175','1544058020139','007','1543544826379','2018-12-06 09:02:35'),
	('1544058184886','1544058184434','1544058020139','007','1543544826379','2018-12-06 09:03:04'),
	('1544578764139','1544578764544','1543474751393','007','1543544826379','2018-12-12 09:39:43'),
	('1544664228560','1544664228345','1543474707783','007','1543544826379','2018-12-13 08:23:47'),
	('1544664544239','1544664544556','1543474689117','007','1543544826379','2018-12-13 08:29:03'),
	('1542509690497','196810161994031002','6002','006','1520819042544','2018-11-18 09:54:50');

/*!40000 ALTER TABLE `hijr_authorities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_country`;

CREATE TABLE `hijr_country` (
  `id_country` varchar(4) NOT NULL DEFAULT '',
  `name_country` varchar(50) NOT NULL DEFAULT '',
  `sequence_country` int(11) NOT NULL,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_country` WRITE;
/*!40000 ALTER TABLE `hijr_country` DISABLE KEYS */;

INSERT INTO `hijr_country` (`id_country`, `name_country`, `sequence_country`)
VALUES
	('AD','Andorra',5),
	('AE','United Arab Emirates',229),
	('AF','Afghanistan',1),
	('AG','Antigua And Barbuda',9),
	('AI','Anguilla',7),
	('AL','Albania',2),
	('AM','Armenia',11),
	('AN','Netherlands Antilles',156),
	('AO','Angola',6),
	('AQ','Antartica',8),
	('AR','Argentina',10),
	('AS','American Samoa',4),
	('AT','Austria',14),
	('AU','Australia',13),
	('AW','Aruba',12),
	('AZ','Azerbaijan',15),
	('BA','Bosnia-Herzegovina',28),
	('BB','Barbados',19),
	('BC','British Indian Ocean Territory',31),
	('BD','Bangladesh',18),
	('BE','Belgium',21),
	('BF','Burkina Faso',34),
	('BG','Bulgaria',33),
	('BH','Bahrain',17),
	('BI','Burundi',35),
	('BJ','Benin',23),
	('BM','Bermuda',24),
	('BN','Brunei Darussalam',32),
	('BO','Bolivia',26),
	('BQ','Bonaire St Eustatius And Saba',27),
	('BR','Brazil',30),
	('BS','Bahamas',16),
	('BT','Bhutan',25),
	('BW','Botswana',29),
	('BY','Belarus',20),
	('BZ','Belize',22),
	('CA','Canada',38),
	('CC','Cocos Islands',47),
	('CD','Congo The Democratic Rep Of',51),
	('CF','Central African Republic',41),
	('CG','Congo Brazzaville',50),
	('CH','Switzerland',211),
	('CI','Cote D Ivoire',54),
	('CK','Cook Islands',52),
	('CL','Chile',44),
	('CM','Cameroon-Republic Of',37),
	('CN','China',45),
	('CO','Colombia',48),
	('CR','Costa Rica',53),
	('CS','Serbiamontenegro',189),
	('CU','Cuba',56),
	('CV','Cape Verde-Republic Of',39),
	('CW','Curacao',57),
	('CX','Christmas Island',46),
	('CY','Cyprus',58),
	('CZ','Czech Republic',59),
	('DE','Germany',82),
	('DJ','Djibouti',61),
	('DK','Denmark',60),
	('DM','Dominica',62),
	('DO','Dominican Republic',63),
	('DZ','Algeria',3),
	('EC','Ecuador',64),
	('EE','Estonia',69),
	('EG','Egypt',65),
	('ER','Eritrea',68),
	('ES','Spain',200),
	('ET','Ethiopia',70),
	('FI','Finland',75),
	('FJ','Fiji',74),
	('FK','Falkland Islands',71),
	('FM','Micronesia',143),
	('FO','Faroe Islands',72),
	('FR','France',76),
	('GA','Gabon',79),
	('GB','United Kingdom',230),
	('GD','Grenada',88),
	('GE','Georgia',81),
	('GF','French Guiana',77),
	('GG','Gg Migration',83),
	('GH','Ghana',84),
	('GI','Gibraltar',85),
	('GL','Greenland',87),
	('GM','Gambia',80),
	('GN','Guinea',92),
	('GP','Guadeloupe',89),
	('GQ','Equatorial Guinea',67),
	('GR','Greece',86),
	('GT','Guatemala',91),
	('GU','Guam',90),
	('GW','Guinea Bissau',93),
	('GY','Guyana',94),
	('HB','Hb Migration',96),
	('HK','Hong Kong -Sar Of China-',98),
	('HN','Honduras',97),
	('HR','Croatia',55),
	('HS','Channel Islands',43),
	('HT','Haiti',95),
	('HU','Hungary',99),
	('ID','Indonesia',102),
	('IE','Ireland-Republic Of',105),
	('IL','Israel',106),
	('IN','India',101),
	('IQ','Iraq',104),
	('IR','Iran',103),
	('IS','Iceland',100),
	('IT','Italy',107),
	('JA','Johnston Atoll',110),
	('JM','Jamaica',108),
	('JO','Jordan',111),
	('JP','Japan',109),
	('KA','Ka Migration',112),
	('KE','Kenya',114),
	('KG','Kyrgyzstan',119),
	('KH','Cambodia',36),
	('KI','Kiribati',115),
	('KM','Comoros',49),
	('KN','St. Kitts',203),
	('KP','Korea Dem Peoples Rep Of',116),
	('KR','Korea Republic Of',117),
	('KW','Kuwait',118),
	('KY','Cayman Islands',40),
	('KZ','Kazakhstan',113),
	('LA','Lao People\'S Dem Republic',120),
	('LB','Lebanon',122),
	('LC','St. Lucia',204),
	('LI','Liechtenstein',126),
	('LK','Sri Lanka',201),
	('LR','Liberia',124),
	('LS','Lesotho',123),
	('LT','Lithuania',127),
	('LU','Luxembourg',128),
	('LV','Latvia',121),
	('LY','Libya',125),
	('MA','Morocco',149),
	('MC','Monaco',145),
	('MD','Moldova',144),
	('ME','Montenegro',147),
	('MG','Madagascar',131),
	('MH','Marshall Islands',137),
	('MK','Macedonia -Fyrom-',130),
	('ML','Mali',135),
	('MM','Myanmar',151),
	('MN','Mongolia',146),
	('MO','Macao -Sar Of China-',129),
	('MP','Northern Mariana Islands',164),
	('MQ','Martinique',138),
	('MR','Mauritania',139),
	('MS','Montserrat',148),
	('MT','Malta',136),
	('MU','Mauritius Island',140),
	('MV','Maldives Island',134),
	('MW','Malawi',132),
	('MX','Mexico',142),
	('MY','Malaysia',133),
	('MZ','Mozambique',150),
	('NA','Namibia',152),
	('NC','New Caledonia',157),
	('NE','Niger',160),
	('NF','Norfolk Island',163),
	('NG','Nigeria',161),
	('NI','Nicaragua',159),
	('NL','Netherlands',155),
	('NO','Norway',165),
	('NP','Nepal',154),
	('NR','Nauru',153),
	('NU','Niue',162),
	('NZ','New Zealand',158),
	('OM','Oman',166),
	('PA','Panama',170),
	('PE','Peru',173),
	('PF','French Polynesia',78),
	('PG','Papua New Guinea',171),
	('PH','Philippines',174),
	('PK','Pakistan',167),
	('PL','Poland',175),
	('PM','St. Pierre And Miquelon',205),
	('PR','Puerto Rico',177),
	('PS','Palestine - State Of',169),
	('PT','Portugal',176),
	('PW','Palau Islands',168),
	('PY','Paraguay',172),
	('QA','Qatar',178),
	('RE','Reunion Island',179),
	('RO','Romania',180),
	('RS','Serbia',188),
	('RU','Russia',181),
	('RW','Rwanda',182),
	('SA','Saudi Arabia',186),
	('SB','Solomon Islands',196),
	('SC','Seychelles Islands',190),
	('SD','Sudan',207),
	('SE','Sweden',210),
	('SG','Singapore',192),
	('SH','St. Helena Island',202),
	('SI','Slovenia',195),
	('SK','Slovakia',194),
	('SL','Sierra Leone',191),
	('SM','San Marino',184),
	('SN','Senegal',187),
	('SO','Somalia',197),
	('SR','Suriname',208),
	('SS','South Sudan',199),
	('ST','Sao Tome And Principe Islands',185),
	('SV','El Salvador',66),
	('SX','Sint Maarten',193),
	('SY','Syrian Arab Republic',212),
	('SZ','Swaziland',209),
	('TC','Turks And Caicos Islands',225),
	('TD','Chad',42),
	('TG','Togo',218),
	('TH','Thailand',216),
	('TJ','Tajikistan',214),
	('TL','Timor Leste',217),
	('TM','Turkmenistan',224),
	('TN','Tunisia',222),
	('TO','Tonga',219),
	('TP','Tp Migration',220),
	('TR','Turkey',223),
	('TT','Trinidad And Tobago',221),
	('TV','Tuvalu',226),
	('TW','Taiwan',213),
	('TZ','Tanzania-United Republic',215),
	('UA','Ukraine',228),
	('UG','Uganda',227),
	('UM','U.S. Minor Outlying Islands',233),
	('US','United States Of America',231),
	('UY','Uruguay',232),
	('UZ','Uzbekistan',234),
	('VC','St. Vincent',206),
	('VE','Venezuela',236),
	('VG','Virgin Islands-British',238),
	('VI','Virgin Islands-United States',239),
	('VN','Vietnam',237),
	('VU','Vanuatu',235),
	('WF','Wallis And Futuna Islands',241),
	('WI','Wake Island',240),
	('WS','Samoa-Independent State Of',183),
	('XK','Xk Migration',242),
	('XX','Xx Migration',243),
	('YE','Yemen Republic',244),
	('YT','Mayotte',141),
	('YU','Yugoslavia',245),
	('ZA','South Africa',198),
	('ZM','Zambia',247),
	('ZR','Zaire',246),
	('ZW','Zimbabwe',248),
	('ZZ','Fictitious Points',73);

/*!40000 ALTER TABLE `hijr_country` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_district
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_district`;

CREATE TABLE `hijr_district` (
  `id_district` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_province_district` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `latitude_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `longitude_district` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id_district`),
  UNIQUE KEY `kabupaten_id` (`id_district`) USING BTREE,
  KEY `kabupaten_nama` (`name_district`) USING BTREE,
  KEY `provinsi_id` (`id_province_district`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `hijr_district` WRITE;
/*!40000 ALTER TABLE `hijr_district` DISABLE KEYS */;

INSERT INTO `hijr_district` (`id_district`, `id_province_district`, `name_district`, `latitude_district`, `longitude_district`)
VALUES
	('1101','11','Kabupaten Simeulue','2.6439724','96.0255738'),
	('1102','11','Kabupaten Aceh Singkil','2.3589459','97.87216'),
	('1103','11','Kabupaten Aceh Selatan','3.3115056','97.3516558'),
	('1104','11','Kabupaten Aceh Tenggara','3.3088666','97.6982272'),
	('1105','11','Kabupaten Aceh Timur','4.5224111','97.6114217'),
	('1106','11','Kabupaten Aceh Tengah','4.4482641','96.8350999'),
	('1107','11','Kabupaten Aceh Barat','4.4542745','96.1526985'),
	('1108','11','Kabupaten Aceh Besar','5.4529168','95.4777811'),
	('1109','11','Kabupaten Pidie','5.3704132','95.9356872'),
	('1110','11','Kabupaten Bireuen','5.1086446','96.663812'),
	('1111','11','Kabupaten Aceh Utara','4.9786331','97.2221421'),
	('1112','11','Kabupaten Aceh Barat Daya','3.7963426','97.0068393'),
	('1113','11','Kabupaten Gayo Lues','3.955165','97.3516558'),
	('1114','11','Kabupaten Aceh Tamiang','4.2328871','98.0028892'),
	('1115','11','Kabupaten Nagan Raya','4.1248406','96.4929797'),
	('1116','11','Kabupaten Aceh Jaya','4.7873684','95.6457951'),
	('1117','11','Kabupaten Bener Meriah','4.7513606','96.9525224'),
	('1118','11','Kabupaten Pidie Jaya','5.1548063','96.195132'),
	('1171','11','Kota Banda Aceh','5.5482904','95.3237559'),
	('1172','11','Kota Sabang','5.880241','95.336574'),
	('1173','11','Kota Langsa','4.4757799','97.9641022'),
	('1174','11','Kota Lhokseumawe','5.1811638','97.1413222'),
	('1175','11','Kota Subulussalam','2.6449927','98.0165205'),
	('1201','12','Kabupaten Nias','1.0869444','97.7416703'),
	('1202','12','Kabupaten Mandailing Natal','0.7432372','99.3673084'),
	('1203','12','Kabupaten Tapanuli Selatan','1.5774933','99.2785583'),
	('1204','12','Kabupaten Tapanuli Tengah','1.8493299','98.704075'),
	('1205','12','Kabupaten Tapanuli Utara','2.0405246','99.1013498'),
	('1206','12','Kabupaten Toba Samosir','2.3502398','99.2785583'),
	('1207','12','Kabupaten Labuhan Batu','2.3439863','100.1703257'),
	('1208','12','Kabupaten Asahan','2.8174722','99.634135'),
	('1209','12','Kabupaten Simalungun','2.9781612','99.2785583'),
	('1210','12','Kabupaten Dairi','2.8675801','98.265058'),
	('1211','12','Kabupaten Karo','3.1052909','98.265058'),
	('1212','12','Kabupaten Deli Serdang','3.4201802','98.704075'),
	('1213','12','Kabupaten Langkat','3.8653916','98.3088441'),
	('1214','12','Kabupaten Nias Selatan','0.7086091','97.8286368'),
	('1215','12','Kabupaten Humbang Hasundutan','2.1988508','98.5721016'),
	('1216','12','Kabupaten Pakpak Bharat','2.5135376','98.2212979'),
	('1217','12','Kabupaten Samosir','2.6274431','98.7921836'),
	('1218','12','Kabupaten Serdang Bedagai','3.3371694','99.0571089'),
	('1219','12','Kabupaten Batu Bara','3.1740979','99.5006143'),
	('1220','12','Kabupaten Padang Lawas Utara','1.5758644','99.634135'),
	('1221','12','Kabupaten Padang Lawas','1.1186977','99.8124935'),
	('1222','12','Kabupaten Labuhan Batu Selatan','1.8799353','100.1703257'),
	('1223','12','Kabupaten Labuhan Batu Utara','2.3465638','99.8124935'),
	('1224','12','Kabupaten Nias Utara','1.3166036','97.394882'),
	('1225','12','Kabupaten Nias Barat','1.0116383','97.4814163'),
	('1271','12','Kota Sibolga','1.7368371','98.7851121'),
	('1272','12','Kota Tanjung Balai','2.9662346','99.7985457'),
	('1273','12','Kota Pematang Siantar','2.965147','99.0626377'),
	('1274','12','Kota Tebing Tinggi','3.3805315','99.2082661'),
	('1275','12','Kota Medan','3.5951956','98.6722227'),
	('1276','12','Kota Binjai','3.6135482','98.5025286'),
	('1277','12','Kota Padangsidimpuan','1.3721801','99.2730146'),
	('1278','12','Kota Gunungsitoli','1.2783137','97.6084204'),
	('1301','13','Kabupaten Kepulauan Mentawai','-1.426001','98.9245343'),
	('1302','13','Kabupaten Pesisir Selatan','-1.7223147','100.8903099'),
	('1303','13','Kabupaten Solok','-0.9643838','100.8903099'),
	('1304','13','Kabupaten Sijunjung','-0.6480415','100.8627167'),
	('1305','13','Kabupaten Tanah Datar','-0.4797043','100.5746224'),
	('1306','13','Kabupaten Padang Pariaman','-0.5546757','100.2151578'),
	('1307','13','Kabupaten Agam','-0.2209392','100.1703257'),
	('1308','13','Kabupaten Lima Puluh Kota','-0.1791407','100.647812'),
	('1309','13','Kabupaten Pasaman','0.1288752','99.7901781'),
	('1310','13','Kabupaten Solok Selatan','-1.4157329','101.2523792'),
	('1311','13','Kabupaten Dharmasraya','-1.1120568','101.6157773'),
	('1312','13','Kabupaten Pasaman Barat','0.2213005','99.634135'),
	('1371','13','Kota Padang','-0.9470832','100.417181'),
	('1372','13','Kota Solok','-0.7885335','100.6549823'),
	('1373','13','Kota Sawah Lunto','-0.5966493','100.7356108'),
	('1374','13','Kota Padang Panjang','-0.4652798','100.3992289'),
	('1375','13','Kota Bukittinggi','-0.3039178','100.383479'),
	('1376','13','Kota Payakumbuh','-0.2298336','100.6309169'),
	('1377','13','Kota Pariaman','-0.6115939','100.1336713'),
	('1401','14','Kabupaten Kuantan Singingi','-0.4411596','101.5248055'),
	('1402','14','Kabupaten Indragiri Hulu','-0.7361181','102.2547919'),
	('1403','14','Kabupaten Indragiri Hilir','-0.1456733','102.989615'),
	('1404','14','Kabupaten Pelalawan','0.5139797','102.1632718'),
	('1405','14','Kabupaten Siak','0.8118812','101.7979613'),
	('1406','14','Kabupaten Kampar','0.146671','101.1617356'),
	('1407','14','Kabupaten Rokan Hulu','1.0410934','100.439656'),
	('1408','14','Kabupaten Bengkalis','1.4139187','101.6157773'),
	('1409','14','Kabupaten Rokan Hilir','1.6463978','100.8000051'),
	('1410','14','Kabupaten Kepulauan Meranti','0.9208765','102.6675575'),
	('1471','14','Kota Pekanbaru','0.5070677','101.4477793'),
	('1473','14','Kota Dumai','1.6666349','101.4001855'),
	('1501','15','Kabupaten Kerinci','-1.8720467','101.4339148'),
	('1502','15','Kabupaten Merangin','-2.1752789','101.9804613'),
	('1503','15','Kabupaten Sarolangun','-2.3230422','102.7135121'),
	('1504','15','Kabupaten Batang Hari','-1.7083922','103.0817903'),
	('1505','15','Kabupaten Muaro Jambi','-1.6101229','103.6131203'),
	('1506','15','Kabupaten Tanjung Jabung Timur','-1.1024367','103.8216261'),
	('1507','15','Kabupaten Tanjung Jabung Barat','-1.105846','103.0817903'),
	('1508','15','Kabupaten Tebo','-1.2592999','102.3463875'),
	('1509','15','Kabupaten Bungo','-1.6401338','101.8891721'),
	('1571','15','Kota Jambi','-1.6101229','103.6131203'),
	('1572','15','Kota Sungai Penuh','-2.1225969','101.3204164'),
	('1601','16','Kabupaten Ogan Komering Ulu','-4.0283486','104.0072348'),
	('1602','16','Kabupaten Ogan Komering Ilir','-3.4559744','105.2194808'),
	('1603','16','Kabupaten Muara Enim','-3.7114163','104.0072348'),
	('1604','16','Kabupaten Lahat','-3.8008893','103.3587288'),
	('1605','16','Kabupaten Musi Rawas','-3.0956537','103.0817903'),
	('1606','16','Kabupaten Musi Banyuasin','-2.5442029','103.7289167'),
	('1607','16','Kabupaten Banyu Asin','-3.0956537','103.0817903'),
	('1608','16','Kabupaten Ogan Komering Ulu Selatan','-4.6681951','104.0072348'),
	('1609','16','Kabupaten Ogan Komering Ulu Timur','-3.8567934','104.7520939'),
	('1610','16','Kabupaten Ogan Ilir','-3.426544','104.6121475'),
	('1611','16','Kabupaten Empat Lawang','-3.7286029','102.8975098'),
	('1612','16','Kabupaten Penukal Abab Lematang Ilir','',''),
	('1613','16','Kabupaten Musi Rawas Utara','',''),
	('1671','16','Kota Palembang','-2.9760735','104.7754307'),
	('1672','16','Kota Prabumulih','-3.4213707','104.2436833'),
	('1673','16','Kota Pagar Alam','-4.0419617','103.2278845'),
	('1674','16','Kota Lubuklinggau','-3.2995858','102.857236'),
	('1701','17','Kabupaten Bengkulu Selatan','-4.3248409','103.035694'),
	('1702','17','Kabupaten Rejang Lebong','-3.4548154','102.6675575'),
	('1703','17','Kabupaten Bengkulu Utara','-3.2663246','101.9804613'),
	('1704','17','Kabupaten Kaur','-4.5215978','103.2663479'),
	('1705','17','Kabupaten Seluma','-4.0499387','102.7135121'),
	('1706','17','Kabupaten Mukomuko','-2.6449114','101.4339148'),
	('1707','17','Kabupaten Lebong','-3.1455094','102.2090224'),
	('1708','17','Kabupaten Kepahiang','-3.6441941','102.5642261'),
	('1709','17','Kabupaten Bengkulu Tengah','-3.6962324','102.3922135'),
	('1771','17','Kota Bengkulu','-3.7928451','102.2607641'),
	('1801','18','Kabupaten Lampung Barat','-5.1095293','104.1466046'),
	('1802','18','Kabupaten Tanggamus','-5.3027489','104.5655273'),
	('1803','18','Kabupaten Lampung Selatan','-5.5622614','105.5474373'),
	('1804','18','Kabupaten Lampung Timur','-5.1134995','105.6881788'),
	('1805','18','Kabupaten Lampung Tengah','-4.8008086','105.3131185'),
	('1806','18','Kabupaten Lampung Utara','-4.8133905','104.7520939'),
	('1807','18','Kabupaten Way Kanan','-4.4963689','104.5655273'),
	('1808','18','Kabupaten Tulangbawang','-7.086562','110.2850104'),
	('1809','18','Kabupaten Pesawaran','-5.493245','105.0791228'),
	('1810','18','Kabupaten Pringsewu','-5.3331186','104.9856176'),
	('1811','18','Kabupaten Mesuji','-4.0044783','105.3131185'),
	('1812','18','Kabupaten Tulang Bawang Barat','-4.54418','105.1492869'),
	('1813','18','Kabupaten Pesisir Barat','-2.2885814','101.0824912'),
	('1871','18','Kota Bandar Lampung','-5.3971396','105.2667887'),
	('1872','18','Kota Metro','-5.1178394','105.3072646'),
	('1901','19','Kabupaten Bangka','-1.874294','105.92299'),
	('1902','19','Kabupaten Belitung','-2.7216743','107.763621'),
	('1903','19','Kabupaten Bangka Barat','-1.8405046','105.5005483'),
	('1904','19','Kabupaten Bangka Tengah','-2.4007823','106.2051484'),
	('1905','19','Kabupaten Bangka Selatan','-2.7410513','106.4405872'),
	('1906','19','Kabupaten Belitung Timur','-2.8678037','108.1428669'),
	('1971','19','Kota Pangkal Pinang','-2.1316266','106.1169299'),
	('2101','21','Kabupaten Karimun','0.7697665','103.4049445'),
	('2102','21','Kabupaten Bintan','1.0619173','104.5189214'),
	('2103','21','Kabupaten Natuna','3.9456514','108.1428669'),
	('2104','21','Kabupaten Lingga','-0.4726065','104.4257533'),
	('2105','21','Kabupaten Kepulauan Anambas','2.923624','105.7585908'),
	('2171','21','Kota Batam','1.0456264','104.0304535'),
	('2172','21','Kota Tanjung Pinang','0.9046081','104.4839765'),
	('3101','31','Kabupaten Kepulauan Seribu','-5.7985265','106.5071981'),
	('3171','31','Kota Jakarta Selatan','-6.2689913','106.8060388'),
	('3172','31','Kota Jakarta Timur','-6.2250138','106.9004472'),
	('3173','31','Kota Jakarta Pusat','-6.1864864','106.8340911'),
	('3174','31','Kota Jakarta Barat','-6.1683295','106.7588494'),
	('3175','31','Kota Jakarta Utara','-6.1214609','106.7741133'),
	('3201','32','Kabupaten Bogor','-6.5517758','106.6291304'),
	('3202','32','Kabupaten Sukabumi','-7.2134052','106.6291304'),
	('3203','32','Kabupaten Cianjur','-7.3579773','107.1957203'),
	('3204','32','Kabupaten Bandung','-7.1340702','107.6215321'),
	('3205','32','Kabupaten Garut','-7.5012204','107.763621'),
	('3206','32','Kabupaten Tasikmalaya','-7.6513306','108.1428669'),
	('3207','32','Kabupaten Ciamis','-7.3320773','108.3492543'),
	('3208','32','Kabupaten Kuningan','-7.0138053','108.5700636'),
	('3209','32','Kabupaten Cirebon','-6.6898876','108.4750846'),
	('3210','32','Kabupaten Majalengka','-6.7790605','108.2852049'),
	('3211','32','Kabupaten Sumedang','0.6095949','110.0330554'),
	('3212','32','Kabupaten Indramayu','-6.33731','108.3258329'),
	('3213','32','Kabupaten Subang','-6.3487617','107.763621'),
	('3214','32','Kabupaten Purwakarta','-6.5649241','107.4321959'),
	('3215','32','Kabupaten Karawang','-6.3227303','107.3375791'),
	('3216','32','Kabupaten Bekasi','-6.2474466','107.1484521'),
	('3217','32','Kabupaten Bandung Barat','-6.8652214','107.4919767'),
	('3218','32','Kabupaten Pangandaran','-7.683333','108.65'),
	('3271','32','Kota Bogor','-6.5971469','106.8060388'),
	('3272','32','Kota Sukabumi','-6.9277361','106.9299579'),
	('3273','32','Kota Bandung','-6.9174639','107.6191228'),
	('3274','32','Kota Cirebon','-6.7320229','108.5523164'),
	('3275','32','Kota Bekasi','-6.2474466','107.1484521'),
	('3276','32','Kota Depok','-6.4024844','106.7942405'),
	('3277','32','Kota Cimahi','-6.8840816','107.5413039'),
	('3278','32','Kota Tasikmalaya','-7.3505808','108.2171633'),
	('3279','32','Kota Banjar','-7.3745849','108.5581899'),
	('3301','33','Kabupaten Cilacap','-7.6178096','108.902683'),
	('3302','33','Kabupaten Banyumas','-7.4832133','109.140438'),
	('3303','33','Kabupaten Purbalingga','-7.3058578','109.4259114'),
	('3304','33','Kabupaten Banjarnegara','-7.3794368','109.6163185'),
	('3305','33','Kabupaten Kebumen','-7.6680559','109.6524575'),
	('3306','33','Kabupaten Purworejo','-7.6964509','109.9989416'),
	('3307','33','Kabupaten Wonosobo','-7.3632094','109.9001796'),
	('3308','33','Kabupaten Magelang','-7.4305237','110.2832217'),
	('3309','33','Kabupaten Boyolali','-7.4317773','110.6883536'),
	('3310','33','Kabupaten Klaten','-7.739967','110.6645683'),
	('3311','33','Kabupaten Sukoharjo','-7.6483506','110.8552919'),
	('3312','33','Kabupaten Wonogiri','-7.8846484','111.0460407'),
	('3313','33','Kabupaten Karanganyar','-7.6387228','111.0460407'),
	('3314','33','Kabupaten Sragen','-7.43027','111.0091855'),
	('3315','33','Kabupaten Grobogan','-7.0217194','110.9625854'),
	('3316','33','Kabupaten Blora','-7.012244','111.3798928'),
	('3317','33','Kabupaten Rembang','-6.8082115','111.4275888'),
	('3318','33','Kabupaten Pati','-6.7449635','111.0460407'),
	('3319','33','Kabupaten Kudus','-6.7726186','110.8791343'),
	('3320','33','Kabupaten Jepara','-6.582711','110.6786933'),
	('3321','33','Kabupaten Demak','-6.9238879','110.6645683'),
	('3322','33','Kabupaten Semarang','-7.1764785','110.4738762'),
	('3323','33','Kabupaten Temanggung','-7.2748721','110.0891894'),
	('3324','33','Kabupaten Kendal','-7.0265442','110.1879106'),
	('3325','33','Kabupaten Batang','-7.0392183','109.9020509'),
	('3326','33','Kabupaten Pekalongan','-7.0517128','109.6163185'),
	('3327','33','Kabupaten Pemalang','-7.0599422','109.4259114'),
	('3328','33','Kabupaten Tegal','-6.9902371','109.140438'),
	('3329','33','Kabupaten Brebes','-6.9591793','108.902683'),
	('3371','33','Kota Magelang','-7.4797342','110.2176941'),
	('3372','33','Kota Surakarta','-7.5754887','110.8243272'),
	('3373','33','Kota Salatiga','-7.3305234','110.5084366'),
	('3374','33','Kota Semarang','-7.0051453','110.4381254'),
	('3375','33','Kota Pekalongan','-6.8898362','109.6745916'),
	('3376','33','Kota Tegal','-6.8797041','109.1255917'),
	('3401','34','Kabupaten Kulon Progo','-7.8266798','110.1640846'),
	('3402','34','Kabupaten Bantul','-7.9190169','110.3785438'),
	('3403','34','Kabupaten Gunung Kidul','-8.0305091','110.6168921'),
	('3404','34','Kabupaten Sleman','-7.7325213','110.402376'),
	('3471','34','Kota Yogyakarta','-7.7955798','110.3694896'),
	('3501','35','Kabupaten Pacitan','-8.126331','111.1414226'),
	('3502','35','Kabupaten Ponorogo','-7.8650759','111.4696322'),
	('3503','35','Kabupaten Trenggalek','-8.1824112','111.6183755'),
	('3504','35','Kabupaten Tulungagung','-8.0843211','111.9045541'),
	('3505','35','Kabupaten Blitar','-8.1308657','112.2200091'),
	('3506','35','Kabupaten Kediri','-7.8232397','112.1907122'),
	('3507','35','Kabupaten Malang','-8.242209','112.7152125'),
	('3508','35','Kabupaten Lumajang','-8.1327683','113.224557'),
	('3509','35','Kabupaten Jember','-8.1844859','113.6680747'),
	('3510','35','Kabupaten Banyuwangi','-8.2190944','114.3691416'),
	('3511','35','Kabupaten Bondowoso','-7.9673906','113.9060624'),
	('3512','35','Kabupaten Situbondo','-7.7888522','114.1914951'),
	('3513','35','Kabupaten Probolinggo','-7.8717562','113.4776098'),
	('3514','35','Kabupaten Pasuruan','-7.7859961','112.858217'),
	('3515','35','Kabupaten Sidoarjo','-7.4497718','112.7015495'),
	('3516','35','Kabupaten Mojokerto','-7.563831','112.4768287'),
	('3517','35','Kabupaten Jombang','-7.5740867','112.28609'),
	('3518','35','Kabupaten Nganjuk','-7.5943507','111.9045541'),
	('3519','35','Kabupaten Madiun','-7.6093306','111.6183755'),
	('3520','35','Kabupaten Magetan','-7.6433138','111.356045'),
	('3521','35','Kabupaten Ngawi','-7.460987','111.3321974'),
	('3522','35','Kabupaten Bojonegoro','-7.3174629','111.7614661'),
	('3523','35','Kabupaten Tuban','-6.8949099','112.0416754'),
	('3524','35','Kabupaten Lamongan','-7.1269261','112.3337769'),
	('3525','35','Kabupaten Gresik','-7.1550291','112.5721881'),
	('3526','35','Kabupaten Bangkalan','-7.038375','112.9136695'),
	('3527','35','Kabupaten Sampang','-7.0402326','113.2394452'),
	('3528','35','Kabupaten Pamekasan','-7.1050857','113.5252319'),
	('3529','35','Kabupaten Sumenep','-6.9253999','113.9060624'),
	('3571','35','Kota Kediri','-7.8480156','112.0178286'),
	('3572','35','Kota Blitar','-8.0954627','112.1609056'),
	('3573','35','Kota Malang','-7.9666204','112.6326321'),
	('3574','35','Kota Probolinggo','-7.7764226','113.2037131'),
	('3575','35','Kota Pasuruan','-7.6469193','112.8999225'),
	('3576','35','Kota Mojokerto','-7.4704747','112.4401329'),
	('3577','35','Kota Madiun','-7.6310587','111.5300159'),
	('3578','35','Kota Surabaya','-7.2574719','112.7520883'),
	('3579','35','Kota Batu','-7.8830648','112.5334492'),
	('3601','36','Kabupaten Pandeglang','-6.7482706','105.6881788'),
	('3602','36','Kabupaten Lebak','-6.5643956','106.2522143'),
	('3603','36','Kabupaten Tangerang','-6.1872101','106.4877072'),
	('3604','36','Kabupaten Serang','-6.1397339','106.040506'),
	('3671','36','Kota Tangerang','-6.2023936','106.6527099'),
	('3672','36','Kota Cilegon','-6.0025343','106.0111203'),
	('3673','36','Kota Serang','-6.1103661','106.1639749'),
	('3674','36','Kota Tangerang Selatan','-6.2835218','106.7112933'),
	('5101','51','Kabupaten Jembrana','-8.3233438','114.6667939'),
	('5102','51','Kabupaten Tabanan','-8.4595561','115.0465991'),
	('5103','51','Kabupaten Badung','-8.5819296','115.1770586'),
	('5104','51','Kabupaten Gianyar','-8.4248244','115.2600506'),
	('5105','51','Kabupaten Klungkung','-8.727807','115.5444231'),
	('5106','51','Kabupaten Bangli','-8.2975884','115.3548713'),
	('5107','51','Kabupaten Karang Asem','-8.3465933','115.5207358'),
	('5108','51','Kabupaten Buleleng','-8.2238968','114.9516869'),
	('5171','51','Kota Denpasar','-8.6704582','115.2126293'),
	('5201','52','Kabupaten Lombok Barat','-8.6464599','116.1123078'),
	('5202','52','Kabupaten Lombok Tengah','-8.694623','116.2777073'),
	('5203','52','Kabupaten Lombok Timur','-8.5134471','116.5609857'),
	('5204','52','Kabupaten Sumbawa','-8.6529334','117.3616476'),
	('5205','52','Kabupaten Dompu','-8.4966318','118.4747173'),
	('5206','52','Kabupaten Bima','-8.4353962','118.626479'),
	('5207','52','Kabupaten Sumbawa Barat','-8.9292907','116.8910342'),
	('5208','52','Kabupaten Lombok Utara','-8.3739076','116.2777073'),
	('5271','52','Kota Mataram','-8.5769951','116.1004894'),
	('5272','52','Kota Bima','-8.4642661','118.7449028'),
	('5301','53','Kabupaten Sumba Barat','-9.6548326','119.3947135'),
	('5302','53','Kabupaten Sumba Timur','-9.9802103','120.3435506'),
	('5303','53','Kabupaten Kupang','-9.9906166','123.8857747'),
	('5304','53','Kabupaten Timor Tengah Selatan','-9.7762816','124.4198243'),
	('5305','53','Kabupaten Timor Tengah Utara','-9.4522647','124.597132'),
	('5306','53','Kabupaten Belu','-9.1538978','124.906551'),
	('5307','53','Kabupaten Alor','-8.2928427','124.5528387'),
	('5308','53','Kabupaten Lembata','-8.4719075','123.4831906'),
	('5309','53','Kabupaten Flores Timur','-8.3130942','122.9663018'),
	('5310','53','Kabupaten Sikka','-8.6766175','122.1291843'),
	('5311','53','Kabupaten Ende','-8.6762912','121.7195459'),
	('5312','53','Kabupaten Ngada','-8.7430424','120.9876321'),
	('5313','53','Kabupaten Manggarai','-8.6796987','120.3896651'),
	('5314','53','Kabupaten Rote Ndao','-10.7386421','123.1239049'),
	('5315','53','Kabupaten Manggarai Barat','-8.6688149','120.0665236'),
	('5316','53','Kabupaten Sumba Tengah','-9.4879226','119.6962677'),
	('5317','53','Kabupaten Sumba Barat Daya','-9.539139','119.1390642'),
	('5318','53','Kabupaten Nagekeo','-8.6753545','121.3084088'),
	('5319','53','Kabupaten Manggarai Timur','-8.6206712','120.6199895'),
	('5320','53','Kabupaten Sabu Raijua','-10.5541116','121.8334868'),
	('5321','53','Kabupaten Malaka','',''),
	('5371','53','Kota Kupang','-10.1771997','123.6070329'),
	('6101','61','Kabupaten Sambas','1.3625191','109.2831531'),
	('6102','61','Kabupaten Bengkayang','1.06911','109.6639309'),
	('6103','61','Kabupaten Landak','0.4237287','109.7591675'),
	('6104','61','Kabupaten Mempawah','',''),
	('6105','61','Kabupaten Sanggau','0.1400117','110.5215459'),
	('6106','61','Kabupaten Ketapang','-1.859098','109.971901'),
	('6107','61','Kabupaten Sintang','-0.1378068','112.8105512'),
	('6108','61','Kabupaten Kapuas Hulu','0.8336697','113.0011989'),
	('6109','61','Kabupaten Sekadau','-0.0697175','110.9983515'),
	('6110','61','Kabupaten Melawi','-0.7000681','111.6660725'),
	('6111','61','Kabupaten Kayong Utara','-0.9225877','110.0449662'),
	('6112','61','Kabupaten Kubu Raya','-0.3533938','109.4735066'),
	('6171','61','Kota Pontianak','-0.0263303','109.3425039'),
	('6172','61','Kota Singkawang','0.9060204','108.9872049'),
	('6201','62','Kabupaten Kotawaringin Barat','-2.5063419','111.7614661'),
	('6202','62','Kabupaten Kotawaringin Timur','-2.1225475','112.8105512'),
	('6203','62','Kabupaten Kapuas','-1.8116445','114.3341432'),
	('6204','62','Kabupaten Barito Selatan','-1.875943','114.8092691'),
	('6205','62','Kabupaten Barito Utara','-0.9587136','115.094045'),
	('6206','62','Kabupaten Sukamara','-2.6267517','111.2368084'),
	('6207','62','Kabupaten Lamandau','-1.8526377','111.2845025'),
	('6208','62','Kabupaten Seruyan','-3.0123467','112.4291464'),
	('6209','62','Kabupaten Katingan','-0.9758379','112.8105512'),
	('6210','62','Kabupaten Pulang Pisau','-2.6849607','113.9536466'),
	('6211','62','Kabupaten Gunung Mas','-1.2522464','113.5728501'),
	('6212','62','Kabupaten Barito Timur','-2.0123999','115.188916'),
	('6213','62','Kabupaten Murung Raya','-0.1362171','114.3341432'),
	('6271','62','Kota Palangka Raya','-2.2161048','113.913977'),
	('6301','63','Kabupaten Tanah Laut','-3.7694047','114.8092691'),
	('6302','63','Kabupaten Kota Baru','-6.3889127','107.4913489'),
	('6303','63','Kabupaten Banjar','-3.3200228','114.9991464'),
	('6304','63','Kabupaten Barito Kuala','-3.0714738','114.6667939'),
	('6305','63','Kabupaten Tapin','-2.9160746','115.0465991'),
	('6306','63','Kabupaten Hulu Sungai Selatan','-2.7662681','115.2363408'),
	('6307','63','Kabupaten Hulu Sungai Tengah','-2.6153162','115.5207358'),
	('6308','63','Kabupaten Hulu Sungai Utara','-2.4421225','115.188916'),
	('6309','63','Kabupaten Tabalong','-1.864302','115.5681084'),
	('6310','63','Kabupaten Tanah Bumbu','-3.4512244','115.5681084'),
	('6311','63','Kabupaten Balangan','-2.3260425','115.6154732'),
	('6371','63','Kota Banjarmasin','-3.3186067','114.5943784'),
	('6372','63','Kota Banjar Baru','-3.4572422','114.8103181'),
	('6401','64','Kabupaten Paser','-1.7175266','115.9467997'),
	('6402','64','Kabupaten Kutai Barat','-0.4051796','115.8521764'),
	('6403','64','Kabupaten Kutai Kartanegara','-0.1336655','116.6081653'),
	('6404','64','Kabupaten Kutai Timur','0.9433774','116.9852422'),
	('6405','64','Kabupaten Berau','2.0450883','117.3616476'),
	('6409','64','Kabupaten Penajam Paser Utara','-1.2917094','116.5137964'),
	('6411','64','Kabupaten Mahakam Hulu','',''),
	('6471','64','Kota Balikpapan','-1.2379274','116.8528526'),
	('6472','64','Kota Samarinda','-0.4948232','117.1436154'),
	('6474','64','Kota Bontang','0.120863','117.4800445'),
	('6501','65','Kabupaten Malinau','3.0730929','116.0413889'),
	('6502','65','Kabupaten Bulungan','',''),
	('6503','65','Kabupaten Tana Tidung','3.551869','117.0794082'),
	('6504','65','Kabupaten Nunukan','4.0609227','117.666952'),
	('6571','65','Kota Tarakan','3.3273599','117.5785049'),
	('7101','71','Kabupaten Bolaang Mongondow','',''),
	('7102','71','Kabupaten Minahasa','1.2168837','124.8182593'),
	('7103','71','Kabupaten Kepulauan Sangihe','3.5303212','125.5438967'),
	('7104','71','Kabupaten Kepulauan Talaud','4.3066741','126.8034921'),
	('7105','71','Kabupaten Minahasa Selatan','1.0946773','124.4641848'),
	('7106','71','Kabupaten Minahasa Utara','1.5327973','124.994751'),
	('7107','71','Kabupaten Bolaang Mongondow Utara','0.9070359','123.2657311'),
	('7108','71','Kabupaten Siau Tagulandang Biaro','',''),
	('7109','71','Kabupaten Minahasa Tenggara','1.0278551','124.7298765'),
	('7110','71','Kabupaten Bolaang Mongondow Selatan','0.4053215','123.8411288'),
	('7111','71','Kabupaten Bolaang Mongondow Timur','0.7152651','124.4641848'),
	('7171','71','Kota Manado','1.4748305','124.8420794'),
	('7172','71','Kota Bitung','1.4403744','125.1216524'),
	('7173','71','Kota Tomohon','1.3229337','124.8405081'),
	('7174','71','Kota Kotamobagu','0.7243733','124.3199316'),
	('7201','72','Kabupaten Banggai Kepulauan','-1.3075939','123.0338767'),
	('7202','72','Kabupaten Banggai','-0.956178','122.6277455'),
	('7203','72','Kabupaten Morowali','-2.6987231','121.9017954'),
	('7204','72','Kabupaten Poso','-1.6468883','120.4357631'),
	('7205','72','Kabupaten Donggala','-0.4233155','119.8352303'),
	('7206','72','Kabupaten Toli-Toli','0.8768231','120.7579834'),
	('7207','72','Kabupaten Buol','0.9695452','121.3541631'),
	('7208','72','Kabupaten Parigi Moutong','0.5817607','120.8039474'),
	('7209','72','Kabupaten Tojo Una-Una','-1.098757','121.5370003'),
	('7210','72','Kabupaten Sigi','-1.3859904','119.8815203'),
	('7211','72','Kabupaten Banggai Laut','',''),
	('7212','72','Kabupaten Morowali Utara','',''),
	('7271','72','Kota Palu','-0.9002915','119.8779987'),
	('7301','73','Kabupaten Kepulauan Selayar','',''),
	('7302','73','Kabupaten Bulukumba','-5.4329368','120.2051096'),
	('7303','73','Kabupaten Bantaeng','-5.5169316','120.0202964'),
	('7304','73','Kabupaten Jeneponto','-5.554579','119.6730939'),
	('7305','73','Kabupaten Takalar','-5.4162493','119.4875668'),
	('7306','73','Kabupaten Gowa','-5.3102888','119.742604'),
	('7307','73','Kabupaten Sinjai','-5.2171961','120.112735'),
	('7308','73','Kabupaten Maros','-5.0549145','119.6962677'),
	('7309','73','Kabupaten Pangkajene Dan Kepulauan','',''),
	('7310','73','Kabupaten Barru','-4.436417','119.6499162'),
	('7311','73','Kabupaten Bone','-4.7443383','120.0665236'),
	('7312','73','Kabupaten Soppeng','-4.3518541','119.9277947'),
	('7313','73','Kabupaten Wajo','-4.022229','120.0665236'),
	('7314','73','Kabupaten Sidenreng Rappang','',''),
	('7315','73','Kabupaten Pinrang','-3.6483486','119.5571677'),
	('7316','73','Kabupaten Enrekang','-3.4590744','119.8815203'),
	('7317','73','Kabupaten Luwu','-3.3052214','120.2512728'),
	('7318','73','Kabupaten Tana Toraja','-3.0753003','119.742604'),
	('7322','73','Kabupaten Luwu Utara','-2.2690446','119.9740534'),
	('7325','73','Kabupaten Luwu Timur','-2.5825518','121.1710389'),
	('7326','73','Kabupaten Toraja Utara','-2.8621942','119.8352303'),
	('7371','73','Kota Makassar','-5.1476651','119.4327314'),
	('7372','73','Kota Parepare','-4.0096221','119.6290617'),
	('7373','73','Kota Palopo','-3.0108458','120.2022239'),
	('7401','74','Kabupaten Buton','-5.3096355','122.9888319'),
	('7402','74','Kabupaten Muna','-4.901629','122.6277455'),
	('7403','74','Kabupaten Konawe','-3.9380432','122.0837445'),
	('7404','74','Kabupaten Kolaka','-3.9946988','121.5826642'),
	('7405','74','Kabupaten Konawe Selatan','-4.2027915','122.4467238'),
	('7406','74','Kabupaten Bombana','-4.6543462','121.9017954'),
	('7407','74','Kabupaten Wakatobi','-5.3264442','123.5951925'),
	('7408','74','Kabupaten Kolaka Utara','-3.1347227','121.1710389'),
	('7409','74','Kabupaten Buton Utara','-4.7023424','123.0338767'),
	('7410','74','Kabupaten Konawe Utara','-3.3803291','122.0837445'),
	('7411','74','Kabupaten Kolaka Timur','',''),
	('7412','74','Kabupaten Konawe Kepulauan','',''),
	('7413','74','Kabupaten Muna Barat','',''),
	('7414','74','Kabupaten Buton Tengah','',''),
	('7415','74','Kabupaten Buton Selatan','',''),
	('7471','74','Kota Kendari','-3.9984597','122.5129742'),
	('7472','74','Kota Baubau','-5.4700112','122.5976841'),
	('7501','75','Kabupaten Boalemo','0.7013419','122.2653887'),
	('7502','75','Kabupaten Gorontalo','0.6999372','122.4467238'),
	('7503','75','Kabupaten Pohuwato','0.7055278','121.7195459'),
	('7504','75','Kabupaten Bone Bolango','0.5657885','123.3486147'),
	('7505','75','Kabupaten Gorontalo Utara','0.9252647','122.4920088'),
	('7571','75','Kota Gorontalo','0.5435442','123.0567693'),
	('7601','76','Kabupaten Majene','-3.0297251','118.9062794'),
	('7602','76','Kabupaten Polewali Mandar','-3.3419323','119.1390642'),
	('7603','76','Kabupaten Mamasa','-2.9118209','119.3250347'),
	('7604','76','Kabupaten Mamuju','-2.7293364','118.9295737'),
	('7605','76','Kabupaten Mamuju Utara','-1.5264542','119.5107708'),
	('7606','76','Kabupaten Mamuju Tengah','',''),
	('8101','81','Kabupaten Maluku Tenggara Barat','-7.5322642','131.3611121'),
	('8102','81','Kabupaten Maluku Tenggara','-5.7512455','132.7271587'),
	('8103','81','Kabupaten Maluku Tengah','-3.0166501','129.4864411'),
	('8104','81','Kabupaten Buru','-3.3307379','126.6957216'),
	('8105','81','Kabupaten Kepulauan Aru','-6.1946502','134.5501935'),
	('8106','81','Kabupaten Seram Bagian Barat','-3.1271575','128.4008357'),
	('8107','81','Kabupaten Seram Bagian Timur','-3.4233267','130.2271243'),
	('8108','81','Kabupaten Maluku Barat Daya','-7.7851588','126.3498097'),
	('8109','81','Kabupaten Buru Selatan','-3.7273972','126.6957216'),
	('8171','81','Kota Ambon','-3.6553932','128.1907723'),
	('8172','81','Kota Tual','-5.5680317','132.3446399'),
	('8201','82','Kabupaten Halmahera Barat','1.3589663','127.5960704'),
	('8202','82','Kabupaten Halmahera Tengah','0.4419543','128.3587174'),
	('8203','82','Kabupaten Kepulauan Sula','-1.8321222','125.958777'),
	('8204','82','Kabupaten Halmahera Selatan','-1.5109015','127.7237678'),
	('8205','82','Kabupaten Halmahera Utara','1.5074308','127.8936663'),
	('8206','82','Kabupaten Halmahera Timur','1.3121235','128.4849923'),
	('8207','82','Kabupaten Pulau Morotai','2.3656672','128.4008357'),
	('8208','82','Kabupaten Pulau Taliabu','',''),
	('8271','82','Kota Ternate','0.7898868','127.3753792'),
	('8272','82','Kota Tidore Kepulauan','0.5060207','127.681228'),
	('9101','91','Kabupaten Fakfak','-3.097706','133.0194897'),
	('9102','91','Kabupaten Kaimana','-3.288406','133.9436788'),
	('9103','91','Kabupaten Teluk Wondama','-2.8551699','134.3236557'),
	('9104','91','Kabupaten Teluk Bintuni','-1.9056848','133.329466'),
	('9105','91','Kabupaten Manokwari','-0.8614531','134.0620421'),
	('9106','91','Kabupaten Sorong Selatan','-1.7657744','132.1572702'),
	('9107','91','Kabupaten Sorong','-0.8819986','131.2954834'),
	('9108','91','Kabupaten Raja Ampat','-1.0915151','130.8778586'),
	('9109','91','Kabupaten Tambrauw','-0.781856','132.3938375'),
	('9110','91','Kabupaten Maybrat','-1.2970979','132.3150993'),
	('9111','91','Kabupaten Manokwari Selatan','-0.9135107','134.0008674'),
	('9112','91','Kabupaten Pegunungan Arfak','-0.9533333','133.8338889'),
	('9171','91','Kota Sorong','-0.8819986','131.2954834'),
	('9401','94','Kabupaten Merauke','-7.7838334','139.041312'),
	('9402','94','Kabupaten Jayawijaya','-4.0004481','138.7995122'),
	('9403','94','Kabupaten Jayapura','-2.987923','139.8547266'),
	('9404','94','Kabupaten Nabire','-3.5095462','135.7520985'),
	('9408','94','Kabupaten Kepulauan Yapen','-1.7862914','135.6728399'),
	('9409','94','Kabupaten Biak Numfor','-1.0381022','135.9800848'),
	('9410','94','Kabupaten Paniai','-3.7876441','136.3624686'),
	('9411','94','Kabupaten Puncak Jaya','-3.4467891','137.8427298'),
	('9412','94','Kabupaten Mimika','-4.4553223','137.1362125'),
	('9413','94','Kabupaten Boven Digoel','-5.7400018','140.3481835'),
	('9414','94','Kabupaten Mappi','-6.7606468','139.6911374'),
	('9415','94','Kabupaten Asmat','-5.0573958','138.3988186'),
	('9416','94','Kabupaten Yahukimo','-4.4939717','139.5279996'),
	('9417','94','Kabupaten Pegunungan Bintang','-4.5589872','140.5135589'),
	('9418','94','Kabupaten Tolikara','-3.481132','138.4787258'),
	('9419','94','Kabupaten Sarmi','-2.4678144','139.2030851'),
	('9420','94','Kabupaten Keerom','-3.3449536','140.7624493'),
	('9426','94','Kabupaten Waropen','-2.8435717','136.670534'),
	('9427','94','Kabupaten Supiori','-0.7295099','135.6385125'),
	('9428','94','Kabupaten Mamberamo Raya','-2.5331255','137.7637565'),
	('9429','94','Kabupaten Nduga','-4.4069496','138.2393528'),
	('9430','94','Kabupaten Lanny Jaya','-3.971033','138.3190276'),
	('9431','94','Kabupaten Mamberamo Tengah','-3.6321083','138.9605972'),
	('9432','94','Kabupaten Yalimo','-3.7852847','139.4466005'),
	('9433','94','Kabupaten Puncak','-6.7028188','106.9990215'),
	('9434','94','Kabupaten Dogiyai','-4.0454139','135.6763443'),
	('9435','94','Kabupaten Intan Jaya','-3.5076422','136.7478493'),
	('9436','94','Kabupaten Deiyai','-4.154975','136.1279586'),
	('9471','94','Kota Jayapura','-2.5916025','140.6689995');

/*!40000 ALTER TABLE `hijr_district` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_file`;

CREATE TABLE `hijr_file` (
  `id_file` varchar(50) NOT NULL DEFAULT '',
  `name_file` varchar(260) NOT NULL DEFAULT '',
  `folder_file` varchar(260) NOT NULL DEFAULT '',
  `size_file` bigint(20) NOT NULL,
  `type_file` varchar(50) DEFAULT '',
  `reference_id_file` varchar(50) DEFAULT '',
  `module_file` varchar(50) DEFAULT NULL,
  `person_added_file` varchar(200) NOT NULL DEFAULT '',
  `time_added_file` datetime NOT NULL,
  `load_file` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table hijr_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_login`;

CREATE TABLE `hijr_login` (
  `id_login` varchar(50) NOT NULL DEFAULT '',
  `access_token_login` text NOT NULL,
  `client_id_login` varchar(50) NOT NULL DEFAULT '',
  `username_login` varchar(50) NOT NULL DEFAULT '',
  `refresh_token_login` text NOT NULL,
  `created_time_login` datetime NOT NULL,
  `expire_time_login` datetime NOT NULL,
  `token_object_login` blob NOT NULL,
  `account_id_login` varchar(50) DEFAULT NULL,
  `position_id_login` varchar(50) DEFAULT NULL,
  `organization_id_login` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_login` WRITE;
/*!40000 ALTER TABLE `hijr_login` DISABLE KEYS */;

INSERT INTO `hijr_login` (`id_login`, `access_token_login`, `client_id_login`, `username_login`, `refresh_token_login`, `created_time_login`, `expire_time_login`, `token_object_login`, `account_id_login`, `position_id_login`, `organization_id_login`)
VALUES
	('089c42a5-10c9-11e9-b795-1b8ef4780d0d','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTc4MjE1MTIyLCJqdGkiOiJkM2NjYjA2Ny00YTM5LTQ4NjUtOGY1Ny02YzU4YTNmY2ZkMTAiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.JJNifAbsNIu7hkrpfjfkUOTRtI5iSVi-nqwfjrTzmWqwl32jQ9A9-jbcbSYvqIQH6y9zFa0Tn_iNcro-gtRr-qFGl5evVyqGrZq6AE776CNZ3irhOijNjV3LwRxgp-B3BJY7OdS8nqtW0plhA6IfdYB16wVCn7JCN3XYO6FzcfUzcgkDAp5Fi-mFUEyMSmtFGi7wS-pBN7wHF2ZRIuloAB7_hefeCxRI3hthQnz_VnJ6foADVqw6MSjhWBVVxYBd10_OkADrkP9XsXMCVnHbWHGZZXNVJQAdl-fcdo-ZocIETvCqmEddKQ05y4Vpgt-bPdwY62K4BG1RkdahqUpQKg','sso-web','1520819042544::003::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiYXRpIjoiZDNjY2IwNjctNGEzOS00ODY1LThmNTctNmM1OGEzZmNmZDEwIiwiZXhwIjoxNTQ2NzA5MTIyLCJqdGkiOiJhYzE3OWM5Mi0xNjYzLTQ0OGItODMxNy0xMDljOTdiOTRkNWQiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.H-jQ11N68aZU4ktenyoNgko-g7vKkslTDRBCrhUZr8GvGhr9p3QumWJcWAdhUPPXTXxAwkVppxZ1v95HwCGJGrjQMfCrM2tzkG9HEJ9Gf2MaXHYr7rzhzbcMOZ4RB0ytc8kVp7nSCLvlX3XtBEQ9F6AW1XJ-QnU5gveZ-E1_pvOTqFYT4ag5ZHfuq5rN9X_LEp2-tLfz72FKu1u-SHaMoaLK0CwY0ELvftuzGHzqAwKsuzU0LEAbpw4m3u7V7Nx433UhHKEK0d_bebGOzazZxzp4LT0buhpXzzKMqXROv4OlLRLBAX-AByRd_1KXAr00ZT0Bx2OXCIy0p15HY71ztQ','2019-01-05 00:00:00','2020-01-05 00:00:00',X'ACED0005737200436F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F4175746832416363657373546F6B656E0CB29E361B24FACE0200064C00156164646974696F6E616C496E666F726D6174696F6E74000F4C6A6176612F7574696C2F4D61703B4C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B4C000C72656672657368546F6B656E74003F4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F636F6D6D6F6E2F4F417574683252656672657368546F6B656E3B4C000573636F706574000F4C6A6176612F7574696C2F5365743B4C0009746F6B656E547970657400124C6A6176612F6C616E672F537472696E673B4C000576616C756571007E00057870737200176A6176612E7574696C2E4C696E6B6564486173684D617034C04E5C106CC0FB0200015A000B6163636573734F72646572787200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000874000A6163636F756E745F696474000D3135323038313930343235343474000966756C6C5F6E616D6574000B53757065722041646D696E74000F6F7267616E697A6174696F6E5F696474000D3331313032303134303030303074000D706F736974696F6E5F6E616D65740014446972656B747572204F7065726173696F6E616C7400116F7267616E697A6174696F6E5F6E616D6574001750542E48494A5220474C4F42414C20534F4C5554494F4E74000B706F736974696F6E5F696474000330303374000E6163636F756E745F61766174617274003A68747470733A2F2F636C6F75642E68696A722E636F2E69642F73736F2F696D616765732F757365722E706E673F313532303831393034323534347400036A746974002464336363623036372D346133392D343836352D386635372D36633538613366636664313078007372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000016F74F3A809787372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E71007E0002787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C756571007E000578707404D865794A68624763694F694A53557A49314E694973496E523563434936496B705856434A392E65794A316332567958323568625755694F6949784E5449774F4445354D4451794E5451304F6A6F774D444D364F6A4D784D5441794D4445304D4441774D4441694C434A7762334E7064476C76626C39755957316C496A6F6952476C795A577430645849675433426C636D467A61573975595777694C434A76636D6468626D6C365958527062323566626D46745A534936496C42554C6B684A536C496752307850516B464D49464E50544656555355394F496977695958563061473979615852705A584D694F6C7369556B394D525639515331425558315654525649694C434A53543078465830464D5346426656564E4655694973496C4A5054455666515642535830464554556C4F49697769556B394D52563954515652545245356656564E4655694973496C4A50544556665546524D58315654525649694C434A535430784658304651556C3950554556535156525055694973496C4A50544556665545745358315654525649694C434A535430784658314E56554556535830464554556C4F49697769556B394D5256394255464A6656564E4655694A644C434A6A62476C6C626E5266615751694F694A7A63323874643256694969776959574E6A62335675644639705A434936496A45314D6A41344D546B774E4449314E4451694C434A6D6457787358323568625755694F694A546458426C636942425A47317062694973496E4E6A6233426C496A7062496D6870616E4A66593239795A534A644C434A76636D6468626D6C365958527062323566615751694F69497A4D5445774D6A41784E4441774D4441774969776959585270496A6F695A444E6A593249774E6A63744E47457A4F5330304F4459314C54686D4E5463744E6D4D314F47457A5A6D4E6D5A444577496977695A586877496A6F784E5451324E7A41354D5449794C434A7164476B694F694A68597A45334F574D354D6930784E6A597A4C5451304F4749744F444D784E7930784D446C6A4F5464694F54526B4E5751694C434A7762334E7064476C76626C39705A434936496A41774D794973496D466A59323931626E526659585A6864474679496A6F696148523063484D364C79396A624739315A43356F615770794C6D4E764C6D6C6B4C334E7A627939706257466E5A584D7664584E6C63693577626D635F4D5455794D4467784F5441304D6A55304E434A392E482D6A5131314E3638615A55346B74656E796F4E676B6F2D6737764B6B736C54445242437268555A7238477647687239703351756D574A63574164685550505854587841776B567070785A31763935487743474A47726A514D6643724D32747A6B473948454A394766324D615848597237727A687A62634D4F5A34524230797463386B5670376E53434C766C58335874424551394636415731584A2D516E55356776655A2D45315F70764F5471465954346167355A4866757135724E39585F4C4570322D744C667A3732464B7531752D5348614D6F614C4B3043775930454C766674757A47487A7141774B73757A55304C4541627077346D33753756374E783433335568484B454B30645F626562474F7A617A5A787A70344C543062756870587A7A4B4D7158524F76344F6C4C524C4241582D41427952645F314B58417230305A54304278324F5843497930703135485937317A74517371007E001A7708000001681F0C3F8678737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C0001637400164C6A6176612F7574696C2F436F6C6C656374696F6E3B7870737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000174000968696A725F636F72657874000662656172657274049C65794A68624763694F694A53557A49314E694973496E523563434936496B705856434A392E65794A316332567958323568625755694F6949784E5449774F4445354D4451794E5451304F6A6F774D444D364F6A4D784D5441794D4445304D4441774D4441694C434A7762334E7064476C76626C39755957316C496A6F6952476C795A577430645849675433426C636D467A61573975595777694C434A76636D6468626D6C365958527062323566626D46745A534936496C42554C6B684A536C496752307850516B464D49464E50544656555355394F496977695958563061473979615852705A584D694F6C7369556B394D525639515331425558315654525649694C434A53543078465830464D5346426656564E4655694973496C4A5054455666515642535830464554556C4F49697769556B394D52563954515652545245356656564E4655694973496C4A50544556665546524D58315654525649694C434A535430784658304651556C3950554556535156525055694973496C4A50544556665545745358315654525649694C434A535430784658314E56554556535830464554556C4F49697769556B394D5256394255464A6656564E4655694A644C434A6A62476C6C626E5266615751694F694A7A63323874643256694969776959574E6A62335675644639705A434936496A45314D6A41344D546B774E4449314E4451694C434A6D6457787358323568625755694F694A546458426C636942425A47317062694973496E4E6A6233426C496A7062496D6870616E4A66593239795A534A644C434A76636D6468626D6C365958527062323566615751694F69497A4D5445774D6A41784E4441774D444177496977695A586877496A6F784E5463344D6A45314D5449794C434A7164476B694F694A6B4D324E6A596A41324E79303059544D354C5451344E6A55744F4759314E793032597A553459544E6D59325A6B4D5441694C434A7762334E7064476C76626C39705A434936496A41774D794973496D466A59323931626E526659585A6864474679496A6F696148523063484D364C79396A624739315A43356F615770794C6D4E764C6D6C6B4C334E7A627939706257466E5A584D7664584E6C63693577626D635F4D5455794D4467784F5441304D6A55304E434A392E4A4A4E69664162734E497537686B7270666A666B554F5452744935695356692D6E7177666A72547A6D5771776C33326A513941392D6A626362535976714951483679397A466130546E5F694E63726F2D677452722D7146476C35657656797147725A71364145373736434E5A336972684F696A4E6A56334C77527867702D4233424A59374F6453386E71745730706C684136496664594231367756436E374A434E3358594F36467A6366557A63676B4441703546692D6D465545794D536D744647693777532D70424E37774846325A5249756C6F4142375F686566654378524933687468516E7A5F566E4A36666F4144567177364D536A68574256567859426431305F4F6B4144726B50395873584D43566E48625748475A5A584E564A5141646C2D6663646F2D5A6F634945547643716D4564644B5130357934567067742D625064775936324B34424731526B646168715570514B67','1520819042544','003','3110201400000');

/*!40000 ALTER TABLE `hijr_login` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_login_archive
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_login_archive`;

CREATE TABLE `hijr_login_archive` (
  `id_login` varchar(50) NOT NULL DEFAULT '',
  `access_token_login` text NOT NULL,
  `client_id_login` varchar(50) NOT NULL DEFAULT '',
  `username_login` varchar(50) NOT NULL DEFAULT '',
  `refresh_token_login` text NOT NULL,
  `created_time_login` datetime NOT NULL,
  `expire_time_login` datetime NOT NULL,
  `token_object_login` blob NOT NULL,
  `account_id_login` varchar(50) DEFAULT NULL,
  `position_id_login` varchar(50) DEFAULT NULL,
  `organization_id_login` varchar(50) DEFAULT NULL,
  `status_login` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_login_archive` WRITE;
/*!40000 ALTER TABLE `hijr_login_archive` DISABLE KEYS */;

INSERT INTO `hijr_login_archive` (`id_login`, `access_token_login`, `client_id_login`, `username_login`, `refresh_token_login`, `created_time_login`, `expire_time_login`, `token_object_login`, `account_id_login`, `position_id_login`, `organization_id_login`, `status_login`)
VALUES
	('ad6aa5ad-10c5-11e9-963d-d9745082ac83','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTc4MjEzNjgxLCJqdGkiOiIwM2JlYzQ4OS1kYzk1LTQ2ZjEtYmU4Yi0xOGExYTg4NDg3ZmQiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.WoEP18XDgWW4nUPBA9ovIEqJDK2HUunKcIzuzc9uVCteexcTebvyuTzMZ2hadmVO7zXc4GJabORJg0uQvaXQDxx3RZe5as8sISMnd_r7sy7nuOsn47rlJGpI-jGFA3nz-lxngM75-Q_2PCChKsLx9sGalZO2KaBz2nuHLMetp-JMB-uk6_gabGrDwpOyQ52YcrnopcpAalhPQzD8l5ffLqVeTV-KBz2W_206mK9cdgopcKE8B7ml7kBOmAOq5QMzMol7UczvEmSknIsnYoHF4niMVp56Oc5nvor3Yf4QAAlv8W7nn7Uzsylce_PIQBtnDVdaKhccWsq9DEGg1LuU-w','sso-web','1520819042544::003::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiYXRpIjoiMDNiZWM0ODktZGM5NS00NmYxLWJlOGItMThhMWE4ODQ4N2ZkIiwiZXhwIjoxNTQ2NzA3NjgxLCJqdGkiOiIxNjliNjA4My0wYmJjLTRhMjEtYjJmMS03YWMzZTQ1ZWU0MWMiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.DVbCF6mi4wt-0CsRXvonC8xd6OATPL5phzMnbV9NhaxDsiMUOQj7FWLyiaMj_eMhDJ0I0wePlYIMyGGicyTNq5tFAFUu6A4yOZpYc7D6zKqU-n2b3KXqF6Axe3zE21d54QsWAR0FURsqDj1c-SCpStHlpmEXlQQDBANceiMpVmZiiCwMLh2UP4psUZV2Prw2iln_wzv1_Eo24IdqP8pvNwGwdR9eEnr3QWiyay2ZsVc9ws3zdeI1ScAEyCR5tuqsNZWEdXzFnEyWxkrrXabaM1ZTGFxZ5Y1pnhmoWgGhtHni2Hn5eG_-zmOUNJ_8HnLwPTilxQ6QBYzdftpK_1_o5g','2019-01-05 00:00:00','2020-01-05 00:00:00',X'ACED0005737200436F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F4175746832416363657373546F6B656E0CB29E361B24FACE0200064C00156164646974696F6E616C496E666F726D6174696F6E74000F4C6A6176612F7574696C2F4D61703B4C000A65787069726174696F6E7400104C6A6176612F7574696C2F446174653B4C000C72656672657368546F6B656E74003F4C6F72672F737072696E676672616D65776F726B2F73656375726974792F6F61757468322F636F6D6D6F6E2F4F417574683252656672657368546F6B656E3B4C000573636F706574000F4C6A6176612F7574696C2F5365743B4C0009746F6B656E547970657400124C6A6176612F6C616E672F537472696E673B4C000576616C756571007E00057870737200176A6176612E7574696C2E4C696E6B6564486173684D617034C04E5C106CC0FB0200015A000B6163636573734F72646572787200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000874000A6163636F756E745F696474000D3135323038313930343235343474000966756C6C5F6E616D6574000B53757065722041646D696E74000F6F7267616E697A6174696F6E5F696474000D3331313032303134303030303074000D706F736974696F6E5F6E616D65740014446972656B747572204F7065726173696F6E616C7400116F7267616E697A6174696F6E5F6E616D6574001750542E48494A5220474C4F42414C20534F4C5554494F4E74000B706F736974696F6E5F696474000330303374000E6163636F756E745F61766174617274003A68747470733A2F2F636C6F75642E68696A722E636F2E69642F73736F2F696D616765732F757365722E706E673F313532303831393034323534347400036A746974002430336265633438392D646339352D343666312D626538622D31386131613838343837666478007372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000016F74DDA945787372004C6F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744578706972696E674F417574683252656672657368546F6B656E2FDF47639DD0C9B70200014C000A65787069726174696F6E71007E0002787200446F72672E737072696E676672616D65776F726B2E73656375726974792E6F61757468322E636F6D6D6F6E2E44656661756C744F417574683252656672657368546F6B656E73E10E0A6354D45E0200014C000576616C756571007E000578707404D865794A68624763694F694A53557A49314E694973496E523563434936496B705856434A392E65794A316332567958323568625755694F6949784E5449774F4445354D4451794E5451304F6A6F774D444D364F6A4D784D5441794D4445304D4441774D4441694C434A7762334E7064476C76626C39755957316C496A6F6952476C795A577430645849675433426C636D467A61573975595777694C434A76636D6468626D6C365958527062323566626D46745A534936496C42554C6B684A536C496752307850516B464D49464E50544656555355394F496977695958563061473979615852705A584D694F6C7369556B394D525639515331425558315654525649694C434A53543078465830464D5346426656564E4655694973496C4A5054455666515642535830464554556C4F49697769556B394D52563954515652545245356656564E4655694973496C4A50544556665546524D58315654525649694C434A535430784658304651556C3950554556535156525055694973496C4A50544556665545745358315654525649694C434A535430784658314E56554556535830464554556C4F49697769556B394D5256394255464A6656564E4655694A644C434A6A62476C6C626E5266615751694F694A7A63323874643256694969776959574E6A62335675644639705A434936496A45314D6A41344D546B774E4449314E4451694C434A6D6457787358323568625755694F694A546458426C636942425A47317062694973496E4E6A6233426C496A7062496D6870616E4A66593239795A534A644C434A76636D6468626D6C365958527062323566615751694F69497A4D5445774D6A41784E4441774D4441774969776959585270496A6F694D444E695A574D304F446B745A474D354E5330304E6D59784C574A6C4F4749744D5468684D5745344F4451344E325A6B496977695A586877496A6F784E5451324E7A41334E6A67784C434A7164476B694F6949784E6A6C694E6A41344D793077596D4A6A4C5452684D6A4574596A4A6D4D53303359574D7A5A5451315A5755304D574D694C434A7762334E7064476C76626C39705A434936496A41774D794973496D466A59323931626E526659585A6864474679496A6F696148523063484D364C79396A624739315A43356F615770794C6D4E764C6D6C6B4C334E7A627939706257466E5A584D7664584E6C63693577626D635F4D5455794D4467784F5441304D6A55304E434A392E4456624346366D693477742D3043735258766F6E43387864364F4154504C3570687A4D6E6256394E6861784473694D554F516A3746574C7969614D6A5F654D68444A3049307765506C59494D794747696379544E7135744641465575364134794F5A7059633744367A4B71552D6E3262334B58714636417865337A4532316435345173574152304655527371446A31632D5343705374486C706D45586C51514442414E6365694D70566D5A696943774D4C68325550347073555A563250727732696C6E5F777A76315F456F3234496471503870764E77477764523965456E7233515769796179325A735663397773337A646549315363414579435235747571734E5A574564587A466E457957786B7272586162614D315A544746785A355931706E686D6F5767476874486E6932486E3565475F2D7A6D4F554E4A5F38486E4C775054696C7851365142597A646674704B5F315F6F35677371007E001A7708000001681EF640C278737200256A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65536574801D92D18F9B80550200007872002C6A6176612E7574696C2E436F6C6C656374696F6E7324556E6D6F6469666961626C65436F6C6C656374696F6E19420080CB5EF71E0200014C0001637400164C6A6176612F7574696C2F436F6C6C656374696F6E3B7870737200176A6176612E7574696C2E4C696E6B656448617368536574D86CD75A95DD2A1E020000787200116A6176612E7574696C2E48617368536574BA44859596B8B7340300007870770C000000103F4000000000000174000968696A725F636F72657874000662656172657274049C65794A68624763694F694A53557A49314E694973496E523563434936496B705856434A392E65794A316332567958323568625755694F6949784E5449774F4445354D4451794E5451304F6A6F774D444D364F6A4D784D5441794D4445304D4441774D4441694C434A7762334E7064476C76626C39755957316C496A6F6952476C795A577430645849675433426C636D467A61573975595777694C434A76636D6468626D6C365958527062323566626D46745A534936496C42554C6B684A536C496752307850516B464D49464E50544656555355394F496977695958563061473979615852705A584D694F6C7369556B394D525639515331425558315654525649694C434A53543078465830464D5346426656564E4655694973496C4A5054455666515642535830464554556C4F49697769556B394D52563954515652545245356656564E4655694973496C4A50544556665546524D58315654525649694C434A535430784658304651556C3950554556535156525055694973496C4A50544556665545745358315654525649694C434A535430784658314E56554556535830464554556C4F49697769556B394D5256394255464A6656564E4655694A644C434A6A62476C6C626E5266615751694F694A7A63323874643256694969776959574E6A62335675644639705A434936496A45314D6A41344D546B774E4449314E4451694C434A6D6457787358323568625755694F694A546458426C636942425A47317062694973496E4E6A6233426C496A7062496D6870616E4A66593239795A534A644C434A76636D6468626D6C365958527062323566615751694F69497A4D5445774D6A41784E4441774D444177496977695A586877496A6F784E5463344D6A457A4E6A67784C434A7164476B694F6949774D324A6C597A51344F53316B597A6B314C5451325A6A4574596D5534596930784F474578595467344E4467335A6D51694C434A7762334E7064476C76626C39705A434936496A41774D794973496D466A59323931626E526659585A6864474679496A6F696148523063484D364C79396A624739315A43356F615770794C6D4E764C6D6C6B4C334E7A627939706257466E5A584D7664584E6C63693577626D635F4D5455794D4467784F5441304D6A55304E434A392E576F455031385844675757346E55504241396F764945714A444B324855756E4B63497A757A63397556437465657863546562767975547A4D5A326861646D564F377A586334474A61624F524A673075517661585144787833525A65356173387349534D6E645F72377379376E754F736E3437726C4A4770492D6A474641336E7A2D6C786E674D37352D515F32504343684B734C78397347616C5A4F324B61427A326E75484C4D6574702D4A4D422D756B365F67616247724477704F795135325963726E6F70637041616C6850517A44386C3566664C71566554562D4B427A32575F3230366D4B396364676F70634B453842376D6C376B424F6D414F7135514D7A4D6F6C3755637A76456D536B6E49736E596F4846346E694D567035364F63356E766F72335966345141416C763857376E6E37557A73796C63655F50495142746E445664614B6863635773713944454767314C75552D77','1520819042544','003','3110201400000','LOGOUT');

/*!40000 ALTER TABLE `hijr_login_archive` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_organization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_organization`;

CREATE TABLE `hijr_organization` (
  `id_organization` varchar(50) NOT NULL DEFAULT '',
  `name_organization` varchar(100) DEFAULT NULL,
  `position_id_organization` varchar(50) DEFAULT NULL,
  `parent_id_organization` varchar(50) DEFAULT NULL,
  `admin_role_organization` varchar(50) DEFAULT NULL,
  `sequence_organization` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_organization`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_organization` WRITE;
/*!40000 ALTER TABLE `hijr_organization` DISABLE KEYS */;

INSERT INTO `hijr_organization` (`id_organization`, `name_organization`, `position_id_organization`, `parent_id_organization`, `admin_role_organization`, `sequence_organization`)
VALUES
	('1542358938675','Bagian Keuangan','002','3110201400000',NULL,999),
	('1542358951101','Bagian Operasional','003','3110201400000',NULL,999),
	('3110201400000','PT.HIJR GLOBAL SOLUTION','001','3110201400000','ROLE_HGS_ADMIN',1),
	('3110201400001','Kementerian Lingkungan Hidup dan Kehutanan','003','3110201400001','ROLE_LHK_ADMIN',1);

/*!40000 ALTER TABLE `hijr_organization` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_position`;

CREATE TABLE `hijr_position` (
  `id_position` varchar(50) NOT NULL DEFAULT '',
  `name_position` varchar(20) NOT NULL DEFAULT '',
  `description_position` varchar(200) DEFAULT NULL,
  `group_id_position` varchar(50) NOT NULL DEFAULT '',
  `organization_id_position` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `hijr_position` WRITE;
/*!40000 ALTER TABLE `hijr_position` DISABLE KEYS */;

INSERT INTO `hijr_position` (`id_position`, `name_position`, `description_position`, `group_id_position`, `organization_id_position`)
VALUES
	('001','Direktur Utama','Mengelola usaha secara keseluruhan','001','3110201400000'),
	('002','Direktur Keuangan','Mengelola kebijakan keuangan perusahaan','001','3110201400000'),
	('003','Direktur Operasional',NULL,'001','3110201400000'),
	('010','Project Manager','test asd','002','3110201400000'),
	('011','Product Manager',NULL,'002','3110201400000'),
	('012','System Analyst',NULL,'003','3110201400000'),
	('013','Product Analyst',NULL,'003','3110201400000'),
	('014','Data Analyst',NULL,'003','3110201400000'),
	('015','System Designer',NULL,'004','3110201400000'),
	('016','Graphic Designer',NULL,'004','3110201400000'),
	('017','Web Designer',NULL,'004','3110201400000'),
	('018','Web Developer',NULL,'005','3110201400000'),
	('019','Mobile Developer',NULL,'005','3110201400000'),
	('020','Java Programmer',NULL,'006','3110201400000'),
	('021','React Programmer',NULL,'006','3110201400000'),
	('022','Web Programmer',NULL,'006','3110201400000'),
	('023','PHP Programmer',NULL,'006','3110201400000'),
	('102','Magang',NULL,'101','3110201400001'),
	('101','Honorer',NULL,'101','3110201400001'),
	('103','Konsultan',NULL,'101','3110201400001'),
	('1542267548078','asik','yes','004','3110201400000'),
	('104','Pegawai',NULL,'101','3110201400001');

/*!40000 ALTER TABLE `hijr_position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_position_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_position_group`;

CREATE TABLE `hijr_position_group` (
  `id_position_group` varchar(50) NOT NULL DEFAULT '',
  `name_position_group` varchar(200) NOT NULL DEFAULT '',
  `organization_id_position_group` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_position_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `hijr_position_group` WRITE;
/*!40000 ALTER TABLE `hijr_position_group` DISABLE KEYS */;

INSERT INTO `hijr_position_group` (`id_position_group`, `name_position_group`, `organization_id_position_group`)
VALUES
	('001','Direksi','3110201400000'),
	('002','Manager','3110201400000'),
	('003','Analyst','3110201400000'),
	('004','Designer','3110201400000'),
	('005','Developer','3110201400000'),
	('006','Programmer','3110201400000'),
	('101','Non PNS','3110201400001'),
	('1542269160237','yes','3110201400001');

/*!40000 ALTER TABLE `hijr_position_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hijr_province
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hijr_province`;

CREATE TABLE `hijr_province` (
  `id_province` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name_province` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `latitude_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude_province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_province`),
  UNIQUE KEY `provinsi_id` (`id_province`) USING BTREE,
  KEY `provinsi_nama` (`name_province`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `hijr_province` WRITE;
/*!40000 ALTER TABLE `hijr_province` DISABLE KEYS */;

INSERT INTO `hijr_province` (`id_province`, `name_province`, `latitude_province`, `longitude_province`)
VALUES
	('11','Aceh','4.695135','96.7493993'),
	('12','Sumatera Utara','2.0108563','98.9784887'),
	('13','Sumatera Barat','-0.7399397','100.8000051'),
	('14','Riau','0.2933469','101.7068294'),
	('15','Jambi','-1.6101229','103.6131203'),
	('16','Sumatera Selatan','-3.3194374','103.914399'),
	('17','Bengkulu','-3.7928451','102.2607641'),
	('18','Lampung','-4.5585849','105.4068079'),
	('19','Kepulauan Bangka Belitung','-2.7410513','106.4405872'),
	('21','Kepulauan Riau','3.9456514','108.1428669'),
	('31','Dki Jakarta','-6.1744651','106.822745'),
	('32','Jawa Barat','-7.090911','107.668887'),
	('33','Jawa Tengah','-7.150975','110.1402594'),
	('34','Di Yogyakarta','-7.7975915','110.3707141'),
	('35','Jawa Timur','-7.5360639','112.2384017'),
	('36','Banten','-6.4058172','106.0640179'),
	('51','Bali','-8.4095178','115.188916'),
	('52','Nusa Tenggara Barat','-8.6529334','117.3616476'),
	('53','Nusa Tenggara Timur','-8.6573819','121.0793705'),
	('61','Kalimantan Barat','-0.2787808','111.4752851'),
	('62','Kalimantan Tengah','-1.6814878','113.3823545'),
	('63','Kalimantan Selatan','-3.0926415','115.2837585'),
	('64','Kalimantan Timur','0.5386586','116.419389'),
	('65','Kalimantan Utara','3.0730929','116.0413889'),
	('71','Sulawesi Utara','0.6246932','123.9750018'),
	('72','Sulawesi Tengah','-1.4300254','121.4456179'),
	('73','Sulawesi Selatan','-3.6687994','119.9740534'),
	('74','Sulawesi Tenggara','-3.5562244','121.8020017'),
	('75','Gorontalo','0.5435442','123.0567693'),
	('76','Sulawesi Barat','-2.8441371','119.2320784'),
	('81','Maluku','-3.2384616','130.1452734'),
	('82','Maluku Utara','1.5709993','127.8087693'),
	('91','Papua Barat','-1.3361154','133.1747162'),
	('94','Papua','-4.269928','138.0803529');

/*!40000 ALTER TABLE `hijr_province` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_account_login
# ------------------------------------------------------------

DROP VIEW IF EXISTS `oauth_account_login`;

CREATE TABLE `oauth_account_login` (
   `id_account` VARCHAR(50) NOT NULL DEFAULT '',
   `username_account` VARCHAR(30) NULL DEFAULT NULL,
   `password_account` VARCHAR(500) NULL DEFAULT NULL,
   `email_account` VARCHAR(100) NULL DEFAULT NULL,
   `first_name_account` VARCHAR(100) NOT NULL DEFAULT '',
   `last_name_account` VARCHAR(100) NOT NULL DEFAULT '',
   `full_name_account` VARCHAR(201) NOT NULL DEFAULT '',
   `mobile_account` VARCHAR(30) NULL DEFAULT NULL,
   `enabled_account` TINYINT(1) NOT NULL,
   `account_non_expired_account` TINYINT(1) NOT NULL,
   `credentials_non_expired_account` TINYINT(1) NOT NULL,
   `account_non_locked_account` TINYINT(1) NOT NULL,
   `position_id_account_login` VARCHAR(50) NOT NULL DEFAULT '',
   `position_name_account_login` VARCHAR(20) NOT NULL DEFAULT '',
   `organization_id_account_login` VARCHAR(50) NOT NULL DEFAULT '',
   `organization_name_account_login` VARCHAR(100) NULL DEFAULT NULL,
   `status_account_login` INT(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM;



# Dump of table oauth_client_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_client_details`;

CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `oauth_client_details` WRITE;
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;

INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`)
VALUES
	('sistem-web','hijr_core','c5fa05675126f5a343a69a1657ff0e15','read,write','authorization_code,password,refresh_token','','ROLE_USER',31536000,30000,'{}','read,write'),
	('sso-web','hijr_core','c5fa05675126f5a343a69a1657ff0e15','read,write','authorization_code,password,refresh_token','','ROLE_USER',31536000,30000,'{}','read,write');

/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_code`;

CREATE TABLE `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table persistent_logins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(50) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_service_request
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_service_request`;

CREATE TABLE `tbl_service_request` (
  `id_service_request` varchar(50) NOT NULL DEFAULT '',
  `module_service_request` varchar(20) NOT NULL DEFAULT '',
  `created_time_service_request` datetime NOT NULL,
  `status_service_request` varchar(20) DEFAULT '',
  `callback_time_service_request` datetime DEFAULT NULL,
  `reference_id_service_request` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_service_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





# Replace placeholder table for oauth_account_login with correct view syntax
# ------------------------------------------------------------

DROP TABLE `oauth_account_login`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `oauth_account_login`
AS SELECT
   `a`.`id_account` AS `id_account`,
   `a`.`username_account` AS `username_account`,
   `a`.`password_account` AS `password_account`,
   `a`.`email_account` AS `email_account`,
   `a`.`first_name_account` AS `first_name_account`,
   `a`.`last_name_account` AS `last_name_account`,concat(concat(`a`.`first_name_account`,' '),`a`.`last_name_account`) AS `full_name_account`,
   `a`.`mobile_account` AS `mobile_account`,
   `a`.`enabled_account` AS `enabled_account`,
   `a`.`account_non_expired_account` AS `account_non_expired_account`,
   `a`.`credentials_non_expired_account` AS `credentials_non_expired_account`,
   `a`.`account_non_locked_account` AS `account_non_locked_account`,
   `c`.`id_position` AS `position_id_account_login`,
   `c`.`name_position` AS `position_name_account_login`,
   `d`.`id_organization` AS `organization_id_account_login`,
   `d`.`name_organization` AS `organization_name_account_login`,1 AS `status_account_login`
FROM (((`hijr_account` `a` join `hijr_assignment` `b` on((`a`.`id_account` = `b`.`account_id_assignment`))) join `hijr_position` `c` on((`c`.`id_position` = `b`.`position_id_assignment`))) join `hijr_organization` `d` on((`d`.`id_organization` = `b`.`parent_id_assignment`))) where ((`b`.`active_assignment` = 1) and (`b`.`status_assignment` = 'PERMANENT'));

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
