# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 10.1.37-MariaDB)
# Database: ticketdb
# Generation Time: 2019-06-27 07:55:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_purchase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_purchase`;

CREATE TABLE `tbl_purchase` (
  `id_purchase` varchar(50) NOT NULL DEFAULT '',
  `region_id_purchase` varchar(50) DEFAULT NULL,
  `start_date_purchase` datetime DEFAULT NULL,
  `end_date_purchase` datetime DEFAULT NULL,
  `datetime_purchase` datetime NOT NULL,
  `account_id_purchase` varchar(50) NOT NULL DEFAULT '',
  `count_ticket_purchase` int(11) NOT NULL,
  `amount_ticket_purchase` decimal(15,2) DEFAULT '0.00',
  `status_purchase` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_purchase`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_purchase` WRITE;
/*!40000 ALTER TABLE `tbl_purchase` DISABLE KEYS */;

INSERT INTO `tbl_purchase` (`id_purchase`, `region_id_purchase`, `start_date_purchase`, `end_date_purchase`, `datetime_purchase`, `account_id_purchase`, `count_ticket_purchase`, `amount_ticket_purchase`, `status_purchase`)
VALUES
	('1556439317096709','1553051748240951','2019-04-28 00:00:00','2019-04-30 00:00:00','2019-04-30 17:51:22','1520819042544',1,3339000.00,'WAITING'),
	('1556557945675249','1553066489065932','2019-05-07 00:00:00','2019-05-08 00:00:00','2019-05-07 21:01:02','1520819042544',1,2485000.00,'WAITING'),
	('1556580203939228','1553051748240951','2019-04-22 00:00:00','2019-04-29 00:00:00','2019-04-30 06:24:22','1520819042544',1,617000.00,'PAYMENT'),
	('1557239616240836','1553075693221742','2019-05-07 00:00:00','2019-05-08 00:00:00','2019-05-07 22:00:13','1520819042544',2,3235000.00,'WAITING'),
	('1557302585602322','1553051748240951','2019-05-10 00:00:00','2019-05-11 00:00:00','2019-05-08 15:03:05','1520819042544',1,NULL,'CANCEL'),
	('1557302881925253','1553075693221742','2019-05-08 00:00:00','2019-05-11 00:00:00','2019-05-08 15:12:30','1520819042544',1,1267000.00,'PAYMENT'),
	('1557760041172825','1553075693221742','2019-05-13 00:00:00','2019-05-15 00:00:00','2019-05-13 22:07:21','1520819042544',2,NULL,'DRAFT');

/*!40000 ALTER TABLE `tbl_purchase` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_purchase_copy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_purchase_copy`;

CREATE TABLE `tbl_purchase_copy` (
  `id_purchase` varchar(50) NOT NULL DEFAULT '',
  `datetime_purchase` datetime NOT NULL,
  `account_id_purchase` varchar(50) NOT NULL DEFAULT '',
  `count_ticket_purchase` int(11) NOT NULL,
  `amount_ticket_purchase` decimal(15,2) NOT NULL,
  `status_purchase` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_purchase`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_purchase_detil_tour_guide
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_purchase_detil_tour_guide`;

CREATE TABLE `tbl_purchase_detil_tour_guide` (
  `id_purchase_detil_tour_guide` varchar(50) NOT NULL DEFAULT '',
  `header_id_purchase_detil_tour_guide` varchar(50) DEFAULT NULL,
  `full_name_purchase_detil_tour_guide` varchar(50) NOT NULL DEFAULT '',
  `gender_purchase_detil_tour_guide` varchar(50) DEFAULT NULL,
  `photo_purchase_detil_tour_guide` varchar(100) DEFAULT NULL,
  `description_purchase_detil_tour_guide` text,
  `phone_number_purchase_detil_tour_guide` varchar(50) DEFAULT NULL,
  `email_purchase_detil_tour_guide` varchar(50) DEFAULT NULL,
  `cost_per_day_purchase_detil_tour_guide` decimal(15,2) DEFAULT NULL,
  `organization_purchase_detil_tour_guide` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_purchase_detil_tour_guide`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_purchase_detil_tour_guide` WRITE;
/*!40000 ALTER TABLE `tbl_purchase_detil_tour_guide` DISABLE KEYS */;

INSERT INTO `tbl_purchase_detil_tour_guide` (`id_purchase_detil_tour_guide`, `header_id_purchase_detil_tour_guide`, `full_name_purchase_detil_tour_guide`, `gender_purchase_detil_tour_guide`, `photo_purchase_detil_tour_guide`, `description_purchase_detil_tour_guide`, `phone_number_purchase_detil_tour_guide`, `email_purchase_detil_tour_guide`, `cost_per_day_purchase_detil_tour_guide`, `organization_purchase_detil_tour_guide`)
VALUES
	('1556580262384818','1556580203939228','Yulie Rahmawati','PEREMPUAN','1556532372054235','Enjoyed with my tour','0852159588','yulie_rahmawati@email.com',250000.00,'3110201400000'),
	('1556621482462201','1556439317096709','Kit Harington','LAKI_LAKI',NULL,'What u want see,its my responsible to show u','082119260548','kit_harington@email.com',120000.00,'3110201400000'),
	('1556621482464394','1556439317096709','Yulie Rahmawati','PEREMPUAN','1556532372054235','Enjoyed with my tour','0852159588','yulie_rahmawati@email.com',250000.00,'3110201400000'),
	('1556621482470262','1556439317096709','Isaac Hempstead','LAKI_LAKI',NULL,'I will show you, what u want to see :D','082110806729','isaac@email.com',250000.00,'3110201400000'),
	('1557237662871750','1556557945675249','Yulie Rahmawati','PEREMPUAN','1556532372054235','Enjoyed with my tour','0852159588','yulie_rahmawati@email.com',250000.00,'3110201400000'),
	('1557237662879669','1556557945675249','maher','LAKI_LAKI','1556532196181531','','847121241241','maher@email.com',300000.00,'3110201400000'),
	('1557241213622363','1557239616240836','Yulie Rahmawati','PEREMPUAN','1556532372054235','Enjoyed with my tour','0852159588','yulie_rahmawati@email.com',250000.00,'3110201400000'),
	('1557241213624509','1557239616240836','Isaac Hempstead','LAKI_LAKI',NULL,'I will show you, what u want to see :D','082110806729','isaac@email.com',250000.00,'3110201400000'),
	('1557241213625596','1557239616240836','Peter Dinklage','LAKI_LAKI','1556558303706743','Simple For u','0849345453535','peter@emal.com',200000.00,'3110201400000'),
	('1557303150035222','1557302881925253','maher','LAKI_LAKI','1556532196181531','','847121241241','maher@email.com',300000.00,'3110201400000');

/*!40000 ALTER TABLE `tbl_purchase_detil_tour_guide` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_purchase_detil_vehicle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_purchase_detil_vehicle`;

CREATE TABLE `tbl_purchase_detil_vehicle` (
  `id_purchase_detil_vehicle` varchar(50) NOT NULL DEFAULT '',
  `header_id_purchase_detil_vehicle` varchar(50) DEFAULT NULL,
  `name_purchase_detil_vehicle` varchar(50) NOT NULL DEFAULT '',
  `type_purchase_detil_vehicle` varchar(50) DEFAULT NULL,
  `price_purchase_detil_vehicle` int(11) DEFAULT NULL,
  `qty_purchase_detil_vehicle` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_purchase_detil_vehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_purchase_detil_vehicle` WRITE;
/*!40000 ALTER TABLE `tbl_purchase_detil_vehicle` DISABLE KEYS */;

INSERT INTO `tbl_purchase_detil_vehicle` (`id_purchase_detil_vehicle`, `header_id_purchase_detil_vehicle`, `name_purchase_detil_vehicle`, `type_purchase_detil_vehicle`, `price_purchase_detil_vehicle`, `qty_purchase_detil_vehicle`)
VALUES
	('1556580262382310','1556580203939228','Daihatsu Xenia','1',367000,1),
	('1556621482458350','1556439317096709','Daihatsu Xenia','1',367000,1),
	('1556621482459978','1556439317096709','Alphard Transformer','3',784000,3),
	('1557237662859123','1556557945675249','Daihatsu Xenia','1',367000,1),
	('1557237662861173','1556557945675249','Alphard Transformer','3',784000,2),
	('1557241213617491','1557239616240836','Daihatsu Xenia','1',367000,1),
	('1557241213620879','1557239616240836','Alphard Transformer','3',784000,2),
	('1557241213621524','1557239616240836','Innova Super White','5',300000,2),
	('1557303150029620','1557302881925253','Daihatsu Xenia','1',367000,1),
	('1557303150033959','1557302881925253','Innova Super White','5',300000,2);

/*!40000 ALTER TABLE `tbl_purchase_detil_vehicle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_purchase_detil_visitor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_purchase_detil_visitor`;

CREATE TABLE `tbl_purchase_detil_visitor` (
  `id_purchase_detil_visitor` varchar(50) NOT NULL DEFAULT '',
  `header_id_purchase_detil_visitor` varchar(50) DEFAULT NULL,
  `no_identity_purchase_detil_visitor` varchar(50) DEFAULT '',
  `type_identity_purchase_detil_visitor` varchar(50) DEFAULT '',
  `full_name_purchase_detil_visitor` varchar(50) NOT NULL DEFAULT '',
  `phone_number_purchase_detil_visitor` varchar(50) DEFAULT '',
  `email_purchase_detil_visitor` varchar(50) DEFAULT '',
  `responsible_purchase_detil_visitor` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_purchase_detil_visitor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_purchase_detil_visitor` WRITE;
/*!40000 ALTER TABLE `tbl_purchase_detil_visitor` DISABLE KEYS */;

INSERT INTO `tbl_purchase_detil_visitor` (`id_purchase_detil_visitor`, `header_id_purchase_detil_visitor`, `no_identity_purchase_detil_visitor`, `type_identity_purchase_detil_visitor`, `full_name_purchase_detil_visitor`, `phone_number_purchase_detil_visitor`, `email_purchase_detil_visitor`, `responsible_purchase_detil_visitor`)
VALUES
	('1556580262377153','1556580203939228','222999129','','RSCH','+6282119260548','kayumiman@yahoo.co.id',NULL),
	('1556621482442823','1556439317096709','9494194914','KTP','Kayum Munajir','+6282119260548','kayumiman@yahoo.co.id',NULL),
	('1557237662842905','1556557945675249','11111','KTP','satu','+6282110806729','satu@email.com',1),
	('1557237662849497','1556557945675249','93495495949','KTP','Kayum','+6282119260548','kayumiman@yahoo.co.id',0),
	('1557241213611500','1557239616240836','1111111','PASSPORT','Satu','+621111111111','satu@email.com',1),
	('1557241213615356','1557239616240836','819782179817','KTP','Kayum','+6282119260548','kayumiman@yahoo.co.id',0),
	('1557241213616983','1557239616240836','974972749174','KTP','munajir','+62821497919712947','munajir@email.com',0),
	('1557303150018828','1557302881925253','917497417417294','KTP','Rahmat','+6282110806729','rahmat@email.com',1),
	('1557303150024351','1557302881925253','91297412412741','KTP','Maman','+6282119260548','maman@email.com',0);

/*!40000 ALTER TABLE `tbl_purchase_detil_visitor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_region`;

CREATE TABLE `tbl_region` (
  `id_region` varchar(50) NOT NULL DEFAULT '',
  `name_region` varchar(50) DEFAULT NULL,
  `type_region` varchar(50) DEFAULT NULL,
  `description_region` text,
  `location_region` varchar(50) DEFAULT NULL,
  `location_desc_region` varchar(50) DEFAULT NULL,
  `organization_id_region` varchar(50) DEFAULT NULL,
  `refund_region` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_region` WRITE;
/*!40000 ALTER TABLE `tbl_region` DISABLE KEYS */;

INSERT INTO `tbl_region` (`id_region`, `name_region`, `type_region`, `description_region`, `location_region`, `location_desc_region`, `organization_id_region`, `refund_region`)
VALUES
	('1553051748240951','Wisata Gunung Bromo','LAUT',' Gunung Bromo adalah Wisata alam terbaik di Indonesia yang menjadi surga bagi pecinta wisata pegunungan dan para fotografer.  Selain itu juga dapat menikmati keindahan alam Bromo dan sajian sunrise-nya dari atas puncak, wisatawan juga bisa mengunjungi spot lain yang tidak kalah menarik, seperti Bukit Teletubbies dan Lautan Pasir Berbisik. \r\n#phan','2512512134','Jawa Timur','3110201400000',1),
	('1553066489065932','Raja Ampat','LAUT','Raja Ampat Lautnya yg nampak eksotis nan indah.','5255252','Ambon','3110201400000',1),
	('1553075693221742','Taman Nasional Komodo','DARAT','Kawasan yg dihiasi dengan satwa-satwa langkah.','353534346','Jawa','3110201400000',0),
	('1553076020578140','Taman Nasional Bali Barat','DARAT','Pemandangan Alam, gunung, laut dan air terjun yg eksotis','','Bali','3110201400000',NULL),
	('1553165877431282','The Famous Sunrise','DARAT','Mount Bromo is part of BromoTenggerSemeru National Park. Famous for exotic caldera or sea of sand creaters, as well as beautiful views of the sunrise (Bromo Sunrise Tour)','17478768732','Jawa Timur','3110201400000',NULL);

/*!40000 ALTER TABLE `tbl_region` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_region_facility
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_region_facility`;

CREATE TABLE `tbl_region_facility` (
  `id_region_facility` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_facility` varchar(50) DEFAULT NULL,
  `item_region_facility` varchar(50) DEFAULT NULL,
  `price_region_facility` decimal(15,2) DEFAULT '0.00',
  `include_region_facility` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_region_facility`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_region_facility` WRITE;
/*!40000 ALTER TABLE `tbl_region_facility` DISABLE KEYS */;

INSERT INTO `tbl_region_facility` (`id_region_facility`, `header_id_region_facility`, `item_region_facility`, `price_region_facility`, `include_region_facility`)
VALUES
	('1553061259686297','1553051748240951','Transportation',500000.00,'Y'),
	('1553061274710640','1553051748240951','Acomdation 2 night hotel with breakfast',1500000.00,'Y'),
	('1553061287991753','1553051748240951','Tour guide service',200000.00,'Y'),
	('1553061312420108','1553051748240951','2 unit jeep (1-5) 2 point location',2000000.00,'Y'),
	('1553061323040434','1553051748240951','parking & toll',50000.00,'Y'),
	('1553061360210115','1553051748240951','ride horse in Bromo',50000.00,'N'),
	('1553076525550133','1553076020578140','Transportation',NULL,'Y'),
	('1553077372829630','1553076020578140','Acomodation 2 night hotel with breakfast',NULL,'Y'),
	('1553077494918171','1553076020578140','tour guide service',NULL,'Y'),
	('1553077613380524','1553076020578140','parking & toll',NULL,'Y'),
	('1553077703321205','1553076020578140','2 unit jeep',NULL,'Y'),
	('1553077794070115','1553076020578140','enterance free',100000.00,'N'),
	('1553166232599768','1553165877431282','Transportation',NULL,'Y'),
	('1553166251750969','1553165877431282','Acomodation 2 night hotel with breakfast',NULL,'Y'),
	('1553166262444238','1553165877431282','tour guide service',NULL,'Y'),
	('1553166285534556','1553165877431282','Entracee fee',NULL,'Y'),
	('1553166313795179','1553165877431282','2 Unit Jeep(1-5) 2 point location',NULL,'Y'),
	('1553166327744862','1553165877431282','daily mineral water 600ml',NULL,'Y'),
	('1553166340861358','1553165877431282','parking & toll',NULL,'Y'),
	('1553166352956216','1553165877431282','tip crew',NULL,'N'),
	('1553166376552456','1553165877431282','airline tickets PP',NULL,'N'),
	('1553166394586442','1553165877431282','airport tax, airport handling',NULL,'N'),
	('1553166410060141','1553165877431282','ride horses in Bromo',NULL,'N'),
	('1556070307974934','1553066489065932','Tour Guide',200000.00,'Y'),
	('1556628404572607','1553075693221742','Tour Guide',200000.00,'Y'),
	('1556628440909450','1553075693221742','Transportation',450000.00,'Y');

/*!40000 ALTER TABLE `tbl_region_facility` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_region_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_region_photo`;

CREATE TABLE `tbl_region_photo` (
  `id_region_photo` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_photo` varchar(50) DEFAULT NULL,
  `file_id_region_photo` varchar(50) DEFAULT NULL,
  `active_region_photo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_region_photo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_region_photo` WRITE;
/*!40000 ALTER TABLE `tbl_region_photo` DISABLE KEYS */;

INSERT INTO `tbl_region_photo` (`id_region_photo`, `header_id_region_photo`, `file_id_region_photo`, `active_region_photo`)
VALUES
	('1556068117536208','1553051748240951','1556068117477970',0),
	('1556068257768964','1553051748240951','1556068257643559',0),
	('1556068980789839','1553066489065932','1556068980512376',0),
	('1556069009930917','1553066489065932','1556069009774368',0),
	('1556069717357950','1553066489065932','1556069717285535',0),
	('1556100307407161','1553075693221742','1556100306992468',0),
	('1556100508652591','1553076020578140','1556100508537443',1),
	('1556100522048435','1553165877431282','1556100521989192',1),
	('1556531031363721','1553066489065932','1556531030963216',1),
	('1556549582027284','1553066489065932','1556549581680246',0),
	('1556556694067159','1553075693221742','1556556693663841',1),
	('1556653148720288','1553051748240951','1556653147693142',1);

/*!40000 ALTER TABLE `tbl_region_photo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_region_todo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_region_todo`;

CREATE TABLE `tbl_region_todo` (
  `id_region_todo` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_todo` varchar(50) DEFAULT NULL,
  `day_region_todo` int(11) DEFAULT NULL,
  `time_region_todo` datetime DEFAULT NULL,
  `activity_region_todo` varchar(100) DEFAULT NULL,
  `parent_region_todo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_region_todo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_region_todo` WRITE;
/*!40000 ALTER TABLE `tbl_region_todo` DISABLE KEYS */;

INSERT INTO `tbl_region_todo` (`id_region_todo`, `header_id_region_todo`, `day_region_todo`, `time_region_todo`, `activity_region_todo`, `parent_region_todo`)
VALUES
	('1','1553051748240951',1,'1970-01-01 10:09:00','Break again','1553052091348251'),
	('1553052091348251','1553051748240951',1,'1970-01-01 09:09:00','Jelajahi Kota Malang',''),
	('1553057187055646','1553051748240951',2,'1970-01-01 00:00:00','GO',''),
	('1553057353610553','1553051748240951',2,'1970-01-01 07:00:00','Breakfast in resto local','1553057187055646'),
	('1553057369918290','1553051748240951',2,'1970-01-01 08:00:00','GO to Somewhere','1553057187055646'),
	('1553058420053147','1553051748240951',1,'1970-01-01 11:00:00','GO GO','1553052091348251'),
	('1553058440047581','1553051748240951',2,'1970-01-01 09:00:00','Skiping','1553057187055646'),
	('1553058449999983','1553051748240951',3,'1970-01-01 00:00:00','REHAT',''),
	('1553062980819945','1553051748240951',3,'1970-01-01 07:00:00','Dhuha','1553058449999983'),
	('1553096100376481','1553076020578140',1,'1970-01-01 00:00:00','Exploring A',''),
	('1553155551274310','1553076020578140',2,'1970-01-01 08:00:00','Exploring B',''),
	('1553155606739594','1553076020578140',3,'1970-01-01 00:00:00','END Exploring ',''),
	('1553161676081790','1553076020578140',5,'1970-01-01 09:07:00','LIMA LIMA','1553155909656325'),
	('1553162796260660','1553076020578140',1,'1970-01-01 06:00:00','Sarapan DI Resto Lokal','1553096100376481'),
	('1553164778779787','1553076020578140',1,'1970-01-01 07:50:00','Preapere to GO!!','1553096100376481'),
	('1553164995946210','1553076020578140',2,'1970-01-01 07:00:00','Breakfast In the Morning','1553155551274310'),
	('1553165247656237','1553076020578140',2,'1970-01-01 08:00:00','Sholat Dhuha','1553155551274310'),
	('1553165262755604','1553076020578140',3,'1970-01-01 07:00:00','Breakfast','1553155606739594'),
	('1553166465026575','1553165877431282',1,'1970-01-01 06:00:00','GO',''),
	('1553166481506140','1553165877431282',2,'1970-01-01 06:00:00','Explore Bromo',''),
	('1553166509728574','1553165877431282',3,'1970-01-01 17:00:00','End Program',''),
	('1553166525182269','1553165877431282',1,'1970-01-01 08:00:00','Arrival Surabaya','1553166465026575'),
	('1553166549443553','1553165877431282',1,'1970-01-01 09:00:00','Breakfast at local resto','1553166465026575'),
	('1553166574992165','1553165877431282',1,'1970-01-01 11:00:00','Transfer to bato visit jatim park 2','1553166465026575'),
	('1553166593254689','1553165877431282',1,'1970-01-01 13:00:00','Lunch & Ishoma','1553166465026575'),
	('1553166604889777','1553165877431282',1,'1970-01-01 14:00:00','Patikapel','1553166465026575'),
	('1553166619500136','1553165877431282',1,'1970-01-01 16:00:00','Check in Hotel','1553166465026575'),
	('1553166660736641','1553165877431282',2,'1970-01-01 03:00:00','Midhnight Bromo via cemoro lawang','1553166481506140'),
	('1553168739658719','1553165877431282',2,'1970-01-01 07:00:00','Breakfast at camera indah hotel bromo','1553166481506140'),
	('1553168806827826','1553165877431282',2,'1970-01-01 09:00:00','Back to malang','1553166481506140'),
	('1553168823501638','1553165877431282',2,'1970-01-01 12:00:00','Lunch & Ishoma','1553166481506140'),
	('1553168838786640','1553165877431282',2,'1970-01-01 13:00:00','Museum Angkut','1553166481506140'),
	('1553168858722264','1553165877431282',2,'1970-01-01 16:00:00','Alun-alun Batu','1553166481506140'),
	('1553169243729423','1553165877431282',2,'1970-01-01 18:00:00','BNS','1553166481506140'),
	('1553169260265631','1553165877431282',2,'1970-01-01 19:00:00','Dinner at local resto','1553166481506140'),
	('1553169272677651','1553165877431282',2,'1970-01-01 20:00:00','Check in hotel','1553166481506140'),
	('1553169301026966','1553165877431282',3,'1970-01-01 07:00:00','Breakfast & Checkout in hotel','1553166509728574'),
	('1553169390421718','1553165877431282',3,'1970-01-01 08:00:00','Shopping tour till time depature and than transfer to juanda airport Surabaya','1553166509728574'),
	('1553423550922882','1553066489065932',1,'1970-01-01 08:00:00','Breakfast','1553166465026575'),
	('2','1553051748240951',1,'1970-01-01 12:09:00','Ishoma','1553052091348251');

/*!40000 ALTER TABLE `tbl_region_todo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_region_todo_copy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_region_todo_copy`;

CREATE TABLE `tbl_region_todo_copy` (
  `id_region_todo` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_todo` varchar(50) DEFAULT NULL,
  `day_region_todo` int(11) DEFAULT NULL,
  `title_region_todo` varchar(50) DEFAULT NULL,
  `desc_region_todo` varchar(100) DEFAULT NULL,
  `time_region_todo` datetime DEFAULT NULL,
  `activity_region_todo` varchar(50) DEFAULT NULL,
  `parent_region_todo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_region_todo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_tour_guide
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_tour_guide`;

CREATE TABLE `tbl_tour_guide` (
  `id_tour_guide` varchar(50) NOT NULL DEFAULT '',
  `full_name_tour_guide` varchar(100) NOT NULL DEFAULT '',
  `gender_tour_guide` varchar(50) NOT NULL DEFAULT '',
  `photo_tour_guide` varchar(100) DEFAULT NULL,
  `description_tour_guide` text,
  `phone_number_tour_guide` varchar(50) DEFAULT NULL,
  `email_tour_guide` varchar(50) DEFAULT NULL,
  `cost_per_day_tour_guide` decimal(15,2) DEFAULT '0.00',
  `organization_id_tour_guide` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tour_guide`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_tour_guide` WRITE;
/*!40000 ALTER TABLE `tbl_tour_guide` DISABLE KEYS */;

INSERT INTO `tbl_tour_guide` (`id_tour_guide`, `full_name_tour_guide`, `gender_tour_guide`, `photo_tour_guide`, `description_tour_guide`, `phone_number_tour_guide`, `email_tour_guide`, `cost_per_day_tour_guide`, `organization_id_tour_guide`)
VALUES
	('1551853116915302','Kit Harington','LAKI_LAKI',NULL,'What u want see,its my responsible to show u','082119260548','kit_harington@email.com',120000.00,'3110201400000'),
	('1554028973074481','Yulie Rahmawati','PEREMPUAN','1556532372054235','Enjoyed with my tour','0852159588','yulie_rahmawati@email.com',250000.00,'3110201400000'),
	('1554198612659471','Isaac Hempstead','LAKI_LAKI',NULL,'I will show you, what u want to see :D','082110806729','isaac@email.com',250000.00,'3110201400000'),
	('1554732637306729','Peter Dinklage','LAKI_LAKI','1556558303706743','Simple For u','0849345453535','peter@emal.com',200000.00,'3110201400000'),
	('1556532196181355','maher','LAKI_LAKI','1556532196181531','','847121241241','maher@email.com',300000.00,'3110201400000');

/*!40000 ALTER TABLE `tbl_tour_guide` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_type_region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_type_region`;

CREATE TABLE `tbl_type_region` (
  `id_type_region` varchar(50) NOT NULL DEFAULT '',
  `name_type_region` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_type_region`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_type_region` WRITE;
/*!40000 ALTER TABLE `tbl_type_region` DISABLE KEYS */;

INSERT INTO `tbl_type_region` (`id_type_region`, `name_type_region`)
VALUES
	('1556559558850986','LAUT'),
	('1556559565383673','GUNUNG');

/*!40000 ALTER TABLE `tbl_type_region` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_type_vehicle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_type_vehicle`;

CREATE TABLE `tbl_type_vehicle` (
  `id_type_vehicle` varchar(50) NOT NULL DEFAULT '',
  `name_type_vehicle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_type_vehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_type_vehicle` WRITE;
/*!40000 ALTER TABLE `tbl_type_vehicle` DISABLE KEYS */;

INSERT INTO `tbl_type_vehicle` (`id_type_vehicle`, `name_type_vehicle`)
VALUES
	('1','MINI MPV'),
	('2','SUV'),
	('3','Minivan'),
	('4','Minibus'),
	('5','City Car');

/*!40000 ALTER TABLE `tbl_type_vehicle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_vehicle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_vehicle`;

CREATE TABLE `tbl_vehicle` (
  `id_vehicle` varchar(50) NOT NULL DEFAULT '',
  `name_vehicle` varchar(50) NOT NULL DEFAULT '',
  `type_id_vehicle` varchar(50) NOT NULL DEFAULT '',
  `cost_vehicle` decimal(15,2) DEFAULT NULL,
  `photo_vehicle` varchar(100) DEFAULT NULL,
  `seat_capacity_vehicle` int(11) DEFAULT '0',
  `suitcase_vehicle` int(11) DEFAULT '0',
  `description_vehicle` text,
  `provided_vehicle` varchar(50) DEFAULT NULL,
  `organization_id_vehicle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_vehicle` WRITE;
/*!40000 ALTER TABLE `tbl_vehicle` DISABLE KEYS */;

INSERT INTO `tbl_vehicle` (`id_vehicle`, `name_vehicle`, `type_id_vehicle`, `cost_vehicle`, `photo_vehicle`, `seat_capacity_vehicle`, `suitcase_vehicle`, `description_vehicle`, `provided_vehicle`, `organization_id_vehicle`)
VALUES
	('1551852673960765','Toyota Calya','5',367500.00,NULL,5,2,'','Qoyum','3110201400000'),
	('1554142421514719','Suzuki Ertiga','1',367000.00,NULL,6,2,'','','3110201400000'),
	('1554148313313636','Toyota Alphard','3',980000.00,NULL,6,2,'',NULL,'3110201400000'),
	('1554185599696433','Daihatsu Xenia','1',367000.00,'1556558646157432',6,2,'','','3110201400000'),
	('1555605643911329','Alphard Transformer','3',784000.00,'1556534441927466',2,6,'','Qoyum','3110201400000'),
	('1556502810545868','Innova Super White','5',300000.00,'1556525639754372',4,6,'','C1000','3110201400000');

/*!40000 ALTER TABLE `tbl_vehicle` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
