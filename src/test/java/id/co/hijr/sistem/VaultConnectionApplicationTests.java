package id.co.hijr.sistem;

import java.io.File;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import id.co.hijr.sistem.service.VaultService;

@SpringBootTest
class VaultConnectionApplicationTests {

	@Value("classpath:bc here/qoy.png")
    private Resource fileKtpPlain;
	
	@Autowired 
	private VaultService vaultService;
	
	String encryptedNik="";
    
	//@Test
    public void testEncryptString() {
        String nik = "123456789";
        encryptedNik = vaultService.encrypt(nik);
        Assertions.assertNotNull(encryptedNik);
        System.out.println("Encrypted : " + encryptedNik);
        testDecryptString(encryptedNik);
    }
   
    public void testDecryptString(String encryptedNik) {
        try {

            System.out.println("Before Decrypted : " + encryptedNik);
            String decryptedNik = vaultService.decrypt(encryptedNik);
            Assertions.assertNotNull(decryptedNik);
            System.out.println("Decrypted : " + decryptedNik);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    }


    
    public void testEncryptFile() throws Exception {
        File encryptedKtp = vaultService.encrypt(fileKtpPlain.getFile());
        System.out.println("Encrypted : " + encryptedKtp);
    }
   
}
