var path = ctx + '/ticket/bank';
var path_upload = ctx + '/files/';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	display();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	$('[name=type]').on('change', function(){
		if(this.value=="ATM"){
			$('.form-type').show();
		}else if(this.value=="VA"){
			$('.form-type').hide();
		}
	});
	
	$("#filter_code").on('change', function(){
		display();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});
}

var pathsql = ctx + '/restql';
function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');

		
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	if($("#filter_code").val()!="") params+='&filter=code.eq("'+$("#filter_code").val()+'")';
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(pathsql + '/configure?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function fillFormValue(value){
	clearForm();
	
	tinymce.remove('#value');

	$('[name=name]').val(value.name);
	$('[name=type]').val(value.type);
	var h ='<label for="">Value</label>';
	if(value.code=="DISCLAIMER"){
		h+='<textarea id="value" class="form-control" name="value" placeholder="" autocomplete="off"></textarea>';
	}else{
		h+='<input id="value" type="text" class="form-control" name="value" placeholder="" autocomplete="off">';
	}
	$("#form-value").html(h);
	$('[name=id]').val(value.id);
	if(value.type=="CORE") $('[name=name]').attr('disabled', true);
	if(value.code=="DISCLAIMER") {
		initTinyMce('value');
		tinymce.activeEditor.setContent(value.value);
		$('#value').html(value.value);
	}else{
		$('[name=value]').val(value.value);
	}
}

function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	var valueStr=$('[name=value]').val();
	//
		var new_s="";
		if(tinymce.get("value")!=null){
			var htmlObject = $.parseHTML(tinymce.get("value").getContent());
			$.each(htmlObject, function(key){
				$.each($(this).find('img'), function(key_2){
					console.log(this);
					this.src=this.title;
				})
				if(this.outerHTML != undefined)
				new_s=this.outerHTML;
			});
			
			//obj.append('description', tinymce.get("value").getContent());
			valueStr=tinymce.get("value").getContent();
		}
	//
	var url = pathsql+'/configure/save?prefix_entity=tbl_';
	if($('[name=id]').val() != "") url = pathsql+'/configure/save?prefix_entity=tbl_&append=false';
	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray()),
			dataMain : {
				"column" : [
					$('[name=name]').val(),
					valueStr,
					$('[name=type]').val()
				]
			}
		};
	obj.data.value=valueStr;
	if($('[name=id]').val() == "") delete obj.data.id;
	console.log(obj);
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form').modal('hide');
        		display();
        },
        error : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        		$('#modal-form-msg').show();
        },
    });
    
}


function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	$('#modal-form-upload').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}
function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
	row += '<td>'+(num)+'</td>';
	row += '<td class=""> '+value.name +' </td>';
	row += '<td class=""> '+value.value +' </td>';
	row += '<td align="" class=""> '+value.type +' </td>';
	row += '<td class="">';
	//row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-success pill" type="" onclick="doAction(\''+value.id+'\',\'upload\')"><i class="fas fa-fw fa-file-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	if(value.type!="CORE")row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}
function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.form-type').hide();
	$('[name=name]').attr('disabled', false);
	
	
	tinymce.remove('#value');
	var h ='<label for="">Value</label>';
	h+='<input id="value" type="text" class="form-control" name="value" placeholder="" autocomplete="off">';
	
	$("#form-value").html(h);
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(pathsql + '/configure/'+id+'?prefix_entity=tbl_&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(pathsql + '/configure/'+id+'/'+selected_action+"?prefix_entity=tbl_",{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'upload'){
		$('#modal-form-upload-msg').hide();
		$('#modal-form-upload').modal('show');
		
	}
	
}
function saveUpload(){
	var obj = new FormData(document.querySelector('#entry-form-upload'));
	obj.append('reference', selected_id);
	ajaxPOST(path_upload + '/upload',obj,'onModalActionSuccess','onModalActionError');
}