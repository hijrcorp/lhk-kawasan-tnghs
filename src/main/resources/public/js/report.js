var pathsql = ctx + '/restql';
var path = ctx + '/ticket/purchase';
var path_upload = ctx + '/files/';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];
var selected_kode_booking = ($.urlParam('kode_booking') == null ? '' : $.urlParam('kode_booking'));

function init(){
	/*
	$('#form-filter').submit(function(e){
		display();
		//$('#a-export').attr('href','export-data?'+$('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, ''));
		e.preventDefault();		
	});
	$('#btn-search').click(function(){
		display();
	});
	*/
	getListYear();
	//initReport();
	$("[name=filter_end]").change(function(e){
		initReportHarian();
	});
	$("[name=filter_year_s], [name=filter_year_e]").change(function(e){
		console.log(e.target.name)
		if(e.target.name=='filter_year_e'){
			initReportTahunan();
		}else{
			$("[name=filter_year_e]").val("0");
		}
	});
	$("[name=filter_year]").change(function(e){
		initReportBulanan();
	});
}

function getListYear(){
    var arr = [];
	let startYear = new Date().getFullYear();
	// Loop backwards through the previous 5 years using i--
	for (let i = 4; i >= 0; i--) {
		let year = startYear - (4 - i);
	 	arr.push(year);
		$("[name=filter_year_s]").append(new Option(year));
		$("[name=filter_year_e]").append(new Option(year));
		$("[name=filter_year]").append(new Option(year));
	}
    return arr;
}

function renderPercentage(value, numeric) {
	if(numeric == 0) return 0.5;
	return (value < 3 ? 3 : value);
}
//HARIAN
function initReportHarian(){
	var params = "";
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	params+="filter_tanggal_pendakian_s="+$("[name=filter_start]").val()+"&filter_tanggal_pendakian_e="+$("[name=filter_end]").val();
	ajaxGET(path + '/report/harian?'+params,'onGetInitReportHarianSuccess','onGetListError');
}
function onGetInitReportHarianSuccess(response){
	$.LoadingOverlay("hide");
	var arrYearDataSet = {
		label: [],
		backgroundColor: [],
		borderColor: [],
		borderWidth: [],
		data: [],
		countTicket: [],
	};
	//
	$.each(response.data, function(key, obj){
		arrYearDataSet.label.push(response.data[key].name_region);
		arrYearDataSet.backgroundColor.push(arrPropDataSet[key].backgroundColor);
		arrYearDataSet.borderColor.push(arrPropDataSet[key].borderColor);
		arrYearDataSet.borderWidth.push(arrPropDataSet[key].borderWidth);
		arrYearDataSet.data.push(obj.amount_ticket);
		arrYearDataSet.countTicket.push(obj.count_ticket);
	});
	
	var chartData = {
		labels: arrYearDataSet.label,
		datasets: [arrYearDataSet]
	};
	console.log(chartData);
	
	var title="Laporan Grafik Harian"+' '+$("[name=filter_start]").val()+" s.d "+$("[name=filter_end]").val();
	
	var ctx = document.getElementById("chart-harian").getContext("2d");
	
	if(window.myBar == null) {
		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: chartData,
			options: {
				responsive: true,
				legend: {
					position: 'bottom',
	                display: true,
	                labels: {
	                    generateLabels: function() {
	                        var labelsOriginal = [];
							var newdata = chartData.datasets[0];
							console.log(newdata);
							$.each(newdata.label, function(key, value){
								labelsOriginal.push({
		                            text: newdata.label[key],
		                            fillStyle: newdata.backgroundColor[key],
		                            hidden: false,
		                            strokeStyle: newdata.borderColor[key],
		                        });
							})
	                        return labelsOriginal;
	                    }
	                }
				},
				title: {
					display: true,
					text: title
				},
				scales: {
		            yAxes: [{
		                id: 'y-axis-left',
		                type: 'linear',
		                position: 'left',
		                scaleLabel: {
		                    display: true,
		                    labelString: 'Amount Ticket'
		                },
		                ticks: {
		                    beginAtZero: true,
							max: Math.max(...chartData.datasets[0].data),
							callback: function(value){return $.number(value,0)}
		                }
		            }, {
		                id: 'y-axis-right',
		                type: 'linear',
		                position: 'right',
		                scaleLabel: {
		                    display: true,
		                    labelString: 'Count Visitor'
		                },
		                gridLines: {
		                    drawOnChartArea: false
		                },
		                ticks: {
		                    beginAtZero: true,
		                    max: Math.max(...chartData.datasets[0].countTicket)
		                }
		            }],
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var dataset = data.datasets[tooltipItem.datasetIndex];
							return 'Amount Tickets: ' + $.number(dataset.data[tooltipItem.index],0)+", Count Visitor: "+dataset.countTicket[tooltipItem.index];//+" - "+//dataChart[tooltipItem.index][tooltipItem.datasetIndex];
						}
					}
				}
			}
		});
	}else {
		var myChart = window.myBar;
		myChart.data.datasets[0] = chartData.datasets[0];
		myChart.options.scales.yAxes[0].ticks.max = Math.max(...chartData.datasets[0].data)
		myChart.options.scales.yAxes[1].ticks.max = Math.max(...chartData.datasets[0].countTicket)
		myChart.update(); 
    }
}

//TAHUNAN
function initReportTahunan(){
	var params = "";
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	params+="filter_year_s="+$("[name=filter_year_s]").val()+"&filter_year_e="+$("[name=filter_year_e]").val();
	ajaxGET(path + '/report/tahunan?'+params,'onGetInitReportTahunanSuccess','onGetListError');
}

function onGetInitReportTahunanSuccess(response){
	$.LoadingOverlay("hide");
	var arrYear = response.data.masterYear;
	var s_tahun = $("[name=filter_year_s]").val();
	var e_tahun = $("[name=filter_year_e]").val();
	
	var arrYearDataSet = [];
	var maxArrYearDataSet = {countMax:[],amountMax:[]};
	$.each(response.data, function(key, obj){
		//set dataset
		arrYearDataSet.push(
			{
				label: obj.region_name,
				backgroundColor: arrPropDataSet[key].backgroundColor,
				borderColor: arrPropDataSet[key].borderColor,
				borderWidth: arrPropDataSet[key].borderWidth,
				data: [],
				countTicket: [],
			}
		);
		
		//set dataset data
		$.each(obj.region_purchase, function(keys, second_main){
			maxArrYearDataSet.countMax.push(second_main.count_ticket);
			maxArrYearDataSet.amountMax.push(second_main.amount_ticket);
			arrYearDataSet[key].data.push(second_main.amount_ticket);
			arrYearDataSet[key].countTicket.push(second_main.count_ticket);
		});
	});
	arrYear = [];
	for (let i = s_tahun; i <= e_tahun; i++) {
		arrYear.push(i);
	}
	var barChartData = {
		labels: arrYear,
		datasets: arrYearDataSet,
		countMax: Math.max(...maxArrYearDataSet.countMax),
		amountMax: Math.max(...maxArrYearDataSet.amountMax),
	};
	
	console.log(barChartData);
	var title="Laporan Grafik Tahunan"+' '+(s_tahun)+" s.d "+e_tahun;
	renderChart("chart-tahunan", barChartData, title);
}

//BULANAN
function initReportBulanan(){
	var params = "";
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	params+="filter_year="+$("[name=filter_year]").val();
	ajaxGET(path + '/report/bulanan?'+params,'onGetInitReportBulananSuccess','onGetListError');
}

function onGetInitReportBulananSuccess(response){
	$.LoadingOverlay("hide");
	console.log(response);
	
	var arrYearDataSet = [];
	var maxArrYearDataSet = {countMax:[],amountMax:[]};
	
	$.each(response.data, function(key, obj){
		//set dataset
		arrYearDataSet.push(
			{
				label: obj.region_name,
				backgroundColor: arrPropDataSet[key].backgroundColor,
				borderColor: arrPropDataSet[key].borderColor,
				borderWidth: arrPropDataSet[key].borderWidth,
				data: [],
				countTicket: [],
			}
		);
		
		//set dataset data
		$.each(obj.region_purchase, function(keys, second_main){
			maxArrYearDataSet.countMax.push(second_main.count_ticket);
			maxArrYearDataSet.amountMax.push(second_main.amount_ticket);
			arrYearDataSet[key].data.push(second_main.amount_ticket);
			arrYearDataSet[key].countTicket.push(second_main.count_ticket);
		});
	});
	
	var barChartData = {
		labels: moment.monthsShort(),
		datasets: arrYearDataSet,
		countMax: Math.max(...maxArrYearDataSet.countMax),
		amountMax: Math.max(...maxArrYearDataSet.amountMax),
	};
	console.log(barChartData);
	var title="Laporan Grafik Bulanan - "+$("[name=filter_year]").val()+"";
	renderChart("chart-month", barChartData, title);
}



var myBarAll = null;
function renderChart(elem, chartData, title){
	var config = {
		type: 'bar',
		data: chartData,
		options: {
			responsive: true,
			legend: {
				position: 'bottom',
                display: true,
			},
			title: {
				display: true,
				text: title
			},
			scales: {
	            yAxes: [{
	                id: 'y-axis-left',
	                type: 'linear',
	                position: 'left',
	                scaleLabel: {
	                    display: true,
	                    labelString: 'Amount Ticket'
	                },
	                ticks: {
	                    beginAtZero: true,
						max: chartData.amountMax,
						callback: function(value){return $.number(value,0)}
	                }
	            }, {
	                id: 'y-axis-right',
	                type: 'linear',
	                position: 'right',
	                scaleLabel: {
	                    display: true,
	                    labelString: 'Count Visitor'
	                },
	                gridLines: {
	                    drawOnChartArea: false // Avoid overlapping grid lines with the left y-axis
	                },
	                ticks: {
	                    beginAtZero: true,
						max: chartData.countMax
	                }
	            }],
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var dataset = data.datasets[tooltipItem.datasetIndex];
						return 'Amount Tickets: ' + $.number(dataset.data[tooltipItem.index],0)+", Count Visitor: "+dataset.countTicket[tooltipItem.index];//+" - "+//dataChart[tooltipItem.index][tooltipItem.datasetIndex];
					}
				}
			}
		}
	}
	
	var ctx = document.getElementById(elem).getContext("2d");
	
	if(elem=="chart-month"){
		if(window.myBarAllMonth==null){
			window.myBarAllMonth = new Chart(ctx, config);
		}else {
			var myChart = window.myBarAllMonth;
			myChart.data.datasets = chartData.datasets;
			myChart.data.labels = chartData.labels;
			myChart.options.title.text = title;
			myChart.options.scales.yAxes[0].ticks.max = chartData.amountMax;
			myChart.options.scales.yAxes[1].ticks.max = chartData.countMax;
			myChart.update(); 
	    }
	}else if(elem=="chart-tahunan") {
		if(window.myBarAllYear==null){
			window.myBarAllYear = new Chart(ctx, config);
		}else {
			var myChart = window.myBarAllYear;
			myChart.data.datasets = chartData.datasets;
			myChart.data.labels = chartData.labels;
			myChart.options.scales.yAxes[0].ticks.max = chartData.amountMax;
			myChart.options.scales.yAxes[1].ticks.max = chartData.countMax;
			myChart.update(); 
	    }
	}
}

function initReport(){
	var response = {
		data : {
			chartBar : { 
				numeric : {
					kn_dollar_1: 20,
					kn_dollar_2: 50,
					kn_dollar_3: -30,
					kn_rp_1: 9738822353,
					kn_rp_2: 5775275343,
					kn_rp_3: 3963547010,
					rekomendasi_belum_selesai: 883,
					rekomendasi_semua: 9778,
					rekomendasi_sudah_selesai: 7869,
				},
				percentage : {
					kn_dollar_1: 100,
					kn_dollar_2: 250,
					kn_dollar_3: -150,
					kn_rp_1: 100,
					kn_rp_2: 59,
					kn_rp_3: 40,
					rekomendasi_belum_selesai: 9,
					rekomendasi_semua: 100,
					rekomendasi_sudah_selesai: 80,
				}
			}
		}
	}
	const chartBar = response.data.chartBar.percentage
	const chartBarNumeric = response.data.chartBar.numeric
	
	var color = Chart.helpers.color;
	
	$('#kn-1').html('Rp. ' + (chartBarNumeric.kn_rp_1))//.formatMoney(0, '.', ',')
	$('#kn-2').html('Rp. ' + (chartBarNumeric.kn_rp_2))//.formatMoney(0, '.', ',')
	$('#kn-3').html('Rp. ' + (chartBarNumeric.kn_rp_3))//.formatMoney(0, '.', ',')
	
	let dataChart = [
		[
			(chartBarNumeric.rekomendasi_semua),//.formatMoney(0, ',', '.'), 
			(chartBarNumeric.rekomendasi_belum_selesai),//.formatMoney(0, ',', '.'), 
			(chartBarNumeric.rekomendasi_sudah_selesai),//.formatMoney(0, ',', '.')
		],
		[
			'Rp.' + (chartBarNumeric.kn_rp_1),//.formatMoney(0, '.', ','), 
			'Rp.' + (chartBarNumeric.kn_rp_2),//.formatMoney(0, '.', ','), 
			'Rp.' + (chartBarNumeric.kn_rp_3),//.formatMoney(0, '.', ',')
		],
		[
			'$' + (chartBarNumeric.kn_dollar_1),//.formatMoney(0, '.', ','), 
			'$' + (chartBarNumeric.kn_dollar_2),//.formatMoney(0, '.', ','), 
			'$' + (chartBarNumeric.kn_dollar_3),//.formatMoney(0, '.', ',')
		],
	]
	
	/*********** begin chart bar ***********/ 
	var barChartData = {
		labels: ["Rekomendasi", "KN (Rp)", "KN (US$)"],
		datasets: [{
			label: 'Data Awal',
			backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
			borderColor: window.chartColors.blue,
			borderWidth: 1,
			data: [
				renderPercentage(chartBar.rekomendasi_semua, chartBarNumeric.rekomendasi_semua),
				renderPercentage(chartBar.kn_rp_1, chartBar.kn_rp_1),
				renderPercentage(chartBar.kn_dollar_1, chartBar.kn_dollar_1)
			]
		}, {
			label: 'Tindak Lanjut',
			backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
			borderColor: window.chartColors.green,
			borderWidth: 1,
			data: [
				renderPercentage(chartBar.rekomendasi_belum_selesai, chartBarNumeric.rekomendasi_semua),
				renderPercentage(chartBar.kn_rp_2, chartBarNumeric.kn_rp_2),
				renderPercentage(chartBar.kn_dollar_2, chartBarNumeric.kn_dollar_2)
			]
		}, {
			label: 'Sisa',
			backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
			borderColor: window.chartColors.blue,
			borderWidth: 1,
			data: [
				renderPercentage(chartBar.rekomendasi_sudah_selesai, chartBarNumeric.rekomendasi_sudah_selesai),
				renderPercentage(chartBar.kn_rp_3, chartBarNumeric.kn_rp_3),
				renderPercentage(chartBar.kn_dollar_3, chartBarNumeric.kn_dollar_3)
			]
		}]
	
	};
	
	var yearnow=new Date().getFullYear();
	var context1 = document.getElementById("chart-bar").getContext("2d");
	window.myBar = new Chart(context1, {
		type: 'bar',
		data: barChartData,
		options: {
			responsive: true,
			legend: {
				position: 'bottom',
			},
			title: {
				display: true,
				text: 'Progress Tindak Lanjut Hasil Audit '+(yearnow-5)+" s.d "+yearnow
			},
			scales: {
				yAxes: [{
				ticks: {
						min: 0,
						max: 100,
						callback: function(value){return value+ "%"}
					},
					scaleLabel: {
						display: true,
						labelString: "Percentage"
					}
				}]
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var dataset = data.datasets[tooltipItem.datasetIndex];
					return ' ' + dataset.label + ': ' + dataChart[tooltipItem.index][tooltipItem.datasetIndex];
					}
				}
			}
		}
	});
}