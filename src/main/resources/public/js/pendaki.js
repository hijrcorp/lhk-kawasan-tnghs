var path = ctx + '/visitor/identity';
var path_account = ctx + '/account';
var path_region = ctx + '/ticket/region';
var path_group = ctx + '/account/group';
var selected_id = '';
var selected_account_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	//getSelectGroup();
	//getSelectRegion();
	//display();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	
	$('[name=filter_group]').change(function(e){
		$('#search-form').submit();
	});
	
	$('[name=group_id]').change(function(e){
		if(this.value==2002400){
			$('#form-region').show();
		}else{
			$('#form-region').hide();
		}
	});
	
	$('[name=status_valid]').change(function(e){
		if(this.value=='BANNED'){
			$("#form-banned").show();
		}else{
			$("#form-banned").hide();
		}
	});

	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});
	
}

function getSelectRegion(){
	
	$('[name=region_id]').empty();
	$('[name=region_id]').append(new Option('Select Region/Kawasan', ''));
	
	ajaxGET(path_region + '/list','onGetSelectPosition','onGetSelectError');
}
function onGetSelectPosition(response){
	$.each(response.data, function(key, value) {
		$('[name=region_id]').append(new Option(value.name, value.id));
	});
}


function getSelectGroup(){
	$('[name=filter_group]').empty();
	$('[name=filter_group]').append(new Option('All Groups', ''));
	
	$('[name=group_id]').empty();
	$('[name=group_id]').append(new Option('Select Group', ''));
	
	ajaxGET(path_group + '/list?filter_application='+$('[name=filter_application]').val(),'onGetSelectGroup','onGetSelectError');
}
function onGetSelectGroup(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_group]').append(new Option(value.name, value.id));
		$('[name=group_id]').append(new Option(value.name, value.id));
	});
}

function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path + '/list?page='+page+'&'+params+"&limit=50",'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
//	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
	row += '<td>'+(num)+'</td>';
	var status_active = '<span class="badge badge-pill badge-success text-white">ACTIVE <i class="fas fa-check-circle"></i></span>';
	var status_banned = '<span class="badge badge-pill badge-danger text-white">BANNED <i class="fas fa-ban"></i></span>';
	row += '<td class="">'+value.no_identity+'<br/>'+(value.status_valid==null?status_active:status_banned)+'</td>';
	row += '<td class="">'+value.full_name+'</td>';
	row += '<td class="">'+'<i class="fas fa-calendar"></i> '+moment(value.birthdate).format('DD-MM-YYYY').toUpperCase()+'<hr/>'+(value.gender=="L"?'<i class="fas fa-male"></i> ':'<i class="fas fa-female"></i> ')+' '+value.gender+'</td>';
	row += '<td class="">'+value.address+'</td>';
	row += '<td class="">'+'<span class="fas fa-mobile-alt"></span> '+value.phone_number+"<hr/><i class='fas fa-envelope-open-text'></i>  "+value.email+'</td>';
	
	//if(value.account.enabled==true){
	//	row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
	//}else{
	//	row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
	//}
	//row += '<td class="">'+moment(value.account.time_added).fromNow();+'</td>';
	var banned = '<i class="fa fa-user-slash"></i>';
	var unbanned = '<i class="fa fa-user"></i>';
	row += '<td class="">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	/*if(value.status_valid==null)
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'unbanned\')">'+unbanned+'</a>';
	else
		row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'banned\')">'+banned+'</a>';
	*/
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="uploadIdentity(\''+value.id+'\',\'upload\')"><i class="fa fa-fw fa-file-upload"></i></a>';
	//
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	
	$('[name=region_id]').prop('disabled', false);
	$('[name=group_id]').prop('disabled', false);
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
//	console.log(value);
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		//selected_account_id = value.account.id;
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function onModalActionSuccess(response){
//	console.log(response);
//	console.log(selected_action);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(value){
	$("#form-banned").hide();
	clearForm();
	
	$('[name=type_identity]').val(value.type_identity);
	$('[name=no_identity]').val(value.no_identity);
	$('[name=full_name]').val(value.full_name);
	$('[name=gender]').val(value.gender);
	
	$('[name=address]').val(value.address);
	$('[name=birthdate]').val(moment(value.birthdate, 'YYYY-MM-DD').add(1, 'day').format("YYYY-MM-DD"));
	
	$('[name=phone_number]').val(value.phone_number);
	$('[name=email]').val(value.email);
	
	$('[name=status_valid]').val(value.status_valid).trigger("change");
	
	if(value.status_valid=='BANNED'){
		$("#form-banned").show();
		$('[name=date_banned]').val(moment(value.date_banned, 'YYYY-MM-DD').add(1, 'day').format("YYYY-MM-DD"));
		$('[name=days_banned]').val(value.days_banned);
	}
	//$('[name=file]').prop('disabled', true);
}

function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
	ajaxPOST(path + '/save',obj,'onModalActionSuccess','onModalActionError');
}

function uploadIdentity(id){
	ajaxGET_NEW(path + '/'+id,function(response){
		console.log(response);
		var value = response.data;
		Swal.fire({
	      title: 'Your uploaded picture',
	      imageUrl: ctx+'/files/'+value.id_file+'?filename='+value.file_name+'&decrypt',
	      imageAlt: 'The uploaded picture',
		  showCancelButton: true,
		  confirmButtonText: 'Change Picture',
		  cancelButtonText: 'Cancel!',
	    }).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire({
			  title: 'Select image',
			  input: 'file',
			  inputAttributes: {
			    'accept': 'image/*',
			    'aria-label': 'Upload your profile picture'
			  }
			}).then((file) => {
				console.log(file);
				var obj = new FormData();
				if(id != '') obj.append('id', id);
				obj.append('file_identity', file.value);
				ajaxPOST_NEW(path + '/upload',obj,function(response){
					console.log(response);
					
				    /*swal.fire(
				      'Deleted!',
				      'Your file has been deleted.',
				      'success'
				    )*/

					if (file) {
					  const reader = new FileReader()
					  reader.onload = (e) => {
					    Swal.fire({
					      title: 'Your uploaded picture',
					      imageUrl: e.target.result,
					      imageAlt: 'The uploaded picture'
					    })
					  }
					  reader.readAsDataURL(file.value)
					}

				},'onModalActionError');
				
			})
		  }
		})
	},'onActionError');
}

