
var pathsql = ctx + '/restql';
/*var path = ctx + '/ticket/region';
var path_purchase = ctx + '/ticket/purchase';
var path_facility = ctx + '/ticket/region-facility';
var path_todo = ctx + '/ticket/region-todo';
var path_upload = ctx + '/files/';
var current_page = 1;
var params = '';
var appendIds = [];
var select_obj={"id" : "", "header" : "", "mode" : "", "parent" : "", "action" : ""};
var selected_step='';
var total_price=0;*/
//end here

var urlCallbackNotif;

var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = sessionStorage.getItem("temp_timeou");
var timmer;
var emails="";
var isRead=false;

function init(){
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	selected_step = ($.urlParam('step') == null ? '' : $.urlParam('step'));
	ajaxGET_NEW(pathsql + '/configure?filter=name.in("EMAIL_CONFIRM_PURCHASE","DISCLAIMER_PAYMENT")', function(response_configure){
		$.each(response_configure.data, function(){
			
			if(this.name=="DISCLAIMER_PAYMENT"){
				$("#staticBackdrop .modal-body").html(this.value)
			}else{
				emails=this.value;
				$('[name=email_notification]').val(this.value);
			}
		})
	},'onGetListError');
	
	$("#form-detil-transaction").submit(function(e){
		e.preventDefault();
		if($('[name=bank_id]:checked').data('typeBank')=="VA"){
			save('CREATE_VA');
		}else save();
	});
	
	$("#btn-mengerti").click(function(){
		$('#form-detil-transaction').find('button').attr('disabled', false);
		$('#staticBackdrop').modal('hide');

		Swal.fire({
		    icon: 'success',
		    title: 'Terkonfirmasi',
		    text: 'Terimakasih atas Persetujuan Anda.',
			allowOutsideClick: false,
		}).then((result) => {
			console.log(result);
		  if (result.isConfirmed) {
			isRead=true;
			$("#btn-link-mengerti").attr('class', 'disabled btn-link');
		  }
		})
		
	});
	urlCallbackNotif = ($.urlParam('callback') == null ? '' : $.urlParam('callback'));
	//end here
	
	$('[name=file_transaction]').on('change', function(){
		if(this.files.length>0){
			console.log("a");
			if(this.files[0].size > 2173155){
				alert("File tidak boleh lebih dari 2MB.");
				this.value = "";
				document.getElementById("btn-upload").classList.toggle('btn-success');
				document.getElementById("btn-upload").classList.add('btn-warning');
			}else{
				console.log("this.value:"+this.value);
				if(document.getElementById("btn-upload").classList.contains('btn-warning') ){
					document.getElementById("btn-upload").classList.toggle('btn-warning');
					document.getElementById("btn-upload").classList.add('btn-success')
				}else if ( document.getElementById("btn-upload").classList.contains('btn-success') && this.value==""){
					document.getElementById("btn-upload").classList.toggle('btn-success');
					document.getElementById("btn-upload").classList.add('btn-warning');
				}
				
			}
		}else{
			console.log("b");
			document.getElementById("btn-upload").classList.toggle('btn-success');
				document.getElementById("btn-upload").classList.add('btn-warning');
		}
	});
}

function swall_view(src, phonenumber, address){
	var kontent='<p class="font-weigh-bold card-title">Informasi Keluarga yang bisa dihubungi</p><p>Nomor.HP : '+phonenumber+'</p> <p>Alamat Rumah : '+address+'</p>';
	
	if(phonenumber=="null") kontent="";
	Swal.fire({
	  //title: 'Informasi Keluarga yang bisa dihubungi',
	  html: kontent,
	  imageUrl: src,
	  imageWidth: 400,
	  imageHeight: 200,
	  imageAlt: 'Custom image',
	})
}


function onCheckStatusSucc(response){
	console.log(response);
	console.log(response.data.status);
	if(response.data.status!="VERIFYING") {
		clearInterval(timenow);
		window.location.reload();
	}
}

function onGetTimeLeftSucc(response){
	var seco = response.data.timeleftSecond2;
	console.log(response.data.timeleftSecond2);
	sessionStorage.setItem("temp_timeou", seco);
	totalSeconds = sessionStorage.getItem("temp_timeou");
	timmer=setInterval(setTime, 1000);
}

function onCheckErr(response){
	console.log(response);
}

function setTime() {
  ++totalSeconds;
  sessionStorage.setItem("temp_timeou", totalSeconds);
  
  secondsLabel.innerHTML = pad(totalSeconds % 60);
  minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));

  if(totalSeconds==900){
	clearInterval(timmer);
	setTimeout(() => window.location.reload(), 3000);
  }
  //clearInterval(setInterval(setTime, 1000, sessionStorage.getItem("temp_timeou")));
}

function pad(val) {
  var valString = val + "";
  if (valString.length < 2) {
    return "0" + valString;
  } else {
    return valString;
  }
}

function counter(cnt){
    cnt -= 1;
    //aktifkan lagi
    sessionStorage.setItem("temp_timeou", cnt);
    var minutes = parseInt(cnt / 60, 10)
    var seconds = parseInt(cnt % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    jQuery("#spam-timer").html("<strong>"+minutes+":"+seconds+"</strong>");
   
    if(cnt>0){
    	timeou=setTimeout(counter,1000,cnt);
    }else{
		$('#spam-timer').html("<strong>Habis</strong>");
		$('.form-check-input').remove();
		var next_now=parseInt($('.bg-warning').text())+1;
		
		//ajaxGET(url + 'update-durasi-soal?id='+id_pertanyaan+'&durasi='+sessionStorage.getItem("temp_timeou")+'&mark='+next_now,'onUpdateSucc','onSaveError');
		clearInterval(timenow);
		//var row = '<button onclick="renderDisplay(1)" type="button" class="btn m-0 p-0 float-right"><i class="fas fa-chevron-circle-right"></i> Next</button>';
		//$('#footer-pagination').html(row);
    }
}

function save(do_action=0){
	$.LoadingOverlay("show", { textResizeFactor:0.3, text: "Mohon tunggu, kami sedang mempersiapkan form anda.", image : ctx + "/images/loading-spinner.gif" });
	if(do_action=='CREATE_VA'){
		var obj = new FormData()
		obj.append("bank_id", $('[name=bank_id]:checked').val());
		ajaxPOST(path_payment + '/'+selected_id+'/'+do_action,obj,'onPrepareModalActionSuccess','onModalActionError');
	}else{
		var obj = new FormData(document.querySelector('#form-detil-transaction'));
		if($('[name=bank_id]:checked').val()!=undefined) {
			obj.append('bank_id', $('[name=bank_id]:checked').val());
			obj.append('bank_type', $('[name=bank_id]:checked').data('typeBank'));
			
			if(isRead==false) {
				$.LoadingOverlay("hide");
				alert("Anda belum membaca atau menyetujui syarat dan ketentuan.")
				return false;
			}
		}
    	ajaxPOST(path_payment + '/save',obj,'onModalActionSuccess','onModalActionError');
	}
}


function onPrepareModalActionSuccess(response) {
	var value = response.data;
	
	var obj = new FormData(document.querySelector('#form-detil-transaction'));
	if($('[name=bank_id]:checked').val()!=undefined) {
		obj.append('bank_id', $('[name=bank_id]:checked').val());
		obj.append('bank_type', $('[name=bank_id]:checked').data('typeBank'));
	}
	obj.append('bri_signature', value.signature);
	obj.append('bri_timestamp', value.timestamp);

    ajaxPOST(path_payment + '/save',obj,'onModalActionSuccess','onModalActionError');
}

function onModalActionSuccess(resp){
	console.log(resp);
	//window.location.reload();
	window.location.href=window.location.href+"&callback="+resp.next_page;
}

function onModalActionError(resp){
	$.LoadingOverlay("hide");
	console.log(resp);
	alert(resp.responseJSON.message);
}

function upload(confirm=0){
	if(confirm==0) {
		$('[name=file_transaction]').click();
	}
}
function submitupload(confirm=0){
	var obj = new FormData(document.querySelector('#form-detil-transaction'));
	/*if(selected_id != '') obj.append('id', selected_id);
	obj.append('reference', selected_id);*/
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxPOST(path_payment + '/submit-provement',obj,'onModalActionSuccess','onModalActionError');
}

function checkStatusNotifMail(){
	//var qry="";
	//var page=1,params="filter=id_kelompok_jabatan.eq('"+id+"')";
	//var params=$('[name=id]').val();
	//'http://localhost:7078/ntfc/notification/check/'+params
	ajaxGETDANGER(urlCallbackNotif+"?token="+getCookieValue('kawasan.token'),'onGetCheckStatusNotif','onGetSelectError');
}
function onGetSelectError(){
	$.LoadingOverlay("hide");
	clearInterval(checkRealMail);
	template='<div class="form-group">';
	template+='<div class="alert border border-danger" style="height: auto;">';
		template+='<span class="fa fa-info-circle text-danger"></span> Instruksi pembayaran gagal dikirim ke email anda';
	template+='</div>';
	template+='</div>';
	$('#msg-notif').html(template);
	$('#msg-notif').show();
}
function onGetCheckStatusNotif(response){
	console.log(response);
	var template = '';
	template='<div class="form-group">';
	if(response.data.status_notification=="SENT" || response.data.status_notification=="FAILED"){
		if(response.data.status_notification=="FAILED"){
			template+='<div class="alert border border-danger">';
				template+='<span class="fa fa-info-circle text-danger"></span> Instruksi pembayaran gagal dikirim ke email anda';
			template+='</div>';
		}else if(response.data.status_notification=="SENT"){
			template+='<div class="alert border border-info">';
				template+='<span class="fa fa-info-circle text-info"></span> Instruksi pembayaran telah dikirim ke email anda';
			template+='</div>';
		}
		clearInterval(checkRealMail);
	}else{
		template+='<div class="alert border border-info"><img width="23" height="23" src="'+ctx + "/images/loading-spinner.gif"+'"> <small>Sistem sedang mengirimkan email Instruksi pembayaran Anda.</small> </div>';
	}
	$.LoadingOverlay("hide");
	template+='</div>';
	$('#msg-notif').html(template);
	$('#msg-notif').show();
}