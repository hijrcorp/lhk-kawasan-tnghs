var selected_menu = '';
var inst = $('[data-remodal-id=modalku]');

$(document).ready(function(){
	selected_menu = ($.urlParam('menu') == null ? '' : $.urlParam('menu'));
//	alert(getCookieValue(tokenCookieName));
//	if(getCookieValue(tokenCookieName)!=''){
		var cooc="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTc4MjE1MTIyLCJqdGkiOiJkM2NjYjA2Ny00YTM5LTQ4NjUtOGY1Ny02YzU4YTNmY2ZkMTAiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.JJNifAbsNIu7hkrpfjfkUOTRtI5iSVi-nqwfjrTzmWqwl32jQ9A9-jbcbSYvqIQH6y9zFa0Tn_iNcro-gtRr-qFGl5evVyqGrZq6AE776CNZ3irhOijNjV3LwRxgp-B3BJY7OdS8nqtW0plhA6IfdYB16wVCn7JCN3XYO6FzcfUzcgkDAp5Fi-mFUEyMSmtFGi7wS-pBN7wHF2ZRIuloAB7_hefeCxRI3hthQnz_VnJ6foADVqw6MSjhWBVVxYBd10_OkADrkP9XsXMCVnHbWHGZZXNVJQAdl-fcdo-ZocIETvCqmEddKQ05y4Vpgt-bPdwY62K4BG1RkdahqUpQKg";
//	}

	if(getCookieValue(tokenCookieName)!=""){
		$.ajaxSetup({
			headers : {
	//			'Authorization' : 'Bearer '+ cooc
				//'Authorization' : 'Bearer '+ getCookieValue(tokenCookieName)
			}
		});
	}
	
	$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('DD-MM-YYYY'));
		$(this).val(picker.endDate.format('DD-MM-YYYY'));
		picker.autoUpdateInput = true;
		$(this).change();
		
	});
	$('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
	    $(this).val('');
	});
	
	
	$('.select2').select2();
    $('.select2').select2({
    		width: '100%'
    });
	
	$('.sidebar .nav-item' ).each(function() {
		var a = this.firstElementChild;
		if(selected_menu != ''){
			if(a.innerText.trim()==selected_menu.trim()){
				this.classList.add("active");
			}
		}else{
			if((window.location.href.indexOf(a.getAttribute('href'))> 0) == true){
				this.classList.add("active");
			}
		}
		
		
	});
	
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
	
	
	
	moment.locale('id', {
	    relativeTime : {
	        future: "in %s",
	        past:   "%s yg lalu",
	        s  : '1 detik',
	        ss : '%d detik',
	        m:  "1 menit",
	        mm: "%d menit",
	        h:  "1 jam",
	        hh: "%d jam",
	        d:  "1 hari",
	        dd: "%d hari",
	        M:  "1 bulan",
	        MM: "%d bulan",
	        y:  "1 tahun",
	        yy: "%d tahun"
	    }
	});

	
	$('.single-date-picker').daterangepicker({
	    singleDatePicker: true,
		autoUpdateInput: false,
        minDate:moment(),
	    locale: {
	            format: 'DD-MM-YYYY',
	            cancelLabel: 'Clear'
	    }
	});
//	$("input[type='number']").inputSpinner();
	init();
	if(typeof initjsp!="undefined") initjsp();
	
});

$(function() {
	//$('.select-style').selectric();
});

function startLoading(btn){
	$('#'+btn).text('Loading..');
	$('#'+btn).prop('disabled', true);
}

function stopLoading(btn, btnlabel){
	$('#'+btn).text(btnlabel);
	$('#'+btn).prop('disabled', false);
}

function showAlertMessage(msg, timeout = 0, status=false){
	$('#modal-alert-msg').html(msg);
	$('#modal-alert').modal('show');
	var new_alert =$('#modal-alert').find('.alert-success');
	if(status){
		new_alert.removeClass('alert-success');
		new_alert.addClass('alert-danger');
	}
	
	if(timeout >0 ){
		setTimeout(function(){ $('#modal-alert').modal('hide'); }, timeout);
	}
	
}

function onActionError(response){
	console.log(response);
	$.LoadingOverlay("hide");
	$('.modal').modal('hide');
	$('#modal-error-msg').text(response.responseJSON.message);
	$('#modal-error').modal('show');
}

function getURLParam(param) {
	var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
	}
	return getUrlParameter(param);
}

function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

function url_base64_decode(str) {
	var output = str.replace('-', '+').replace('_', '/');
	switch (output.length % 4) {
		case 0:
			break;
		case 2:
			output += '==';
			break;
		case 3:
			output += '=';
			break;
		default:
			throw 'Illegal base64url string!';
	}
	var result = window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	return result;
}


function ajaxPOST(url,obj,fnsuccess, fnerror){
	$.ajax({
	    url : url,
	    method: "POST",
	    crossDomain: true,
	    contentType: false,
	    processData: false,
	    data : obj,
	    cache: false,
	    success : function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error : function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	});
}
function ajaxGET(url, fnsuccess, fnerror){
	$.ajax({
	    url: url,
	    method: "GET",
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error: function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    }
		
	});	
}
function ajaxGETDANGER(url, fnsuccess, fnerror){
	$.ajax({
	    url: url,
	    method: "GET",
	    crossDomain: true,
	    contentType: "application/x-www-form-urlencoded",
	    success: function (response) {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    },
	    error: function (response) {
			var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
	    }
		
	});	
}

function changeLang(lang){
	console.log(lang.split('_')[0]);
	moment.locale(lang.split('_')[0]);
	location.href = '?lang='+lang;
}


///** custom by Hadi **/

function getValue(value, replace = '') {
    return (value == null || value == 'null' ? replace : value);
}

function getDateForList(datetime, text = 'INVALID DATE') {
    if(datetime==null) return text;
    let result = moment(datetime).format('DD-MM-YYYY').toUpperCase();
    return (result == text ? '' : result);
}

function getDateTimeForList(datetime, text = 'INVALID DATE') {
    if(datetime==null) return text;
    let result = moment(datetime).format('DD MMMM YYYY HH:mm').toUpperCase();
    return (result == text ? '' : result);
}


$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var contains = function(needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if(!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function(needle) {
            var i = -1, index = -1;

            for(i = 0; i < this.length; i++) {
                var item = this[i];

                if((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};

function remodal_view(val){
	inst=inst.remodal();
	inst.open();
	$('#view_photo').attr("src", val);
	console.log(val);
}

function _parse(nomorNIK) {
    nomorNIK = String(nomorNIK);

    if (nomorNIK.length == 16) {
        let thisYear = new Date().getFullYear().toString().substr(-2);
        let thisCode = nomorNIK.substr(-4);

        let thisRegion = {
            provinsi: nomorNIK.substr(0, 2),
            kota: nomorNIK.substr(2, 2),
            kabupaten: nomorNIK.substr(2, 2),
            kecamatan: nomorNIK.substr(4, 2)
        }
        let thisDate = {
            hari: (nomorNIK.substr(6, 2) > 40) ? nomorNIK.substr(6, 2) - 40 : nomorNIK.substr(6, 2),
            bulan: nomorNIK.substr(8, 2),
            tahun: (nomorNIK.substr(10, 2) > 1 && nomorNIK.substr(10, 2) < thisYear) ? "20" + nomorNIK.substr(10, 2) : "19" + nomorNIK.substr(10, 2)
        }

        thisDate.lahir = moment(new Date(thisDate.tahun + "-" + thisDate.bulan + "-" + thisDate.hari).toLocaleDateString()).format("YYYY-MM-DD");
		
		//
			
        	//$data['tanggal_lahir'] = substr($nik, 6, 2);
			/*if (intval($data['tanggal_lahir']) > 40) {
		        $data['tanggal_lahir_2'] = intval($data['tanggal_lahir']) - 40;
	            $gender = 'Wanita';
	        } else {
	            $data['tanggal_lahir_2'] = intval($data['tanggal_lahir']);
	            $gender = 'Pria';
	        }*/
	
			let thisGender;
			let tanggalLahir = nomorNIK.substr(6, 2);
			if(parseInt(tanggalLahir) > 40){
				thisGender='P';
			}else{
				thisGender='L';
			}
		
		let thisAge = moment().diff(moment(new Date(thisDate.lahir)).format("YYYY-MM-DD"), 'years');

        return {
            nik: nomorNIK,
            wilayah: thisRegion,
            tanggal: thisDate,
            uniq: thisCode,
			gender: thisGender,
			age : thisAge,
            _link: {
                _wilayah: 'http://www.kemendagri.go.id/pages/data-wilayah'
            }
        }   
    } else {
		return {
            error: `Nomor NIK harus 16 digit`,
		}
    }
}

function messageAlert(msg="", icon="", title=""){
	Swal.fire({
	  icon: icon,
	  title: title,
	  text: msg,
	  //footer: '<a href>Why do I have this issue?</a>'
	})
}

var is_weekend =  function(date1){
    var dt = new Date(date1);
     
    if(dt.getDay() == 6 || dt.getDay() == 0){
    	return true;
    }else{
		return false;
	}
}
