var path = ctx + '/authorities';
var path_account = ctx + '/account';
var path_region = ctx + '/ticket/region';
var path_group = ctx + '/account/group';
var selected_id = '';
var selected_account_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	getSelectGroup();
	getSelectRegion();
	display();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	
	$('[name=filter_group]').change(function(e){
		$('#search-form').submit();
	});
	
	$('[name=group_id]').change(function(e){
		if(this.value==2002400){
			$('#form-region').show();
		}else{
			$('#form-region').hide();
		}
	});

	$('#search-form').submit(function(e){
		display();
		e.preventDefault();
	});
	
	$('#entry-form').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});
	
}

function getSelectRegion(){
	
	$('[name=region_id]').empty();
	$('[name=region_id]').append(new Option('Select Region/Kawasan', ''));
	
	ajaxGET(path_region + '/list','onGetSelectPosition','onGetSelectError');
}
function onGetSelectPosition(response){
	$.each(response.data, function(key, value) {
		$('[name=region_id]').append(new Option(value.name, value.id));
	});
}


function getSelectGroup(){
	$('[name=filter_group]').empty();
	$('[name=filter_group]').append(new Option('All Groups', ''));
	
	$('[name=group_id]').empty();
	$('[name=group_id]').append(new Option('Select Group', ''));
	
	ajaxGET(path_group + '/list?filter_application='+$('[name=filter_application]').val(),'onGetSelectGroup','onGetSelectError');
}
function onGetSelectGroup(response){
	$.each(response.data, function(key, value) {
		$('[name=filter_group]').append(new Option(value.name, value.id));
		$('[name=group_id]').append(new Option(value.name, value.id));
	});
}

function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');

		
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path + '/list?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
//	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}

function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function renderRow(value, num, method='add'){
//	console.log(value);
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
	row += '<td>'+(num)+'</td>';
	row += '<td class="">'+value.account.username+'</td>';
	row += '<td class="">'+value.account.first_name+'</td>';
	row += '<td class="">'+value.account.last_name+'</td>';
	row += '<td class="">'+value.group.name+'</td>';
	if(value.account.enabled==true){
		row += '<td class=""><span class="badge badge-pill badge-success">'+(Cookies.get(localeCookieName)=='en_US'?'Enabled':'Aktif')+'</span></td>';
	}else{
		row += '<td class=""><span class="badge badge-pill badge-secondary">'+(Cookies.get(localeCookieName)=='en_US'?'Disabled':'Tidak Aktif')+'</span></td>';
	}
	//row += '<td class="">'+moment(value.account.time_added).fromNow();+'</td>';
	row += '<td class="">';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm" type="button" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	
	$('[name=region_id]').prop('disabled', false);
	$('[name=group_id]').prop('disabled', false);
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
//	console.log(value);
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		selected_account_id = value.account.id;
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function onModalActionSuccess(response){
//	console.log(response);
//	console.log(selected_action);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function fillFormValue(value){
	clearForm();
	
	$('[name=username]').val(value.account.username);
	$('[name=first_name]').val(value.account.first_name);
	$('[name=last_name]').val(value.account.last_name);
	$('[name=email]').val(value.account.email);
	$('[name=mobile]').val(value.account.mobile);
	$('[name=position_id]').val(value.position.id);
	$('[name=group_id]').val(value.group.id).trigger("change");
	$('[name=group_id]').prop('disabled', true);
	if(value.group.name!="Supervisor"){
		$('[name=region_id]').prop('disabled', true);
	}
	$('[name=region_id]').val(value.account.region_id).trigger("change");
	if(value.account.enabled == false) $('[name=enabled]').prop('checked', true);
}

function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_account_id != '') {
		obj.append('id', selected_account_id);
		ajaxPOST(path_account + '/update',obj,'onModalActionSuccess','onModalActionError');
	}else{
		ajaxPOST(path_account + '/save',obj,'onModalActionSuccess','onModalActionError');
	}
}



