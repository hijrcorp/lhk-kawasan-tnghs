var path = ctx + '/ticket/region';
var pathsql = ctx + '/restql';
var path_purchase = ctx + '/ticket/purchase';
var path_facility = ctx + '/ticket/region-facility';
var path_todo = ctx + '/ticket/region-todo';
var path_upload = ctx + '/files/';
var current_page = 1;
var params = '';
var appendIds = [];
var select_obj={"id" : "", "header" : "", "mode" : "", "parent" : "", "action" : ""};
var selected_step='';
var total_price=0;
var list_date=[];
var weekend_price=0;
var weekend_wna_price=0;
var whatsapp=0;
var DISABLED_BOOKING=false;
var MAX_PERSON_BOOKING=10;
var LEADER_AGE_BOOKING=13;
var GENERAL_AGE_BOOKING=0;
var DISCLAIMER_BOOKING="";
var DISCLAIMER_PAYMENT="";
function getForm(){
	$('[name=no_identity]').val("1111");
	$('[name=full_name]').val("ahmad");
	$('[name=full_name]').val("ahmad");
	$('[name=phone_number]').val("82119260548");
	$('[name=email]').val("kayumiman@gmail.com");
}

function checkSize(){
	var totalSizeFile=0;
	$("[name=file_identity]").each(function(e){
	    if(this.files.length>0) totalSizeFile+=(this.files[0].size/ 1024 / 1024);
	});
	return totalSizeFile;
}

function init(){
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	selected_step = ($.urlParam('step') == null ? '' : $.urlParam('step'));
	
	//filter=name.in("WEEKEND_PRICE","WHATSAPP")
	ajaxGET_NEW(pathsql + '/configure?', function(response_configure){
		$.each(response_configure.data, function(){
			if(this.name=="WEEKEND_PRICE") weekend_price=this.value;
			if(this.name=="WEEKEND_WNA_PRICE") weekend_wna_price=this.value;
			if(this.name=="WHATSAPP") whatsapp=this.value;
			if(this.name=="DISABLED_BOOKING") {
				if(this.value=="TRUE") {
					DISABLED_BOOKING=true;
				}
			}
			if(this.name=="MAX_PERSON_BOOKING"){
				MAX_PERSON_BOOKING=this.value;
			}
			if(this.name=="LEADER_AGE_BOOKING"){
				LEADER_AGE_BOOKING=this.value;
			}
			if(this.name=="GENERAL_AGE_BOOKING"){
				GENERAL_AGE_BOOKING=this.value;
			}
			if(this.name=="DISCLAIMER_BOOKING"){
				$("#disclaimer-peraturan-modal .modal-body").html(this.value);
			}
			if(this.name=="DISCLAIMER_SOP"){
				$("#sop-modal .modal-body").html(this.value);
			}
		})
		if(nameJSP=="general")renderWidgetWhatsapp();
		
		if(dataAgenda!=''){
			$.each(JSON.parse(dataAgenda), function(){ 
				if(this.end_date!=null){
					var start = moment(this.start_date, 'YYYY-MM-DD').subtract(1, 'd')._d;
					var end = moment(this.end_date, 'YYYY-MM-DD').subtract(1, 'd')._d;
					
					var loop = new Date(start);
					while(loop <= end){       
						list_date.push(loop);
					   	var newDate = loop.setDate(loop.getDate() + 1);
					   	loop = new Date(newDate);
					}
				}else{
					var start = moment(this.start_date, 'YYYY-MM-DD')._d;
					var loop = new Date(start);
					list_date.push(loop);
				}
				
			});
		}
	},'onGetListError');
	
	
	$("#entry-form").submit(function(e){
		e.preventDefault();
		
		this.setAttribute("data-toggle", "pill");
		this.setAttribute("href", "#pills-book-1-1");
		save();
	});
	
	$("#btn-book").click(function(){
		clearForm();
	});

	$(document).on('click', '.card.vehicle', function() {
//       $(".card.vehicle").removeClass("border-primary");
		if($(event.target).closest('.input-group').length > 0) {
	        return false; // if you want to ignore the click completely
	        // return; // else
	    }
       $(this).toggleClass("card-pricing border-primary");
	});
	$(document).on('click', '.card .tour-guide', function() {
//       $(".card .tour-guide").removeClass("shadow-lg");
       $(this).toggleClass("shadow-lg");
	});
	
	
	/*$("#btn-book-1-1").click(function(){
			var arayas=[];
			var unique=[];
		if($("[data-key]").length!=getDataArr().person.length) {
			
		}
		$("[data-key]").find('input, select, textarea').not("input:checked").each(function(key){
			var thiselem = this.parentElement.parentElement.parentElement.parentElement;
			var person = thiselem.parentElement.dataset.key!=undefined?thiselem.parentElement.dataset.key:thiselem.dataset.key;
			if(this.value=="" && this.type!="file") arayas.push(this.getAttribute('data-label')+"-"+(person==0?"Person in charge":"Person "+person));
			//console.log(this.parentElement.parentElement.parentElement.parentElement.dataset.key);
			if(this.type=="file" && this.disabled==false && this.files.length==0) arayas.push(this.getAttribute('data-label')+"-"+(person==0?"Person in charge":"Person "+person));
		})
		unique = [...new Set(arayas)];
		
		console.log(arayas);
		console.log(unique);
		if(arayas.length>0){
			var msg = ("<div class='text-left text-danger'><Strong style='color:black!important'>Mohon periksa dengan teliti inputan berikut:</strong><br/>"+arayas.toString().replaceAll(",","<br/>")+"</div>");//.replaceAll(",","<br/></div>")
			Swal.fire({
			  icon: 'Warning',
			  title: 'Data yang Anda input belum lengkap',
			  html: msg,
			  footer: '<a href="javascript:void(0)" onclick="swal.close()">OK.</a>',
			  allowOutsideClick: false,
			  showConfirmButton: false,
			})
		}else{
			this.setAttribute("data-toggle", "pill");
			this.setAttribute("href", "#pills-book-1-1");
		}
	});*/
	
	$("#btn-book-1-1").click(function(){
		validateAndSubmitForms();
		var limitMB = 10;
		if(checkSize().toFixed(2)>limitMB) {
			Swal.fire({
			  icon: 'warning',
			  title: 'Ukuran total file identitas yang Anda unggah melebihi batas '+limitMB+'MB. Silakan kurangi ukuran file dan pastikan setiap file memiliki ukuran rata-rata maksimal 300KB per file. Terima kasih!',
			  //html: 'Ukuran total file identitas yang Anda unggah melebihi batas 4MB. Silakan kurangi ukuran file dan pastikan setiap file memiliki ukuran rata-rata maksimal 300KB per file. Terima kasih!',
			  //footer: '<a href="javascript:void(0)" onclick="swal.close()">OK.</a>',
			  allowOutsideClick: false,
			  showConfirmButton: true,
			})
			return false;
		}
		 if($("[data-key]").length!=getDataArr().person.length) {
		 	alert("Data Pendaki belum lengkap, mohon periksa kembali.");
		 	return false;
		 }
		 this.setAttribute("data-toggle", "pill");
		 this.setAttribute("href", "#pills-book-1-1");
		 $(".scroll-to-top ").click();
		
	});
		
	$("#btn-book-4").click(function(){
		
		/*var passTent=false;
		if($('#row-tenda-kecil').find('.btn-success').length > 0){
			passTent=true;
		}else if($('#row-tenda-besar').find('.btn-success').length > 0){
			passTent=true;
		}
		
		if(!passTent){
//			alert("Kapling tenda belum dipilih.");
			 return false;
		}*/
		/*if($('.row-rtc').hasClass('bg-primary')==false){
			alert("Basecamp belum dipilih.");
			return false;
		}else */
		if($('.row-region-turun').hasClass('bg-primary')==false){
			alert("Lokasi turun belum dipilih.");
			return false;
		}else{
			this.setAttribute("data-toggle", "pill");
			this.setAttribute("href", "#pills-book-4");
			
			total_price=$("[data-biaya_kawasan]").data('biaya_kawasan');
			renderPerson();
			renderTransitCamp();
			renderTourGuide();
			renderVehicle();
		}
	});
		
	$("#btn-book-5").click(function(){
		var pass=$("#form-luggage")[0].checkValidity();
		if(!pass){
			alert("Barang bawaan belum dipilih, mohon pastikan semuanya telah dipilih.");
			 return false;
		}
		
		this.setAttribute("data-toggle", "pill");
		this.setAttribute("href", "#pills-book-5");
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_id != '' && selected_step != ''){
			save(1);
		}
	});
	
	$('#start_date').on('change', function(){
	    $('#end_date').daterangepicker({
	            alwaysShowCalendars: true,
	            showCustomRangeLabel: true,
	           	startDate:moment($('#start_date').val(), "DD-MM-YYYY").add($("#total-price-tiket").data("maxDay")-1, 'd'),
	           	endDate:moment($('#start_date').val(), "DD-MM-YYYY").add($("#total-price-tiket").data("maxDay"), 'd'),
				//set 0 if wanna fleksibel
	            minDate:moment($('#start_date').val(), "DD-MM-YYYY").add($("#total-price-tiket").data("maxDay")-1, 'd'),
	            maxDate:moment($('#start_date').val(), "DD-MM-YYYY").add($("#total-price-tiket").data("maxDay")-1, 'd'),

	            //endDate:moment($('#start_date').val(), "DD-MM-YYYY").add($("#total-price-tiket").data("maxDay"), 'd'),
	            singleDatePicker: true,
	            showDropdowns:true,
	            autoUpdateInput: true,
	            locale: {
		            format: 'DD-MM-YYYY',
		            cancelLabel: 'Clear',
			        "customRangeLabel": "Custom",
			        "weekLabel": "W",
			        "daysOfWeek": [
			            "Mg",
			            "Sn",
			            "Se",
			            "Ra",
			            "Ka",
			            "Ju",
			            "Sa"
			        ]
	            },
		});
	});
	
	$('[name=file_identity]').on('change', function(e){
		if(this.files.length>0){
			console.log(this.files);
			if(this.files[0].size > 2173155){
				alert("File tidak boleh lebih dari 2MB.");
				this.value = "";
			}else{
				console.log(this.files[0].size);
			}
		}
	});
	$('[name=no_identity]').on('change', function(e){
		var pass=true;
		var index = this.parentElement.parentElement.parentElement.parentElement.dataset.key;
		var no_iden = this.value;
		//check identity not more than 1
		if(index > 1){
			var arr_duplicate=[];
			$.each(getDataArr().person, function(key){
				var unique = this.no_identity;
				if(unique!=""){
					console.log(this);
					console.log(no_iden+"=="+unique);
					if(no_iden==unique && index!=key){
						pass=false;
					}
				}
			});
			console.log(arr_duplicate);
		}
		//end here
		if(pass){
			
			var countNIK=0;
			var countPASSPORT=0;
			
			
				if(!isNaN(this.value.toString())){
					$($('[name=type_identity]')[index]).val("NIK").trigger('change');
					
					$.each(getDataArr().personTemp, function(key){
						console.log(this)
						if(this.responsible==0){
							if(this.type_identity=="NIK") countNIK++;
							if(this.type_identity=="PASSPORT") countPASSPORT++;
						}
					});
					console.log("countNIK"+countNIK);
					console.log("countPASSPORT"+countPASSPORT);
			
					if(countNIK > $("#total-visitor").data('count_wni')) {
						showAlertMessage("Jumlah Pendaki WNI maximal adalah "+ $("#total-visitor").data('count_wni') +" orang", 0, 'alert-warning');
						$($("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement).find('input, select').val("");
					}else{
				
						console.log(index);
						var aData = _parse(no_iden);
						console.log(aData);
						if(aData.error){
							console.log(aData.error);
							showAlertMessage(aData.error, 0, 'alert-warning');
							//$('[name=no_identity]')[index].value="";
							//$("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement.parentElement.reset();
							$($("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement).find('input, select').val("");
						}else{
							console.log(aData);
							var pass=false;
							var age=0;
							
							if(index==0 || index==1){
								if(aData.age < LEADER_AGE_BOOKING){
									pass=true;
									age=LEADER_AGE_BOOKING;
								}
							}else{
								if(aData.age < GENERAL_AGE_BOOKING){
									pass=true;
									age=GENERAL_AGE_BOOKING;
								}
							}
							
							if(pass){
								showAlertMessage('Mohon maaf anda tidak dapat menggunakan identitas ini, minimal usia '+age+' tahun', 0, 'alert-warning');
								//$('[name=no_identity]')[index].value="";
								//$("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement.parentElement.reset();
								$($("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement).find('input, select').val("");
							}else{
								console.log(this.parentElement.parentElement.parentElement.children[2].children[0].children[1].children[0].value=moment(aData.tanggal.lahir).format("YYYY-MM-DD"));
								console.log(this.parentElement.parentElement.parentElement.children[2].children[1].children[1].children[0].value=aData.gender);
								var param="&filter=no_identity.eq('"+this.value+"')";
								$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
								ajaxGET(path_purchase + '/history_visitor'+"/"+this.value+"?prefix_entity=view_&limit=1000000"+param,'onCheckVisitorSuccess','onGetListError', {"params" : index});
							}
							
						}
					}
				}else{
					$($('[name=type_identity]')[index]).val("PASSPORT").trigger('change');
					
					$.each(getDataArr().personTemp, function(key){
						console.log(this)
						if(this.responsible==0){
							if(this.type_identity=="NIK") countNIK++;
							if(this.type_identity=="PASSPORT") countPASSPORT++;
						}
					});
					console.log("countNIK"+countNIK);
					console.log("countPASSPORT"+countPASSPORT);
					
					if(countPASSPORT > $("#total-visitor").data('count_wna')) {
						showAlertMessage("Jumlah Pendaki WNA maximal adalah "+ $("#total-visitor").data('count_wna') +" orang", 0, 'alert-warning');
						$($("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement).find('input, select').val("");
					}else{
			
						var param="&filter=no_identity.eq('"+this.value+"')";
						$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
						ajaxGET(path_purchase + '/history_visitor'+"/"+this.value+"?prefix_entity=view_&limit=1000000"+param,'onCheckVisitorSuccess','onGetListError', {"params" : index});
					}
				}
		}else{
			showAlertMessage('Mohon maaf anda tidak dapat menggunakan identitas <strong>'+no_iden+'</strong>, 2x dalam 1 form. ', 0, 'alert-warning');
			//pass=false;
			//$('[name=no_identity]')[index].value="";
			//$("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement.parentElement.reset();
			$($("[name=no_identity]")[index].parentElement.parentElement.parentElement.parentElement).find('input, select').val("");
		}
	});
	
	
	$('.single-date-picker').daterangepicker({
	    singleDatePicker: true,
		autoUpdateInput: false,
        minDate:moment().add(1,'days'),
        maxDate:moment().add(30,'days'),
	    locale: {
	            format: 'DD-MM-YYYY',
	            cancelLabel: 'Clear',
		        "customRangeLabel": "Custom",
		        "weekLabel": "W",
		        "daysOfWeek": [
		            "Mg",
		            "Sn",
		            "Se",
		            "Ra",
		            "Ka",
		            "Ju",
		            "Sa"
		        ]
	    }
		/*showWeekNumbers:true,
		beforeShowDay: function(date){
			alert(1);
			var holidays = ["25-10-2021","28-10-2021"];
		    var datestring = jQuery.datepicker.formatDate('DD-MM-YYYY', date);
		    return [ holidays.indexOf(datestring) == -1 ]
		}*/
	});
	
	$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('DD-MM-YYYY'));
		$(this).val(picker.endDate.format('DD-MM-YYYY'));
		picker.autoUpdateInput = true;
		$(this).change();
		
	});
	$('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
	    $(this).val('');
	});
	
	
		$('.single-date-picker').on('click', function(){
			if(DISABLED_BOOKING){
				$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
				  //console.log(picker.startDate.format('YYYY-MM-DD'));
				  //console.log(picker.endDate.format('YYYY-MM-DD'));
					$('.single-date-picker').val('');
					//$('.single-date-picker').prop('readonly',true);
					alert("Mohon Maaf, saat ini sistem sedang dinonaktifkan.");
				});
			}
		});
	
	
	$(function (){
		var $popover = $('.example-popover>.form-row>.trigger').popover({
			//trigger: 'focus',
			html: true,
			placement: 'bottom',
			content: function(){
			var price_wni=getPrice('WNI');//(parseInt($('[name=count_ticket]').data('price_wni'))+getWeekendPrice(weekend_price));
			var price_wna=getPrice('WNA');//(parseInt($('[name=count_ticket]').data('price_wna'))+getWeekendPrice(weekend_wna_price));
			var content = '		                <div class="form-group">'+
			'		                        		<label class="control-label col-md-12"><Strong>WNI</Strong><br/> <i>Price : IDR'+price_wni+'</i></label>'+
			'										<div class="row">'+
			'										<div class="col-md-6">'+
				'		                        		<div class="input-group">'+
				'								            <div class="input-group-prepend">'+
				'								                <button class="btn btn-outline-danger btn-minus demise" type="button">-</button>'+
				'								            </div>'+
				'								            <input id="count-wni" type="number" class="form-control form-control-sm demise" data-price="'+$('[name=count_ticket]').data('price_wni')+'" placeholder="Number of travelers/visitors" value="'+$('[name=count_ticket]').data('count_ticket_wni')+'" min="0" max="'+MAX_PERSON_BOOKING+'">'+
				'								            <div class="input-group-append">'+
				'								                <button class="btn btn-outline-success btn-plus demise" type="button">+</button>'+
				'								            </div>'+
				'								        </div>'+
			'								        </div>'+
			'										<div class="col-md-6 align-self-center">'+
				'		                        		<label class="control-label">IDR <i id="label_price_wni" class="font-weight-bold"> '+($('[name=count_ticket]').data('count_ticket_wni')*getPrice('WNI'))+'</i></label>'+
			'								        </div>'+
			'								        </div>'+
			'		                        	</div>'+
			'		                        	<div class="form-group">'+
			'		                        		<label class="control-label col-md-12"><Strong>WNA</Strong><br/> <i>Price : IDR'+price_wna+'</i></label>'+
			'										<div class="row">'+
			'										<div class="col-md-6">'+
			'		                        		<div class="input-group">'+
			'								            <div class="input-group-prepend">'+
			'								                <button class="btn btn-outline-danger btn-minus demise" type="button">-</button>'+
			'								            </div>'+
			'								            <input id="count-wna" type="number" class="form-control form-control-sm demise" data-price="'+$('[name=count_ticket]').data('price_wna')+'" placeholder="Number of travelers/visitors" value="'+$('[name=count_ticket]').data('count_ticket_wna')+'" min="0" max="10">'+
			'								            <div class="input-group-append">'+
			'								                <button class="btn btn-outline-success btn-plus demise" type="button">+</button>'+
			'								            </div>'+
			'								        </div>'+
			'								        </div>'+
			'										<div class="col-md-6 align-self-center">'+
				'		                        		<label class="control-label">IDR <i id="label_price_wna" class="font-weight-bold"> '+($('[name=count_ticket]').data('count_ticket_wna')*getPrice('WNA'))+'</i></label>'+
			'								        </div>'+
			'								        </div>'+
			'		                        	</div>'+
			'		                        	<hr/>'+
			'		                        	<div class="alert alert-primary mt-3">'+
			'				                    	<!-- <p id="price-tiket" data-price="7000" data-price-def="7000">Price : IDR7,000.00/Days</p>'+
			'				                    	<hr> -->'+
			'				                    	<div class="font-weight-bold">IDR <span id="sum-price-tiket">'+($.number( $('#price-tiket').data('price') ,0))+'</span></div>'+
			'				                    	<!-- <p id="total-days-tiket">0 hari 0 malam (0D0N) </p> -->'+
			'				                    </div>';
	
				return content;//$(this).parent().find('.content').html();
			}
		});
			
		if($('#start_date').val()!="" && $('#end_date').val()!=""){
			$('.example-popover>.form-row>.trigger').popover('hide');
		}else $('.example-popover>.form-row>.trigger').show();
		
		//open
		$('.example-popover>.form-row>.trigger').click(function(e){
			e.stopPropagation();
			if($('#start_date').val()=="" && $('#end_date').val()=="") {
				$('.example-popover>.form-row>.trigger').popover('hide');
				alert("Tanggal Pendakian belum dipilih.");
			}else setPrice();
		});
		
		//close
		$(document).not(document.getElementsByClassName( "button-plus button-minus" )).click(function(e){
			if(!$(e.target).is('.demise, .popover')){
				$('.example-popover>.form-row>.trigger').popover('hide');
			}
		});
		
		
		$('.example-popover>.form-row>.trigger').on('shown.bs.popover', function () {
			$('.btn-plus, .btn-minus').on('click', function(e) {	
			  	const isNegative = $(e.target).closest('.btn-minus').is('.btn-minus');
			  	const input = $(e.target).closest('.input-group').find('input');
			  	if (input.is('input')) {
					var total=0;
					$.each($('.popover-body').find('input'), function(){
						console.log(this.value);
						total+=parseInt(this.value);
					});
					console.log(total);
					console.log(input);
					console.log(isNegative);
					if(total>=MAX_PERSON_BOOKING && !isNegative) return alert('mohon maaf, maximal pendaki adalah '+MAX_PERSON_BOOKING);
					else {
						input[0][isNegative ? 'stepDown' : 'stepUp']();
						setPrice();
					}
			  	}
			})
		})
		//$('.example-popover>.form-row>.trigger').show();
	})

	
}

window.addEventListener("load", function(){
    //everything is fully loaded, don't use me if you can use DOMContentLoaded
	$.LoadingOverlay("hide");
	if(document.getElementById('btn-check')!=null) document.getElementById('btn-check').setAttribute("class",  "text-small small example-popover");
	for(var i = 0; i < document.getElementsByTagName('input').length; i++) {
    	document.getElementsByTagName('input')[i].disabled=false;
	}
	
    document.getElementsByTagName('button').disabled=false;
});

var holidays = { // keys are formatted as month,week,day
    "0,2,1": "Martin Luther King, Jr. Day",
    "1,2,1": "President's Day",
    "2,1,0": "Daylight Savings Time Begins",
    "3,3,3": "Administrative Assistants Day",
    "4,1,0": "Mother's Day",
    "4,-1,1": "Memorial Day",
    "5,2,0": "Father's Day",
    "6,2,0": "Parents Day",
    "8,0,1": "Labor Day",
    "8,1,0": "Grandparents Day",
    "8,-1,0": "Gold Star Mothers Day",
    "9,1,1": "Columbus Day",
    "10,0,0": "Daylight Savings Time Ends",
    "10,3,4": "Thanksgiving Day"
};
function getDate(year, month, week, day) {
    var firstDay = 1;
    if (week < 0) {
        month++;
        firstDay--;
    }
    var date = new Date(year, month, (week * 7) + firstDay);
    if (day < date.getDay()) {
        day += 7;
    }
    date.setDate(date.getDate() - date.getDay() + day);
    return date;
}
function getHoliday(month, week, day) {
    return holidays[month + "," + week + "," + day];
}
function getDateString(year, month, week, day) {
    var date = getDate(year, month, week, day);
    var holiday = getHoliday(month, week, day);
    var dateString = date.toLocaleDateString();
    if (holiday) {
        dateString += " \xa0\xa0\xa0" + holiday;
    }
    return dateString;
}

function onCheckVisitorSuccess(response, obj){
	$.LoadingOverlay("hide");
	console.log(obj);
	var index = obj.params;
	
	$('[name=phone_code]')[index].value="+62";
	
	var value = response.data;
	
	if(response.data!=null && value.status_valid != "BANNED"){
		$('[name=full_name]')[index].value=value.full_name;
		$('[name=type_identity]')[index].value=value.type_identity;
		$('[name=file_identity]')[index].disabled= true;
		$('[name=address]')[index].value=value.address;
		
		$('[name=gender]')[index].value=value.gender;
		$('[name=birthdate]')[index].value=moment(value.birthdate, 'YYYY-MM-DD').add(1, 'day').format("YYYY-MM-DD");
		
		$('[name=email]')[index].value=value.email;
		if(value.phone_number!=null) $('[name=phone_number]')[index].value=value.phone_number.toString().substring(3, value.phone_number.toString().length);
		$($('[name=type_identity]')[index]).trigger('change');
		
	}else{
		$('[name=full_name]')[index].value="";
		//$('[name=type_identity]')[index].value="";
		$('[name=file_identity]')[index].disabled= false;
		$('[name=phone_number]')[index].value="";
		$('[name=email]')[index].value="";
		$('[name=address]')[index].value="";
		if(value.status_valid == "BANNED") {
			$('[name=birthdate]')[index].value="";
			$('[name=gender]')[index].value="";
			$('[name=type_identity]')[index].value="";
			$('[name=no_identity]')[index].value="";
			showAlertMessage("Identitas yang digunakan sedang dibanned, karena dianggap melanggar ketentuan, silahkan hubungi admin untuk info lebih lanjut. ", 0, 'alert-warning');
			return; 
			//alert("Identitas yang digunakan sedang dibanned, karena dianggap melanggar ketentuan, silahkan hubungi admin untuk info lebih lanjut. ");
		}
	}
		
}

function onGetListError(response){
	console.log();
	$.LoadingOverlay("hide");
	if(response.status==401){
		console.log("Belum login..");
	}else{
		alert("Please Try Again, connection error.");
	}
}	

function clearForm(){
	$('#entry-form')[0].reset();
	//
	$('#modal-form-msg').hide();
}

function s_div(val_html='', style=''){
	val_html='<div class='+val_html+' style='+style+'>';
	return val_html;
}
function e_div(){
	return '</div>';
}

function save(do_action=0){
//	var obj = new FormData(document.querySelector('#entry-form-popover'));
	if(selected_id != '' && selected_step !='') {
		obj = new FormData();
		obj.append('id', selected_id);
		obj.append('region_end_id', $('.row-region-turun.bg-primary')[0].dataset.id);
		obj.append('region_end_name', $('.row-region-turun.bg-primary')[0].dataset.nameRegion);
		//obj.append('vehicle_id', getDataArr().vehicle);
		//obj.append('tour_guide_id', getDataArr().guide);
		obj.append('amount_ticket', parseInt(total_price));
		obj.append('amount_total_ticket', parseInt(total_price));
		obj.append('detil_visitor', JSON.stringify(getDataArr().person));
		//start wajib untuk reschedule
		obj.append('detil_transit_camp', JSON.stringify(getDataArr().transit));
		obj.append('detil_luggage', JSON.stringify(getDataArr().luggage));
		//end wajib untuk reschedule
		obj.append('other_luggage', $('[name=other_luggage]').val());
		
		obj.append('detil_vehicle', JSON.stringify(getDataArr().vehicle));
		obj.append('detil_guide', JSON.stringify(getDataArr().guide));
		jQuery.each(jQuery('[name=file_identity]'), function(i, file) {
			if(file.files.length>0) obj.append('file_identity', file.files[0]);
			else obj.append('file_identity', new File([], "NONE"));
		});
	}else{
		var obj = new FormData(document.querySelector('#entry-form-popover'));
		obj.append('amount_total_ticket', total_price);
		obj.append('file_identity', []);
		obj.append('region_id', selected_id);
		obj.append('region_name', $('#label_region_name').text());
		obj.append('count_ticket_wni', $('[name=count_ticket]').data('count_ticket_wni'));
		obj.append('count_ticket_wna', $('[name=count_ticket]').data('count_ticket_wna'));
		
	}
	if(selected_id != '' && selected_step != ''){
		if(do_action==0){
			var msg = "<div><strong>Are your booking details correct?</strong></div>";
			msg+="<div class='mt-1'>You will not be able to change your booking details once you proceed to payment. Do you want to continue?</div>"
			$('#modal-confirm-msg').html(msg);
			$('#modal-confirm').modal('show');
	
			$('#modal-confirm').find('.btn-secondary').text("Check Again");
			$('#modal-confirm').find('.btn-primary').text("Yes, Continue to payment");
			return false;
		}
	}
	/*if(work_time(new Date()) || is_weekend_agenda(list_date) || is_weekend(moment(new Date(),'DD/MM/YYYY').format("MMM DD YYYY"))){
		
		Swal.fire({
		  icon: 'warning',
		  //title: 'Info Price Pendakian',
		  html: '<p>Mohon maaf, anda belum dapat melakukan booking online pada luar jam layanan kami. Silahkan coba kembali pada pukul 08:00 - 17:00, Terima Kasih</p>',
		  allowOutsideClick: false,
		  //showConfirmButton: false,
		})
		
	}else{
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	    ajaxPOST(path_purchase + '/save',obj,'onModalActionSuccess','onModalActionError');
	}*/
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxPOST(path_purchase + '/save',obj,'onModalActionSuccess','onModalActionError');
}

function onModalActionSuccess(resp){
	console.log(resp);
	if(selected_id != '' && selected_step != ''){
		window.location.href='payment?id='+resp.data.id+'&step=1';
	}else{
		window.location.href='booking?id='+resp.data.id+'&step=1';
	}
	
}
function onModalActionError(response){
	
	$.LoadingOverlay("hide");
	console.log(response);
	console.log();
	if(response.status==401){
		showAlertMessage(response.responseJSON.error+", Silahkan Login lebih dahulu.", 5500, true);
	}else{
		showAlertMessage(response.responseJSON.message, 5500, true);
	}
}

function setDataTemp(param){
	console.log(param);
	var temp_arr=[];
	
	var result = { };
	$.each($('#form-travel-detil').serializeArray(), function() {
	    result[this.name] = this.value;
	});
	console.log(result);
	
	new_obj = {}

	$.each($(form).serializeArray(), function(i, obj) { 
		new_obj[obj.name] = obj.value 
	})
	
	//if param set detail vehicle
	//$('#pills-book-2')
}

function findGetValue(e, val){
	return $(e).find('[name='+val+']').val();
}


function getDataArr(){
	var obj = {
		personTemp: [],
		person: [],
		vehicle: [],
		guide: [],
		ticket: [],
		transit: {}, //form-transit-camp-detil
		luggage: [],
	}

	$.each($("[data-key]"), function(key){
		if(findGetValue(this, 'no_identity')!="" && findGetValue(this, 'full_name')!=""
		 && findGetValue(this, 'type_identity')!="" && findGetValue(this, 'birthdate')!=""
			&& findGetValue(this, 'gender')!="" && findGetValue(this, 'phone_number')!=""
			&& ((findGetValue(this, 'email')!="" && key==0) || key>0) && findGetValue(this, 'phone_number_family')!=""
			 && findGetValue(this, 'address')!="" && findGetValue(this, 'address_family')!=""
		){
			person = {
				type_identity: "",
				no_identity: "",
				file_identity: "",
				full_name: "",
				phone_number: "",
				email: "",
				responsible: 0,
				gender: "",
				birthdate: "",
				phone_number_family: "",
				address_family: "",
				address: "",
				amount_price: 0
			}
			person['type_identity'] = $(this).find('[name=type_identity]').val();
			person['file_identity'] = $(this).find('[name=file_identity]')[0].files;
			person['no_identity'] = $(this).find('[name=no_identity]').val();
			person['full_name'] = $(this).find('[name=full_name]').val();
			person['phone_number'] = $(this).find('[name=phone_code]').val()+$(this).find('[name=phone_number]').val();
			person['email'] = $(this).find('[name=email]').val();
			person['phone_number_family'] = $(this).find('[name=phone_number_family]').val();
			person['address_family'] = $(this).find('[name=address_family]').val();
			person['address'] = $(this).find('[name=address]').val();
			if(key==0) person['responsible'] = 1;
			person['gender'] = $(this).find('[name=gender]').val();
			person['birthdate'] = $(this).find('[name=birthdate]').val();
			person['amount_price'] = $(this).find('[data-price]').data('price');
			obj['person'].push(person);
		}
		
		//
		
			personTemp = {
				type_identity: "",
				responsible: 0,
			}
			personTemp['type_identity'] = $(this).find('[name=type_identity]').val();
			if(key==0) personTemp['responsible'] = 1;
			obj['personTemp'].push(personTemp);
			
	});
		
	$.each($("[data-luggage]"), function(key){
		luggage = {
			qty_luggage: "",
			name_luggage: "",
			type_luggage: "",
			desc_luggage: "",
		}
		luggage['qty_luggage'] = $(this).find('[name=qty_luggage]').val();
		luggage['name_luggage'] = $(this).find('[name=name_luggage]').val();
		luggage['type_luggage'] = $(this).find('[name=type_luggage]').val();
		luggage['desc_luggage'] = $(this).find('[name=desc_luggage]').val();
	
		obj['luggage'].push(luggage);
    });
	//obj['luggage'].push({other_luggage: $('[name=other_luggage]').val()});
	//
	var all_obj_vehicle = $('.vehicle.border-primary');//$('[data-id_vehicle]');
	$.each(all_obj_vehicle, function(key){
		vehicle = {
			vehicle_id: "",
			qty_vehicle: 0,
		}
		console.log($(this).data('id_vehicle'));
		vehicle['vehicle_id'] = $(this).data('id_vehicle');
		vehicle['qty_vehicle'] = $(this).find('[name=qty_vehicle]').val();
		obj['vehicle'].push(vehicle);
	});
	//
	var all_obj_guide = $('.tour-guide.shadow-lg');
	$.each(all_obj_guide, function(key){
		guide = {
			guide_id: "",
		}
		console.log($(this).data('id_vehicle'));
		guide['guide_id'] = $(this).data('id_tour_guide');
		obj['guide'].push(guide);
	});
	obj['transit']={
		id_transit_camp : $('[name=id_master_transit]').val(),
		name_transit_camp : $('[name=id_master_transit] option:selected').text(),
		transit_camp : ($('.row-rtc.bg-primary')[0]!=undefined?$('.row-rtc.bg-primary')[0].cells[1].innerHTML:""),
		start_transit_camp : $('[name=id_master_transit]').data("start"),
		end_transit_camp : $('[name=id_master_transit]').data("end"),
		type_camp : $('[name=type_camp]').val(),
		count_camp : $('[name=count_camp]').val(),
		code_unique : ($('[name=code_unique]').val()!=null?$('[name=code_unique]').val().replace("\n",""):"")
		//$('[name=code_unique]').val(),
	};
	//obj['vehicle'] = $('#pills-tabContent').find('.border-primary').data('id_vehicle');
//	obj['guide'] = $('#pills-book-3').find('.shadow-lg').data('id_tour_guide');
	return obj;
}

(function($,undefined){
	  '$:nomunge'; // Used by YUI compressor.

	  $.fn.serializeObject = function(){
	    var obj = {};

	    $.each( this.serializeArray(), function(i,o){
	      var n = o.name,
	        v = o.value;

	        obj[n] = obj[n] === undefined ? v : $.isArray( obj[n] ) ? obj[n].concat( v ) : [ obj[n], v ];
	    });

	    return obj;
	  };

	})(jQuery);

function renderTransitCamp(){
	var tbody=$('#container-transit-camp');
	var html ='';
	var value=getDataArr().transit
	
	var html = '<div class="card mb-3">'+
	'   <div class="card-header">'+
	'      <div class="row align-items-center">'+
	'         <div class="col">'+
	'            <div class="mb-0">Jalur Turun</div>'+
	'         </div>'+
	'         <div class="col-auto">'+
	'            <a class="d-none btn btn-falcon-default btn-sm" href="#!">'+
	'               <svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">'+
	'                  <path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path>'+
	'               </svg>'+
	'               <!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details'+
	'            </a>'+
	'         </div>'+
	'      </div>'+
	'   </div>'+
	'   <div class="card-body bg-light border-top">'+
	'      <div class="row">'+
	'         <div class="col-lg col-xxl-5 mt-0 mt-lg-0 offset-xxl-1">'+
	'            <div class="row d-none">'+
	'               <div class="col-5 col-sm-4">'+
	'                  <p class="font-weight-semi-bold mb-0">Tempat Transit</p>'+
	'               </div>'+
	'               <div class="col">'+
	'                  <p class="font-weight-semi-bold mb-0">: '+value.name_transit_camp+'</p>'+
	'               </div>'+
	'            </div>'+
	'            <div class="row d-none">'+
	'               <div class="col-5 col-sm-4">'+
	'                  <p class="font-weight-semi-bold mb-0">Jam wajib lapor di BC</p>'+
	'               </div>'+
	'               <div class="col">'+
	'                  <p class="font-weight-semi-bold mb-0">: '+value.transit_camp+'</p>'+
	'               </div>'+
	'            </div>'+
	'            <div class="row d-none">'+
	'               <div class="col-5 col-sm-4">'+
	'                  <p class="font-weight-semi-bold mb-1">Jumlah Tenda</p>'+
	'               </div>'+
	'               <div class="col">'+
	'                  <p class="font-weight-semi-bold mb-0">: '+value.count_camp+'</p>'+
	'               </div>'+
	'            </div>'+
	'            <div class="row  d-none">'+
	'               <div class="col-5 col-sm-4">'+
	'                  <p class="font-weight-semi-bold mb-1">Nomor Kapling</p>'+
	'               </div>'+
	'               <div class="col"><a href="javascript:void(0)">: '+value.code_unique+'</a></div>'+
	'            </div>'+
	'            <div class="row">'+
	'               <div class="col-5 col-sm-4">'+
	'                  <p class="font-weight-semi-bold mb-1">Jalur Turun</p>'+
	'               </div>'+
	'               <div class="col"><a href="javascript:void(0)">: '+$('.row-region-turun.bg-primary')[0].dataset.nameRegion+'</a></div>'+
	'            </div>'+
	'         </div>'+
	'      </div>'+
	'   </div>'+
	'</div>';
	

	tbody.html(html);
}

function renderPerson(){
	var tbody=$('#container-person');
	var tbody2=$('#container-person-contact');
	var html ='';
	var html2 ='';
	var data=getDataArr();
	var idx=0;
	$.each(data.person, function(key){
	if(this.responsible==0){
	html += '<div class="card mb-3">'+
	'	            <div class="card-header">'+
	'	              <div class="row align-items-center">'+
	'	                <div class="col">'+
	'	                  <div class="mb-0">Person '+(key)+'</div>'+
	'	                </div>'+
	'	                <div class="col-auto"><a class="d-none btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>'+
	'	              </div>'+
	'	            </div>'+
	'	            <div class="card-body bg-light border-top">'+
	'				<div class="row">'+
	'				<div class="col-lg col-xxl-5 mt-0 mt-lg-0 offset-xxl-1">'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-0">Identity</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<p class="font-weight-semi-bold mb-0">'+this.type_identity+'('+this.no_identity+')</p>'+
	'						</div>'+
	'					</div>'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-0">Full Name</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<p class="font-weight-semi-bold mb-0">'+this.full_name+'</p>'+
	'						</div>'+
	'					</div>'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-1">Phone number</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<p class="font-weight-semi-bold mb-0">'+this.phone_number+'</p>'+
	'						</div>'+
	'					</div>'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-1">Email</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<a href="mailto:'+this.email+'">'+this.email+'</a>'+
	'						</div>'+
	'					</div>'+
	'				</div>'+
	'				</div>'+
	'	            </div>'+
	'	          </div>';
	}else{
		idx=0;
		html2 += '<div class="card mb-3">'+
		'	            <div class="card-header">'+
		'	              <div class="row align-items-center">'+
		'	                <div class="col">'+
		'	                  <div class="mb-0">Person in charge '+""+'</div>'+
		'	                </div>'+
		'	                <div class="col-auto"><a class="d-none btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>'+
		'	              </div>'+
		'	            </div>'+
		'	            <div class="card-body bg-light border-top">'+
		'				<div class="row">'+
		'				<div class="col-lg col-xxl-5 mt-0 mt-lg-0 offset-xxl-1">'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-0">Identity</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.type_identity+'('+this.no_identity+')</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-0">Full Name</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.full_name+'</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-1">Phone number</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.phone_number+'</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-1">Email</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<a href="mailto:tony@gmail.com">'+this.email+'</a>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-1">Family Number</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.phone_number_family+'</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-1">Family Address</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.address_family+'</p>'+
		'						</div>'+
		'					</div>'+
		'				</div>'+
		'				</div>'+
		'	            </div>'+
		'	          </div>';
//		}
		idx++;
	}
		});
//	});
	tbody.html(html);
	tbody2.html(html2);
}

function renderTourGuide(){
	var path=ctx + '/ticket/tour-guide';
	var id= $('.tour-guide.shadow-lg').data('id_tour_guide');
//	ajaxGET(path + '/'+id+'?action='+selected_action,'onRenderTourGuide','onActionError');
	ajaxGET(path + '/list','onRenderTourGuide','onActionError');
}
function onRenderTourGuide(resp){
	console.log(resp);
//	var value=resp.data;
//	$('#name_tour_guide').text(value.full_name);
//	$('#cost_tour_guide').text(value.cost_per_day);
//	$('#phone_tour_guide').text(value.phone_number);
//	$('#email_tour_guide').text(value.email);
//	renderPhoto();
	//
	var tbody=$('#container-guide');
	var html ='';
	var num=1;
	$.each(getDataArr().guide, function(key, temp_value){
		$.each(resp.data, function(key, value){
		if(temp_value.guide_id==value.id){
			total_price+=value.cost_per_day;
		html += '<div class="card mb-3">'+
		'    <div class="card-header">'+
		'        <div class="row align-items-center">'+
		'            <div class="col">'+
		'                <div class="mb-0">Tour Guide '+(num)+'</div>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'    <div class="card-body bg-light border-top">'+
		'        <div class="row no-gutters">'+
		'            <div class="col-auto">'+
		'                <img id="photo_tour_guide" src="'+value.photo+'" class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" width="150">'+
		'            </div>'+
		'            <div class="col-md-8">'+
		'                <div class="card-body">'+
		'                    <div class="card-title" id="name_tour_guide">'+value.full_name+'</div>'+
		'                    <strong class="card-text text-success"><span id="cost_tour_guide">IDR '+$.number(value.cost_per_day)+'</span><span>/Day</span></strong>'+
		'                    <div class="row card-text">'+
		'                        <div class="col-auto"><small class="text-muted">Phone : <span id="phone_tour_guide">'+value.phone_number+'</span></small></div>'+
		'                        <div class="col-auto"><small class="text-muted">Email : <span id="email_tour_guide">'+value.email+'</span></small></div>'+
		'                    </div>'+
		'                    <div class="row card-text">'+
		'                        <div class="col-auto"><small class="text-muted">Language : <span id="language_tour_guide">-</span></small></div>'+
		'                    </div>'+
		'                </div>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'</div>';
		num++;
		}
		});
	});
	tbody.html(html);
	$('#total-price-pay').text($.number(total_price));
}
function renderPhoto(){
	var path=ctx + '/files';
	ajaxGET(path + '/'+'list','onRenderPhoto','onActionError');
}
function onRenderPhoto(resp){
	var path='';
	var id_guide= $('.tour-guide.shadow-lg').data('id_tour_guide');
	var id_vehicle= $('.vehicle.border-primary').data('id_vehicle');
	console.log(resp);
	$.each(resp.data, function(key){
		if(this.reference_id==id_guide){
			path=ctx+"/files/"+this.id+"?filename="+this.name+"&download";
			$('#photo_tour_guide').attr('src', path);
		}
		if(this.reference_id==id_vehicle){
			path=ctx+"/files/"+this.id+"?filename="+this.name+"&download";
			$('#vehicle_photo').attr('src', path);
		}
	});
	return path;
}
function renderVehicle(){
	var path=ctx + '/ticket/vehicle';
//	var id= $('.vehicle.border-primary').data('id_vehicle');
	ajaxGET(path + '/list','onRenderVehicle','onActionError');
}
function onRenderVehicle(resp){
	console.log(resp);
//	var value=resp.data;
//	$('#vehicle_name').text(value.name);
//	$('#vehicle_cost').text(value.cost);
//	$('#vehicle_seat').text(value.seat_capacity);
//	$('#vehicle_suitcase').text(value.suitcase);
	//
	var tbody=$('#container-vehicle');
	var html ='';
	var num=1;
	$.each(getDataArr().vehicle, function(key, temp_value){
		$.each(resp.data, function(key, value){
		if(temp_value.vehicle_id==value.id){
			total_price+=value.cost*temp_value.qty_vehicle;
		html += '<div class="card mb-3">'+
		'           <div class="card-header">'+
		'             <div class="row align-items-center">'+
		'               <div class="col">'+
		'                 <div class="mb-0">Vehicle '+(num)+'</div>'+
		'               </div>'+
		'             </div>'+
		'           </div>'+
		'           <div class="card-body bg-light border-top">'+
		'            <div class="row no-gutters">'+
		'		    <div class="col-md-4">'+
		'		      <img id="vehicle_photo" src="'+value.photo+'" class="card-img" alt="...">'+
		'		    </div>'+
		'		    <div class="col-md-8">'+
		'		      <div class="card-body">'+
		'		        <div class="card-title" id="vehicle_name">'+value.name+'</div>'+
		'		        <strong class="card-text text-success"><span id="vehicle_cost">IDR '+$.number(value.cost)+'</span>/Day</strong>'+
		'		        <div class="row card-text">'+
		'					<div class="col-auto"><small class="text-muted">Seat : <span id="vehicle_seat">'+value.seat_capacity+'</span></small></div>'+
		'					<div class="col-auto"><small class="text-muted">Suitcase : <span id="vehicle_suitcase">'+value.suitcase+'</span></small></div>'+
		'		      	</div>'+
		'		        <div class="row card-text">'+
		'					<div class="col-auto"><small class="text-muted">QTY : <span id="vehicle_qty">'+temp_value.qty_vehicle+'</span></small></div>'+
		'		      	</div>'+
		'		    </div>'+
		'		  	</div>'+
		'           </div>'+
		'           </div>'+
		'           </div>';
		num++;
		}
		});
	});
	tbody.html(html);
	$('#total-price-pay').text($.number(total_price));
}
function onActionError(){
	alert("ERR");
}

function checkKuota(){
	$('#btn-booking').prop('disabled', true);
	if(parseInt($('[name=count_ticket]').val()) > parseInt($('[name=count_ticket]')[0].max)){
		alert("maximal pendaki 10");
		$('[name=count_ticket]').val(10);
	}else if(parseInt($('[name=count_ticket]').val()) < 4){
		alert("minimal 4 pedaki");
		$('[name=count_ticket]').val(4);
	}
}

function getWeekendPrice(weekend_price, all){
	var new_price=0;
	var start = moment($('#start_date').val(), 'DD/MM/YYYY');//.subtract(1, 'd')._d;
	var end = moment($('#end_date').val(), 'DD/MM/YYYY');//.subtract(1, 'd')._d;
	var loop = new Date(start);
	
	while(loop <= end){
		//cek sabtu/minggu
	   	if(is_weekend(moment(loop,'DD/MM/YYYY').format("MMM DD YYYY"))){
			new_price=parseInt(weekend_price);
	    }else{
			//cek agenda 
			$.each(list_date, function(){
				if(loop.getTime()==this.getTime()){
			    	new_price=parseInt(weekend_price);
				}
			});
		}
	
	   	var newDate = loop.setDate(loop.getDate() + 1);
	   	loop = new Date(newDate);
	}
	
	if(all) new_price=parseInt($('#price-tiket').data('price'))+parseInt(new_price);
	
	return new_price;
}

function setPrice(e){
	if($('#start_date').val()=="" || $('#end_date').val()==""){
		alert("Tanggal Pendakian belum dipilih.");
	}else{
		//set value for form popover
		$('#label_price_wni').text($('#count-wni').val()*(getPrice('WNI')));
		var price_wni=$('#count-wni').val()*(getPrice('WNI'));
		
		$('#label_price_wna').text($('#count-wna').val()*(getPrice('WNA')));
		var price_wna=$('#count-wna').val()*(getPrice('WNA'));
		
		//not
		
		$('#sum-price-tiket').text($.number( parseInt(price_wni)+parseInt(price_wna) ,0)+"");
		$('#price-tiket').text("IDR "+$.number( parseInt(price_wni)+parseInt(price_wna) ,0)+"");
		$('#price-tiket').data('price', parseInt(price_wni)+parseInt(price_wna));
		//end
		
		$('[name=count_ticket]').val($('#count-wni').val() +" WNI, "+$('#count-wna').val()+" WNA");
		$('[name=count_ticket]').data('count_ticket_wni', $('#count-wni').val());
		$('[name=count_ticket]').data('count_ticket_wna', $('#count-wna').val())
	
		var day = $("#total-price-tiket").data("maxDay");//moment.duration( moment(moment($('#end_date').val()).format("YYYY-DD-MM")).diff(moment(moment($('#start_date').val()).format("YYYY-DD-MM")))).asDays()+1;
		var start = moment($('#start_date').val(),'DD/MM/YYYY').format("DD-MM-YYYY");
		var end   = moment($('#end_date').val(),'DD/MM/YYYY').format("DD-MM-YYYY");
		var day = moment(end,'DD/MM/YYYY').diff(moment(start,'DD/MM/YYYY'), 'days')+1;
		
		var new_price=parseInt(price_wni)+parseInt(price_wna);
		//0;//parseInt($('#price-tiket').data('price'));//0;
		
		$('#price-tiket').data('priceDef', new_price);
		var night = $("#total-price-tiket").data("maxDay")-1;//moment.duration( moment(moment($('#end_date').val()).format("YYYY-DD-MM")).diff(moment(moment($('#start_date').val()).format("YYYY-DD-MM")))).asDays();
		
		total_price = parseInt(new_price);// * parseInt(day);
		
		//$('#total-price-tiket').html(($.number(new_price))+" x "+day+" ("+$.number(total_price)+")");
		$('#btn-booking').prop('disabled', false);
		
		$('#total-days-tiket').html(day+" hari "+night+" malam ("+day+"D"+night+"N)");
		
		//cek kuota
		var param="filter_start_date="+$('[name=start_date]').val();
		$("#sisa-kuota").LoadingOverlay("show");
		ajaxGET_NEW(path + '/checkKuota/'+selected_id+'?'+param, function(response){
			$("#sisa-kuota").LoadingOverlay("hide");
			console.log(response);
			$("#sisa-kuota").text(response.data.sisa_kuota);
		},'onErrCheckKuota');
	}
}

function onErrCheckKuota(response){
	console.log(response);
	$("#sisa-kuota").LoadingOverlay("hide");
	if(response.status==401){
		alert("Sistem mendeteksi anda belum melakukan login.");
	}
}

function createFormTent(small_tent, large_tent, code, e){
	//
	var photo = e.dataset.name_photo;
	var photo_id = e.dataset.id_photo;

	if(photo=="null" || photo_id=="null"){
	}else{
		$("#img-photo-tent").attr("src", path_upload+photo_id+"?filename="+photo);
	}
	//
	params = $('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	//params+="&filter_status_booking=DONE";
	params+="&filter_status=NOT_DRAFT"; 
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	ajaxGET(path_purchase + '/list?page='+1+'&'+params,'onGetListTestSuccess','onGetListError');
}

function onGetListTestSuccess(response){
	$.LoadingOverlay("hide");
	console.log(response);
	
	var row_small="";
	var row_large="";
	
	var small_tent = $('.row-rtc.bg-primary').data('smallTent');
	var large_tent = $('.row-rtc.bg-primary').data('largeTent');
	var code_tent = $('.row-rtc.bg-primary').data('codeTent');
	var name_tent = $('.row-rtc.bg-primary').data('nameTent');
	$('#title-choose-capling-tent').text(name_tent+"("+code_tent+"):Kecil(K)/Besar(B)");
	if(response.count > 0){
		var stringval="";
		var arrval=[];
		$.each(response.data, function(key, value){
			if(value.status_expired_date==0 && value.purchase_detil_transit_camp.length > 0){
				console.log(moment(value.end_date_serialize, "YYYY-MM-DD"));
				console.log("==");
				console.log(moment($("[data-end_date]").data('end_date'), "DD-MM-YYYY"));
				console.log(moment(value.end_date_serialize, "DD-MM-YYYY").isSame(moment($("[data-end_date]").data('end_date'), "DD-MM-YYYY")))
				
				if(moment(value.end_date_serialize, "YYYY-MM-DD").isSame(moment($("[data-end_date]").data('end_date'), "DD-MM-YYYY"))){
					stringval=value.purchase_detil_transit_camp[0].code_unique;
					if(stringval.split(",").length > 1){
						stringval=stringval.split(",");
						for(var i=0; i < stringval.length; i++){
							arrval.push(stringval[i]);
						}
					}else{
						arrval.push(stringval);
					}
				}
			}
		})

		console.log(arrval);
		
		
		for(var i=0; i < small_tent; i++){
			var count=0;
			for(var s=0; s < arrval.length; s++){
				console.log(code_tent+"K-"+(i+1)+"=="+arrval[s]);
				if(code_tent+"K-"+(i+1)==arrval[s]){
					count+=1;
				}
			}
			row_small +=renderRowTent(code_tent+"K-"+(i+1), "K", "btn-warning", count);
		}
			
		for(var i=0; i < large_tent; i++){
			var count=0;
			for(var s=0; s < arrval.length; s++){
				console.log(code_tent+"B-"+(i+1)+"=="+arrval[s]);
				if(code_tent+"B-"+(i+1)==arrval[s]){
					count+=1;
				}
			}
			row_large +=renderRowTent(code_tent+"B-"+(i+1), "B", "btn-primary", count);
		}
		
		
	}else{
		for(var i=0; i < small_tent; i++){
			row_small +=renderRowTent(code_tent+"K-"+(i+1), "K", "btn-warning");
		}
		
		for(var i=0; i < large_tent; i++){
			row_large +=renderRowTent(code_tent+"B-"+(i+1), "B", "btn-primary");
		}
	}
	
	$('#row-tenda-kecil').html(row_small);
	$('#row-tenda-besar').html(row_large);
}

function renderRowTent(value, size, color, count=0){
	var row = "";
	//row += '<button class="btn btn-success">'+code+" "+value+'</button>';
	
	row += '<button '+(count > 0?"disabled":"")+' data-size="'+size+'" onclick="cekStatusTenda(this);" class="text-small btn col-md-2 p-1 m-1 text-center '+(count > 0?"btn-danger":color)+'">'+(value.split("-")[0]+"<br/>-"+value.split("-")[1])+'</button>';
	//row += '<div class="border col-md-2 p-1 m-1 text-center bg-success">'+code+" "+value+'</div>';
	
	return row;
}

function cekStatusTenda(e, remove=false){
	var color= (e.dataset.size=="B"?"btn-primary":e.dataset.size=="K"?"btn-warning":"");
	if(remove==true){
		$(e).removeClass("btn-success").addClass(color);
		$(e).attr("onclick", "cekStatusTenda(this)");
	}else{
		var c=$("[data-count_ticket]").data('count_ticket');
		if(e.dataset.size=="B"){
			if( c == 5 || c == 6 || c == 9 || c == 10){
				if(c == 9 || c == 10){
					if($("#row-tenda-besar").find(".btn-success").length >= 1){
						alert("Mohon maaf anda hanya boleh memilih 1 tenda besar, maximal boleh di isi 6 pendaki. Atau apabila anda ingin mengubah kapling tenda yang lain, silahkan klik 1x pada kapling sebelumnya.");
					}else{
						$(e).removeClass("btn-primary").addClass("btn-success");
						$(e).attr("onclick", "cekStatusTenda(this, true)");
					}
				}else{
					if($("#row-tenda-besar").find(".btn-success").length == 0){
						$(e).removeClass("btn-primary").addClass("btn-success");
						$(e).attr("onclick", "cekStatusTenda(this, true)");
					}else{
						alert("Mohon maaf, maximal 6 pendaki, hanya boleh membawa 1 tenda besar. Atau apabila anda ingin mengubah kapling tenda yang lain, silahkan klik 1x pada kapling sebelumnya.");
					}
				}
				
				
			}else{
				alert("Mohon maaf, untuk dapat memilih tenda besar, hanya bisa dilakukan pada jumlah pendaki(6, 9, 10)");
			}
		}else if(e.dataset.size=="K"){
			if(c == 4 || c == 7 || c == 8 || c == 9 || c == 10 ){
				if(c == 9 || c == 10){
					if($("#row-tenda-kecil").find(".btn-success").length >= 1){
						alert("Mohon maaf anda hanya boleh memilih 1 tenda kecil, maximal boleh di isi 4 pendaki. Atau apabila anda ingin mengubah kapling tenda yang lain, silahkan klik 1x pada kapling sebelumnya.");
					}else{
						$(e).removeClass("btn-warning").addClass("btn-success");
						$(e).attr("onclick", "cekStatusTenda(this, true)");
					}
				}else{
					if(c == 7 || c == 8){
						if($("#row-tenda-kecil").find(".btn-success").length <= 1){
							$(e).removeClass("btn-warning").addClass("btn-success");
							$(e).attr("onclick", "cekStatusTenda(this, true)");
						}else{
							alert("Mohon maaf anda hanya boleh memilih 2 tenda kecil, maximal boleh di isi 7-8 pendaki. Atau apabila anda ingin mengubah kapling tenda yang lain, silahkan klik 1x pada kapling sebelumnya.");
						}
					}else{
						if($("#row-tenda-kecil").find(".btn-success").length == 0){
							$(e).removeClass("btn-warning").addClass("btn-success");
							$(e).attr("onclick", "cekStatusTenda(this, true)");
						}else{
							alert("Mohon maaf, maximal 4 pendaki, hanya boleh membawa 1 tenda kecil. Atau apabila anda ingin mengubah kapling tenda yang lain, silahkan klik 1x pada kapling sebelumnya.");
						}
					}
				}
			}else{
				alert("Mohon maaf, untuk dapat memilih tenda kecil, hanya bisa dilakukan pada jumlah pendaki(4, 7, 8)");
			}
		}
	}
	var kapling =[];
	try{
		if($("#row-tenda-kecil,#row-tenda-besar").find(".btn-success")[0] != undefined ){
			kapling.push($("#row-tenda-kecil,#row-tenda-besar").find(".btn-success")[0].innerText);
		}
		if($("#row-tenda-kecil,#row-tenda-besar").find(".btn-success")[1] != undefined ){
			kapling.push($("#row-tenda-kecil,#row-tenda-besar").find(".btn-success")[1].innerText);
		}
		$('[name=code_unique]').html(new Option(kapling, kapling))
		$('[name=count_camp]').val(kapling.length);
	}catch(e){
		console.log("it's okay");
		$('[name=code_unique]').html("");
		$('[name=count_camp]').val("");
	}
	
}
function setPendaki(){
	
}

function make_skeleton() {
    var output = '';
    for (var count = 0; count < 6; count++) {
        output += '<div class="col-4">';
        output += '<div class="ph-item">';
        output += '<div class="ph-col-12">';
        output += '<div class="ph-picture"></div>';
        output += '<div class="ph-row">';
        output += '<div class="ph-col-6 big"></div>';
        output += '<div class="ph-col-4 empty big"></div>';
        output += '<div class="ph-col-12"></div>'
        output += '<div class="ph-col-12"></div>'
        output += '</div>';
        output += '</div>';
        output += '</div>';
        output += '</div>';
    }
    return output;
}

function infoPrice(){
	var price_wni=(parseInt($('[name=count_ticket]').data('price_wni'))+getWeekendPrice(weekend_price));
	var price_wna=(parseInt($('[name=count_ticket]').data('price_wna'))+getWeekendPrice(weekend_wna_price));
	
	var row = '<div class="text-left">'+
	'   <p style="text-align: justify;"><strong>Tarif Tiket Masuk TNGHS Pendaki Dalam Negeri</strong></p>'+
	'   <div class="" style="text-align: justify;">'+
	'      <li>Hari Kerja <strong>Rp. '+$.number(parseInt($('[name=count_ticket]').data('price_wni')),0)+',-</strong> per orang per hari<br><span class="d-none">(terdiri dari karcis masuk Rp.10.000,- melakukan kegiatan di dalam kawasan Rp. 5 .000,- dan asuransi Rp. 4.000,-)  </span></li>'+
	'      <br>'+
	'      <li>Hari Libur <strong>Rp. '+$.number(parseInt($('[name=count_ticket]').data('price_wni'))+parseInt(weekend_price),0)+',-</strong> per orang per hari<br><span class="d-none">(terdiri dari karcis masuk Rp.15.000,- melakukan kegiatan di dalam kawasan Rp. 5 .000,- dan asuransi Rp. 4.000,-)</span></li>'+
	'   </div>'+
	'   <hr>'+
	'   <p style="text-align: justify;"><strong>Tarif Tiket Masuk TNGHS Pendaki Mancanegara</strong></p>'+
	'   <div class="checklist" style="text-align: justify;">'+
	'      <li>Hari Kerja <strong>Rp. '+$.number(parseInt($('[name=count_ticket]').data('price_wna')),0)+',- </strong>per orang per hari<br><span class="d-none">(terdiri dari karcis masuk Rp.200.000,- melakukan kegiatan di dalam kawasan Rp. 5 .000,- dan asuransi Rp. 5.000,-)</span></li>'+
	'      <br>'+
	'      <li>Hari Libur <strong>Rp. '+$.number(parseInt($('[name=count_ticket]').data('price_wna'))+parseInt(weekend_wna_price),0)+',-</strong> per orang per hari<br><span class="d-none">(terdiri dari karcis masuk Rp.300.000,- melakukan kegiatan di dalam kawasan Rp. 5 .000,- dan asuransi Rp. 5.000,-)</span></li>'+
	'   </div>'+
	'</div>';
		

	

	Swal.fire({
	  icon: 'info',
	  //title: 'Info Price Pendakian',
	  html: row,//"<div class='text-left'><p><strong>WNI</strong> : <span class='text-primary'>"+$('[name=count_ticket]').data('price_wni')+'</span> <i class="fas fa-arrow-right"></i> <span class="text-success">'+(parseInt($('[name=count_ticket]').data('price_wni'))+parseInt(weekend_price))+"</span></p> <p><strong>WNA</strong> : <span class='text-primary'>"+$('[name=count_ticket]').data('price_wna')+'</span> <i class="fas fa-arrow-right"></i> <span class="text-success">'+(parseInt($('[name=count_ticket]').data('price_wna'))+parseInt(weekend_wna_price))+"</span></p><hr/><p class='text-primary'>*WEEKDAYS</p><p class='text-success'>*WEEKENDS</p></di>",
	  footer: '<a href="javascript:void(0)" onclick="swal.close()">OK.</a>',
	  allowOutsideClick: false,
	  showConfirmButton: false,
	})
}


function getPrice(mode){
	var new_price=0;
	var start = moment($('#start_date').val(), 'DD/MM/YYYY');//.subtract(1, 'd')._d;
	var end = moment($('#end_date').val(), 'DD/MM/YYYY');//.subtract(1, 'd')._d;
	var loop = new Date(start);
	
	while(loop <= end){
		if(is_weekend(moment(loop,'DD/MM/YYYY').format("MMM DD YYYY"))){
			if(mode=="WNI") new_price+=parseInt($('[name=count_ticket]').data('price_wni')) + parseInt(weekend_price);
			else if(mode=="WNA") new_price+=parseInt($('[name=count_ticket]').data('price_wna')) +parseInt(weekend_wna_price);
	    }else{
			//cek agenda 
			var readDays=true;
			$.each(list_date, function(){
				if(loop.getTime()==this.getTime()){
					readDays=false;
					if(mode=="WNI") new_price+=parseInt($('[name=count_ticket]').data('price_wni')) + parseInt(weekend_price);
					else if(mode=="WNA") new_price+=parseInt($('[name=count_ticket]').data('price_wna')) +parseInt(weekend_wna_price);
				}
			});
			
			if(readDays) {
				if(mode=="WNI") new_price+=parseInt($('[name=count_ticket]').data('price_wni'));
				else if(mode=="WNA") new_price+=parseInt($('[name=count_ticket]').data('price_wna'));
			}
		}
	   	var newDate = loop.setDate(loop.getDate() + 1);
	   	loop = new Date(newDate);
	}
	
	//if(all) new_price=parseInt($('#price-tiket').data('price'))+parseInt(new_price);
	
	return new_price;
}