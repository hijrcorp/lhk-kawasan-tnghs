var pathsql = ctx + '/restql';
var path = ctx + '/ticket/purchase';
var path_upload = ctx + '/files/';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];
var selected_kode_booking = ($.urlParam('kode_booking') == null ? '' : $.urlParam('kode_booking'));
var role = position.toString().replaceAll("[","").replaceAll("]","").replaceAll(" ","").toString().split(",");

function init(){
	
	if(role.indexOf("ROLE_SUPER_ADMIN") !== -1) {
		$('[name=filter_status]').val(["VERIFYING", "WAITING"]).trigger('change');
	}else $('[name=filter_status]').val(["PAYMENT"]).trigger('change');
	
	display();

	ajaxGET_NEW(pathsql + '/region', function(response){
		$.each(response.data, function(key,value){ 
			$('[name=filter_id_region]').append(new Option(value.name, value.id));
		});
	},'onGetListError');
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-add-type').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearFormType();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' || selected_action == 'RESCHEDULE_ACTIVE' || selected_action == 'RESCHEDULE_NON_ACTIVE'){
			doAction(selected_id, selected_action, 1);
		}
		
	});
	$('.table-responsive').on('show.bs.dropdown', function () {
	     $('.table-responsive').css( "overflow", "inherit" );
	});

	$('.table-responsive').on('hide.bs.dropdown', function () {
	     $('.table-responsive').css( "overflow", "auto" );
	})
	
	$('#form-filter').submit(function(e){
		display();
		//$('#a-export').attr('href','export-data?'+$('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, ''));
		e.preventDefault();		
	});
	$('#btn-search').click(function(){
		var page=1;
		var params="";
		$('#more-button-row').remove();
		$('#retry-button-row').remove();
		
		var tbody = $("#tbl-data").find('tbody');
		
		// disable kondisi ini kalo pake metode refresh semua isi table
		if(params == '' || page ==1){
			tbody.text('');
		}
		
		tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
		
		//params = $('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
		
		if($('[name=filter_keyword]').val()!='') params+="&filter_no_booking="+$('[name=filter_keyword]').val();
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		ajaxGET(path + '/list?page='+page+'&'+params+"&order=start_date_purchase",'onGetListSuccess','onGetListError');
	});

    $(function() {

        var start = moment();
        var end = moment().add(29, 'days');

        function cb(start, end) {
            // $('#reportrangestart')
            // $('#reportrangestart').val(start.format('MM/D/YYYY'));
           
            $('#reportrangestart').val(start.format('DD-MM-YYYY'));
            $('#reportrangeend').val(end.format('DD-MM-YYYY'));
            // $('#reportrangestart').val(start.format('YYYY-MM-DD'));
            // $('#reportrangeend').val(end.format('YYYY/MM-DD'));
        }

        $('#reportrangestart').daterangepicker({
            //singleDatePicker: true,
             "locale": {
             "format": "DD-MM-YYYY",
             "separator": " = ",
             },
            startDate: start,
            endDate: end,
            ranges: {
               /*'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            	*/
			}
        }, cb);

        cb(start, end);
    });

	  $('#reportrangestart').on('cancel.daterangepicker', function(ev, picker) {
	       $('#reportrangestart').val('');
	       $('#reportrangeend').val('');
	  });
}

function generateExcel(){
	var obj = $('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    ajaxGETDATA(path + '/preview-export?'+obj,'onGenerateExcelSuccess','onSaveError');
}

function onGenerateExcelSuccess(response){
	$.LoadingOverlay("hide");
	var a = document.createElement('a');
    var url = window.URL.createObjectURL(response);
    a.href = url;
    a.download = 'myfile.xlsx';
    document.body.append(a);
    a.click();
    a.remove();
    window.URL.revokeObjectURL(url);
}

function display(page = 1){
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $('#form-filter').serialize() || page ==1){
		tbody.text('');
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	
	if($('[name=filter_keyword]').val()!='') params+="&filter_no_booking="+$('[name=filter_keyword]').val();
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	if(selected_kode_booking!="") params+="&filter_no_booking="+selected_kode_booking;
	ajaxGET(path + '/list?page='+page+'&'+params+"&order=start_date_purchase",'onGetListSuccess','onGetListError');
	
	current_page = page;
}

function onGetListSuccess(response){
	$.LoadingOverlay("hide");
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			//LORD
	
			//LORD
			var num_sub=1;
			$.each(value.purchase_detil,function(key,value){
				row += '<div id="collapse1" class="collapse bg-info" aria-labelledby="headingOne" data-parent="#accordionExample" style="">';
				//if(!contains.call(appendIds, value.id)){
					row += '<tr class="data-row" id="row-'+value.id+'" style="background-color: rgb(0 123 255 / 50%);">'+
					'   <td></td>'+
					'   <td>'+value.code_booking+'</td>'+
					
					'   <td class="">'+value.region.name+'</td>'+
					'   <td class="">'+moment(value.start_date).format("dddd, DD MMMM YYYY")+'</td>'+
					'   <td class=""><a href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'detil-identity\',\'0\');">'+value.count_ticket+' orang</a></td>'+
					'   <td class="text-rights">'+$.number(value.amount_ticket)+'</td>';
					
					var status_name=value.status_name.split(",")[0];
					var status_color=value.status_name.split(",")[1];
					var status_icon=value.status_name.split(",")[2];
				
					if(status_name=="WAITING") status_name="Waiting for Payment";
					if(status_name=="VERIFYING") {
						status_name="Verifying Payment";
						if(moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrAfter(moment().format("YYYY-MM-DD"))){
							status_color="primary";
						}else {
							status_color="danger";
							status_icon="exclamation-circle";
						}
					}
					
					
					row += '<td class="">';
						row +='<span class="badge badge-pill badge-'+status_color+' text-white">'+status_name +' <i class="fas fa-'+status_icon+'"></i></span>';
					row += '</td>';
					
					//bukti bayar here
					if(value.status_booking==null) {
						value['status_booking']= "DRAFT";
						status_color="warning";
						status_icon="exclamation-circle";
					}
					
					var btnDetilTransaksi = "";
					if(value.purchase_detil_transaction!=null){
						if(value.purchase_detil_transaction.length > 0){
							if(value.purchase_detil_transaction[0].file_id!=null){
								row += "<td class=''>";
									row +="<span class='badge badge-pill badge-"+status_color+" text-white'>"+value.status_booking +" <i class='fas fa-"+status_icon+"'></i></span> <br/>";
									row +="<button onclick='provement_view("+JSON.stringify(Object.assign(value.purchase_detil_transaction[0], {"tf": value.amount_ticket}))+");' class='btn btn-link btn-smalll btn-sm'>detail</button>";
								row +="</td>";
							}else{
								row += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
							}
						}else{
							row += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
						}
					}else{
						row += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
					}
					
						row += '<td class="">';
								row += '<div class="dropleft position-static">';
								row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info pill dropdown-toggles"  data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a> ';
							
								row += '<div class="dropdown-menu">';
							if(value.status!="PAYMENT"){
								
								if(value.status!="DRAFT" && value.status_booking!="DRAFT")row +='<a class="dropdown-item" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'view\',\'0\');">View Details</a>';//fillView();
								
								if(value.status=="PAYMENT"){
									row +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'TICKET\')">E-ticket</a>';
									if(value.status_reschedule==null || value.status_reschedule=="RESCHEDULE_NON_ACTIVE"){
										row +='      <a href="javascript:void(0)" class="dropdown-item text-success td-action d-none" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_ACTIVE\',\'0\');">Aktifkan Reschedule</a>';
									}else if(value.status_reschedule=="RESCHEDULE_ACTIVE"){
										row +='      <a href="javascript:void(0)" class="dropdown-item text-danger td-action" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_NON_ACTIVE\',\'0\');">Non-Aktifkan Reschedule</a>';
									}
								}
							
								if(value.status=="VERIFYING"){
									row +='      <a class="dropdown-item text-success td-action" href="#" onclick="doAction(\''+value.id+'\',\'PAYMENT\',\'1\')">PAID</a>';
								}
								if(value.status=="WAITING" || value.status=="VERIFYING"){
									row +='      <a class="dropdown-item text-muted  td-action" href="#" onclick="doAction(\''+value.id+'\',\'CANCEL\',\'0\')">CANCEL</a>';
								}
								if(value.status=="DRAFT" || value.status=="CANCEL" || value.status_expired_date==1){
									row +='<a class="dropdown-item text-danger td-action" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'delete\')">DELETE</a>';
								}
								
							}else{
								//row +=btnDetilTransaksi;
								if(value.status=="PAYMENT"){
									row +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'TICKET\')">E-ticket</a>';
									if(value.status_reschedule==null || value.status_reschedule=="RESCHEDULE_NON_ACTIVE"){
										row +='      <a href="javascript:void(0)" class="dropdown-item text-success td-action d-none" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_ACTIVE\',\'0\');">Aktifkan Reschedule</a>';
									}else if(value.status_reschedule=="RESCHEDULE_ACTIVE"){
										row +='      <a href="javascript:void(0)" class="dropdown-item text-danger td-action" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_NON_ACTIVE\',\'0\');">Non-Aktifkan Reschedule</a>';
									}
								}
							}
								row +='    </div>';
								row += '</div>';
						row += '</td>';
					
					row += '</tr>';
	
					num_sub++;
				//}
				row += '</div>';
			});
			num++;
		}
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function fillFormValue(value){
	clearForm();
	$('[name=name]').val(value.name);
}
function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(path + '/save',obj,'onModalActionSuccess','onModalActionError');
}


function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit' || selected_action == 'RESCHEDULE_ACTIVE' || selected_action == 'RESCHEDULE_NON_ACTIVE'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}else if(selected_action == 'PAYMENT' || selected_action == 'CANCEL') {
		updateRow(response.data);
	    /*Swal.fire(
	      'Berhasil!',
	      'Data berhasil ditambahkan.',
	      'success'
	    )*/
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	$('#modal-form-upload').modal('hide');
	selected_action = '';
	selected_id='';
	$.LoadingOverlay("hide");
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}
function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	var check='<div class="form-group form-check">'+
	    '<input type="checkbox" class="form-check-input" id="exampleCheck1">'+
    	//'<label class="form-check-label" for="exampleCheck1">Check me out</label>'+
    	'</div>';
	//row += '<td>'+(check)+'</td>';
	row += '<td>'+(num)+'</td>';
	row += '<td>'+value.code_booking+'</td>';
	row += '<td class=""> '+value.region.name +' </td>';

	row += '<td class=""> '+moment(value.start_date).format("dddd, DD MMMM YYYY")+' </td>';
	row += '<td class=""> <a href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'detil-identity\',\'0\');">'+value.count_ticket +' orang</a></td>';


	row += '<td class="text-rights"> '+$.number(value.amount_ticket) +' </td>';
	
	//row += '<td class="small">'+moment(value.datetime_serialize).format("DD-MM-YYYY HH:mm:SS");
	//row += '</td>';
	
	
	var status_name=value.status_name.split(",")[0];
	var status_color=value.status_name.split(",")[1];
	var status_icon=value.status_name.split(",")[2];

	if(status_name=="WAITING") status_name="Waiting for Payment";
	if(status_name=="VERIFYING") {
		status_name="Verifying Payment";
		console.log(moment(value.end_date).format("YYYY-MM-DD")+">>>"+moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrAfter(moment().format("YYYY-MM-DD")));
		if(moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrAfter(moment().format("YYYY-MM-DD"))){
			status_color="primary";
		}else {
			status_color="danger";
			status_icon="exclamation-circle";
		}
	}
	
	row += '<td class="">';
		row +='<span class="badge badge-pill badge-'+status_color+' text-white">'+status_name +' <i class="fas fa-'+status_icon+'"></i></span>';
	row += '</td>';
	
	//bukti bayar here
	if(value.status_booking==null) {
		value['status_booking']= "DRAFT";
		status_color="warning";
		status_icon="exclamation-circle";
	}
	if(value.purchase_detil_transaction!=null){
		if(value.purchase_detil_transaction.length > 0){
			if(value.purchase_detil_transaction[0].file_id!=null){
				value.purchase_detil_transaction[0].account_name_sender=value.purchase_detil_transaction[0].account_name_sender.replaceAll("'", "`");
				value.purchase_detil_transaction[0].file_transaction=value.purchase_detil_transaction[0].file_transaction.replaceAll("'", "");
				value.purchase_detil_transaction[0].file_name=value.purchase_detil_transaction[0].file_name.replaceAll("'", "");
				row += "<td class=''>";
					row +="<span class='badge badge-pill badge-"+status_color+" text-white'>"+value.status_booking +" <i class='fas fa-"+status_icon+"'></i></span> <br/>";
					row +="<button onclick='provement_view("+JSON.stringify(Object.assign(value.purchase_detil_transaction[0], {"tf": value.amount_ticket}))+");' class='btn btn-link btn-smalll btn-sm'>detail</button>";
				row +="</td>";
			}else{
				row += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
			}
		}else{
			row += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
		}
	}else{
		row += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
	}
	
	row += '<td class="">';
	row += '<div class="dropleft position-static">';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info pill dropdown-toggles"  data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a> ';

	row += '<div class="dropdown-menu">';
	
	if(value.status!="DRAFT" && value.status_booking!="DRAFT")row +='<a class="dropdown-item" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'view\',\'0\');">View Details</a>';//fillView();
	
	if(value.status=="PAYMENT"){
		row +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'TICKET\')">E-ticket</a>';
		if(value.status_reschedule==null || value.status_reschedule=="RESCHEDULE_NON_ACTIVE"){
			row +='      <a href="javascript:void(0)" class="dropdown-item text-success td-action d-none" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_ACTIVE\',\'0\');">Aktifkan Reschedule</a>';
		}else if(value.status_reschedule=="RESCHEDULE_ACTIVE"){
			row +='      <a href="javascript:void(0)" class="dropdown-item text-danger td-action" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_NON_ACTIVE\',\'0\');">Non-Aktifkan Reschedule</a>';
		}
	}

	if(value.status=="VERIFYING"){
		row +='      <a class="dropdown-item text-success td-action" href="#" onclick="doAction(\''+value.id+'\',\'PAYMENT\',\'1\')">PAID</a>';
	}
	if(value.status=="WAITING" || value.status=="VERIFYING"){
		row +='      <a class="dropdown-item text-muted  td-action" href="#" onclick="doAction(\''+value.id+'\',\'CANCEL\',\'0\')">CANCEL</a>';
	}
	if(value.status=="DRAFT" || value.status=="CANCEL" || value.status_expired_date==1){
		row +='<a class="dropdown-item text-danger td-action" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'delete\')">DELETE</a>';
	}
	row +='    </div>';
	row += '</div>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	if($('[name=filter_status]').val()=="WAITING"){
		if(value.status_expired_date==1 && status_name=="EXPIRED") row="";
	}
	return row;
}


function provement_view(obj){
	console.log(obj);

	var kontent = '<p class="text-left font-weight-bold card-title">Detail Transfer</p>'+
	'<p class="text-left">Tanggal: '+moment(obj.transaction_date_serialize, "YYYY-MM-DD").format("DD MMM YYYY")+'</p>'+//29 Okt 2021
	'<p class="text-left">Jumlah: '+$.number(obj.tf)+'</p>'+
	'<p class="text-left">Pengirim: '+obj.account_name_sender+'</p>'+
	'<p class="text-left">Bank: '+obj.bank_name_sender+' '+obj.account_number_sender+'</p>'+
	'</div>';
	
	Swal.fire({
	  //title: 'Informasi Keluarga yang bisa dihubungi',
	  html: kontent,
	  imageUrl: ctx+'/files/'+obj.file_id+'?filename='+obj.file_name+'&download',
	  //imageWidth: 400,
	  //imageHeight: 200,
	  imageAlt: 'Custom image',
	})
}
function addslashes(string) {
    return string.
    replace(/\\/g, '\\\\').
    replace(/\u0008/g, '\\b').
    replace(/\t/g, '\\t').
    replace(/\n/g, '\\n').
    replace(/\f/g, '\\f').
    replace(/\r/g, '\\r').
    replace(/'/g, '\\\'').
    replace(/"/g, '\\"');
}

function superDownload(url, mode = ""){
	console.log('original url: ', url);
    
	if(mode=="SERTIFIKAT") {
		url=ctx + "/export/e-sertifikat/"+url+"?nip=";
	}else if(mode=="TICKET") {
		url=ctx + "/export/e-ticket/"+url+"?nip=";
	}
	
	/*if(url.indexOf('?') > 0) url = url + '&';
    else url = url + '?';
    url = url.replace('export', 'export-to-mail');
    url += 'encrypt='+isEncrypt+'&';

    console.log('-- verify then download --');
    console.log('url before: ', url);
    console.log('url: ', url+"token="+"token");*/

    $.getJSON(url, function(response) {
		console.log(response);
        if (response.code == 200) {
            console.log('verify download OK');
            console.log(response.data);
            var path_download = ctx + '/export/download?filename='+addslashes(response.data);
            console.log('path_download: ', path_download);
            //$('#nameFileWillBeSentToEmail').html((response.data));
			//console.log(isEncrypt);
            //if(isEncrypt == false) {
               // window.open(path_download, '_blank');
                //location = path_download;
            //} else {
           //    // $('#modalFileWillBeSentToEmail').modal('show');
           // }
			window.open(path_download, '_blank');
        }else{
            //$('#verifyThenDownloadMsg').html(response.message);
            //$('#modalVerifyThenDownload').modal('show');
            //alert(response.message);
        }
    });
}

function  fillView(obj){
	console.log(obj);
	var element = $("#modal-view-only .modal-body .col-md-8 div.media-body")[0];
	element.children[0].innerHTML=": "+obj.region.name;
	element.children[1].innerHTML=": "+moment(obj.start_date).format("dddd, DD MMMM yyyy")
	var arrtim=[]; var arrketua=[];
	$.each(obj.purchase_detil_visitor, function(key){
		if(this.responsible=="1") arrketua.push(this.visitorIdentity.full_name);
		//if(this.responsible=="0") tim+=","+this.full_name;
		if(key > 1)arrtim.push(this.visitorIdentity.full_name);
	});
	try{
		var transit_camp = obj.purchase_detil_transit_camp[0];
		
		element.children[2].innerHTML=": "+transit_camp.name_transit_camp;
		element.children[3].innerHTML=": "+transit_camp.report_time_camp;
		element.children[4].innerHTML=": "+transit_camp.code_unique;
	}catch(error) {
		
	}
	element.children[5].innerHTML=": "+obj.count_ticket;
	element.children[6].innerHTML=": "+arrketua;
	element.children[7].innerHTML=": "+arrtim; 
	element.children[8].innerHTML=": "+$.number(obj.amount_ticket);
	element.children[9].innerHTML=": "+obj.code_booking;
	element.children[10].innerHTML=": "+obj.id;
	
	$("#modal-view-only").modal("show");
}

function fillIdentity(obj){
	console.log(obj);
	/*var element = $("#modal-view-only .modal-body .col-8 div.media-body")[0];
	element.children[0].innerHTML=": "+obj.region.name;
	element.children[1].innerHTML=": "+moment(obj.start_date).format("LLLL");
	var arrtim=[]; var arrketua=[];
	$.each(obj.purchase_detil_visitor, function(key){
		if(this.responsible=="1") arrketua.push(this.full_name);
		//if(this.responsible=="0") tim+=","+this.full_name;
		if(key > 1)arrtim.push(this.full_name);
	});
	element.children[5].innerHTML=": "+obj.count_ticket;
	element.children[6].innerHTML=": "+arrketua;
	element.children[7].innerHTML=": "+arrtim; 
	element.children[8].innerHTML=": "+$.number(obj.amount_total_ticket);*/
	
	var tbody = $("#tbl-identity").find('tbody');
	var row = "";
	var num = $('.data-row-identity').length+1;
	$.each(obj.purchase_detil_visitor, function(key){
		if(this.responsible==0){
		row += '<tr class="data-row-identity" id="row-id">'+
		'   <td>'+(key)+'</td>'+
		'   <td>'+this.visitorIdentity.no_identity+'</td>'+
		'   <td class=""> '+this.visitorIdentity.full_name+' </td>'+
		'   <td class=""> '+this.visitorIdentity.gender+' </td>'+
		'   <td class=""> '+this.phone_number+' </td>'+
		'   <td class="text-center"><a href="javascript:void(0)" onclick="remodal_view(\'/kawasan/files/'+this.visitorIdentity.id_file+'?filename='+this.visitorIdentity.file_name+'&download&decrypt\',\''+(this.visitorIdentity.file_name!=null?this.visitorIdentity.file_name.split('.').pop():"")+'\');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>'+
		'</tr>';
		num++;
		}
	});
	
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	
	$('#modal-detil-identity').modal('show');
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else {
		var obj = new FormData();
		if(action=="CANCEL"){
			id=id.split("::");
			obj.append('description_message', id[1]);
			id=id[0];
		}
		ajaxPOST(path + '/'+id+'/'+selected_action, obj, 'onModalActionSuccess','onActionError');
	}
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete' || selected_action == 'RESCHEDULE_NON_ACTIVE'  || selected_action == 'RESCHEDULE_ACTIVE' || selected_action == "CANCEL"){
		if(selected_action=="CANCEL"){
			Swal.fire({
			  input: 'textarea',
			  inputLabel: 'Apakah Anda yakin akan membatalkan pemesan tiket ini??',
			  inputPlaceholder: 'Ketik pesan email alasan purchase dicancel...',
			  inputAttributes: {
			    'aria-label': 'Type your message here'
			  },
			  showCancelButton: true
			}).then((result) => {
		  		if (result.isConfirmed) {
					console.log(result);
					doAction(value.id+"::"+result.value, 'CANCEL', 1);
			  	}
			})
		}else{
			$('#modal-confirm-msg').html(response.message);
			$('#modal-confirm').modal('show');
		}
	}else if(selected_action == 'view') {
		fillView(response.data);
	}else if(selected_action == 'detil-identity') {
		fillIdentity(response.data);
	}
	
}