var path = ctx + '/ticket/purchase';
var path_purchase = ctx + '/ticket/purchase';
var path_upload = ctx + '/files/';
var path_account = ctx + '/account';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];
var selected_status = '';

function init(){
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	selected_action = ($.urlParam('action') == null ? '' : $.urlParam('action'));
	selected_menu = ($.urlParam('menu') == null ? '' : $.urlParam('menu'));
	
	if(selected_action!=''){
		save();
	}
	
	
	if(selected_menu!="") display();
		
	
	$('#v-purchase_list-tab').click(function(){
		display();
	});
	$('#v-booking_list-tab').click(function(){
		display();
	});
	
	
	$('#form-register').submit(function(e){
		var obj = new FormData(document.querySelector('#form-register'));
		selected_action="UPDATE";
	    //ajaxPOST(path_account + '/save',obj,'onSaveForm','onModalActionError');
		var id = $('[name=id]').val();
		ajaxPOST(path_account + '/'+id+'/'+selected_action,obj,'onSaveForm','onActionError');
		e.preventDefault();
	});
}

function onSaveForm(response){
	console.log(response);
	 Swal.fire(
      'Berhasil!',
      response.message,
      'success'
    );

	$('#form-register').find('input').prop('disabled', true);
	$('#btn-submit').prop('disabled', true);
	$('#btn-edit').prop('disabled', false);
}

function display(page = 1){
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody_purchase = $("#container-purchase-list");
	var tbody_booking = $("#container-booking-list");
	
	tbody_purchase.text('');
	tbody_purchase.append('<div id="loading-row" class="list-group-item text-center loading-row"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div>');

	
	tbody_booking.text('');
	tbody_booking.append('<div id="loading-row" class="list-group-item text-center loading-row"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div>');
	
//	params = $( "#search-form" ).serialize();
	var mainPath="list";
	if(selected_menu=="purchase"){
		params="filter_status=NOT_PAYMENT";
	}
	if(selected_menu=="booking"){
		mainPath="list-boarding";
		params="filter_status=PAYMENT";
	}
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
	setTimeout(() => {
		ajaxGET(path + '/'+mainPath+'?page='+page+'&'+params,'onGetListSuccess','onGetListError');
	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
	console.log(response);
	$('.loading-row').remove();
	$('#no-data-row').remove();

	var tbody_purchase = $("#container-purchase-list");
	var tbody_booking = $("#container-booking-list");

	var row_purchase= "";
	var row_booking= "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			var status_name=value.status.split(",")[0];
			//if(status_name=='WAITING'){
				row_purchase += renderRow(value, num, 'WAITING');
			//}
			if(status_name=='PAYMENT' && value.status_booking){
				row_booking += renderRow(value, num, 'PAYMENT');
			}
			
			//LORD
			var num_sub=1;
			$.each(value.purchase_detil,function(key,value){
				row_booking += '<div id="collapse1" class="collapse bg-info" aria-labelledby="headingOne" data-parent="#accordionExample" style="">';
				//if(!contains.call(appendIds, value.id)){
					row_booking += '<tr class="data-row" id="row-'+value.id+'" style="background-color: rgb(0 123 255 / 50%);">'+
					'   <td></td>'+
					'   <td>'+num_sub+'</td>'+
					
					'   <td class="">'+value.region_name+'</td>'+
					'   <td class="">'+moment(value.start_date).format("dddd, DD MMMM YYYY")+'</td>'+
					'   <td class=""><a href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'detil-identity\',\'0\');">'+value.count_ticket+' orang</a></td>'+
					'   <td class="text-rights">'+$.number(value.amount_ticket)+'</td>';
					
					var status_name=value.status_name.split(",")[0];
					var status_color=value.status_name.split(",")[1];
					var status_icon=value.status_name.split(",")[2];
				
					if(status_name=="WAITING") status_name="Waiting for Payment";
					if(status_name=="VERIFYING") {
						status_name="Verifying Payment";
						if(moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrAfter(moment().format("YYYY-MM-DD"))){
							status_color="primary";
						}else {
							status_color="danger";
							status_icon="exclamation-circle";
						}
					}
					
					
					row_booking += '<td class="">';
						row_booking +='<span class="badge badge-pill badge-'+status_color+' text-white">'+status_name +' <i class="fas fa-'+status_icon+'"></i></span>';
					row_booking += '</td>';
					
					//bukti bayar here
					if(value.status_booking==null) {
						value['status_booking']= "DRAFT";
						status_color="warning";
						status_icon="exclamation-circle";
					}
					
					var btnDetilTransaksi = "";
					if(value.purchase_detil_transaction!=null){
						if(value.purchase_detil_transaction.length > 0){
							if(value.purchase_detil_transaction[0].file_id!=null){
								row_booking += "<td class=''>";
									row_booking +="<span class='badge badge-pill badge-"+status_color+" text-white'>"+value.status_booking +" <i class='fas fa-"+status_icon+"'></i></span> <br/>";
									row_booking +="<button onclick='provement_view("+JSON.stringify(Object.assign(value.purchase_detil_transaction[0], {"tf": value.amount_ticket}))+");' class='btn btn-link btn-smalll btn-sm'>detail</button>";
								row_booking +="</td>";
							}else{
								row_booking += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
							}
						}else{
							row_booking += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
						}
					}else{
						row_booking += '<td class=""><span class="badge badge-pill badge-'+status_color+' text-white">'+value.status_booking +' <i class="fas fa-'+status_icon+'"></i></span> </td>';
					}
					
						row_booking += '<td class="">';
								row_booking += '<div class="dropleft position-static">';
								row_booking += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info pill dropdown-toggles"  data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a> ';
							
								row_booking += '<div class="dropdown-menu">';
							if(value.status!="PAYMENT"){
								
								if(value.status!="DRAFT" && value.status_booking!="DRAFT")row_booking +='<a class="dropdown-item" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'view\',\'0\');">View Details</a>';//fillView();
								
								if(value.status=="PAYMENT"){
									row_booking +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'TICKET\')">E-ticket</a>';
									if(value.status_reschedule==null || value.status_reschedule=="RESCHEDULE_NON_ACTIVE"){
										row_booking +='      <a href="javascript:void(0)" class="dropdown-item text-success td-action d-none" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_ACTIVE\',\'0\');">Aktifkan Reschedule</a>';
									}else if(value.status_reschedule=="RESCHEDULE_ACTIVE"){
										row_booking +='      <a href="javascript:void(0)" class="dropdown-item text-danger td-action" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_NON_ACTIVE\',\'0\');">Non-Aktifkan Reschedule</a>';
									}
								}
							
								if(value.status=="VERIFYING"){
									row_booking +='      <a class="dropdown-item text-success td-action" href="#" onclick="doAction(\''+value.id+'\',\'PAYMENT\',\'1\')">PAID</a>';
								}
								if(value.status=="WAITING" || value.status=="VERIFYING"){
									row_booking +='      <a class="dropdown-item text-muted  td-action" href="#" onclick="doAction(\''+value.id+'\',\'CANCEL\',\'0\')">CANCEL</a>';
								}
								if(value.status=="DRAFT" || value.status=="CANCEL" || value.status_expired_date==1){
									row_booking +='<a class="dropdown-item text-danger td-action" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'delete\')">DELETE</a>';
								}
								
							}else{
								//row_booking +=btnDetilTransaksi;
								if(value.status=="PAYMENT"){
									row_booking +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'TICKET\')">E-ticket</a>';
									if(value.status_reschedule==null || value.status_reschedule=="RESCHEDULE_NON_ACTIVE"){
										row_booking +='      <a href="javascript:void(0)" class="dropdown-item text-success td-action d-none" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_ACTIVE\',\'0\');">Aktifkan Reschedule</a>';
									}else if(value.status_reschedule=="RESCHEDULE_ACTIVE"){
										row_booking +='      <a href="javascript:void(0)" class="dropdown-item text-danger td-action" onclick="doAction(\''+value.id+'\',\'RESCHEDULE_NON_ACTIVE\',\'0\');">Non-Aktifkan Reschedule</a>';
									}
								}
							}
								row_booking +='    </div>';
								row_booking += '</div>';
						row_booking += '</td>';
					
					row_booking += '</tr>';
	
					num_sub++;
				//}
				row_booking += '</div>';
			});
			num++;
		}
	});
//	if(response.next_more){
//		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
//		row += '	<button class="row_booking btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
//		row += '</div></td></tr>';
//	}
	row_purchase == "" ? tbody_purchase.html(no_data('PURCHASE')) : tbody_purchase.append(row_purchase);
	row_booking == "" ? tbody_booking.html(no_data('BOOKING')) : tbody_booking.append(row_booking);
	
}

function no_data(action){
	var html='';
	if(action=='PURCHASE'){
		html = '<div id="no-data-row" class="card mb-3 nodata" >'+
		'						  <div class="row no-gutters">'+
		'						    <div class="col-auto p-3">'+
		'						      <img width="100" src="https://ik.imagekit.io/tvlk/image/imageResource/2017/05/23/1495539753478-be3f37a9133fa6e3f40688781d07e885.png?tr=q-75" class="img-fluid" alt="...">'+
		'						    </div>'+
		'						    <div class="col-md-8">'+
		'						      <div class="card-body">'+
		'						        <h5 class="card-title">No Active Booking Found</h5>'+
		'						        <p class="card-text">Anyting you booked shows up here, but it seems like you haven\'t made any.Let\'s create one via homepage!</p>'+
		'						        <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->'+
		'						      </div>'+
		'						    </div>'+
		'						  </div>'+
		'						</div>';
	}else if(action=='BOOKING'){
		html = '<div id="no-data-row" class="card mb-3 nodata" >'+
		'						  <div class="row no-gutters">'+
		'						    <div class="col-auto p-3">'+
		'						      <img width="100" src="https://ik.imagekit.io/tvlk/image/imageResource/2017/05/23/1495539753478-be3f37a9133fa6e3f40688781d07e885.png?tr=q-75" class="img-fluid" alt="...">'+
		'						    </div>'+
		'						    <div class="col-md-8">'+
		'						      <div class="card-body">'+
		'						        <h5 class="card-title">No Purchases Found</h5>'+
		'						        <p class="card-text">No Purchases FoundYou haven\'t made any purchases within the past 30 days. If you have made any purchases previously, use Filter to see them. <a href="#">Make a New Purchase</a></p>'+
		'						        <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->'+
		'						      </div>'+
		'						    </div>'+
		'						  </div>'+
		'						</div>';
	}
	return html;
}

function fillFormValue(value){
	clearForm();
	$('[name=name]').val(value.name);
}
function save(){
	var obj = new FormData();
	if(selected_id != '') {
		obj.append('id', selected_id);
		obj.append('status', selected_action);
	}else{
		alert("WRONG");
		return false;
	}
    ajaxPOST(path_purchase + '/submit',obj,'onModalActionSuccess','onModalActionError');
}


function onModalActionSuccess(resp){
	console.log(resp);
	if(selected_id != ''){
		if(selected_status!='DRAFT'){
//			window.location.href='payment?id='+resp.data.id+'&step=1';
			window.location.href='manage-user';
		}else{
			window.location.href='general';
		}
	}else{
		if(selected_action!=""){
			var value = resp.data;
			var status_name=value.status_name.split(",")[0];
			var status_color="badge-"+value.status_name.split(",")[1];
			var status_icon=value.status_name.split(",")[2];
			
			var el = $('#row-'+resp.data.id).find('span.badge');
			el.attr("class", "badge badge-pill "+status_color+" text-white");
			el.html(status_name+" <i class='fas fa-"+status_icon+"'></i>");
			
			var el = $('#row-'+resp.data.id).find('.action-right');
			el.html('<a href="?menu=purchase_detil&id='+value.id+'" class="">See Details</a>');
			
			//$('#modal-confirm').modal('hide');
			//$('#modal-form').modal('hide');
			selected_action = '';
			//selected_id='';
			showAlertMessage(resp.message, 1500);
		}
	}
}
function onModalActionError(response){
	console.log(response);
	showAlertMessage(response.responseJSON.message, 1500, true);
}

function checkRoleUser(value) {
  return value == 'ROLE_KAWASAN_OPERATOR';
}

function renderRow(value, num, method){
	var row = "";

	var status_name=value.status_name.split(",")[0];
	var status_color=value.status_name.split(",")[1];
	var status_icon=value.status_name.split(",")[2];

	var button='';
	
	if(status_name=='WAITING' || status_name=='VERIFYING'){
		button = '<!-- Example split danger button -->'+
		'<a target="_blank" href="payment?id='+value.id+'&step=1" class="mr-3">Continue to Payment</a>'+
		'<div class="btn-group">'+
		'<a href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>'+
		'  <div class="dropdown-menu">'+
		'    <a class="dropdown-item" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'CANCEL\',\'1\')">Cancel Purchase</a>'+
		'  </div>'+
		'</div>';
	}else if(status_name=='DRAFT'){
		button = '<!-- Example split danger button -->'+
		'<a target="_blank" href="booking?id='+value.id+'&step=1" class="">Continue to Booking</a>'+
		'<div class="btn-group">'+
		'<a href="#" class="mr-3 d-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>'+
		'  <div class="dropdown-menu">'+
		'    <a class="dropdown-item" href="?id='+value.id+'&action=CANCEL">Cancel Purchase</a>'+
		'  </div>'+
		'</div>';
	}
	
	var arrRole = roleList.replaceAll(/\[|\]/g,'').replace(/ /g, '').split(',');
	roleOpr = arrRole.find(checkRoleUser);
	if(roleOpr=="ROLE_KAWASAN_OPERATOR"){
		button="";
	}
	
	var icon_status=' <i class="fas exclamation-circle text-warnings"></i>';
	var icon_cancel=' <i class="fas fa-ban text-dangers"></i>';
	var icon_sukses=' <i class="fas fa-check-circle text-successs"></i>';
	var icon_waiting=' <i class="fas fa-clock text-infos"></i>';
	
	if(status_name=="WAITING") {
		status_name="Waiting for Payment";
		icon_status='<span class="badge badge-pill badge-info text-white">'+status_name+icon_waiting+'</span>';
	}
	if(status_name=="VERIFYING") {
		status_name="Verifying Payment";
		icon_status='<span class="badge badge-pill badge-primary text-white">'+status_name+icon_waiting+'</span>';
	}
	if(status_name=="CANCEL") {
		status_name="Cancel Payment";
		icon_status='<span class="badge badge-pill badge-danger text-white">'+status_name+icon_cancel+'</span>';
	}
	if(status_name=="PAYMENT") {
		icon_status='<span class="badge badge-pill badge-success text-white">'+status_name+icon_sukses+'</span>';
	}
	if(status_name=="EXPIRED") {
		icon_status='<span class="badge badge-pill badge-danger text-white">'+status_name+icon_status+'</span>';
	}
	row += '<div id="row-'+value.id+'" class="card mt-3">'+
	'							  <div class="card-header ">'+
	'							  	<div class="row">'+
	'								    <div class="col text-left  "><span class="text-muted">Booking Code</span> <strong>'+value.code_booking+'</strong></div>'+
	'								    <div class="col text-right"><strong>IDR '+$.number((selected_menu=="booking"?value.amount_ticket_boarding:value.amount_ticket))+'</strong></div>'+
	'							    </div>'+
	'							  </div>'+
/*	'							  <div class="card-body pt-2 pb-2">'+
	'							    <p class="card-text">'+value.region.name+'</p>'+
	'							  </div>'+*/
	'<ul class="list-group list-group-flush">'+
	'   <li class="list-group-item">';
		row +='<div class="row">'+
		'         <div class="col"><div><a class="'+((selected_menu!="booking"?"disabled btn-link":""))+'" target="_blank" href="?menu=booking_detil&id='+value.id+'">'+value.id+'<a/></div>'+icon_status+'</div>'+
		'         <div class="col text-right">'+value.region_name+' - '+value.regionEndName+'</div>'+
		'      </div>';
	$.each(value.purchase_detil,function(key,value){
		
	var status_name=value.status_name.split(",")[0];
	var status_color=value.status_name.split(",")[1];
	var status_icon=value.status_name.split(",")[2];
	
	
	if(status_name=="WAITING") {
		status_name="Waiting for Payment";
		icon_status='<span class="badge badge-pill badge-info text-white">'+status_name+icon_waiting+'</span>';
	}
	if(status_name=="VERIFYING") {
		status_name="Verifying Payment";
		icon_status='<span class="badge badge-pill badge-primary text-white">'+status_name+icon_waiting+'</span>';
	}
	if(status_name=="CANCEL") {
		status_name="Cancel Payment";
		icon_status='<span class="badge badge-pill badge-danger text-white">'+status_name+icon_cancel+'</span>';
	}
	if(status_name=="PAYMENT") {
		icon_status='<span class="badge badge-pill badge-success text-white">'+status_name+icon_sukses+'</span>';
	}
	
		row +='<li class="list-group-item">'+
		'<div class="row">'+
		'         <div class="col"><div><a target="_blank" href="?menu=booking_detil&id='+value.id+'">'+value.id+'<a/></div>'+icon_status+'</div>'+
		'         <div class="col text-right">'+value.region_name+' - '+value.regionEndName+'</div>'+
		'      </div>'+
		'</li>';
	});
	
	row += '</li>'+
	'</ul>'+
	'							  <div class="card-footer">'+
	'							  <div class="row">'+
	'									<div class="col text-left align-self-center"><span class="d-none badge badge-pill badge-'+status_color+' text-white">'+status_name +' <i class="fas fa-'+status_icon+'"></i></span> </div>'+
//	'								    <div class="col text-left">Awaiting Selection of payment Method</div>'+
	'								    <div class="col text-right action-right"> '+button+'</div>'+
	'							    </div>'+
	'							  </div>'+
	'							</div>';
		
	
//	if(method != 'replace') {
//		row += '</tr>';
//	}
	
	return row;
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action,{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function swall_view(src, phonenumber, address){
		var kontent;//='<p class="font-weigh-bold card-title">Informasi Keluarga yang bisa dihubungi</p><p>Nomor.HP : '+phonenumber+'</p> <p>Alamat Rumah : '+address+'</p>';
		
		if(phonenumber=="null") kontent="";
		Swal.fire({
		  //title: 'Informasi Keluarga yang bisa dihubungi',
		  html: kontent,
		  imageUrl: src,
		  imageWidth: 400,
		  imageHeight: 200,
		  imageAlt: 'Custom image',
		})
	}