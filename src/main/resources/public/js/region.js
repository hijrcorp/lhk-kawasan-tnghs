var pathsql = ctx + '/restql';
var path = ctx + '/ticket/region';
var path_facility = ctx + '/ticket/region-facility';
var path_transit = ctx + '/ticket/region-transit';
var path_photo = ctx + '/ticket/region-photo';
var path_todo = ctx + '/ticket/region-todo';
var path_upload = ctx + '/files/';
var current_page = 1;
var params = '';
var appendIds = [];
var select_obj={"id" : "", "header" : "", "mode" : "", "parent" : "", "action" : ""};

function init(){
	display();
	initTinyMce('description');
	getSelectTransitCamp();
	$('#btn-add').click(function(e){
		path = ctx + '/ticket/region';
		select_obj['id'] = '';
		clearForm();
		add('master');
	});

	
	$('#btn-modal-confirm-yes').click(function(e){
		if(select_obj['action'] == 'delete' ){
			doAction(select_obj['id'], select_obj['action'], 1);
		}
		
	});
	$('#modal-form').on('hidden.bs.modal', function (e) {
		select_obj['action'] = '';
		select_obj['id'] = '';
		select_obj['header'] = '';
		select_obj['mode'] = '';
		$('#modal-form').find('.modal-dialog').removeClass("modal-lg");
	});
	$('#modal-confirm').on('hidden.bs.modal', function (e) {
		select_obj['action'] = '';
		select_obj['id'] = '';
		select_obj['header'] = '';
		select_obj['mode'] = '';
	});
	$('#modal-alert').on('hidden.bs.modal', function (e) {
		select_obj['action'] = '';
		select_obj['id'] = '';
		select_obj['header'] = '';
		select_obj['mode'] = '';
	});
	
	$(document).on('click', '.list-group li', function() {
       $(".list-group li").removeClass("active");
       $(this).toggleClass("active");
	});
	
	$("[name=file]").on('change', function() {
		if(this.files.length>0){
			if(this.files[0].size > 3173155){
				alert("File tidak boleh lebih dari 3MB.");
				this.value = "";
			}else{
				console.log(this.files[0].size);
				$('.custom-file-label').text(this.files[0].name);
			}
		}
	});
	
}

function getSelectTransitCamp(){
	$('[name=id_kelompok_jabatan]').empty();
	//$('[name=id_kelompok_jabatan]').append(new Option('Pilih Kelompok Jabatan', ''));
	var page=1,params="",qry="";
	ajaxGET(pathsql + '/master_transit_camp?page='+page+'	&'+params+'&'+qry,'onGetSelectTransitCamp','onGetSelectError');
}
function onGetSelectTransitCamp(response){
	$.each(response.data, function(key, value) {
		$('[name=id_master_transit_camp]').append(new Option(value.name, value.id));
		$('#id_master_transit_camp').append(new Option(value.code, value.id));
	});
}

function reset_add(){
	clearForm();
	//
	$('#entry-form-tent :input').prop("disabled", true);
	$('#entry-form-tent').hide();
	
	$('#entry-form-photo :input').prop("disabled", true);
	$('#entry-form-photo').hide();
	
	$('#entry-form-facility :input').prop("disabled", true);
	$('#entry-form-facility').hide();
	
	$('#entry-form-transit :input').prop("disabled", true);
	$('#entry-form-transit').hide();
	
	$('#entry-form-todolist :input').prop("disabled", true);
	$('#entry-form-todolist').hide();
	
	$('#entry-form :input').prop("disabled", true);
	$('#entry-form').hide();
}
function setMode(mode,id,header){
	select_obj['mode']=mode;
	if(select_obj['mode']=='todo') {
		
		select_obj['id']=id;
		select_obj['header']=header;
		$("#btn-todo-edit-"+header).removeAttr("disabled");
		$("#btn-todo-delete-"+header).removeAttr("disabled");
		
	}
}
function add(mode,idx,parent,days){
	reset_add();
//	select_obj['action']='add';
	select_obj['mode']=mode;
	select_obj['header']=idx;
	select_obj['parent']=parent;
	if(select_obj['mode']=='photo'){
		$('#entry-form-photo :input').prop("disabled", false);
		$('#entry-form-photo').show();
	}else if(select_obj['mode']=='tent'){
		$('#entry-form-tent :input').prop("disabled", false);
		$('#entry-form-tent').show();
	}else if(select_obj['mode']=='facility'){
		$('#entry-form-facility :input').prop("disabled", false);
		$('#entry-form-facility').show();
	}else if(select_obj['mode']=='transit'){
		$('#entry-form-transit :input').prop("disabled", false);
		$('#entry-form-transit').show();
	}else if(select_obj['mode']=='todo'){
		$('[name=parent]').val(parent);
		$('[name=day]').val(days);
		$('#entry-form-todolist :input').prop("disabled", false);
		$('#entry-form-todolist').show()
	}else{
		$('#entry-form :input').prop("disabled", false);
		$('#entry-form').show();
		$('#modal-form').find('.modal-dialog').addClass("modal-lg");
	}
}

function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#table-card-body");//$("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
//	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
//
//		
//	}
	
//	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	tbody.append('<div id="loading-row" class="list-group-item text-center text-dark"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div>');
		
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path + '/list?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#table-card-body");//.find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
//		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
//		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
//	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	row == "" ? tbody.html('<div id="no-data-row" class="nodata list-group-item text-center text-dark" align="center">Data Tidak Ada.</div>') : tbody.html(row);
	
}

function fillFormValue(value){
//	clearForm();
	console.log("??", value);
	add(select_obj['mode'],value.header_id);
	tinymce.remove('#description');
	if(select_obj['mode']=='facility'){
		$('[name=item]').val(value.item);
		$('[name=code]').val(value.code);
		$('[name=price]').val(value.price);
		$('[name=include]').val(value.include);
		select_obj['id']=value.id;
	}else if(select_obj['mode']=='transit'){
		$('[name=code]').val(value.code);
		$('[name=id_master_transit_camp]').val(value.id_master_transit_camp);
		$('[name=start_register]').val(value.start_register_serialize);
		$('[name=end_register]').val(value.end_register_serialize);
		$('[name=extimate_arrived]').val(value.extimate_arrived_serialize);
		$('[name=tent_small]').val(value.number_of_small_tents);
		$('[name=tent_large]').val(value.number_of_large_tents);
		select_obj['id']=value.id;
	}else if(select_obj['mode']=='todo'){
		$('[name=day]').val(value.day);
		$('[name=times]').val(value.time_serialize);
		$('[name=activity]').val(value.activity);
		$('[name=parent]').val(value.parent);
		select_obj['id']=value.id;
	}else{
		$('[name=name]').val(value.name);
		$('[name=type]').val(value.type);
		$('[name=location]').val(value.location);
		$('[name=kuota]').val(value.kuota);
		$('[name=max_day]').val(value.max_day);
		$('[name=location_desc]').val(value.location_desc);
		$('#description').val(value.description);
		//if(value.description!=""){
			initTinyMce('description');
			tinymce.activeEditor.setContent(value.description);
			$('#description').html(value.description);
		//}
	}
}
function onModalUploadSuccess(resp){
	var value=resp.data;
	console.clear();
	console.log(resp);
//	alert("lanjut upload");
	obj = new FormData(document.querySelector('#entry-form-photo'));
	obj.append('header_id', select_obj['header']);
	obj.append('file_id', value.id);
	console.log(obj.getAll('header_id'));
	console.log(obj.getAll('file_id'));
	var new_path=ctx + '/ticket/region-photo';
	ajaxPOST(new_path + '/save',obj,'onModalActionSuccess','onModalActionError');
}
function onModalUploadError(resp){
	alert("Gagal upload");
}
function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(select_obj['id'] != '') obj.append('id', select_obj['id']);
	
	if(select_obj['mode']=='photo'){
		/*obj = new FormData(document.querySelector('#entry-form-photo'));
		obj.append('reference', select_obj['header']);
		ajaxPOST(path_upload + '/upload',obj,'onModalUploadSuccess','onModalUploadError');*/
		
		obj = new FormData(document.querySelector('#entry-form-photo'));
		if(select_obj['id'] != '') obj.append('id', select_obj['id']);
		obj.append('header_id', select_obj['header']);
		ajaxPOST(path_photo + '/save',obj,'onModalActionSuccess','onModalActionError');	
	}else if(select_obj['mode']=='facility'){
		obj = new FormData(document.querySelector('#entry-form-facility'));
		if(select_obj['id'] != '') obj.append('id', select_obj['id']);
		obj.append('header_id', select_obj['header']);
		ajaxPOST(path_facility + '/save',obj,'onModalActionSuccess','onModalActionError');
	}else if(select_obj['mode']=='transit'){
		obj = new FormData(document.querySelector('#entry-form-transit'));
		if(select_obj['id'] != '') obj.append('id', select_obj['id']);
		obj.append('header_id', select_obj['header']);
		ajaxPOST(path_transit + '/save',obj,'onModalActionSuccess','onModalActionError');
	}else if(select_obj['mode']=='todo'){
		obj = new FormData(document.querySelector('#entry-form-todolist'));
		if(select_obj['id'] != '') obj.append('id', select_obj['id']);
		obj.append('header_id', select_obj['header']);
		ajaxPOST(path_todo + '/save',obj,'onModalActionSuccess','onModalActionError');
	}else{
		var new_s="";
		if(tinymce.get("description")!=null){
			var htmlObject = $.parseHTML(tinymce.get("description").getContent());
			$.each(htmlObject, function(key){
				$.each($(this).find('img'), function(key_2){
					console.log(this);
					this.src=this.title;
				})
				if(this.outerHTML != undefined)
				new_s=this.outerHTML;
			});
			
			obj.append('description', tinymce.get("description").getContent());
		}
		ajaxPOST(path + '/save',obj,'onModalActionSuccess','onModalActionError');
	}
    console.log(select_obj);
}


function onModalActionSuccess(response){
	
	$.LoadingOverlay("hide");
	console.log(select_obj['mode']);
	console.log(select_obj['action']);
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	if(select_obj['mode'] == 'master'){
		display();
		window.location.reload();
	}else if(select_obj['mode'] == 'todo'){
		display_new_todo();
	}else{
		// Update data ke existing tabel
		//select_obj['action'] == 'add' &&
		if( select_obj['id'] == '' && select_obj['action'] != 'remove' && select_obj['action'] != 'edit-status'){
			appendRow(response.data);
		}else if(select_obj['action'] == 'edit'){
			updateRow(response.data);
		}else if(select_obj['action'] == 'edit-status'){
			/*var num = $('.data-row-photo-'+select_obj['header']).length+1;
			$.each(value.list_photo.transaction, function(key){
				if(!contains.call(appendIds, this.id)){
					row += renderRowPhoto(this, num);
					num++;
				}
			});*/
			console.log("awakening "+response);
			$('.status-active').html("");
			$('#status-'+response.data.id).html((response.data.active?"<i class='fas fa-check'></i>":""));
		
		}else if(select_obj['action'] == 'delete'){
			removeRow(response.data);
		}
	
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	
	$.LoadingOverlay("hide");
	console.log(response);
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}

function appendRow(value){
	console.log("romadhan "+ value);
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = "";
	if(select_obj['mode']=='photo'){
		
		$('.status-active').html("");
//		display_new_photo();
		num = $('.data-row-photo-'+select_obj['header']).length+1;
		tbody = $("#tbl-data-photo-"+select_obj['header']).find('tbody');
		row = renderRowPhoto(value, num);
		tbody.append(row);
		
	}else if(select_obj['mode']=='facility'){
		num = $('.data-row-facility-'+select_obj['header']).length+1;
		tbody = $("#tbl-data-facility-"+select_obj['header']).find('tbody');
		row = renderRowFacility(value, num);
		tbody.append(row);
	}else if(select_obj['mode']=='transit'){
		num = $('.data-row-transit-'+select_obj['header']).length+1;
		tbody = $("#tbl-data-transit-"+select_obj['header']).find('tbody');
		row = renderRowTransitCamp(value, num);
		tbody.append(row);
	}else if(select_obj['mode']=='todo'){
		row = renderRow(value, num);
//		tbody = $("#tbl-todo");//.find('tbody');
//		row = render_new_todo(value, num);
//		tbody.append(row);
		display_new_todo();
	}else{
		row = renderRow(value, num);
		display();
	}

	console.log(tbody);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	console.log(select_obj['mode']);
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = "";
	
	//
//		$('#loading-row').remove();
//		$('#no-data-row').remove();
		
//		var more = $('#more-button-row').html();
//		$('#more-button-row').remove();
		
//		var tbody = $("#tbl-data").find('tbody');
		
//		var num = $('.data-row').length+1;
//		var row = renderRow(value, num);
		if(select_obj['mode']=='photo'){
			//row-1610949888797753 
			console.log(value);
				tbody = $("#tbl-data-photo-"+select_obj['header']).find('tbody');
				row = renderRowPhoto(value, num, 'replace');
				//
			num = $('.data-row-photo-'+select_obj['header']).length+1;
			tbody = $("#tbl-data-photo-"+select_obj['header']).find('tbody');
			row = renderRowPhoto(value, num);
			tbody.append(row);
			//
		}else if(select_obj['mode']=='transit'){
			tbody = $("#tbl-data-transit-"+select_obj['header']).find('tbody');
			row = renderRowTransitCamp(value, num, 'replace');
		}else if(select_obj['mode']=='facility'){
//			num = $('.data-row-facility-'+select_obj['header']).length;
			tbody = $("#tbl-data-facility-"+select_obj['header']).find('tbody');
			row = renderRowFacility(value, num, 'replace');
//			tbody.append(row);
		}else if(select_obj['mode']=='todo'){
			row = renderRow(value, num, 'replace');
			display_new_todo();
		}else{
			row = renderRow(value, num, 'replace');
			display();
			//window.location.reload();
		}
	
//		console.log(tbody);
	
//		appendIds.push(value.id);
//		tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
	//
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function renderRowPhoto(value, num, method='add'){
	console.log("UP ", value.list_photo);
//	value=value.use_this;
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row-photo-'+value.header_id+'" id="row-'+value.id+'" ondblclick="setMode(\'photo\');doAction(\''+value.id+'\',\'edit-status\',\'1\')">';
	}
//	row += '<tr class="data-row" id="row-'+value.id+'">';
		row += '<td>'+(num)+'</td>';
		//if(method=='add'){
		//	row += '<td class=""> '+ (value.list_photo!=null?value.list_photo.name:"N/A") +' </td>';
		//}else{
			row += '<td class=""> '+ (value.name_file!=null?value.name_file:"N/A") +' </td>';
		//}
		row += '<td id="status-'+value.id+'" class="status-active"> '+(value.active?"<i class='fas fa-check'></i>":"") +' </td>';
		
		row += '<td class="">';//inst.open()
			row += '<a href="javascript:void(0)" onclick="remodal_view(\''+ctx+'/files/'+(value.name_file!=null?value.file_id:"N/A")+'?filename='+(value.name_file!=null?value.name_file:"N/A")+'&download\');" class="btn btn-sm btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="setMode(\'photo\');doAction(\''+value.id+'\',\'delete\',\'0\')"><i class="fas fa-fw fa-trash"></i></a>';
			
		row += '</td>';
//	row += '</tr>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function renderRowFacility(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row-facility-'+value.header_id+'" id="row-'+value.id+'">';
	}
//	row += '<tr class="data-row" id="row-'+value.id+'">';
		row += '<td>'+(num)+'</td>';
		row += '<td class=""> '+ value.item +' </td>';
		row += '<td class=""> '+ $.number(value.price) +' </td>';
		row += '<td class=""> '+ value.include +' </td>';
		row += '<td class="">';
			row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="setMode(\'facility\');doAction(\''+value.id+'\',\'edit\');"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="setMode(\'facility\');doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
		row += '</td>';
//	row += '</tr>';

	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function renderRowTransitCamp(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row-transit-'+value.header_id+'" id="row-'+value.id+'">';
	}
		row += '<td>'+(num)+'</td>';
		row += '<td class=""> '+ value.code +' </td>';
		row += '<td class=""> '+ value.name_master_transit_camp +' </td>';
		row += '<td class=""> '+ value.start_register_serialize +"-"+ value.end_register_serialize +' </td>';
		row += '<td class=""> '+ value.extimate_arrived_serialize +' </td>';
		row += '<td class="">';
			row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="setMode(\'transit\');doAction(\''+value.id+'\',\'edit\');"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
			row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="setMode(\'transit\');doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a> ' ;
			row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-primary pill d-none" type="" onclick="setMode(\'tent\');add(\'tent\',\''+value.id+'\')" data-toggle="modal" data-target="#modal-form" ><i class="fas fa-fw fa-home"></i></a>';
			row += '<a href="javascript:void(0)" onclick="remodal_view(\''+ctx+'/files/'+(value.name_photo!=null?value.photo:"N/A")+'?filename='+(value.name_photo!=null?value.name_photo:"N/A")+'&download\');" class="btn btn-sm btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a> ';
		row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		//row += '<tr class="data-row" id="row-'+value.id+'">';
		row += '<div class="card" id="row-'+value.id+'">';
	}
	//

//	row += '<div class="card">';
		//master
		row += '<div class="card-header card-header-2" id="heading'+num+'">';
			row += '<div class="row rows-body text-dark"">';
					row += '<div class="col-md-1">'+num+'</div>';
					row += '<div class="col-md-3">'+value.name+'</div>';
					row += '<div class="col">'+value.type+'</div>';
					row += '<div class="col">'+value.location_desc+'</div>';
					row += '<div class="col">';
						row += '<a href="#collapse'+num+'" data-toggle="collapse" class="btn btn-sm btn-square btn-primary pill" style="opacity: 0.9;"><i class="fas fa-fw fa-info"></i></a> ';
						row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="setMode(\'master\');doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
						row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="setMode(\'master\');doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
					row += '</div>';
	//				row += '<div class="col-md-2 text-truncate d-inline-block">'+value.description+'</div>';
			row += '</div>';
		row += '</div>';
		//end master
	
			row += '<div id="collapse'+num+'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">';
			row += '<div class="card-body">';
			row += '<div class="container-fluid">';
			row += '<div class="row p-md-3 justify-content-center">';
			row += '<div class="col-sm-12">';
			//

			//master		
			row += '<div class="card mb-3 d-none">';
				row += '<div class="card-header" style="background: #0f8094!important;">';

				row += '<div class="custom-control custom-switch">'+
				'			  <input type="checkbox" class="custom-control-input" id="customSwitch0">'+
				'			  <label class="custom-control-label" for="customSwitch0">Refundable</label>'+
				'			</div>';
				row +='</div>';
				
			row += '</div>';
			
			//
		//master		
		row += '<div class="card mb-3">';
			row += '<div class="card-header" style="background: #0f8094!important;">Description</div>';
			row += '<div class="card-body bg-light text-dark">';
				row += '<p class="card-text">'+value.description+'</p>';
			row += '</div>';
		row += '</div>';
		//end master
	
		//photo
		row += '<div class="card mb-3">';
		//row += '<div class="card-header" style="background: #0f8094!important;">Description</div>';
		row += '<div class="card-body bg-light text-dark">';
			row += '<h5 class="card-title">';
				row += '<div class="row">';
					row += '<div class="col-6 pt-1"><span>Photos</span></div>';
					row += '<div class="col-6 text-right"><button onclick="add(\''+"photo"+'\',\''+value.id+'\');" data-toggle="modal" data-target="#modal-form" class="btn btn-secondary">Add</button></div>';
				row += '</div>';
			row += '</h5>';
			row += '<div class="row">';
			row += s_div('col-md-12');
				row += '<div class="table-responsive">';
				row +='<table id="tbl-data-photo-'+value.id+'" class="table table-sm table-striped">';
					row +='<thead class="">'+
						'<tr>'+
							'<th scope="col">#</th>'+
							'<th scope="col">Item</th>'+
							'<th scope="col">Main</th>'+
							'<th scope="col">Action</th>'+
						'</tr>';
					row +='</thead>';
					row +='<tbody class="bg-light text-dark">';

					var num = $('.data-row-photo-'+select_obj['header']).length+1;
					$.each(value.list_photo.transaction, function(key){
//						console.log(thos);
						if(!contains.call(appendIds, this.id)){
							row += renderRowPhoto(this, num);
							num++;
						}
					});
						
					row +='</tbody>';
				row +='</table>';
			row += e_div();
			row += "<p class='small text-mutted text-muted'>*Untuk meng-aktifkan/nonaktifkan, silahkan double klik pada salah satu row.</p>";
			row += e_div();
			row += '</div>';
		row += '</div>';
		row += '</div>';
		//end photo
		
		
		//transit
		row += '<div class="card mb-3">';
		row += '<div class="card-header" style="background: #0f8094!important;">';
			row += '<div class="row">';
				row += '<div class="col-6 pt-1"><span>Transit Camp</span></div>';
				row += '<div class="col-6 text-right"><button onclick="add(\''+"transit"+'\',\''+value.id+'\');" data-toggle="modal" data-target="#modal-form" class="btn bg-head">Add</button></div>';
			row += '</div>';
		row += '</div>';
		row += '<div class="card-body bg-light text-dark">';
		
		row += s_div('row');
			row += '<div class="col-md-12 table-responsive">';
			row +='<table id="tbl-data-transit-'+value.id+'" class="table table-sm table-striped">';
				row +='<thead class="">'+
					'<tr>'+
						'<th scope="col">#</th>'+
						'<th scope="col">Code</th>'+
						'<th scope="col">Transit</th>'+
						'<th scope="col">Registrasi</th>'+
						'<th scope="col">Estimasi</th>'+
					'</tr>';
				row +='</thead>';
				row +='<tbody class="bg-light text-dark">';
					var num = $('.data-row-transit-'+select_obj['header']).length+1;
					$.each(value.list_transit_camp, function(key){

						if(!contains.call(appendIds, this.id)){
							row += renderRowTransitCamp(this, num);
							num++;
						}
					});
				row +='</tbody>';
			row +='</table>';
			row += e_div();
		row += e_div();
    	
    	row += e_div();
    	row += e_div();
		//end transit
		
		//facility
		row += '<div class="card mb-3">';
		row += '<div class="card-header" style="background: #0f8094!important;">';
			row += '<div class="row">';
				row += '<div class="col-6 pt-1"><span>Facility</span></div>';
				row += '<div class="col-6 text-right"><button onclick="add(\''+"facility"+'\',\''+value.id+'\');" data-toggle="modal" data-target="#modal-form" class="btn bg-head">Add</button></div>';
			row += '</div>';
		row += '</div>';
		row += '<div class="card-body bg-light text-dark">';
		
		row += s_div('row');
			row += s_div('col-md-12');
			row += '<div class="table-responsive">';
			row +='<table id="tbl-data-facility-'+value.id+'" class="table table-sm table-striped">';
				row +='<thead class="">'+
					'<tr>'+
						'<th scope="col">#</th>'+
						'<th scope="col">Item</th>'+
						'<th scope="col">Price</th>'+
						'<th scope="col">Include</th>'+
						'<th scope="col">Action</th>'+
					'</tr>';
				row +='</thead>';
				row +='<tbody class="bg-light text-dark">';
					var num = $('.data-row-facility-'+select_obj['header']).length+1;
					$.each(value.list_facility, function(key){

						if(!contains.call(appendIds, this.id)){
							row += renderRowFacility(this, num);
							num++;
						}
					});
				row +='</tbody>';
			row +='</table>';
			row += e_div();
			row += e_div();
		row += e_div();
    	
    	row += e_div();
    	row += e_div();
		//end facility
	    	
    	//todolist
		row += '<div class="card d-none">';
		row += '<div class="card-header" style="background: #0f8094!important;">';
			row += '<div class="row">';
				row += '<div class="col-md-6 pt-1"><span>Planing/TODOLIST</span></div>';
				row += '<div class="col-md-6 text-right"><button id="btn-todo-edit-'+value.id+'" onclick="doAction(\''+this.id+'\',\'edit\')" class="btn bg-head" disabled> <i class="fas fa-fw fa-pencil-alt"></i></button> <button id="btn-todo-delete-'+value.id+'" onclick="doAction(\''+this.id+'\',\'delete\')" class="btn bg-head" disabled> <i class="fas fa-fw fa-trash"></i></button></div>';
			row += '</div>';
		row += '</div>';
		row += '<div class="card-body bg-light text-dark">';
		
		row += s_div('row');
			row += s_div('col-md-12');
			row += '<ul id="tbl-todo-'+value.id+'" class="list-group">';
				var data_level=[];
				var data_sub_level=[];
				$.each(value.list_todo, function(key){
					console.log("iman", this);
					if(this.parent==null || this.parent=="") data_level.push(this);
					else data_sub_level.push(this);
				});

				$.each(data_level, function(key){
					row += '<li onclick="setMode(\'todo\',\''+this.id+'\',\''+value.id+'\')" data-toggle="collapse" data-target=".qoyum'+this.id+'" class="list-group-item list-group-item-action">';
						row += '<i class="fas fa-plus mr-2"></i> Day '+(this.day)+' : '+this.activity+'';
				    row += '</li>';
				    val_id=this.id;
					$.each(data_sub_level, function(key){
						if(val_id==this.parent){
							row += '<div class="collapse qoyum'+this.parent+'" style="margin-bottom: -1px;border-top-left-radius: .25rem;border-top-right-radius: .25rem;">';
							    row += '<li onclick="setMode(\'todo\',\''+this.id+'\',\''+value.id+'\');" class="list-group-item list-group-item-action">';
									row += '<i class="fas fa-calendar-check mr-2 ml-4"></i>'+this.time_serialize+': '+this.activity;
							    row += '</li>';
						    row += '</div>';
						}
					});
			    	row += '<div class="collapse qoyum'+this.id+'" style="margin-bottom: -1px;border-top-left-radius: .25rem;border-top-right-radius: .25rem;">';
					    row += '<li onclick="add(\''+"todo"+'\',\''+""+'\',\''+this.id+'\',\''+this.day+'\');setMode(\''+"todo"+'\',\''+""+'\',\''+value.id+'\');" data-toggle="modal" data-target="#modal-form" class="list-group-item list-group-item-action">';
							row += '<i class="fas fa-plus mr-2 ml-4"></i> +++ Add NEW +++';
						row += '</li>';
				    row += '</div>';
				});
				  
				row += '<li onclick="add(\''+"todo"+'\',\''+value.id+'\');" data-toggle="modal" data-target="#modal-form" class="list-group-item list-group-item-action">';
					row += '<i class="fas fa-plus mr-2"></i> +++ Add NEW +++';
				row += '</li>';
			row += '</ul>';
		    row += e_div();
    	row += e_div();
    	
    	row += e_div();
    	row += e_div();
		//end todolist
				
				row += e_div();
			row += e_div();
			row += e_div();
			row += e_div();
	      row += e_div();
	    
	
	if(method != 'replace') {
//		row += '</tr>';
		row += '</div>';
	}
	
	return row;
}

function createFormTent(){
	var row="";
	for(var i=0; i < $("[name=jumlah_tenda]").val(); i++){
		row +=renderRowTent(i+1);
	}
	if($("[name=ukuran_tenda]").val()=="B"){
		$('#row-tenda-besar').html(row);
	}else if($("[name=ukuran_tenda]").val()=="K"){
		$('#row-tenda-kecil').html(row);
	}
}

function renderRowTent(value, num, method='add'){
	var row = "";
	
	row += '<div class="border col-md-2 p-1 m-1 text-center bg-success">'+$('#id_master_transit_camp :selected').text()+$("[name=ukuran_tenda]").val()+" "+value+'</div>';
	
	return row;
}

function display_new_todo(page=1){
	var tbody = $("#tbl-todo-"+select_obj['header']);//$("#tbl-data").find('tbody');
	tbody.text('');
	tbody.append('<div id="loading-row" class="list-group-item text-center text-dark"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div>');		
	ajaxGET(ctx + '/ticket/region' + '/list?page='+page+'&'+params,'onrender_new_todoSuccess','onrender_new_todoError');
}
function onrender_new_todoSuccess(response){
	console.log(response);

	var tbody = $("#tbl-todo-"+select_obj['header']);
	var row="";

	$.each(response.data,function(key,value){
//		$.each(value.list_todo, function(key){

			var data_level=[];
			var data_sub_level=[];
			$.each(value.list_todo, function(key){
				console.log("iman", this);
				if(this.parent==null || this.parent=="") data_level.push(this);
				else data_sub_level.push(this);
			});

			$.each(data_level, function(key){
				if(this.header_id==select_obj['header']){
				row += '<li onclick="setMode(\'todo\',\''+this.id+'\',\''+value.id+'\')" data-toggle="collapse" data-target=".qoyum'+this.id+'" class="list-group-item list-group-item-action">';
					row += '<i class="fas fa-plus mr-2"></i> Day '+(this.day)+' : '+this.activity+'';
			    row += '</li>';
			    val_id=this.id;
				$.each(data_sub_level, function(key){
					if(val_id==this.parent){
						row += '<div class="collapse qoyum'+this.parent+'" style="margin-bottom: -1px;border-top-left-radius: .25rem;border-top-right-radius: .25rem;">';
						    row += '<li onclick="setMode(\'todo\',\''+this.id+'\',\''+value.id+'\');" class="list-group-item list-group-item-action">';
								row += '<i class="fas fa-calendar-check mr-2 ml-4"></i>'+this.time_serialize+': '+this.activity;
						    row += '</li>';
					    row += '</div>';
					}
				});
		    	row += '<div class="collapse qoyum'+this.id+'" style="margin-bottom: -1px;border-top-left-radius: .25rem;border-top-right-radius: .25rem;">';
				    row += '<li onclick="add(\''+"todo"+'\',\''+""+'\',\''+this.id+'\',\''+this.day+'\');setMode(\''+"todo"+'\',\''+""+'\',\''+value.id+'\');" data-toggle="modal" data-target="#modal-form" class="list-group-item list-group-item-action">';
						row += '<i class="fas fa-plus mr-2 ml-4"></i> +++ Add NEW +++';
					row += '</li>';
			    row += '</div>';


				}
			});

//			if(this.header_id==select_obj['header']){
//			}
			
//		});
	});

	row += '<li onclick="add(\''+"todo"+'\',\''+select_obj['header']+'\');" data-toggle="modal" data-target="#modal-form" class="list-group-item list-group-item-action">';
		row += '<i class="fas fa-plus mr-2"></i> +++ Add NEW +++';
	row += '</li>';
	
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
}

function display_new_photo(page=1){
	var tbody = $("#tbl-data-photo-"+select_obj['header']).find('tbody');
	tbody.text('');
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');		
	ajaxGET(path + '/list?page='+page+'&'+params,'onrender_new_photoSuccess','onrender_new_todoError');
}
function onrender_new_photoSuccess(response){
	console.log(response);

	var tbody = $("#tbl-data-photo-"+select_obj['header']).find('tbody');
	var row="";

	$.each(response.data,function(key,value){
		$.each(value.list_photo, function(key){
			row += '<tr class="data-row-photo" id="row-'+this.id+'">';
				row += '<td>'+(key+1)+'</td>';
				row += '<td class=""> '+ this.name +' </td>';
				row += '<td class=""> '+ "<i class='fas fa-check'></i>" +' </td>';
				row += '<td class="">';//inst.open()
					row += '<a href="javascript:void(0)" onclick="remodal_view("'+ctx+'/files/'+this.id+'?filename='+this.name+'&download");" class="btn btn-sm btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a> ';
//					row += '<a target="blank" href="'+ctx+'/files/'+this.id+'?filename='+this.name+'&download" class="btn btn-sm btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a> ';
					row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="setMode(\'photo\');doAction(\''+this.id+'\',\'remove\',\'1\')"><i class="fas fa-fw fa-trash"></i></a>';
				row += '</td>';
			row += '</tr>';
		});
	});
	
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
}

function s_div(val_html='', style=''){
	val_html='<div class='+val_html+' style='+style+'>';
	return val_html;
}
function e_div(){
	return '</div>';
}
function clearForm(){
	$('#entry-form')[0].reset();
	//
	$('#entry-form-photo')[0].reset();
	
	$('.custom-file-label').text("");
	$('#entry-form-facility')[0].reset();
	$('#entry-form-transit')[0].reset();
	$('#entry-form-todolist')[0].reset();
	//
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
//	selected_action = action;
	select_obj['action'] = action;
	$('#modal-form-msg').hide();
	if(select_obj['mode']=='photo'){
		path=path_photo;
	}else if(select_obj['mode']=='facility'){
		path=path_facility;
	}else if(select_obj['mode']=='transit'){
		path=path_transit;
		select_obj['action']=action;
	}else if(select_obj['mode']=='todo'){
		path=path_todo;
		id=select_obj['id'];
		select_obj['action']=action;
	}else{
		$('#modal-form').find('.modal-dialog').addClass("modal-lg");
		path=ctx + '/ticket/region';
		
	}
//	alert(path+" "+id+ " "+select_obj['mode']);
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+select_obj['action'],'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+select_obj['action'],{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
	select_obj['id'] = value.id;
	
	
	if(select_obj['action'] == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
		
	}else if(select_obj['action'] == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function upload(){
	var path = ctx + '/files';
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
//	obj.append('folder', 'uploads');
    ajaxPOST(path + '/upload',obj,'onModalActionSuccess','onModalActionError');
}
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function initTinyMce(element){
    tinymce.init({
      height : "450px",
      selector: 'textarea#'+element,
//      plugins: 'code', //'image code'
		plugins: [
		    'advlist autolink lists link image charmap print preview anchor',
		    'searchreplace visualblocks code fullscreen',
		    'insertdatetime media table paste code help wordcount',
			'hr'
		  ],
//      toolbar: 'undo redo | code',//'undo redo | link image | code'
  toolbar: 'undo redo | formatselect | ' +
  'bold italic backcolor | alignleft aligncenter ' +
  'alignright alignjustify | bullist numlist outdent indent | ' +
  'removeformat | help | hr',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
      image_title: true,
      automatic_uploads: true,
      file_picker_types: 'image',
    /*  
	file_picker_callback: function (cb, value, meta) {
    	  
        var input = document.createElement('input');
        input.setAttribute('name', 'lampiran[]');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

       
        input.onchange = function () {
          var file = this.files[0];

          var reader = new FileReader();
          reader.onload = function () {
            var id = 'blobid' + (new Date()).getTime();
            var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            cb(blobInfo.blobUri(), { title: file.name});
          };
          reader.readAsDataURL(file);
          
          input.setAttribute('data-name', file.name);
        };

        input.click();
        $('#form-file').append(input);
      }*/
    });
}