var path = ctx + '/ticket/bank';
var path_upload = ctx + '/files/';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	display();
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		//clearForm();
	});
	
	$('[name=type]').on('change', function(){
		if(this.value=="ATM"){
			$('.form-type').show();
		}else if(this.value=="VA"){
			$('.form-type').hide();
		}
	});

	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});
}

var pathsql = ctx + '/restql';
function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');

		
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(pathsql + '/agenda?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

var list_general=[];
function onGetListSuccess(response){
	list_general=[];
	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
		if(this.end_date==null){
			list_general.push({
				publicId: this.id,
		        color: this.code,
		        title: this.title,
		        start: moment(this.start_date, 'YYYY/MM/DD').format('YYYY-MM-DD'),
		    });
		}else{
			list_general.push({
				publicId: this.id,
				  //groupId: 999,
		        color: this.code,
		        title: this.title,
		        start: moment(this.start_date, 'YYYY/MM/DD').format('YYYY-MM-DD'),
		        end: moment(this.end_date, 'YYYY/MM/DD').add(1, 'days').format('YYYY-MM-DD')
		    });
		}

		/*general_code.push({
	          groupId: 999,
	          title: 'All Day Event',
	          start: '2021-09-01',
	          end: '2021-09-10'
	    });

		general_code.push({
	          groupId: 999,
	          title: 'All Day Event',
	          url: 'http://google.com/',
	          start: '2021-09-01',
	          end: '2021-09-10'
	    });*/

	});
	
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	console.log(list_general);
	renderCalendar();
}

function fillFormValue(value){
	clearForm();
	
	$('[name=name]').val(value.name);
	$('[name=type]').val(value.type);
	$('[name=value]').val(value.value);
	$('[name=id]').val(value.id);
	if(value.type=="CORE") $('[name=name]').attr('disabled', true);
}

function save(){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	var url = pathsql+'/agenda/save?prefix_entity=tbl_';
	if($('[name=id]').val() != "") url = pathsql+'/agenda/save?prefix_entity=tbl_&append=false';
	window.obj = {
			data : objectifyForm($('#entry-form').serializeArray()),
			dataMain : {
				"column" : [
					$('[name=name]').val(),
					$('[name=value]').val(),
					$('[name=type]').val(),
					$('[name=start_date]').val()
				]
			}
		};
	if($('[name=id]').val() == "") delete obj.data.id;
	if($('[name=end_date]').val() == "") delete obj.data.end_date;
	console.log(obj);
	$.ajax({
        url : url,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form').modal('hide');
        		display();
        },
        error : function (response) {
        		$.LoadingOverlay("hide");
        		$('#modal-form-msg').text(response.responseJSON.message);
        		$('#modal-form-msg').removeClass('d-none');
        		$('#modal-form-msg').show();
        },
    });
    
}


function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
		display();
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	$('#modal-form-upload').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}
function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
	row += '<td>'+(num)+'</td>';
	row += '<td class=""> '+value.code +' </td>';
	row += '<td class=""> '+value.title +' </td>';
	row += '<td align="" class=""> '+moment(value.start_date,'YYYY/MM/DD').format('YYYY-MM-DD')+" <strong>s.d</strong> "+moment(value.end_date,'YYYY/MM/DD').format('YYYY-MM-DD')+' </td>';
	row += '<td class="">';
	//row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-success pill" type="" onclick="doAction(\''+value.id+'\',\'upload\')"><i class="fas fa-fw fa-file-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	if(value.type!="CORE")row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}
function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
	$('.form-type').hide();
	$('[name=name]').attr('disabled', false);
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(pathsql + '/agenda/'+id+'?prefix_entity=tbl_&action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(pathsql + '/agenda/'+id+'/'+selected_action+"?prefix_entity=tbl_",{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'upload'){
		$('#modal-form-upload-msg').hide();
		$('#modal-form-upload').modal('show');
		
	}
	
}

function renderCalendar() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      initialDate: ($('[name=start_date]').val()==''?moment().format('YYYY-MM-DD'):$('[name=start_date]').val()),
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
	  locale: 'id',
      select: function(arg) {
		console.log(arg);
        $('#btn-add').click();
		clearForm();
		$('[name=start_date]').val(moment(arg.startStr,'YYYY/MM/DD').format('YYYY-MM-DD'));
		$('[name=end_date]').val(moment(arg.endStr,'YYYY/MM/DD').subtract(1, 'd').format('YYYY-MM-DD'));
		if($('[name=start_date]').val()==$('[name=end_date]').val()){
			$('[name=end_date]').val('');
		}
		
		/*var title = prompt('Event Title:');
        if (title) {
          calendar.addEvent({
            title: title,
            start: arg.start,
            end: arg.end,
            allDay: arg.allDay
          })
        }
        calendar.unselect()*/

      },
      eventClick: function(arg) {
		//console.log(arg.el.fcSeg.eventRange.def.extendedProps.publicId);
		doAction(arg.el.fcSeg.eventRange.def.extendedProps.publicId,'delete');
        /*if (confirm('Are you sure you want to delete this event?')) {
          arg.event.remove()
        }*/
      },
      editable: true,
      dayMaxEvents: true, // allow "more" link when too many events
      events: list_general
    });

    calendar.render();

	$('.fc-toolbar.fc-header-toolbar').addClass('row col-lg-12');
  }