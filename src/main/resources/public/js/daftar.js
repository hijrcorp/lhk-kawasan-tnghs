var path = ctx + '/authorities';
var path_account = ctx + '/account';
var path_position = ctx + '/position';
var path_group = ctx + '/account/group';
var selected_id = '';
var selected_account_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_account_id = '';
		selected_action = 'add';
		clearForm();
	});
	
	$('#form-register').submit(function(e){
		save();
		e.preventDefault();
	});
	
	
}


function onGetListError(response){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = '<tr id="retry-button-row" class="retry-button-row"><td colspan="20"><div align="center">';
	row += '	<button class="btn btn-outline-secondary" onclick="display('+current_page+')">Error! Coba lagi..</button>';
	row += '</div></td></tr>';
		
		
	tbody.append(row);
	
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action+'?force=true',{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
//	console.log(value);
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		selected_account_id = value.account.id;
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}
	
}

function onModalActionSuccess(response){
	console.log(response);
	/*$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	selected_action = '';
	selected_id='';*/
	//showAlertMessage(response.message, 1500, doOther);
	
	Swal.fire({
	  title: 'Pendaftaran akun anda berhasil.',
	  text: "Anda sekarang sudah bisa login, untuk memesan tiket pendakian. ",
	  icon: 'success',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Login Sekarang',
	  cancelButtonText: 'Close',
	}).then((result) => {
	  if (result.isConfirmed) {
	    window.location.href=getURLParam("url").replace("/id/", "?id=");
	  }
	})
}

function onModalActionError(response){
	Swal.fire({
	  icon: 'warning',
	  //title: 'Oops...',
	  text: "Mohon Maaf, "+response.responseJSON.message,
	  //footer: '<a href>Why do I have this issue?</a>'
	})
	
	//$('#modal-form-msg').text(response.responseJSON.message);
	//$('#modal-form-msg').show();
}

function fillFormValue(value){
	clearForm();
	
	$('[name=username]').val(value.account.username);
	$('[name=first_name]').val(value.account.first_name);
	$('[name=last_name]').val(value.account.last_name);
	$('[name=email]').val(value.account.email);
	$('[name=mobile]').val(value.account.mobile);
	$('[name=position_id]').val(value.position.id);
	$('[name=group_id]').val(value.group.id);
	if(value.account.enabled == false) $('[name=enabled]').prop('checked', true);
}

function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#form-register'));
    ajaxPOST(path_account + '/save',obj,'onModalActionSuccess','onModalActionError');
}



