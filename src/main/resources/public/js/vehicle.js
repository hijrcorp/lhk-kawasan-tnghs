var path = ctx + '/ticket/vehicle';
var path_type = ctx + '/ticket/type-vehicle';
var path_upload = ctx + '/files/';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	display();
	displayType();
	getSelectType();
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-add-type').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearFormType();
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});
	
	$(document).on('click', 'table tbody tr', function() {
       $("table tbody tr").removeClass("table-primary");
       $(this).toggleClass("table-primary");
	});
}

function getSelectType(){
//    $('[name="kelas[]"]').empty();
//    $('[name="kelas[]"]').append(new Option('Choose Type', ''));
    ajaxGET(path_type + '/list?page='+1+'&'+"",'onGetSelectType','onGetListError');
}
function onGetSelectType(response){
    console.log(response);
    $.each(response.data, function(key, value) {
        $('[name=type_id]').append(new Option(value.name, value.id));
    });
//     $('[name="kelas[]"]').selectric('refresh');
}

function display(page = 1){
	
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');

		
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $( "#search-form" ).serialize();
	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path + '/list?page='+page+'&'+params,'onGetListSuccess','onGetListError');
//	}, 1000);
		
	current_page = page;
}

function onGetListSuccess(response){
	console.log(response);
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function fillFormValue(value){
	clearForm();
	
	$('[name=name]').val(value.name);
	$('[name=type_id]').val(value.type_id);
	$('[name=cost]').val(value.cost);
	$('[name=seat_capacity]').val(value.seat_capacity);
	$('[name=suitcase]').val(value.suitcase);
	$('[name=description]').val(value.description);
	$('[name=provided]').val(value.provided);
}
function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
//	obj.append('file', selected_id);
    ajaxPOST(path + '/save',obj,'onModalActionSuccess','onModalActionError');
}


function onModalActionSuccess(response){
	console.log(response);
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	$('#modal-form-upload').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}
function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function renderRow(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
	row += '<td>'+(num)+'</td>';
	row += '<td class=""> '+value.name +' </td>';
	row += '<td class=""> '+value.type_name +' </td>';
	row += '<td align="" class=""> '+value.seat_capacity +' </td>';
	row += '<td align="" class=""> '+value.suitcase +' </td>';
	row += '<td align="" class=""> '+$.number(value.cost) +' </td>';
	row += '<td align="" class=""> '+value.provided +' </td>';
	row += '<td class="">';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-success pill" type="" onclick="doAction(\''+value.id+'\',\'upload\')"><i class="fas fa-fw fa-file-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}
function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action,{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'upload'){
//		$('#modal-confirm-msg').html(response.message);
//		$('#modal-confirm').modal('show');
		$('#modal-form-upload-msg').hide();
		$('#modal-form-upload').modal('show');
		
	}
	
}

//


function displayType(page = 1){
	$('#more-button-row-category').remove();
	$('#retry-button-row-category').remove();
	
	var tbody = $("#tbl-data-category").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
//	if(params == '' || params != $( "#search-form" ).serialize() || page ==1){
		tbody.text('');
//	}
	
	tbody.append('<tr id="loading-row-category"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
//	params = $( "#search-form" ).serialize();
//	console.log(path + '/list?page='+page+'&'+params);
	// Aktifin komen di bawah untuk ngetes loading spinner
//	setTimeout(() => {
		ajaxGET(path_type + '/list?page='+page+'&'+params,'onGetListTypeSuccess','onGetListError');
//	}, 1000);
	current_page = page;
}

function onGetListTypeSuccess(response){
//	console.log(response);
	
	$('#loading-row-category').remove();
	$('#no-data-row-category').remove();
	
	var tbody = $("#tbl-data-category").find('tbody');
	
	var row = "";
	var num = $('.data-row-category').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRowType(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row-category" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row-category" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
}

function renderRowType(value, num, method='add'){
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	
//	row += '<td>'+(num)+'</td>';
	row += '<td class="" colspan="2"> '+value.name +' </td>';
	row += '<td class="">';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-info pill" type="" onclick="doAction(\''+value.id+'\',\'edit\')"><i class="fas fa-fw fa-pencil-alt"></i></a> ';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-danger pill" type="" onclick="doAction(\''+value.id+'\',\'delete\')"><i class="fas fa-fw fa-trash"></i></a>';
	row += '</td>';
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function saveType(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form-category'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(path_type + '/save',obj,'onSaveTypeSuccess','onModalActionError');
}
function onSaveTypeSuccess(resp){
	alert("SUC");
}

function saveUpload(){
	var obj = new FormData(document.querySelector('#entry-form-upload'));
	obj.append('reference', selected_id);
	ajaxPOST(path_upload + '/upload',obj,'onModalActionSuccess','onModalActionError');
}