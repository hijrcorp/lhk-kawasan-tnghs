var path = ctx + '/ticket/region';
var path_purchase = ctx + '/ticket/purchase';
var path_facility = ctx + '/ticket/region-facility';
var path_todo = ctx + '/ticket/region-todo';
var path_upload = ctx + '/files/';
var current_page = 1;
var params = '';
var appendIds = [];
var select_obj={"id" : "", "header" : "", "mode" : "", "parent" : "", "action" : ""};
var selected_step='';
var total_price=0;

function init(){
	selected_id = ($.urlParam('id') == null ? '' : $.urlParam('id'));
	selected_step = ($.urlParam('step') == null ? '' : $.urlParam('step'));

//	display();
//	$("#search-form").submit(function(e){
//		e.preventDefault();
//		display();
//	});

	$("#entry-form").submit(function(e){
		e.preventDefault();
		save();
	});
	$("#btn-book").click(function(){
		clearForm();
	});

	$(document).on('click', '.card.vehicle', function() {
//       $(".card.vehicle").removeClass("border-primary");
		if($(event.target).closest('.input-group').length > 0) {
	        return false; // if you want to ignore the click completely
	        // return; // else
	    }
       $(this).toggleClass("card-pricing border-primary");
	});
	
	$(document).on('click', '.card .tour-guide', function() {
//       $(".card .tour-guide").removeClass("shadow-lg");
       $(this).toggleClass("shadow-lg");
	});
	
	$("#btn-book-4").click(function(){
		total_price=0;
		renderPerson();
		renderTourGuide();
		renderVehicle();
	});

	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_id != '' && selected_step != ''){
			save(1);
		}
	});
	
}

function checkAsLeader(){
	/*var identity=["1111111","1111111", "2222222", "3333333", "4444444"];
	var namefull=["satu","satu", "dua", "tiga", "empat"];
	for(var i=0; i < $('[name=no_identity]').length;i++){
		$('[name=no_identity]')[i].value=identity[i];
		$('[name=full_name]')[i].value=namefull[i];
		$('[name=phone_number]')[i].value="82"+identity[i];
		$('[name=email]')[i].value=namefull[i]+"@email.com";
	}*/
		$('[name=no_identity]')[1].value=$('[name=no_identity]')[0].value;
		$('[name=full_name]')[1].value=$('[name=full_name]')[0].value;
		$('[name=phone_number]')[1].value=$('[name=phone_number]')[0].value;
		$('[name=email]')[1].value=$('[name=email]')[0].value;
		$('[name=type_identity]')[1].value=$('[name=type_identity]')[0].value;
}

function clearForm(){
	$('#entry-form')[0].reset();
	//
	$('#modal-form-msg').hide();
}

function s_div(val_html='', style=''){
	val_html='<div class='+val_html+' style='+style+'>';
	return val_html;
}
function e_div(){
	return '</div>';
}

function save(do_action=0){
	var obj = new FormData();
	if(selected_id != '' && selected_step !='') {
		obj = new FormData();
		obj.append('id', selected_id);
		//obj.append('vehicle_id', getDataArr().vehicle);
		//obj.append('tour_guide_id', getDataArr().guide);
		obj.append('amount_ticket', total_price);
		obj.append('detil_visitor', JSON.stringify(getDataArr().person));
		obj.append('detil_vehicle', JSON.stringify(getDataArr().vehicle));
		obj.append('detil_guide', JSON.stringify(getDataArr().guide));
	}else{
		obj.append('region_id', selected_id);
	}
	if(selected_id != '' && selected_step != ''){
	if(do_action==0){
		var msg = "<div><strong>Are your booking details correct?</strong></div>";
		msg+="<div class='mt-1'>You will not be able to change your booking details once you proceed to payment. Do you want to continue?</div>"
		$('#modal-confirm-msg').html(msg);
		$('#modal-confirm').modal('show');

		$('#modal-confirm').find('.btn-secondary').text("Check Again");
		$('#modal-confirm').find('.btn-primary').text("Yes, Continue to payment");
		return false;
	}
	}
    ajaxPOST(path_purchase + '/save',obj,'onModalActionSuccess','onModalActionError');
}

function onModalActionSuccess(resp){
	console.log(resp);
	if(selected_id != '' && selected_step != ''){
		window.location.href='payment?id='+resp.data.id+'&step=1';
	}else{
		window.location.href='booking?id='+resp.data.id+'&step=1';
	}
	
}
function onModalActionError(response){
	console.log(response);
	showAlertMessage(response.responseJSON.message, 1500, true);
}

function setDataTemp(param){
	console.log(param);
	var temp_arr=[];
	
	var result = { };
	$.each($('#form-travel-detil').serializeArray(), function() {
	    result[this.name] = this.value;
	});
	console.log(result);
	
	new_obj = {}

	$.each($(form).serializeArray(), function(i, obj) { 
		new_obj[obj.name] = obj.value 
	})
	
	//if param set detail vehicle
	$('#pills-book-2')
}

function getDataArr(){
	var obj = {
		person: [],
		vehicle: [],
		guide: []
	}

	$.each($("[data-key]"), function(key){
		if($(this).find('[name=no_identity]').val()!="") {
			var obj_person = {
				type_identity: $(this).find('[name=type_identity]').val(),
				no_identity: $(this).find('[name=no_identity]').val(),
				full_name: $(this).find('[name=full_name]').val(),
				phone_number: $(this).find('[name=phone_code]').val()+$(this).find('[name=phone_number]').val(),
				email: $(this).find('[name=email]').val(),
				responsible: (key==0?1:0)
			}
			//obj['person'].push(obj_person);
		}
	});
	//
	var all_obj_vehicle = $('.vehicle.border-primary');//$('[data-id_vehicle]');
	$.each(all_obj_vehicle, function(key){
		vehicle = {
			vehicle_id: "",
			qty_vehicle: 0,
		}
		console.log($(this).data('id_vehicle'));
		vehicle['vehicle_id'] = $(this).data('id_vehicle');
		vehicle['qty_vehicle'] = $(this).find('[name=qty_vehicle]').val();
		obj['vehicle'].push(vehicle);
	});
	//
	var all_obj_guide = $('.tour-guide.shadow-lg');
	$.each(all_obj_guide, function(key){
		guide = {
			guide_id: "",
		}
		console.log($(this).data('id_vehicle'));
		guide['guide_id'] = $(this).data('id_tour_guide');
		obj['guide'].push(guide);
	});

	//obj['vehicle'] = $('#pills-tabContent').find('.border-primary').data('id_vehicle');
	//obj['guide'] = $('#pills-book-3').find('.shadow-lg').data('id_tour_guide');
	return obj;
}

(function($,undefined){
	  '$:nomunge'; // Used by YUI compressor.

	  $.fn.serializeObject = function(){
	    var obj = {};

	    $.each( this.serializeArray(), function(i,o){
	      var n = o.name,
	        v = o.value;

	        obj[n] = obj[n] === undefined ? v : $.isArray( obj[n] ) ? obj[n].concat( v ) : [ obj[n], v ];
	    });

	    return obj;
	  };

	})(jQuery);

function renderPerson(){
	var tbody=$('#container-person');
	var tbody2=$('#container-person-contact');
	var html ='';
	var html2 ='';
	var data=getDataArr();
	var idx=0;
	$.each(data.person, function(key){
	if(this.responsible==0){
	html += '<div class="card mb-3">'+
	'	            <div class="card-header">'+
	'	              <div class="row align-items-center">'+
	'	                <div class="col">'+
	'	                  <h5 class="mb-0">Person '+(key)+'</h5>'+
	'	                </div>'+
	'	                <div class="col-auto"><a class="btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>'+
	'	              </div>'+
	'	            </div>'+
	'	            <div class="card-body bg-light border-top">'+
	'				<div class="row">'+
	'				<div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-0">Identity</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<p class="font-weight-semi-bold mb-0">'+this.type_identity+'('+this.no_identity+')</p>'+
	'						</div>'+
	'					</div>'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-0">Full Name</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<p class="font-weight-semi-bold mb-0">'+this.full_name+'</p>'+
	'						</div>'+
	'					</div>'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-1">Phone number</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<p class="font-weight-semi-bold mb-0">'+this.phone_number+'</p>'+
	'						</div>'+
	'					</div>'+
	'					<div class="row">'+
	'						<div class="col-5 col-sm-4">'+
	'							<p class="font-weight-semi-bold mb-1">Email</p>'+
	'						</div>'+
	'						<div class="col">'+
	'							<a href="mailto:tony@gmail.com">'+this.email+'</a>'+
	'						</div>'+
	'					</div>'+
	'				</div>'+
	'				</div>'+
	'	            </div>'+
	'	          </div>';
	}else{
		idx=0;
		html2 += '<div class="card mb-3">'+
		'	            <div class="card-header">'+
		'	              <div class="row align-items-center">'+
		'	                <div class="col">'+
		'	                  <h5 class="mb-0">Contacts '+""+'</h5>'+
		'	                </div>'+
		'	                <div class="col-auto"><a class="btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>'+
		'	              </div>'+
		'	            </div>'+
		'	            <div class="card-body bg-light border-top">'+
		'				<div class="row">'+
		'				<div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-0">Identity</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.type_identity+'('+this.no_identity+')</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-0">Full Name</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.full_name+'</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-1">Phone number</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<p class="font-weight-semi-bold mb-0">'+this.phone_number+'</p>'+
		'						</div>'+
		'					</div>'+
		'					<div class="row">'+
		'						<div class="col-5 col-sm-4">'+
		'							<p class="font-weight-semi-bold mb-1">Email</p>'+
		'						</div>'+
		'						<div class="col">'+
		'							<a href="mailto:tony@gmail.com">'+this.email+'</a>'+
		'						</div>'+
		'					</div>'+
		'				</div>'+
		'				</div>'+
		'	            </div>'+
		'	          </div>';
//		}
		idx++;
	}
		});
//	});
	tbody.html(html);
	tbody2.html(html2);
}

function renderTourGuide(){
	var path=ctx + '/ticket/tour-guide';
	var id= $('.tour-guide.shadow-lg').data('id_tour_guide');
//	ajaxGET(path + '/'+id+'?action='+selected_action,'onRenderTourGuide','onActionError');
	ajaxGET(path + '/list','onRenderTourGuide','onActionError');
}
function onRenderTourGuide(resp){
	console.log(resp);
//	var value=resp.data;
//	$('#name_tour_guide').text(value.full_name);
//	$('#cost_tour_guide').text(value.cost_per_day);
//	$('#phone_tour_guide').text(value.phone_number);
//	$('#email_tour_guide').text(value.email);
//	renderPhoto();
	//
	var tbody=$('#container-guide');
	var html ='';
	var num=1;
	$.each(getDataArr().guide, function(key, temp_value){
		$.each(resp.data, function(key, value){
		if(temp_value.guide_id==value.id){
			total_price+=value.cost_per_day;
		html += '<div class="card mb-3">'+
		'    <div class="card-header">'+
		'        <div class="row align-items-center">'+
		'            <div class="col">'+
		'                <h5 class="mb-0">Tour Guide '+(num)+'</h5>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'    <div class="card-body bg-light border-top">'+
		'        <div class="row no-gutters">'+
		'            <div class="col-auto">'+
		'                <img id="photo_tour_guide" src="'+value.photo+'" class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" width="150">'+
		'            </div>'+
		'            <div class="col-md-8">'+
		'                <div class="card-body">'+
		'                    <h5 class="card-title" id="name_tour_guide">'+value.full_name+'</h5>'+
		'                    <strong class="card-text text-success"><span id="cost_tour_guide">IDR '+$.number(value.cost_per_day)+'</span><span>/Day</span></strong>'+
		'                    <div class="row card-text">'+
		'                        <div class="col-auto"><small class="text-muted">Phone : <span id="phone_tour_guide">'+value.phone_number+'</span></small></div>'+
		'                        <div class="col-auto"><small class="text-muted">Email : <span id="email_tour_guide">'+value.email+'</span></small></div>'+
		'                    </div>'+
		'                    <div class="row card-text">'+
		'                        <div class="col-auto"><small class="text-muted">Language : <span id="language_tour_guide">-</span></small></div>'+
		'                    </div>'+
		'                </div>'+
		'            </div>'+
		'        </div>'+
		'    </div>'+
		'</div>';
		num++;
		}
		});
	});
	tbody.html(html);
	$('#total-price-pay').text($.number(total_price));
}
function renderPhoto(){
	var path=ctx + '/files';
	ajaxGET(path + '/'+'list','onRenderPhoto','onActionError');
}
function onRenderPhoto(resp){
	var path='';
	var id_guide= $('.tour-guide.shadow-lg').data('id_tour_guide');
	var id_vehicle= $('.vehicle.border-primary').data('id_vehicle');
	console.log(resp);
	$.each(resp.data, function(key){
		if(this.reference_id==id_guide){
			path=ctx+"/files/"+this.id+"?filename="+this.name+"&download";
			$('#photo_tour_guide').attr('src', path);
		}
		if(this.reference_id==id_vehicle){
			path=ctx+"/files/"+this.id+"?filename="+this.name+"&download";
			$('#vehicle_photo').attr('src', path);
		}
	});
	return path;
}
function renderVehicle(){
	var path=ctx + '/ticket/vehicle';
//	var id= $('.vehicle.border-primary').data('id_vehicle');
	ajaxGET(path + '/list','onRenderVehicle','onActionError');
}
function onRenderVehicle(resp){
	console.log(resp);
//	var value=resp.data;
//	$('#vehicle_name').text(value.name);
//	$('#vehicle_cost').text(value.cost);
//	$('#vehicle_seat').text(value.seat_capacity);
//	$('#vehicle_suitcase').text(value.suitcase);
	//
	var tbody=$('#container-vehicle');
	var html ='';
	var num=1;
	$.each(getDataArr().vehicle, function(key, temp_value){
		$.each(resp.data, function(key, value){
		if(temp_value.vehicle_id==value.id){
			total_price+=value.cost*temp_value.qty_vehicle;
		html += '<div class="card mb-3">'+
		'           <div class="card-header">'+
		'             <div class="row align-items-center">'+
		'               <div class="col">'+
		'                 <h5 class="mb-0">Vehicle '+(num)+'</h5>'+
		'               </div>'+
		'             </div>'+
		'           </div>'+
		'           <div class="card-body bg-light border-top">'+
		'            <div class="row no-gutters">'+
		'		    <div class="col-md-4">'+
		'		      <img id="vehicle_photo" src="'+value.photo+'" class="card-img" alt="...">'+
		'		    </div>'+
		'		    <div class="col-md-8">'+
		'		      <div class="card-body">'+
		'		        <h5 class="card-title" id="vehicle_name">'+value.name+'</h5>'+
		'		        <strong class="card-text text-success"><span id="vehicle_cost">IDR '+$.number(value.cost)+'</span>/Day</strong>'+
		'		        <div class="row card-text">'+
		'					<div class="col-auto"><small class="text-muted">Seat : <span id="vehicle_seat">'+value.seat_capacity+'</span></small></div>'+
		'					<div class="col-auto"><small class="text-muted">Suitcase : <span id="vehicle_suitcase">'+value.suitcase+'</span></small></div>'+
		'		      	</div>'+
		'		        <div class="row card-text">'+
		'					<div class="col-auto"><small class="text-muted">QTY : <span id="vehicle_qty">'+temp_value.qty_vehicle+'</span></small></div>'+
		'		      	</div>'+
		'		    </div>'+
		'		  	</div>'+
		'           </div>'+
		'           </div>'+
		'           </div>';
		num++;
		}
		});
	});
	tbody.html(html);
	$('#total-price-pay').text($.number(total_price));
}
function onActionError(){
	alert("ERR");
}
