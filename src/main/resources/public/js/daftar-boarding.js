var pathsql = ctx + '/restql';
var path = ctx + '/ticket/purchase';
var path_upload = ctx + '/files/';
var selected_id = '';
var current_page = 1;
var selected_action = '';
var params = '';
var appendIds = [];

function init(){
	
	ajaxGET_NEW(pathsql + '/region', function(response){
		$.each(response.data, function(key,value){ 
			$('[name=filter_id_region]').append(new Option(value.name, value.id));
		});
	},'onGetListError');
	
	display();
	$('#btn-add').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearForm();
	});
	$('#btn-add-type').click(function(e){
		selected_id = '';
		selected_action = 'add';
		clearFormType();
	});
	
	$('#btn-modal-confirm-yes').click(function(e){
		if(selected_action == 'delete' ){
			doAction(selected_id, selected_action, 1);
		}
		
	});
	$('.table-responsive').on('show.bs.dropdown', function () {
	     $('.table-responsive').css( "overflow", "inherit" );
	});

	$('.table-responsive').on('hide.bs.dropdown', function () {
	     $('.table-responsive').css( "overflow", "auto" );
	})
	
	$('#form-filter').submit(function(e){
		display();
		$('#a-export').attr('href','export-data?'+$('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, ''));
		e.preventDefault();		
	});
	$('#btn-search').click(function(){
		display();
	});
}

function display(page = 1){
	
	$('#more-button-row').remove();
	$('#retry-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	// disable kondisi ini kalo pake metode refresh semua isi table
	if(params == '' || params != $('#form-filter').serialize() || page ==1){
		tbody.text('');
	}
	
	tbody.append('<tr id="loading-row"><td colspan="20"><div class="list-group-item text-center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Mohon Tunggu..</div></td></tr>');
	
	params = $('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
	console.log(path + '/list?page='+page+'&'+params);
	params +="&filter_status=PAYMENT"; //&filter_status_boarding=
	
	if($('[name=filter_keyword]').val()!='') params+="&filter_no_booking="+$('[name=filter_keyword]').val();
	
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	
	ajaxGET(path + '/list-boarding?page='+page+'&'+params,'onGetListSuccess','onGetListError');	
	current_page = page;
}

function onGetListSuccess(response){
	$.LoadingOverlay("hide");
	
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var row = "";
	var num = $('.data-row').length+1;
	$.each(response.data,function(key,value){
		if(!contains.call(appendIds, value.id)){
			row += renderRow(value, num);
			num++;
		}
		
	});
	if(response.next_more){
		row += '<tr id="more-button-row" class="more-button-row"><td colspan="20"><div align="center">';
		row += '	<button class="btn btn-outline-secondary" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
		row += '</div></td></tr>';
	}
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.append(row);
	
}

function fillFormValue(value){
	clearForm();
	$('[name=name]').val(value.name);
}
function save(){
	console.log('save');
	var obj = new FormData(document.querySelector('#entry-form'));
	if(selected_id != '') obj.append('id', selected_id);
    ajaxPOST(path + '/save',obj,'onModalActionSuccess','onModalActionError');
}


function onModalActionSuccess(response){
	$.LoadingOverlay("hide");
	// Kalo mau refresh semua data yang tampil di table
	// display();
	
	// Update data ke existing tabel
	if(selected_action == 'add'){
		appendRow(response.data);
	}else if(selected_action == 'edit'){
		updateRow(response.data);
	}else if(selected_action == 'delete'){
		removeRow(response.data);
	}else if(selected_action == 'PAYMENT' || selected_action == 'CANCEL') {
		updateRow(response.data);
		//$('#row-'+response.data.id).find('td span').html(response.data.status_name);
	}
	
	$('#modal-confirm').modal('hide');
	$('#modal-form').modal('hide');
	$('#modal-form-upload').modal('hide');
	selected_action = '';
	selected_id='';
	
	showAlertMessage(response.message, 1500);
}

function onModalActionError(response){
	$('#modal-form-msg').text(response.responseJSON.message);
	$('#modal-form-msg').show();
}
function appendRow(value){
	$('#loading-row').remove();
	$('#no-data-row').remove();
	
	var more = $('#more-button-row').html();
	$('#more-button-row').remove();
	
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('.data-row').length+1;
	var row = renderRow(value, num);
	
	tbody.append(row);

	appendIds.push(value.id);
	tbody.append('<tr id="more-button-row" class="more-button-row">'+more+'</tr>');
}

function updateRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	var num = $('#row-'+value.id+' td:first-child').text();
	var row = renderRow(value, num, 'replace');
	$('#row-'+value.id).html(row);
}

function removeRow(value){
	var tbody = $("#tbl-data").find('tbody');
	
	$('#row-'+value.id).remove();
	
	var i = 1;
	$('.data-row td:first-child' ).each(function() {
	  this.innerHTML = i++;
	});
}
function renderRow(value, num, method='add'){
	console.log(value);
	var row = "";
	if(method != 'replace') {
		row += '<tr class="data-row" id="row-'+value.id+'">';
	}
	row += '<td>'+(num)+'</td>';
	row += '<td>'+value.id+'</td>';
	row += '<td>'+value.code_booking+'</td>';
	row += '<td class=""> '+value.region.name +' </td>';
	
	row += '<td class=""> '+moment(value.start_date).format("ddd, DD MMMM YYYY")+' </td>';
	row += '<td class=""> '+moment(value.end_date).format("ddd, DD MMMM YYYY")+' </td>';
	row += '<td class=""> '+value.count_ticket_boarding +' orang <br/><button onclick="doAction(\''+value.id+'\',\'detil-identity-boarding\',\'0\');" class="btn btn-outline-dark btn-smalll btn-sm">detail</button></td>';
	
	var status_name=value.status_name.split(",")[0];
	var status_color=value.status_name.split(",")[1];
	var status_icon=value.status_name.split(",")[2];
	if(status_name=="WAITING") status_name="Waiting for Payment";
	if(status_name=="VERIFYING") status_name="Verifying Payment";
	
	row += '<td class="d-none"><span class="badge badge-pill badge-'+status_color+' text-white">'+status_name +' <i class="fas fa-'+status_icon+'"></i></span></td>';
	
	
	var statusBor_name="";
	var statusBor_color="";
	var statusBor_icon="";
	if(value.status_boarding==1) {
		statusBor_name="SEDANG_NAIK";
		console.log(moment(value.end_date).format("YYYY-MM-DD")+"IS AFTER?"+moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrBefore());
		//if(moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrBefore()) {
		if(moment(moment(value.end_date).format("YYYY-MM-DD")).isSameOrAfter(moment().format("YYYY-MM-DD"))){
			statusBor_color="primary";
		}/*else if(moment(moment(value.end_date).format("YYYY-MM-DD")).isAfter(moment().format("YYYY-MM-DD"))){
			statusBor_color="primary";
		}*/else {
			statusBor_color="danger";
			statusBor_icon="exclamation-circle";
		}
	}else if(value.status_boarding==2) {
		statusBor_name="SELESAI";
		statusBor_color="success"
	}else {
		statusBor_name="BELUM_BOARDING";
		if(moment(moment(value.end_date).format("YYYY-MM-DD")).isBefore()) {
			statusBor_color="danger"
			statusBor_icon="exclamation-circle"
		}else statusBor_color="warning"
		
	}
		
	console.log(moment(value.end_date).startOf('day').fromNow());        // a day ago
	console.log(moment(value.end_date).endOf('day').fromNow());           // in 2 hours
	console.log(moment(value.end_date).startOf('hour').fromNow());    
	console.log(moment(value.end_date).isBefore());   
	
	row += '<td class=""><span class="badge badge-pill badge-'+statusBor_color+' text-white">'+statusBor_name +' <i class="fas fa-'+statusBor_icon+'"></i></span></td>';
	
	
	row += '<td class="">';
	row += '<div class="dropleft position-static">';
	row += '<a href="javascript:void(0)" class="btn btn-sm btn-square btn-outline-info pill dropdown-toggles"  data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a> ';
	row += '<div class="dropdown-menu">';
	row +='      <a class="dropdown-item d-none" href="javascript:void(0)" onclick="doAction(\''+value.id+'\',\'view\',\'0\');">View Details</a>';
	
	if(value.status=="PAYMENT"){
		//row +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'TICKET\')">E-ticket</a>';
		
		if(value.status_boarding!=null && statusBor_name=="SELESAI") row +='      <a href="javascript:void(0)" class="dropdown-item" onclick="superDownload('+value.id+'\,\'SERTIFIKAT\')">E-Sertifikat</a>';
	}
	
	row +='      <a class="dropdown-item" href="../e-ticket?id='+value.id+'">Pemeriksaan</a>';
	row +='    </div>';
	row += '</div>';
	row += '</td>';
	
	if(method != 'replace') {
		row += '</tr>';
	}
	
	return row;
}

function addslashes(string) {
    return string.
    replace(/\\/g, '\\\\').
    replace(/\u0008/g, '\\b').
    replace(/\t/g, '\\t').
    replace(/\n/g, '\\n').
    replace(/\f/g, '\\f').
    replace(/\r/g, '\\r').
    replace(/'/g, '\\\'').
    replace(/"/g, '\\"');
}

function superDownload(url, mode = ""){
	console.log('original url: ', url);
    
	if(mode=="SERTIFIKAT") {
		url=ctx + "/export/e-sertifikat/"+url+"?nip=";
	}else if(mode=="TICKET") {
		url=ctx + "/export/e-ticket/"+url+"?nip=";
	}
	
	/*if(url.indexOf('?') > 0) url = url + '&';
    else url = url + '?';
    url = url.replace('export', 'export-to-mail');
    url += 'encrypt='+isEncrypt+'&';

    console.log('-- verify then download --');
    console.log('url before: ', url);
    console.log('url: ', url+"token="+"token");*/

    $.getJSON(url, function(response) {
		console.log(response);
        if (response.code == 200) {
            console.log('verify download OK');
            console.log(response.data);
            var path_download = ctx + '/export/download?filename='+addslashes(response.data);
            console.log('path_download: ', path_download);
            //$('#nameFileWillBeSentToEmail').html((response.data));
			//console.log(isEncrypt);
            //if(isEncrypt == false) {
               // window.open(path_download, '_blank');
                //location = path_download;
            //} else {
           //    // $('#modalFileWillBeSentToEmail').modal('show');
           // }
			window.open(path_download, '_blank');
        }else{
            //$('#verifyThenDownloadMsg').html(response.message);
            //$('#modalVerifyThenDownload').modal('show');
            //alert(response.message);
        }
    });
}

function  fillView(obj){
	console.log(obj);
	var element = $("#modal-view-only .modal-body .col-8 div.media-body")[0];
	element.children[0].innerHTML=": "+obj.region.name;
	element.children[1].innerHTML=": "+moment(obj.start_date).format("LLLL");
	var arrtim=[]; var arrketua=[];
	$.each(obj.purchase_detil_visitor, function(key){
		if(this.responsible=="1") arrketua.push(this.full_name);
		//if(this.responsible=="0") tim+=","+this.full_name;
		if(key > 1)arrtim.push(this.full_name);
	});
	var transit_camp = obj.purchase_detil_transit_camp[0];
	
		element.children[2].innerHTML=": "+transit_camp.name_transit_camp;
		element.children[3].innerHTML=": "+transit_camp.report_time_camp;
		element.children[4].innerHTML=": "+transit_camp.code_unique;
		element.children[5].innerHTML=": "+obj.count_ticket;
		element.children[6].innerHTML=": "+arrketua;
		element.children[7].innerHTML=": "+arrtim; 
		element.children[8].innerHTML=": "+$.number(obj.amount_total_ticket);
		element.children[9].innerHTML=": "+obj.code_booking;
	
	$("#modal-view-only").modal("show");
}

function fillIdentity(obj){
	console.log(obj);
	/*var element = $("#modal-view-only .modal-body .col-8 div.media-body")[0];
	element.children[0].innerHTML=": "+obj.region.name;
	element.children[1].innerHTML=": "+moment(obj.start_date).format("LLLL");
	var arrtim=[]; var arrketua=[];
	$.each(obj.purchase_detil_visitor, function(key){
		if(this.responsible=="1") arrketua.push(this.full_name);
		//if(this.responsible=="0") tim+=","+this.full_name;
		if(key > 1)arrtim.push(this.full_name);
	});
	element.children[5].innerHTML=": "+obj.count_ticket;
	element.children[6].innerHTML=": "+arrketua;
	element.children[7].innerHTML=": "+arrtim; 
	element.children[8].innerHTML=": "+$.number(obj.amount_total_ticket);*/
	
	var tbody = $("#tbl-identity").find('tbody');
	var row = "";
	var num = $('.data-row-identity').length+1;
	$.each(obj.purchase_detil_visitor, function(key){
		if(this.responsible==0){
		row += '<tr class="data-row-identity" id="row-id">'+
		'   <td>'+(key)+'</td>'+
		'   <td>'+this.visitorIdentity.no_identity+'</td>'+
		'   <td class=""> '+this.visitorIdentity.full_name+'</td>'+
		'   <td class=""> '+this.visitorIdentity.gender+'</td>'+
		'   <td class=""> '+this.address+' </td>'+
		'   <td class=""> '+this.phone_number+' </td>'+
		'   <td class="text-center"><a href="javascript:void(0)" onclick="remodal_view(\'/kawasan/files/'+this.visitorIdentity.id_file+'?filename='+this.visitorIdentity.file_name+'&download&decrypt\');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>'+
		'</tr>';
		num++;
		}
	});
	
	row == "" ? tbody.html('<tr id="no-data-row" class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
	
	$('#modal-detil-identity').modal('show');
}

function clearForm(){
	$('#entry-form')[0].reset();
	$('#modal-form-msg').hide();
}

function doAction(id, action, confirm = 0){
	$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
	selected_action = action;
	$('#modal-form-msg').hide();
	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
	else ajaxPOST(path + '/'+id+'/'+selected_action,{},'onModalActionSuccess','onActionError');
}

function onPrepareModalActionSuccess(response) {
	$.LoadingOverlay("hide");
	var value = response.data;
	selected_id = value.id;
	if(selected_action == 'edit'){
		fillFormValue(value);
		$('#modal-form').modal('show');
	}else if(selected_action == 'delete'){
		$('#modal-confirm-msg').html(response.message);
		$('#modal-confirm').modal('show');
	}else if(selected_action == 'view') {
		fillView(response.data);
	}else if(selected_action == 'detil-identity-boarding') {
		fillIdentity(response.data);
	}
	
}