var path = null;
var data = {};

var selected_id = '';
var selected_row = '';

var list_database_application = [];

function init(){
    
    if($.urlParam('id') != null) {
        path = URL.product.view.replace('{id}', $.urlParam('id'));
    } else {
        location='rating';
    }
    
    display();
    
}

function onGetProductSuccess(response, selected_id){
    console.log('product_id: ', response.data);
    $('[name=product_id]').empty().append(new Option('----- Select Product -----'));
    $.each(response.data, function(key, value) {
        $('[name=product_id]').append(new Option(value.name, value.id));
    });
    console.log('selected product_id: ', selected_id);
    if(selected_id != null) $('[name=product_id]').val(selected_id);
}

function onGetCommentatorSuccess(response, selected_id){
    console.log('commentator_id: ', response.data);
    $('[name=commentator_id]').empty();
    $('[name=commentator_id]').append(new Option('----- Select Commentator -----'));
    $.each(response.data, function(key, value) {
        $('[name=commentator_id]').append(new Option(value.name, value.id));
    });
    console.log('selected commentator_id: ', selected_id);
    if(selected_id != null) $('[name=commentator_id]').val(selected_id);
}

function display(page, params = '', timeout=0){


    // rating
    ajaxGET(path, function(response){
        console.log('/rating/api/.../list: ', response.data);
        data = response.data;
        
        if(data.rating_summary.toString().includes('.') == false) data.rating_summary = data.rating_summary + '.0';
        
        $('[name=product-name]').html(getValue(data.name));
        $('[name=rating-picture]').html(getRating(data.rating_summary));
        $('[name=rating-picture-large]').html(getRatingLarge(data.rating_summary));
        $('[name=rating-value]').html(getValue(data.rating_summary, 0));
        $('[name=rating-amount]').html(getValue(data.amount_rating, 0));

        $('[name=rating-1]').html(getValue(data.rating_1, 0));
        $('[name=rating-2]').html(getValue(data.rating_2, 0));
        $('[name=rating-3]').html(getValue(data.rating_3, 0));
        $('[name=rating-4]').html(getValue(data.rating_4, 0));
        $('[name=rating-5]').html(getValue(data.rating_5, 0));
        
        $('[name=rating-1-bar]').width(getValue(data.percentage_rating_1, 0));
        $('[name=rating-2-bar]').width(getValue(data.percentage_rating_2, 0));
        $('[name=rating-3-bar]').width(getValue(data.percentage_rating_3, 0));
        $('[name=rating-4-bar]').width(getValue(data.percentage_rating_4, 0));
        $('[name=rating-5-bar]').width(getValue(data.percentage_rating_5, 0));

        var comments = '';
        $.each(data.listRating, function(index, value){
            comments += '<div class="m-t-20">';
            comments += '  <div class="card">';
            comments += '    <div class="card-body">';
            comments += '      <div class="row">';
            comments += '        <div class="col-md-2">';
            comments += '          <img src="'+(value.picture_url==null?'https://image.ibb.co/jw55Ex/def_face.jpg':value.picture_url)+'" class="img img-rounded img-fluid"/>';
            comments += '          <p class="text-secondary text-center">'+moment(value.date).fromNow()+'</p>';
            comments += '        </div>';
            comments += '        <div class="col-md-10">';
            comments += '          <p>';
            comments += '           <a class="float-left" '+(value.commentator_url!=null?'href="'+value.commentator_url:'href="#" onclick="return false;"')+'"'+'><strong>'+value.commentator.name+'</strong></a>';
            comments += '            <span class="float-right"><i class="text-warning fa fa-star'+(value.value>=5?'':'-o')+'"></i></span>';
            comments += '            <span class="float-right"><i class="text-warning fa fa-star'+(value.value>=4?'':'-o')+'"></i></span>';
            comments += '            <span class="float-right"><i class="text-warning fa fa-star'+(value.value>=3?'':'-o')+'"></i></span>';
            comments += '            <span class="float-right"><i class="text-warning fa fa-star'+(value.value>=2?'':'-o')+'"></i></span>';
            comments += '            <span class="float-right"><i class="text-warning fa fa-star'+(value.value>=1?'':'-o')+'"></i></span>';
            comments += '           </p>';
            comments += '           <div class="clearfix"></div>';
            comments += '          <p>'+value.comment+'</p>';
            comments += '          <p>';
            comments += '            <button onclick="remove(\''+value.id+'\')" class="float-right btn text-white btn-danger ml-2">  <i class="fa fa-trash"></i> Delete</button>';
            //comments += '            <a class="float-right btn btn-outline-primary ml-2">  <i class="fa fa-reply"></i> Reply</a>';
            //comments += '            <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>';
            comments += '          </p>';
            comments += '        </div>';
            comments += '      </div>';
            comments += '    </div>';
            comments += '  </div>';
            comments += '</div>';
        });
        $('#div-comment').html(comments);
        
    });
}

function add(){
	selected_id = '';

	$('#form-label').text('Add');
	$('#frm').find("input[type=text], textarea, select, input[type=date]").val('');
	$('#frm').find("input[type=checkbox], input[type=radio]").prop("checked",false);

    $('[name=product_id]').empty().append(new Option('----- Select Product -----'));
    $('[name=commentator_id]').empty().append(new Option('----- Select Commentator -----'));
	$('#frm select option:first-child').prop("selected", true);
	$('[name=value][value=5]').prop("checked", true);

	$('#error_msg').hide();
	$('#modal-form').modal('show');
}

function save(){
	var obj = new FormData(document.querySelector('#frm'));
	if(selected_id != '') obj.append('id', selected_id);
    obj.append('product_id', $.urlParam('id'));
    obj.append('value', window.star);
    console.log(obj);
    ajaxPOST(URL.rating.save, obj, function(response){
        console.log(response);
        $('#modal-form').modal('hide');
        display(1);
        $('[name=comment]').val('');
    }, function(response){
        console.log('error saat save');
    });
}

function edit(id, row){
	selected_id = id;
	selected_row = row;
	console.log('id: ', id);
	$('#form-label').text('Edit');
	ajaxGET(URL.rating.view.replace('{id}', id), function(response){
	    console.log(response);
	    var value = response.data;
	    $('[name=database_application_id]').val(value.database_application_id);
        $('[name=product_id]').val(value.product_id);
        $('[name=commentator_id]').val(value.commentator_id);
	    $('[name=date]').datetimepicker({ defaultDate: getDateForInput(value.date) });
	    $('[name=date]').val(getDateForInput(value.date));
	    $('[name=value][value='+value.value+']').prop("checked", true);
	    $('[name=comment]').val(value.comment);

        var commentator_id = value.commentator_id;
        var database_application_id = value.database_application_id;
        ajaxGET(URL.rating.database.application.product.list.replace('{databaseApplicationId}', database_application_id), function(response){ onGetProductSuccess(response, value.product_id) });
        $.each(list_database_application, function(key, value){
            if(value.id == database_application_id) {
                ajaxGET(URL.rating.database.system.commentator.list.replace('{databaseSystemId}', value.database_system_id), function(response){ onGetCommentatorSuccess(response, commentator_id) });
                return false;
            }
        });
	    
	    $('#error_msg').hide();
	    $('#modal-form').modal('show');
	});
}

function remove(id, confirm = 0){
	$('#data-remove-id').data('id', id);
	if(id != null) selected_id = id;
	if(confirm == 0) ajaxGET(URL.rating.view.replace('{id}', id), function(response) {
	    var value = response.data;
	    console.log('selected_remove: ', value);
	    $('#data-remove-id').data('id', value.id_rating);
	    $('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus rating dan comment ini?</p>')
	    $('#modal-remove').modal('show');
	});
	else ajaxPOST(URL.rating.remove.replace('{id}', id),{}, function(response){
	    display(1);
	    $('#modal-remove').modal('hide');
	});
}



