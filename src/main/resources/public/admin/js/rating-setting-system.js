var selected_id = '';
var selected_row = '';

var list_database_application = [];

function init(){

    display(1, '', 0);

    // untuk database
    ajaxGET(URL.rating.database.system.list_database, function(response){
        console.log('list_database: ', response.data);
        list_database_application = response.data;
        $('[name=database_name]').empty();
        $('[name=database_name]').append(new Option('----- Select Database System -----'));
        $.each(response.data, function(key, value) {
            $('[name=database_name]').append(new Option(value, value));
        });
    });

    // untuk database table
    $('[name=database_name]').change(function(){
        var this_value = $(this).val();
        ajaxGET(URL.rating.database.system.list_table.replace('{databaseId}', this_value), function(response){
            console.log('list_table: ', response.data);
            $('[name=application_table_name], [name=user_table_name]').empty();
            $('[name=application_table_name], [name=user_table_name]').append(new Option('----- Select Table Name -----'));
            $.each(response.data, function(key, value) {
                $('[name=application_table_name], [name=user_table_name]').append(new Option(value, value));
            });
        });
        
        $('[name=application_column_name]').empty();
        $('[name=application_column_name]').append(new Option('----- Select Column Name -----'));
        $('[name=application_column_value]').empty();
        $('[name=application_column_value]').append(new Option('----- Select Column Value -----'));
        
        $('[name=user_table_column_id], [name=user_table_column_first_name], [name=user_table_column_last_name]').empty();
        $('[name=user_table_column_id]').append(new Option('----- Select Column ID -----'));
        $('[name=user_table_column_first_name]').append(new Option('----- Select First Name -----'));
        $('[name=user_table_column_last_name]').append(new Option('----- Select Last Name -----'));
    });

    // untuk database column application
    $('[name=application_table_name]').change(function(){
        var this_value = $(this).val();
        ajaxGET(URL.rating.database.system.list_column.replace('{databaseId}', $('[name=database_name]').val()).replace('{tableId}', this_value), function(response){
            console.log('list_column_application: ', response.data);
            $('[name=application_column_name]').empty();
            $('[name=application_column_name]').append(new Option('----- Select Column Name -----'));
            $.each(response.data, function(key, value) {
                $('[name=application_column_name]').append(new Option(value, value));
            });
        });
        $('[name=application_column_value]').empty();
        $('[name=application_column_value]').append(new Option('----- Select Column Value -----'));
    });
    
    $('[name=application_column_name]').change(function(){
        var this_value = $(this).val();
        ajaxGET(URL.rating.database.system.list_value.replace('{databaseId}', $('[name=database_name]').val()).replace('{tableId}', $('[name=application_table_name]').val()).replace('{columnId}', this_value), function(response){
            console.log('list_column_application: ', response.data);
            $('[name=application_column_value]').empty();
            $('[name=application_column_value]').append(new Option('----- Select Column Value -----'));
            $.each(response.data, function(key, value) {
                $('[name=application_column_value]').append(new Option(value, value));
            });
        });
    });

    // untuk database column account
    $('[name=user_table_name]').change(function(){
        var this_value = $(this).val();
        ajaxGET(URL.rating.database.system.list_column.replace('{databaseId}', $('[name=database_name]').val()).replace('{tableId}', this_value), function(response){
            console.log('list_column_application: ', response.data);
            $('[name=user_table_column_id], [name=user_table_column_first_name], [name=user_table_column_last_name]').empty();
            $('[name=user_table_column_id]').append(new Option('----- Select Column ID -----'));
            $('[name=user_table_column_first_name]').append(new Option('----- Select First Name -----'));
            $('[name=user_table_column_last_name]').append(new Option('----- Select Last Name -----'));
            $.each(response.data, function(key, value) {
                $('[name=user_table_column_id], [name=user_table_column_first_name], [name=user_table_column_last_name]').append(new Option(value, value));
            });
        });
    });
    
}

function refresh(){
    display(1);
}

function onGetCommentatorSuccess(response, selected_id){
    console.log('commentator_id: ', response.data);
    $('[name=commentator_id]').empty();
    $('[name=commentator_id]').append(new Option('----- Select Commentator -----'));
    $.each(response.data, function(key, value) {
        $('[name=commentator_id]').append(new Option(value.name, value.id));
    });
    console.log('selected: ', selected_id);
    if(selected_id != null) $('[name=commentator_id]').val(selected_id);
}

function display(page, params = '', timeout=0){
    var tbody = $("#tbl-data").find('tbody');
    $('.more-button-row').remove();
    tbody.append('<tr class="loading-row"><td colspan="20"><div align="center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Loading..</div></td></tr>');
    setTimeout(() => ajaxGET(URL.rating.database.system.list + '?page='+page+'&'+params,'onGetListSuccess','onGetListError'), timeout)
}

function onGetListSuccess(response){
    var tbody = $("#tbl-data").find('tbody');
    console.log('contoh data: ', response);
    console.log('contoh data: ', response.data[0]);
    console.log('contoh data: ', JSON.stringify(response.data[0]));
    var row = "";
    var num = 1;
    $.each(response.data,function(key,value){
        //console.log(value);
        row += '<tr class="data-row" id="row-'+value.id_rating+'">';
        row += '<td>'+num+'</td>';
        row += '<td>'+getValue(value.database_name)+'</td>';
        row += '<td>';
        row += '<b>Table Name: </b>'+getValue(value.application_table_name) + '<br/>';
        row += '<b>Column Name: </b>'+getValue(value.application_column_name) + '<br/>';
        row += '<b>Column Value: </b>'+getValue(value.application_column_value);
        row += '</td>';
        row += '<td>';
        row += '<b>Table Name: </b>'+getValue(value.user_table_name) + '<br/>';
        row += '<b>Column ID: </b>'+getValue(value.user_table_column_id) + '<br/>';
        row += '<b>First Name: </b>'+getValue(value.user_table_column_first_name) + '<br/>';
        row += '<b>Last Name: </b>'+getValue(value.user_table_column_last_name);
        row += '</td>';
        row += '<td nowrap>';
        //row += '<button type="button" class="btn btn-info" onclick="view(\''+value.id+'\')"><i class="fa fa-search"></i></button> ';
        row += '<button type="button" class="btn btn-warning" onclick="edit(\''+value.id+'\', \''+num+'\')"><i class="fa fa-pencil-square-o"></i></button> ';
        row += '<button type="button" class="btn btn-danger" onclick="remove(\''+value.id+'\')"><i class="fa fa-trash"></i></button>';
        row += '</td>';
        row += '</tr>';
        num++;
    });
    if(response.next_more){
        row += '<tr class="more-button-row"><td colspan="20"><div id="tblFooter" align="center">';
        row += '    <button id="btnMore" class="btn waves-effect green sh" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        row += '</div></td></tr>';
    }
    row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
    $('.loading-row').remove();
}

function add(){
    selected_id = '';

    $('#form-label').text('Add');
    $('#frm').find("input[type=text], textarea, select, input[type=date]").val('')
    $('#frm').find("input[type=checkbox], input[type=radio]").prop("checked",false);
    
    $("input[name='value'][value='5']").prop('checked', true);

    $('#error_msg').hide();
    $('#modal-form').modal('show');
}

function save(){
    var obj = new FormData(document.querySelector('#frm'));
    if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(URL.rating.database.system.save, obj, function(response){
        console.log(response);
        $('#modal-form').modal('hide');
        display(1);
    }, function(response){
        console.log('error saat save');
    });
}

function edit(id, row){
    selected_id = id;
    selected_row = row;
    console.log('id: ', id);
    $('#form-label').text('Edit');
    ajaxGET(URL.rating.database.system.view.replace('{id}', id), function(response){
        console.log(response);
        var value = response.data;
        $('[name=database_name]').val(value.database_name);
        $('[name=commentator_id]').val(value.commentator_id);
        console.log(value.data);
        $('[name=date]').datetimepicker({ defaultDate: getDateForInput(value.date) });
        $('[name=date]').val(getDateForInput(value.date));
        $('[name=value][value='+value.value+']').prop("checked", true);
        $('[name=comment]').val(value.comment);

        var database_name = value.database_name;
        var commentator_id = value.commentator_id;
        $.each(list_database_application, function(key, value){
            if(value.id == database_name) {
                ajaxGET(URL.rating.database.system.commentator.list.replace('{databaseSystemId}', value.database_name), function(response){ onGetCommentatorSuccess(response, commentator_id) });
                return false;
            }
        });
        
        $('#error_msg').hide();
        $('#modal-form').modal('show');
    });
}

function remove(id, confirm = 0){
    $('#data-remove-id').data('id', id);
    if(id != null) selected_id = id;
    if(confirm == 0) ajaxGET(URL.rating.database.system.view.replace('{id}', id), function(response) {
        var value = response.data;
        console.log('selected_remove: ', value);
        $('#data-remove-id').data('id', value.id_rating);
        $('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data rating bintang "'+value.value+'" ini?</p>')
        $('#modal-remove').modal('show');
    });
    else ajaxPOST(URL.rating.database.system.remove.replace('{id}', id),{}, function(response){
        display(1);
        $('#modal-remove').modal('hide');
    });
}



