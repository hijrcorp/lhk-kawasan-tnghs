var selected_id = '';
var selected_row = '';

function init(){

    display(1, '', 0);

    ajaxGET(URL.product.list, function(response){
        console.log('product_id: ', response.data);
        $('[name=product_id]').empty().append(new Option('----- Select Product -----'));
        $.each(response.data, function(key, value) {
            $('[name=product_id]').append(new Option(value.name, value.id));
        });
    });

    ajaxGET(URL.commentator.list, function(response){
        console.log('commentator_id: ', response.data);
        $('[name=commentator_id]').empty().append(new Option('----- Select Commentator -----'));
        $.each(response.data, function(key, value) {
            $('[name=commentator_id]').append(new Option(value.name, value.id));
        });
    });
    
}

function refresh(){
    display(1);
}

function display(page, params = '', timeout=0){
    var tbody = $("#tbl-data").find('tbody');
    $('.more-button-row').remove();
    tbody.append('<tr class="loading-row"><td colspan="20"><div align="center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Loading..</div></td></tr>');
    setTimeout(() => ajaxGET(URL.rating.list + '?page='+page+'&'+params,'onGetListSuccess','onGetListError'), timeout)
}

function onGetListSuccess(response){
    var tbody = $("#tbl-data").find('tbody');
    console.log('contoh data: ', response);
    console.log('contoh data: ', response.data[0]);
    console.log('contoh data: ', JSON.stringify(response.data[0]));
    var row = "";
    var num = 1;
    $.each(response.data,function(key,value){
        //console.log(value);
        row += '<tr class="data-row" id="row-'+value.id_rating+'">';
        row += '<td>'+num+'</td>';
        //row += '<td>'+getValue(value.database_table.database.name)+'.'+getValue(value.database_table.name)+'</td>';
        row += '<td>'+getValue(value.product.application)+'</td>';
        row += '<td>'+getValue(value.product.name)+'</td>';
        row += '<td>';
        row += '<b>Person: </b>'+ getValue(value.commentator.name)+'<br/>';
        row += '<b>Date: </b>'+ getDateTimeForList(value.date)+'<br/>';
        row += '<b>Rating: </b>'+ getRating(value.value)+'<br/>';
        row += '<b>Description: </b>'+ getValue(value.comment)+'<br/>';
        row += '</td>';
        row += '<td nowrap>';
        row += '<a href="./product-detail?id='+value.id+'&application='+value.product.application+'&product='+value.product.name+'" class="btn btn-info"><i class="fa fa-search"></i></a> ';
        row += '<button type="button" class="btn btn-warning" onclick="edit(\''+value.id+'\', \''+num+'\')"><i class="fa fa-pencil-square-o"></i></button> ';
        row += '<button type="button" class="btn btn-danger" onclick="remove(\''+value.id+'\')"><i class="fa fa-trash"></i></button>';
        row += '</td>';
        row += '</tr>';
        num++;
    });
    if(response.next_more){
        row += '<tr class="more-button-row"><td colspan="20"><div id="tblFooter" align="center">';
        row += '    <button id="btnMore" class="btn waves-effect green sh" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        row += '</div></td></tr>';
    }
    row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Tidak Ada.</div></td></tr>') : tbody.html(row);
    $('.loading-row').remove();
}

function add(){
    selected_id = '';

    $('#form-label').text('Add');
    $('#frm').find("input, textarea, select").not('input[type=hidden], input[type=checkbox], input[type=radio]').val('');
    $('#frm').find("input[type=checkbox], input[type=radio]").prop("checked",false);

    $('#frm select option:first-child').select2().prop("selected", true).select2();
    $('[name=value][value=5]').prop("checked", true);

    $('#modal-form-msg').hide();
    $('#modal-form').modal('show');
}

function save(){
    var obj = new FormData(document.querySelector('#frm'));
    if(selected_id != '') obj.append('id', selected_id);
    console.log(obj);
    ajaxPOST(URL.rating.save, obj, function(response){
        console.log(response);
        $('#modal-form').modal('hide');
        display(1);
    }, function(response){
        $('#modal-form-msg').text(response.responseJSON.message);
        $('#modal-form-msg').show();
    });
}

function edit(id, row){
    selected_id = id;
    selected_row = row;
    console.log('id: ', id);
    $('#form-label').text('Edit');
    ajaxGET(URL.rating.view.replace('{id}', id), function(response){
        console.log(response);
        var value = response.data;
        $('[name=product_id]').val(value.product_id).select2();
        $('[name=commentator_id]').val(value.commentator_id).select2();
        $('[name=date]').val(getDateForInput(value.date)).datetimepicker({ defaultDate: getDateForInput(value.date) });
        $('[name=value][value='+value.value+']').prop("checked", true);
        $('[name=comment]').val(value.comment);
        
        $('#modal-form-msg').hide();
        $('#modal-form').modal('show');
    });
}

function remove(id, confirm = 0){
    $('#data-remove-id').data('id', id);
    if(id != null) selected_id = id;
    if(confirm == 0) ajaxGET(URL.rating.view.replace('{id}', id), function(response) {
        var value = response.data;
        console.log('selected_remove: ', value);
        $('#data-remove-id').data('id', value.id_rating);
        $('#modal-remove-content').html('<p>Apakah anda yakin akan menghapus data rating <br/>"'+value.commentator.name+'" dengan value "'+value.date+'" ini?</p>')
        $('#modal-remove').modal('show');
    });
    else ajaxPOST(URL.rating.remove.replace('{id}', id),{}, function(response){
        display(1);
        $('#modal-remove').modal('hide');
    });
}

