//var path = ctx + '/rating';
var path_detail = ctx + '/rating-detail';
var path_group = ctx + '/group';
var path_plant = ctx + '/plant';
var path_account = ctx + '/account';
var path_action = ctx + '/action';
var path_checklist = ctx + '/checklist';
var path_export = ctx + '/export';
var urlRating = "./rating/";

var isNew = true;

selected_id = "";
selected_id_detail = "";
public_is_remove = false;
public_is_download_multi = false;
public_default_checklist = '';

list_technician = [];
list_checklist = [];

list_detail = [];
list_rating_for_download_weekly = [];

/* membersihkan value yang ada di dalem input, sekalian ngebersihin message (jika ada bekas error) */
function clearForm(){
    $('#frm').find("input[type=text], textarea, select, input[type=date]").val('');
    $('#frm').find("input[type=checkbox], input[type=radio]").prop("checked",false);
    $('[name=period]:first').prop('checked', true);
    
    $("#tbl-data-detail").find('tbody').html('<tr class="nodata"><td colspan="20"><div  align="center">Data Not Found.</div></td></tr>');
    
    clearMessage();
}

function clearFormDetail(){

    $('#frmDetail').find("input[type=text], textarea, select, input[type=date]").val('');
    $('#frmDetail').find("input[type=checkbox], input[type=radio]").prop("checked",false);
    $('[name=result]:first').prop('checked', true);
    $('[name=method]:first').prop('checked', true);
    
    clearMessageDetail();
}

function init(){
    
    display();
    
    // request data and display with table view
//    ajaxGET(path_group + '/list?schedule='+$.urlParam('period'),'onGetGroupSuccess','onGetGroupError');
//    ajaxGET(path_plant + '/list?filter_checklist=1','onGetPlantSuccess','onGetPlantError');
//    ajaxGET(path_plant + '/list','onGetAllPlantSuccess','onGetPlantError');
//    ajaxGET(path_account + '/list/page/0','onGetTechnicianSuccess','onGetAccountError');
//    ajaxGET(path_action + '/list','onGetActionSuccess','onGetActionError');
    
    if($.urlParam('page') == 'report')
    $('[name=filter_period]').change(function(){
       if($(this).val() == 'WEEKLY') $('#div-filter-month').show();
       else $('#div-filter-month').hide();
    });
    
    $('[name=saveType]').change(function(){
        if($(this).val() == 'NEW') {
            $('#newSaveAsChecklist').show();
            $('#replaceSaveAsChecklist').hide();
            $('#ifNewIsCheck').show();
        }
        else {
            $('#newSaveAsChecklist').hide();
            $('#replaceSaveAsChecklist').show();
            $('#ifNewIsCheck').hide();
        }
    });


    if($.urlParam('period') == 'weekly' || $.urlParam('period') == 'monthly') {
        selected_id = getUUIDString();
        if($.urlParam('id') != null) {
            isNew = false;
            setTimeout(function(){
                edit($.urlParam('id'));
            }, 500);
        }
    }
    
    // halo
    
    /* ini tambahan parameter setting untuk dialog/modal window agar tidak ketutup pada saat klik di luar window */
    $('#modalForm, #modalFormDetail, #modalFormSaveAsChecklist, #modalFormLoadChecklist').modal({
        keyboard: true,
        backdrop: "static",
        show:false,
        
    });

    $('#modalconfirm').modal({
        keyboard: true,
        backdrop: "static",
        show:false,
        
    }).on('shown.bs.modal', function (e) {
        //setTimeout(function(){$('#modalconfirm').modal('hide')}, 10) -- ini cara untuk bikin show message with auto hide
    });
    
    /* ini fungsi tombol delete */
    $('body').on('click', "button[name='btnDelete']", function (){
        var id = $(this).data('id');
        $('#modalconfirm').data('id',id);   
        $('#modalconfirm').modal('show');
    });
    $('body').on('click', "button[name='btnDeleteDetail']", function (){
        var id = $(this).data('id');
        $('#modalconfirmDetail').data('id',id);   
        $('#modalconfirmDetail').modal('show');
    });
    
    $("#btnConfirm").click(function() {
        var id = $('#modalconfirm').data('id');
        ajaxPOST(URL.rating.database.delete.replace('{id}', id),{},'onRemoveSuccess','onRemoveError');
    });
    
    $("#btnConfirmDetail").click(function() {
        var id = $('#modalconfirmDetail').data('id');
        ajaxPOST(path_detail + '/'+id+'/delete',{},'onRemoveDetailSuccess','onRemoveDetailError');
    });

    /* ini fungsi tombol refresh untuk meretrieve ulang data sekaligus membersihkan filter (jika ada) */
    $("#btnRefresh").click(function() {
        display();
    });
    
    $("#btnRefreshDetail").click(function() {
        displayDetail();
    });

    $("#frmfind").submit(function() {
        var params = "?name="+$('#srcName').val();
        params = params + "&filterName="+$('#listName').val();
        var url = urlRating+"listFil"+params;
        display(url);
        return false;
    });

}

function onGetGroupSuccess(response){
//    console.log('group', path_group + '/list');
//    console.log('group response: ', response);
//    console.log('group response.data: ', response.data);
    $("[name=group_id], [name=group_id_view], [name=filter_group_id], #group_id").empty();
    $("[name=group_id], [name=group_id_view], [name=filter_group_id], #group_id").append(new Option('-- Select One --', ''));
    $.each(response.data, function(key, value) {
        if($.urlParam('period') != null) {
            if($.urlParam('period').toLocaleLowerCase() == value.schedule.toLocaleLowerCase()) {
                $("[name=group_id], [name=group_id_view], [name=filter_group_id], #group_id").append(new Option(value.name, value.id));
            }
        } else {
            $("[name=group_id], [name=group_id_view], [name=filter_group_id], #group_id").append(new Option(value.name, value.id));   
        }
    });
    
    if(response.data.length == 1 && $("[name=group_id]").length > 0) $("[name=group_id]")[0].selectedIndex = 1;
}

function onGetAllPlantSuccess(response){
//    console.log('plant', path_group + '/list');
//    console.log('plant response: ', response);
//    console.log('plant response.data: ', response.data);
    $("#plant_id").empty();
    $("#plant_id").append(new Option('-- Select One --', ''));
    $.each(response.data, function(key, value) {
        $("#plant_id").append(new Option(value.name, value.id));
    });
}

function onGetPlantSuccess(response){
//    console.log('plant', path_group + '/list');
//    console.log('plant response: ', response);
//    console.log('plant response.data: ', response.data);
    $("[name=plant_id], [name=plant_id_view], [name=filter_plant_id], #plant_id").empty();
    $("[name=plant_id], [name=plant_id_view], [name=filter_plant_id], #plant_id").append(new Option('-- Select One --', ''));
    $.each(response.data, function(key, value) {
        $("[name=plant_id], [name=plant_id_view], [name=filter_plant_id], #plant_id").append(new Option(value.name, value.id));
    });
}

function onGetTechnicianSuccess(response){
//    console.log('technician' + path_account + '/list/page/0');
//    console.log('technician response: ', response);
//    console.log('technician response.data: ', response.data);
    list_technician = response.data;
    $("[name=person_id], [name=person_id_view], [name=filter_person_id]").empty();
    $("[name=person_id], [name=person_id_view], [name=filter_person_id]").append(new Option('-- Select One --', ''));
    $.each(response.data, function(key, value) {
        $("[name=person_id], [name=person_id_view], [name=filter_person_id]").append(new Option(value.first_name + ' ' + value.last_name, value.id));
    });
    if(response.data.length == 1 && $("[name=person_id]").length > 0) $("[name=person_id]")[0].selectedIndex = 1;
}

function onGetActionSuccess(response){
//    console.log('action', path_account + '/list');
//    console.log('action response: ', response);
//    console.log('action response.data: ', response.data);
    $("[name=action_id]").empty();
    $("[name=action_id]").append(new Option('-- Select One --', ''));
    $.each(response.data, function(key, value) {
        $("[name=action_id]").append(new Option(value.name, value.id));
    });
}

function reset() {
    if($.urlParam('period') != 'weekly') $('#div-filter-month').hide();
    else $('#div-filter-month').show();
    $('[name=filter_keyword]').val('');
    $('[name=filter_group_id]').val('');
    $('[name=filter_plant_id]').val('');
    $('[name=filter_person_id]').val('');
    $('[name=filter_date_begin]').val('');
    $('[name=filter_date_end]').val('');
    $.each($('[name=filter_period]'), function(){
        $(this).prop('checked', true);
    });
}

function getParameters() {

    var params = '';
    if($('[name=filter_keyword]').val() != undefined) {
        params += 'filter_keyword='+$('[name=filter_keyword]').val();
    }
    if($('[name=filter_group_id]').val() != undefined && $('[name=filter_group_id]').val() != '') {
        params += '&filter_group_id='+$('[name=filter_group_id]').val();
    }
    if($('[name=filter_plant_id]').val() != undefined && $('[name=filter_plant_id]').val() != '') {
        params += '&filter_plant_id='+$('[name=filter_plant_id]').val();
    }
    if($('[name=filter_person_id]').val() != undefined && $('[name=filter_person_id]').val() != '') {
        params += '&filter_person_id='+$('[name=filter_person_id]').val();
    }
    if($('[name=filter_date_begin]').val() != undefined && $('[name=filter_date_begin]').val() != '') {
        params += '&filter_date_begin='+$('[name=filter_date_begin]').val();
    }
    if($('[name=filter_date_end]').val() != undefined && $('[name=filter_date_end]').val() != '') {
        params += '&filter_date_end='+$('[name=filter_date_end]').val();
    }
    if($('[name=filter_period]').val() != undefined) {
        var listPeriod = [];
        $.each($("input[name='filter_period']:checked"), function(){            
            listPeriod.push($(this).val());
        });
        params += '&filter_period='+listPeriod.join(',');
    }
    if($.urlParam('page') == 'report') {
        params += '&year='+$('[name=filter_year]').val();
        if($("input[name='filter_period']:checked").val() == 'WEEKLY') {
            params += '&month='+$('[name=filter_month]').val();
        }
    }
    params += '&filter_period='+$.urlParam('period');
    params += '&year='+$('[name=filter_year]').val();
    params += '&list_rating_for_download_weekly='+JSON.stringify(list_rating_for_download_weekly);
    if($('[name=filter_month]').val() != null) params += '&month='+$('[name=filter_month]').val();
    return params;
}

function display(page, params = '', is_remove = true){
    
    clearMessageFilter();
    
    public_is_remove = is_remove;
    
    //params = getParameters();
    
    if(page == null) page = 1;
    if($.urlParam('page') == 'report') page = 0; 
    
    var tbody = $("#tbl-data").find('tbody');
    $('.more-button-row').remove();
    tbody.append('<tr class="loading-row"><td colspan="20"><div align="center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Loading..</div></td></tr>');
    console.log(URL.rating.database.list + '?page='+page+'&'+params);
    ajaxGET(URL.rating.database.list + '?page='+page+'&'+params,'onGetListSuccess','onGetListError');
}

function onGetListSuccess(response){
    var tbody = $("#tbl-data").find('tbody');
    console.log('contoh data: ', response);
    console.log('contoh data: ', response.data[0]);
    console.log('contoh data: ', JSON.stringify(response.data[0]));
    var row = "";
    var num = 1;
    $.each(response.data,function(key,value){
        //console.log(value);
        row += render_row(value, num);
        if(value.id != null){
            num++;
        }
    });
    if(response.next_more){
        row += '<tr class="more-button-row"><td colspan="20"><div id="tblFooter" align="center">';
        row += '    <button id="btnMore" class="btn waves-effect green sh" onclick="display('+response.next_page_number+', '+null+', '+false+')">Tampilkan Selanjutnya..</button>';
        row += '</div></td></tr>';
    }
    if(row == "") {
        tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Not Found.</div></td></tr>');
    } else {
        if(public_is_remove == true) {
            tbody.html(row);
        } else {
            tbody.html(tbody.html()+row);
        }
    }
    $('.loading-row').remove();
    if($.urlParam('page') == 'report'/* && $('[name=filter_period]:checked').val() == 'MONTHLY'*/) {
        $('[id=list-date]').hide();
        $('[id=list-period]').hide();
    } else {
        $('[id=list-date]').show();
        $('[id=list-period]').hide();
    }
}

function render_row(value, num){
    var row = "";
    var rowDetail = '';
    var list_table = [];

    if(value.list_database_table[0].id != null && value.list_database_table.length > 0) {
        rowDetail += '<table class="" style="margin-top: .4rem">';
        rowDetail += '<tr style="border-width: 0px;">';
        rowDetail += '<th style="border-width: 0px; padding: .1rem 0 0 .75rem; text-align: left;">No</th>';
        rowDetail += '<th style="border-width: 0px; padding: .1rem 0 0 .75rem; text-align: left;">Code</th>';
//        rowDetail += '<th style="border-width: 0px; padding: .1rem 0 0 .75rem; text-align: left;">Table Name</th>';
//        rowDetail += '<th style="border-width: 0px; padding: .1rem 0 0 .75rem; text-align: left;">Column Id</th>';
//        rowDetail += '<th style="border-width: 0px; padding: .1rem 0 0 .75rem; text-align: left;">Column Name</th>';
        rowDetail += '<th style="border-width: 0px; padding: .1rem 0 0 .75rem; text-align: left;">API</th>';
        rowDetail += '</tr>';
        $.each(value.list_database_table, function(key2, value2){
            rowDetail += '<tr style="border-width: 0px;">';
            rowDetail += '<td style="border-width: 0px; padding: 0 0 0 .75rem; text-align: center;">'+(key2+1)+'.</td>';
            rowDetail += '<td style="border-width: 0px; padding: 0 0 0 .9rem; text-align: left;">'+value2.code+'</td>';
//            rowDetail += '<td style="border-width: 0px; padding: 0 0 0 .9rem; text-align: left;">'+value2.name+'</td>';
//            rowDetail += '<td style="border-width: 0px; padding: 0 0 0 .9rem; text-align: left;">'+value2.column_id+'</td>';
//            rowDetail += '<td style="border-width: 0px; padding: 0 0 0 .9rem; text-align: left;">'+value2.column_name+'</td>';
            rowDetail += '<td style="border-width: 0px; padding: 0 0 0 .9rem; text-align: left;">' + window.location.origin + ctx + value2.apiUrl + '</td>';
            rowDetail += '</tr>';
            list_table.push('<b></b>');
        });
        rowDetail += '</table>';
    }
    row += '<tr class="data-row" id="row-'+value.id+'">'; 
    row += '<td '+num+'><center>'+num+'</center></td>';
    row += '<td>'+getValue(value.name)+''+rowDetail+'</td>';
    //row += '<td>'+getValue(value.remarks)+'</td>';
    row += '<td nowrap style="vertical-align: top; width: 1%;">';
    //row += '<button onClick="view(\''+value.id+'\');" class="btn btn-info"><i class="fas fa-search" aria-hidden="true"></i></button> ';
    row += '<button onClick="edit(\''+value.id+'\');" class="btn btn-warning btn-pencil"><i class="fas fa-pencil-square-o white" aria-hidden="true"></i></button> ';
    row += '<button onClick="delete(\''+value.id+'\');" class="btn btn-danger"><i class="fas fa-trash" aria-hidden="true"></span></button>';
    row += '</td>';
    row += '</tr>';
    return row;
}

function view(id) {
    selected_id = id;
    var n = new Date().getTime();
    displayDetail();
    $.getJSON(path+'/'+selected_id+'?'+n, function(response, status) {
        console.log('rating.response: ', response);
        console.log('rating.response.data: ', response.data);
        //console.log('rating.response.data.json: ', JSON.stringify(response.data));

        window.obj = response.data;
        window.data = response.data;

        var date = new Date(data.date);
        var strDate = date.getFullYear() + '-' + pad(date.getMonth()+1, 2) + '-' + pad(date.getDate(), 2);

        $("[name=group_id_view]").val(data.group_id);
        $("[name=date_view]").val(strDate);
        $("[name=plant_id_view]").val(data.plant_id);
        $("[name=person_id_view]").val(data.person_id);
        $("[name=period_view][value='"+data.period+"']").prop('checked', true);
        $("[name=period_description_view]").val(data.period_description);
        $("[name=remarks_view]").val(data.remarks);
        
        $('#modalView').modal('show');
    }); 
}


function search() {
    display();
}

function downloadMulti() {
    public_is_download_multi = true;
    display();
    /*var params = getParameters();
    var url = path_export+'/rating?'+params
    console.log('url download multi: ', url);
    window.location.href = url;*/
}

function downloadOne(id) {
    if(id == null) id = selected_id;
    var url = path_export+'/rating/'+id;
    console.log('url download one: ', url);
    window.location.href = url;
}


/* ini fungsi tombol edit di list data table dengan nampilin dialog/modal window */
function edit(id) {
    isNew = false;
    selected_id = id;
    clearForm();
    var path = URL.rating.database.view.replace('{id}', selected_id);
    console.log('edit path: ', path);
    $.getJSON(path, function(response, status) {
        console.log('rating.response: ', response);
        console.log('rating.response.data: ', response.data);
        if(response.data == null) {
            location='rating?period='+$.urlParam('period');
        }
        //console.log('rating.response.data.json: ', JSON.stringify(response.data));
        $('#myModalLabel').html("Update Rating");
        $('#btnSave').html('Update');

        window.obj = response.data;
        window.data = response.data;

        var date = new Date(data.date);
        var strDate = date.getFullYear() + '-' + pad(date.getMonth()+1, 2) + '-' + pad(date.getDate(), 2);

        $("[name=group_id]").val(data.group_id);
        $("[name=date]").val(strDate);
        $("[name=plant_id]").val(data.plant_id);
        $("[name=person_id]").val(data.person_id);
        $("[name=period][value='"+data.period+"']").prop('checked', true);
        $("[name=period_description]").val(data.period_description);
        $("[name=remarks]").val(data.remarks);
        
        $.each(list_technician, function(key, value){
           if(value.id == data.person_id) {
               $("[name=person_id_readonly]").val(value.full_name);
           } 
        });
        
        $('#modalForm').modal('show');
    }); 
    displayDetail();
}


function validationMessage(isValidateFinalSave=true){
    var msg = '';
    if ($("[name=group_id]").val() == '') {
        msg += '* Service Rating cannot be empty<br />';
    }
    if ($("[name=plant_id]").val() == '') {
        msg += '* Area Rating cannot be empty<br />';
    }
    if ($("[name=date]").val() == '') {
        msg += '* Standard Rating cannot be empty<br />';
    }
    if ($("[name=person_id]").val() == '' && isValidateFinalSave == true) {
        msg += '* Technician Rating cannot be empty<br />';
    }
    if (list_detail.length <= 0) {
        msg += '* List Area Rating cannot be empty<br />';
    }
    if ($("[name=period]").val() == '') {
        msg += '* Period Rating cannot be empty<br />';
    }
    
//    if ($("[name=period_description]").val() == '' && isValidateFinalSave == true) {
//        msg += '* Period Description Rating cannot be empty<br />';
//    }
    var isValidDetail = false;
    $.each(list_detail, function(key, value){
        if(value.result == null) {
            isValidDetail = true;
        }
    });
    if(isValidDetail == true && isValidateFinalSave == true) {
        msg += '* Area Follow Up (Action) and Result cannot be empty<br />';
    }
    return msg;
}

function back(){
    if(confirm('Are you sure want to back?')) {
        location='rating?period='+$.urlParam('period');
    }
}

function save(){

    var msg = validationMessage();

    if (msg != '') {
        addAlert("alert-danger", msg);
    } else {
        console.log('[save rating detil]');
        console.log('header_id: ', selected_id);
        console.log('================================================');
        clearMessage();
        var obj = new FormData(document.querySelector('#frm'));
        obj.append('id', selected_id);
        obj.append('is_new', isNew);
        obj.append('status', 'SUBMITTED');
        console.log(obj);
        ajaxPOST(path + '/save',obj,'onSaveSuccess','onSaveError');
    }
}

function onSaveSuccess(response){
    console.log(response);
    $('#modalForm').modal('hide');
    //display(1);
    alert('Rating data successfully submitted.');
    location='rating?period='+$.urlParam('period');
}



function saveDetail(){
    
    var listMethod = [];
    $.each($("input[name='method']:checked"), function(){            
        listMethod.push($(this).val());
    });

    var msg = '';
    if ($("[name=area]").val() == '') {
        msg += '* Area Rating cannot be empty<br />';
    }
    if ($("[name=point]").val() == '') {
        msg += '* Check Point Rating cannot be empty<br />';
    }
    if ($("[name=standard]").val() == '') {
        msg += '* Standard Rating cannot be empty<br />';
    }
    if ($("[name=tools]").val() == '') {
        msg += '* Tools Rating cannot be empty<br />';
    }
    if (listMethod.length <= 0) {
        msg += '* Method Rating cannot be empty<br />';
    }
    if ($("[name=action_id]").val() == '') {
        //msg += '* Follow Up (Action) Rating cannot be empty<br />';
    }
    if ($("[name=result]").val() == '') {
        msg += '* Result Rating cannot be empty<br />';
    }

    if (msg != '') {
        addAlertDetail("alert-danger", msg);
    } else {
        console.log('[save rating detil]');
        console.log('header_id: ', selected_id);
        console.log('================================================');
        clearMessageDetail();
        var obj = new FormData(document.querySelector('#frmDetail'));
        if(selected_id_detail != '') obj.append('id', selected_id_detail);
        obj.append('header_id', selected_id);
        obj.append('is_new', isNew);
        obj.delete('method');
        obj.append('method', listMethod.join(','));
        console.log(obj);
        ajaxPOST(path_detail + '/save',obj,'onSaveDetailSuccess','onSaveDetailError');
    }
}

function onSaveDetailSuccess(response){
    console.log(response);
    $('#modalFormDetail').modal('hide');
    displayDetail();
}

/* ini fungsi tombol New untuk tambah data baru dengan nampilin dialog/modal window */
function add(){
    clearForm();
    $('#modalForm').data('id',0);
    $('#myModalLabel').html("Add Rating");
    $('#btnSave').html('Save');
    $('#modalForm').modal('show');
    isNew = true;
    selected_id = getUUIDString();
    
    if($("[name=person_id]").find('option').length == 2) {
        $("[name=person_id]").val($($("[name=person_id]").find('option')[1]).val());
    }
    
};

function addDetail(){
    clearFormDetail();
    selected_id_detail = "";
    $('#modalFormDetail').data('id',0);
    $('#myModalLabelDetail').html("Add Check Point");
    $('#btnSaveDetail').html('Save');
    $('#modalFormDetail').modal('show');
};

function onRemoveSuccess(response){
    $('#modalconfirm').modal('hide');
    $('#btnRefresh').trigger('click');
}

function onRemoveDetailSuccess(response){
    displayDetail();
    $('#modalconfirmDetail').modal('hide');
    $('#btnRefreshDetail').trigger('click');
}

    /* Fungsi umum untuk menampilkan tombol-tombol paging halaman 
    url: end point retrieve data
    pagecount: jumlah halaman dari banyaknya data
    activepage: halaman ke - n yang sedang ditampilkan datanya
    rowcount: jumlah record per halaman yang ingin ditampilkan
*/
function createPagination(url, q, pagecount, activepage, rowcount) {
    var html = '';
    $('.pagination').text('');

    if (rowcount == 0) {
        html += '<li class="disabled"><span aria-hidden="true">&laquo;</span> </li>';
        html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
    } else {
        if (activepage == 1) {
            html += '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
        } else {
            html += '<li><a href="javascript:display(\'' + url + (activepage - 1) + q + '\')" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a></li>';
        }
        var i;
        for ( i = 1; i <= pagecount; i++) {

            if (activepage == i) {
                html += '<li class="active"><a href="javascript:display(\'' + url + i + q + '\')">' + i + '</a></li>';
            } else {
                html += '<li><a href="javascript:display(\'' + url + i + q + '\')">' + i + '</a></li>';
            }
        }
        if (activepage == pagecount) {
            html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
        } else {
            html += '<li><a href="javascript:display(\'' + url + (activepage + 1) + q + '\')" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
        }
    }

    $('.pagination').append(html);

}

function clearMessage(){
    $('#msg').hide();
    $('#msg').removeClass("alert-success");
    $('#msg').removeClass("alert-danger");
    $('#msg').text('');
}

function clearMessageDetail(){
    $('#msg-detail').hide();
    $('#msg-detail').removeClass("alert-success");
    $('#msg-detail').removeClass("alert-danger");
    $('#msg-detail').text('');
}

function clearMessageFilter(){
    $('#msg-filter').hide();
    $('#msg-filter').removeClass("alert-success");
    $('#msg-filter').removeClass("alert-danger");
    $('#msg-filter').text('');
}

/*
 * Fungsi umum yang digunakan untuk menampilkan alert di page
 * jika terjadi error tampilkan background merah dan hijau jika sukses
 */
function addAlert(type, message) {
    clearMessage();
    $('#msg').addClass(type);
    var premsg = '';
    if (type == 'alert-success') {
        premsg = '<strong>Success!</strong><br />';
    }
    if (type == 'alert-danger') {
        premsg = '<strong>Error!</strong><br />';
    }
    $('#msg').append(premsg + message);
    $('#msg').show();
}

function addAlertDetail(type, message) {
    clearMessageDetail();
    $('#msg-detail').addClass(type);
    var premsg = '';
    if (type == 'alert-success') {
        premsg = '<strong>Success!</strong><br />';
    }
    if (type == 'alert-danger') {
        premsg = '<strong>Error!</strong><br />';
    }
    $('#msg-detail').append(premsg + message);
    $('#msg-detail').show();
}

function addAlertFilter(type, message) {
    clearMessageFilter();
    $('#msg-filter').addClass(type);
    var premsg = '';
    if (type == 'alert-success') {
        premsg = '<strong>Success!</strong><br />';
    }
    if (type == 'alert-danger') {
        premsg = '<strong>Download Error!</strong><br />';
    }
    $('#msg-filter').append(premsg + message);
    $('#msg-filter').show();
}

monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function formatDate(date) {
  var retval = date.getDate() + " " + monthNames[date.getMonth()] + ' ' + date.getFullYear() ;
  return retval;
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}










function displayDetail(){
    var tbody = $("#tbl-data-detail").find('tbody');
    $('.more-button-row-detail').remove();
    tbody.append('<tr class="loading-row"><td colspan="20"><div align="center"><img width="20" src="'+ctx+'/images/loading-spinner.gif"/> Loading..</div></td></tr>');
    params = "filter_header_id="+selected_id;
    console.log(path_detail + '/list?'+params);
    ajaxGET(path_detail + '/list?&'+params,'onGetListDetailSuccess','onGetListDetailError');
}

function onGetListDetailSuccess(response){
    var tbody = $("#tbl-data-detail").find('tbody');
    var tbodyView = $("#tbl-data-view").find('tbody');
    console.log('contoh data: ', response);
    console.log('contoh data: ', response.data[0]);
    console.log('contoh data: ', JSON.stringify(response.data[0]));
    list_detail = response.data;
    var row = "";
    var num = 1;
    $.each(response.data,function(key,value){
        //console.log(value);
        row += render_row_detail(value, num);
        num++;
    });
    if(response.next_more){
        row += '<tr class="more-button-row"><td colspan="20"><div id="tblFooter" align="center">';
        row += '    <button id="btnMore" class="btn waves-effect green sh" onclick="display('+response.next_page_number+')">Tampilkan Selanjutnya..</button>';
        row += '</div></td></tr>';
    }
    row == "" ? tbody.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Not Found.</div></td></tr>') : tbody.html(row);
    row == "" ? tbodyView.html('<tr class="nodata"><td colspan="20"><div  align="center">Data Not Found.</div></td></tr>') : tbodyView.html(row);
    tbody.find('.loading-row').remove();
    tbodyView.find('.loading-row').remove();
    tbodyView.find('button').remove();
    tbodyView.find('[id=btn-action]').remove();
}

function render_row_detail(value, num){
    var row = "";
    
    var listMethod = [];
    if(value.method != null) {
        arrayMethod = value.method.split(',');
        $.each(arrayMethod, function(index, value){
            if(value == 'LIHAT') listMethod.push('<img src="'+ctx+'/images/eye.png" width="30px" />');
            else if(value == 'DENGAR') listMethod.push('<img src="'+ctx+'/images/ear.png" width="30px" />');
            else if(value == 'FISIK') listMethod.push('<img src="'+ctx+'/images/hand.png" width="30px" />');
        });
    }
    
    row += '<tr class="data-row" id="row-'+value.id_kategori_atribut_detil+'">'; 
    row += '<td '+num+'><center>'+num+'</center></td>';
    row += '<td>'+getValue(value.area)+'</td>';
    row += '<td>'+getValue(value.point)+'</td>';
    row += '<td>'+getValueNewLine(value.standard)+'</td>';
    row += '<td nowrap>'+getValue(listMethod.join('<br/>'))+'</td>';
    row += '<td>'+getValueNewLine(value.tools)+'</td>';
    row += '<td>'+getValue(value.actionObject.name)+'</td>';
    row += '<td nowrap><span class="badge" style="background-color: '+(value.result=='GOOD'?'green">GOOD':'red">'+getValue(value.result).replace('_', ' '))+'</span></td>';
    //row += '<td>'+getValue(value.remarks)+'</td>';
    row += '<td id="btn-action" nowrap style="vertical-align: top;">';
    row += '<button type="button" onClick="editDetail(\''+value.id+'\');" data-id="' + value.id + '" class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
    row += '<button name="btnDeleteDetail" data-id="' + value.id + '" class="btn btn-default" type="button"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>';
    row += '</td>';
    row += '</tr>';
    return row;
}

/* ini fungsi tombol edit di list data table dengan nampilin dialog/modal window */
function editDetail(id) {
    selected_id_detail = id;
    var n = new Date().getTime();
    clearFormDetail();
    $.getJSON(path_detail+'/'+selected_id_detail+'?'+n, function(response, status) {
        console.log('rating_detail.response: ', response);
        console.log('rating_detail.response.data: ', response.data);
        //console.log('rating_detail.response.data.json: ', JSON.stringify(response.data));
        $('#myModalLabelDetail').html("Update Rating");
        $('#btnSaveDetail').html('Update');

        window.data = response.data;

        $("[name=area]").val(data.area);
        $("[name=point]").val(data.point);
        $("[name=standard]").val(data.standard);
        $("[name=tools]").val(data.tools);
        $("[name=action_id]").val(data.action_id);
        $("[name=result][value='"+data.result+"']").prop('checked', true);
        
        $('[name=method]:first').prop('checked', false);
        $.each(data.method.split(','), function(index, value){
            $('[name=method][value="'+value+'"]').prop('checked', true);
        });
        
        $('#modalFormDetail').modal('show');
    }); 
}





function showSaveAsChecklist() {

    var msg = validationMessage(false);

    if (msg != '') {
        addAlert("alert-danger", msg);
    } else {
        clearMessage();
        $('#replaceSaveAsChecklist').hide();
        $('#newSaveAsChecklist').show();
        $('#modalFormSaveAsChecklist').modal('show');
        $('[name=saveType]:first').prop('checked', true);
        $("[name=newSaveAsChecklist").val('');
        var param="?filter_group_id="+$('[name=group_id]').val()+"&filter_plant_id="+$('[name=plant_id]').val();
        ajaxGET(path_checklist + '/list'+param,'onGetNewChooseSaveAsChecklistSuccess','onGetNewChooseSaveAsChecklistError');
    }
}

function onGetNewChooseSaveAsChecklistSuccess(response){
    console.log('checklist', path_checklist + '/list');
    console.log('checklist response: ', response);
    console.log('checklist response.data: ', response.data);
    $("[name=replaceSaveAsChecklist], [name=loadChecklist]").empty();
    $("[name=replaceSaveAsChecklist], [name=loadChecklist]").append(new Option('-- Select One --', ''));
    
    $.each(response.data, function(key, value) {
        if(value.active) public_default_checklist = value.id;
        $("[name=replaceSaveAsChecklist], [name=loadChecklist]").append(new Option(value.name + (value.active?' [DEFAULT]':''), value.id));
    });
    list_checklist = response.data;
    $("[name=replaceSaveAsChecklist]").change(function(){
        if($(this).val() == public_default_checklist) {
            $("[name=active][value='1']").prop('checked', true);
        } else {
            $("[name=active][value='0']").prop('checked', true);
        }
    });
}

function saveSaveAsChecklist() {

    var obj = new FormData();
    
    if($('[name=saveType]:checked').val() == 'NEW') {
        console.log('[SAVE NEW CHECKLIST]');
        obj.append('name', $('[name=newSaveAsChecklist]').val());
        obj.append('group_id', $('#group_id').val());
        obj.append('plant_id', $('#plant_id').val());
    } else {
        console.log('[SAVE REPLACE CHECKLIST]');
        obj.append('id', $("[name=replaceSaveAsChecklist").val());
        $.each(list_checklist, function(key, value){
           if(value.id == $("[name=replaceSaveAsChecklist").val()) {
               obj.append('name', value.name);
           } 
        });
        obj.append('group_id', $('[name=group_id]').val());
        obj.append('plant_id', $('[name=plant_id]').val());
    }

    
    obj.append('active', $('[name=active]:checked').val());

    console.log('selected: ', selected_id);
    console.log('================================================');
    clearMessage();
    obj.append('rating_id', selected_id);
    console.log(obj);
    ajaxPOST(path_checklist + '/save',obj,'onSaveChecklistSuccess','onSaveChecklistError');
}


function onSaveChecklistSuccess(response){
    console.log(response);
    $('#modalFormSaveAsChecklist').modal('hide');
}

function autoLoadChecklist() {
    if($('[name=group_id]').val() == "" || $('[name=plant_id]').val()==""){
        alert("SERVICE & AREA WAJIB DIISI!");
    }else{
        
        if(confirm("Are you sure want to load checklist for '"+$('[name=plant_id] option:selected').text()+"'?")) {
            var obj = new FormData(document.querySelector('#frm'));
            obj.append('is_new', false);
            ajaxPOST(path + '/reload-checklist/'+selected_id+'/default',obj,'onLoadChecklistSuccess','onLoadChecklistError');
            console.log('Auto load');
        }
    }
}


function showLoadChecklist() {
    if($('[name=group_id]').val() == "" || $('[name=plant_id]').val()==""){
        alert("SERVICE & AREA WAJIB DIISI!");
    }else{
        $('#modalFormLoadChecklist').modal('show');
        var param="?filter_group_id="+$('[name=group_id]').val()+"&filter_plant_id="+$('[name=plant_id]').val();
        ajaxGET(path_checklist + '/list'+param,'onGetNewChooseSaveAsChecklistSuccess','onGetNewChooseSaveAsChecklistError');
    }
}
function renderChecklist(){
    var param="?filter_group_id="+$('[name=group_id]').val()+"&filter_plant_id="+$('[name=plant_id]').val();
    ajaxGET(path_checklist + '/list'+param,'onGetNewChooseSaveAsChecklistSuccess','onGetNewChooseSaveAsChecklistError');
}

function doLoadChecklist() {
    if($('[name=loadChecklist]').val() != '' && confirm("Are you sure want to load selected checklist for '"+ $('[name=plant_id] option:selected').text()  +"'?")) {
        var selected_value_checklist = {}
        $.each(list_checklist, function(key, value){
            if(value.id == $("[name=loadChecklist]").val()) {
                selected_value_checklist = value;
                return;
            } 
         });
        console.log(selected_value_checklist);
        var obj = new FormData(document.querySelector('#frm'));
        obj.append('is_new', false);
        ajaxPOST(path + '/reload-checklist/'+selected_id+'/'+$("[name=loadChecklist]").val(),obj,'onLoadChecklistSuccess','onLoadChecklistError');
        console.log("BEFORE SINDAH "+path + '/reload-checklist/'+selected_id+'/'+$("[name=loadChecklist]").val());
    }
}
function onLoadChecklistError(){
    alert("Checklist (default) item tidak tersedia.");
}

function onLoadChecklistSuccess(response){
    console.log('checklist success: ', response);
    var data = response.data;
    var date = new Date(data.date);
    var strDate = date.getFullYear() + '-' + pad(date.getMonth()+1, 2) + '-' + pad(date.getDate(), 2);
    $("[name=group_id]").val(data.group_id);
    $("[name=date]").val(strDate);
    $("[name=plant_id]").val(data.plant_id);
    $("[name=person_id]").val(data.person_id);
    $("[name=period][value='"+data.period+"']").prop('checked', true);
    $("[name=period_description]").val(data.period_description);
    $("[name=remarks]").val(data.remarks);
    $('#modalFormLoadChecklist').modal('hide');
    //display(1);
    displayDetail();
}
