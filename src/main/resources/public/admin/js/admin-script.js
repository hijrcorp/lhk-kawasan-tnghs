
const ENVIRONMENT = "DEVELOPMENT!";

const URL = {
    payment: {
        list: ctx + '/payment/list',
        view: ctx + '/payment/{id}',
        remove: ctx + '/payment/{id}/delete',
    },
    rating: {
        list: ctx + '/rating/list',
        view: ctx + '/rating/{id}',
        save: ctx + '/rating/save',
        remove: ctx + '/rating/{id}/delete',
    },
    product: {
        list: ctx + '/product/list',
        view: ctx + '/product/{id}',
        save: ctx + '/product/save',
        remove: ctx + '/product/{id}/delete',
    },
    reference: {
        product: { list: ctx + '/product/list?no_offset=1' },
        commentator: { list: ctx + '/commentator/list?no_offset=1' },
    }
}

const months = ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
const monthsShort = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];

window.star = 5;

$(document).ready(function(){
	$('#error_msg').hide();
	
	document.body.style.zoom = "93%";

    $('form').attr('autocomplete', 'off');
    $('.datetimepicker').each(function(){
        $(this).addClass('datetimepicker-input');
        $(this).attr('data-toggle', 'datetimepicker');
        $(this).attr('data-target', '[name='+$(this).attr('name')+']');
        $(this).datetimepicker({ format: 'DD/MM/YYYY LT' });
    });
	
//	$.ajaxSetup({
//		headers : {
//			'Authorization' : 'Bearer '+ getCookieValue(tokenCookieName)
//		}
//	});
	
//	$.fn.datepicker.dates['id'] = {
//        days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"],
//        daysShort: ["Ming", "Sen", "Sel", "Rab", "Kab", "Jum", "Sab", "Ming"],
//        daysMin: ["Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Ming"],
//        months: months,
//        monthsShort: monthsShort,
//        today: "Hari Ini"
//    };
//    
//    $('.datepicker').datepicker({
//        format: "dd-mm-yyyy",
//        clearBtn: true,
//        language: "id",
//        keyboardNavigation: false
//    });
//    
//    $('.datepicker-indonesia').datepicker({
//        format: "d MM yyyy",
//        clearBtn: true,
//        language: "id",
//        weekStart: 1,
//        clearBtn: false,
//        keyboardNavigation: false
//    });
//    
//    $('.datepicker-indonesia-m').datepicker({
//        format: "d M yyyy",
//        clearBtn: true,
//        language: "id",
//        weekStart: 1,
//        keyboardNavigation: false
//    });
	
	$('.datepicker').on('changeDate', function(ev){
	    $(this).datepicker('hide');
	});

	$(':input[readonly]').css({'background-color':'rgba(0,0,0,0.06)'});

    $("input[type=number]").keypress(function(event) {
        if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
            return false;
        }
    });
	
});

function show_image(img){
	$('#modal-preview-title').text('Perbesar Gambar');
	$('#modal-preview-content').html('<img src="'+img+'" width="640px" />');
	$('#modal-preview').modal('open');
}

function getURLParam(param) {
	var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
	}
	return getUrlParameter(param);
}

function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

function url_base64_decode(str) {
	var output = str.replace('-', '+').replace('_', '/');
	switch (output.length % 4) {
		case 0:
			break;
		case 2:
			output += '==';
			break;
		case 3:
			output += '=';
			break;
		default:
			throw 'Illegal base64url string!';
	}
	var result = window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	return result;
}

function logout(){
	$( "#logout" ).text("Please wait..");
	$.ajax({
	    url: ctx + '/revoke-token',
	    method: "GET",
	    crossDomain: true,
	    contentType: "application/x-www-form-urlencoded",
	    data: '',
	    cache: false,
	    success: function (data) {
	    		clearTokenAndRedirect();
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    		clearTokenAndRedirect();
	    }
		
	});	
}
var domainRoot = window.location.hostname.replace(window.location.hostname.substr(0,window.location.hostname.indexOf(".")),'');

function clearTokenAndRedirect(){
	var org = jQuery.parseJSON(url_base64_decode(getCookieValue(tokenCookieName).split('\.')[1])).user_name.split('::')[1];
	console.log(org);
	if(window.location.hostname != 'localhost') {
		Cookies.remove(tokenCookieName,{domain: domainRoot, path: '/'});
		//Cookies.remove('oauth2-refresh-token',{domain: '.hijr.co.id', path: '/'});;	
	}else{
		Cookies.remove(tokenCookieName,{ path: '/'});
		//Cookies.remove('oauth2-refresh-token',{path: '/'});;
	}
	location.href=ctx + '/login?org='+org;
}

function ajaxPOST(url,obj,fnsuccess, fnerror){
	$.ajax({
	    url : url,
	    method: "POST",
	    crossDomain: true,
	    contentType: false,
	    processData: false,
	    data : obj,
	    cache: false,
	    success : function (response) {
            if (fnsuccess instanceof Function) {
                fnsuccess(response);
            } else {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
            }
	    },
	    error : function (response) {
    		$('#error_msg').show();
    		console.log(response);
            if (fnerror instanceof Function) {
                fnerror(response);
            } else {
    			var fn = window[fnerror];
        		if(typeof fn === 'function') {
        		    fn(response);
        		}
            }
	    },
	});
}
function ajaxGET(url, fnsuccess, fnerror){
	$.ajax({
	    url: url,
	    method: "GET",
	    success: function (response) {
            if (fnsuccess instanceof Function) {
                fnsuccess(response);
            } else {
		    	var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
            }
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
            if (fnerror instanceof Function) {
                fnerror();
            } else {
    			var fn = window[fnerror];
        		if(typeof fn === 'function') {
        		    fn(fnerror);
        		}
            }
	    }
		
	});	
}

function startLoading(btn){
    $('#'+btn).text('Loading..');
    $('#'+btn).prop('disabled', true);
}

function stopLoading(btn, btnlabel){
    $('#'+btn).text(btnlabel);
    $('#'+btn).prop('disabled', false);
}

function showAlertMessage(msg, timeout = 0){
    $('#modal-alert-msg').html(msg);
    $('#modal-alert').modal('show');
    if(timeout >0 ){
        setTimeout(function(){ $('#modal-alert').modal('hide'); }, timeout);
    }
    
}

function showAlertMessage(msg, timeout = 0, type){
    $('#modal-alert-msg').html(msg);
    $('#modal-alert').modal('show');
    if(type!=undefined){
    	$('#modal-alert').find('.modal-header').attr('class', 'modal-header '+type)
    }
    if(timeout >0 ){
        setTimeout(function(){ $('#modal-alert').modal('hide'); }, timeout);
    }
    
}


/** custom by Hadi **/

function getRating(value) {
    value = parseInt(value);
    return '<img src="'+ctx+'/images/star/'+value+'.png" />';
}

function getRatingLarge(value) {
    value = parseInt(value);
    return '<img src="'+ctx+'/images/star/'+value+'.png" width="150px" />';
}

function getValue(value, replace = '') {
    return (value == null || value == 'null' || value == '' ? replace : value) 
}

function getValueGender(value, undefined2 = '-', male = 'Jantan', female = 'Betina') {
    return (value == null || value == 'null' ? undefined2 : (value == 1 ? male : female)); 
}

function getDateForList(datetime) {
    alert(datetime);
    if(datetime==null) return '-';
	let result = moment(datetime).format('DD-MMM-YYYY').toUpperCase();
	return (result == 'INVALID DATE' ? '' : result);
}

function getDateForInput(datetime) {
    return moment(datetime).format('DD/MM/YYYY LT');
}

function getTDValueKodeTagging(kode_tagging){
    return '<td'+(kode_tagging==null||kode_tagging==''?' nowrap':'')+'>'+getValue(kode_tagging, '(Tidak Diketahui)')+'</td>';
}

function upperCaseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

function getUUIDString() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
// UUIDv4 = function b(a){return a?(a^Math.random()*16>>a/4).toString(16):([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,b)}

if (typeof String.prototype.toCamelCase !== 'function') {
    String.prototype.toCamelCase = function(){
        return this.replace(/[-_]([a-z])/g, function (g) { return g[1].toUpperCase(); })
    };
}


$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


//$('#modal-form, #modal-remove, #modal-remove-new, #modal-preview, #modal-confirm, #modal-konfirmasi').modal({
//	dismissible:false
//});
//
//
//$('#modal-alert, #modal-failed-alert, #modal-alert-success').modal({
//	dismissible:false,
//	ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
//        
//        console.log(modal, trigger);
//		setTimeout(function (){modal.modal('close')}, 2000);
//      }
//});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp. ' + rev2.split('').reverse().join('');
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += "";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

function randomInt(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

function getAutoNumberDate() {
	return new Date().getTime() + randomInt(100, 999);
}

function capitalizeFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function getJenisMutasiColor(jenis_mutasi) {
    var warna = 'pink';
    if(jenis_mutasi=='UKURAN') warna = 'orange';
    else if(jenis_mutasi=='KELAMIN') warna = 'blue';
    else if(jenis_mutasi=='LAPORAN_LAMPIRAN'||jenis_mutasi=='LAPORAN_TENTANG') warna = 'deep-orange';
    else if(jenis_mutasi=='DATA_AWAL') warna = 'teal';
    else if(jenis_mutasi=='MASUK') warna = 'green';
    else if(jenis_mutasi=='KELUAR') warna = 'red';
    else if(jenis_mutasi=='PENAMBAHAN_KELAHIRAN') warna = 'cyan';
    else if(jenis_mutasi=='PENGURANGAN_KEMATIAN') warna = 'grey';
    else if(jenis_mutasi=='PERKEMBANGAN_ANAKAN') warna = 'purple';
    return warna;
}

function strip(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

function setStar(ths,sno){
    window.star = sno;
    for (var i=1;i<=5;i++){
      var cur=document.getElementById("star"+i)
      cur.className="fa fa-lg fa-star"
    }

    for (var i=1;i<=sno;i++){
      var cur=document.getElementById("star"+i)
      if(cur.className=="fa fa-lg fa-star") {
        cur.className="fa fa-lg fa-star text-warning"
      }
    }
  }

