<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top">

    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%@ include file = "inc/sidebar.jsp" %>  

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">User Account</a>
            </li>
            <li class="breadcrumb-item active">${requestScope.appname}</li>
          </ol>

          

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              User Account
              <div class="float-right">
              <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-outline-secondary" type="button">Add New</button>
              </div>
              </div>
            <div class="card-body">
            		<form id="search-form">
            			<input type="hidden" name="filter_application" value="${requestScope.app}">
            			<!-- Aktifin input di bawah untuk ngetes paging  -->
            			<!-- <input type="hidden" name="limit" value="2">  -->
            			<div class="form-group row">
			    			<div class="col-sm-6">
  							<div class="input-group">
							  <input type="text" class="form-control" name="filter_keyword" placeholder="Type any keyword to find spesific data">
							  <div class="input-group-append">
							    <button class="btn btn-outline-secondary" type="submit">Search</button>
							  </div>
							</div>
				  		</div>
				  		<div class="col-sm-3 offset-sm-3">
				    			<select name="filter_group" class="form-control"></select>
				  		</div>
				  	</div>
				</form>
				<div class="table-responsive">
                <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    	  <th>No</th>
                      <th>Username</th>
                      <th width="20%"><spring:message code="account.kolom.label.firstname"/></th>
                      <th width="20%"><spring:message code="account.kolom.label.lastname"/></th>
                      <th width="15%">Job Role</th>
                      <th width="15%">Group</th>
                      <th>Status</th>
                      <th width="10%">Created</th>
                      <th width="10%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <%@ include file = "inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    
      <!-- Form Modal-->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content"><form id="entry-form"><input type="hidden" name="application_id" value="${requestScope.app}">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          			<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
          			<div class="form-row">
					    <div class="form-group col-md-6">
					      <label>First Name</label>
					      <input type="text" class="form-control" name="first_name" autocomplete="off">
					    </div>
					    <div class="form-group col-md-6">
					      <label>Last Name</label>
					      <input type="text" class="form-control" name="last_name" autocomplete="off">
					    </div>
					  </div>
					  <div class="form-row">
				     	<div class="form-group col-md-6">
					      <label>Email (Optional)</label>
					      <input type="text" class="form-control" name="email" autocomplete="off">
					    </div>
					    
					    <div class="form-group col-md-6">
					      <label>Mobile (Optional)</label>
					      <input type="text" class="form-control" name="mobile" autocomplete="off">
					    </div>
					  </div> 
				     <div class="form-row">
				     	<div class="form-group col-md-6">
					      <label>Job Role</label>
					      <select name="position_id" class="form-control"></select>
					    </div>
					    
					    <div class="form-group col-md-6">
					      <label>User Group</label>
					      <select name="group_id" class="form-control"></select>
					    </div>
					  </div>     
            			<div class="form-row">
            				<div class="form-group col-md-6">
					      <label>User Name</label>
					      <input type="text" class="form-control" name="username" autocomplete="off">
					    </div>
            				<div class="form-group col-sm-6">
            					<label></label>
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" value="true" name="enabled">
							  <label class="form-check-label">
							    Disabled Account
							  </label>
							</div>
				  		</div>
				  	</div>
				
          
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" type="submit">Save</button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/c/account.js"></script>

  </body>

</html>
