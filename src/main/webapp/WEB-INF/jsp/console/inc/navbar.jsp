<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="#">Admin Console</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto">
      
        <li class="nav-item dropdown arrow-down no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="lang-dropdown"  onclick="changeLang('${requestScope.locale=='en_US'?'in_ID':'en_US'}')">
            		<span class="flag-icon ${requestScope.locale=='en_US'?'flag-icon-id':'flag-icon-us'}"> </span> ${requestScope.locale=='en_US'?'Bahasa':'English'}
            </a>
        </li>
        
        <li class="nav-item dropdown arrow-down">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <img src="${requestScope.avatar}" width="25px" /> ${requestScope.realName}  
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          	<a class="dropdown-item" href="${pageContext.request.contextPath}/login-password" target="_blank">Change Password</a>
            <a class="dropdown-item" href="${pageContext.request.contextPath}/login-logout">Logout</a>
          </div>
        </li>
      </ul>

    </nav>