<!-- Sidebar -->
      <ul class="sidebar navbar-nav">
         <li class="nav-header text-muted">
            <span>ACCESS MANAGEMENT</span>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/${requestScope.app}/console/account">
            <i class="fas fa-fw fa-user"></i>
            <span>User Account</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/${requestScope.app}/console/group">
            <i class="fas fa-fw fa-tag"></i>
            <span>Group Authorities</span></a>
        </li>
        <li class="nav-header text-muted">
            <span>GENERAL REPORTS</span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fas fa-fw fa-file"></i>
            <span>Summary Report</span></a>
        </li>
      </ul>