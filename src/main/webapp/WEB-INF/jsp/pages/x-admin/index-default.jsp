<!DOCTYPE html>
<html lang="en">

<%@ include file = "../inc/header.jsp" %>

  <body id="page-top">

    <%@ include file = "../inc/navbar.jsp" %>

    <div id="wrapper">

    <%@ include file = "../inc/sidebar.jsp" %>  

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item active">Selamat Datang di Aplikasi PPS Online - Payments</li>
          </ol>

    <div class="row">
      <div class="col-md-6">
      <img class="img-fluid" src="${pageContext.request.contextPath}/images/cover.jpg" />
      </div>
    </div>
    
    <div class="row py-3">
      <div class="col-md-6">
      <h5 class="card-title">Rekapitulasi Satwa Berdasarkan Taksa</h5>
          <table id="tbl-data" class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th width="5%">No</th>
                      <th width="50%">Taksa</th>
                      <th width="10%">Sehat</th>
                      <th width="10%">Sakit</th>
                      <th width="10%">Mati</th>
                      <th width="10%">Titip</th>
                      <th width="10%">Keluar</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                  <tfoot>
                    
                  </tfoot>
                </table>
          </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <%@ include file = "../inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <%@ include file = "../inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/dashboard.js"></script>

  </body>

</html>
