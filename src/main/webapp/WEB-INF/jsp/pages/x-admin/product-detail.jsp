<%@ include file="./inc/header.jsp" %>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <strong class="card-title">Product Detail</strong>
            </div>
            <div class="card-body">
              
              <div class="row">
                <div class="col-md-4">
                    <!-- https://www.tokopedia.com/arsus1/power-bank-10000-mah-vivan-vpb-a10/review?src=topads -->
                  <img src="http://hadiubaidillah.com/public/images/3.jpg" />
                </div>
                <div class="col-md-8">
                  <div class="typo-headers">
                    <h2 class="pb-2 display-5" name="product-name">Sections & Modal Names H2</h2>
                    <span name="rating-picture"></span> <span name="rating-amount" class="m-l-10"></span> Rating
                  </div>
                </div>
              </div>
              
              <div class="m-t-20">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="pull-right text-right">
                          <h2 name="rating-value"></h2>
                          <span name="rating-picture-large"></span>
                          <span name="rating-amount" class="m-l-10"></span> Rating
                        </div>
                      </div>
                      <div class="col-md-1" style="width: 30px !important;"><center><div style="border-left: 1px solid black; height: 100px; width: 1px;"></div></center></div>
                      <div class="col-md-7">
                        <% for(int i = 5; i >= 1; i--) { %>
                          <div class="row">
                            <div class="col-md-2 align-self-center">
                              <span style="margin-right: 10px;"><%=(i==1?"&nbsp;"+i : i)%></span> <span class="text-warning fa fa-star" style="margin-right: 10px;"></span>
                            </div>
                            <div class="col-md-8 align-self-center">
                              <div class="progress" style="margin-bottom: 0px; min-width: 100%; max-height: 12px; !important;">
                                <div name="rating-<%=i%>-bar" class="progress-bar bg-warning" role="progressbar"></div>
                              </div>
                            </div>
                            <div class="col-md-2 align-self-center">
                              <span name="rating-<%=i%>"></span>
                            </div>
                          </div>
                        <% } %>
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="m-t-20">
                <div class="card">
                  <div class="card-header">Credit Card</div>
                  <div class="card-body">
                    <form id="frm" novalidate="novalidate">
                      <div class="row form-group">
                        <div class="col col-md-2 text-right">
                          <label class="form-control-label">Rating <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-12 col-md-10">
                          <div class="form-check-inline form-check" style="">
                            <i class="fa fa-lg fa-star text-warning" id="star1" onclick="setStar(this,1)"></i>
                            <i class="fa fa-lg fa-star text-warning" id="star2" onclick="setStar(this,2)"></i>
                            <i class="fa fa-lg fa-star text-warning" id="star3" onclick="setStar(this,3)"></i>
                            <i class="fa fa-lg fa-star text-warning" id="star4" onclick="setStar(this,4)"></i>
                            <i class="fa fa-lg fa-star text-warning" id="star5" onclick="setStar(this,5)"></i>
                          </div>
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col col-md-2 text-right">
                          <label class="form-control-label">Comment <span class="text-danger">*</span></label>
                        </div>
                        <div class="col-12 col-md-10">
                          <textarea class="form-control" name="comment" rows="3"></textarea>
                        </div>
                      </div>
                      <div>
                        <button type="button" onclick="save();" class="btn btn-lg btn-info btn-block">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              
              <div id="div-comment"></div>
              
              <!-- 
              <div class="m-t-20">
              
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-2">
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                        <p class="text-secondary text-center">15 Minutes Ago</p>
                      </div>
                      <div class="col-md-10">
                        <p>
                          <a class="float-left" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Maniruzzaman Akash</strong></a>
                          <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                          <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                          <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                          <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                
                         </p>
                         <div class="clearfix"></div>
                        <p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>
                          <a class="float-right btn btn-outline-primary ml-2"> <i class="fa fa-reply"></i> Reply</a>
                          <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                         </p>
                      </div>
                    </div>
                      <div class="card card-inner m-t-20">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-2">
                              <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                              <p class="text-secondary text-center">15 Minutes Ago</p>
                            </div>
                            <div class="col-md-10">
                              <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Maniruzzaman Akash</strong></a></p>
                              <p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                              <p>
                                <a class="float-right btn btn-outline-primary ml-2">  <i class="fa fa-reply"></i> Reply</a>
                                <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                
              </div> -->
              
            </div>
          </div>
        </div>
      </div>
      
      
      
      
      
      
      <!-- <div class="row">
        <div class="col-md-12 m-t-20">
          <div class="card">
            <div class="card-header">
              <big>Rating Detail</big><br/>
              <small id="product-name"></small>
            </div>
            <div class="card-body">
              <form action="" method="post" novalidate="novalidate">
                  <div class="form-group">
                    <div class="col-md-12">
                      <label>Database <span class="text-danger">*</span></label>
                      <select name="database_application_id" class="form-control">
                        <option>----- Select Database -----</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Product <span class="text-danger">*</span></label>
                      <select name="product_id" class="form-control">
                        <option>----- Select Product -----</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <label>Commentator <span class="text-danger">*</span></label>
                      <select name="commentator_id" class="form-control">
                        <option>----- Select Commentator -----</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-6">
                      <label>Date <span class="text-danger">*</span></label>
                      <input name="date" class="form-control datetimepicker-input datetimepicker"></select>
                    </div>
                    <div class="col col-md-6">
                      <label>Rating <span class="text-danger">*</span></label>
                      <div class="">
                        <div class="form-check-inline form-check" style="">
                          <label for="inline-radio1" class="form-check-label " style="padding-right: 20px;">
                            <input type="radio" id="inline-radio1" name="value" value="1" class="form-check-input" checked="checked" selected>1
                          </label>
                          <label for="inline-radio2" class="form-check-label " style="padding-right: 20px;">
                            <input type="radio" id="inline-radio2" name="value" value="2" class="form-check-input">2
                          </label>
                          <label for="inline-radio3" class="form-check-label " style="padding-right: 20px;">
                            <input type="radio" id="inline-radio3" name="value" value="3" class="form-check-input">3
                          </label>
                          <label for="inline-radio4" class="form-check-label " style="padding-right: 20px;">
                            <input type="radio" id="inline-radio4" name="value" value="4" class="form-check-input">4
                          </label>
                          <label for="inline-radio5" class="form-check-label " style="padding-right: 20px;">
                            <input type="radio" id="inline-radio5" name="value" value="5" class="form-check-input">5
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12">
                      <label>Comment <span class="text-danger">*</span></label>
                      <textarea class="form-control" name="comment" rows="3"></textarea>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">
                      <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                      <i class="fa fa-ban"></i> Reset
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div> -->
      </div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="${assetPath}/js/product-detail.js"></script>
<%@ include file="./inc/footer.jsp" %>
