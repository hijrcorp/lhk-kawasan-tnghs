<%@ include file="./inc/header.jsp" %>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="pull-left overview-wrap">
            <h2 class="title-1">Rating List</h2>
          </div>
          <div class="pull-right">
            <button type="button" onClick="add();" class="btn btn-primary"><i class="fas fa-plus"></i> New</button>
            <button type="button" onClick="refresh();" class="btn btn-primary"><i class="fas fa-refresh"></i> Refresh</button>
          </div>
        </div>
        <div class="col-md-12 m-t-20">
          <!-- DATA TABLE-->
          <div class="table-responsive m-b-40">
            <table id="tbl-data" class="table table-borderless table-data3">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Application</th>
                  <th>Product</th>
                  <th>Rating</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <!-- END DATA TABLE-->
        </div>
      </div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->



<!-- Modal Feature -->
<form id="frm" class="form-horizontal">
  <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="form-label">Add</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="card-body card-block modal-body">
          <div id="modal-form-msg" class="form-group col-md-12 sufee-alert alert with-close alert-danger alert-dismissible fade show">
            <span class="message">You successfully read this important alert.</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label>Product <span class="text-danger">*</span></label>
              <select name="product_id" class="form-control select2">
                <option>----- Select Product -----</option>
              </select>
            </div>
            <div class="col-md-6">
              <label>Commentator <span class="text-danger">*</span></label>
              <select name="commentator_id" class="form-control select2">
                <option>----- Select Commentator -----</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label>Date <span class="text-danger">*</span></label>
              <input name="date" class="form-control datetimepicker-input datetimepicker"></select>
            </div>
            <div class="col col-md-6">
              <label>Rating <span class="text-danger">*</span></label>
              <div class="">
                <div class="form-check-inline form-check" style="">
                  <label for="inline-radio1" class="form-check-label " style="padding-right: 20px;">
                    <input type="radio" id="inline-radio1" name="value" value="1" class="form-check-input" checked="checked" selected>1
                  </label>
                  <label for="inline-radio2" class="form-check-label " style="padding-right: 20px;">
                    <input type="radio" id="inline-radio2" name="value" value="2" class="form-check-input">2
                  </label>
                  <label for="inline-radio3" class="form-check-label " style="padding-right: 20px;">
                    <input type="radio" id="inline-radio3" name="value" value="3" class="form-check-input">3
                  </label>
                  <label for="inline-radio4" class="form-check-label " style="padding-right: 20px;">
                    <input type="radio" id="inline-radio4" name="value" value="4" class="form-check-input">4
                  </label>
                  <label for="inline-radio5" class="form-check-label " style="padding-right: 20px;">
                    <input type="radio" id="inline-radio5" name="value" value="5" class="form-check-input">5
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label>Comment <span class="text-danger">*</span></label>
              <textarea class="form-control" name="comment" rows="3"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button onclick="save();" type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Feature -->

<script src="${assetPath}/js/rating-list.js"></script>
<%@ include file="./inc/footer.jsp" %>
