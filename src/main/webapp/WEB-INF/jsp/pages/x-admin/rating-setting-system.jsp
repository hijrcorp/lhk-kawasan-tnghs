<%@ include file="./inc/header.jsp" %>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="pull-right">
            <button type="button" onClick="add();" class="btn btn-primary"><i class="fas fa-plus"></i> New</button>
            <button type="button" onClick="refresh();" class="btn btn-primary"><i class="fas fa-refresh"></i> Refresh</button>
          </div>
        </div>
        <div class="col-md-12 m-t-20">
          <!-- DATA TABLE-->
          <div class="table-responsive m-b-40">
            <table id="tbl-data" class="table table-borderless table-data3">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Database</th>
                  <th>Application</th>
                  <th>Account</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <!-- END DATA TABLE-->
        </div>
      </div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->



<!-- Modal Feature -->
<form id="frm" class="form-horizontal">
  <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="form-label">Add</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="card-body card-block modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label>Database System <span class="text-danger">*</span></label>
              <select name="database_name" class="form-control">
                <option>----- Select Database System -----</option>
              </select>
            </div>
          </div>
          <hr/>
          <div class="form-group">
            <div class="col-md-12">
              <label>Application <span class="text-danger">*</span></label>
              <select name="application_table_name" class="form-control">
                <option>----- Select Table Name -----</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <select name="application_column_name" class="form-control">
                <option>----- Select Column Name -----</option>
              </select>
            </div>
            <div class="col-md-6">
              <select name="application_column_value" class="form-control">
                <option>----- Select Column Value -----</option>
              </select>
            </div>
          </div>
          <hr/>
          <div class="form-group">
            <div class="col-md-12">
              <label>Account <span class="text-danger">*</span></label>
              <select name="user_table_name" class="form-control">
                <option>----- Select Table Name -----</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <select name="user_table_column_id" class="form-control">
                <option>----- Select Column ID -----</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <select name="user_table_column_first_name" class="form-control">
                <option>----- Select First Name -----</option>
              </select>
            </div>
            <div class="col-md-6">
              <select name="user_table_column_last_name" class="form-control">
                <option>----- Select Last Name -----</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button onclick="save();" type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Feature -->

<script src="${assetPath}/js/rating-setting-system.js"></script>
<%@ include file="./inc/footer.jsp" %>
