<%@ include file="./inc/header.jsp" %>

<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="pull-right">
            <button type="button" onClick="add();" class="btn btn-primary"><i class="fas fa-plus"></i> New</button>
            <button type="button" onClick="refresh();" class="btn btn-primary"><i class="fas fa-refresh"></i> Refresh</button>
          </div>
        </div>
        <div class="col-md-12 m-t-20">
          <!-- DATA TABLE-->
          <div class="table-responsive m-b-40">
            <table id="tbl-data" class="table table-borderless table-striped table-data3">
              <thead>
                <tr>
                  <th width="10">No</th>
                  <th>Database</th>
                  <!-- <th>Remarks</th> -->
                  <th width="10">Action</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <!-- END DATA TABLE-->
        </div>
      </div>
      <%@ include file = "./inc/trademark.jsp" %>
    </div>
  </div>
</div>
<!-- END MAIN CONTENT-->

<script src="${assetPath}/js/rating-setting.js"></script>
<%@ include file="./inc/footer.jsp" %>
