
    </div>
  </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
    
     
    <!-- Modal Feature -->
    <div class="modal fade" id="modal-remove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="form-label">Delete</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p id="modal-remove-content"></p>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button id="data-remove-id" data-id="0" onclick="remove($(this).data('id'), 1);" type="button" class="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Feature -->

    <!-- Confirmation Modal-->
    <div class="modal fade" id="modal-remove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-confirm-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
            <button class="btn btn-primary" id="btn-modal-confirm-yes" type="button">Yes</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Confirmation Modal-->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-confirm-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
            <button class="btn btn-primary" id="btn-modal-confirm-yes" type="button">Yes</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-success">
            <h5 class="modal-title" id="exampleModalLabel">Alert</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-alert-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Error Modal-->
    <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-error-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="${contextPath}/vendor/jquery/jquery.min.js"></script>
    <script src="${contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="${contextPath}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="${contextPath}/vendor/jquery.disableAutoFill.min.js"></script>
    <script src="${contextPath}/vendor/js.cookie.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="${contextPath}/vendor/chart.js/Chart.min.js"></script>
    <script src="${contextPath}/vendor/moment.min.js"></script>
    <script src="${contextPath}/vendor/daterangepicker.js"></script>
	<script src="${contextPath}/vendor/select2.min.js"></script>
	<script src="${contextPath}/vendor/ekko-lightbox.min.js"></script>
	<script src="${contextPath}/vendor/loadingoverlay.min.js"></script>
  
    <!-- DateTime Picker-->
    <%-- <script src="${contextPath}/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script> --%>
    <script src="${contextPath}/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    
    <!-- Kebutuhan dari common.js -->
    <script src="${contextPath}/vendor/remodal/remodal.js"></script>
    <script src="${contextPath}/vendor/bootstrap-input-spinner-master/bootstrap-input-spinner.js"></script>
	
	<!-- Custom scripts for all pages-->
	<script src="${contextPath}/js/common.js"></script>
  
  
    <!-- Vendor JS -->
    <script src="${contextPath}/vendor/slick/slick.min.js">
    </script>
    <script src="${contextPath}/vendor/wow/wow.min.js"></script>
    <script src="${contextPath}/vendor/animsition/animsition.min.js"></script>
    <script src="${contextPath}/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="${contextPath}/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="${contextPath}/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="${contextPath}/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="${contextPath}/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="${contextPath}/vendor/chartjs/Chart.bundle.min.js"></script>

    <!-- Main JS-->
    <script src="${assetPath}/js/main.js"></script>
    <script src="${assetPath}/js/admin-script.js"></script>
  

</body>

</html>
<!-- end document-->