<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="menu" scope="session">
  <nav class="navbar-sidebar">
    <ul class="list-unstyled navbar__list">
      <li><a href="index"><i class="fas fa-tachometer-alt"></i>Dashboard</a></li>
      <li class="has-sub">
        <a class="open" href="#"><i class="fas fa-credit-card"></i>Payment</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
          <li><a href="invoice"><i class="fas fa-file-text"></i> &nbsp; Invoice</a></li>
          <li><a href="provider"><i class="fas fa-credit-card"></i> Provider</a></li>
        </ul>
      </li>
      <li class="has-sub">
        <a class="open" href="#"><i class="fas fa-star"></i>Rating</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
          <li><a href="rating"><i class="fas fa-list"></i> List</a></li>
          <li><a href="rating-setting"><i class="fas fa-cog"></i> Setting</a></li>
          <!-- <li><a href="rating-example"><i class="fas fa-lightbulb-o"></i> &nbsp;Example</a></li>
          <li><a href="rating-setting-application"><i class="fas fa-cog"></i> Setting Application</a></li>
          <li><a href="rating-setting-system"><i class="fas fa-cog"></i> Setting System</a></li> -->
        </ul>
      </li>
      <!-- <li><a href="rating"><i class="fas fa-star"></i>Rating</a></li> -->
      <hr/>
      <li><a href="z-table"><i class="fas fa-table"></i>Tables</a></li>
      <li><a href="z-form"><i class="far fa-check-square"></i>Forms</a></li>
      <li><a href="#"><i class="fas fa-calendar-alt"></i>Calendar</a></li>
      <li><a href="z-map"><i class="fas fa-map-marker-alt"></i>Maps</a></li>
      <li class="has-sub">
        <a class="js-arrow" href="#"><i class="fas fa-copy"></i>Pages</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list">
          <li><a href="z-login">Login</a></li>
          <li><a href="z-register">Register</a></li>
          <li><a href="z-forget-pass">Forget Password</a></li>
        </ul>
      </li>
      <li class="has-sub">
        <a class="js-arrow" href="#"><i class="fas fa-desktop"></i>UI Elements</a>
        <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: block;">
          <li><a href="z-button">Button</a></li>
          <li><a href="z-badge">Badges</a></li>
          <li><a href="z-tab">Tabs</a></li>
          <li><a href="z-card">Cards</a></li>
          <li><a href="z-alert">Alerts</a></li>
          <li><a href="z-progress-bar">Progress Bars</a></li>
          <li><a href="z-modal">Modals</a></li>
          <li><a href="z-switch">Switchs</a></li>
          <li><a href="z-grid">Grids</a></li>
          <li><a href="z-fontawesome">Fontawesome Icon</a></li>
          <li><a href="z-typo">Typography</a></li>
        </ul>
      </li>
    </ul>
  </nav>
</c:set>




<c:set var="menuDesktop" scope="session" value="${menu}" />

<!-- MENU SIDEBAR-->
  <aside class="menu-sidebar d-none d-lg-block">
    <div class="logo"><a href="#"><img src="${assetPath}/images/icon/logo.png" alt="Logo" /></a></div>
    <div class="menu-sidebar__content js-scrollbar1">${menuDesktop}</div>
  </aside>
<!-- END MENU SIDEBAR-->

<c:set var="menuMobile" scope="session" value="${menu}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'navbar-sidebar', 'navbar-mobile')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__list', 'navbar-mobile__list list-unstyled')}" />
<c:set var="menuMobile" scope="session" value="${fn:replace(menuMobile, 'list-unstyled navbar__sub-list js-sub-list', 'navbar-mobile-sub__list list-unstyled js-sub-list')}" />
<!-- HEADER MOBILE-->
  <header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
      <div class="container-fluid">
        <div class="header-mobile-inner">
          <a class="logo" href="index"><img src="${assetPath}/images/icon/logo.png" alt="Logo" /></a>
          <button class="hamburger hamburger--slider" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
        </div>
      </div>
    </div>
    ${menuMobile}
  </header>
<!-- END HEADER MOBILE-->

<style>
@media (max-width: 991px) {
    .header-desktop {
        height: 75px;
    }
}
</style>

<%--@ include file = "./menu-mobile.jsp" --%>
<%--@ include file = "./menu-desktop.jsp" --%>