<!DOCTYPE html>
<html lang="en">

<%@ include file = "./head.jsp" %>

<body class="animsition">
  <div class="page-wrapper">
  
    <%@ include file = "./menu.jsp" %>

    <div class="page-container">
    
      <%@ include file = "./navbar.jsp" %>