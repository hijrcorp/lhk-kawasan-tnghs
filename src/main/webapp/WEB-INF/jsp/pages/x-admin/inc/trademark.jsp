<div class="row">
  <div class="col-md-12">
    <div class="copyright">
      <p>
        Copyright � 2019 -  <%= new java.text.SimpleDateFormat("yyyy").format(new java.util.Date()) %> <a href="http://www.hijr.co.id/" target="_blank">PT. Hijr Global Solution</a>. <br/>All rights reserved.</a>.
      </p>
    </div>
  </div>
</div>