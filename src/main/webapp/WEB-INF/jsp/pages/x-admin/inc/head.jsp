  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  <head>

    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Hijr Administrator">
    <meta name="author" content="PT. Hijr Global Solution">
    
    <c:set var="contextPath" scope="session" value="${pageContext.request.contextPath}" />

    <title>Hijr Administrator</title>

    <!-- Fontfaces CSS-->
    <link href="${assetPath}/css/font-face.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap core CSS-->
    <link href="${contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="${contextPath}/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="${contextPath}/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <%-- <link href="${contextPath}/vendor/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet" media="screen"> --%>
    <link href="${contextPath}/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

    <!-- Main CSS-->
    <link href="${assetPath}/css/theme.css" rel="stylesheet" media="all">
    <link href="${assetPath}/css/styles.css" rel="stylesheet" media="all">

    <!-- Custom fonts for this template-->
    <%-- <link href="${contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> --%>

    <link href="${contextPath}/vendor/select2.min.css" rel="stylesheet">
	<link href="${contextPath}/vendor/daterangepicker.css" rel="stylesheet"> 
	<link href="${contextPath}/vendor/ekko-lightbox.css" rel="stylesheet">
	
	<script>var ctx = "${contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<script>var localeCookieName = "${localeCookieName}"</script>
  </head>
