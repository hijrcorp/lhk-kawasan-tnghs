<%@page import="java.util.Calendar"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilPaymentMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilPayment"%>
<%@page import="id.co.hijr.ticket.mapper.MasterPaymentMapper"%>
<%@page import="id.co.hijr.ticket.model.MasterPayment"%>
<%@page import="id.co.hijr.sistem.service.VaultService"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilLuggage"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransitCamp"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="java.util.Date"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransaction"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Currency"%>
<%@page import="id.co.hijr.ticket.mapper.BankMapper"%>
<%@page import="id.co.hijr.ticket.model.Bank"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.HashSet"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
PurchaseDetilTransactionMapper purchaseDetilTransactionMapper = appCtx.getBean(PurchaseDetilTransactionMapper.class);
///
PurchaseDetilVisitorMapper purchaseDetilVisitorMapper = appCtx.getBean(PurchaseDetilVisitorMapper.class);
PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper = appCtx.getBean(PurchaseDetilTransitCampMapper.class);
PurchaseDetilLuggageMapper purchaseDetilLuggageMapper = appCtx.getBean(PurchaseDetilLuggageMapper.class);
PurchaseDetilPaymentMapper purchaseDetilPaymentMapper = appCtx.getBean(PurchaseDetilPaymentMapper.class);
//
BankMapper bankMapper = appCtx.getBean(BankMapper.class);
MasterPaymentMapper masterPaymentMapper = appCtx.getBean(MasterPaymentMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
PurchaseDetilTransaction purchaseDetilTransaction = purchaseDetilTransactionMapper.getEntityHeader(id);
System.out.print("OIKAN");
/* System.out.print(purchaseDetilTransaction); */

QueryParameter param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
Purchase purchase= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
//
param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.HEADER_ID + " = '"+purchase.getId()+"')");
List<PurchaseDetilVisitor> dataPurchaseDetilVisitor = purchaseDetilVisitorMapper.getList(param_detil);
//purchase.setPurchaseDetilVisitor(dataPurchaseDetilVisitor);

param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+purchase.getId()+"')");
List<PurchaseDetilTransaction> dataPurchaseDetilTransaction = purchaseDetilTransactionMapper.getList(param_detil);
//purchase.setPurchaseDetilTransaction(dataPurchaseDetilTransaction);

param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+purchase.getId()+"')");
List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param_detil);
//purchase.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);

param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilLuggage.HEADER_ID+ " = '"+purchase.getId()+"')");
List<PurchaseDetilLuggage> dataPurchaseDetilLuggage = purchaseDetilLuggageMapper.getList(param_detil);


param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilPayment.ID_HEADER+ " = '"+purchase.getId()+"')");
List<PurchaseDetilPayment> dataPurchaseDetilPayment = purchaseDetilPaymentMapper.getList(param_detil);
//purchase.setPurchaseDetilLuggage(dataPurchaseDetilLuggage);
//

List<Bank> bank = bankMapper.getList(new QueryParameter());
List<MasterPayment> masterPayment = masterPaymentMapper.getList(new QueryParameter());

NumberFormat formatter = NumberFormat.getCurrencyInstance();
Locale myIndonesianLocale = new Locale("in", "ID");
formatter.setCurrency(Currency.getInstance(myIndonesianLocale));


//untuk format tanggal atau waktu
SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy", new Locale("in"));
SimpleDateFormat sdftime = new SimpleDateFormat("hh.mm aa");
SimpleDateFormat sdftimeleft = new SimpleDateFormat("hh:mm:ss");
SimpleDateFormat sdf_MY = new SimpleDateFormat("MMMM yyyy");


Calendar c = Calendar.getInstance();
int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

VaultService vaultService = appCtx.getBean(VaultService.class);

int priceMasterPayment=0;
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<%@ include file = "inc/header_khusus.jsp" %>
<style>
.form-control {
	height: calc(1.5em + .75rem + 2px);
}
</style>
  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">
	
		<nav class="navbar navbar-expand-lg navbar-light bg-default">
		<div class="container p-3">
		
		  <a class="navbar-brand text-white" href="${pageContext.request.contextPath}/page/general">
		    <img src="${contextPathPublic}/images/logo-tnghs.png" width="35" height="35" class="d-inline-block align-top rounded-circle" alt="">
		    Kawasan
		  </a>
		  
		<ul class=" nav nav-pills ml-auto">
		  <li class="nav-item">
		    <a id="btn-book-1" class="nav-link disabled" data-toggle="pill" href="#pills-book-1">Information</a>
		  </li>
		  <li class="nav-item d-none">
		    <a id="btn-book-2" class="nav-link disabled" data-toggle="pill" href="#pills-book-2">Vehicle</a>
		  </li>
		  <li class="nav-item d-none">
		    <a id="btn-book-3" class="nav-link disabled" data-toggle="pill" href="#pills-book-3">Tour Guide</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-4" class="nav-link disabled" data-toggle="pill" href="#pills-book-4">Confirmation</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-4" class="nav-link text-white active" data-toggle="pill" href="#pills-book-4">Pay</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">E-ticket</a>
		  </li>
		</ul>
		
		</div>
		</nav>
	
  	<div id="wrapper">
    <div id="content-wrapper">
	<div class="d-flex align-items-center h-100">
	
      <div class="container">
      
		<div class="row justify-content-center">
		
		<%-- <c:if test="${param.step != null && param.step == 1}"> --%>
		<% 
		if(purchaseDetilTransaction==null) { 

		if(purchase.getStatusExpiredDate()==1){ 
			response.sendRedirect("/kawasan/page/expired?id="+id);
		} 
		%>
		<div class="col-md-6">
		<div class="card">
		<div class="card-header bg-light">
		    <h5 class="mb-0 text-center">Choose Your Payment Method </h5>
		</div>
		  <div class="card-body">
			<div class="row ">
				<div class="col-md-12">
				 <form>
				 <%
				 QueryParameter param = new QueryParameter();
				 param.setClause(param.getClause()+" AND "+Bank.TYPE+" ='"+"ATM"+"'");
				 bank = bankMapper.getList(param);
				 %>
				  <div class="form-row">
                  	<div class="form-group col-md-12">
                  	  <div class="card-title font-weight-bold">Bank Transfer(Verifikasi Manual)</div>
                  	  <% for(Bank b : bank) { %>
                  	  		<% if(b.getType().equals("ATM")) {%>
                  	  		<label class="form-control form-control-sms radio-inline"><input type="radio" name="bank_id" value="<% out.print(b.getId()); %>" data-type-bank="<% out.print(b.getType()); %>"> <% out.print(b.getName()); %></label>
                  	  		<% }%>
                  	  <% } %>
                     </div>
                  </div>
                  
                 <%
				 param = new QueryParameter();
				 param.setClause(param.getClause()+" AND "+Bank.TYPE+" ='"+"VA"+"'");
				 bank = bankMapper.getList(param);
				 %>
				  <div class="form-row <% if( bank.size()==0) out.print("d-none"); else out.print(""); %>">
                  	<div class="form-group col-md-12">
                  	  <div class="card-title font-weight-bold">Transfer Virtual Account(Verifikasi by Sistem)</div>
                  	  <% for(Bank b : bank) { %>
	                  	  	<% if(b.getType().equals("VA")) {%>
                  	  		<label class="form-control form-control-sms radio-inline"><input type="radio" name="bank_id" value="<% out.print(b.getId()); %>" data-type-bank="<% out.print(b.getType()); %>"> <% out.print(b.getName()); %></label>
                  	  		<% }%>
                  	  <% } %>
                     </div>
                  </div>
				</form>
				</div>
			</div>
		  </div>
		  
		  <div class="card-body bg-light">
			<div class="row ">
				<div class="col-md-12">
				 <form>
				  <div class="form-row">
                  	<div class="form-group col-md-12">
                  	  <div class="card-title font-weight-bold">Detail Price</div>
                  	  <div class="row">
	                      <label class="col-6 small"><% out.print("PNBP WNI"+"("+purchase.getCountTicketWni()+" orang x "+purchase.getTotalDays()+" hari)"); %> </label>
	                      <label class="col-6 small text-right"><% out.print(formatter.format(purchase.getAmountTicketWni())); %> </label>
                      </div>
                  	  <div class="row">
	                      <label class="col-6 small"><% out.print("PNBP WNA"+"("+purchase.getCountTicketWna()+" orang x "+purchase.getTotalDays()+" hari)"); %> </label>
	                      <label class="col-6 small text-right"><% out.print(formatter.format(purchase.getAmountTicketWna())); %> </label>
                      </div>
                  	  <!-- <div class="row">
	                      <label class="col-6">Service Fee</label>
	                      <label class="col-6 text-right">0</label>
                      </div> -->
                      <%
                      for(MasterPayment mp : masterPayment) { 
                      %>
							<div class="row">
							<% if(mp.getName().equals("PNBP Pendakian")) { %>
		                      	<label class="col-8 small"><% out.print(mp.getName()+"("+(formatter.format(mp.getPrice()))+" x  "+purchase.getCountTicket()+" orang)"); %></label>
		                      	<label class="col-4 small text-right"><% out.print(formatter.format(mp.getPrice()*purchase.getCountTicket())); %></label>
		                    <% } else if(mp.getName().equals("Jasa")) { %>
		                      	<label class="col-8 small"><% out.print(mp.getName()+"("+(formatter.format(mp.getPrice()))+" x  "+purchase.getCountTicket()+" orang x "+1+" kegiatan)"); %></label>
		                      	<label class="col-4 small text-right"><% out.print(formatter.format(mp.getPrice()*purchase.getCountTicket())); %></label>
	                    	<% 
	                    	  	priceMasterPayment+=mp.getPrice()*purchase.getCountTicket();
	                    		}else if(mp.getName().equals("Asuransi")) { %>
		                      	<label class="col-8 small"><% out.print(mp.getName()+" WNI("+(formatter.format(mp.getPrice()))+" x  "+purchase.getCountTicketWni()+" orang x "+purchase.getTotalDays()+" hari)"); %></label>
		                      	<label class="col-4 small text-right"><% out.print(formatter.format(mp.getPrice()*purchase.getCountTicketWni()*purchase.getTotalDays())); %></label>
		                    <% 
                    	  		priceMasterPayment+=mp.getPrice()*purchase.getCountTicketWni()*purchase.getTotalDays();
		                    }else if(mp.getName().equals("Asuransi WNA")) { %>
		                      	<label class="col-8 small"><% out.print(mp.getName()+"("+(formatter.format(mp.getPrice()))+" x  "+purchase.getCountTicketWna()+" orang x "+purchase.getTotalDays()+" hari)"); %></label>
		                      	<label class="col-4 small text-right"><% out.print(formatter.format(mp.getPrice()*purchase.getCountTicketWna()*purchase.getTotalDays())); %></label>
		                    <% 
                    	  		priceMasterPayment+=mp.getPrice()*purchase.getCountTicketWna()*purchase.getTotalDays();
		                    } else if(mp.getName().equals("Traking")) { %>
		                      	<label class="col-8 small"><% out.print(mp.getName()+"("+(formatter.format(mp.getPrice()))+" x  "+purchase.getCountTicket()+" orang x "+1+" kegiatan)"); %></label>
		                      	<label class="col-4 small text-right"><% out.print(formatter.format(mp.getPrice()*purchase.getCountTicket())); %></label>
		                    <% 
                    	  		priceMasterPayment+=mp.getPrice()*purchase.getCountTicket();
		                    } %>
	                      	</div>
                  	  <% } %>
                  	  <div class="row d-none">
	                      <label class="col-6">Unique Code</label>
	                      <label class="col-6 text-right"><% out.print(purchase.getNumberUnique()); %></label>
                      </div>
                      <hr/>
                  	  <div class="row">
	                      <label class="col-6">Total Price</label>
	                      <label class="col-6 text-right font-weight-bold"><% out.print(formatter.format(purchase.getAmountTotalTicket()+priceMasterPayment)); %></label>
                      </div>
                     </div>
                  </div>
				</form>
				</div>
			</div>
		  </div>
		  
		  <div class="card-body">
			<div class="row ">
				<div class="col-md-12">
				 <form id="form-detil-transaction">
				 <input type="hidden" name="header_id" value="<%out.print(request.getParameter("id"));%>">
				 <input type="hidden" name="step" value="2">
				  <div class="form-row">
				  <div class="form-group col-md-12">
				    <div class="form-check-label text-justify" >Agar dapat melanjutkan pembayaran mohon untuk	 mengklik link berikut 
				     <a id="btn-link-mengerti" href="#staticBackdrop" data-toggle="modal" >Syarat & ketentuan dan Kebijakan Privasi TNGHS</a>
				     dan <strong>membacanya hingga selesai.</strong></div>
				  </div>
				  </div>
				  <button type="submit" class="btn btn-primary float-right" >Continue/Lanjutkan</button>
				</form>
				</div>
			</div>
		  </div>
		</div>
		</div>
		<% }else {
				if(purchaseDetilTransaction.getConfirmationPayment()==null){

					if(purchaseDetilTransaction.getStatusExpired()==1){ 
						response.sendRedirect("/kawasan/page/expired?id="+id);
					}
					%>
						
			<div class="col-md-6">
			
			<% if(purchaseDetilTransaction.getPaymentMethod().equals("ATM")
					//request.getParameter("VA")==null
					){ %>
			<div class="card">
			<div class="card-header">
			    <h5 class="card-title">Petunjuk Pembayaran</h5>
			</div>
			  <div class="card-body">
				<div class="row ">
					<div class="col-md-12">
					 <form id="form-detil-transaction">
					 <input type="hidden" name="id" value="<%out.print(purchaseDetilTransaction.getId());%>">
					 <input type="hidden" name="step" value="3">
					 <input type="hidden" name="confirmation_payment" value="1">
					 <div id="msg-notif">
					 </div>
					  <div class="form-row">
	                  	<div class="form-group col-md-12">
	                  	  <div class="card-title font-weight-bold">1. Selesaikan Pembayaran Sebelum</div>
	                      	<div class="card border-info">
							<div class="card-body ">
			                	<div class="row">
			                    <div class="col-sm-12 text-justify">
			                    	<div class="font-weight-bold">
			                    		<% out.print(new Date().getDate()==purchaseDetilTransaction.getExpiredDate().getDate()?"Today":"Tomorrow"); %> <% out.print(sdftime.format(purchaseDetilTransaction.getExpiredDate())); %>
			                    	</div>
			                    	<%
			                    	String dateStart = "21/08/23 08:00:00";
			                    	String dateStop = "21/08/23 08:30:00";

			                    	// Custom date format
			                    	SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd HH:mm:ss");  

			                    	Date d1 = format.parse(dateStart);
			                    	Date d2 = format.parse(dateStop);

			                    	// Get msec from each, and subtract.
			                    	long diff = purchaseDetilTransaction.getExpiredDate().getTime() - purchaseDetilTransaction.getSubmitedDate().getTime();//
			                    	//diff= d2.getTime() - d1.getTime();
			                    	long diffSeconds = diff / 1000 % 60;  
			                    	long diffMinutes = diff / (60 * 1000) % 60; 
			                    	long diffHours = diff / (60 * 60 * 1000);
			                    	%>
			                    	<div>Selesaikan pembayaran dalam <% out.print(diffHours+" jam "+diffMinutes+" menit"); %> </div>
			                    	<small class="text-muted">Perlu diingat anda hanya memiliki waktu 2 jam untuk menyelesaikan pembayaran, harap tidak mengklik tombol "Saya Sudah Bayar" sebelum anda benar-benar telah melakukan pembayaran, agar admin dapat mengetahui informasi pembayaran yang anda lakukan dengan cepat. Setelah itu admin akan memverifikasi pesanan anda dalam waktu kurang lebih 15 menit.</small>
								</div>
			                    </div>
		                    </div>
	                    	</div>
	                     </div>
	                  </div>
					  <div class="form-row">
	                  	<div class="form-group col-md-12">
	                  	  <div class="card-title font-weight-bold">2. Mohon Transfer Ke</div>
	                      <!-- <label class="form-control form-control-sms radio-inline">Hari Ini 15:40 PM</label>
	                      <p>Selesaikan pembayaran dalam 1 jam 2 menit</p> -->
	                      <div class="card">
							<div class="card-body ">
			                	<div class="row">
			                     <label class="col-sm-6  card-title font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getName()); %></label>
			                     <div class="col-sm-6"><span>-</span></div>
			                    </div>
			                	<div class="row">
			                     <label class="col-sm-6">Kode Bank</label>
			                     <label class="col-sm-6 font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getCode()); %></label>
			                    </div>
			                	<div class="row">
			                     <label class="col-sm-6">Nomor Rekening</label>
			                     <label class="col-sm-6 font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getAccountNumber()); %></label>
			                    </div>
			                	<div class="row">
			                     <label class="col-sm-6">Nama Penerima</label>
			                     <label class="col-sm-6 font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getAccountHolderName()); %></label>
			                    </div>
								<hr/>
			                	<div class="row">
			                     <label class="col-sm-6">Jumlah Transfer</label>
			                     <%
			                     for(PurchaseDetilPayment pdp : dataPurchaseDetilPayment){
			                    	  priceMasterPayment+=pdp.getPrice();
			                     }
			                     String money = formatter.format(purchaseDetilTransaction.getPurchase().getAmountTotalTicket()+priceMasterPayment);
			                     String newmoney = money.substring(0, money.length()-2).replaceAll("\\.", "");
			                     
			                     //newmoney = newmoney.substring(0, money.length());
			                     String[] newformat = newmoney.split(",");
			                     %>
			                     <% if(newformat.length==3) {%>
			                     	<label class="col-sm-6 font-weight-bold"><% out.print(newformat[0]+","+newformat[1]+",<span class='bg-warning'>"+newformat[2]+"</span>"); %></label>
			                     <% }else{ %>
			                      	<% if(newformat.length > 0) {%>
			                     	<label class="col-sm-6 font-weight-bold"><% out.print(newformat[0]+",<span class='bg-warning'>"+newformat[1]+"</span>"); %></label>
			                     	<% }else{ %>
			                     		<label class="col-sm-6 font-weight-bold d-none"><% out.print(newmoney+",<span class='bg-warning'>"+0+"</span>"); %></label>
			                     	<% } %>
			                    <% } %>
			                    </div>
							</div>
							<div class="card-footer">
			                	<div class="row bg-warning">
			                     <div class="col-sm-12">PENTING! Harap cantumkan kode <strong>#INV<% out.print(purchaseDetilTransaction.getPurchase().getNumberUnique()); %></strong> secara benar, pada berita transfer agar booking cepat terverifikasi.</div>
			                    </div>
							</div>
	                      </div>
	                      
						  <div class="form-row pt-3">
		                  	<div class="form-group col-md-12">
		                  	  <div class="card-title font-weight-bold">3. Anda Sudah membayar?</div>
		                      <p>Setelah pembayaran Anda dikonfirmasi, kami akan mengirim voucher aktivitas ke alamat email anda.</p>
				  			  <button type="submit" class="btn btn-primary">Saya Sudah Bayar</button>
		                     </div>
		                  </div>
	                     </div>
	                  </div>
					</form>
					</div>
				</div>
			  </div>
			</div>
			
			<%}
			if(purchaseDetilTransaction.getPaymentMethod().equals("VA")){ 
                 if(purchase.getStatus().equals("PAYMENT")){ %>
                 		<div class="card">
					<div class="card-body ">
	                	<div class="row">
	                     <label class="col-sm-12 text-center">Proses Pembayaran telah berhasil diverifikasi, silahkan cek email anda untuk mendapatkan e-ticket anda.
	                     atau <a href="manage-user?id=<% out.print(id);%>&menu=booking_detil">klik disini</a>
	                     </label>
	                     
	                    </div>
					</div>
                     </div>
                 	<% }
                 	else if(purchase.getStatus().equals("CANCEL")){%>
                 	<div class="card">
					<div class="card-body ">
	                	<div class="row">
	                     <label class="col-sm-12 text-center">Proses Pembayaran telah di batalkan, karena sistem tidak menemukan pembayaran anda.</label>
	                    </div>
					</div>
                     </div>
			<%	}else{
			%>
			
			<div class="card">
			<div class="card-header">
			    <h5 class="card-title">Petunjuk Pembayaran</h5>
			</div>
			  <div class="card-body">
				<div class="row ">
					<div class="col-md-12">
					 <form id="form-detil-transaction">
					 <input type="hidden" name="id" value="<%out.print(purchaseDetilTransaction.getId());%>">
					 <input type="hidden" name="step" value="3">
					 <input type="hidden" name="confirmation_payment" value="1">
					 <div id="msg-notif" class="form-row">
					 </div>
					  <div class="form-row">
	                  	<div class="form-group col-md-12">
	                  	  <div class="card-title font-weight-bold">1. Selesaikan Pembayaran Sebelum</div>
	                      
	                      
	                      	<div class="card">
							<div class="card-body ">
			                	<div class="row">
			                    <div class="col-sm-12">
			                    	<div class="font-weight-bold">
			                    		<% out.print(new Date().getDate()==purchaseDetilTransaction.getExpiredDate().getDate()?"Today":"Tomorrow"); %> <% out.print(sdftime.format(purchaseDetilTransaction.getExpiredDate())); %>
			                    	</div>
			                    	<%
			                    	String dateStart = "21/08/23 08:00:00";
			                    	String dateStop = "21/08/23 08:30:00";

			                    	// Custom date format
			                    	SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd HH:mm:ss");  

			                    	Date d1 = format.parse(dateStart);
			                    	Date d2 = format.parse(dateStop);

			                    	// Get msec from each, and subtract.
			                    	long diff = purchaseDetilTransaction.getExpiredDate().getTime() - purchaseDetilTransaction.getSubmitedDate().getTime();//
			                    	//diff= d2.getTime() - d1.getTime();
			                    	long diffSeconds = diff / 1000 % 60;  
			                    	long diffMinutes = diff / (60 * 1000) % 60; 
			                    	long diffHours = diff / (60 * 60 * 1000);
			                    	%>
			                    	<div>Selesaikan pembayaran dalam <% out.print(diffHours+" jam "+diffMinutes+" menit"); %> </div>
								</div>
			                    </div>
		                    </div>
	                    	</div>
	                     
	                     </div>
	                  </div>
					  <div class="form-row">
	                  	<div class="form-group col-md-12">
	                  	  <div class="card-title font-weight-bold">2. Mohon Transfer Ke</div>
	                      <!-- <label class="form-control form-control-sms radio-inline">Hari Ini 15:40 PM</label>
	                      <p>Selesaikan pembayaran dalam 1 jam 2 menit</p> -->
	                      <div class="card">
							<div class="card-body ">
			                	<div class="row">
			                     <label class="col-sm-6  card-title font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getName()); %></label>
			                     <div class="col-sm-6"><span>-</span></div>
			                    </div>
			                	<%-- <div class="row">
			                     <label class="col-sm-6">Kode Bank</label>
			                     <label class="col-sm-6 font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getCode()); %></label>
			                    </div> --%>
			                	<div class="row">
			                     <label class="col-sm-6">Nomor Virtual Account</label>
			                     <label class="col-sm-6 font-weight-bold"><% out.print(purchaseDetilTransaction.getPaymentCode()); %></label>
			                    </div>
			                	<div class="row">
			                     <label class="col-sm-6">Nama Penerima</label>
			                     <label class="col-sm-6 font-weight-bold"><% out.print(purchaseDetilTransaction.getBank().getAccountHolderName()); %></label>
			                    </div>
								<hr/>
			                	<div class="row">
			                     <label class="col-sm-6">Jumlah Transfer</label>
			                     <%
			                     String  money = formatter.format(purchaseDetilTransaction.getPurchase().getAmountTotalTicket());
			                     String newmoney = money.substring(0, money.length()-2).replaceAll("\\.", "");
			                     
			                     //newmoney = newmoney.substring(0, money.length());
			                     String[] newformat = newmoney.split(",");
			                     %>
			                     <!-- <label class="col-sm-6 font-weight-bold">1,000.<span class="bg-warning">202</span><br/> -->
			                     <%-- <label class="col-sm-6 font-weight-bold"><% out.print(newmoney); %></label> --%>
			                     <% if(newformat.length==3) {%>
			                     
			                     <label class="col-sm-6 font-weight-bold"><% out.print(newformat[0]+","+newformat[1]+",<span class='bg-warning'>"+newformat[2]+"</span>"); %></label>
			                     <% }else{ %>
			                      	<% if(newformat.length > 0) {%>
			                     	<label class="col-sm-6 font-weight-bold"><% out.print(newformat[0]+",<span class='bg-warning'>"+newformat[1]+"</span>"); %></label>
			                     	<% }else{ %>
			                     		<label class="col-sm-6 font-weight-bold d-none"><% out.print(newmoney+",<span class='bg-warning'>"+0+"</span>"); %></label>
			                     	<% } %>
			                    <% } %>
			                    </div>
							</div>
							<div class="card-footer d-none">
			                	<div class="row bg-warning">
			                     <div class="col-sm-12">PENTING! Harap mencantumkan kode <strong>#INV<% out.print(purchaseDetilTransaction.getPurchase().getNumberUnique()); %>SIM</strong> pada berita transfer agar booking anda cepet terkonfirmasi.</div>
			                    </div>
							</div>
	                      </div>
	                      
						  <div class="form-row pt-3">
		                  	<div class="form-group col-md-12">
		                  	  <div class="card-title font-weight-bold">3. Anda Sudah membayar?</div>
		                      <p>Setelah sistem mendeteksi pembayaran anda, maka sistem akan mengirim e-ticket ke alamat email anda.</p>
				  			  <button type="submit" class="btn btn-primary d-none">Lanjut</button>
		                     </div>
		                  </div>
	                     </div>
	                  </div>
					</form>
					</div>
				</div>
			  </div>
			</div>
				
			<% }
			
			} %>
			</div>
					<%
				}else{
					if(purchaseDetilTransaction.getConfirmationPayment()==1){
						if(purchase.getStatus().equals("PAYMENT")){
							
						}else if(purchaseDetilTransaction.getStatusExpired()==1){ 
							//response.sendRedirect("/kawasan/page/expired?id="+id);
						}
						%>
						<% if(purchaseDetilTransaction.getPaymentMethod().equals("VA")) { %>
							
							<div class="col-md-6">
							<div class="card">
							<div class="card-header">
							    <h5 class="card-title">Konfirmasi Pembayaran</h5>
							</div>
							  <div class="card-body">
								<div class="row ">
									<div class="col-md-12">
									 <form id="form-detil-transaction">
						 			 <input type="hidden" name="reference" value="<%out.print(purchaseDetilTransaction.getId());%>">
						 			 <input type="hidden" name="id" value="<%out.print(purchaseDetilTransaction.getId());%>">
									  <div class="form-row">
					                  	<div class="form-group col-md-12">
					                  <% if(purchase.getStatus().equals("PAYMENT")){ %>
					                  		<div class="card mt-3">
											<div class="card-body ">
							                	<div class="row d-none">
							                     <label class="col-sm-12 text-center">Kami sedang melakukan verifikasi pembayaran Anda.</label>
							                    </div>
							                	<div class="row">
							                     <label class="col-sm-12 text-center">Proses Pembayaran telah berhasil diverifikasi, silahkan cek email anda untuk mendapatkan e-ticket anda.
							                     atau <a href="manage-user?id=<% out.print(id);%>&menu=booking_detil">klik disini</a>
							                     </label>
							                     
							                    </div>
											</div>
					                      </div>
					                  	<% }
					                  	else if(purchase.getStatus().equals("CANCEL")){%>
					                  	<div class="card mt-3">
											<div class="card-body ">
							                	<div class="row">
							                     <label class="col-sm-12 text-center">Proses Pembayaran telah di batalkan, karena sistem tidak menemukan pembayaran anda.</label>
							                    </div>
											</div>
					                      </div>
					                  	<%}else{%>
					                  	
					                      <div class="card">
											<div class="card-body ">
							                	<div class="row">
							                     <label class="col-sm-12">Waktu Proses</label>
							                    </div>
							                	<div class="row">
							                     <h5 class="col-sm-12"><span id="minutes">00</span>:<span id="seconds">00</span></h5>
							                     <!-- <h5 id="span-timer" class="col-sm-12">Mohon tunggu..</h5> -->
							                    </div>
							                	<div class="row">
							                     <label class="col-sm-12">Maksimal 15 menit</label>
							                    </div>
											</div>
					                      </div>
					                      
					                      <div class="card mt-3">
											<div class="card-body ">
							                	<div class="row">
							                     <label class="col-sm-12 text-center">Kami sedang melakukan verifikasi pembayaran Anda.</label>
							                    </div>
							                	<div class="row">
							                     <label class="col-sm-12 text-center">Proses verifikasi dapat berlangsung hingga 15 menit.</label>
							                    </div>
											</div>
					                      </div>
					                      
					                    <% } %>
					                     </div>
					                  </div>
									</form>
									</div>
								</div>
							  </div>
							</div>
							</div>
						<% }else{ %>
							<div class="col-md-6 lead">
							<div class="card">
							<div class="card-header">
							    <h5 class="card-title">Konfirmasi Pembayaran</h5>
							</div>
							  <div class="card-body">
								<div class="row ">
									<div class="col-md-12">
									 <form id="form-detil-transaction">
						 			 <input type="hidden" name="reference" value="<%out.print(purchaseDetilTransaction.getId());%>">
									   <input type="hidden" name="id" value="<%out.print(purchaseDetilTransaction.getId());%>">
									 <div class="form-row">
					                  	<div class="form-group col-md-12">
					                  	<% if(purchaseDetilTransaction.getFileId()!=null){ 
					                  	if(purchase.getStatus().equals("PAYMENT")){ %>
					                  		<div class="card mt-3">
											<div class="card-body ">
							                	<div class="row d-none">
							                     <label class="col-sm-12 text-center">Kami sedang melakukan verifikasi pembayaran Anda.</label>
							                    </div>
							                	<div class="row">
							                     <div class="col-sm-12 text-center">
							                     	<h2>Pembayaran Berhasil</h2>
								                     <span class="fa fa-3x fa-check-circle text-success"></span>
								                     <label class="text-center">
								                     Proses Pembayaran telah berhasil diverifikasi, silahkan cek email anda untuk mendapatkan e-ticket anda.
								                     atau <a href="manage-user?id=<% out.print(id);%>&menu=booking_detil">klik disini</a>
								                     </label>
							                     </div>
							                    </div>
											</div>
					                      </div>
					                  	<% }
					                  	else if(purchase.getStatus().equals("CANCEL")){%>
					                  	<div class="card mt-3">
											<div class="card-body ">
							                	<div class="row">
							                     <div class="col-sm-12 text-center">
							                     	<h2>Pembayaran di Tolak</h2>
								                     <span class="fa fa-3x fa-times-circle text-danger"></span>
								                     <label class="text-center">
								                     Proses Pembayaran Anda di tolak, karena sistem tidak menemukan pembayaran anda.
								                     atau <a href="manage-user?id=<% out.print(id);%>&menu=booking_detil">klik disini</a>
								                     </label>
							                     </div>
							                    </div>
											</div>
					                      </div>
					                  	<%}else{
					                  		if(purchaseDetilTransaction.getStatusExpired()==1){
					                  			%>
					 							<div class="card mt-3">
													<div class="card-body small">
									                	<div class="row">
									                		<label class="col-sm-12 text-center">Mohon Maaf, sepertinya admin kami belum dapat mengkonfirmasi pembayaran anda. <a href="${pageContext.request.contextPath}/page/general">Untuk info lebih lanjut, silahkan hubungi admin kami, terimakasih</a></label>
									                    </div>
													</div>
							                    </div>
					 							<%
					 						}else{
					 							%>
					 							
					                      <div class="card">
											<div class="card-body text-center">
												<div class="row">
													<label class="col-sm-12"><span class="far fa-2x fa-clock text-warning"></span></label>
												</div>
							                	<div class="row">
							                     <label class="col-sm-12">Waktu Proses</label>
							                    </div>
							                	<div class="row">
							                     <h5 class="col-sm-12"><span id="minutes">00</span>:<span id="seconds">00</span></h5>
							                     <!-- <h5 id="span-timer" class="col-sm-12">Mohon tunggu..</h5> -->
							                    </div>
							                    <div class="row">
							                     <label class="col-sm-12 text-center">Proses verifikasi dapat berlangsung hingga 15 menit.</label>
							                    </div>
											</div>
					                      </div>
					                      
					                      
					                      <div class="card mt-3">
											<div class="card-body small">
												<div class="d-none">
												<%
												out.print(timeOfDay);
							                	if(timeOfDay >= 8 && timeOfDay <= 16){
							                		out.print("IN TIME");
							                	}else{
							                		out.print("OUT TIME");        
							                	} 
							                	%>
							                	</div>
							                	
												<%
							                	if(timeOfDay >= 8 && timeOfDay <= 16){
												%>
													<div class="row">
								                     <label class="col-sm-12 text-center">Kami sedang melakukan verifikasi pembayaran Anda.</label>
								                    </div>
								                	<div class="row">
								                     <label class="col-sm-12 text-center">Proses verifikasi dapat berlangsung hingga 15 menit.</label>
								                    </div>
								                <%
							                	}else{
							                		%>
								                	<div class="row">
								                     <label class="col-sm-12 text-center">Anda memesan tiket diluar jam kerja, apabila dalam 15 menit proses verifikasi belum selesai, Anda tidak perlu khawatir kami akan segera memproses pada jam kerja kami 08.00 - 16.00</label>
								                    </div>
							                		<%
							                	}
							                	%>
											</div>
					                      </div>
					 							<%
					 						}
					                      
					                     }
					                  	}else{ %>
					                      <div class="row">
											<div class="col-md-12 ">
												  <label class="font-weight-bold">Informasi Rekening Pengirim</label>
												  <div class="form-row">
												    <div class="form-group col-md-12">
												      <label for="">Nama Pengirim*</label>
												      <input type="text" placeholder="Ketik Nama Pengirim" class="form-control" name="account_name_sender" autocomplete="off">
												    </div>
												  </div>
												  <div class="form-row">
												    <div class="form-group col-md-6">
												      <label for="">Nama Bank*</label>
												      <input type="text" placeholder="Ketik Nama Bank Pengirim" class="form-control" name="bank_name_sender" autocomplete="off">
												    </div>
												    <div class="form-group col-md-6">
												      <label for="">Nomor Rekening*</label>
												      <input type="text" placeholder="Nomor Rekening Pengirim" class="form-control" name="account_number_sender" autocomplete="off">
												    </div>
												  </div>
												  
												  <hr/>
												  
												  <label class="font-weight-bold">Informasi Rekening Penerima</label>
												  <div class="form-row">
												    <div class="form-group col-md-12">
												      <label for="">Nama Penerima*</label>
												      <input readonly type="text" placeholder="Ketik Nama Penerima" class="form-control" name="account_name_receiver" autocomplete="off" value="<% out.print(purchaseDetilTransaction.getBank().getAccountHolderName()); %>">
												    </div>
												  </div>
												  <div class="form-row">
												    <div class="form-group col-md-6">
												      <label for="">Nama Bank*</label>
												      <input readonly type="text" placeholder="Ketik Nama Bank Penerima" class="form-control" name="bank_name_receiver" autocomplete="off" value="<% out.print(purchaseDetilTransaction.getBank().getName()); %>">
												    </div>
												    <div class="form-group col-md-6">
												      <label for="">Nomor Rekening*</label>
												      <input readonly type="text" placeholder="Nomor Rekening Penerima" class="form-control" name="account_number_receiver" autocomplete="off" value="<% out.print(purchaseDetilTransaction.getBank().getAccountNumber()); %>">
												    </div>
												  </div>
												  
												  <hr/>
												  <label class="font-weight-bold">Upload Bukti dan Tanggal Transfer</label>
												  
												  <div class="form-row">
												    <div class="form-group col-md-6">
												      <label for="">Tanggal Transfer*</label>
												      <input type="date" class="form-control" name="transaction_date" autocomplete="off">
												    </div>
												    <div class="form-group col-md-6">
												      <label for="">Unggah Bukti Transfer*</label>
									                     <input required name="file_transaction" type="file" class="d-none" accept="image/*">
									                     <button id="btn-upload" type="button" onclick="upload();" class="btn btn-warning text-white">Unggah Bukti Pembayaran</button>
									                     <span class="text-small small text-muted">*Anda dapat mengunggah sampai 2MB dalam format PNG, JPG atau JPEG</span>
												    </div>
												  </div>
												  
												  <div class="form-row d-none">
												    <div class="form-group col-md-12">
												      <input type="text" class="form-control" name="email_notification" autocomplete="off">
												    </div>
												  </div>
												  <hr/>
												  
					                  		<% 	
					                  			if(purchaseDetilTransaction.getFileId()==null){ 
					                  				 out.print("<button onclick='submitupload()'  type='button' class='btn btn-primary float-right'>Submit</button>");
					                  			}else{ 
					                  				out.print("<button type='submit' class='btn btn-primary float-right'>Submit</button>");
					                  			} 
					                  		%>
												 
											</div>
					                      </div>
					                      
					                      <% } %>
					                     </div>
					                  </div>
									</form>
									</div>
								</div>
							  </div>
							</div>
							</div>
						<% } %>
						
						<%
					}
				}
			}
		%>
		
        <%  if(!purchase.getStatus().equals("PAYMENT")){ %>
		<div class="col-md-4 mt-3 mt-lg-0">
			<div class="card">
			   <!-- <div class="card-header">
			      <h5 class="card-title">NO Pesanan</h5>
			      </div> -->
			   <div class="card-body small">
			      <div class="card-title mb-1 text-muted">BOOKING DETAILS</div>
			      <hr>
			      <div class="row no-gutters">
			         <div class="col-6 text-muted">
			            <p class="mb-2">Jalur</p>
			            <p class="mb-2 fs--1">Hari, Tgl Pendakian</p>
			            <!-- <p class="mb-2 fs--1">Transit Camp</p>
			            <p class="mb-2 fs--1">Jam Lapor di BC</p>
			            <p class="mb-2 fs--1 ">Nomor Kapling</p> -->
			            <p class="mb-2 fs--1">Nama Ketua</p>
			            <p class="mb-2 fs--1">Jumlah Pendaki</p>
			            <p class="mb-2 fs--1">Jumlah Pembayaran</p>
			            <p class="mb-2 fs--1">Kode Booking</p>
			         </div>
			         
			        <%
			        String ketuaTim="";
			         if(purchase.getParentId()!=null){
			        	param_detil = new QueryParameter();
						param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
						param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is not null)");
						Purchase purchaseNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
						
						
						//manipulate this for childeren purchase cause the parent have what this need
						purchase.setPurchaseDetilVisitor(purchaseNoParent.getPurchaseDetilVisitor());
						purchase.setPurchaseDetilTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
			         	

			        	param_detil = new QueryParameter();
						param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
						param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is null)");
						purchaseNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
						ketuaTim=purchaseNoParent.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName();
					}else{
			        	 ketuaTim=purchase.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName();
			         }
					%>
					
			         <div class="col-6">
			            <div class="media text-right">
			               <div class="media-body">
			                  <p class="mb-2 font-weight-bold"> <% out.print(purchase.getRegion().getName()+" - "+purchase.getRegionEndName()); %></p>
			                  <p class="mb-2 font-weight-bold"> <% out.print(sdf.format(purchase.getStartDate())); %></p>
 			                  <%-- <p class="mb-2 font-weight-bold text-truncate"> <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp()); %></p>
 			                  <p class="mb-2 fs--1 font-weight-bold"> <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp()); %></p> --%>
 			                 <%--  <p class="mb-2 fs--1 font-weight-bold d-none"> <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique()); %></p> --%>
 			                  
			                  <p class="mb-2 fs--1 font-weight-bold"><% out.print(ketuaTim); %></p>
			                  
			                  <p class="mb-2 fs--1 font-weight-bold"> <a title="klik untuk melihat detail" onclick="$('#modal-view').modal('show')" href="javascript:void(0)"><% out.print(purchase.getCountTicket()); %></a></p>
			                  <% 
			                  //System.out.println("MAGIC");
			                  //System.out.println(dataPurchaseDetilPayment); 
			                  %>
			                  <% if(dataPurchaseDetilPayment.size()==0) { %>
			                  	<p class="mb-2 fs--1 font-weight-bold"> <% out.print(formatter.format(purchase.getAmountTicket()+priceMasterPayment)); %></p>
			                  <% }else{ %>
			                  	<p class="mb-2 fs--1 font-weight-bold"> <% out.print(formatter.format(purchase.getAmountTicket())); %></p>
			                  <% } %>
			                  <p class="mb-2 fs--1 font-weight-bold"> <% out.print(purchase.getCodeBooking()); %></p>
			                  
			               </div>
			            </div>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="card d-none">
				<!-- <div class="card-header">
				    <h5 class="card-title">NO Pesanan</h5>
				</div> -->
				<div class="card-body " style="font-size:14px;">
					<div class="card-title mb-1 text-muted">BOOKING ID</div>
					<div><% out.print(purchase.getId()); %></div>
					<hr/>
					<div class="card-title mb-1 text-muted">BOOKING DETAILS</div>
					<div class="row">
                     <div class="col-sm-12">
                     	<label class="font-weight-bold"><% out.print(purchase.getRegion().getName()+"("+purchase.getTotalDays()+" days)"); %></label>
					 </div>
					</div>
                	<div class="row">
                     <label class="col-sm-6 text-muted">Selected Visit Date</label>
                     <label class="col-sm-6 text-right font-weight-bold"><% out.print(sdf.format(purchase.getStartDate())); %></label>
                    </div>
                	<div class="row">
                     <label class="col-sm-6 text-muted">Until Visit Date</label>
                     <label class="col-sm-6 text-right font-weight-bold"><% out.print(sdf.format(purchase.getEndDate())); %></label>
                    </div>
                	<div class="row">
                     <label class="col-sm-6 text-muted">Total Visitor</label>
                     <label class="col-sm-6 text-right font-weight-bold">Pax:<% out.print(purchase.getCountTicket()); %></label>
                    </div>
				</div>
			</div>
		</div>
		
         <% } %>
		</div>
		</div>
	</div>
	</div>
	</div>
	
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header alert-primary">
            <h5 class="modal-title" id="">Detail Pendaki</h5>
            <!-- <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button> -->
          </div>
          <div class="modal-body">
          <p id="modal-alert-view-msg"></p>
          <div class="row">
			      <div class="col-md-12">
			      	<div class="table-responsive ">
			         <table id="tbl-detil-individu" class="table table-sm table-striped table-hover m-0" style="opacity: 0.8;overflow: auto; display: inline-table;">
			            <thead class="bg-head text-dark">
			               <tr>
			                  <th class="align-middle" scope="col">#</th>
			                  <th class="align-middle" scope="col">No.Identitas</th>
			                  <th class="align-middle" scope="col">Nama Lengkap</th>
			                  <th class="align-middle" scope="col">Jenis Kelamin</th>
			                  <th class="align-middle" scope="col">No.HP</th>
			                  <th class="align-middle" scope="col"></th>
			               </tr>
			            </thead>
			            <tbody class="bg-light text-dark">
			            <%	int no=1;
			            	for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
			            		pdv.getVisitorIdentity().setNoIdentity(vaultService.decrypt(pdv.getVisitorIdentity().getNoIdentity()));
			            		
			            		if(pdv.getResponsible().equals("0")){
			           	%>
			            			<tr class="data-row-identity <% out.println((purchase.getParentId()==null && no==1?"bg-success":""));  %>" id="row-<% out.print(pdv.getId()); %>">   
					            		<td><% out.print(no); %></td>   
					            		<td><% out.print(pdv.getVisitorIdentity().getNoIdentity()); %></td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getFullName()); %> </td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getGender()); %> </td>   
					            		<td class=""> <% out.print(pdv.getPhoneNumber()); %> </td>  
					            		<td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getVisitorIdentity().getIdFile()); %>?filename=<% out.print(pdv.getVisitorIdentity().getFileName()); %>&download&decrypt','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
					            	</tr>
			            <%
			            			no++;
			            		}
			            	}
			            %>
			           </tbody>
			         </table>
			         </div>
			      </div>
			   </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
	<div class="remodal" data-remodal-id="modalku" style="padding: 0;min-width: 75%;">
	  <button data-remodal-action="close" class="remodal-close"></button>
	  <!-- <h1>Remodal</h1> -->
	  	<img id="view_photo" src="" class="img-fluid">
	</div>
	
	<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="staticBackdropLabel">Syarat & ketentuan dan Kebijakan Privasi </h5>
	        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button> -->
	      </div>
	      <div class="modal-body lead">
	        <!-- <div class="font-weight-bold">Saya menyatakan hal-hal sebagai berikut :</div>
			<p>1. Saya telah memberikan data yang benar dan saya bertanggung jawab penuh terhadap kebenaran data tersebut.</p>
			<p>2. Setelah melakukan pembayaran dan mendapatkan kode booking, saya tidak akan melakukan pergantian atau penambahan personil dalam tim.</p>
			<p>3. Apabila ada perubahan jumlah dan nama personil, maka saya akan melakukan pendaftaran baru sesuai syarat dan ketentuan yang berlaku.</p>
			<p>4. Apabila sudah melakukan payment/pembayaran maka uang tidak dapat dikembalikan.</p> -->
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	        <button id="btn-mengerti" type="button" class="btn btn-success">Saya Mengerti & SETUJU</button>
	      </div>
	    </div>
	  </div>
	</div>
    
    <%@ include file = "inc/footer.jsp" %>
    <script>
    	var path_payment = ctx + '/ticket/payment';
    	var checkRealMail;
    	var timenow;
		function initjsp(){
			<%
			if(!purchase.getStatus().equals("VERIFYING")){
				System.out.println("ci coder");
			}else if(purchaseDetilTransaction!=null){ %>
			<%	if(purchaseDetilTransaction.getConfirmationPayment()!=null && purchaseDetilTransaction.getConfirmationPayment()==1 ){ %>
					<% if(purchaseDetilTransaction.getFileId()!=null) { %>
						//remember this is js
						ajaxGET(path_payment +"/"+$('[name=reference]').val()+"/CHECK_TIMELEFT",'onGetTimeLeftSucc','onCheckErr');

						
						timenow = setInterval(() => {
							console.log("check something");
						   	ajaxGET(ctx + '/ticket/purchase/status/'+selected_id,'onCheckStatusSucc','onCheckErr');
						}, 5000);
					<% } %>
			<%	}else{ %>
					$.LoadingOverlay("show", { textResizeFactor:0.3, text: "Mohon tunggu, kami sedang mempersiapkan form anda.", image : ctx + "/images/loading-spinner.gif" });
			   		checkRealMail = setInterval(function(){ checkStatusNotifMail(); }, 3000);
			<% 	} %>
			<% } %>
		}
    </script>
    
    <script src="${pageContext.request.contextPath}/js/payment.js"></script>
  </body>

</html>
