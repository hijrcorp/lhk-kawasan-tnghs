<%@page import="id.co.hijr.sistem.model.AccountGroup"%>
<%@page import="id.co.hijr.sistem.mapper.AccountGroupMapper"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionPhotoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionPhoto"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
RegionPhotoMapper regionPhotoMapper = appCtx.getBean(RegionPhotoMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);
AccountGroupMapper accountGroupMapper = appCtx.getBean(AccountGroupMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
QueryParameter param = new QueryParameter();
//
String paramfilter="";
paramfilter=request.getParameter("filter_keyword");
if(paramfilter==null)paramfilter="";

param = new QueryParameter();
param.setClause(param.getClause()+" AND "+Region.NAME+" LIKE  '%"+paramfilter+"%'");

if(!id.equals("0")){
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+Region.ID+" =  '"+id+"'");
}

List<Region> region = regionMapper.getList(param);
List<File> list_photo = new ArrayList<>();
List<RegionTodo> list_todo = new ArrayList<>();
List<RegionFacility> list_facility = new ArrayList<>();
if(region.size() == 1){
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.get(0).getId()+"'");
	list_photo = fileMapper.getList(param);

	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.get(0).getId()+"'");
	list_todo = regionTodoMapper.getList(param);
	region.get(0).setList_todo(list_todo);
	
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.get(0).getId()+"'");
	list_facility = regionFacilityMapper.getList(param);
	region.get(0).setList_facility(list_facility);
}
//param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
/* List<RegionFacility> regionFacility = regionFacilityMapper.getList(param); */
param = new QueryParameter();
List<RegionPhoto> regionPhoto = regionPhotoMapper.getList(param);


param = new QueryParameter();
param.setClause(param.getClause()+" AND "+AccountGroup.APPLICATION_ID+" =  '008'");
List<AccountGroup> accountGroup = accountGroupMapper.getList(param);


//untuk format currency
NumberFormat formatter = NumberFormat.getCurrencyInstance();
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>

  <body id="page-top">
    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">
      <div class="container">
      
		<div class="row justify-content-center">
		<div class="col-md-6 pb-3">
		   <div class="card">
		      <div class="card-body">
			    <div class="card-title"><h5>Apakah anda Pendaki Baru? </h5><p>Kami Sarankan agar anda menyimak video <strong>Safety Talk</strong> berikut:</p></div>
				<div class="embed-responsive embed-responsive-16by9">
				<!-- <iframe src="https://www.youtube.com/embed/nEbkGRrF2PM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" height="434" width="500"></iframe> -->
				<iframe width="1205" height="678" src="https://www.youtube.com/embed/gzqR_HvIVyI" title="BERPETUALANG DI TAMAN NASIONAL GUNUNG HALIMUN SALAK" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<hr/>
			    <div class="card-title">Mohon disimak sampai habis ya, Terima Kasih ^_^</div>
			    <!-- <p>1. Perlangkapan Standar yaitu: PRIBADI DAN REGU</p>
			    <p>2.</p>
			    <p>3.</p>
			    <p>4.</p>
			    <p>5.</p>
			    <p>6.</p>
			    <p>7.</p>
			    <p>8.</p> -->
				</div>
		   </div>
		</div>
		<div class="col-md-6">
		   <div class="card">
		      <div class="card-body">
		         <div class="row justify-content-center">
		            <div class="col-3">
		               <img src="${pageContext.request.contextPath}/images/logo-tnghs.png" class="img-fluid">
		            </div>
		         </div>
		         <h4 class="card-title text-center p-3">Balai TN Gunung Halimun Salak</h4>
		         <div class="row ">
		            <div class="col-md-12">
		               <form id="form-register">
		                <input type="hidden" class="form-control" name="application_id" value="008">
		                  <div class="form-row">
		                     <div class="form-group col-md-6">
		                        <label>Nama Depan</label>
		                        <input type="text" placeholder="Ketik Nama Depan" class="form-control" name="first_name" autocomplete="off">
		                     </div>
		                     <div class="form-group col-md-6">
		                        <label>Nama Belakang</label>
		                        <input type="text" placeholder="Ketik Nama Belakang" class="form-control" name="last_name" autocomplete="off">
		                     </div>
		                  </div>
		                  <div class="form-row">
		                     <div class="form-group col-md-12">
		                        <label>Alamat Email</label>
		                        <input type="email" placeholder="Ketik Email" class="form-control" name="email" autocomplete="off" onkeyup="$('[name=username]').val(this.value.split('@')[0])">
		                     </div>
		                  </div>
		                  <div class="form-row">
		                     <div class="form-group col-md-12">
		                        <label>Nomor HP</label>
		                        <input type="number" placeholder="Ketik Nomor HP" class="form-control" name="mobile" autocomplete="off">
		                     </div>
		                  </div>
		                  <c:choose>
		                  	<c:when test="${fn:containsIgnoreCase(roleList, 'ROLE_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_SUPERVISOR')}">
			                  <div class="form-row">
			                     <div class="form-group col">
			                        <label>Akses Grup</label>
			                        <select name="group_id" class="form-control">
			                        <% for(AccountGroup ac : accountGroup) {
			                        	out.print("<option value='"+ac.getId()+"'>"+ac.getName()+"</option>");
			                        } %>
			                        </select>
			                     </div>
			                  </div>
		                  	</c:when>
		                  	<c:otherwise>
			                     <input type="text" name="group_id" class="form-control d-none" value="2002200">
		                  	</c:otherwise>
		                  </c:choose>
		                  
		                  <div class="form-row">
		                     <div class="form-group col-md-12">
		                        <label>Kata Sandi</label>
		                        <input type="password" placeholder="Ketik Kata Sandi" class="form-control" name="password" autocomplete="off">
		                     </div>
		                  </div>
		                  
		                  <div class="form-row">
		                     <div class="form-group col-md-12">
		                        <label>Ulangi Kata Sandi</label>
		                        <input type="password" placeholder="Ketik Ulang Kata Sandi" class="form-control" name="password2" autocomplete="off">
		                     </div>
		                  </div>
		                  
		                  <div class="form-row">
		                     <div class="form-group col">
		                        <label>Username</label>
		                        <input type="text" placeholder="Username" class="form-control" name="username" autocomplete="off">
		                     </div>
		                     <!-- <div class="form-group col">
		                        <label></label>
		                        <div class="form-check">
		                           <input class="form-check-input" type="checkbox" value="true" name="enabled">
		                           <label class="form-check-label">
		                           Reset Password
		                           </label>
		                        </div>
		                     </div> -->
		                  </div>
		                  <!-- <div class="form-group form-check">
		                     <input type="checkbox" class="form-check-input" id="exampleCheck1">
		                     <label class="form-check-label" for="exampleCheck1">Check me out</label>
		                  </div> -->
		                  <div class="border border-bottom mb-3"></div>
		                  <div class="row justify-content-center text-center">
		                     <div class="col">
		                        <button type="submit" class="btn btn-primary btn-block">DAFTAR</button>
		                     </div>
		                  </div>
		               </form>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		</div>
		</div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    <%@ include file = "inc/footer.jsp" %>
    <%-- <%@ include file = "inc/footer-general.jsp" %> --%>
    
    <script src="${pageContext.request.contextPath}/js/daftar.js"></script>

  </body>

</html>
