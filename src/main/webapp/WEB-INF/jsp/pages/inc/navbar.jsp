<%@page import="org.springframework.web.util.UrlPathHelper"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<nav class="navbar navbar-expand-lg static-top navbar-light bg-default">
	  <div class="container">
      
	  <a class="navbar-brand text-white" href="${pageContext.request.contextPath}/page/general">
	    <img src="${contextPathPublic}/images/logo-tnghs.png" width="35" height="35" class="d-inline-block align-top rounded-circle" alt="">
	    Kawasan
	  </a>
		  
	  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ">
      
      	<c:if test="${requestScope.realName==null}">
	       	<li class="nav-item ">
	        	<a class="nav-link text-primary btn btn-outline-light" href="${pageContext.request.contextPath}/page/admin/">Login <span class="sr-only">(current)</span></a>
	      	</li>
      	</c:if>
      	<c:if test="${requestScope.realName!=null}">
        <li class="nav-item dropdown arrow-down "> <!-- d-block d-md-nones -->
          <a class="nav-link dropdown-toggle text-white" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <img src="${requestScope.avatar}" width="25px" /> ${requestScope.realName}  
          </a>
          <!-- d-block d-md-none -->
          <div class="dropdown-menu dropdown-menu-right " aria-labelledby="userDropdown">
          	<c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_SUPERVISOR')}">
          		<a class="dropdown-item" href="${pageContext.request.contextPath}/page/admin/">Manajemen Data</a>
            </c:if>
            <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_USER')}">
          		<a class="dropdown-item" href="${pageContext.request.contextPath}/page/manage-user?menu=booking">My Booking</a>
            </c:if>
            <a class="dropdown-item" href="${pageContext.request.contextPath}/login-password" target="_blank">Change Password</a>
            <a class="dropdown-item" href="${pageContext.request.contextPath}/login-logout">Logout</a>
          </div>
        </li>
        </c:if>
      </ul>
       	<form class="form-inline">
       	<%
       		String myparam = "";
       		if(request.getParameter("id")!=null) { 
       			myparam="/id/"+request.getParameter("id"); 
       		}
       	%>
      	<%
	    // Get the request URI
	    String requestUri = request.getRequestURI();
	
	    // Extract the last part of the URI (e.g., 'daftar')
	    String[] uriParts = requestUri.split("/");
	    String lastPart = uriParts[uriParts.length - 1];
	    request.setAttribute("lastPart", lastPart);
		%>
		<c:if test="${requestScope.realName==null && lastPart ne 'daftar.jsp'}">
		<%-- <% if(!lastPart.equals("daftar.jsp")){ %> --%>
      		<a class="nav-link text-white" href="${pageContext.request.contextPath}/page/daftar?url=<%= new UrlPathHelper().getOriginatingRequestUri(request) %>">Register <span class="sr-only">(current)</span></a>
     	<%-- <% } %> --%>
     	</c:if>
	     <a class="nav-link text-white" href="${pageContext.request.contextPath}/page/data-pendaki">Data Pendaki</a>
	     <a class="nav-link text-white" onclick="$('#sop-modal').modal('show')" href="javascript:void(0)">SOP</a>
	     
	  	</form>
	  	</div>
	  	</div>
    </nav>