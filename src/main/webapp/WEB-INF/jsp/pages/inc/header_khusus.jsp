  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tiket Kawasan - Kementerian Lingkungan Hidup dan Kehutanan</title>

    <link rel="icon" type="image/png" sizes="32x32" href="${contextPathPublic}/images/logo-tnghs.png">

    <!-- Bootstrap core CSS-->
    <%-- <link href="${contextPathPublic}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --%>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 -->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- Custom fonts for this template-->
    <link href="${contextPathPublic}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="${contextPathPublic}/vendor/select2.min.css" rel="stylesheet">
	<link href="${contextPathPublic}/vendor/daterangepicker.css" rel="stylesheet"> 
	<link href="${contextPathPublic}/vendor/ekko-lightbox.css" rel="stylesheet"> 
	
	<link rel="stylesheet" href="${contextPathPublic}/vendor/remodal/remodal.css">
	<link rel="stylesheet" href="${contextPathPublic}/vendor/remodal/remodal-default-theme.css">
	
	
	<!-- <link rel="stylesheet" href="@sweetalert2/theme-bootstrap-4/bootstrap-4.css"> -->
	<link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
	
	<!-- Custom styles for this template-->
    <link href="${contextPathPublic}/css/sb-admin.css" rel="stylesheet">
	
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<script>var localeCookieName = "${localeCookieName}"</script>
	<script>var whatsapp = "${whatsapp}"</script>
	<script>var weekendPrice = "${weekendPrice}"</script>
	<script>var dataAgenda = '${dataAgenda}'</script>
	<script>var pageNotComplete = '${pageNotComplete}'</script>
	<script>var bookingNotAllowed = '${bookingNotAllowed}'</script>
	<script>var nameJSP = '${nameJSP}'</script>
	<script>var roleList = '${roleList}'</script>
	<style>
		.bg-default {
		 background-color: #3c465f;
		}
		.bg-head {
			background-color: rgba(170, 192, 171, 0.5);
			color: white;
		}
		.bg-gradient {
			background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);
			height: 100%;
			color: white;
		}
		thead.bg-head {
			background-color: #0f8094!important;
		    color: white!important;
	    }
		.pill {
			border-radius: 26px;
		}
		.btn-light {
		    border-color: transparent!important;
		}
		.form-control {
			height: 45px;
			/* border: none;
			background: #f7f7f7; */
			/* width: 100%;
			padding: 1rem;
			border-radius: 23px;
			color: #333; */
		}
		.form-control-lg{
			height:calc(1.5em + 1rem + 2px)!important;
		}
		.bg-orange {
			background: var(--orange);
			color: white;
		}
		.text-orange {
			color: var(--orange);
		}
		.card-img-top {
			    min-height: 150px;
			height: 15vw;
		   	object-fit: cover;
		}
		
		/*custome table*/
		.rows-head {
			opacity: 0.8;
		    overflow: hidden;
		    background-color: #0f8094!important;
		    color: white!important;
		    padding: .75rem;
		}
		.rows-body {
			opacity: 0.8;
		    overflow: hidden;
		    background-color: #f8f9fa!important;
		    padding: .75rem;
		}
		.card-header-2 {
	    	padding: 0 1rem;
	    }
	    
	    .col-cos {
   	        padding-left: 15px;
   	        margin: 10px;
	    }
	</style>
	
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 425px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        background:#e6eaed!important;
      }
      .controls {
        background-color: #fff;
        border-radius: 2px;
        border: 1px solid transparent;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        height: 29px;
        margin-left: 17px;
        margin-top: 10px;
        outline: none;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      .controls:focus {
        border-color: #4d90fe;
      }
      .title {
        font-weight: bold;
      }
      #infowindow-content {
        display: none;
      }
      #map #infowindow-content {
        display: inline;
      }
	.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    	border-radius: 0;
	    background: none;
	    border: 2px solid #007bff;
	    color:var(--blue);
	}
	.nav-pills .nav-link {
    	/* border-radius: 0;
	    background: none;
	    border-bottom: 2px solid #007bff; */
	    color:var(--info);
	}
	
	.card-deck .card.active {
    	border-radius: 0;
	    border: 1px solid #007bff;
	}
	

    </style>
    <!-- own bootstrap -->
    <style>
	    .pd-y-15 {
		    padding-top: 15px;
		    padding-bottom: 15px;
		}
		
		.pd-x-30 {
		    padding-left: 30px;
		    padding-right: 30px;
		}
		.bd-2 {
		    border-width: 2px;
		}
		.btn-oblong {
		    border-radius: 50px;
		}
		.tx-12 {
		    font-size: 12px;
		}
		.tx-spacing-6 {
		    letter-spacing: 3px;
		}
		
		.btn-falcon-default {
		    color: #4d5969;
		    background-color: #fff;
		    border-color: #fff;
		    -webkit-box-shadow: 0 0 0 1px rgba(43,45,80,.1), 0 2px 5px 0 rgba(43,45,80,.08), 0 1px 1.5px 0 rgba(0,0,0,.07), 0 1px 2px 0 rgba(0,0,0,.08);
		    box-shadow: 0 0 0 1px rgba(43,45,80,.1), 0 2px 5px 0 rgba(43,45,80,.08), 0 1px 1.5px 0 rgba(0,0,0,.07), 0 1px 2px 0 rgba(0,0,0,.08);
		}
		
		.svg-inline--fa {
		    display: inline-block;
		    font-size: inherit;
		    height: 1em;
		    overflow: visible;
		    vertical-align: -.125em;
		}
		
		.card-pricing.border-primary {
		    z-index: 1;
		    border: 3px solid #007bff;
		}
		.card-pricing .list-unstyled li {
		    padding: .5rem 0;
		    color: #6c757d;
		}
		
		.avatar-xl {
		    height: 2rem;
		    width: 2rem;
	    }
		
		.avatar {
		   	position: relative;
		    display: inline-block;
	    }
		
		/* // Small devices (landscape phones, 576px and up) */
		@media (min-width: 320px) {
			.height-costume{
				height:50vh;
			}
		}
		
		@media (min-width: 576px){
			.container-fluid-sm{
				width: 100%!important;
			}
		}
		
		/* // Medium devices (tablets, 768px and up) */
		@media (min-width: 768px) {
			.height-costume{
				height:532px;
			}
			
			.container-fluid-md{
				width: 70%!important;
			}
			.d-md-inline-grid{display:inline-grid!important}
			.btn-md-light {
				border-color: transparent!important;
			}
			.bg-md-head {
				background-color: rgba(170, 192, 171, 0.5);
	    		color: white!important;
			}
		}
		
		/* // Large devices (desktops, 992px and up) */
		@media (min-width: 992px) { 
			.height-costume{
				height:532px;
			}
		}
		
		/* // Extra large devices (large desktops, 1200px and up) */
		@media (min-width: 1200px) {
			.height-costume{
				height:532px;
			}
			
			.hv-md-80{
				height: 83vh;
			}
			.hv-md-200{
				height: 284vh;
			}
		}
		
		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
		  -webkit-appearance: none;
		  margin: 0;
		}
		
		/* Firefox */
		input[type=number] {
		  -moz-appearance: textfield;
		}
		
		.popover {
			width:100%;
		}
    </style>
    
  </head>
