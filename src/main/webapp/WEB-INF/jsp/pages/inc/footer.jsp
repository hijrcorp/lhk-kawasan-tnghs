<!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Confirmation Modal-->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-confirm-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
            <button class="btn btn-primary" id="btn-modal-confirm-yes" type="button">Yes</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-success">
            <h5 class="modal-title" id="exampleModalLabel">Alert</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-alert-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Error Modal-->
    <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-error-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="sop-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="staticBackdropLabel">SOP PEMESANAN TIKET KAWASAN PENDAKIAN</h5>
	        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button> -->
	      </div>
	      <div class="modal-body lead">
	      ${disclaimerSop}
	       	<!-- <p>1. Kunjungi Portal kawasan booking online : <a href="http://localhost:9910/kawasan/page/general">http://localhost:9910/kawasan/page/general</a>.</p>
			<p>2. Pilih Gate Jadwal naik dan tentukan jumlah pendaki.</p>
			<p>3. Isi Form dan Lengkapi biodata, pastikan email dan no.hp valid, karena Konfirmasi Pembayaran akan dikirim ke email ketua kelompok.</p> -->
		  </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	        <!-- <button id="btn-mengerti" type="button" class="btn btn-success">OK</button> -->
	      </div>
	    </div>
	  </div>
	</div>
    
    <div class="modal fade" id="disclaimer-peraturan-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="staticBackdropLabel">Disclaimer Peraturan</h5>
	        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button> -->
	      </div>
	      <div class="modal-body lead">
	       
		
			<!-- <p>1. Saya akan mematuhi semua peraturan yang diatur oleh TNHGS.</p>
			<p>2. Jika Jumlah minimal pendaki tidak hadir ketika pendakian, maka kami bersedia untuk membatalkan pendakian.</p>
			<p>3. Apabila ada perubahan jumlah dan nama personil, maka saya akan melakukan pendaftaran baru sesuai syarat dan ketentuan yang berlaku.</p>
			<p>4. Pemesanan tiket anda hanya akan diapprovel di hari jam kerja.</p> -->
			
		  </div>
	      <div class="modal-footer">
	        <button id="btn-tidak-mengerti" type="button" class="btn btn-danger" data-dismiss="modal">SAYA TIDAK MENGERTI INI</button>
	        <button id="btn-mengerti" type="button" class="btn btn-success">SAYA MENGERTI</button>
	      </div>
	    </div>
	  </div>
	</div>

    <!-- Bootstrap core JavaScript-->
    <c:choose>
	    <c:when test = "${jquery != true}">
    		<script src="${contextPathPublic}/vendor/jquery/jquery.min.js"></script>
	    </c:when>
	</c:choose>
    <script src="${contextPathPublic}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="${contextPathPublic}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="${contextPathPublic}/vendor/jquery.disableAutoFill.min.js"></script>
    <script src="${contextPathPublic}/vendor/js.cookie.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="${contextPathPublic}/vendor/chart.js/Chart.min.js"></script>
    <%-- <script src="${contextPathPublic}/vendor/moment.min.js"></script> --%>
   	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    
    <script src="${contextPathPublic}/vendor/daterangepicker.js"></script>
	<script src="${contextPathPublic}/vendor/select2.min.js"></script>
	<script src="${contextPathPublic}/vendor/ekko-lightbox.min.js"></script>
	
	<c:choose>
	    <c:when test = "${jquery != true}">
			<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
	    </c:when>
	<%-- <script src="${contextPathPublic}/vendor/loadingoverlay.min.js"></script> --%>
	</c:choose>
	
	<script src="${contextPathPublic}/vendor/remodal/remodal.js"></script>
	<script src="${contextPathPublic}/vendor/bootstrap-input-spinner-master/bootstrap-input-spinner.js"></script>
	
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		
	
	<!-- Custom scripts for all pages-->
    <script src="${pageContext.request.contextPath}/js/sb-admin.min.js"></script>
    <script src="${contextPathPublic}/vendor/jquery/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/common.js"></script>