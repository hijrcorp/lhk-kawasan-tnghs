    <div class="container-fluid bg-head p-3">
		<div class="media">
		  <img style="width:100px" src="${contextPathPublic}/images/logo-menlhk.png" class="align-self-start mr-3" alt="...">
		  <div class="media-body">
		    <h3 class="mt-0">Aplikasi Kawasan</h3>
		    <p>Aplikasi Kawasan adalah aplikasi pemesanan tiket online <br/>dengan  dengan fokus masuk kawasan wisata.</p>
	      </div>
	      <div class="pr-5 pt-3 pb-3">
	      <a href="${pageContext.request.contextPath}/page/dashboard" class="mr-4" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-home fa-4x"></i> Home</a>
	      <a href="${pageContext.request.contextPath}/page/" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-globe fa-4x"></i> Website</a>
	      </div>
		</div>
	</div>