<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Sidebar -->
      <ul class="sidebar navbar-nav">
      	<c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN')}">
        <li class="nav-header text-muted">
            <span>DATA PENDUKUNG</span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/satuan">
          <i class="fas fa-fw fa-weight"></i>
            <span>Satuan Ukuran</span>
           </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/disposal">
            <i class="fas fa-fw fa-sign-in-alt"></i>
            <span>Jenis Disposal</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/pekerjaan">
            <i class="fas fa-fw fa-cog"></i>
            <span>Daftar Pekerjaan</span></a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/taksa">
            <i class="fas fa-fw fa-tags"></i>
            <span>Taksa Satwa</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/sumber">
            <i class="fas fa-fw fa-info"></i>
            <span>Sumber Satwa</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/satwa">
            <i class="fas fa-fw fa-bug"></i>
            <span>Jenis Satwa</span></a>
        </li>
        <li class="nav-header text-muted">
            <span>DATA UTAMA</span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/pengguna">
            <i class="fas fa-fw fa-user"></i>
            <span>Akun Pengguna</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/kandang">
          <i class="fas fa-fw fa-cube"></i>
            <span>Master Kandang</span>
           </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/lokasi">
            <i class="fas fa-fw fa-map-marker-alt"></i>
            <span>Lokasi Disposal</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/inventori?menu=Inventaris Satwa">
            <i class="fas fa-fw fa-paw"></i>
            <span>Inventaris Satwa</span></a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
        <li class="nav-header text-muted">
            <span>CATATAN LOG</span>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/keeper">
            <i class="fas fa-fw fa-book"></i>
            <span>Log Keeper</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/dokter">
          <i class="fas fa-fw fa-medkit"></i>
            <span>Log Dokter</span>
           </a>
        </li>
        </c:if>
        <li class="nav-header text-muted">
            <span>MENU PELAYANAN</span>
        </li>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') && !fn:containsIgnoreCase(roleList, 'ROLE_PPS_ADMIN')}">
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/inventori?menu=Inventaris Satwa">
            <i class="fas fa-fw fa-paw"></i>
            <span>Inventaris Satwa</span></a>
        </li>
        </c:if>
        
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN')}">
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/penerimaan?menu=Daftar Penerimaan">
          <i class="fas fa-fw fa-list-alt"></i>
            <span>Daftar Penerimaan</span>
           </a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/pengeluaran?menu=Daftar Pengeluaran&type=PERMOHONAN">
            <i class="fas fa-fw fa-list"></i>
            <span>Daftar Pengeluaran</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/form-pengeluaran?menu=Form Pengeluaran&type=PERMOHONAN">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Tambah Pengeluaran</span></a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/form-penerimaan?menu=Form Penerimaan">
          <i class="fas fa-fw fa-sign-in-alt"></i>
            <span>Tambah Penerimaan</span>
           </a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU')}">
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/pengeluaran?proses=VERIFIKASI&menu=Verifikasi Pengeluaran&type=PERMOHONAN">
            <i class="fas fa-fw fa-calendar-check"></i>
            <span>Verifikasi Pengeluaran</span></a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/pengeluaran?proses=PERSETUJUAN&menu=Persetujuan Pengeluaran&type=PERMOHONAN">
            <i class="fas fa-fw fa-thumbs-up"></i>
            <span>Persetujuan Pengeluaran</span></a>
        </li>
        </c:if>
        <li class="nav-header text-muted">
            <span>LAPORAN</span>
        </li>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_OPERATOR')}">
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/keeper?laporan&menu=Catatan Keeper">
          <i class="fas fa-fw fa-file"></i>
            <span>Catatan Keeper</span>
           </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/dokter?laporan&menu=Catatan Dokter">
            <i class="fas fa-fw fa-file"></i>
            <span>Catatan Dokter</span></a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/penerimaan?laporan&menu=Penerimaan Satwa">
          <i class="fas fa-fw fa-file"></i>
            <span>Penerimaan Satwa</span>
           </a>
        </li>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(roleList, 'ROLE_PPS_PELAYANAN') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_TU') || fn:containsIgnoreCase(roleList, 'ROLE_PPS_KEPALA')}">
         <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/page/pengeluaran?laporan&menu=Pengeluaran Satwa&type=PENYERAHAN">
            <i class="fas fa-fw fa-file"></i>
            <span>Pengeluaran Satwa</span></a>
        </li>
        </c:if>
      </ul>
      
    