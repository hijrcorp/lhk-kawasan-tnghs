<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">

    <%-- <%@ include file = "inc/navbar.jsp" %> --%>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">

		
		<header class="image-bg-fluid-height hm-stylish-strong w-100" style="background: url('https://tvlk.imgix.net/imageResource/2018/11/27/1543315914093-c3d1c5417fde9597c6d391674f5feeae.png?auto=compress%2Cformat&cs=srgb&fm=png&ixlib=java-1.1.12&q=75')no-repeat center;background-size: cover;height:360px">
			<!-- <div class="container-fluid">
				<div class=" col-md-offset-1 col-md-6 text-md-left text-center-sm m__">
					<h3 class="text-md-lef white-text">Inspektorat Jenderal Kementerian LHK</h3>
					<div class="blok-white"></div>
					<div class="clear1"></div>
					<p class="white-text lh">Inspektorat Jenderal mempunyai tugas melaksanakan pengawasan intern di lingkungan Kementerian Lingkungan Hutan dan Kehutanan
				</p></div>
				<div class="col-md-2 col-md-offset-2 text-right m_">
					<center>
						<img class="rounded-circle" src="https://www.bbksdajabar.com/satsdn/images/logo-menlhk.png" height="145" width="145" style="border:3px solid white;border-radius:100%;">
					</center>
				</div>
		
			</div> -->
		</header>
		
          	
		<%-- <div class="media bg-head pill pl-5 pt-3 pb-3">
		  <img style="width:100px" src="https://www.bbksdajabar.com/satsdn/images/logo-menlhk.png" class="align-self-start mr-3" alt="...">
		  <div class="media-body">
		    <h3 class="mt-0">Aplikasi Kawasan</h3>
		    <p>Aplikasi Kawasan adalah aplikasi pemesanan tiket online <br/>dengan  dengan fokus masuk kawasan wisata.</p>
	      </div>
	      <div class="pr-5 pt-3 pb-3">
	      <a href="${pageContext.request.contextPath}/page/dashboard" class="mr-4" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-home fa-4x"></i> Home</a>
	      <a href="#" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-globe fa-4x"></i> Website</a>
	      </div>
		</div> --%>
		
		<div class="row ">
			<div class="col-md-12 bg-white">
				<!-- <div class="form-inline">
				  <div class="form-group">
				    <div class="input-group input-group-lg">
				      <div class="input-group-addon">Nomor LHP</div>
				      <input id="txtsearch" size="50" type="text" class="form-control input-sm" aria-label="..." placeholder="Ketik Nomor LHP">
						      <span class="input-group-btn">
						        <button id="btnSearch" onclick="search()" type="button" class="btn btn-default btn-sm">
									<span class="glyphicon glyphicon-search"></span> Cari
								</button>
							
						      </span>
				    </div>
				  </div>
				   <button onclick="addLhp()" id="btnPencarianLhp" type="button" class="btn btn-success btn-lg">
							Pencarian Lanjutan
					</button>
				</div> -->
				
			 <div class="container-fluid">
			 <div class="row  justify-content-center">
			   <!--  <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label> -->
			    <div class="col-sm-8">
					<div class="input-group mb-3 mt-3">
					  <input type="text" class="form-control" placeholder="Enter destination name or activity" aria-label="Recipient's username" aria-describedby="button-addon2">
					  <div class="input-group-append">
					    <button style="border-top-right-radius: 23px!important;border-bottom-right-radius: 23px!important;" class="btn btn-outline-light text-muted" type="button" id="button-addon2">Search!</button>
					  </div>
					</div>
			    </div>
			  </div>
			  </div>
			</div>
		</div>
		
        <div class="container-fluid" style="width:70%">

          <!-- Breadcrumbs-->
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item active">Selamat Datang di Aplikasi PPS Online</li>
          </ol> -->
		
		
		<div class="container-fluid pl-5s pt-5 pb-3 pr-5s d-none">
		<div class="card bg-head pill">
	    <div class="card-body">
	    <h3 class="card-title">
		<div class="row">
			<div class="col-md-6">
				<span>Region</span>
			</div>
			<div class="col-md-6">
				<button id=btn-add data-toggle="modal" data-target="#modal-form" class="btn btn-light pill btn-lg pl-5 pr-5 float-right">Add <span class="badge badge-pill badge-success">New</span></button>
			</div>
		</div>
		</h3>
		<div class="border-bottom mb-3"></div>
		<table id="tbl-data" class="table table-striped" style="opacity: 0.8;border-radius: 23px;overflow: hidden;">
			<thead class="bg-head text-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Region Name</th>
					<th scope="col">Region Type</th>
					<!-- <th scope="col">Photo</th> -->
					<th scope="col">Region Services</th>
					<th scope="col">Desc</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody class="bg-light text-dark">
			</tbody>
		</table>
	    </div>
		</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-4">
				<div class="card-deck">
				  <div class="card">
				    <img src="https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnG98M/3uI16G3tLN17gcLc6PsaHRlPhwKyEDXJ3hZPmbanBtM8D6JzU42qyM4VaPjiMqUlXTdzUr2jNOPsddnkC49390o5IOgU1/OoIc4cRxiqiStapzN6HYw6bU84dDsQ==" class="card-img-top" alt="...">
				    <div class="card-body">
				      <div class="card-title">Lombok Elephant Park</div>
				      <small class="text-muted"><i class="fas fa-map-marker-alt"></i> Mataram City Center</small>
				    </div>
				    <div class="card-footer bg-white">
				      <div class="row">
					      <div class="text-success font-weight-bold col mt-2">RP. 50.000</div>
					      <div class="col"><button class="btn btn-primary  pull-right float-right">Find Ticket</button></div>
				      </div>
				    </div>
				  </div>
				  <div class="card">
				    <img src="https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnK7BSkr0GEBPtBkJwbVRAXcyq6Ndl8vsX80mNeI22b2xchSDlAoDMxHn7JlbRnnXTtyWX1XfFy5+v1lnGIJxMSFcUqdyRyD9UJOvlXmodZQnBJnKCIcsxFFsAZ7cOFwG+w==" class="card-img-top" alt="...">
				    <div class="card-body">
				      <div class="card-title">Nirwana Waterpark</div>
				      <small class="text-muted"><i class="fas fa-map-marker-alt"></i> Mataram City Center</small>
				    </div>
				    <div class="card-footer bg-white">
				      <div class="row">
					      <div class="text-success font-weight-bold col mt-2">RP. 25.000</div>
					      <div class="col"><button class="btn btn-primary  pull-right float-right">Find Ticket</button></div>
				      </div>
				    </div>
				  </div>
				  <div class="card">
				    <img src="https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnDPWT9XqmSRCKB6GZsrMNdfpWXt7tNNZMxgYDIQg4Nh1hQ4iGSuorJGUmgPlqhqezPzg3fX3jgdEmEV0ZGBE34rzycGUYftdZSKyLatH7uKcwOonl58Fy1rEYn7dTKtYkQ==" class="card-img-top" alt="...">
				    <div class="card-body">
				      <div class="card-title">Taman Air Kurakura Kids Funhouse</div>
				      <small class="text-muted"><i class="fas fa-map-marker-alt"></i> Mataram City Center</small>
				      <!-- <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p> -->
				    </div>
				    <div class="card-footer bg-white">
				      <div class="row">
					      <div class="text-success font-weight-bold col mt-2">RP. 20.000</div>
					      <div class="col"><button class="btn btn-primary  pull-right float-right">Find Ticket</button></div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
		
        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form">
			  <div class="form-group">
			    <label for="">Nama Kawsan</label>
			    <input type="text" class="form-control" name="name" placeholder="">
			  </div>
			  <div class="form-group">
			    <label for="">Jenis Kawasan</label>
			    <input type="text" class="form-control" name="type" placeholder="">
			  </div>
			  <!-- <div class="form-group">
			    <label for="">Biaya</label>
			    <input type="text" class="form-control" name="cost" placeholder="">
			  </div> -->
			  <div class="form-group">
			    <label for="">Servis Layanan Kawasan</label>
			    <input type="text" class="form-control" name="services" placeholder="">
			  </div>
			  <div class="form-group">
			    <label for="">Deksripsi Kendaraan(Opsional)</label>
			    <textarea class="form-control" name="description" rows="5"></textarea>
			  </div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="save();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/region.js"></script>

  </body>

</html>
