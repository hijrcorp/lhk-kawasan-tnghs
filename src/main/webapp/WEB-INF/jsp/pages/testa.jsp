<!-- <html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>ArcGIS Developer Guide: Display a map (2D)</title>
  <style>
    html,
    body,
    #viewDiv {
      padding: 0;
      margin: 0;
      height: 100%;
      width: 100%;
    }
  </style>

  <link rel="stylesheet" href="https://js.arcgis.com/4.19/esri/themes/light/main.css">
  <script src="https://js.arcgis.com/4.19/"></script>

  <script>
    require([
     "esri/config",
      "esri/Map",
      "esri/views/MapView"
    ], function (esriConfig,Map, MapView) {

      esriConfig.apiKey = "AAPK2caaa6ef6daf4149ba667de18a5abb2dNsYQcau1sHqzsn69jQ98CU-hb9oo2YFKH1vlxyT8rl-QY9k-LI8w_UPOzCWmALbP";
      const map = new Map({
        basemap: "arcgis-topographic" // Basemap layer
      });

      const view = new MapView({
        map: map,
        center: [-118.805, 34.027],
        zoom: 13, // scale: 72223.819286
        container: "viewDiv",
        constraints: {
          snapToZoom: false
        }
      });

    });
  </script>
</head>

<body>
  <div id="viewDiv"></div>
</body>

</html> -->

<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="initial-scale=1,maximum-scale=1,user-scalable=no"
    />
    <!-- ArcGIS Mapping APIs and Location Services Guide
  Learn more: https://developers.arcgis.com/documentation/mapping-apis-and-services/maps/
-->
<title>ArcGIS Developer Guide: Display a vector tile layer</title>

    <style>
      html,
      body,
      #viewDiv {
        padding: 0;
        margin: 0;
        height: 100%;
        width: 100%;
      }
    </style>

<link rel="stylesheet" href="https://js.arcgis.com/4.19/esri/themes/light/main.css">
<script src="https://js.arcgis.com/4.19/"></script>

<script>
  require([
    "esri/config",
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/FeatureLayer"

  ], (esriConfig, Map, MapView, FeatureLayer)=> {


    esriConfig.apiKey = "AAPK2caaa6ef6daf4149ba667de18a5abb2dNsYQcau1sHqzsn69jQ98CU-hb9oo2YFKH1vlxyT8rl-QY9k-LI8w_UPOzCWmALbP";

    const map = new Map({
      basemap: "arcgis-topographic"//,
      //layers: [layer]
    });

    const view = new MapView({
      container: "viewDiv",
      center: [108.3618,-6.923,108.4092,-6.8941],
      zoom: 18,
      map: map,
      constraints: {
        snapToZoom: false
      }
    });


    const layer_pos = new FeatureLayer({
      url: "https://services3.arcgis.com/EWi64kQxfGrFaTYd/arcgis/rest/services/join_apuy/FeatureServer/0"
    });
    const layer_line = new FeatureLayer({
        url: "https://services3.arcgis.com/EWi64kQxfGrFaTYd/arcgis/rest/services/join_apuy/FeatureServer/1"
      });

    map.add(layer_pos);
    map.add(layer_line);

    const popupTrailheads = {
      "title": "Trailhead",
      "content": "<b>Trail:</b> {TRL_NAME}<br><b>City:</b> {CITY_JUR}<br><b>Cross Street:</b> {X_STREET}<br><b>Parking:</b> {PARKING}<br><b>Elevation:</b> {ELEV_FT} ft"
    }
    const trailheads = new FeatureLayer({
       url: "https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Trailheads_Styled/FeatureServer/0",
       outFields: ["TRL_NAME","CITY_JUR","X_STREET","PARKING","ELEV_FT"],
       popupTemplate: popupTrailheads
     });

     map.add(trailheads);
  });
</script>
</head>

<body>
<div id="viewDiva"></div>
</body>
</html>