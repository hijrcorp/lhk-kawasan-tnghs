<%@page import="id.co.hijr.ticket.mapper.TourGuideMapper"%>
<%@page import="id.co.hijr.ticket.model.TourGuide"%>
<%@page import="java.util.HashSet"%>
<%@page import="id.co.hijr.ticket.model.TypeVehicle"%>
<%@page import="id.co.hijr.ticket.mapper.TypeVehicleMapper"%>
<%@page import="id.co.hijr.ticket.model.Vehicle"%>
<%@page import="id.co.hijr.ticket.mapper.VehicleMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
VehicleMapper vehicleMapper = appCtx.getBean(VehicleMapper.class);
TypeVehicleMapper typeVehicleMapper = appCtx.getBean(TypeVehicleMapper.class);
TourGuideMapper tourGuideMapper = appCtx.getBean(TourGuideMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
Purchase purchase = new Purchase();
System.out.println("step: "+ request.getParameter("step"));
if(request.getParameter("step") != null){
	purchase = purchaseMapper.getEntity(id); 
	id=purchase.getRegionId();
}
Region region = regionMapper.getEntity(id);
QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
List<File> list_photo = fileMapper.getList(param);
//region.setList_photo(list_photo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.getId()+"'");
List<RegionTodo> list_todo = regionTodoMapper.getList(param);
region.setList_todo(list_todo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");
List<RegionFacility> list_facility = regionFacilityMapper.getList(param);
if(request.getParameter("step") == null) region.setList_facility(list_facility);
//
param = new QueryParameter();
//param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");/*  */
List<Vehicle> list_vehicle = vehicleMapper.getList(param);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">
	
    <%-- <%@ include file = "inc/navbar.jsp" %> --%>
	<!-- costume -->
	
	<c:if test="${param.step != null && param.step == 1}">		
		<!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid p-3" style="width:70%">
		  <a class="navbar-brand" href="#">
		    <img src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
		    Kawasan
		  </a>
		  
		  <div class="collapse navbar-collapse" id="navbarText">
			    <div class="navbar-text ml-auto">
			      <span><span class="badge badge-pill badge-primary">1</span> Book</span>
			      <span>-</span>
			      <span><span class="badge badge-pill badge-primary">2</span> Pay</span>
			      <span>-</span>
			      <span><span class="badge badge-pill badge-primary">3</span> Process</span>
			      <span>-</span>
			      <span><span class="badge badge-pill badge-primary">4</span> E-ticket</span>
			    </div>
		  </div>
		</div>
		</nav> -->
		<nav class="navbar navbar-expand-lg navbar-light bg-white">
		<div class="container p-3">
		
		  <a class="navbar-brand" href="${pageContext.request.contextPath}/page/general">
		    <img src="${pageContext.request.contextPath}/images/logo-kawasan-icon.svg" width="35" height="35" class="d-inline-block align-top" alt="">
		    Kawasan
		  </a>
		  
		<ul class=" nav nav-pills ml-auto">
		  <li class="nav-item">
		    <a id="btn-book-1" class="nav-link active" data-toggle="pill" href="#pills-book-1">Information</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-2" class="nav-link" data-toggle="pill" href="#pills-book-2">Vehicle</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-3" class="nav-link" data-toggle="pill" href="#pills-book-3">Tour Guide</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-4" class="nav-link" data-toggle="pill" href="#pills-book-4">Confirmation</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-4" class="nav-link" data-toggle="pill" href="#pills-book-4">Pay</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">E-ticket</a>
		  </li>
		</ul>
		</div>
		</nav>
	</c:if>
	<!--  -->
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">
		
		<c:if test="${param.step == null}">
		
		<!-- <header class="image-bg-fluid-height hm-stylish-strong w-100" style="background: url('https://tvlk.imgix.net/imageResource/2018/11/27/1543315914093-c3d1c5417fde9597c6d391674f5feeae.png?auto=compress%2Cformat&cs=srgb&fm=png&ixlib=java-1.1.12&q=75')no-repeat center;background-size: cover;height:360px">
			
		</header> -->
		
		 <div class="container mb-3">
		 <div class="row justify-content-center">
		    <div class="col-md-12">
				<button onclick="$('#container-search').toggle()" class="btn btn-primary btn-sm px-4 px-sm-5 float-right" type="button">Change Search</button>
			</div>
		  </div>
		  </div>
		
		 <div id="container-search" class="container">
		 <div class="row  justify-content-center">
		   <!--  <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label> -->
		    <div class="col-md-12">
		    
				<div class="container-fluid pb-3 pt-3	bg-white">
					<div class="input-group ">
					  <input type="text" class="form-control" placeholder="Enter destination name or activity" aria-label="Recipient's username" aria-describedby="button-addon2">
					  <div class="input-group-append">
					    <button style="border-top-right-radius: 23px!important;border-bottom-right-radius: 23px!important;" class="btn btn-outline-light text-muted" type="button" id="button-addon2">Search!</button>
					  </div>
					</div>
			    </div>
		    </div>
		  </div>
		  </div>
		
        <div class="container">
		
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="card mb-3">
				<!-- <div style="max-height: 450px;height: 450px;background-image: url('https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnG98M/3uI16G3tLN17gcLc6PsaHRlPhwKyEDXJ3hZPmbanBtM8D6JzU42qyM4VaPjiMqUlXTdzUr2jNOPsddnkC49390o5IOgU1/OoIc4cRxiqiStapzN6HYw6bU84dDsQ=='); background-size: cover;"> -->
				<!--Carousel Wrapper-->
				<div>
				<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
				  <!--Slides-->
				  <div class="carousel-inner" role="listbox">
				  <% 
				  int key=0;
					  for(File o2 : list_photo){
					  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
					  	o2.setName(o2.getName().replace(" ", "%"));
					  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
					  	if(key==0){
						  	out.println("<div class='carousel-item active'>");
						  		out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
					      	out.println("</div>");
					  	}else if(key>0){	
						  	out.println("<div class='carousel-item'>");
						  		out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
					      	out.println("</div>");
					  	}
				      	key++;
					  }
				  %>
				  </div>
				  <!--/.Slides-->
				  <!--Controls-->
				  <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				  <!--/.Controls-->
				  <ol class="carousel-indicators">
					  <% 
					  key=0;
					  for(File o2 : list_photo){
						o2.setName(o2.getName().replace(" ", "%"));
					  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
					  	if(key==0){
						  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
						  	out.println("<li style='width: 6.25rem!important;' data-target='#carousel-thumb' data-slide-to="+key+" class='active'>");
						  	out.println("<img class='d-block w-100' src="+path+" class='img-fluid'></li>");
					  	}else if(key>0){
					  		String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
						  	out.println("<li style='width: 6.25rem!important;' data-target='#carousel-thumb' data-slide-to="+key+">");
						  	out.println("<img class='d-block w-100' src="+path+" class='img-fluid'></li>");
					  	}
				      	key++;
					  } 
					  %>
				  </ol>
				</div>
				<!--/.Carousel Wrapper-->
				</div>
			<!-- </div> -->
            <div class="card-body">
              <div class="row justify-content-between align-items-center">
                <div class="col">
                  <div class="media">
                     <div class="media-body fs--1">
                      <h6 class="fs-0"><% out.println(region.getName()); %></h6>
                      <p class="mb-0"><i class="fas fa-map-marker-alt"></i> <% out.println(region.getLocationDesc()); %></p>
                     <!--  <p class="mb-0 text-justify">
                      	Minahasa Utara menyuguhkan berbagai tempat yang tidak kalah menarik untuk dikunjungi. Berikut ini 5(lima) tempat wisata di Minahasa Utara yang mungkin bisa menjadi alternatif wisatawan untuk berwisata
                      </p> -->
                    </div>
                  </div>
                </div>
                <div class="col-md-auto mt-4 mt-md-0">
                	<div class="">
                		<p><span class="fs-0 font-weight-light">Mulai Dari</span></p>
                		<h5 class="fs-0 text-orange font-weight-light">Rp. 50.000 - Rp. 90.000</h5>
                		<button id="btn-book" data-toggle="modal" data-target="#modal-form" class="btn btn-sm px-4 px-sm-5 btn-block bg-orange" type="button">Book Now</button>
                	</div>
                </div>
              </div>
            </div>
          </div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="card">
			  <div class="card-header">
			    <ul class="nav nav-tabs card-header-tabs">
			      <li class="nav-item">
			        <a class="nav-link active" data-toggle="tab" href="#nav-info">Overview</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link " data-toggle="tab" href="#nav-desc">What To Expect</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-toggle="tab" href="#nav-perhatian">Inclusions & Exclusions</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-toggle="tab" href="#nav-refund">Cancellation Policy</a>
			      </li>
			    </ul>
			  </div>
			  <div class="tab-content" id="nav-tabContent">
				  <div class="tab-pane fade" id="nav-desc" role="tabpanel" aria-labelledby="nav-profile-tab">
					  <div class="card-body">
				      	<div class="accordion" id="accordionExample">
						  <% 
						  key=0;
						  String row="";
						  for(RegionTodo o3 : list_todo){
							if(o3.getParent().isEmpty() || o3.getParent().equals("")) {
								row+="<div class='card'>";
								row+="<div class='bg-light p-1' id='heading"+key+"'>";
								row+="<h2 class='mb-0'>";
									row+="<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#collapse"+key+"' aria-expanded='true' aria-controls='collapseOne'>DAY "+o3.getDay()+" : "+o3.getActivity()+"</button>";
					          	row+="</h2>";
					          	row+="</div>";
							
					          	if(key==0){
					          		row+="<div id='collapse"+key+"' class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>";
					          	}else{
					          		row+="<div id='collapse"+key+"' class='collapse' aria-labelledby='headingOne' data-parent='#accordionExample'>";
						         }
					          	row+="<div class='card-body'>";
					          	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
								  for(RegionTodo o4 : list_todo){
									if(o3.getId().equals(o4.getParent())){
					          			row+="<p><i class='far fa-calendar-check'></i> "+dateFormat.format(o4.getTime())+" : "+o4.getActivity()+"</br></p>";
									}
								  }
					          		row+="</div>";
							    row+="</div>";
							    
							    row+="</div>";
							}
					      	key++;
						  } 
						  out.print(row);
						  %>
						  
						</div>
				      	
					  </div>
				  </div>
				  <div class="tab-pane fade" id="nav-refund" role="tabpanel" aria-labelledby="nav-profile-tab">
					  <div class="card-body">
					    <!-- <h5 class="card-title">Special title treatment 2</h5> -->
					    <p>For a full refund, cancel at least 24 hours in advance of the start date of the experience.</p>
				    	<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
					  </div>
				  </div>
				  <div class="tab-pane fade show active" id="nav-info" role="tabpanel">
					<div class="card-body">
					    <p class="card-text"><% out.print(region.getDescription()); %></p>
					</div>
				  </div>
				  <div class="tab-pane fade" id="nav-perhatian" role="tabpanel">
					<div class="card-body">
				    	<div class="row">
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold"><i class="fas fa-check-circle fa-1x text-success mr-3"></i> Inclusions </h5><!-- Harga Termasuk -->
				      			<ul>
								  <% 
								  	key=0;
									for(RegionFacility o3 : list_facility){
										if(o3.isInclude().equals("Y")){
									  		out.println("<li>"+o3.getItem()+"</li> ");
										}
										key++;
									}
								  %>
							    </ul>
					    	</div>
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold"><i class="fas fa-times-circle text-danger mr-3"></i> Exclusions</h5> <!-- Harga Tidak Termasuk -->
				      			<ul>
								  <% 
								  	key=0;
									for(RegionFacility o3 : list_facility){
										if(o3.isInclude().equals("N")){
									  		out.println("<li>"+o3.getItem()+"</li> ");
										}
										key++;
									}
								  %>
							    </ul>
					    	</div>
				    	</div>
				    </div>
				  </div>
				</div>
			</div>
			</div>
		</div>
		
		<div class="row mt-3 d-none">
			<div class="col-md-12">
				<div class="card-deck">
				  <div class="card">
				    <div class="card-body text-justify">
				      	<h5 class="card-title font-weight-bold">Deskripsi</h5>
				    	<p class="card-text">Minahasa Utara menyuguhkan berbagai tempat yang tidak kalah menarik untuk dikunjungi. Berikut ini 5(lima) tempat wisata di Minahasa Utara yang mungkin bisa menjadi alternatif wisatawan untuk berwisata, yaitu:</p>
						<ul>
							<li>Pantai Pal, memiliki pasir putih dan ombak yang tidak terlalu besar ini merupakan pantai yang cukup ramai dikunjungi masyarakat Minahasa Utara.</li>
							<li>Pantai Pulisan, memiliki luas kurang lebih 500 meter dengan hamparan pasir putih dan air lautanya yang jernih, dipinggir pantai terdapat karang yang membuatnya semakin menarik untuk dipandang.</li>
							<li>Pulau Lihaga, merupakan sebuah pulau yang terletak diujung pulai sulawesi, pulau ini memiliki luas 8 Ha dan berada di wilayah administrasi Kecamatan Likupang Barat.</li>
							<li>Pulau Gangga, memiliki pantai berpasir putih, pulau gangga juga menawarkan pemandangan bawah lautnya yang mempesona.</li>
							<li>Air Terjun Tunan, memiliki ketinggian 86 meter dan sudah dikelola oleh pemerintah setempat sebagai objek wisata.</li>
						</ul>
				    	<hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Potensi Lokasi</h5>
				    	<p class="card-text">Terumbu Karang, Mangrove, Lamun, Paus, Lumba-lumba, Duyung, Penyu, Manta, Hiu, Ikan karang, Napoleon.</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Wisata yang Tersedia</h5>
				    	<p class="card-text">Menyelam, Snorkeling, Olahraga air, Goa, Tempat bersejarah, Pasar tradisional almadidi, Taman Nasional Gunung klabat</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Fasilitas Pendukung Wisata</h5>
				    	<p class="card-text">Penginapan, Restoran, Toko souvenir, Mercusuar dan Pelabuhan</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Transportasi</h5>
				    	<p class="card-text">Dari Kota Manado dapat ditempuh dengan kendaraan darat menuju Minahasa utara, sekitar 30 menit. Transportasi antar pulau dapat ditempuh dengan speed boat.</p>
				    	
				   	</div>
				  </div>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-12">
				<div class="card-deck">
				  <div class="card">
				    <div class="card-body text-justify">
				    	<div class="row">
				    		<div class="col-md-6">
				    		<div class="media">
				    		<div class="media-body">
				    			<div class="text-center" style="background-image: url('${pageContext.request.contextPath}/images/maps.png');height: 225px;width: 450px;"></div>
				    		</div>
					    	</div>
				    		</div>
					    	
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold">Detail Lokasi</h5>
				      			<p class="card-text">
				      				Please make your own way to: Lombok Elephant Park, Jalan Raya Tanjung, Nusa Tenggara Barat. 83352, Sire, Sigar Penjalin, Tanjung , Lombok Utara, Sigar Penjalin, Tanjung, North Lombok Regency, Nusa Tenggara Barat 83352, Indonesia
				      			</p>
					    	</div>
				    	</div>
				    </div>
				    <!-- <div class="card-footer bg-white">
				      <div class="row">
					      <div class="text-warning font-weight-bold col mt-2">RP. 50.000</div>
					      <div class="col"><button class="btn btn-warning  pull-right float-right">Buy Ticket</button></div>
				      </div>
				    </div> -->
				  </div>
				</div>
			</div>
		</div>
		
        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  
		</c:if>
		
		<c:if test="${param.step != null && param.step == 1}">	
		<%
			
		%>	
		<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-book-1">
		<!-- start -->
		
        <div class="container">
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			<h4 class="">Contact Details</h4>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-8">
			<div class="card">
			  <div class="card-header">
			    <h5>Contact Details (for E-ticket)</h5>
			  </div>
			    <div class="card-body">
				    <form>
					  <div class="form-group">
					    <label for="">Full Name<span class="text-danger">*</span></label>
					    <input type="text" class="form-control" id="">
					    <small id="" class="form-text text-muted">As on ID Card/passport/driving license (without degree or special characters).</small>
					  </div>
					  
					  <div class="form-row">
							<div class="col-md-6">
					    		<label for="">Mobile Number<span class="text-danger">*</span></label>
							</div>
							<div class="col-md-6">
					    		<label for="">Email<span class="text-danger">*</span></label>
							</div>
					  </div>
					  <div class="form-row">
							<div class="form-group col-md-2">
								<input type="text" class="form-control" name="" placeholder="+62">
							</div>
							<div class="form-group col-md-4">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
							<div class="form-group col-md-6">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
					  </div>
					  <div class="form-row">
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g +6282119260548 for Country (+62) and Mobile No. 082119260548</small>
							</div>
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g email@example.com</small>
							</div>
					  </div>
					</form>
			    </div>
			</div>
			
			<div class="p-3"></div>
			<h4 class="mb-3">Traveler Details</h4>
			<% 
			//out.print("KE "+purchase.getCountTicket());
			for(int i=0; i < purchase.getCountTicket(); i++) { %>

				<div class="card mb-3">
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <h5 class="mb-0" id="followers">Adult <span class="d-none d-sm-inline-block"><%out.print((i+1)); %></span></h5>
	                </div>
	                <div class="col">
	                  <form>
	                    <div class="row no-gutters float-right">
	                      <div class="col d-md-block d-none">
	                      	<select class="custom-select custom-select-sm ml-2">
	                          <option selected="selected">Choose Your Type Identity</option>
	                          <option>KTP</option>
	                          <option>Passport</option>
	                          <option>Other</option>
	                        </select>
	                      </div>
	                    </div>
	                  </form>
	                </div>
	              </div>
				</div>
				    <div class="card-body">
					    <form>
						  <div class="form-row">
								<div class="form-group col">
								    <label for="">No. Identity<span class="text-danger">*</span></label>
								    <input type="text" class="form-control" id="">
								</div>
								<div class="form-group col">
								    <label for="">Full Name<span class="text-danger">*</span></label>
								    <input type="text" class="form-control" id="">
								</div>
						  </div>
						  
						  <div class="form-row">
								<div class="col-md-6">
						    		<label for="">Mobile Number<span class="text-danger">*</span></label>
								</div>
								<div class="col-md-6">
						    		<label for="">Email<span class="text-danger">*</span></label>
								</div>
						  </div>
						  <div class="form-row">
								<div class="form-group col-md-2">
									<input type="text" class="form-control" name="" placeholder="+62">
								</div>
								<div class="form-group col-md-4">
								      <input type="text" class="form-control" name="" placeholder="">
								</div>
								<div class="form-group col-md-6">
								      <input type="text" class="form-control" name="" placeholder="">
								</div>
						  </div>
						  <div class="form-row">
								<div class="col-md-6">
						    		<small id="" class="form-text text-muted">e.g +6282119260548 for Country (+62) and Mobile No. 082119260548</small>
								</div>
								<div class="col-md-6">
						    		<small id="" class="form-text text-muted">e.g email@example.com</small>
								</div>
						  </div>
						</form>
				    </div>
				</div>
		<%	}
			%>
			
			<button type="button" onclick="$('#btn-book-2').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			
			</div>
			<div class="col-md-4">
			<div class="card">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				int key=0;
				for(File o2 : list_photo){
				  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
				  	o2.setName(o2.getName().replace(" ", "%"));
				  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
				  	if(key==0){
				  		//out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
				  		out.println("<img style='width:70px;height:70px;object-fit:cover' src="+path+" class='mr-3 img-fluid'>");
				  	}
				  	key++;
				}
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 class="mt-0"><% out.print(region.getName()); %></h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Visit Date</span> <span class="float-right small"><% out.print(purchase.getStartDate()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Time Slot</span> <span class="float-right small"><% out.print(purchase.getEndDate()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			  </ul>
			  <div class="card-body">
			    <span class="text-success"><i class="far fa-check-circle "></i> Refundable</span>
			  </div>
			</div>
			</div>
			
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-2" >
		<!-- start -->
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Vehicle Options</h4>
			<h4 class="mb-3"><small class="text-muted">Choose vehicle for you tour</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
			<ul class="nav nav-pills justify-content-center border-bottom border-info">
				
			 <%
			 List<String> str = new ArrayList<>();
			 for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){
				 for(Vehicle vehicle : list_vehicle){
					 if(tv.getId().equals(vehicle.getTypeId())){
					 	if(!str.contains(tv.getName()))
						 str.add(tv.getName());
					 }
				 }
			 }
			 System.out.print("V: "+str);
			 key=0;
				for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){ %>
					<% 
					for(int i=0; i < str.size(); i++) {
						if(str.get(i).equals(tv.getName())){
							%>
							<li class="nav-item">
					    	<a class="nav-link <% if(key==0) out.print("active"); %>" id="pills-car<%out.print(tv.getId());%>-tab" data-toggle="pill" href="#pills-car<%out.print(tv.getId());%>"><% out.print(tv.getName()); %></a>
						   	</li>
							<%
						}
					}
					%>
					
				   	
	 		 <% 
	 		 	key++;
	 		 	} 
	 		 %> 	
			</ul>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="tab-content" id="pills-tabContent">
			<% key=0; for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){ %>
				<div class="tab-pane fade<% if(key==0) out.print("show active"); %>" id="pills-car<%out.print(tv.getId());%>"><!-- show active -->
					<div data-id="container" class="row">
					<%
						for(Vehicle vehicle : list_vehicle){ %>
						
							<%
							param = new QueryParameter();
							param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+vehicle.getId()+"'");
							System.out.println("si "+fileMapper.getList(param).size()+" "+vehicle.getId());
							for(File vehicle_file : fileMapper.getList(param)){ 
								vehicle_file.setName(vehicle_file.getName().replace(" ", "%"));
							  	String path=request.getContextPath()+"/files/"+vehicle_file.getId()+"?filename="+vehicle_file.getName()+"&download";
							  	//vehicle.setPhoto(path);
							if(tv.getId().equals(vehicle.getTypeId())){
							%>
							
							<div class="col-md-4 mt-3">
							<div class="card mb-3">
				  				<img src="<% out.print(path); %>" class="card-img-top" alt="...">
							    <div class="card-body">
									  <div class="form-row">
											<div class="col-md-12">	
												<h5 class="card-title"><% out.print(vehicle.getName()+" ("+vehicle.getTypeName()+")"); %></h5>
									    		<small id="" class="form-text text-muted">Starting From</small>
									    		<small id="" class="form-text text-primary"><% out.print(vehicle.getCost()); %> / Day</small>
											</div>
									  </div>
							    </div>
							</div>
							</div>
						
					<% 		
							}
							}
						} 
					%>
					</div>
				</div>
				<% key++;} %>
			</div>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-3').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-3" >
			
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Tour Guide</h4>
			<h4 class="mb-3"><small class="text-muted">Choose someone to become your tour guide</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="card bg-light">
			<div class="card-body bg-light p-0">
	    	<div class="row no-gutters text-center fs--1">
	    	<%
	    	
    		for(TourGuide tg : tourGuideMapper.getList(new QueryParameter())){
    		//
	    		param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+tg.getId()+"'");
				for(File tour_file : fileMapper.getList(param)){ 
					tour_file.setName(tour_file.getName().replace(" ", "%"));
				  	String path=request.getContextPath()+"/files/"+tour_file.getId()+"?filename="+tour_file.getName()+"&download";
				  	//if(tg.getId().equals(vehicle.getTypeId())){
		               	out.print("<div class='col-6 col-md-4 col-xl-3 col-xxl-2 mb-1'>");
		    			out.print("<div class='bg-white p-3 h-100'>");
		    			out.print("<a href='../pages/profile.html'><img class='img-thumbnail img-fluid rounded-circle mb-3 shadow-sm' src="+path+" width='100'></a>");
		   				out.print("<h6 class='mb-1'><a href='../pages/profile.html'>"+tg.getFullName()+"</a></h6>");
						out.print("<p class='small mb-1'><a class='text-muted' href='#!'>"+tg.getDescription()+"</a></p>");
		               	out.print("</div>");
		               	out.print("</div>");
				  	//}
	    		}
    		}
           	%>
              </div>
	  		</div>
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-2').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-4">
		<div class="container">
		
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			<h4 class="">Contact Details</h4>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-8">
			
			<div class="card mb-3">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="mb-0">Kayum Munajir</h5>
                </div>
                <div class="col-auto"><a class="btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>
              </div>
            </div>
            <div class="card-body bg-light border-top">
			<div class="row">
	<div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">
		<div class="row">
			<div class="col-5 col-sm-4">
				<p class="font-weight-semi-bold mb-0">No.Identity</p>
			</div>
			<div class="col">
				<p class="font-weight-semi-bold mb-0">7C23435</p>
			</div>
		</div>
		<div class="row">
			<div class="col-5 col-sm-4">
				<p class="font-weight-semi-bold mb-0">Full Name</p>
			</div>
			<div class="col">
				<p class="font-weight-semi-bold mb-0">Kayum Munajir</p>
			</div>
		</div>
		<div class="row">
			<div class="col-5 col-sm-4">
				<p class="font-weight-semi-bold mb-1">Phone number</p>
			</div>
			<div class="col">
				<p class="font-weight-semi-bold mb-0">+6282119260548</p>
			</div>
		</div>
		<div class="row">
			<div class="col-5 col-sm-4">
				<p class="font-weight-semi-bold mb-1">Email</p>
			</div>
			<div class="col">
				<a href="mailto:tony@gmail.com">tony@gmail.com</a>
			</div>
		</div>
	</div>
</div>
            </div>
          </div>
			
		<div class="card mb-3">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col">
                  <h5 class="mb-0">Tour Guide</h5>
                </div>
                <div class="col-auto"><a class="btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>
              </div>
            </div>
            <div class="card-body bg-light border-top">
	            <div class="row no-gutters">
			    <div class="col-auto">
			      <img src="http://localhost:9910/kawasan/files/1554198623696115?filename=6.jpg&download" class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" width="150">
			    </div>
			    <div class="col-md-8">
			      <div class="card-body">
			        <h5 class="card-title">Isaac Hempstead</h5>
			        <strong class="card-text text-success"><span>RP </span>280.000<span>/Day</span></strong>
			        <div class="row card-text"><div class="col-auto"><small class="text-muted">Phone : +6282119260548</small></div>
						<div class="col-auto"><small class="text-muted">Email : issac@email.com</small></div>
					</div>
			        <div class="row card-text">
						<div class="col-auto"><small class="text-muted">Language : Engslish</small></div>
					</div>
			      </div>
			    </div>
			  	</div>
            </div>
          </div>
			
		<div class="card mb-3">
           <div class="card-header">
             <div class="row align-items-center">
               <div class="col">
                 <h5 class="mb-0">Vehicle</h5>
               </div>
               <div class="col-auto"><a class="btn btn-falcon-default btn-sm" href="#!"><svg class="svg-inline--fa fa-pencil-alt fa-w-16 fs--2 mr-1" aria-hidden="true" data-prefix="fas" data-icon="pencil-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"></path></svg><!-- <span class="fas fa-pencil-alt fs--2 mr-1"></span> -->Update details</a></div>
             </div>
           </div>
           <div class="card-body bg-light border-top">
            <div class="row no-gutters">
		    <div class="col-md-4">
		      <img src="http://localhost:9910/kawasan/files/1554143418941617?filename=gallery-img-3.jpg&amp;download" class="card-img" alt="...">
		    </div>
		    <div class="col-md-8">
		      <div class="card-body">
		        <h5 class="card-title">Suzuki Ertiga (MINI MPV)</h5>
		        <strong class="card-text text-success">RP 280.000</strong>
		        <div class="row card-text"><div class="col-auto"><small class="text-muted">Seat : 6</small></div>
					<div class="col-auto"><small class="text-muted">Suitcase : 2</small></div></div>
		      </div>
		    </div>
		  	</div>
           </div>
           </div>
          
			<hr>
			</div>
			
			<div class="col-md-4">
			<div class="card">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				//key=0;
				for(File o2 : list_photo){
				  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
				  	o2.setName(o2.getName().replace(" ", "%"));
				  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
				  	//if(key==0){
				  		//out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
				  		out.println("<img style='width:70px;height:70px;object-fit:cover' src="+path+" class='mr-3 img-fluid'>");
				  	//}
				  	//key++;
				}
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 class="mt-0"><% out.print(region.getName()); %></h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Visit Date</span> <span class="float-right small"><% out.print(purchase.getStartDate()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Time Slot</span> <span class="float-right small"><% out.print(purchase.getEndDate()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			  </ul>
			  <div class="card-body">
			    <span class="text-success"><i class="far fa-check-circle "></i> Refundable</span>
			  </div>
			</div>
			<button type="button" onclick="$('#btn-book-5').click()" class="btn btn-outline-lights bg-orange btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 btn-block mt-3">Continue to Payment</button>
			</div>
			
		</div>
		</div>
		</div>
		
		<div class="tab-pane fade" id="pills-book-5">
		<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
			<div class="card mb-3">
            <div class="card-body">
              <div class="row align-items-center text-center mb-3">
                <div class="col-sm-6 text-sm-left"><img src="https://prium.github.io/falcon/assets/img/logos/logo-invoice.png" alt="invoice" width="150"></div>
                <div class="col text-sm-right mt-3 mt-sm-0">
                  <h2 class="mb-3">Invoice</h2>
                  <h5>Falcon Design Studio</h5>
                  <p class="small mb-0">156 University Ave, Toronto<br>On, Canada, M5H 2H7</p>
                </div>
                <div class="col-12">
                  <hr>
                </div>
              </div>
              <div class="row justify-content-between align-items-center">
                <div class="col">
                  <h6 class="text-muted">Invoice to</h6>
                  <h5>Antonio Banderas</h5>
                  <p class="small">1954 Bloor Street West<br>Torronto ON, M6P 3K9<br>Canada</p>
                  <p class="small"><a href="mailto:example@gmail.com">example@gmail.com</a><br><a href="tel:444466667777">+4444-6666-7777</a></p>
                </div>
                <div class="col-sm-auto ml-auto">
                  <div class="table-responsive">
                    <table class="table table-sm table-borderless fs--1">
                      <tbody>
                        <tr>
                          <th class="text-sm-right">Invoice No:</th>
                          <td>14</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Order Number:</th>
                          <td>AD20294</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Invoice Date:</th>
                          <td>2018-09-25</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Payment Due:</th>
                          <td>Upon receipt</td>
                        </tr>
                        <tr class="alert-success font-weight-bold">
                          <th class="text-sm-right">Amount Due:</th>
                          <td>$19688.40</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="table-responsive mt-4 fs--1">
                <table class="table table-striped border-bottom">
                  <thead>
                    <tr class="bg-primary text-white">
                      <th class="border-0">Items</th>
                      <th class="border-0 text-center">Quantity</th>
                      <th class="border-0 text-right">Price per item</th>
                      <th class="border-0 text-right">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Ticket</h6>
                        <p class="mb-0">Raja Ampat</p>
                      </td>
                      <td class="align-middle text-center">2</td>
                      <td class="align-middle text-right">$65.00</td>
                      <td class="align-middle text-right">$130.00</td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Vehicle</h6>
                        <p class="mb-0">Fortuner</p>
                      </td>
                      <td class="align-middle text-center">1</td>
                      <td class="align-middle text-right">$2,100.00</td>
                      <td class="align-middle text-right">$2,100.00</td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Tour Guide</h6>
                        <p class="mb-0">Mr.alaex</p>
                      </td>
                      <td class="align-middle text-center">6</td>
                      <td class="align-middle text-right">$2,000.00</td>
                      <td class="align-middle text-right">$12,000.00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="row no-gutters justify-content-end">
                <div class="col-auto">
                  <table class="table table-sm table-borderless fs--1 text-right">
                    <tbody>
                    <!-- <tr>
                      <th class="text-900">Subtotal:</th>
                      <td class="font-weight-semi-bold">$18,230.00 </td>
                    </tr>
                    <tr>
                      <th class="text-900">Tax 8%:</th>
                      <td class="font-weight-semi-bold">$1458.40</td>
                    </tr> -->
                    <tr class="border-top">
                      <th class="text-900">Total:</th>
                      <td class="font-weight-semi-bold">$19688.40</td>
                    </tr>
                    <tr class="border-top border-2x font-weight-bold text-900">
                      <th>Amount Due:</th>
                      <td>$19688.40</td>
                    </tr>
                  </tbody></table>
                </div>
              </div>
            </div>
            <div class="card-footer bg-light">
              <p class="fs--1 mb-0"><strong>Notes: </strong>We really appreciate your business and if there's anything else we can do, please let us know!</p>
            </div>
          </div>
			</div>
		</div>
		</div>
		</div>
		
		</div>
		</c:if>
      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="max-width:">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Booking Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body bg-light pt-0">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form" class="mt-3">
		  	<div class="form-row">
				<div class="form-group col-md-12">
					<input type="number" class="form-control" name="count_ticket" placeholder="Number of travelers/visitors">
				</div>
			</div>
		  	<div class="form-row">
				<div class="form-group col-md-6">
					<input type="text" class="form-control single-date-picker" name="start_date" placeholder="Check In">
				</div>
				<div class="form-group col-md-6">
				      <input type="text" class="form-control single-date-picker" name="end_date" placeholder="Check Out">
				</div>
			</div>
			<input name="region_id" type="text" value="${param.id}">
			<!-- <input name="authorization" type="text" value="Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTc4MjE1MTIyLCJqdGkiOiJkM2NjYjA2Ny00YTM5LTQ4NjUtOGY1Ny02YzU4YTNmY2ZkMTAiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.JJNifAbsNIu7hkrpfjfkUOTRtI5iSVi-nqwfjrTzmWqwl32jQ9A9-jbcbSYvqIQH6y9zFa0Tn_iNcro-gtRr-qFGl5evVyqGrZq6AE776CNZ3irhOijNjV3LwRxgp-B3BJY7OdS8nqtW0plhA6IfdYB16wVCn7JCN3XYO6FzcfUzcgkDAp5Fi-mFUEyMSmtFGi7wS-pBN7wHF2ZRIuloAB7_hefeCxRI3hthQnz_VnJ6foADVqw6MSjhWBVVxYBd10_OkADrkP9XsXMCVnHbWHGZZXNVJQAdl-fcdo-ZocIETvCqmEddKQ05y4Vpgt-bPdwY62K4BG1RkdahqUpQKg"> -->
			
			<button type="submit" id="btn-submit">?</button>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" onclick="$('#btn-submit').click();" class="btn btn-success pill pl-3 pr-3"><i class="fa fa-check"></i> Done</button>
	      </div>
	    </div>
	  </div>
	</div>
    <%@ include file = "inc/footer.jsp" %>
    <script>$('#container-search').hide()</script>
    <script src="${pageContext.request.contextPath}/js/general.js"></script>

  </body>

</html>
