<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body class="body" id="page-top">

    <%@ include file = "inc/navbar-head.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">

        <div class="container-fluid">
        
		<!-- <div class="container-fluid pl-5s pt-5 pb-3 pr-5s"> -->
		<div class="row justify-content-center">
		<div class="col-md-10">
		<div class="card bg-head">
	    <div class="card-header">
		<div class="row">
			<div class="col-md-6">
				<h4>Region</h4>
			</div>
			<div class="col-md-6">
				<button id=btn-add data-toggle="modal" data-target="#modal-form" class="btn btn-light pill pl-4 pr-4 float-right"><span class="fas fa-plus"></span> <span class="badge badge-pill badge-success">NEW</span></button>
			</div>
		</div>
		</div>
	    <div class="card-body p-0">
		<div class="accordion" id="accordionExample">
		  <div class="card">
		    <div class="card-header card-header-2">
				<div class="row rows-head">
					<div class="col-md-1">#</div>
					<div class="col-md-3">Name </div>
					<div class="col">Type Region</div>
					<div class="col">Location</div>
					<div class="col">Action</div>
				</div>
		    </div>
		  </div>
		  <div id="table-card-body" style="opacity:0.8"></div>
		</div>
	    </div>
	    <div class="card-header card-header-2s d-none">
			<div class="row rows-heads">
				<div class="col">11 Items - view all <i class="fas fa-angle-right ml-1"></i></div>
				<div class="col float-right text-right">
			        <button class="btn btn-light btn-sm" type="button" disabled="disabled"><span>Previous</span></button>
			        <button class="btn btn-primary btn-sm px-4 ml-2" type="button"><span>Next</span></button>
		        </div>
			</div>
	    </div>
		</div>
		</div>
		</div>
		<!-- </div> -->
		
        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog " role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form" style="display:none">
			  <div class="form-group row">
			  	<div class="col-md-6">
				    <label for="">Name</label>
				    <input type="text" class="form-control" name="name" placeholder="Entry Region Name">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Type</label>
	                <select name="type" class="form-control pt-2 pb-1 ">
	                    <option value="">Select Region Type</option>
	                    <option value="DARAT">DARAT</option>
	                    <option value="LAUT">LAUT</option>
	                </select>
                </div>
			  </div>
			  
			  	
			  
			  <div class="form-group row">
			  	<div class="col-md-6">
				    <label for="">Kuota</label>
				    <input type="number" class="form-control" name="kuota" placeholder="Entry Kuota" value="0">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Max Days</label>
				    <input type="number" class="form-control" name="max_day" placeholder="Entry Maximal Hari" value="2">
			    </div>
			  </div>
			  
			  <div class="form-group row">
			  	<div class="col-md-12">
				    <label for="">Location(optional)</label>
				    <input type="text" class="form-control" name="location" placeholder="Entry Coordinat Location">
			    </div>
			  </div>
			  
			  <div class="form-group row">
			  	<div class="col-md-12">
				    <label for="">Address Location</label>
				    <input type="text" class="form-control" name="location_desc" placeholder="Address/Alamat Location Kawasan">
                </div>
			  </div>
			  
			  <div class="form-group">
			    <label for="">Description/Overview</label>
			    <textarea placeholder="Ex: Gunung Bromo adalah bagian dari Taman Nasional Bromoenggeremeru. Terkenal karena kaldera yang eksotis." class="form-control p-3" id="description" rows="6"></textarea>
			  </div>
			  
			  <div class="form-group row">
		    	<div class="col">
				<label for="">Upload Denah Pendakian</label>
				<div class="custom-file">
				  <input type="file" class="custom-file-input" id="customFile" name="file">
				  <label class="custom-file-label" for="customFile">Click here to upload</label>
				</div>
				</div>
			  	<div class="col">
				    <label for="">Refund(optional)</label>
				    <div class="custom-control custom-switch">
					  <input type="checkbox" class="custom-control-input" id="refund" name="refund">
					  <label class="custom-control-label" for="refund">Refund</label>
					</div>
			    </div>
			  </div>
			  <!-- <div class="form-group">
			  	<div style="display: none">
			      <input id="pac-input"
			             class="controls"
			             type="text"
			             placeholder="Enter a location">
			    </div>
			    <div id="map"></div>
			    <div id="infowindow-content">
			      <span id="place-name"  class="title"></span><br>
			      <strong>Place ID</strong>: <span id="place-id"></span><br>
			      <span id="place-address"></span>
			    </div>
			  </div> -->
			  
			  <!-- <div class="form-group">
			    <label for="">Gambar Kawasan</label>
			    <div class="row">
			    <div id="container-upload">
			    <div class="col-md-3">
			    <div class="bg-head d-flex justify-content-center align-items-center pill" style="
				    height: 161px;
				    /* background-image: url(https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnG98M/3uI16G3tLN17gcLc6PsaHRlPhwKyEDXJ3hZPmbanBtM8D6JzU42qyM4VaPjiMqUlXTdzUr2jNOPsddnkC49390o5IOgU1/OoIc4cRxiqiStapzN6HYw6bU84dDsQ==); */
				    background-size: cover;
				"><i class="fas fa-camera fa-3x"></i></div>
				</div>
				</div>
				</div>
			  </div>
			  <input type="file" id="file_kawasan" name="file" onchange="readURL(this)">
			  <div class="form-group">
			    <label for="">Servis / Layanan Kawasan</label>
			    <input type="text" class="form-control" name="services" placeholder="">
			  </div> -->
			</form>
			<!-- <hr/> -->
			 <form id="entry-form-facility" style="display:none">
			  <div class="form-group row">
			  	<div class="col-md-12">
				    <label for="">Code</label>
	                <select name="code" class="form-control select-style p-2">
	                    <option value="DEFAULT">DEFAULT</option>
	                    <option value="WNA">WNA</option>
	                </select>
			    </div>
			    
			  </div>
			  <div class="form-group row">
			  	<div class="col-md-12">
				    <label for="">Item</label>
				    <input type="text" class="form-control" name="item" placeholder="Entry Item">
			    </div>
			  </div>
			  
			  <div class="form-group row">
			  	<div class="col-md-6">
				    <label for="">Price</label>
				    <input type="text" class="form-control" name="price" placeholder="Entry Price Item">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Include</label>
	                <select name="include" class="form-control select-style p-2">
	                    <option value="">Select Include</option>
	                    <option value="Y">Y</option>
	                    <option value="N">N</option>
	                </select>
                </div>
			  </div>
			
			</form>
			
			<!-- <hr/> -->
			
			 <form id="entry-form-transit" style="display:none">
			 
			  <div class="form-group row">
			  	<div class="col-12">
				    <label for="">Code</label>
				    <input type="text" class="form-control" name="code" placeholder="Contoh : TC 1, TC 2, dst..">
			    </div>
		      </div>
			  <div class="form-group row">
			  	<div class="col-md-12">
				    <label for="">Transit Camp</label>
	                <select name="id_master_transit_camp" class="form-control p-2">
	                    <option value="">Pilih Lokasi Transit Camp</option>
	                </select>
			    </div>
			  </div>
			  <div class="form-group row">
			  	<div class="col-12">
				    <label for="">Waktu Registrasi(Start and End)</label>
				    <div class="input-group">
				    <input type="time" class="form-control" name="start_register" placeholder="">
					  <div class="input-group-prepend">
					    <span class="input-group-text">S.D</span>
					  </div>
				    <input type="time" class="form-control" name="end_register" placeholder="">
					</div>
			    </div>
			  </div>
			  <div class="form-group row">
			  	<div class="col-12">
				    <label for="">Perkiraan Waktu Tiba</label>
				    <input type="time" class="form-control" name="extimate_arrived" placeholder="">
			    </div>
			  </div>
			  <div class="form-group row">
			  	<div class="col-6">
				    <label for="">Small Tent</label>
				    <input type="number" class="form-control" name="tent_small" placeholder="Jumlah Tenda Kecil">
			    </div>
			  	<div class="col-6">
				    <label for="">Large Tent</label>
				    <input type="number" class="form-control" name="tent_large" placeholder="Jumlah Tenda Besar">
			    </div>
			  </div>
			  <div class="form-group row">
		    	<div class="col">
			    <label for="">Upload Denah Tenda Kapling(Opsional)</label>
				<div class="custom-file">
				  <input type="file" class="custom-file-input" id="customFile" name="file">
				  <label class="custom-file-label" for="customFile">Choose file</label>
				</div>
				</div>
			  </div>
			  <!-- <div class="form-group row">
			  	<div class="col-md-6">
				    <label for="">Price</label>
				    <input type="text" class="form-control" name="price" placeholder="Entry Price Item">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Include</label>
	                <select name="include" class="form-control select-style">
	                    <option value="">Select Include</option>
	                    <option value="TRUE">Y</option>
	                    <option value="FALSE">N</option>
	                </select>
                </div>
			  </div> -->
			
			</form>
			
			<!-- <hr/> -->
			 <form id="entry-form-todolist" style="display:none">
			 
			  <div class="form-group row">
			  	<div class="col-3">
				    <label for="">Parent</label>
				    <input type="text" class="form-control" name="parent" readonly="readonly">
			    </div>
		      </div>
			  <div class="form-group row">
			  	<div class="col-3">
				    <label for="">Days</label>
				    <input type="number" class="form-control" name="day" placeholder="Entry Days">
			    </div>
			  	<div class="col-3">
				    <label for="">Time</label>
				    <input type="time" class="form-control" name="times" placeholder="">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Activity</label>
				    <input type="text" class="form-control" name="activity" placeholder="Entry Activity">
			    </div>
			  </div>
			  
			  <!-- <div class="form-group row">
			  	<div class="col-md-6">
				    <label for="">Price</label>
				    <input type="text" class="form-control" name="price" placeholder="Entry Price Item">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Include</label>
	                <select name="include" class="form-control select-style">
	                    <option value="">Select Include</option>
	                    <option value="TRUE">Y</option>
	                    <option value="FALSE">N</option>
	                </select>
                </div>
			  </div> -->
			
			</form>
			
			 <form id="entry-form-tent" style="display:none">
			  
			  <div class="form-group row">
			  	<div class="col-md-6">
				    <label for="">Jumlah Tenda</label>
				    <input type="text" class="form-control" name="jumlah_tenda" placeholder="Masukan jumlah tenda yang ingin dibuat..">
			    </div>
			  	<div class="col-md-6">
				    <label for="">Ukuran Tenda</label>
	                <select name="ukuran_tenda" class="form-control p-2">
	                    <option value="">Select Ukuran Tenda</option>
	                    <option value="K">KECIL</option>
	                    <option value="B">BESAR</option>
	                </select>
                </div>
			  </div>
			  
			   <select id="id_master_transit_camp" class="form-control p-2">
                   <option value="">Choose</option>
               </select>
			  <button type="button" onclick="createFormTent();" class="btn btn-success pill pl-5 pr-5 mb-3">Submit</button>
			  
			  <div class="form-group row">
			  <div class="col-md-6">
			    <div class="card">
			      <div class="card-header card-title bg-warning">Tenda Kecil</div>
			      <div class="card-body">
			        <div id="row-tenda-kecil" class="row">
			          <!-- <div class="border col-md-2 p-1 m-1 text-center bg-success">PGK 1</div> -->
			        </div>
			      </div>
			    </div>
			  </div>
			  <div class="col-md-6">
			    <div class="card">
			      <div class="card-header card-title bg-primary">Tenda Besar</div>
			      <div class="card-body">
			        <div id="row-tenda-besar" class="row">
			          <!-- <div class="border col-md-2 p-1 m-1 text-center bg-success">PGB 1</div> -->
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			  
			</form>
			
			<!-- <hr/> -->
			<form id="entry-form-photo" style="display:none">
			  <div class="form-group">
			    <label for="">Region Photo</label>
			    <div class="row">
			    	<div class="col">
					<div class="custom-file">
					  <input type="file" class="custom-file-input" id="customFile" name="file">
					  <label class="custom-file-label" for="customFile">Choose file</label>
					</div>
					</div>
<!-- 			    	<div class="col">
					  <button class="btn btn-secondary btn-block">Preview</button>
					</div> -->
				</div>
			  </div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="save();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<div class="remodal" data-remodal-id="modalku" style="padding: 0;">
	<!-- min-width: 75%; -->
	  <button data-remodal-action="close" class="remodal-close"></button>
	  <!-- <h1>Remodal</h1> -->
	  	<img id="view_photo" src="" class="img-fluid">
	 </div>
	
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/region.js"></script>
    
    <!-- batas -->
    <script>
	// This sample requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script
	// src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
	
	/* function initMap() {
	  var map = new google.maps.Map(
	      document.getElementById('map'),
	      {center: {lat: -33.8688, lng: 151.2195}, zoom: 13});
	
	  var input = document.getElementById('pac-input');
	
	  var autocomplete = new google.maps.places.Autocomplete(input);
	
	  autocomplete.bindTo('bounds', map);
	
	  // Specify just the place data fields that you need.
	  autocomplete.setFields(['place_id', 'geometry', 'name', 'formatted_address']);
	
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
	  var infowindow = new google.maps.InfoWindow();
	  var infowindowContent = document.getElementById('infowindow-content');
	  infowindow.setContent(infowindowContent);
	
	  var geocoder = new google.maps.Geocoder;
	
	  var marker = new google.maps.Marker({map: map});
	  marker.addListener('click', function() {
	    infowindow.open(map, marker);
	  });
	
	  autocomplete.addListener('place_changed', function() {
	    infowindow.close();
	    var place = autocomplete.getPlace();
	
	    if (!place.place_id) {
	      return;
	    }
	    geocoder.geocode({'placeId': place.place_id}, function(results, status) {
	      if (status !== 'OK') {
	        window.alert('Geocoder failed due to: ' + status);
	        return;
	      }
	
	      map.setZoom(11);
	      map.setCenter(results[0].geometry.location);
	
	      // Set the position of the marker using the place ID and location.
	      marker.setPlace(
	          {placeId: place.place_id, location: results[0].geometry.location});
	
	      marker.setVisible(true);
	
	      infowindowContent.children['place-name'].textContent = place.name;
	      infowindowContent.children['place-id'].textContent = place.place_id;
	      infowindowContent.children['place-address'].textContent =
	          results[0].formatted_address;
	
	      infowindow.open(map, marker);
	    });
	  });
	} */
	    </script>
<!-- 	    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWgtK7esihCzjpOEvZU1dza1XM6ocaPy8&libraries=places&callback=initMap" async defer></script> -->
	    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSxY4GlSEbJ2MzlIg5y5lIhyOcMdsYiVg&libraries=places&callback=initMap" async defer></script> -->
    <!-- end -->

  </body>

</html>
