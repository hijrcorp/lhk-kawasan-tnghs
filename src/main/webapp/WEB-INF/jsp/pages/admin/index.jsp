<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%@ include file = "inc/temp-header.jsp" %>


	
	
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 
		
      <div id="content-wrapper">
		
		<!-- container-fluid  -->
        <div class="container-fluid container-fluid-md container-fluid-sm"> <!-- d-none d-md-block -->
        
        <!-- </div>
        <div class="container-fluid d-md-none d-block">
		 </div> -->
		 
		<c:choose>
		<c:when test = "${fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN')}">
       		
			<div class="container pl-md-5 pt-5 pb-md-3 pr-md-5">
				
			<div class="row">
				<div class="col-md-4 col-6 pb-3">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/purchase" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-swatchbook fa-4x mb-4"></i> 
						<button type="button" class="btn btn-light btn-lg bg-head pill pl-md-5 pr-md-5 btn-block">Purchase</button>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-6 pb-3">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/region" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-map-marked-alt fa-4x mb-4"></i> 
						<button type="button" class="btn btn-light btn-lg bg-head pill pl-md-5 pr-md-5 btn-block">Region</button>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-6 pb-3">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/sub-master" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-table fa-4x mb-4"></i> 
						<button type="button" class="btn btn-light btn-lg bg-head pill pl-md-5 pr-md-5 btn-block">Master</button>
						</a>
					</div>
				</div>
				
				<div class="col-md-4 col-6 pt-md-5">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/daftar-boarding" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-ticket-alt fa-4x mb-4"></i> 
						<button  type="button" class="btn btn-light btn-lg bg-head pill pl-md-5 pr-md-5 btn-block">Boarding</button>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-6 pt-md-5">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/sub-master?mode=" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-cog fa-4x mb-4"></i> 
						<button  type="button" class="btn btn-light btn-lg bg-head pill pl-md-5 pr-md-5 btn-block">Settings</button>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-6 pt-md-5">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/report" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
						<i class="fa fa-chart-bar fa-4x mb-4"></i> 
						<button  type="button" class="btn btn-light btn-lg bg-head pill pl-md-5 pr-md-5 btn-block">Report</button>
						</a>
					</div>
				</div>
			</div>
			</div>
		</c:when>
		
		
		<c:when test="${fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_OPERATOR') || fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_SUPERVISOR')}">
			
			<div class="container pl-5 pt-5 pb-3 pr-5">
				
			<div class="row">
				<div class="col-md-4 col-6">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/purchase" class="mr-4s" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-swatchbook fa-4x ml-5 mr-5 mb-4"></i> 
						<button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Purchase</button>
						</a>
					</div>
				</div>
				
				<div class="col-md-4 col-6">
					<div class="text-center">
						<a href="${pageContext.request.contextPath}/page/admin/daftar-boarding" class="mr-4s" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;">
						<i class="fas fa-ticket-alt fa-4x ml-5 mr-5 mb-4"></i> 
						<button  type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Boarding</button>
						</a>
					</div>
				</div>
			</div>
			</div>
		</c:when>
		
		</c:choose>
		
        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <%@ include file = "inc/footer.jsp" %>
    
    <%-- <script src="${pageContext.request.contextPath}/js/dashboard.js"></script> --%>
	<script>function init(){};</script>
  </body>

</html>
