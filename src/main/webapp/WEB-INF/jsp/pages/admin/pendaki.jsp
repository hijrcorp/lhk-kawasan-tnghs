<%@ include file = "inc/header.jsp" %>

<%@ include file = "inc/navbar-head.jsp" %>
<style>
	.form-control {
	    padding: 0.5rem;
	    border-radius: 10px;
    }
    .bg-gradient-new {
	    background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);
	    color: white;
	}
</style>

    <div id="wrapper" class="main-content">
    
      <div id="content-wrapper" class="section__content section__content--p20">

        <div class="container-fluid">
        
		<div class="row justify-content-center">
    	<div class="col-md-10">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb d-none">
            <li class="breadcrumb-item">
              <a href="${pageContext.request.contextPath}/page/admin/">Home</a>
            </li>
            <li class="breadcrumb-item active">Settings</li>
          </ol>

          <!-- DataTables Example -->
          <div class="card mb-3 bg-head">
            <div class="card-header">
              <i class="fas fa-table"></i>
             	Pendaki
              <div class="float-right">
                <button id="btn-add" data-toggle="modal" data-target="#modal-form" class="btn btn-light pills" type="button"><i class="fas fa-fw fa-plus"></i> New</button>
                <button id="btn-download" class="btn btn-dark d-none" type="button"><i class="fas fa-fw fa-download"></i> Unduh</button>
              </div>
              </div>
            <div class="card-body p-0">
           		<form id="search-form" class="pl-3 pr-3 pt-3">
            			<input type="hidden" name="filter_application" value="KAWASAN">
            			<!-- Aktifin input di bawah untuk ngetes paging  -->
            			<!-- <input type="hidden" name="limit" value="2">  -->
            			<div class="form-group row">
			    			<div class="col-sm-6 pb-3">
  							<div class="input-group">
							  <input type="text" class="form-control" name="filter_keyword" placeholder="Ketik kata kunci data yang ingin dicari">
							  <div class="input-group-append">
							    <button class="btn btn-secondary" type="submit">Cari</button>
							  </div>
							</div>
				  		</div>
				  		<div class="col-sm-3 offset-sm-3">
			    			<select name="filter_status_valid" class="form-control">
						      	<option value="0">Semua Status</option>
						      	<option value="">ACTIVE</option>
						      	<option value="BANNED">BANNED</option>
			    			</select>
				  		</div>
				  	</div>
				</form>
				<div class="table-responsive">
                <table id="tbl-data" class="table table-responsive table-striped table-hover m-0" style="opacity: 0.8;overflow: hidden;display:inline-table">
                  <thead class="bg-head text-dark">
					<tr>
					      <th width="5%" class="align-middle">No</th>
					      <th width="15%" class="align-middle">Nomor Identitas</th>
					      <th width="20%" class="align-middle">Nama Lengkap</th>
					      <th width="15%" class="align-middle">Birth Date</th>
					      <th width="10%" class="align-middle">Alamat Rumah</th>
					      <th width="15%" class="align-middle">Phone Number/E-mail</th>
					      <th width="5%" class="text-center align-middle">Aksi</th>
					</tr>
                  </thead>
                  <tbody class="bg-light text-dark">
                    <tr><td colspan="20"><div align="center">Ketik Kata Kunci Pencarian Anda, kemudian klik tombol cari..</div></td></tr>
                  </tbody>
                </table>
                </div>
            </div>
          </div>
          </div>
          </div>
		
        </div>
        <!-- /.container-fluid -->

        <%@ include file = "inc/trademark.jsp" %>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    
      <!-- Form Modal-->
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content pill bg-gradient-new"><form id="entry-form"><input type="hidden" name="application_id" value="KAWASAN">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Entri</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          			<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
          			<div class="form-row">
					    <div class="form-group col-md-6">
					      <label>Nomor Identitas</label>
					      <input type="text" class="form-control" name="no_identity" autocomplete="off">
					    </div>
					    <div class="form-group col-md-6">
					      <label>Tipe Identitas</label>
					      <select name="type_identity" class="form-control">
					      	<option>Tentukan Tipe Identitas</option>
					      	<option value="NIK">NIK(WNI)</option>
					      	<option value="PASSPORT">PASSPORT(WNA)</option>
					      </select>
					    </div>
					  </div>
          			<div class="form-row">
					    <div class="form-group col-md-12">
					      <label>Nama Lengkap</label>
					      <input type="text" class="form-control" name="full_name" autocomplete="off">
					    </div>
					  </div>
				     <div class="form-row">
					    <div class="form-group col-md-6">
					      <label>Jenis Kelamin</label>
					      <select name="gender" class="form-control">
					      	<option>Tentukan Jenis Kelamin</option>
					      	<option value="L">LAKI-LAKI</option>
					      	<option value="P">PEREMPUAN</option>
					      </select>
					    </div>
					    
					    <div class="form-group col-md-6">
					      <label>Tanggal Lahir</label>
					      <input type="date" class="form-control" name="birthdate" autocomplete="off">
					    </div>
					  </div>
				     <div class="form-row">
					    <div class="form-group col-md-12">
					      <label>Alamat Rumah</label>
					      <textarea name="address" class="form-control" rows="4"></textarea>
					    </div>
					 </div>
					 
					  <div class="form-row">
					    <div class="form-group col-md-6">
					      <label>Nomor HP</label>
					      <input type="text" class="form-control" name="phone_number" autocomplete="off">
					    </div>
				     	<div class="form-group col-md-6">
					      <label>Email</label>
					      <input type="text" class="form-control" name="email" autocomplete="off">
					    </div>
					  </div> 
					  
            		<div class="form-row">
            			<div class="form-group col-md-6">
					      <label>Status</label>
					      <select name="status_valid" class="form-control">
					      	<option>Tentukan Status</option>
					      	<option value="">ACTIVE</option>
					      	<option value="BANNED">BANNED</option>
					      </select>
					    </div>
            			<!-- <div class="form-group col-md-6">
					      <label>Upload KTP</label>
					      <input type="file" name="file_identity" class="form-control">
					    </div> -->
				  	</div>
				  	<hr/>
				     <div id="form-banned" class="form-row">
					    
					    <div class="form-group col-md-6">
					      <label>Tanggal Banned</label>
					      <input type="date" class="form-control" name="date_banned" autocomplete="off">
					    </div>
					    
					    <div class="form-group col-md-6">
					      <label>Hari Banned</label>
					      <input type="text" class="form-control" placeholder="Jumlah Hari Banned" name="days_banned" autocomplete="off">
					    </div>
					    
					  </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
            <button id="btn-save" class="btn btn-primary" type="submit">Simpan</button>
          </div>
          </form>
        </div>
      </div>
    </div>

<script src="${pageContext.request.contextPath}/js/pendaki.js"></script>
<%@ include file = "inc/footer.jsp" %>
    
<script>$('.sticky-footer').addClass("d-none")</script>