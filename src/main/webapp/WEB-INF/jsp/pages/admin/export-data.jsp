<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="java.util.Date"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransaction"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Currency"%>
<%@page import="id.co.hijr.ticket.mapper.BankMapper"%>
<%@page import="id.co.hijr.ticket.model.Bank"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.HashSet"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

//init the mapper
PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
PurchaseDetilTransactionMapper purchaseDetilTransactionMapper = appCtx.getBean(PurchaseDetilTransactionMapper.class);
PurchaseDetilVisitorMapper purchaseDetilVisitorMapper = appCtx.getBean(PurchaseDetilVisitorMapper.class);
BankMapper bankMapper = appCtx.getBean(BankMapper.class);
//end init mapper

//start add paremeter
String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
//end add parameter

//run this to know the status page if error
System.out.print("OIKAN");

PurchaseDetilTransaction purchaseDetilTransaction = purchaseDetilTransactionMapper.getEntityHeader(id);
Purchase purchase= purchaseMapper.getEntity(id);
String purchasedetilvisitor = ",";
List<String> arraypurchasedetilvisitor = new ArrayList<String> ();
for(PurchaseDetilVisitor p : purchaseDetilVisitorMapper.getEntityHeader(id)){
	//if(p.getResponsible().equals("0")) arraypurchasedetilvisitor.add(p.getFullName());
}
QueryParameter param =  new QueryParameter();
if(request.getParameter("start_date")!=null && request.getParameter("end_date")!=null){
	param.setClause(param.getClause()+" AND "+Purchase.START_DATE+" between "+request.getParameter("start_date")+" AND "+request.getParameter("end_date"));
}
if(request.getParameter("filter_status")!=null){
	param.setClause(param.getClause()+" AND "+Purchase.STATUS+" = '"+request.getParameter("filter_status")+"'");
}
param.setOrder("datetime_purchase desc");
List<Purchase> listPurchase= purchaseMapper.getList(param);

//for format monney
NumberFormat formatter = NumberFormat.getCurrencyInstance();
Locale myIndonesianLocale = new Locale("in", "ID");
formatter.setCurrency(Currency.getInstance(myIndonesianLocale));


//for format date and time
SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy");
SimpleDateFormat sdftime = new SimpleDateFormat("hh.mm aa");
SimpleDateFormat sdftimeleft = new SimpleDateFormat("hh:mm:ss");
SimpleDateFormat sdf_MY = new SimpleDateFormat("MMMM yyyy");
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	
    <link href="${pageContext.request.contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<style>
		.bg-holder {
		    position: absolute;
		    width: 100%;
		    min-height: 100%;
		    top: 0;
		    left: 0;
		    background-size: cover;
		    background-position: center;
		    overflow: hidden;
		    will-change: transform,opacity,filter;
		    -webkit-backface-visibility: hidden;
		    backface-visibility: hidden;
		    background-repeat: no-repeat;
		    z-index: 0;
		}
		
		.bg-card {
		    background-size: contain;
		    background-position: right;
		    border-top-right-radius: .375rem;
		    border-bottom-right-radius: .375rem;
		}
		.badge-success {
		    color: #fff;
		    background-color: #28a745;
		}
		.badge-danger {
		    color: #fff;
		    background-color: #dc3545;
		}
		.badge-warning {
		    color: #212529;
		    background-color: #ffc107;
		}
		.badge-info {
		    color: #fff;
		    background-color: #17a2b8;
		}
	</style>
    <title>Laporan</title>
  </head>
  <body>
  <div class="container">
   <div class="card mb-3">
      <div class="bg-holder d-none d-lg-block bg-card" style="background-image:url('https://prium.github.io/falcon/v2.8.0/default/assets/img/illustrations/corner-4.png');opacity: 0.7;"></div>
      <!--/.bg-holder-->
      <div class="card-body">
         <h5>Laporan</h5>
         <p class="d-none fs--1">BOOKING ID: #2737<br>April 21, 2019, 5:33 PM</p>
         <div class="d-none">
            <strong class="mr-2">Status: </strong>
            <div class="badge badge-pill badge-soft-success fs--2">Completed <span class="fa fa-check ml-1" data-fa-transform="shrink-2"></span></div>
         </div>
      </div>
   </div>
  <%--  <div class="card mb-3">
      <div class="card-body">
         <div class="row">
            <div class="col-4">
               <h6 class="mb-2">BOOKING ID</h6>
               <h5 class="mb-3 fs-0 text-info"><% out.print("purchase.getId()"); %></h5>
            </div>
            <div class="col-4">
               <h5 class="fs-0 mb-3 "><% out.print("purchase.getRegion().getName()"); %></h5>
               <h6 class="mb-2">Selected Visit Date</h6>
               <p class="mb-0 fs--1 text-muted"><% out.print("sdf.format(purchase.getStartDateSerialize())"); %></p>
            </div>
            <div class="col-4">
               <h5 class="mb-3 fs-0"></h5>
               <div class="media">
                  <img class="mr-3" src="../assets/img/icons/visa.jpg" width="40" alt="">
                  <div class="media-body">
                     <h6 class="mb-2">Until Visit Date</h6>
                     <p class="mb-0 fs--1 text-muted"><% out.print("sdf.format(purchase.getEndDateSerialize())"); %></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="card mb-3">
      <div class="card-body">
         <div class="row">
            <div class="col-4">
               <h5 class="fs-0 mb-3 ">BOOKING DETAILS</h5>
               <h6 class="mb-2">Jumlah Tiket</h6>
               <p class="mb-0 fs--1">Fasilitas</p>
               <p class="mb-0 fs--1">Keterangan Tiket</p>
            </div>
            <div class="col-4">
               <h5 class="mb-3 fs-0"></h5>
               <div class="media">
                  <img class="mr-3" src="../assets/img/icons/visa.jpg" width="40" alt="">
                  <div class="media-body">
                     <h6 class="mb-2"><% out.print("purchase.getCountTicket()"); %></h6>
                     <p class="mb-0 fs--1">Mobil, Tour Guide, dll</p>
                     <p class="mb-0 fs--1"> <strong>Untuk :</strong> <% out.print("arraypurchasedetilvisitor"); %></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="card mb-3">
      <div class="card-body">
         <div class="row">
            <div class="col-3">
               <p class="mb-0 fs--1 text-muted">Costumer Service</p>
               <h6 class="mb-2">+62 21 1500 308</h6>
            </div>
            <div class="col-3">
               <p class="mb-0 fs--1 text-muted">Costumer Service Email</p>
               <h6 class="mb-2">cs@kawasan.email.com</h6>
            </div>
            <div class="col-3">
               <p class="mb-0 fs--1 text-muted">Order Number</p>
               <h6 class="mb-2">211500308</h6>
            </div>
            <div class="col-3 border text-center">
               <p class="mb-0 fs--1 text-muted">Kawasan</p>
               <h6 class="mb-2">Ticket Lorem</h6>
            </div>
         </div>
      </div>
   </div> --%>
   
   <div class="card mb-3">
      <div class="card-body">
         <div class="row">
            <div class="col-12">
            
               <!-- <table class="table table-responsive table-striped table-hover m-0" style="opacity: 0.8;overflow: hidden;display:inline-table">
				<thead class="bg-head text-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Code</th>
						<th scope="col">Customer</th>
						<th scope="col">Email</th>
						<th scope="col">Region</th>
						<th scope="col">Status</th>
						<th scope="col" class="text-right">Amount</th>
					</tr>
				</thead>
				</table> -->
               <table id="tbl-data" class="table table-responsive table-striped table-hover m-0" style="opacity: 0.8;overflow: hidden;display:inline-table">
				<!-- <thead class="bg-head text-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Code</th>
						<th scope="col">Customer</th>
						<th scope="col">Email</th>
						<th scope="col">Region</th>
						<th scope="col">Status</th>
						<th scope="col" class="text-right">Amount</th>
					</tr>
				</thead> -->
               		<tbody class="bg-light text-dark">
               		<% int i=0; int totalPrice=0; 
               			for(Purchase o : listPurchase){
               				%>
               				<tr class="data-row" id="row-1612194202665882">
							   <td><% out.print(i+1); %></td>
							   <td><% out.print(o.getId()); %></td>
							   <%
							   o.setPurchaseDetilVisitor(purchaseDetilVisitorMapper.getEntityHeader(o.getId()));
							   	if(o.getPurchaseDetilVisitor().size() > 0){
							  	%>
							  		<td><% out.print(o.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName()); %></td>
							  		<td><% out.print(o.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getEmail()); %></td>
							  	<%
							   	}else{
							   	%>
								   	<td><% out.print("NA"); %></td>
								   	<td><% out.print("NA"); %></td>
							   <%
							   	}
							   %>
							   <td><% out.print(o.getRegion().getName()); %></td>
							   <% 
							   	String statusName = o.getStatusName().split(",")[0];
							   	String statusColor = o.getStatusName().split(",")[1];
							   	String statusIcon = o.getStatusName().split(",")[2];

							   	if(statusName.equals("WAITING")) statusName="Waiting for Payment";
							   	if(statusName.equals("VERIFYING")) statusName="Verifying Payment";
							   %>
							   	<td class=""><span class="badge badge-pill badge-<% out.print(statusColor); %> text-white"><% out.print(statusName); %> <i class="fas fa-<% out.print(statusIcon); %>"></i></span>
									</br><% out.print(sdf.format(o.getDatetimeSerialize())); %>
								</td>
							   <td class="text-right"><% out.print(formatter.format(o.getTotalAmount())); %></td>
							</tr>
               				<%
               				i++;
               				totalPrice+=o.getTotalAmount();
               						
               			}
               		%>
               		<!-- <tr class="data-row" id="row-1612194202665882">
					   <td>1</td>
					   <td>1612194202665882</td>
					   <td class=""> Fulana </td>
					   <td class=""> fulana@email.com </td>
					   <td class=""> Wisata Gunung Bromo </td>
					   <td class=""><span class="badge badge-pill badge-success text-white">PAYMENT <i class="fas fa-check-circle"></i></span><br>01-02-2021 22:57:00</td>
					   <td class="text-right"> 30,224 </td>
					</tr>
					<tr class="data-row" id="row-1612188464410411">
					   <td>2</td>
					   <td>1612188464410411</td>
					   <td class=""> Randi </td>
					   <td class=""> randi@yahoo.co.id </td>
					   <td class=""> Wisata Gunung Bromo </td>
					   <td class=""><span class="badge badge-pill badge-danger text-white">CANCEL <i class="fas fa-ban"></i></span><br>01-02-2021 21:08:00</td>
					   <td class="text-right"> 728 </td>
					</tr> -->
					
               		</tbody>
               		<tfoot class="bg-head text-dark">
					<tr>
						<th class="text-center" colspan="6">TOTAL</th>
						<th scope="col" class="text-right"><% out.print(formatter.format(totalPrice)); %></th>
					</tr>
					</tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- Optional JavaScript; choose one of the two! -->
   <!-- Option 1: Bootstrap Bundle with Popper -->
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   <!-- Option 2: Separate Popper and Bootstrap JS -->
   <!--
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
      -->
</div>
</body>
</html>