<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%@ include file = "inc/navbar-head.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item active">Selamat Datang di Aplikasi PPS Online</li>
          </ol> -->
		
		<div class="row justify-content-center">
		<div class="col-md-6">
		<div class="card bg-head pill">
	    <div class="card-body">
	    <div class="card-title">
		<div class="row">
			<div class="col-md-6">
				<h4>Type Vehicle</h4>
			</div>
			<div class="col-md-6">
				<button id=btn-add data-toggle="modal" data-target="#modal-form" class="btn btn-light pill btn-lg pl-5 pr-5 float-right">Add <span class="badge badge-pill badge-success">New</span></button>
			</div>
		</div>
		</div>
		<div class="border-bottom mb-3"></div>
		<div class="row">
		<div class="col-md-12">
			<table id="tbl-data" class="table table-striped" style="opacity: 0.8;border-radius: 23px;overflow: hidden;">
				<thead class="bg-head text-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Type Name</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody class="bg-light text-dark">
				</tbody>
			</table>
		</div>
		</div>
	    </div>
		</div>
		</div>
		</div>

        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-md" role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
     		<form id="entry-form">
			  <div class="form-group">
			    <label for="">Type Name</label>
			    <input type="text" class="form-control" name="name" placeholder="">
			  </div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="save();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
	
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/type-vehicle.js"></script>

  </body>

</html>
