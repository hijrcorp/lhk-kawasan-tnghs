<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
<style>
	.form-control {
	    padding: 0.5rem;
	    border-radius: 10px;
    }
    
    <c:choose>
         <c:when test = "${fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_OPERATOR') && !fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_ADMIN')}">
         	.td-action {
			    display: none;
		    }
         </c:when>
   </c:choose>
   
</style>
  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%@ include file = "inc/navbar-head.jsp" %>
	
    <div id="wrapper">

      <div id="content-wrapper">

        <div class="container-fluid">
		
		<!-- <div class="row justify-content-center">
			<div class="col-md-6">
				<div class="card bg-heads">
				<div class="card-header">
				<div class="row">
					<div class="col-md-8">
						<h4>Tahunan</h4>
					</div>
					<div class="col-md-4">
					    <select class="form-control">
						  <option value="0">Pilih Tahun</option>
						</select>
					</div>
				</div>
				</div>
				
			    <div class="card-body p-2">
					<div class="row pt-3 p-4">
						
						<div class="col-sm-8 my-auto">
							<canvas id="chart-bar"></canvas>
						</div>
						<div class="col-sm-4 text-center my-auto">
							<div class="h4 mb-0 text-primary kn-text" id="kn-1">Rp. 0</div>
							<div class="small text-muted">Total KN</div>
							<hr>
							<div class="h4 mb-0 text-warning kn-text" id="kn-2">Rp. 0</div>
							<div class="small text-muted">KN Tindak Lanut</div>
							<hr>
							<div class="h4 mb-0 text-danger kn-text" id="kn-3">Rp. 0</div>
							<div class="small text-muted">KN Sisa</div>
						</div>
					</div>
			    </div>
			    
				</div>
			</div>
		</div>
			
		<hr/> -->
		
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="card bg-head">
				<div class="card-header">
				<div class="row">
					<div class="col-md-7">
						<h4>Tahunan</h4>
					</div>
					<div class="col-md-5">
					    <div class="input-group">
						    <select name="filter_year_s" class="form-control">
							  <option value="0">Pilih Mulai Tahun</option>
							</select>
							  <div class="input-group-append">
							    <button id="btn-search" class="btn btn-secondary" type="button">Sampai</button>
							  </div>
						    <select name="filter_year_e" class="form-control">
							  <option value="0">Pilih Akhir Tahun</option>
							</select>
						</div>
					</div>
				</div>
				</div>
				
			    <div class="card-body p-2 bg-light">
					<div class="row pt-3 p-4">
						<div class="col-sm-12 my-auto">
							<canvas id="chart-tahunan"></canvas>
						</div>
					</div>
			    </div>
			    
				</div>
			</div>
			</div>
			
		<hr/>
			
		<div class="row justify-content-center">
			
			<div class="col-md-10">
				<div class="card bg-head ">
				<div class="card-header">
				<div class="row">
					<div class="col-md-8">
						<h4>Bulanan</h4>
					</div>
					<div class="col-md-4">
					    <select name="filter_year" class="form-control">
						  <option value="0">Pilih Tahun</option>
						</select>
					</div>
				</div>
				</div>
				
			    <div class="card-body p-2 bg-light">
					<div class="row pt-3 p-4">
						<div class="col-sm-12 my-auto">
							<canvas id="chart-month"></canvas>
						</div>
					</div>
			    </div>
			    
				</div>
			</div>
		</div>
		
		<hr/>
		
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="card bg-head ">
				<div class="card-header">
				<div class="row">
					<div class="col-md-8">
						<h4>Harian</h4>
					</div>
					<div class="col-md-4">
					    <div class="input-group">
						  <input type="date" class="form-control" name="filter_start" placeholder="Start Date">
						  <div class="input-group-append">
						    <button id="btn-search" class="btn btn-secondary" type="button">Sampai</button>
						  </div>
						  <input type="date" class="form-control" name="filter_end" placeholder="End Date">
						</div>
					</div>
				</div>
				</div>
				
			    <div class="card-body p-2 bg-light">
					<div class="row pt-3 p-4">
						<div class="col-sm-12 my-auto">
							<canvas id="chart-harian"></canvas>
						</div>
					</div>
			    </div>
			    
				</div>
			</div>
		</div>

        </div>
      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
	 
    <%@ include file = "inc/footer.jsp" %>
    <script type="text/javascript">
	    window.chartColors = {
	    	red: 'rgb(255, 99, 132)',
	    	orange: 'rgb(255, 159, 64)',
	    	yellow: 'rgb(255, 205, 86)',
	    	green: 'rgb(75, 192, 192)',
	    	blue: 'rgb(54, 162, 235)',
	    	purple: 'rgb(153, 102, 255)',
	    	grey: 'rgb(201, 203, 207)'
	    };
	    var color = Chart.helpers.color;
	
	    	
	    var arrPropDataSet = [
	    	{
	    		backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.blue,
	    		borderWidth: 1,
	    	},
	    	{
	    		backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.green,
	    		borderWidth: 1,
	    	},
	    	{
	    		backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.blue,
	    		borderWidth: 1,
	    	},
	    	{
	    		backgroundColor: color(window.chartColors.purple).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.blue,
	    		borderWidth: 1,
	    	},
	    	{
	    		backgroundColor: color(window.chartColors.grey).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.green,
	    		borderWidth: 1,
	    	},
	    	{
	    		backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.blue,
	    		borderWidth: 1,
	    	},
	    	{
	    		backgroundColor: color(window.chartColors.orange).alpha(0.5).rgbString(),
	    		borderColor: window.chartColors.green,
	    		borderWidth: 1,
	    	},
	    ];
	
	    (function(global) {
	
	    	var COLORS = [
	    		'#4dc9f6',
	    		'#f67019',
	    		'#f53794',
	    		'#537bc4',
	    		'#acc236',
	    		'#166a8f',
	    		'#00a950',
	    		'#58595b',
	    		'#8549ba'
	    	];
	
	    	var Samples = global.Samples || (global.Samples = {});
	    	var Color = global.Color;
	
	    	Samples.utils = {
	    		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
	    		srand: function(seed) {
	    			this._seed = seed;
	    		},
	
	    		rand: function(min, max) {
	    			var seed = this._seed;
	    			min = min === undefined ? 0 : min;
	    			max = max === undefined ? 1 : max;
	    			this._seed = (seed * 9301 + 49297) % 233280;
	    			return min + (this._seed / 233280) * (max - min);
	    		},
	
	    		numbers: function(config) {
	    			var cfg = config || {};
	    			var min = cfg.min || 0;
	    			var max = cfg.max || 1;
	    			var from = cfg.from || [];
	    			var count = cfg.count || 8;
	    			var decimals = cfg.decimals || 8;
	    			var continuity = cfg.continuity || 1;
	    			var dfactor = Math.pow(10, decimals) || 0;
	    			var data = [];
	    			var i, value;
	
	    			for (i = 0; i < count; ++i) {
	    				value = (from[i] || 0) + this.rand(min, max);
	    				if (this.rand() <= continuity) {
	    					data.push(Math.round(dfactor * value) / dfactor);
	    				} else {
	    					data.push(null);
	    				}
	    			}
	
	    			return data;
	    		},
	
	    		labels: function(config) {
	    			var cfg = config || {};
	    			var min = cfg.min || 0;
	    			var max = cfg.max || 100;
	    			var count = cfg.count || 8;
	    			var step = (max - min) / count;
	    			var decimals = cfg.decimals || 8;
	    			var dfactor = Math.pow(10, decimals) || 0;
	    			var prefix = cfg.prefix || '';
	    			var values = [];
	    			var i;
	
	    			for (i = min; i < max; i += step) {
	    				values.push(prefix + Math.round(dfactor * i) / dfactor);
	    			}
	
	    			return values;
	    		},
	
	    		color: function(index) {
	    			return COLORS[index % COLORS.length];
	    		},
	
	    		transparentize: function(color, opacity) {
	    			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
	    			return Color(color).alpha(alpha).rgbString();
	    		}
	    	};
	
	    }(this));
    </script>
    <script src="${pageContext.request.contextPath}/js/report.js"></script>

  </body>

</html>
