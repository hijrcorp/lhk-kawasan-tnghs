<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%@ include file = "inc/navbar-head.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item active">Selamat Datang di Aplikasi PPS Online</li>
          </ol> -->
		
		<div class="row justify-content-center">
		<div class="col-md-9">
		<div class="card bg-head pill">
	    <div class="card-body">
	    <div class="card-title">
		<div class="row">
			<div class="col-md-6">
				<h4>Vehicle</h4>
			</div>
			<div class="col-md-6">
				<button id=btn-add data-toggle="modal" data-target="#modal-form" class="btn btn-light pill pl-3 pr-3 float-right"><span class="fas fa-plus"></span> New</button>
			</div>
		</div>
		</div>
		<div class="border-bottom mb-3"></div>
		<div class="row">
		<!-- <div class="col-md-3">
			<table id="tbl-data-category" class="table table-striped table-hover" style="opacity: 0.8;border-radius: 23px;overflow: hidden;">
				<thead class="bg-head text-dark">
					<tr>
						<th scope="col"></th>
						<th scope="col">Category Vehicle</th>
						<th scope="col">
							<a href="javascript:void(0)" class="btn btn-sm btn-square btn-secondary pill float-right" data-toggle="modal" data-target="#modal-form-category" ><i class="fas fa-fw fa-plus"></i></a>
						</th>
					</tr>
				</thead>
				<tbody class="bg-light text-dark">
				</tbody>
			</table>
			<span>*please select one </span>
		</div> -->
		<div class="col-md-12">
			<table id="tbl-data" class="table table-striped" style="opacity: 0.8;border-radius: 23px;overflow: hidden;">
				<thead class="bg-head text-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Vehicle Name</th>
						<th scope="col">Vehicle Type</th>
						<!-- <th scope="col">Photo</th> -->
						<th scope="col">Seat Capacity</th>
						<th scope="col">Suitcase</th>
						<th scope="col">Cost/Day</th>
						<th scope="col">Provided</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody class="bg-light text-dark">
				</tbody>
			</table>
			<!-- <span>*to show list table, please select category </span> -->
		</div>
		</div>
	    </div>
		</div>
		</div>
		</div>

        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
     		<form id="entry-form">
		    <div class="row">
			  <div class="col-md-7 form-group">
			    <label for="">Vehicle Name</label>
			    <input type="text" class="form-control" name="name" placeholder="Ex : Toyota Fortuner">
			  </div>
			  <div class="col-md-5 form-group">
			    <label for="">Vehicle Type</label>
                <select name="type_id" class="form-control select-style">
                    <option value="">Select Type</option>
                </select>
			  </div>
		  	</div>
		  	
		    <div class="row">
			  <div class="col form-group">
			    <label for="">Seat Capacity</label>
			    <input type="text" class="form-control" name="seat_capacity" placeholder="Ex: 4">
			  </div>
			  <div class="col form-group">
			    <label for="">Suitcase</label>
			    <input type="text" class="form-control" name="suitcase" placeholder="Ex: 6">
			  </div>
			  <div class="col form-group">
			    <label for="">Cost/Day</label>
			    <input type="text" class="form-control" name="cost" placeholder="Cost/Day">
			  </div>
		  	</div>
		  	
		    <div class="row">
			  <div class="form-group col">
			    <label for="">Provided By</label>
			    <input type="text" class="form-control" name="provided" placeholder="Provided by">
			  </div>
			  <div class="form-group col">
			    <label for="">Photo</label>
			    <button type="button" onclick="$([name=photo]).click()" class="btn btn-lgs bg-head ml-2 btn-block">Click to upload photo</button>
			    <input type="file" name="photo" class="d-none">
			  </div>
			  
		    	<!-- <div class="col-md-6 form-group">
			    <label for="">Photo</label>
				<div class="custom-file">
				  <input type="file" class="custom-file-input" id="customFile" name="photo">
				  <label class="custom-file-label" for="customFile">Choose Photo FIle</label>
				</div>
				</div> -->
			</div>
			  
			  <div class="form-group">
			    <label for="">Vehicle Description(Optional)</label>
			    <textarea class="form-control" name="description" rows="4"></textarea>
			  </div>
			  <!-- <div class="form-group form-check">
			    <input type="checkbox" class="form-check-input" id="exampleCheck1">
			    <label class="form-check-label" for="exampleCheck1">Check me out</label>
			  </div>
			  <button type="submit" class="btn btn-primary">Submit</button> -->
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="save();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal-form-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-category-msg" class="alert alert-danger" role="alert"></div>
     		<form id="entry-form-category">
			  <div class="form-group">
			    <label for="">Type Name</label>
			    <input type="text" class="form-control" name="name" placeholder="">
			  </div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="saveCategory();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal-form-upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">UPLOAD PHOTO</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-upload-msg" class="alert alert-danger" role="alert"></div>
				<form id="entry-form-upload" >
				  <div class="form-group">
				    <label for="">Vehicle Photo</label>
				    <div class="row">
				    	<div class="col-md-12">
						<div class="custom-file">
						  <input type="file" class="custom-file-input" id="customFile" name="file">
						  <label class="custom-file-label" for="customFile">Choose file</label>
						</div>
						</div>
					</div>
				  </div>
				</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="saveUpload();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/vehicle.js"></script>

  </body>

</html>
