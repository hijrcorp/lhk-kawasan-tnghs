    <div class="container-fluid bg-head p-3 d-none d-md-block">
		<div class="media">
		  <img style="width:100px" src="${contextPathPublic}/images/logo-tnghs.png" class="align-self-start mr-3" alt="...">
		  <div class="media-body">
		    <h3 class="mt-0">Aplikasi Pemesanan Tiket Online</h3>
		    <p>Aplikasi Kawasan adalah aplikasi pemesanan tiket online <br/> masuk kawasan wisata.</p>
	      </div>
	      <div class="pr-5 pt-3 pb-3">
	      <a href="${pageContext.request.contextPath}/page/admin/" class="mr-4" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-home fa-4x"></i> Home</a>
	      <a href="${pageContext.request.contextPath}/page/general" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-globe fa-4x"></i> Website</a>
	      </div>
		</div>
	</div>
	
    <div class="container-fluid bg-head p-3 d-block d-md-none">
		<div class="row d-flex justify-content-between">
		<div class="col-4">
			<img style="width:100px" src="${contextPathPublic}/images/logo-tnghs.png" class="align-self-start mr-3" alt="...">
		</div>
	      <div class="col-8 text-right">
		      <a href="${pageContext.request.contextPath}/page/admin/" class="mr-4" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-home fa-4x"></i> Home</a>
		      <a href="${pageContext.request.contextPath}/page/general" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-globe fa-4x"></i> Website</a>
	      </div>
		</div>
		<div class="row">
			<div class="col-12">
			    <h3 class="mt-0">Aplikasi Pemesanan Tiket Online</h3>
			    <p>Aplikasi Kawasan adalah aplikasi pemesanan tiket online <br/> masuk kawasan wisata.</p>
		     </div>
		</div>
	</div>