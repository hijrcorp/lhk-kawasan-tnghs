
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tiket Kawasan - Kementerian Lingkungan Hidup dan Kehutanan</title>

    <link rel="icon" type="image/png" sizes="32x32" href="${contextPathPublic}/images/logo-tnghs.png">

    <!-- Bootstrap core CSS-->
    <%-- <link href="${contextPathPublic}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --%>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom fonts for this template-->
    <link href="${contextPathPublic}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="${contextPathPublic}/vendor/select2.min.css" rel="stylesheet">
	<link href="${contextPathPublic}/vendor/daterangepicker.css" rel="stylesheet"> 
	<link href="${contextPathPublic}/vendor/ekko-lightbox.css" rel="stylesheet"> 
	
	<link rel="stylesheet" href="${contextPathPublic}/vendor/remodal/remodal.css">
	<link rel="stylesheet" href="${contextPathPublic}/vendor/remodal/remodal-default-theme.css">
	
	<!-- Custom styles for this template-->
    <link href="${contextPathPublic}/css/sb-admin.css" rel="stylesheet">
	
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<script>var localeCookieName = "${localeCookieName}"</script>
	<script>var position = "${roleList}"</script>
	<style>
	
      html, body {
        background-image: linear-gradient(to bottom right, rgb(2, 124, 161), rgb(149, 169, 18));
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100%;
        background-attachment: fixed;
      }
		.bg-head {
			background-color: rgba(170, 192, 171, 0.5);
			color: white;
		}
		.bg-gradient {
			background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);
			height: 100%;
			color: white;
		}
		thead.bg-head {
			background-color: #0f8094!important;
		    color: white!important;
	    }
		.pill {
			border-radius: 26px;
		}
		.btn-light {
		    border-color: transparent!important;
		}
		.form-control {
			height: 45px;
			border: none;
			background: #f7f7f7;
			width: 100%;
			padding: 1rem;
			border-radius: 23px;
			color: #333;
		}
		.bg-orange {
			background: var(--orange);
			color: white;
		}
		.text-orange {
			color: var(--orange);
		}
		.card-img-top {
			height: 15vw;
		   	object-fit: cover;
		}
		
		/*custome table*/
		.rows-head {
			opacity: 0.8;
		    overflow: hidden;
		    background-color: #0f8094!important;
		    color: white!important;
		    padding: .75rem;
		}
		.rows-body {
			opacity: 0.8;
		    overflow: hidden;
		    background-color: #f8f9fa!important;
		    padding: .75rem;
		}
		.card-header-2 {
	    	padding: 0 1rem;
	    }
	    
	    .col-cos {
   	        padding-left: 15px;
   	        margin: 10px;
	    }
	</style>
	
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 425px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        background-color: #fff;
        border-radius: 2px;
        border: 1px solid transparent;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        box-sizing: border-box;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        height: 29px;
        margin-left: 17px;
        margin-top: 10px;
        outline: none;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      .controls:focus {
        border-color: #4d90fe;
      }
      .title {
        font-weight: bold;
      }
      #infowindow-content {
        display: none;
      }
      #map #infowindow-content {
        display: inline;
      }
      
	@media (min-width: 576px){
		.container-fluid-sm{
			width: 100%!important;
		}
	}
	@media (min-width:768px){
		.container-fluid-md{
			width: 70%!important;
		}
		.d-md-inline-grid{display:inline-grid!important}
		.btn-md-light {
			border-color: transparent!important;
		}
		.bg-md-head {
			background-color: rgba(170, 192, 171, 0.5);
    		color: white!important;
		}
		/* .d-md-inline-block{display:inline-block!important}
		.d-md-block{display:block!important}
		.d-md-table{display:table!important}
		.d-md-table-row{display:table-row!important}
		.d-md-table-cell{display:table-cell!important}
		.d-md-flex{display:-ms-flexbox!important;display:flex!important}
		.d-md-inline-flex{display:-ms-inline-flexbox!important;display:inline-flex!important} */
	}
	
    </style>
  </head>
    <c:choose>
         <c:when test = "${!fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN') && !fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_OPERATOR')  && !fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_SUPERVISOR')}">
         	<c:redirect url="/page/general"/>
         </c:when>
   </c:choose>