<!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Confirmation Modal-->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-confirm-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
            <button class="btn btn-primary" id="btn-modal-confirm-yes" type="button">Yes</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Alert Modal-->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-success">
            <h5 class="modal-title" id="exampleModalLabel">Alert</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-alert-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Error Modal-->
    <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header alert-danger">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">
          <p id="modal-error-msg"></p></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="${contextPathPublic}/vendor/jquery/jquery.min.js"></script>
    <script src="${contextPathPublic}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="${contextPathPublic}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="${contextPathPublic}/vendor/jquery.disableAutoFill.min.js"></script>
    <script src="${contextPathPublic}/vendor/js.cookie.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="${contextPathPublic}/vendor/chart.js/Chart.min.js"></script>
    <script src="${contextPathPublic}/vendor/moment.min.js"></script>
    <script src="${contextPathPublic}/vendor/daterangepicker.js"></script>
	<script src="${contextPathPublic}/vendor/select2.min.js"></script>
	<script src="${contextPathPublic}/vendor/ekko-lightbox.min.js"></script>
	<script src="${contextPathPublic}/vendor/loadingoverlay.min.js"></script>
	<script src="${contextPathPublic}/vendor/remodal/remodal.js"></script>
	<script src="${contextPathPublic}/vendor/bootstrap-input-spinner-master/bootstrap-input-spinner.js"></script>
	
	<!-- Custom scripts for all pages-->
    <script src="${pageContext.request.contextPath}/js/sb-admin.min.js"></script>
    <script src="${contextPathPublic}/vendor/jquery/jquery.number.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/common-general.js"></script>