<div class="container-fluid bg-head d-none d-md-block">
	<div class="media p-5">
	  	<img style="width:100px" src="${pageContext.request.contextPath}/images/logo-tnghs.png" class="align-self-start mr-3" alt="...">
	 	<div class="media-body">
			<h3 class="mt-0">Aplikasi Pemesanan Tiket Online</h3>
			<h4 class="mt-0">Balai Taman Nasional Gunung Halimun Salak</h4>
			<p>Aplikasi kawasan adalah aplikasi online yang dapat membantu pembelian tiket wisata masuk kawasan.</p>
	    </div>
	    <div class="pr-5 pt-3 pb-3">
	    	<a href="${pageContext.request.contextPath}/page/admin/" class="mr-4" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-home fa-4x"></i> Home</a>
			<a href="${pageContext.request.contextPath}/page/" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-globe fa-4x"></i> Website</a>
	    	<a href="${pageContext.request.contextPath}/login-logout" class="mr-4 d-none" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-exit fa-4x"></i> Logout</a>
		</div>
	</div>
	</div>
	
    <div class="d-md-container-fluid bg-head d-block d-md-none p-4">
		<div class="row d-flex justify-content-between">
		<div class="col-4">
			<img style="width:100px" src="${pageContext.request.contextPath}/images/logo-tnghs.png" class="align-self-start mr-3" alt="...">
		</div>
	      <div class="col-8 text-right">
		      <a href="${pageContext.request.contextPath}/page/admin/" class="mr-4" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-home fa-4x"></i> Home</a>
		      <a href="${pageContext.request.contextPath}/page/general" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;"><i class="fas fa-globe fa-4x"></i> Website</a>
	      </div>
		</div>
		<div class="row">
			<div class="col-12 text-cente">
			    <h3 class="mt-3 mb-3">Aplikasi Pemesanan Tiket Online</h3>
			    <p class="lead">Aplikasi Kawasan adalah aplikasi pemesanan tiket online <br/>dengan  dengan fokus masuk kawasan wisata.</p>
		     </div>
		</div>
	</div>