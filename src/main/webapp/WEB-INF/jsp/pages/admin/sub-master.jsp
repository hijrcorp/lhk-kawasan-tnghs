<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%@ include file = "inc/temp-header.jsp" %>
 
	
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 
		
      <div id="content-wrapper">
		
        <div class="container-fluid container-fluid-md container-fluid-sm">

		<div class="container pl-5 pt-5 pb-3 pr-5">
	   <% if(request.getParameter("mode")!=null) {%>
		<div class="row">
			<div class="col-md-4 pb-3">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/pengguna" class="mr-4s" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-users fa-4x ml-5 mr-5 mb-4" style="font-size: 7rem;"></i> 
					<button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Pengguna/Akun</button>
					</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/configure" class="mr-4s" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fa fa-cog fa-4x ml-5 mr-5 mb-4" style="font-size: 7rem;"></i> 
					<button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Configure</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 pb-3">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/pendaki" class="mr-4s" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fa fa-user fa-4x ml-5 mr-5 mb-4" style="font-size: 7rem;"></i> 
					<button type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5">Pendaki</button>
					</a>
				</div>
			</div>
		</div>
	   <% }else{ %>
		<div class="row">
			<div class="col-md-4 col-6 pb-3">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/bank" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-university fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Bank</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 col-6 pb-3">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/transit-camp" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fa fa-road fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Transit Camp</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 col-6 pb-3">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/luggage" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-briefcase fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Luggage</button>
					</a>
				</div>
			</div>
			
			<!-- new -->
			<div class="col-md-4 d-none">
				<div class="text-center">
					<div class="mr-4s" style="display:inline-grid;text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-car-alt fa-4x ml-5 mr-5 mb-4" style="font-size: 7rem;"></i> 
					<button id="btnGroupDrop1" type="button" class="btn btn-light btn-lg bg-head pill pl-5 pr-5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Vehicle</button>
					
					<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(70px, 133px, 0px);">
					  <a class="dropdown-item" href="${pageContext.request.contextPath}/page/admin/vehicle">Master Vehicle</a>
					  <a class="dropdown-item" href="${pageContext.request.contextPath}/page/admin/type-vehicle">Type Vehicle</a>
					</div>
					</div>
					
				</div>
			</div>
			<div class="col-md-4 col-6 pt-md-5">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/type-region" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-map-marked-alt fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Type Region</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 col-6 pt-md-5">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/agenda" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-calendar fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Agenda Calendar</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 col-6 pt-md-5">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/configure-kuota" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-calendar fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Kuota</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 col-6 pt-md-5">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/master-payment" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-university fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block text-white">Master Payment</button>
					</a>
				</div>
			</div>
			<div class="col-md-4 d-none">
				<div class="text-center">
					<a href="${pageContext.request.contextPath}/page/admin/tour-guide" class="d-md-inline-grid" style="text-decoration:none;text-align:center;color:white!important;">
					<i class="fas fa-users fa-4x mb-4"></i> 
					<button type="button" class="btn btn-md-light btn-lg bg-md-head pill pl-md-5 pr-md-5 btn-block">Tour Guide</button>
					</a>
				</div>
			</div>
		</div>
		
		<% } %>
		</div>
		
		
		<div class="row py-3">
		<div class="col-md-6">
       </div>
       </div>

        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/dashboard.js"></script>

  </body>

</html>
