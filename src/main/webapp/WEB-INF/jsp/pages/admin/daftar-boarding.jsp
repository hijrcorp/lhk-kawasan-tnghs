<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>
<style>
	.form-control {
	    padding: 0.5rem;
	    border-radius: 10px;
    }
</style>
  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%@ include file = "inc/navbar-head.jsp" %>
	
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item active">Selamat Datang di Aplikasi PPS Online</li>
          </ol> -->
		
		<div class="row justify-content-center">
		<div class="col-md-10">
		<div class="card bg-head ">
		<div class="card-header">
		<div class="row">
			<div class="col-6">
				<h4>Boarding</h4>
			</div>
			<div class="col-6">
				<button data-toggle="modal" data-target="#modal-form" class="btn btn-light pill pl-4 pr-4 float-right"><i class="fas fa-filter"></i> Filter</button>
				<a id="a-export" target="_blank" href="export-data" class="btn btn-light pill pl-4 pr-4 float-right mr-1 d-none"><i class="fas fa-file"></i> Download</a>
			</div>
		</div>
		</div>
		
	    <div class="card-body p-2">
	    
	    <div class="row no-gutters ">
			<div class="col-md-6">
			    <div class="input-group">
				  <input type="text" class="form-control" name="filter_keyword" placeholder="Ketik Kode/ID/NO Purchase/Booking untuk mencari data..">
				  <div class="input-group-append">
				    <button id="btn-search" class="btn btn-secondary" type="button">Cari</button>
				  </div>
				</div>
			</div>
		</div>
		<div class="row pt-3">
		<div class="col-md-12">
			<div class="table-responsive">
			<table id="tbl-data" class="table table-striped table-hover m-0" style="opacity: 0.8;overflow: hidden;display:inline-table">
				<thead class="bg-head text-dark">
					<tr>
						<th scope="col" class="align-middle">#</th>
						<th scope="col" class="align-middle">Nomor Purchase</th>
						<th scope="col" class="align-middle">Kode Booking</th>
						<th scope="col" class="align-middle">Jalur Pendakian</th>
						<th scope="col" class="align-middle">Tanggal Naik</th>
						<th scope="col" class="align-middle">Tanggal Turun</th>
						<th scope="col" class="align-middle">Jumlah Pendaki</th>
						<th scope="col" class="align-middle">Status Boarding</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody class="bg-light text-dark">
				</tbody>
			</table>
			</div>
		</div>
		</div>
	    </div>
	    
		<div class="card-footer">
		<div class="row align-items-center d-none">
		    <div class="col">
		        <p class="mb-0 fs--1"><span class="d-none d-sm-inline-block mr-1">All Items - </span><a class="font-weight-semi-bold text-warning" href="#">view all <span class="fas fa-angle-right ml-1" data-fa-transform="down-1"></span></a></p>
		    </div>
		    <div class="col-auto">
		        <button class="btn btn-light btn-sm" type="button" disabled="disabled"><span>Previous</span></button>
		        <button class="btn btn-primary btn-sm px-4 ml-2" type="button" disabled="disabled"><span>Next</span></button>
		    </div>
		</div>
		</div>
		</div>
		</div>
		</div>

        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- stqrt modal view -->
	<div class="modal fade" id="modal-view-only" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
	   <div class="modal-content pill bg-gradient">
	      <form>
		      <div class="modal-header">
		        <h5 class="modal-title" id="">Purchase Details</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
	         <div class="modal-body">
	            <div id="modal-form-msg" class="alert alert-danger" role="alert" style="display: none;"></div>
	            <div class="row">
	               <div class="col-4">
	                  <h6 class="mb-2">Jalur</h6>
	                  <p class="mb-2 fs--1">Hari, Tanggal Pendakian</p>
	                  <p class="mb-2 fs--1">Transit Camp</p>
	                  <p class="mb-2 fs--1">Jam Wajib Lapor di BC</p>
	                  <p class="mb-2 fs--1">Nomor Kapling</p>
	                  <p class="mb-2 fs--1">Jumlah Pendaki</p>
	                  <p class="mb-2 fs--1">Nama Ketua</p>
	                  <p class="mb-2 fs--1">Nama Anggota</p>
	                  <h6 class="mb-2">Jumlah Pembayaran</h6>
	                  <h6 class="mb-2">Kode Booking</h6>
	               </div>
	               <div class="col-8">
	                  <div class="media">
	                     <img class="mr-3" src="" width="40" alt="">
	                     <div class="media-body">
	                        <h6 class="mb-2">: -</h6>
	                        <h6 class="mb-2">: -</h6>
	                        <p class="mb-2 fs--1 ">: </p>
	                        <p class="mb-2 fs--1 ">: </p>
	                        <p class="mb-2 fs--1 ">: </p>
	                        <p class="mb-2 fs--1">: -</p>
	                        <p class="mb-2 fs--1 ">: -</p>
	                        <p class="mb-2 fs--1 ">: -</p>
	                        <p class="mb-2 fs--1 ">: -</p>
	                        <p class="mb-2 fs--1 ">: -</p>
	                     </div>
	                  </div>
	               </div>
	            </div>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	         </div>
	      </form>
	   </div>
	</div>
	</div>
	<!-- end modal view -->
	
	<!-- stqrt modal detail identity -->
	<div class="modal fade" id="modal-detil-identity" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
	   <div class="modal-content pill bg-gradient">
	      <form>
		      <div class="modal-header">
		        <h5 class="modal-title" id="">Detail Pendaki</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
	         <div class="modal-body p-0">
			   <div id="modal-form-msg" class="alert alert-danger" role="alert" style="display: none;"></div>
			   <div class="row">
			      <div class="col-md-12">
					<div class="table-responsive">
			         <table id="tbl-identity" class="table table-sm table-striped table-hover m-0" style="opacity: 0.8; overflow: auto; display: inline-table;">
			            <thead class="bg-head text-dark">
			               <tr>
							  <th class="align-middle" style="width: 5%;">#</th>
							  <th class="align-middle" style="width: 10%;">No.Identitas</th>
							  <th class="align-middle" style="width: 22%;">Nama Lengkap</th>
							  <th class="align-middle" style="width: 8%;">Gender</th>
							  <th class="align-middle" style="width: 30%;">Alamat</th>
							  <th class="align-middle" style="width: 10%;">No.HP</th>
							  <th class="align-middle" style="width: 5%;"></th>
							</tr>
			            </thead>
			            <tbody class="bg-light text-dark">
			               <!-- <tr class="data-row-identity" id="row-1616758228914846">
			                  <td>1</td>
			                  <td>1616758228914846</td>
			                  <td class=""> Kayum Munajir </td>
			                  <td class=""> Laki-Laki</td>
			                  <td class="">08219260548</td>
			                  <td class="text-center"><a href="javascript:void(0)" onclick="remodal_view('/kawasan/files/1616767392828216?filename=1.png&amp;download');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
			               </tr> -->
			            </tbody>
			         </table>
			         </div>
			      </div>
			   </div>
			</div>
	         <div class="modal-footer">
	            <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	         </div>
	      </form>
	   </div>
	</div>
	</div>
	<!-- end modal view -->
	
	<!-- modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-md" role="document">
	    <div class="modal-content pill bg-gradient">
     	<form id="form-filter">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Filter Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert" style="display: none;"></div>
			  <!-- <div class="form-row d-none">
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="text" class="form-control single-date-picker" id="start_date" name="start_date" placeholder="Check In"> 
                  </div>
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="text" class="form-control single-date-picker" id="end_date" name="end_date" placeholder="Check Out">
                  </div>
              </div> -->
              <div class="form-row ">
                  <div class="form-group col-md-6">
                      <select name="filter_id_region" class="form-control">
                      	<option value="">Jalur Pendakian</option>
                      </select> 
                  </div>
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="text" class="form-control single-date-picker" id="start_date" name="filter_tanggal_pendakian" placeholder="Tanggal Pendakian"> 
                  </div>
                  
              </div>
			  <div class="form-row">
                  <div class="form-group col-md-12">
                      <select name="filter_status_boarding" class="form-control">
                      	<option value="">PILIH STATUS BOARDING</option>
                      	<!-- <option value="WAITING">WAITING</option> -->
                      	<option value="0">BELUM_BOARDING</option>
                      	<option value="1">SEDANG_NAIK</option>
                      	<option value="2">SELESAI</option>
                      </select> 
                  </div>
              </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-secondary pill pl-5 pr-5">Filter</button>
	      </div>
		</form>
	    </div>
	  </div>
	</div>
	
	
	<div class="remodal" data-remodal-id="modalku" ><!--  style="padding: 0;min-width: 75%;" -->
	  <button data-remodal-action="close" class="remodal-close"></button>
	  <!-- <h1>Remodal</h1> -->
	  	<img id="view_photo" src="" class="img-fluid">
	 </div>
	 
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/daftar-boarding.js"></script>

  </body>

</html>
