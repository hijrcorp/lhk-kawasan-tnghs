
<link rel='stylesheet' type='text/css' href='<?php echo base_url('assets/application.css')?>'>
<style type="text/css">
	
	body {
		 font-family: "Georgia", serif;
		 text-align: -webkit-center;
	}
	TD {
	    font-family: Georgia,serif;
	    /*font-size: 8;*/
	    color: #000000;
	}

	#scissors {
	    height: 43px; /* image height */
	    width: 90%;
	    margin: auto auto;
	    background-image: url('http://i.stack.imgur.com/cXciH.png');
	    background-repeat: no-repeat;
	    background-position: right;
	    position: relative;
	}
	#scissors div {
	    position: relative;
	    top: 50%;
	    border-top: 3px dashed black;
	    margin-top: -3px;
	}
</style>
<!-- <php print_r($output); ?> -->
	<table style="margin-top: 20px;" width="650" border="0" cellpadding="3" cellspacing="3" bgcolor="white">
		<!-- <tr>
			<td align="center">
				<span class="Title">Rincian Pembayaran </span>
			</td>
		</tr>
		<tr height="5">
			<td></td>
		</tr> -->
		<tr>
			<td align="center">
				<table border="0" class="List" style="border-collapse: collapse">
					<tr>
						<td rowspan="16" align="center" style="border: 2px solid black;width: 85;"><img src="<?php echo base_url('assets/img/qrcode.png')?>" width:20px="" style="width: 80px;"></td>
						<td rowspan="16" align="center">&nbsp;</td>
						<td width="">NO.</td>
						<td width="" bgcolor="yellow" style="border-bottom: 1px solid black;"><?php echo $output['kode'];?></td>
						<td align="center" width="" style="border-bottom: 1px solid black;">/</td>
						<td width="" style="border-bottom: 1px solid black;">
							<b><?php echo $output['ref'];?></b>
						</td>
						<!-- <td width="30">&nbsp;</td> -->
						<td align="center" colspan="11" rowspan="2" style="font-size:18px"><b>KWITANSI</b></td>
					</tr>
					<tr>
						<td width="" colspan="4">&nbsp;</td>
						<!-- <td width="30">&nbsp;</td> -->
					</tr>
					<tr class="">
						<td colspan="4" style="width: 22%;">Telah terima dari</td>
						<td>:</td>
						<td colspan="8" style="border-bottom: 1px solid black; width: 32%;font-size: 14px" >
							<b><?php echo $output['nama'];?></b>
						</td>
						<td style="border-bottom: 1px solid black;width: 11%;font-size: 14px">Kelas :</td>
						<td colspan="3" style="border-bottom: 1px solid black;"><b><?php echo $output['kelas'];?></b></td>
					</tr>
					<tr>
						<td colspan="15">&nbsp;</td>
						<!-- <td width="30">&nbsp;</td> -->
					</tr>
					<tr class="">
						<td colspan="4">Uang sejumlah</td>
						<td >:</td>
						<td bgcolor="#EEEEEE" colspan="10" style="font-style: italic;-webkit-print-color-adjust: exact;">
							<?php echo $output['total_iuran_text'];?>
						</td>
					</tr>
					<tr>
						<td colspan="15">&nbsp;</td>
						<!-- <td width="30">&nbsp;</td> -->
					</tr>
					<tr>
						<td colspan="4">Untuk Pembayaran</td>
						<td  style="border-bottom: 1px solid black;">:</td>
						<td style="border-bottom: 1px solid black;width: 24%;">
							a. SPP
						</td>
						<td  style="border-bottom: 1px solid black;width: 4%">Rp.</td>
						<td style="border-bottom: 1px solid black;width: 15%" colspan="6" align="right"><b><?php echo $output['spp']['jumlah'];?></b></td>
						<td align="right" style="border-bottom: 1px solid black;">Untuk bln :</td>
						<td align="center" style="border-bottom: 1px solid black;"><b><?php echo $output['spp']['bulan'];?></b></td>
					</tr>
					<tr style="border-bottom: 1px solid black;">
						<td colspan="4">&nbsp;</td>
						<td>:</td>
						<td>
							b. DSP
						</td>
						<td>Rp.</td>
						<td colspan="6" align="right"><b><?php echo $output['dsp'];?></b></td>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr style="border-bottom: 1px solid black;">
						<td colspan="4">&nbsp;</td>
						<td>:</td>
						<td>
							c. Keuangan Lain-lain
						</td>
						<td>Rp.</td>
						<td colspan="6" align="right"><b><?php echo $output['dll'];?></b></td>
						<td colspan="3" align="right"></td>
					</tr>
					<tr style="border-bottom: 1px solid black;">
						<td colspan="15">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="15">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="9"></td>
						<!-- <td>&nbsp;</td> -->
						<td colspan="6" style="border-bottom: 1px solid black;">Cibungbulang,</td>
					</tr>
					<tr>
						<td colspan="15">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="15">&nbsp;</td>
					</tr>
					<tr>
						<td height="30px"><b>Rp</b></td>
						<td align="right" style="border-top: 2px solid black;" colspan="4"><b><?php echo $output['total_iuran_number'];?></b></td>
					</tr>
					<tr>
					    <td>&nbsp;</td>
						<td style="border-top: 2px solid black;">ket:</td>
						<td colspan="7" align="right">&nbsp;</td>
					    <td colspan="6"><b>DEVITA OKTAVIA, S.Pd</b></td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- <tr>
			<td align="center">
				<table width='600' class='List'>
					<tr class='Header'>
						<td align='center'>#</td>
						<td valign='top'>Item Pembayaran&nbsp;</td>
						<td valign='top'>Nominal (Rp)&nbsp;</td>
					</tr>
					<tr class='RowLight' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;1&nbsp;</td>
						<td valign='top' align='left'>1200 - S K S&nbsp;</td>
						<td valign='top' align='right'>900,000&nbsp;</td>
					</tr>
					<tr class='RowDark' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;2&nbsp;</td>
						<td valign='top' align='left'>1210 - Ujian Semester&nbsp;</td>
						<td valign='top' align='right'>450,000&nbsp;</td>
					</tr>
					<tr class='RowLight' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;3&nbsp;</td>
						<td valign='top' align='left'>6032 - Kerja Praktik&nbsp;</td>
						<td valign='top' align='right'>300,000&nbsp;</td>
					</tr>
					<tr class='RowDark' onmouseover='javascript:OnMouseOver(this);' onmouseout='javascript:OnMouseOut(this);'>
						<td align='center' valign='top'>&nbsp;4&nbsp;</td>
						<td valign='top' align='left'>6033 - Seminar Proposal&nbsp;</td>
						<td valign='top' align='right'>300,000&nbsp;</td>
					</tr>
				</table>
				<table width='600' class='List'>
					<tr height='10' class='Navigation'></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="600">
					<tr height="25" bgcolor="#EEEEEE">
						<td align="right" width="449">
							<b>J U M L A H :</b>
						</td>
						<td align="right">
							<b>1,950,000</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="600">
					<tr>
						<td width="350" valign="top">
							<font face="Arial" size="1">
								<u>Catatan</u>:
								<br />
								<br/>Pembayaran dapat dilakukan melalui transfer Via ATM/Mobile Banking: 
								<br/>1). No. Rek VA Bank Muamalat Indonesia  
								<b>8169991505150974</b>
								<br/>2). No. Rek VA Bank Syariah Mandiri 
								<b>9009045151105150974</b>
								<br/>3). Kendala Teknis VA bisa menghubungi 
								<b>08561942487</b> (WA Only) 
							</font>
							<br />
							<p align="center"> 20190315150049 
								<img src="https://siak.uika-bogor.ac.id/lib/barcode.inc.php?code=151105150974" /> 89BEFF8EF3CCB623144FBC4D1F70C6C9 
							</p>
						</td>
						<td width="250" valign="top"> Bogor, 15-03-2019
							<br /> a.n. Kepala BASK
							<br /> Kasubbag. Keuangan
							<br />
							<img src="tpl/printer/ttd_bauk_keuangan.png" width="100" />
							<p>(Gatot Darmadi, S.Kom)</p>
						</td>
					</tr>
				</table>
			</td>
		</tr> -->
	</table>
	<div style="border: 1px dashed;margin-top: 20px;"></div>
	<!-- <div id="scissors">
	    <div></div>
	</div> -->
	<script language='javascript'>
		setTimeout(function () { window.print(); }, 500);
        window.onfocus = function () { setTimeout(function () { window.close(); }, 100); }
	</script>