<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;">

    <%-- <%@ include file = "inc/navbar.jsp" %> --%>
    
    <%@ include file = "inc/navbar-head.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <!-- <ol class="breadcrumb">
            <li class="breadcrumb-item active">Selamat Datang di Aplikasi PPS Online</li>
          </ol> -->
		
		<div class="row justify-content-center mt-4">
		<div class="col-md-10">
		<div class="card bg-head pill">
	    <div class="card-body">
		<div class="row">
			<div class="col-6 py-2">
				<h4>Kuota</h4>
			</div>
			<div class="col-6">
				<button id=btn-add data-toggle="modal" data-target="#modal-form" class="btn btn-light pill pl-4 pr-4 float-right"><span class="fas fa-plus"></span> New </button>

			</div>
		</div>
		<div class="border-bottom mb-3"></div>
		<div class="table-responsive">
		<table id="tbl-data" class="table table-striped" style="opacity: 0.8;border-radius: 23px;overflow: hidden;">
			<thead class="bg-head text-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Code</th>
					<th scope="col">Date</th>
					<th scope="col">Value</th>
					<th scope="col">Type</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody class="bg-light text-dark">
			</tbody>
		</table>
		</div>
	    </div>
		</div>
		</div>
		</div>

        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog " role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Entry Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form">
		      <input type="text" class="d-none" name="id" placeholder="" autocomplete="off">
		      <div class="form-row">
			  <div class="form-group col">
			    <label for="">Code</label>
			    <input type="text" class="form-control" name="code" placeholder="" autocomplete="off">
			  </div>
			  </div>
			  
              <div class="form-row ">
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="text" class="form-control single-date-picker" name="start_date" placeholder="Start Date"> 
                  </div>
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="text" class="form-control single-date-picker" name="end_date" placeholder="Until Date"> 
                  </div>
              </div>
              
		      <div class="form-row">
			  <div class="form-group col">
			    <label for="">Value</label>
			    <input type="text" class="form-control" name="value" placeholder="" autocomplete="off">
			  </div>
			  </div>
			  
		      <div class="form-row">
			  <div class="form-group col">
			    <label for="">Type</label>
			    <input  type="text" class="form-control" value="REF" name="type" placeholder="" autocomplete="off">
			  </div>
			  </div>
			  
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="save();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal UPLOAD-->
	<div class="modal fade" id="modal-form-upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content pill bg-gradient">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">UPLOAD PHOTO</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-upload-msg" class="alert alert-danger" role="alert"></div>
				<form id="entry-form-upload" >
				  <div class="form-group">
				    <label for="">Tour Guide Photo</label>
				    <div class="row">
				    	<div class="col-md-12">
						<div class="custom-file">
						  <input type="file" class="custom-file-input" id="customFile" name="file">
						  <label class="custom-file-label" for="customFile">Choose file</label>
						</div>
						</div>
					</div>
				  </div>
				</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn bg-head pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="saveUpload();" class="btn btn-secondary pill pl-5 pr-5">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/configure-kuota.js"></script>

  </body>

</html>
