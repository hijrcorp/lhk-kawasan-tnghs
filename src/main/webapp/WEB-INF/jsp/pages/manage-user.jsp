<%@page import="id.co.hijr.sistem.service.VaultService"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransitCamp"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransaction"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="java.util.Currency"%>
<%@page import="java.util.Locale"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.sistem.model.Account"%>
<%@page import="id.co.hijr.sistem.mapper.AccountMapper"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionPhotoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionPhoto"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
RegionPhotoMapper regionPhotoMapper = appCtx.getBean(RegionPhotoMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);
AccountMapper accountMapper = appCtx.getBean(AccountMapper.class);
PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
PurchaseDetilTransactionMapper purchaseDetilTransactionMapper = appCtx.getBean(PurchaseDetilTransactionMapper.class);
PurchaseDetilVisitorMapper purchaseDetilVisitorMapper = appCtx.getBean(PurchaseDetilVisitorMapper.class);
PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper = appCtx.getBean(PurchaseDetilTransitCampMapper.class);
PurchaseDetilLuggageMapper purchaseDetilLuggageMapper = appCtx.getBean(PurchaseDetilLuggageMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
QueryParameter param = new QueryParameter();

Purchase purchase= purchaseMapper.getEntity(id);


if(request.getParameter("id")!=null) {
	if(purchase==null){
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
		purchase = purchaseMapper.getListExtended(param).get(0);
	}
	if(request.getParameter("menu").equals("purchase_detil") || request.getParameter("menu").equals("booking_detil")){
		QueryParameter param_detil = new QueryParameter();
		param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.HEADER_ID + " = '"+purchase.getId()+"')");
		List<PurchaseDetilVisitor> dataPurchaseDetilVisitor = purchaseDetilVisitorMapper.getList(param_detil);
		//purchase.setPurchaseDetilVisitor(dataPurchaseDetilVisitor);
		
		param_detil = new QueryParameter();
		param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+purchase.getId()+"')");
		List<PurchaseDetilTransaction> dataPurchaseDetilTransaction = purchaseDetilTransactionMapper.getList(param_detil);
		//purchase.setPurchaseDetilTransaction(dataPurchaseDetilTransaction);
		
		param_detil = new QueryParameter();
		param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+purchase.getId()+"')");
		List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param_detil);
		//purchase.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);
		
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
		purchase= purchaseMapper.getListExtended(param).get(0); //getEntity(id);
		
	}
}

String ketuaTim="";
String numberFam="";
String addressFam="";
if(purchase!=null){
	if(purchase.getParentId()!=null){
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
		param.setInnerClause(param.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is not null)");
		Purchase purchaseNoParent= purchaseMapper.getListExtended(param).get(0); //getEntity(id);
		
		//manipulate this for childeren purchase cause the parent have what this need
		purchase.setPurchaseDetilVisitor(purchaseNoParent.getPurchaseDetilVisitor());
		purchase.setPurchaseDetilTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
		
	
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
		param.setInnerClause(param.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is null)");
		Purchase purchaseNoParentNull = purchaseMapper.getListExtended(param).get(0);
		
		for(PurchaseDetilVisitor pdv : purchaseNoParentNull.getPurchaseDetilVisitor()){
			if(pdv.getResponsible().equals("1")) {
		    	numberFam=pdv.getPhoneNumberFamily();
		     	addressFam=pdv.getAddressFamily();
			}
		}
	
		ketuaTim=purchaseNoParentNull.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName();
		
	
		QueryParameter param_detil = new QueryParameter();
		param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
		param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.PARENT_HEADER_ID + " ='"+purchase.getId()+"')");
		Purchase purchaseDetilVisitorWithHeader= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
		
		purchase.setPurchaseDetilVisitor(purchaseDetilVisitorWithHeader.getPurchaseDetilVisitor());
	}else{
		ketuaTim=purchase.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName();
		 
		QueryParameter param_detil = new QueryParameter();
		param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getId()+"')");
		param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.PARENT_HEADER_ID + " is "+" null "+")");
		Purchase purchaseDetilVisitorWithHeader= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
		
		purchase.setPurchaseDetilVisitor(purchaseDetilVisitorWithHeader.getPurchaseDetilVisitor());
	
	
		for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
			if(pdv.getResponsible().equals("1")) {
		    	numberFam=pdv.getPhoneNumberFamily();
		     	addressFam=pdv.getAddressFamily();
			}
		}
		
	}
}


String paramfilter="";
paramfilter=request.getParameter("filter_keyword");
if(paramfilter==null)paramfilter="";

param = new QueryParameter();
param.setClause(param.getClause()+" AND "+Region.NAME+" LIKE  '%"+paramfilter+"%'");

if(!id.equals("0")){
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+Region.ID+" =  '"+id+"'");
}

List<Region> region = regionMapper.getList(param);
List<File> list_photo = new ArrayList<>();
List<RegionTodo> list_todo = new ArrayList<>();
List<RegionFacility> list_facility = new ArrayList<>();
if(region.size() == 1){
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.get(0).getId()+"'");
	list_photo = fileMapper.getList(param);

	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.get(0).getId()+"'");
	list_todo = regionTodoMapper.getList(param);
	region.get(0).setList_todo(list_todo);
	
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.get(0).getId()+"'");
	list_facility = regionFacilityMapper.getList(param);
	region.get(0).setList_facility(list_facility);
}
//param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
/* List<RegionFacility> regionFacility = regionFacilityMapper.getList(param); */
param = new QueryParameter();
List<RegionPhoto> regionPhoto = regionPhotoMapper.getList(param);

Account account = accountMapper.getEntity(request.getAttribute("username").toString());
//untuk format currency
NumberFormat formatter = NumberFormat.getCurrencyInstance();
Locale myIndonesianLocale = new Locale("in", "ID");
formatter.setCurrency(Currency.getInstance(myIndonesianLocale));


//untuk format tanggal atau waktu
SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy", myIndonesianLocale);
SimpleDateFormat sdftime = new SimpleDateFormat("hh.mm aa");
SimpleDateFormat sdftimeleft = new SimpleDateFormat("hh:mm:ss");
SimpleDateFormat sdf_MY = new SimpleDateFormat("MMMM yyyy");


VaultService vaultService = appCtx.getBean(VaultService.class);
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>

  <body id="page-top">
    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

    <div id="content-wrapper">	
		<div class="container-fluid">
			<!-- <div class="row">
				<div class="col-md-4">hwhw</div>
				<div class="col-md-8">jafhjjhfwjfafawf</div>
			</div> -->
			<%
				String modemenu= request.getParameter("menu")!=null?request.getParameter("menu"):"";
			%>
			<div class="row justify-content-center">
			  <div class="col-md-3">
			    <div class="nav flex-column nav-pills bg-white border rounded" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			      <a class="nav-link p-3 text-dark" href="#">
		           <div class="text-center">
		           <img src="${requestScope.avatar}" width="50px" /> 
		           </div>
		           <div class="text-center"><strong>${requestScope.realName}</strong> <div class="text-muted small"><%out.print(account.getEmail()); %></div>
		           </div>
		          </a>
		          <div class="border text-dark border-bottom"></div>
			      <a class="nav-link text-dark <% out.print(modemenu.equals("purchase_detil")?"active":""); %> <% out.print(modemenu.equals("purchase")?"active show":""); %>" id="v-purchase_list-tab"  href="manage-user?menu=purchase" role="tab" aria-controls="v-pills-purchase_list" aria-selected="false"><i class="fas fa-receipt text-primary mr-1"></i> Purchase List</a>
			      <a class="nav-link text-dark <% out.print(modemenu.equals("booking_detil")?"active":""); %> <% out.print(modemenu.equals("booking")?"active show":""); %>" id="v-booking_list-tab" href="manage-user?menu=booking" role="tab" aria-controls="v-pills-booking_list" aria-selected="false"><i class="fas fa-file-alt text-primary mr-1"></i> My Booking</a>
			      
			      <a class="nav-link text-dark <% out.print(modemenu.equals("account")?"active show":""); %>" id="v-pills-home-tab" href="manage-user?menu=account" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="fa fa-cog text-primary"></i>  My Account</a>
			      <div class="border text-dark border-bottom"></div>
			      <a class="nav-link text-dark p-3" href="${pageContext.request.contextPath}/login-logout"><i class="fa fa-power-off text-danger"></i> Log Out</a>
			    </div>
			  </div>
			  <div class="col-md-6">
			    <div class="tab-content" id="v-pills-tabContent">
			      	<div class="tab-pane fade <% out.print(modemenu.equals("account")?"active show":""); %>" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
						<div class="card">
						  <div class="card-body">
						  <div class="row">
						  	<div class="col text-left">
						   		<h5>Edit My Account</h5>
						   	</div>
						  </div>
						  </div>
						</div>
						
						<div class="card mt-3 ">
						  <div class="card-body">
						    <div class="row ">
							   <div class="col-md-12">
							      <form id="form-register">
							         <input type="hidden" class="form-control" name="id" value="<%out.print(account.getId()); %>">
							         <div class="form-row">
							            <div class="form-group col-md-6">
							               <label>Nama Depan</label>
							               <input disabled type="text" class="form-control" name="first_name" autocomplete="off" value="<%out.print(account.getFirstName()); %>">
							            </div>
							            <div class="form-group col-md-6">
							               <label>Nama Belakang</label>
							               <input disabled type="text" class="form-control" name="last_name" autocomplete="off" value="<%out.print(account.getLastName()); %>">
							            </div>
							         </div>
							         <div class="form-row">
							            <div class="form-group col-md-6">
							               <label>Alamat Email</label>
							               <input disabled type="email" class="form-control" name="email" autocomplete="off" value="<%out.print(account.getEmail()); %>" onkeyup="$('[name=username]').val(this.value.split('@')[0])">
							            </div>
							            <div class="form-group col-md-6">
							               <label>Nomor HP</label>
							               <input disabled type="number" class="form-control" name="mobile" autocomplete="off" value="<%out.print(account.getMobile()); %>">
							            </div>
							         </div>
							         <div class="form-row d-none">
							            <div class="form-group col">
							               <label>Akses Grup</label>
							               <select name="group_id" class="form-control">
							                  <option value="2002200">Pendaki</option>
							               </select>
							            </div>
							         </div>
							         <div class="form-row">
							            <div class="form-group col-6">
							               <label>Username</label>
							               <input disabled type="text" class="form-control" name="username" autocomplete="off" value="<%out.print(account.getUsername()); %>">
							            </div>
							            <!-- <div class="form-group col">
							               <label></label>
							               <div class="form-check">
							                  <input class="form-check-input" type="checkbox" value="true" name="enabled">
							                  <label class="form-check-label">
							                  Reset Password
							                  </label>
							               </div>
							               </div> -->
							         </div>
							         <!-- <div class="form-group form-check">
							            <input type="checkbox" class="form-check-input" id="exampleCheck1">
							            <label class="form-check-label" for="exampleCheck1">Check me out</label>
							            </div> -->
							         <div class="row justify-content-center text-right">
							            <div class="col">
							               <button id="btn-edit" onclick="$('#form-register').find('input').prop('disabled', false);$('#form-register').find('button').prop('disabled', false);" type="button" class="btn btn-primary ">EDIT</button>
							               <button id="btn-submit" type="submit" class="btn btn-success " disabled>SAVE</button>
							            </div>
							         </div>
							      </form>
							   </div>
							</div>
						  </div>
						</div>

					</div>
			      	<div class="tab-pane fade <% out.print(modemenu.equals("purchase")?"active show":""); %>" id="v-pills-purchase_list" role="tabpanel" aria-labelledby="v-pills-profile-tab">
						<div class="card mb-1">
						  <div class="card-body">
						    Find all your e-tickets on <a href="manage-user?menu=booking">My Booking</a>
						  </div>
						</div>
						
						<ul class="nav nav-pills nav-justified bg-light mt-3 mb-4 d-none">
						  <li class="nav-item">
						    <a class="nav-link active" href="#">Past 30 days</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#">Apr 2019</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#">Mar 2019</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#">Customize date</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#">Filter</a>
						  </li>
						</ul>
						
						<h5>Ongoing Purchases</h5>
						
						<div id="container-purchase-list"></div>
						
					</div>
			      <div class="mt-3 mt-md-1 tab-pane fade <% out.print(modemenu.equals("booking")?"active show":""); %>" id="v-pills-booking_list" role="tabpanel" aria-labelledby="v-pills-messages-tab">
			      		<h4>Active E-Tickets</h4>
			      		
			      		<div id="container-booking-list">
						<div class="card mb-3" >
						  <div class="row no-gutters">
						    <div class="col-auto p-3">
						      <img width="100" src="https://ik.imagekit.io/tvlk/image/imageResource/2017/05/23/1495539753478-be3f37a9133fa6e3f40688781d07e885.png?tr=q-75" class="img-fluid" alt="...">
						    </div>
						    <div class="col-md-8">
						      <div class="card-body">
						        <h5 class="card-title">No Active Booking Found</h5>
						        <p class="card-text">Anyting you booked shows up here, but it seems like you haven't made any.Let's create one via homepage!</p>
						        <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
						      </div>
						    </div>
						  </div>
						</div>
						</div>
						
						<!-- <h4>Purchase List</h4>
						<div class="card mt-3">
						  <div class="card-body">
						    View your <a href="#">Purchase History</a>
						  </div>
						</div> -->
			      </div>
			      
			      <div class="tab-pane fade <% out.print(modemenu.equals("booking_detil")?"active show":""); %>" id="v-pills-booking_detil" role="tabpanel" aria-labelledby="v-pills-messages-tab">
			      		<div class="row justify-content-between">
						    <div class="col-9">
								<a href="?menu=booking" class="btn btn-link float-left"><i class="fas fa-arrow-left"></i> Back</a>
							</div>
							<div class="col-3 d-none">
							    <a target="_blank" href="" class="btn btn-sm btn-primary float-right"><i class="fas fa-file-pdf"></i> Download</a>
							</div>
						</div>
			      		
			      		<div id="container-booking-detail">
			      			<% if(purchase==null) { %>
							<div class="card mb-3" >
							  <div class="row no-gutters">
							    <div class="col-auto p-3">
							      <img width="100" src="https://ik.imagekit.io/tvlk/image/imageResource/2017/05/23/1495539753478-be3f37a9133fa6e3f40688781d07e885.png?tr=q-75" class="img-fluid" alt="...">
							    </div>
							    <div class="col-md-8">
							      <div class="card-body">
							        <h5 class="card-title">No Active Booking Found</h5>
							        <p class="card-text">Anyting you booked shows up here, but it seems like you haven't made any.Let's create one via homepage!</p>
							        <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
							      </div>
							    </div>
							  </div>
							</div>
							<% }else{ %>
							<div class="card mb-3">
					         <div class="card-header bg-white">
					         	<div class="row">
									<div class="col text-left  "><strong>Booking Details</strong></div>
									<div class="col text-right">Booking ID <strong><% out.print(purchase.getId()); %></strong></div>	
								</div>
					         </div>
					         <div class="card-body">
					            <div class="row">
					               <div class="col-6 col-md-4">
					                  <h6 class="mb-2">Jalur</h6>
					                  <p class="mb-2 fs--1">Hari, Tanggal Pendakian</p>
					                  <!-- <p class="mb-2 fs--1">Transit Camp</p>
					                  <p class="mb-2 fs--1">Jam Wajib Lapor di BC</p>
					                  <p class="mb-2 fs--1">Nomor Kapling</p> -->
					                  <p class="mb-2 fs--1">Nama Ketua</p>
					                  <p class="mb-2 fs--1">Jumlah Pendaki</p>
					                  <p class="mb-2">Jumlah Pembayaran</p>
					                  <p class="mb-2">Kode Booking</p>
					               </div>
					               <div class="col-6 col-md-8">
					                  <div class="media">
					                     <!-- <img class="mr-3" src="../assets/img/icons/visa.jpg" width="40" alt=""> -->
					                     <div class="media-body">
					                        <h6 class="mb-2">: <% out.print(purchase.getRegionName()+" - "+purchase.getRegionEndName()); %></h6>
					                        <h6 class="mb-2">: <% out.print(sdf.format(purchase.getStartDateSerialize())); %></h6>
					                        <%-- <p class="mb-2 fs--1 ">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp()); %></p>
					                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp()); %></p>
					                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique()); %></p> --%>
					                        <p class="mb-2 fs--1 font-weight-bold">: <% out.print(ketuaTim); %></p>
						                  	<p class="mb-2 fs--1 font-weight-bold">: <a title="klik untuk melihat detail" onclick="$('#modal-view').modal('show')" href="javascript:void(0)"><% out.print(purchase.getCountTicket()); %></a></p>
							                <p class="mb-2 fs--1 ">: <% out.print(formatter.format(purchase.getAmountTicket())); %></p>
					                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getCodeBooking()); %></p>
					                     </div>
					                  </div>
					               </div>
					            </div>
					            
					         </div>
					         
					         <div class="card-footer bg-white">
					         	<div class="row">
									<div class="col text-right">
										<% if(purchase.getStatusReschedule()!=null) { %>
											<a target="_blank" href="reschedule?id=<% out.print(purchase.getId()); %>&step=1" class="btn btn-link">Request Reschedule</a>
										<% } %>
										<% 
										boolean purchaseCheck=false;
										if(purchase.getParentId()!=null){
											param = new QueryParameter();
											param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
											if(purchaseMapper.getList(param).get(0).getPurchaseDetil()==null) {
												purchaseCheck=true;
											}
										}
										if(purchase.getParentId()==null && purchaseCheck) { %>
										<% } %>
										<a target="_blank" href="new-booking?id=<% out.print(purchase.getId()); %>&step=1" class="btn btn-link">Request Rebooking</a>
										
										<% if(purchase.getStatus().equals("PAYMENT")) { %>
											<a target="_blank" href="${pageContext.request.contextPath}/export/download?filename=<% out.print(id); %>.pdf" class="btn btn-link">E-ticket</a>
										<% } %>
									</div>	
								</div>
							 </div>
					      </div>
					      
					      <div class="row justify-content-center">
						   <div class="col-md-12">
						      <div class="card mb-3">
						         <div class="card-header bg-white">
						            <div class="row">
						               <div class="col text-left "><strong>Pendaki Details</strong></div>
						            </div>
						         </div>
						         <div class="card-body p-0 table-responsive">
						            <table id="tbl-detil-individu" class="border-0 table table-responsive table-sma table-striped table-hover m-0" style="opacity: 0.8;overflow: auto;display: inline-table;">
						               <thead class="bg-dark text-white align-middle">
						                  <tr>
						                     <th scope="col" class="text-center align-middle">#</th>
						                     <th scope="col" class=" align-middle">No.Identitas</th>
						                     <th scope="col" class=" align-middle">Nama Lengkap</th>
						                     <th scope="col" class=" align-middle">Gender</th>
						                     <th scope="col" class=" align-middle">No.HP</th>
						                     <th scope="col"></th>
						                  </tr>
						               </thead>
						               <tbody class="bg-light text-dark">
						               <%
						               int key=0;
						               int no=1;
						               for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()) {
						               	if(pdv.getResponsible().equals("0")) {
							               %>
							                  <tr data-visitor="1619736496187845" class="data-row-identity" id="row-1619736496187845">
							                     <td><% out.print(no); %></td>
							                     <td><% out.print(vaultService.decrypt(pdv.getVisitorIdentity().getNoIdentity())); %></td>
							                     <td class=""> <% out.print(pdv.getVisitorIdentity().getFullName()); %> </td>
							                     <td class=""> <% out.print(pdv.getVisitorIdentity().getGender()); %> </td>
							                     <td class=""> <% out.print(pdv.getPhoneNumber()); %> </td>
						            			 <td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getVisitorIdentity().getIdFile()); %>?filename=<% out.print(pdv.getVisitorIdentity().getFileName()); %>&download&decrypt','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
							                  </tr>
							               <%
						               		no++;
						               	}
						               	key++;
						               } %>
						               </tbody>
						            </table>
						         </div>
						         <div class="card-body">
						            <form>
						               <div class="form-group">
						                  <label for="exampleInputEmail1">No.HP Keluarga</label>
						                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" readonly="" value="<% out.print(numberFam);%>">
						                  <small id="emailHelp" class="form-text text-muted">*nomor handphone keluarga yang bisa dihubungi.</small>
						               </div>
						               <div class="form-group">
						                  <label for="exampleInputPassword1">Alamat Keluarga</label>
						                  <textarea class="form-control" id="exampleInputPassword1" readonly="" rows="3"><% out.print(addressFam);%></textarea>
						               </div>
						            </form>
						         </div>
						      </div>
						   </div>
						</div>
					      <% } %>
						</div>
						
						<!-- <h4>Purchase List</h4>
						<div class="card mt-3">
						  <div class="card-body">
						    View your <a href="#">Purchase History</a>
						  </div>
						</div> -->
			      </div>
			      
			      <div class="tab-pane fade <% out.print(modemenu.equals("purchase_detil")?"active show":""); %>" id="v-pills-purchase_detil" role="tabpanel" aria-labelledby="v-pills-messages-tab">
			      		<div class="row justify-content-center">
						    <div class="col-12">
								<a href="?menu=purchase" class="btn btn-link float-left"><i class="fas fa-arrow-left"></i> Back</a>
							</div>
						</div>
			      		
			      		<div id="container-purchase-detail">
			      			<% if(purchase==null) { %>
							<div class="card mb-3" >
							  <div class="row no-gutters">
							    <div class="col-auto p-3">
							      <img width="100" src="https://ik.imagekit.io/tvlk/image/imageResource/2017/05/23/1495539753478-be3f37a9133fa6e3f40688781d07e885.png?tr=q-75" class="img-fluid" alt="...">
							    </div>
							    <div class="col-md-8">
							      <div class="card-body">
							        <h5 class="card-title">No Active Booking Found</h5>
							        <p class="card-text">Anyting you booked shows up here, but it seems like you haven't made any.Let's create one via homepage!</p>
							        <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
							      </div>
							    </div>
							  </div>
							</div>
							<% }else{ %>
							
							<div class="card mb-3">
					         <div class="card-header bg-white">
					         	<div class="row">
									<div class="col text-left align-self-center"><strong>Purchase <% out.print(purchase.getStatus().equals("PAYMENT")?"Successful":purchase.getStatus()); %></strong></div>
									<div class="col text-right"> <strong><% out.print(formatter.format(purchase.getAmountTotalTicket())); %></strong></div>	
								</div>
					         </div>
					         <div class="card-body">
					            <div class="row">
					               <div class="col-6 col-md-4">
					                  <h6 class="mb-2 text-muted">PURCHASED ON</h6>
					                  <p class="mb-2 fs--1"><% out.print(sdf.format(purchase.getPurchaseDetilTransaction().get(0).getSubmitedDate())); %></p>
					               </div>
					               <div class="col-6 col-md-8">
					                  <div class="media">
					                     <!-- <img class="mr-3" src="../assets/img/icons/visa.jpg" width="40" alt=""> -->
					                     <div class="media-body">
					                        <h6 class="mb-2 text-muted"> PAYMENT METHOD</h6>
					                        <h6 class="mb-2"><% out.print(purchase.getPurchaseDetilTransaction().get(0).getBankId()); %> </h6>
					                     </div>
					                  </div>
					               </div>
					            </div>
					            
					         </div>
					      </div>
					      
					         	<div class="row  mb-1">
									<div class="col text-left text-muted"><strong>E-ticket Details</strong></div>	
								</div>
								
							<div class="card mb-3">
					         <div class="card-body">
					            <div class="row">
					               <div class="col-6 col-md-4">
					                  <h6 class="mb-2">Jalur</h6>
					                  <p class="mb-2 fs--1">Hari, Tanggal Pendakian</p>
					                  <!-- <p class="mb-2 fs--1">Transit Camp</p> -->
					                  <h6 class="mb-2">Kode Booking</h6>
					               </div>
					               <div class="col-6 col-md-8">
					                  <div class="media">
					                     <!-- <img class="mr-3" src="../assets/img/icons/visa.jpg" width="40" alt=""> -->
					                     <div class="media-body">
					                        <h6 class="mb-2">: <% out.print(purchase.getRegion().getName()); %></h6>
					                        <h6 class="mb-2">: <% out.print(sdf.format(purchase.getStartDateSerialize())); %></h6>
					                        <%-- <p class="mb-2 fs--1 ">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp()); %></p> --%>
					                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getCodeBooking()); %></p>
					                     </div>
					                  </div>
					               </div>
					            </div>
					            
					         </div>
					         
					         <div class="card-footer bg-white">
					         	<div class="row">
					         		<div class="col text-left align-self-center"><span class="badge badge-pill badge-<% if(purchase.getStatus().equals("CANCEL")) out.print("warning"); else out.print("success"); %> '+status_color+' text-white"><% out.print(purchase.getStatus()); %> <i class="fas fa-'+status_icon+'"></i></span> </div>
									<div class="col text-right">
										<a target="_blank" href="?menu=booking_detil&id=<% out.print(purchase.getId()); %>" class="btn btn-link">Details</a>
									</div>	
								</div>
							 </div>
					      </div>
					      
					      <% } %>
						</div>
						
						<!-- <h4>Purchase List</h4>
						<div class="card mt-3">
						  <div class="card-body">
						    View your <a href="#">Purchase History</a>
						  </div>
						</div> -->
			      </div>
			      
			      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
			    </div>
			  </div>
			</div>
		</div>
    </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    <%@ include file = "inc/footer.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/manage-user.js"></script>
	
  </body>

</html>
