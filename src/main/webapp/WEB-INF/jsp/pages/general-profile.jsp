<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
String id = request.getParameter("id")!=null?request.getParameter("id"):"0";

Region region = regionMapper.getEntity(id);
QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
List<File> list_photo = fileMapper.getList(param);
//region.setList_photo("photo", list_photo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.getId()+"'");
List<RegionTodo> list_todo = regionTodoMapper.getList(param);
region.setList_todo(list_todo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");
List<RegionFacility> list_facility = regionFacilityMapper.getList(param);
region.setList_facility(list_facility);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>

  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">
	
    <%-- <%@ include file = "inc/navbar.jsp" %> --%>
	<!-- costume -->
	
	<c:if test="${param.step != null && param.step == 1}">	
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid p-3" style="width:70%">
		
		  <a class="navbar-brand" href="#">
		    <img src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
		    Kawasan
		  </a>
		  
		<ul class=" nav nav-pills ml-auto">
		  <li class="nav-item">
		    <a id="btn-book-1" class="nav-link active" data-toggle="pill" href="#pills-book-1">Basic Information</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-2" class="nav-link" data-toggle="pill" href="#pills-book-2">Vehicle</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-3" class="nav-link" data-toggle="pill" href="#pills-book-3">Tour Guide</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">E-ticket</a>
		  </li>
		</ul>
		</div>
		</nav>
	</c:if>
	<!--  -->
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">
		
		<c:if test="${param.step == null}">
		
		<!-- <header class="image-bg-fluid-height hm-stylish-strong w-100" style="background: url('https://tvlk.imgix.net/imageResource/2018/11/27/1543315914093-c3d1c5417fde9597c6d391674f5feeae.png?auto=compress%2Cformat&cs=srgb&fm=png&ixlib=java-1.1.12&q=75')no-repeat center;background-size: cover;height:360px">
			
		</header> -->
		
		 <div class="container-fluid mb-3" style="width:70%">
		 <div class="row justify-content-center">
		    <div class="col-md-12">
				<button onclick="$('#container-search').toggle()" class="btn btn-primary btn-sm px-4 px-sm-5 float-right" type="button">Change Search</button>
			</div>
		  </div>
		  </div>
		
		 <div id="container-search" class="container-fluid" style="width:70%">
		 <div class="row  justify-content-center">
		   <!--  <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label> -->
		    <div class="col-md-12">
		    
				<div class="container-fluid pb-3 pt-3	bg-white">
					<div class="input-group ">
					  <input type="text" class="form-control" placeholder="Enter destination name or activity" aria-label="Recipient's username" aria-describedby="button-addon2">
					  <div class="input-group-append">
					    <button style="border-top-right-radius: 23px!important;border-bottom-right-radius: 23px!important;" class="btn btn-outline-light text-muted" type="button" id="button-addon2">Search!</button>
					  </div>
					</div>
			    </div>
		    </div>
		  </div>
		  </div>
		
        <div class="container-fluid" style="width:70%">
		
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="card mb-3">
				<!-- <div style="max-height: 450px;height: 450px;background-image: url('https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnG98M/3uI16G3tLN17gcLc6PsaHRlPhwKyEDXJ3hZPmbanBtM8D6JzU42qyM4VaPjiMqUlXTdzUr2jNOPsddnkC49390o5IOgU1/OoIc4cRxiqiStapzN6HYw6bU84dDsQ=='); background-size: cover;"> -->
				<!--Carousel Wrapper-->
				<div>
				<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
				  <!--Slides-->
				  <div class="carousel-inner" role="listbox">
				  <% 
				  int key=0;
					  for(File o2 : list_photo){
					  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
					  	o2.setName(o2.getName().replace(" ", "%"));
					  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
					  	if(key==0){
						  	out.println("<div class='carousel-item active'>");
						  		out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
					      	out.println("</div>");
					  	}else if(key>0){	
						  	out.println("<div class='carousel-item'>");
						  		out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
					      	out.println("</div>");
					  	}
				      	key++;
					  }
				  %>
				  </div>
				  <!--/.Slides-->
				  <!--Controls-->
				  <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				  <!--/.Controls-->
				  <ol class="carousel-indicators">
					  <% 
					  key=0;
					  for(File o2 : list_photo){
						o2.setName(o2.getName().replace(" ", "%"));
					  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
					  	if(key==0){
						  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
						  	out.println("<li style='width: 6.25rem!important;' data-target='#carousel-thumb' data-slide-to="+key+" class='active'>");
						  	out.println("<img class='d-block w-100' src="+path+" class='img-fluid'></li>");
					  	}else if(key>0){
					  		String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
						  	out.println("<li style='width: 6.25rem!important;' data-target='#carousel-thumb' data-slide-to="+key+">");
						  	out.println("<img class='d-block w-100' src="+path+" class='img-fluid'></li>");
					  	}
				      	key++;
					  } 
					  %>
				  </ol>
				</div>
				<!--/.Carousel Wrapper-->
				</div>
			<!-- </div> -->
            <div class="card-body">
              <div class="row justify-content-between align-items-center">
                <div class="col">
                  <div class="media">
                     <div class="media-body fs--1">
                      <h5 class="fs-0"><% out.println(region.getName()); %></h5>
                      <p class="mb-0"><i class="fas fa-map-marker-alt"></i> <% out.println(region.getLocationDesc()); %></p>
                     <!--  <p class="mb-0 text-justify">
                      	Minahasa Utara menyuguhkan berbagai tempat yang tidak kalah menarik untuk dikunjungi. Berikut ini 5(lima) tempat wisata di Minahasa Utara yang mungkin bisa menjadi alternatif wisatawan untuk berwisata
                      </p> -->
                    </div>
                  </div>
                </div>
                <div class="col-md-auto mt-4 mt-md-0">
                	<div class="">
                		<p><span class="fs-0 font-weight-light">Mulai Dari</span></p>
                		<h5 class="fs-0 text-orange font-weight-light">Rp. 50.000 - Rp. 90.000</h5>
                		<button id="btn-book" data-toggle="modal" data-target="#modal-form" class="btn btn-sm px-4 px-sm-5 btn-block bg-orange" type="button">Book Now</button>
                	</div>
                </div>
              </div>
            </div>
          </div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="card">
			  <div class="card-header">
			    <ul class="nav nav-tabs card-header-tabs">
			      <li class="nav-item">
			        <a class="nav-link active" data-toggle="tab" href="#nav-info">Overview</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link " data-toggle="tab" href="#nav-desc">What To Expect</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-toggle="tab" href="#nav-perhatian">Inclusions & Exclusions</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-toggle="tab" href="#nav-refund">Cancellation Policy</a>
			      </li>
			    </ul>
			  </div>
			  <div class="tab-content" id="nav-tabContent">
				  <div class="tab-pane fade" id="nav-desc" role="tabpanel" aria-labelledby="nav-profile-tab">
					  <div class="card-body">
				      	<div class="accordion" id="accordionExample">
						  <% 
						  key=0;
						  String row="";
						  for(RegionTodo o3 : list_todo){
							if(o3.getParent().isEmpty() || o3.getParent().equals("")) {
								row+="<div class='card'>";
								row+="<div class='bg-light p-1' id='heading"+key+"'>";
								row+="<h2 class='mb-0'>";
									row+="<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#collapse"+key+"' aria-expanded='true' aria-controls='collapseOne'>DAY "+o3.getDay()+" : "+o3.getActivity()+"</button>";
					          	row+="</h2>";
					          	row+="</div>";
							
					          	if(key==0){
					          		row+="<div id='collapse"+key+"' class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>";
					          	}else{
					          		row+="<div id='collapse"+key+"' class='collapse' aria-labelledby='headingOne' data-parent='#accordionExample'>";
						         }
					          	row+="<div class='card-body'>";
					          	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
								  for(RegionTodo o4 : list_todo){
									if(o3.getId().equals(o4.getParent())){
					          			row+="<p><i class='far fa-calendar-check'></i> "+dateFormat.format(o4.getTime())+" : "+o4.getActivity()+"</br></p>";
									}
								  }
					          		row+="</div>";
							    row+="</div>";
							    
							    row+="</div>";
							}
					      	key++;
						  } 
						  out.print(row);
						  %>
						  
						</div>
				      	
					  </div>
				  </div>
				  <div class="tab-pane fade" id="nav-refund" role="tabpanel" aria-labelledby="nav-profile-tab">
					  <div class="card-body">
					    <!-- <h5 class="card-title">Special title treatment 2</h5> -->
					    <p>For a full refund, cancel at least 24 hours in advance of the start date of the experience.</p>
				    	<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
					  </div>
				  </div>
				  <div class="tab-pane fade show active" id="nav-info" role="tabpanel">
					<div class="card-body">
					    <p class="card-text"><% out.print(region.getDescription()); %></p>
					</div>
				  </div>
				  <div class="tab-pane fade" id="nav-perhatian" role="tabpanel">
					<div class="card-body">
				    	<div class="row">
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold"><i class="fas fa-check-circle fa-1x text-success mr-3"></i> Inclusions </h5><!-- Harga Termasuk -->
				      			<ul>
								  <% 
								  	key=0;
									for(RegionFacility o3 : list_facility){
										if(o3.isInclude().equals("Y")){
									  		out.println("<li>"+o3.getItem()+"</li> ");
										}
										key++;
									}
								  %>
							    </ul>
					    	</div>
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold"><i class="fas fa-times-circle text-danger mr-3"></i> Exclusions</h5> <!-- Harga Tidak Termasuk -->
				      			<ul>
								  <% 
								  	key=0;
									for(RegionFacility o3 : list_facility){
										if(o3.isInclude().equals("N")){
									  		out.println("<li>"+o3.getItem()+"</li> ");
										}
										key++;
									}
								  %>
							    </ul>
					    	</div>
				    	</div>
				    </div>
				  </div>
				</div>
			</div>
			</div>
		</div>
		
		<div class="row mt-3 d-none">
			<div class="col-md-12">
				<div class="card-deck">
				  <div class="card">
				    <div class="card-body text-justify">
				      	<h5 class="card-title font-weight-bold">Deskripsi</h5>
				    	<p class="card-text">Minahasa Utara menyuguhkan berbagai tempat yang tidak kalah menarik untuk dikunjungi. Berikut ini 5(lima) tempat wisata di Minahasa Utara yang mungkin bisa menjadi alternatif wisatawan untuk berwisata, yaitu:</p>
						<ul>
							<li>Pantai Pal, memiliki pasir putih dan ombak yang tidak terlalu besar ini merupakan pantai yang cukup ramai dikunjungi masyarakat Minahasa Utara.</li>
							<li>Pantai Pulisan, memiliki luas kurang lebih 500 meter dengan hamparan pasir putih dan air lautanya yang jernih, dipinggir pantai terdapat karang yang membuatnya semakin menarik untuk dipandang.</li>
							<li>Pulau Lihaga, merupakan sebuah pulau yang terletak diujung pulai sulawesi, pulau ini memiliki luas 8 Ha dan berada di wilayah administrasi Kecamatan Likupang Barat.</li>
							<li>Pulau Gangga, memiliki pantai berpasir putih, pulau gangga juga menawarkan pemandangan bawah lautnya yang mempesona.</li>
							<li>Air Terjun Tunan, memiliki ketinggian 86 meter dan sudah dikelola oleh pemerintah setempat sebagai objek wisata.</li>
						</ul>
				    	<hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Potensi Lokasi</h5>
				    	<p class="card-text">Terumbu Karang, Mangrove, Lamun, Paus, Lumba-lumba, Duyung, Penyu, Manta, Hiu, Ikan karang, Napoleon.</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Wisata yang Tersedia</h5>
				    	<p class="card-text">Menyelam, Snorkeling, Olahraga air, Goa, Tempat bersejarah, Pasar tradisional almadidi, Taman Nasional Gunung klabat</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Fasilitas Pendukung Wisata</h5>
				    	<p class="card-text">Penginapan, Restoran, Toko souvenir, Mercusuar dan Pelabuhan</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Transportasi</h5>
				    	<p class="card-text">Dari Kota Manado dapat ditempuh dengan kendaraan darat menuju Minahasa utara, sekitar 30 menit. Transportasi antar pulau dapat ditempuh dengan speed boat.</p>
				    	
				   	</div>
				  </div>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-12">
				<div class="card-deck">
				  <div class="card">
				    <div class="card-body text-justify">
				    	<div class="row">
				    		<div class="col-md-6">
				    		<div class="media">
				    		<div class="media-body">
				    			<div class="text-center" style="background-image: url('${pageContext.request.contextPath}/images/maps.png');height: 225px;width: 450px;"></div>
				    		</div>
					    	</div>
				    		</div>
					    	
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold">Detail Lokasi</h5>
				      			<p class="card-text">
				      				Please make your own way to: Lombok Elephant Park, Jalan Raya Tanjung, Nusa Tenggara Barat. 83352, Sire, Sigar Penjalin, Tanjung , Lombok Utara, Sigar Penjalin, Tanjung, North Lombok Regency, Nusa Tenggara Barat 83352, Indonesia
				      			</p>
					    	</div>
				    	</div>
				    </div>
				    <!-- <div class="card-footer bg-white">
				      <div class="row">
					      <div class="text-warning font-weight-bold col mt-2">RP. 50.000</div>
					      <div class="col"><button class="btn btn-warning  pull-right float-right">Buy Ticket</button></div>
				      </div>
				    </div> -->
				  </div>
				</div>
			</div>
		</div>
		
        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  
		</c:if>
		
		<c:if test="${param.step != null && param.step == 1}">		
		<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-book-1">
		<!-- start -->
		
        <div class="container-fluid" style="width:70%">
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			<h4 class="">Contact Details</h4>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-8">
			<div class="card">
			  <div class="card-header">
			    <h5>Contact Details (for E-ticket)</h5>
			  </div>
			    <div class="card-body">
				    <form>
					  <div class="form-group">
					    <label for="">Full Name<span class="text-danger">*</span></label>
					    <input type="text" class="form-control" id="">
					    <small id="" class="form-text text-muted">As on ID Card/passport/driving license (without degree or special characters).</small>
					  </div>
					  
					  <div class="form-row">
							<div class="col-md-6">
					    		<label for="">Mobile Number<span class="text-danger">*</span></label>
							</div>
							<div class="col-md-6">
					    		<label for="">Email<span class="text-danger">*</span></label>
							</div>
					  </div>
					  <div class="form-row">
							<div class="form-group col-md-2">
								<input type="text" class="form-control" name="" placeholder="+62">
							</div>
							<div class="form-group col-md-4">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
							<div class="form-group col-md-6">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
					  </div>
					  <div class="form-row">
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g +6282119260548 for Country (+62) and Mobile No. 082119260548</small>
							</div>
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g email@example.com</small>
							</div>
					  </div>
					</form>
			    </div>
			</div>
			
			<div class="p-3"></div>
			<h4 class="mb-3">Traveler Details</h4>
			<div class="card mb-3">
			  <div class="card-header">
			    <h5>Adult 1</h5>
			  </div>
			    <div class="card-body">
				    <form>
					  <div class="form-row">
							<div class="form-group col-md-3">
							    <label for="">Title<span class="text-danger">*</span></label>
							    <input type="text" class="form-control" id="">
							</div>
							<div class="form-group col">
							    <label for="">Full Name<span class="text-danger">*</span></label>
							    <input type="text" class="form-control" id="">
							</div>
					  </div>
					  
					  <div class="form-row">
							<div class="col-md-6">
					    		<label for="">Mobile Number<span class="text-danger">*</span></label>
							</div>
							<div class="col-md-6">
					    		<label for="">Email<span class="text-danger">*</span></label>
							</div>
					  </div>
					  <div class="form-row">
							<div class="form-group col-md-2">
								<input type="text" class="form-control" name="" placeholder="+62">
							</div>
							<div class="form-group col-md-4">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
							<div class="form-group col-md-6">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
					  </div>
					  <div class="form-row">
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g +6282119260548 for Country (+62) and Mobile No. 082119260548</small>
							</div>
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g email@example.com</small>
							</div>
					  </div>
					</form>
			    </div>
			</div>
			<div class="card mb-3">
			  <div class="card-header">
			    <h5>Adult 2</h5>
			  </div>
			    <div class="card-body">
				    <form>
					  <div class="form-row">
							<div class="form-group col-md-3">
							    <label for="">Title<span class="text-danger">*</span></label>
							    <input type="text" class="form-control" id="">
							</div>
							<div class="form-group col">
							    <label for="">Full Name<span class="text-danger">*</span></label>
							    <input type="text" class="form-control" id="">
							</div>
					  </div>
					  
					  <div class="form-row">
							<div class="col-md-6">
					    		<label for="">Mobile Number<span class="text-danger">*</span></label>
							</div>
							<div class="col-md-6">
					    		<label for="">Email<span class="text-danger">*</span></label>
							</div>
					  </div>
					  <div class="form-row">
							<div class="form-group col-md-2">
								<input type="text" class="form-control" name="" placeholder="+62">
							</div>
							<div class="form-group col-md-4">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
							<div class="form-group col-md-6">
							      <input type="text" class="form-control" name="" placeholder="">
							</div>
					  </div>
					  <div class="form-row">
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g +6282119260548 for Country (+62) and Mobile No. 082119260548</small>
							</div>
							<div class="col-md-6">
					    		<small id="" class="form-text text-muted">e.g email@example.com</small>
							</div>
					  </div>
					</form>
			    </div>
			</div>
			
			<button type="button" onclick="$('#btn-book-2').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			
			</div>
			
			<div class="col-md-4">
			<div class="card">
			<div class="card-header"><h5><span>Region</span> <a href="#" class="card-link">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				  <img style="width:70px;height:70px;" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="...">
				  <div class="media-body">
				    <h5 class="mt-0">Taman Wisata Komodo</h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Visit Date</span> <span class="float-right small">Wed, 27 Mar 2019</span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Time Slot</span> <span class="float-right small">Fri, 29 Mar 2019</span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span class="float-right small">General: 2</span></li>
			  </ul>
			  <div class="card-body">
			    <span class="text-success"><i class="fa fa-check-circle"></i> Refundable</span>
			  </div>
			</div>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-2" >
		<!-- start -->
        <div class="container-fluid" style="width:70%">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Vehicle Options</h4>
			<h4 class="mb-3"><small class="text-muted">Choose vehicle for you tour</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
			<ul class="nav nav-pills justify-content-center border-bottom border-info">
			  <li class="nav-item">
			    <a class="nav-link active" id="pills-car1-tab" data-toggle="pill" href="#pills-car1">Mini MVP</a>
 			 </li>
			  <li class="nav-item">
			    <a class="nav-link"  id="pills-car2-tab" data-toggle="pill" href="#pills-car2">SUV</a>
			  </li>
			</ul>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-car1" >
				
			<div class="card-deck">
			<div class="card mb-3">
  				<img src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2018/09/25/1537874598792-fedd7643bc6df88a74e69c237bd4bfc6.jpeg" class="card-img-top" alt="...">
			    <div class="card-body">
					  <div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title">All New Avanza</h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<small id="" class="form-text text-primary">Rp300.500 / Day</small>
							</div>
					  </div>
			    </div>
			</div>
			<div class="card mb-3">
  				<img src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2018/09/25/1537874598792-fedd7643bc6df88a74e69c237bd4bfc6.jpeg" class="card-img-top" alt="...">
			    <div class="card-body">
					  <div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title">Grand New Avanza</h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<small id="" class="form-text text-primary">Rp350.500 / Day</small>
							</div>
					  </div>
			    </div>
			</div>
			<div class="card mb-3">
  				<img src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2018/09/25/1537874598792-fedd7643bc6df88a74e69c237bd4bfc6.jpeg" class="card-img-top" alt="...">
			    <div class="card-body">
					  <div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title">Daihatsu Xenia</h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<small id="" class="form-text text-primary">Rp380.500 / Day</small>
							</div>
					  </div>
			    </div>
			</div>
			</div>
			</div>
			
			<div class="tab-pane fade" id="pills-car2" >
			<div class="card-deck">
			<div class="card mb-3">
  				<img src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2018/09/25/1537874598792-fedd7643bc6df88a74e69c237bd4bfc6.jpeg" class="card-img-top" alt="...">
			    <div class="card-body">
					  <div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title">Toyota Fortuner</h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<small id="" class="form-text text-primary">Rp980.500 / Day</small>
							</div>
					  </div>
			    </div>
			</div>
			<div class="card mb-3">
  				<img src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2018/09/25/1537874598792-fedd7643bc6df88a74e69c237bd4bfc6.jpeg" class="card-img-top" alt="...">
			    <div class="card-body">
					  <div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title">Mitsubishi Pajero</h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<small id="" class="form-text text-primary">Rp1.050.500 / Day</small>
							</div>
					  </div>
			    </div>
			</div>
			<div class="card mb-3">
  				<img src="https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2018/09/25/1537874598792-fedd7643bc6df88a74e69c237bd4bfc6.jpeg" class="card-img-top" alt="...">
			    <div class="card-body">
					  <div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title">Toyota Fortuner VRZ</h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<small id="" class="form-text text-primary">Rp1.080.500 / Day</small>
							</div>
					  </div>
			    </div>
			</div>
			</div>
			</div>
			</div>
			
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-3').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-3" >
			
        <div class="container-fluid" style="width:70%">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Tour Guide</h4>
			<h4 class="mb-3"><small class="text-muted">Choose someone to become your tour guide</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="card">
			<div class="card-body">
	    	<div class="row no-gutters text-center fs--1">
                <div class="col-6 col-md-4 col-xl-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="../pages/profile.html"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="https://prium.github.io/falcon/assets/img/team/1.jpg" alt="" width="100"></a>
                    <h6 class="mb-1"><a href="../pages/profile.html">Emilia Clarke</a></h6>
                    <p class="fs--2 mb-1"><a class="text-700" href="#!">Technext limited</a></p>
                  </div>
                </div>
                <div class="col-6 col-md-4 col-xl-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="../pages/profile.html"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="https://prium.github.io/falcon/assets/img/team/2.jpg" alt="" width="100"></a>
                    <h6 class="mb-1"><a href="../pages/profile.html">Kit Harington</a></h6>
                    <p class="fs--2 mb-1"><a class="text-700" href="#!">Harvard Korea Society</a></p>
                  </div>
                </div>
                <div class="col-6 col-md-4 col-xl-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="../pages/profile.html"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="https://prium.github.io/falcon/assets/img/team/3.jpg" alt="" width="100"></a>
                    <h6 class="mb-1"><a href="../pages/profile.html">Sophie Turner</a></h6>
                    <p class="fs--2 mb-1"><a class="text-700" href="#!">Graduate Student Council</a></p>
                  </div>
                </div>
                <div class="col-6 col-md-4 col-xl-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="../pages/profile.html"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="https://prium.github.io/falcon/assets/img/team/4.jpg" alt="" width="100"></a>
                    <h6 class="mb-1"><a href="../pages/profile.html">Peter Dinklage</a></h6>
                    <p class="fs--2 mb-1"><a class="text-700" href="#!">Art Club, MIT</a></p>
                  </div>
                </div>
                <div class="col-6 col-md-4 col-xl-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="../pages/profile.html"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="https://prium.github.io/falcon/assets/img/team/5.jpg" alt="" width="100"></a>
                    <h6 class="mb-1"><a href="../pages/profile.html">Nikolaj Coster</a></h6>
                    <p class="fs--2 mb-1"><a class="text-700" href="#!">Archery Club, MIT</a></p>
                  </div>
                </div>
                <div class="col-6 col-md-4 col-xl-3 col-xxl-2 mb-1">
                  <div class="bg-white p-3 h-100"><a href="../pages/profile.html"><img class="img-thumbnail img-fluid rounded-circle mb-3 shadow-sm" src="https://prium.github.io/falcon/assets/img/team/6.jpg" alt="" width="100"></a>
                    <h6 class="mb-1"><a href="../pages/profile.html">Isaac Hempstead</a></h6>
                    <p class="fs--2 mb-1"><a class="text-700" href="#!">Asymptones</a></p>
                  </div>
                </div>
              </div>
	  		</div>
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-2').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		</div>
		
		</div>
		</c:if>
      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="max-width:">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Booking Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body bg-light pt-0">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form" class="mt-3">
		  	<div class="form-row">
				<div class="form-group col-md-12">
					<input type="number" class="form-control" name="type" placeholder="Total Visitors">
				</div>
			</div>
		  	<div class="form-row">
				<div class="form-group col-md-6">
					<input type="date" class="form-control" name="name" placeholder="Check In">
				</div>
				<div class="form-group col-md-6">
				      <input type="date" class="form-control" name="type" placeholder="Check Out">
				</div>
			</div>
			  <!-- <div class="form-group">
			    <label class="font-weight-bold">Your Name</label>
			    <input type="text" class="form-control" name="services" placeholder="">
			  </div>
			  <div class="form-group">
			    <label class="font-weight-bold">Phone Number</label>
			    <input type="text" class="form-control" name="services" placeholder="">
			  </div>
			  <div class="form-group">
			    <label class="font-weight-bold">Your Message/Note</label>
			    <textarea class="form-control" name="description" rows="5"></textarea>
			  </div> -->
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" onclick="window.location.href='?id=1553051748240951&step=1'" class="btn btn-success pill pl-3 pr-3"><i class="fa fa-check"></i> Done</button>
	      </div>
	    </div>
	  </div>
	</div>
    <%@ include file = "inc/footer.jsp" %>
    <script>$('#container-search').hide()</script>
    <script src="${pageContext.request.contextPath}/js/general.js"></script>

  </body>

</html>
