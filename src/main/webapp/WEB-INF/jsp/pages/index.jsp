<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Currency"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionPhotoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionPhoto"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
RegionPhotoMapper regionPhotoMapper = appCtx.getBean(RegionPhotoMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
QueryParameter param = new QueryParameter();
Map <String, Object> params = new HashMap<>();
//
String paramfilter="";
paramfilter=request.getParameter("filter_keyword");
if(paramfilter==null)paramfilter="";

//param = new QueryParameter();
//param.setClause(param.getClause()+" AND "+Region.NAME+" LIKE  '%"+paramfilter+"%'");
params = new HashMap<>();
params.put("regionName", paramfilter);

if(!id.equals("0")){
	//param = new QueryParameter();
	//param.setClause(param.getClause()+" AND "+Region.ID+" =  '"+id+"'");
	params = new HashMap<>();
	params.put("regionId", id);
}

//List<Region> region = regionMapper.getList(param);
List<Region> region = regionMapper.getListPublic(params);
List<File> list_photo = new ArrayList<>();
List<RegionTodo> list_todo = new ArrayList<>();
List<RegionFacility> list_facility = new ArrayList<>();
if(region.size() == 1){
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.get(0).getId()+"'");
	list_photo = fileMapper.getList(param);

	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.get(0).getId()+"'");
	list_todo = regionTodoMapper.getList(param);
	region.get(0).setList_todo(list_todo);
	
	param = new QueryParameter();
	param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.get(0).getId()+"'");
	list_facility = regionFacilityMapper.getList(param);
	region.get(0).setList_facility(list_facility);
}
//param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
/* List<RegionFacility> regionFacility = regionFacilityMapper.getList(param); */
param = new QueryParameter();
List<RegionPhoto> regionPhoto = regionPhotoMapper.getList(param);



//untuk format currency
NumberFormat formatter = NumberFormat.getCurrencyInstance();
Locale myIndonesianLocale = new Locale("in", "ID");
formatter.setCurrency(Currency.getInstance(myIndonesianLocale));
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>

  <body id="page-top">
    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper" class="pt-0">

		<c:if test="${param.id == 0 || param.id == null}">
		<%-- <header class="image-bg-fluid-height hm-stylish-strong w-100" style="background: url('${pageContext.request.contextPath}/images/header-tn-salak2.png')no-repeat center;background-size: cover;height:250px"></header> --%>
		<div class="row">
		   <div class="col-md-12">
		      <header class="image-bg-fluid-height hm-stylish-strong w-100 d-flex" style="background: url('https://imgsrv2.voi.id/ouC3N0Qug2QKl_2MhGzDaFbagCbRkXWYuhpwouOTeKw/auto/1200/675/sm/1/bG9jYWw6Ly8vcHVibGlzaGVycy8zNDQwNy8yMDIxMDIyMDE1NDUtbWFpbi5qcGc.jpg')no-repeat center;background-size: cover;height: 350px;background-color: #495057;/* color: white; */background-blend-mode: overlay;">
		         <div class="new-intro-banner__content container text-white align-self-center" data-v-654185b6="">
		            <div class="row">
		               <div class="col-md-10">
		                  <!----> 
		                  <h1 class="font-weight-bold"><span data-test-id="intro-banner-title" class="new-intro-banner__content-title" data-v-654185b6="" style="
		                     /* font-size: 5rem; */
		                     /* line-height: 5.25rem; */
		                     /* font-weight: 600; */
		                     "><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
		                     Taman Nasional Gunung Halimun Salak
		                     </font></font></span>
		                  </h1>
		                  <div class="new-intro-banner__datepicker-wrapper" data-v-654185b6="" style="/* display: inline-block; *//* min-width: 610px; */">
		                     <p class="new-intro-banner__content-abstract" data-v-654185b6="" style="
		                        font-size: 1.5rem;
		                        font-weight: 700;
		                        line-height: 1.875rem;
		                        margin: 0 0 24px;
		                        max-width: 640px;
		                        text-shadow: 1px 1px rgb(0 0 0 / 70%);
		                        "><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Selamat Datang di Aplikasi Kawasan,<br/> Aplikasi kawasan merupakan aplikasi online yang dapat membantu pembelian tiket wisata masuk kawasan.</font></font></p>
		                  </div>
		               </div>
		               <div class="col-md-auto"><img src="/kawasan/images/logo-tnghs.png" class="align-self-start mr-3 d-none d-md-block" alt="..." style="width: 144px;">
		               </div>
		            </div>
		         </div>
		      </header>
		   </div>
		</div>
		
		<div class="row ">
			<div class="col-md-12 bg-white">
			 <div class="container-fluid">
			 <div class="row justify-content-center">
			   <!--  <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label> -->
			    <div class="col-sm-8">
			    <form id="search-form">
					<div class="input-group mb-3 mt-3">
					  <input name="filter_keyword" type="text" class="form-control form-control-lg" placeholder="Enter destination name or activity" value="<% out.print(paramfilter); %>">
					  <div class="input-group-append">
					    <button  style="" class="btn btn-outline-secondary " type="submit" id="btn-search">Search!</button>
					  </div>
					  <c:if test="${param.view == 1}">
					  	<a href="${pageContext.request.contextPath}/page/general" class="btn btn-default ml-1"><i class="fas fa-th fa-2x "></i></a>
					  </c:if>
					  <c:if test="${param.view == '' ||  param.view == null}">
					  	<a href="${pageContext.request.contextPath}/page/general?view=1" class="btn btn-outline-secondary ml-1"><i class="fas fa-list fa-2x text-muted"></i></a>
					  </c:if>
					</div>
				</form>
			    </div>
			  </div>
			  </div>
			</div>
		</div>
		</c:if>
		
		<c:if test="${param.id != null}">
		
		 <div class="container mb-3">
		 <div class="row justify-content-center">
		    <div class="col-md-6">
				<a href='${pageContext.request.contextPath}/page/general' class="btn btn-primary btn-sm px-4 px-sm-5 float-left">Back</a>
			</div>
		    <div class="col-md-6">
				<button onclick="$('#container-search').toggle()" class="btn btn-primary btn-sm px-4 px-sm-5 float-right" type="button">Change Search</button>
			</div>
		  </div>
		  </div>
		
		 <div id="container-search" class="container" style="display: none;">
		 <div class="row  justify-content-center">
		   <!--  <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label> -->
		    <div class="col-md-12">
				<div class="container-fluid pb-3 pt-3	bg-white">
				    <form id="search-form">
						<div class="input-group mb-3 mt-3">
						  <input name="filter_keyword" type="text" class="form-control" placeholder="Enter destination name or activity" value="<% out.print(paramfilter); %>">
						  <div class="input-group-append">
						    <button  style="border-top-right-radius: 23px!important;border-bottom-right-radius: 23px!important;" class="btn btn-outline-light text-muted" type="submit" id="btn-search">Search!</button>
						  </div>
						</div>
					</form>
			    </div>
		    </div>
		  </div>
		  </div>
		
        <div class="container">
		
		<div class="row mt-3">
			<div class="col-md-8">
			<div class="card mb-3">
				<!-- <div style="max-height: 450px;height: 450px;background-image: url('https://d1nabgopwop1kh.cloudfront.net/v2/experience-asset/guys1L+Yyer9kzI3sp/pb0CG1j2bhflZGFUZOoIf1YMmzMd/HaD8U/YhZI+EvjMMnSJBgizSbB+q3P+OTt4rnG98M/3uI16G3tLN17gcLc6PsaHRlPhwKyEDXJ3hZPmbanBtM8D6JzU42qyM4VaPjiMqUlXTdzUr2jNOPsddnkC49390o5IOgU1/OoIc4cRxiqiStapzN6HYw6bU84dDsQ=='); background-size: cover;"> -->
				<!--Carousel Wrapper-->
				<div>
				<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
				  <!--Slides-->
				  <div class="carousel-inner" role="listbox">
				  <% 
				  int key=0;
					  for(File o2 : list_photo){
					  	o2.setName(o2.getName().replace(" ", "%"));
					  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
					  	if(key==0){
						  	out.println("<div class='carousel-item active'>");
						  		out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
					      	out.println("</div>");
					  	}else if(key>0){	
						  	out.println("<div class='carousel-item'>");
						  		out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
					      	out.println("</div>");
					  	}
				      	key++;
					  }
				  %>
				  </div>
				  <!--/.Slides-->
				  <!--Controls-->
				  <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				  <!--/.Controls-->
				  <ol class="carousel-indicators">
					  <% 
					  key=0;
					  for(File o2 : list_photo){
						o2.setName(o2.getName().replace(" ", "%"));
					  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
					  	if(key==0){
						  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
						  	out.println("<li style='width: 6.25rem!important;' data-target='#carousel-thumb' data-slide-to="+key+" class='active'>");
						  	out.println("<img class='d-block w-100' src="+path+" class='img-fluid'></li>");
					  	}else if(key>0){
					  		String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
						  	out.println("<li style='width: 6.25rem!important;' data-target='#carousel-thumb' data-slide-to="+key+">");
						  	out.println("<img class='d-block w-100' src="+path+" class='img-fluid'></li>");
					  	}
				      	key++;
					  } 
					  %>
				  </ol>
				</div>
				<!--/.Carousel Wrapper-->
				</div>
			<!-- </div> -->
            <div class="card-body d-none">
              <div class="row justify-content-between align-items-center">
                <div class="col">
                  <div class="media">
                     <div class="media-body fs--1">
                      <h6 class="fs-0"><% out.println(region.get(0).getName()); %></h6>
                      <p class="mb-0"><i class="fas fa-map-marker-alt"></i> <% out.println(region.get(0).getLocationDesc()); %></p>
                     <!--  <p class="mb-0 text-justify">
                      	Minahasa Utara menyuguhkan berbagai tempat yang tidak kalah menarik untuk dikunjungi. Berikut ini 5(lima) tempat wisata di Minahasa Utara yang mungkin bisa menjadi alternatif wisatawan untuk berwisata
                      </p> -->
                    </div>
                  </div>
                </div>
                
				  <%
				  	int price_start_from=0;
					for(RegionFacility o3 : list_facility){
						price_start_from+=o3.getPrice()==null?0:o3.getPrice();
					}
				  %>
                <div class="col-md-auto mt-4 mt-md-0">
                	<div class="">
                		<p><span class="fs-0 font-weight-light">Mulai Dari</span></p>
                		<!-- <h5 class="fs-0 text-orange font-weight-light">Rp. 50.000 - Rp. 90.000</h5> -->
                		<h5 class="fs-0 text-orange font-weight-light"><span><% out.print(formatter.format(price_start_from)); %></span></h5>
                		<!-- <button id="btn-book" data-toggle="modal" data-target="#modal-form" class="btn btn-sm px-4 px-sm-5 btn-block bg-orange" type="button">Book Now</button> -->
                		<a tabindex="0" class="btn btn-lg btn-block bg-orange">Book Now</a>
                	</div>
                </div>
              </div>
            </div>
          </div>
			</div>
			<div class="col-md-4 p-0 ">
		    <div class="card mb-3 ">
		        <div class="card-body">
		            <div class="row justify-content-between align-items-center">
		                <div class="col">
		                    <div class="media">
		                        <div class="media-body fs--1">
		                            <h5 class="fs-0"><% out.println(region.get(0).getName()); %></h5>
		                            <p class="mb-0"><i class="fas fa-map-marker-alt"></i> <% out.println(region.get(0).getLocationDesc()); %></p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <hr>
		            <div class="row">
		                <div class="col">
		                    <form id="entry-form-popover">
		                        <div class="form-row">
		                            <div class="form-group col-md-6">
		                                <input autocomplete="off" type="text" class="form-control single-date-picker" id="start_date" name="start_date" placeholder="Check In"> </div>
		                            <div class="form-group col-md-6">
		                                <input autocomplete="off" type="text" class="form-control single-date-picker" id="end_date" name="end_date" placeholder="Check Out"> </div>
		                        </div>
		                        <div class="form-row">
		                            <div class="form-group col-md-12">
								        <span>Kuota Tersedia : <span class="text-success"><% out.println(region.get(0).getSisaKuota()); %></span></span>
		                            </div>
		                        </div>
		                        <div class="form-row g">
		                            <div class="form-group col-md-12">
								        <input onkeyup="checkKuota();" type="number" class="form-control form-control-sm" name="count_ticket" placeholder="Number of travelers/visitors" value="1" min="1" max="<% out.println(region.get(0).getSisaKuota()); %>">
		                            </div>
		                        </div>
		                    </form>
		                </div>
		            </div>
		            <div class="row justify-content-between align-items-center">
		                <div class="col">
		                    <%-- <p><span class="fs-0 font-weight-light">Start From</span></p>
		                    <!-- <h5 class="fs-0 text-orange font-weight-light">Rp. 50.000 - Rp. 90.000</h5> -->
		                    <h5 class="fs-0 text-orange font-weight-light"><span><% out.print(formatter.format(price_start_from)); %></span></h5>
		                     --%><!-- <button id="btn-book" data-toggle="modal" data-target="#modal-form" class="btn btn-sm px-4 px-sm-5 btn-block bg-orange" type="button">Book Now</button> -->
		                    <a onclick="save();" tabindex="0" class="btn btn-lg btn-block bg-orange example-popover" data-original-title="" title="">Book Now</a>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-12">
			<div class="card">
			  <div class="card-header">
			    <ul class="nav nav-tabs card-header-tabs">
			      <li class="nav-item">
			        <a class="nav-link active" data-toggle="tab" href="#nav-info">Overview</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link " data-toggle="tab" href="#nav-desc">Itenerary</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" data-toggle="tab" href="#nav-perhatian">Inclusions & Exclusions</a>
			      </li>
			      <% if(region.get(0).getRefund()!=null) {
			      if(region.get(0).getRefund().equals("1")) { %>
			      <li class="nav-item">
			        <a class="nav-link" data-toggle="tab" href="#nav-refund">Cancellation Policy</a>
			      </li>
			      <% } } %>
			    </ul>
			  </div>
			  <div class="tab-content" id="nav-tabContent">
				  <div class="tab-pane fade" id="nav-desc" role="tabpanel" aria-labelledby="nav-profile-tab">
					  <div class="card-body">
				      	<div class="accordion" id="accordionExample">
						  <% 
						  key=0;
						  String row="";
						  for(RegionTodo o3 : list_todo){
							if(o3.getParent().isEmpty() || o3.getParent().equals("")) {
								row+="<div class='card'>";
								row+="<div class='bg-light p-1' id='heading"+key+"'>";
								row+="<h2 class='mb-0'>";
									row+="<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#collapse"+key+"' aria-expanded='true' aria-controls='collapseOne'>DAY "+o3.getDay()+" : "+o3.getActivity()+"</button>";
					          	row+="</h2>";
					          	row+="</div>";
							
					          	if(key==0){
					          		row+="<div id='collapse"+key+"' class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>";
					          	}else{
					          		row+="<div id='collapse"+key+"' class='collapse' aria-labelledby='headingOne' data-parent='#accordionExample'>";
						         }
					          	row+="<div class='card-body'>";
					          	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
								  for(RegionTodo o4 : list_todo){
									if(o3.getId().equals(o4.getParent())){
					          			row+="<p><i class='far fa-calendar-check'></i> "+dateFormat.format(o4.getTime())+" : "+o4.getActivity()+"</br></p>";
									}
								  }
					          		row+="</div>";
							    row+="</div>";
							    
							    row+="</div>";
							}
					      	key++;
						  } 
						  out.print(row);
						  %>
						  
						</div>
				      	
					  </div>
				  </div>
				  <% if(region.get(0).getRefund()!=null) 	if(region.get(0).getRefund().equals("1")) { %>
				  <div class="tab-pane fade" id="nav-refund" role="tabpanel" aria-labelledby="nav-profile-tab">
					  <div class="card-body">
					    <!-- <h5 class="card-title">Special title treatment 2</h5> -->
					    <p>For a full refund, cancel at least 24 hours in advance of the start date of the experience.</p>
				    	<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
					  </div>
				  </div>
				  <%} %>
				  <div class="tab-pane fade show active" id="nav-info" role="tabpanel">
					<div class="card-body">
					    <p class="card-text"><% out.print(region.get(0).getDescription()); %></p>
					</div>
				  </div>
				  <div class="tab-pane fade" id="nav-perhatian" role="tabpanel">
					<div class="card-body">
				    	<div class="row">
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold"><i class="fas fa-check-circle fa-1x text-success mr-3"></i> Inclusions </h5><!-- Harga Termasuk -->
				      			<ul>
								  <%
								  	key=0;
									for(RegionFacility o3 : list_facility){
										if(o3.isInclude().equals("Y")){
									  		out.println("<li>"+o3.getItem()+"</li> ");
										}
										key++;
									}
								  %>
							    </ul>
					    	</div>
				    		<div class="col-md-6">
				      			<h5 class="card-title font-weight-bold"><i class="fas fa-times-circle text-danger mr-3"></i> Exclusions</h5> <!-- Harga Tidak Termasuk -->
				      			<ul>
								  <% 
								  	key=0;
									for(RegionFacility o3 : list_facility){
										if(o3.isInclude().equals("N")){
									  		out.println("<li>"+o3.getItem()+"</li> ");
										}
										key++;
									}
								  %>
							    </ul>
					    	</div>
				    	</div>
				    </div>
				  </div>
				</div>
			</div>
			</div>
		</div>
		
		<div class="row mt-3 d-none">
			<div class="col-md-12">
				<div class="card-deck">
				  <div class="card">
				    <div class="card-body text-justify">
				      	<h5 class="card-title font-weight-bold">Deskripsi</h5>
				    	<p class="card-text">Minahasa Utara menyuguhkan berbagai tempat yang tidak kalah menarik untuk dikunjungi. Berikut ini 5(lima) tempat wisata di Minahasa Utara yang mungkin bisa menjadi alternatif wisatawan untuk berwisata, yaitu:</p>
						<ul>
							<li>Pantai Pal, memiliki pasir putih dan ombak yang tidak terlalu besar ini merupakan pantai yang cukup ramai dikunjungi masyarakat Minahasa Utara.</li>
							<li>Pantai Pulisan, memiliki luas kurang lebih 500 meter dengan hamparan pasir putih dan air lautanya yang jernih, dipinggir pantai terdapat karang yang membuatnya semakin menarik untuk dipandang.</li>
							<li>Pulau Lihaga, merupakan sebuah pulau yang terletak diujung pulai sulawesi, pulau ini memiliki luas 8 Ha dan berada di wilayah administrasi Kecamatan Likupang Barat.</li>
							<li>Pulau Gangga, memiliki pantai berpasir putih, pulau gangga juga menawarkan pemandangan bawah lautnya yang mempesona.</li>
							<li>Air Terjun Tunan, memiliki ketinggian 86 meter dan sudah dikelola oleh pemerintah setempat sebagai objek wisata.</li>
						</ul>
				    	<hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Potensi Lokasi</h5>
				    	<p class="card-text">Terumbu Karang, Mangrove, Lamun, Paus, Lumba-lumba, Duyung, Penyu, Manta, Hiu, Ikan karang, Napoleon.</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Wisata yang Tersedia</h5>
				    	<p class="card-text">Menyelam, Snorkeling, Olahraga air, Goa, Tempat bersejarah, Pasar tradisional almadidi, Taman Nasional Gunung klabat</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Fasilitas Pendukung Wisata</h5>
				    	<p class="card-text">Penginapan, Restoran, Toko souvenir, Mercusuar dan Pelabuhan</p><hr/>
				    	
				    	<h5 class="card-title font-weight-bold">Transportasi</h5>
				    	<p class="card-text">Dari Kota Manado dapat ditempuh dengan kendaraan darat menuju Minahasa utara, sekitar 30 menit. Transportasi antar pulau dapat ditempuh dengan speed boat.</p>
				    	
				   	</div>
				  </div>
				</div>
			</div>
		</div>
		
		<div class="row mt-3">
			<div class="col-md-12">
				<div class="card-deck">
				  <div class="card">
				    <div class="card-body text-justify">
				    	<div class="row">
				    		<div class="col-auto">
				    		<div class="media">
				    		<div class="media-body">
				    			<div class="text-center" style="background-image: url('${pageContext.request.contextPath}/images/maps.png');height: 225px;width: 450px;"></div>
				    		</div>
					    	</div>
				    		</div>
					    	
				    		<div class="col-md-6 d-none">
				      			<h5 class="card-title font-weight-bold">Detail Lokasi</h5>
				      			<p class="card-text">
				      				Please make your own way to: Lombok Elephant Park, Jalan Raya Tanjung, Nusa Tenggara Barat. 83352, Sire, Sigar Penjalin, Tanjung , Lombok Utara, Sigar Penjalin, Tanjung, North Lombok Regency, Nusa Tenggara Barat 83352, Indonesia
				      			</p>
					    	</div>
				    	</div>
				    </div>
				    <!-- <div class="card-footer bg-white">
				      <div class="row">
					      <div class="text-warning font-weight-bold col mt-2">RP. 50.000</div>
					      <div class="col"><button class="btn btn-warning  pull-right float-right">Buy Ticket</button></div>
				      </div>
				    </div> -->
				  </div>
				</div>
			</div>
		</div>
		
        </div>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  
		</c:if>
		
		<c:if test="${param.id == 0 || param.id == null}">
        <div class="container"> 
	        <div class="row mt-3">
	        	<div class="col-12">
	        		<h5>Silahkan Pilih Jalur Pendakian</h5>
	        		<hr/>
	        	</div>
	        </div>
			<div id="container-general" class="row">
			<%
			String row="";
			String row_2="";
			int key=0;
			List<File> list_photo_master = fileMapper.getList(new QueryParameter());
			for(Region o : region){
				param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+RegionPhoto.ACTIVE+"='"+1+"'");
				List<RegionPhoto> list_photo_transction  = regionPhotoMapper.getList(param);
				
				param = new QueryParameter();
		    	param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+o.getId()+"'");
	    		List<RegionFacility> regionFacility = regionFacilityMapper.getList(param);
	  	
	            int price_start_from=0;
	            for(RegionFacility o3 : regionFacility){
	                price_start_from+=o3.getPrice()==null?0:o3.getPrice();
	            }
	                
				row += "<div class='col-md-4 mt-3'>";
					row += "<div class='card'>";
						row_2 += "<div class='card mt-3'>";
						row_2 += "<div class='row no-gutters'>";
							row_2 += "<div class='col-md-3'>";
				
						for(RegionPhoto rp: list_photo_transction) {
							String path=request.getContextPath()+"/images/notfound.png";
							path=request.getContextPath()+"/files/"+rp.getFileId()+"?filename="+rp.getNameFile()+"&download";
					  		
							if(rp.getHeaderId().equals(o.getId())  && rp.isActive()){
						  		row += "<img src='"+path+"' class='card-img-top'>";
								row_2 += "<img src='"+path+"' class='card-img'>";
							}
						}
							row_2 += "</div>";
							row_2 += "<div class='col-md-7'>";
							row_2 += "<div class='card-body'>";
								row_2 += "<h5 class='card-title'>"+o.getName()+"</h5>";
								row_2 += "<p class='card-text text-truncate' style='white-space: pre;'>"+o.getDescription()+"</p>";
								row_2 += "<p class='card-text'><small class='text-muted'><i class='fas fa-map-marker-alt'></i> "+o.getLocationDesc()+"</small></p>";
								//row_2 += "<a href='#' class='btn btn-primary'>Find ticket</a>";
							row_2 += "</div>";
							row_2 += "</div>";
	
							row_2 += "<div class='col-md-2'>";
							row_2 += "<div class='card-body'>";
								//row_2 += "<h5 class='card-title'>"+formatter.format(price_start_from)+"</h5>";
								row_2 += "<p><h5 class='text-success'><span>"+formatter.format(price_start_from)+"</span></h5></p>";
								row_2 += "<a href='"+request.getContextPath()+"/page/general?id="+o.getId()+"' class='btn btn-primary'>Find ticket</a>";
							row_2 += "</div>";
							row_2 += "</div>";
						row_2 += "</div>";
						row_2 += "</div>";
						
					row += "<div class='card-body'>";
				    	row += "<div class='card-title'>"+o.getName()+"</div>";
				    		row += "<small class='text-muted'><i class='fas fa-map-marker-alt'></i> "+o.getLocationDesc()+"</small>";
				      row += "</div>";
				    row += "<div class='card-footer bg-white'>";
				    	row += "<div class='row'>";
				    	
				    	
				    		row += "<div class='text-success col mt-2 d-none'><span>"+formatter.format(price_start_from)+"</span></div>";
				    			row += "<div class='col'><a href='"+request.getContextPath()+"/page/general?id="+o.getId()+"' class='btn btn-sm btn-primary pull-right float-right'>Check Ticket</a></div>";
					      row += "</div>";
				      row += "</div>";
				    row += "</div>";
				  row += "</div>"; 
				  key++;
			}
			%>
			<c:if test="${param.view == 1}">
			    <div class="col">
				        <%out.print(row_2);%>
			    </div>
			</c:if>
			<c:if test="${param.view == '' ||  param.view == null}">
				<%out.print(row);%>
		  	</c:if>
			</div>
        </div>
       	</c:if>
       	
        <!-- /.container-fluid -->

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    <%@ include file = "inc/footer-general.jsp" %>
    
    <script src="${pageContext.request.contextPath}/js/general.js"></script>
	
	<footer class="bg-light py-5">
            <div class="container px-4 px-lg-5"><div class="small text-center text-muted">Copyright � 2022 - Taman Nasional Gunung Halimun Salak</div></div>
    </footer>
  </body>

</html>
