<%@page import="id.co.hijr.sistem.common.Utils"%>
<%@page import="id.co.hijr.sistem.service.VaultService"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilLuggage"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransitCamp"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="java.util.Date"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransaction"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Currency"%>
<%@page import="id.co.hijr.ticket.mapper.BankMapper"%>
<%@page import="id.co.hijr.ticket.model.Bank"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.HashSet"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
PurchaseDetilTransactionMapper purchaseDetilTransactionMapper = appCtx.getBean(PurchaseDetilTransactionMapper.class);
PurchaseDetilVisitorMapper purchaseDetilVisitorMapper = appCtx.getBean(PurchaseDetilVisitorMapper.class);
PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper = appCtx.getBean(PurchaseDetilTransitCampMapper.class);
PurchaseDetilLuggageMapper purchaseDetilLuggageMapper = appCtx.getBean(PurchaseDetilLuggageMapper.class);

BankMapper bankMapper = appCtx.getBean(BankMapper.class);
String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
PurchaseDetilTransaction purchaseDetilTransaction = purchaseDetilTransactionMapper.getEntityHeader(id);
 
System.out.print("OIKAN");
/*System.out.print(purchaseDetilTransaction); */
//Purchase purchase= purchaseMapper.getEntity(id);

QueryParameter param = new QueryParameter();
param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
Purchase purchase= purchaseMapper.getListExtended(param).get(0); //getEntity(id);

try {
	purchase.getPurchaseDetilTransitCamp().get(0);
} catch (Exception e) {
	List<PurchaseDetilTransitCamp> pdtc = new ArrayList<>();
	PurchaseDetilTransitCamp p = new PurchaseDetilTransitCamp();
	p.setCodeUnique("");
	p.setNameTransitCamp("-");
	p.setReportTimeCamp("-");
	pdtc.add(p);
	purchase.setPurchaseDetilTransitCamp(pdtc);
	// TODO: handle exception
}

QueryParameter param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.HEADER_ID + " = '"+purchase.getId()+"')");
List<PurchaseDetilVisitor> dataPurchaseDetilVisitor = purchaseDetilVisitorMapper.getList(param_detil);
//purchase.setPurchaseDetilVisitor(dataPurchaseDetilVisitor);

param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+purchase.getId()+"')");
List<PurchaseDetilTransaction> dataPurchaseDetilTransaction = purchaseDetilTransactionMapper.getList(param_detil);
//purchase.setPurchaseDetilTransaction(dataPurchaseDetilTransaction);

param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+purchase.getId()+"')");
List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param_detil);
//purchase.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);

param_detil = new QueryParameter();
param_detil.setOrder(PurchaseDetilLuggage.TYPE_LUGGAGE);
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilLuggage.HEADER_ID+ " = '"+purchase.getId()+"')");
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilLuggage.STATUS+ " is "+" null "+")");
List<PurchaseDetilLuggage> dataPurchaseDetilLuggage = purchaseDetilLuggageMapper.getList(param_detil);
purchase.setPurchaseDetilLuggage(dataPurchaseDetilLuggage);

/* String purchasedetilvisitor = ",";
List<String> arraypurchasedetilvisitor = new ArrayList<String> ();
for(PurchaseDetilVisitor p : purchaseDetilVisitorMapper.getEntityHeader(id)){
	
	if(p.getResponsible().equals("0")) arraypurchasedetilvisitor.add(p.getFullName());
} */


List<Bank> bank = bankMapper.getList(new QueryParameter());


NumberFormat formatter = NumberFormat.getCurrencyInstance();
Locale myIndonesianLocale = new Locale("in", "ID");
formatter.setCurrency(Currency.getInstance(myIndonesianLocale));


//untuk format tanggal atau waktu
SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy", myIndonesianLocale);
SimpleDateFormat sdftime = new SimpleDateFormat("hh.mm aa");
SimpleDateFormat sdftimeleft = new SimpleDateFormat("hh:mm:ss");
SimpleDateFormat sdf_MY = new SimpleDateFormat("MMMM yyyy");

VaultService vaultService = appCtx.getBean(VaultService.class);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	
    <link href="${pageContext.request.contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<style>
		.bg-holder {
		    position: absolute;
		    width: 100%;
		    min-height: 100%;
		    top: 0;
		    left: 0;
		    background-size: cover;
		    background-position: center;
		    overflow: hidden;
		    will-change: transform,opacity,filter;
		    -webkit-backface-visibility: hidden;
		    backface-visibility: hidden;
		    background-repeat: no-repeat;
		    z-index: 0;
		}
		
		.bg-card {
		    background-size: contain;
		    background-position: right;
		    border-top-right-radius: .375rem;
		    border-bottom-right-radius: .375rem;
		}
	</style>
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/tracking.css">
    <title>E-Ticket</title>
    <link rel="icon" type="image/png" sizes="32x32" href="${contextPathPublic}/images/logo-tnghs.png">
  </head>
  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">
    
<%-- <nav class="navbar navbar-expand-lg navbar-light bg-white">
<div class="container p-3">

  <a class="navbar-brand" href="${pageContext.request.contextPath}/page/general">
    <img src="${pageContext.request.contextPath}/images/icon-ciremai.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
    Kawasan
  </a>
  

</div>
</nav> --%>

  <div class="container">
   
    	<%
    		Date datenow = new Date();
    		//datenow.setHours(0);
    		//datenow.setMinutes(0);
    		//datenow.setSeconds(0);
    	%>
    <c:choose>
         <c:when test = "${fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_OPERATOR')}">
         	<%-- <c:redirect url="/page/general"/> --%>
         <%-- ${fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN')} --%>
         <% if(purchase.getStartDate().after(datenow) && request.getParameter("force")==null && purchase.getStatusBoarding()==null) {%>
			  <div class="row justify-content-center text-center pb-3">  
			  <div class="col-md-12">
		      <div class="card mt-3">
		         <div class="card-body">
		         	<div class="card-title">Mohon maaf, Waktu Boarding, belum dibuka.</div>
		         </div>
	         </div>
	         </div>
	         </div>
         <% }else if(purchase.getEndDate().before(datenow) && request.getParameter("force")==null && purchase.getStatusBoarding()==null) {%>
			  <div class="row justify-content-center text-center pb-3">  
			  <div class="col-md-12">
		      <div class="card mt-3">
		         <div class="card-body">
		         	<div class="card-title">Mohon maaf, batas Waktu Boarding, telah lewat, <a href="?id=<% out.print(purchase.getId()); %>&force=TRUE">tetap lanjutkan</a>.</div>
		         </div>
	         </div>
	         </div>
	         </div>
	        
        <div class="row d-none">
	      <div class="col-md-12 col-lg-12">
	        <div id="tracking-pre"></div>
			<div id="tracking">
	            <div id="tracking-status" class="text-center ">
	               <p class="tracking-status text-tight"></p>
	            </div>
	            <div id="body-tracking" class="tracking-list">
	               <div class="tracking-item">
	                  <div class="tracking-icon status-outfordelivery bg-success">
	                     <i class="fas fa-check"></i>
	                  </div>
	                  <div class="tracking-date">Jul 20, 2018<span>08:58 AM</span></div>
	                  <div class="tracking-content">BOARDING MASUK<span>Boarding telah dilakukan</span></div>
	               </div>
	
				   <!-- <div class="tracking-item">
	                  <div class="tracking-icon status-intransit bg-primary">
	                     <i class="fas fa-circle"></i>
	                  </div>
	                  <div class="tracking-date">Aug 10, 2018<span>05:01 PM</span></div>
	                  <div class="tracking-content">TELAH_DIBACA<span>Pertanyaan anda telah diterima oleh pihak terkait</span></div>
	               </div> -->
	               
	               <div class="tracking-item">
	                  <div class="tracking-icon status-intransit">
	                     <i class="fas fa-circle"></i>
	                  </div>
	                  <div class="tracking-date">Aug 10, 2018<span>11:19 AM</span></div>
	                  <div class="tracking-content">BOARDING KELUAR<span>Boarding belum dilakukan.</span></div>
	               </div>
	               
	            </div>
	         </div>
	      </div>
	   </div>
         <% }else{ %>
         	
		<div class="row justify-content-center">
		   <div class="col-md-8">
		      <div class="card mb-3">
		         <div class="bg-holder d-nonea d-lg-block bg-card" style="background-image:url('https://prium.github.io/falcon/v2.8.0/default/assets/img/illustrations/corner-4.png');opacity: 0.7;"></div>
		         <div class="card-body">
		             <%
				   	String titleform="Masuk";
					if(purchase.getStatusBoarding()!=null){
					   	if(purchase.getStatusBoarding().equals("1")) titleform="Keluar";
					   	if(purchase.getStatusBoarding().equals("2")) titleform="Selesai";
					}	
				   %>
		            <div class="row">
		            <h5 class="col">Pemeriksaan <% out.print(titleform); %></h5>
		            <a class='col d-none'><span class="">Kembali ke Halaman Utama</span></a>
		            </div>
		            <p class="d-none fs--1">BOOKING ID: #2737<br>April 21, 2019, 5:33 PM</p>
		            
		            <div class="d-none">
		               <strong class="mr-2">Status: </strong>
		               
		               <div class="badge rounded-pill bg-success fs--2">Completed <span class="fa fa-check ml-1" data-fa-transform="shrink-2"></span></div>
		            
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		<div class="row justify-content-center">
		   <div class="col-md-8">
		      <div class="card mb-3">
		         <div class="card-body">
		            <div class="row">
		               <div class="col-6 col-md-4">
		                  <h5 class="fs-0 mb-3 ">BOOKING DETAILS</h5>
		                  <h6 class="mb-2">Jalur</h6>
		                  <p class="mb-2 fs--1">Hari, Tanggal Pendakian</p>
		                  <p class="mb-2 fs--1">Transit Camp</p>
		                  <p class="mb-2 fs--1">Jam Wajib Lapor di BC</p>
		                  <p class="mb-2 fs--1 d-none">Nomor Kapling</p>
		                  <p class="mb-2 fs--1">Nama Ketua</p>
		                  <p class="mb-2 fs--1">Jumlah Pendaki</p>
		                  <p class="mb-2">Total Pembayaran</p>
		                  <p class="mb-2">Kode Booking</p>
		               </div>
		               <%
		               

						param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
						Purchase purchaseBoarding= purchaseMapper.getListBoarding(param).get(0);//getListExtended(param).get(0); //getEntity(id);
						purchase.setCountTicketBoarding(purchaseBoarding.getCountTicketBoarding());
						purchase.setAmountTicket(purchaseBoarding.getAmountTicketBoarding());
		               %>
		               <div class="col-6 col-md-8">
		                  <h5 class="mb-3 fs-0"></h5>
		                  <div class="media">
		                     <img class="mr-3" src="" width="40" alt=""> 
		                     <!-- ../assets/img/icons/visa.jpg -->
		                     <div class="media-body">
		                        <h6 class="mb-2">: <% out.print(purchase.getRegion().getName()); %></h6>
		                        <h6 class="mb-2">: <% out.print(sdf.format(purchase.getStartDateSerialize())); %></h6>
		                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp()); %></p>
		                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp()); %></p>
		                        <p class="mb-2 fs--1 d-none">: <% out.print(purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique()); %></p>
		                        <p class="mb-2 fs--1 font-weight-bold">: <% out.print(purchase.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName()); %></p>
			                  	<p class="mb-2 fs--1 font-weight-bold">: <a class="text-decoration-none" title="klik untuk melihat detail" onclick="" href="javascript:void(0)"><% out.print(purchase.getCountTicketBoarding()); %></a></p>
				                <p class="mb-2 fs--1 ">: <% out.print(formatter.format(purchase.getAmountTicket())); %></p>
		                        <p class="mb-2 fs--1 ">: <% out.print(purchase.getCodeBooking()); %></p>
		                     </div>
		                  </div>
		               </div>
		            </div>
		            
		         </div>
		      </div>
		   </div>
		</div>
		
		<div class="row justify-content-center">
		   <div class="col-md-8">
		      <div class="card mb-3">
		         <div class="card-body">
				   <div class="row">
				      <div class="col-md-6">
				         <h5 class="card-title text-muted ">PENDAKI DETAILS</h5>
				      </div>
				      <div class="col-md-6 d-none">
				         <button class="btn btn-light pill float-right">Tambahkan Pendaki</button>
				      </div>
				   </div>
				   <div class="row table-responsive">
				      <div class="col-md-12 table-responsive">
				        <table id="tbl-detil-individu" class="table table-sma table-striped table-hover m-0" style="opacity: 0.8;overflow: auto; display: inline-table;">
				           <thead class="bg-dark text-white align-middle">
				              <tr>
				                 <th scope="col" colspan="2" class="text-center"><a class=" text-link text-decoration-none" id="tab-pendaki" href="#" data-toggle="tooltip" data-placement="top" title="Silahkan cek salah satu pendaki yang akan dibanned">#</a></th>
				                 <th scope="col">No.Identitas</th>
				                 <th scope="col">Nama Lengkap</th>
				                 <th scope="col">Jenis Kelamin</th>
				                 <th scope="col">No.HP</th>
				                 <th scope="col"></th>
				              </tr>
				           </thead>
				           <tbody class="bg-light text-dark">
				           <% 
				          	//if(purchase.getParentId()!=null){
					           param = new QueryParameter();
					           param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
					           param.setInnerClause(param.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is "+" NULL "+")");
					           Purchase purchaseParent= purchaseMapper.getListExtended(param).get(0);
					           
					           purchase.setPurchaseDetilVisitor(purchaseParent.getPurchaseDetilVisitor());
				          	//}
				           	String mode="";
				           	if(purchase.getStatusBoarding()!=null){
							   	if(purchase.getStatusBoarding().equals("1")) mode="KELUAR";
							   	if(purchase.getStatusBoarding().equals("2")) mode="SELESAI";
							}
				           int key=0;
				           int no=1; String numberFam=""; String addressFam="";
				           	for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
				           		if(key==0){ %>
				           			<%-- <tr data-visitor="<% out.print(pdv.getId()); %>" class="data-row-identity bg-success" id="row-<% out.print(pdv.getId()); %>">   
					           			<td><% out.print("<input name='id_visitor' value="+pdv.getId()+" type='checkbox'>"); %></td>
					            		<td><% out.print(no); %></td>   
					            		<td><% out.print(pdv.getNoIdentity()); %></td>   
					            		<td class=""> <% out.print(pdv.getFullName()); %> </td>   
					            		<td class=""> <% out.print(pdv.getGender()); %> </td>   
					            		<td class=""> <% out.print(pdv.getPhoneNumber()); %> </td>  
					            		<td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getFileId()); %>?filename=<% out.print(pdv.getNameFile()); %>&download','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
					            	</tr> --%>
					            	<% 
					            	numberFam=pdv.getPhoneNumberFamily();
					            	addressFam=pdv.getAddressFamily();
				           		}
				           		
				           		if(key>0){
				           			if(mode.equals("KELUAR") || mode.equals("SELESAI")) {
					           			if(pdv.isIsValid()!=null) {
					           				%>
					           		<tr data-visitor="<% out.print(pdv.getId()); %>" class="data-row-identity" id="row-<% out.print(pdv.getId()); %>">
					           			<td><% out.print("<input data-id-visitor="+pdv.getIdVisitorIdentity()+" name='id_visitor' checked disabled value="+pdv.getId()+" type='checkbox'>"); %></td>
					            		<td><% out.print(no); %></td>   
					            		<td><% out.print(vaultService.decrypt(pdv.getVisitorIdentity().getNoIdentity())); %></td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getFullName()); %> </td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getGender()); %> </td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getPhoneNumber()); %> </td>  
					            		<td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getVisitorIdentity().getIdFile()); %>?filename=<% out.print(pdv.getVisitorIdentity().getFileName()); %>&download&decrypt','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
					            	</tr>
					           				<%
					           			}
				           			}else{
				           		%>
				           		
				           			<tr data-visitor="<% out.print(pdv.getId()); %>" class="data-row-identity" id="row-<% out.print(pdv.getId()); %>">
					           			<td><% out.print("<input name='id_visitor' value="+pdv.getId()+" type='checkbox'>"); %></td>
					            		<td><% out.print(no); %></td>   
					            		<td><% out.print(vaultService.decrypt(pdv.getVisitorIdentity().getNoIdentity())); %></td>     
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getFullName()); %> </td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getGender()); %> </td>   
					            		<td class=""> <% out.print(pdv.getVisitorIdentity().getPhoneNumber()); %> </td>  
					            		<td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getVisitorIdentity().getIdFile()); %>?filename=<% out.print(pdv.getVisitorIdentity().getFileName()); %>&download&decrypt','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
					            	</tr>
					            	
				           		<%
				           			} 
					           		no++;
				           		}
				           		key++;
				           	}
				           %>
				           	
				          </tbody>
				        </table>
				      </div>
				   </div>
				   <hr>
				   <form>
				      <div class="form-group">
				         <label for="exampleInputEmail1">No.HP Keluarga</label>
				         <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" readonly="" value="<% out.print(numberFam); %>">
				         <small id="emailHelp" class="form-text text-muted">*nomor handphone keluarga yang bisa dihubungi.</small>
				      </div>
				      <div class="form-group">
				         <label for="exampleInputPassword1">Alamat Keluarga</label>
				         <textarea class="form-control" id="exampleInputPassword1" readonly="" rows="3"> <% out.print(addressFam);%></textarea>
				      </div>
				   </form>
				</div>
		      </div>
		   </div>
		</div>
		
		<div class="row justify-content-center">
		   <div class="col-md-8">
		      <div class="card mb-3">
		         <div class="card-body">
				<div class="row">
				    <div class="col-md-12">
				        <h5 class="card-title text-muted">LUGGAGE DETAILS</h5>
				    </div>
				</div>
				<div class="row">
				     <div class="col-md-12">
				    	<div class="table-responsive">
				        <table id="tbl-detil-luggage" class="table table-responsive table-sma table-striped table-hover m-0" style="opacity: 0.8;overflow: auto; display: inline-table;">
				           <thead class="bg-dark text-white align-middle">
				              <tr>
				                 <th scope="col" colspan="2" class="text-center">#</th>
				                 <th scope="col">Barang Bawaan</th>
				                 <th scope="col">Pribadi/Kelompok/<br/>Logistik/Lainnya</th>
				                 <th scope="col">QTY</th>
				                 <th scope="col">QTY<br/>Actual</th>
				              </tr>
				           </thead>
				           <tbody class="bg-light text-dark">
				           
				           <% 
				           	String valueformsampah="";
				           	no=1;
				           	for(PurchaseDetilLuggage pdl : purchase.getPurchaseDetilLuggage()){
								if(pdl.getTypeLuggage()==null) valueformsampah=pdl.getDescLuggage();
				           		if(mode.equals("KELUAR") || mode.equals("SELESAI")) {
				           			if(pdl.isIsValidLuggage()!=null) {
				           		%>
					           		<tr data-luggage="<% out.print(pdl.getId()); %>" class="data-row-luggage" id="row-<% out.print(pdl.getId()); %>">
						           		<td><% out.print("<input checked name='id_luggage' value="+pdl.getId()+" type='checkbox' disabled>"); %></td>
						           		<td><% out.print(no); %></td>
						           		<td class=""> <% out.print(pdl.getNameLuggage()); %> </td>   
						           		<td class=""><% out.print(pdl.getTypeLuggage()); %></td>   
						           		<td class=""><% out.print(pdl.getQtyLuggage()); %></td>  
						           		<td class=""><% out.print(pdl.getQtyActualLuggage()); %></td>  
						           	</tr>
					           	
				           		<%
				           			no++;
				           			}
				           		}else{
								%>
									<tr data-luggage="<% out.print(pdl.getId()); %>" class="data-row-luggage" id="row-<% out.print(pdl.getId()); %>">
						           		<td><% out.print("<input name='id_luggage' value="+pdl.getId()+" type='checkbox'>"); %></td>
						           		<td><% out.print(no); %></td>
						           		<td class=""> <% out.print(pdl.getNameLuggage()); %> </td>   
						           		<td class=""><% out.print(pdl.getTypeLuggage()); %></td>   
						           		<td class=""><% out.print(pdl.getQtyLuggage()); %></td>  
						           		<td class=""><% out.print("<input name='qty_actual_luggage' class='form-control' type='number' value="+pdl.getQtyLuggage()+">"); %></td>  
						           	</tr>
								<%
					           			no++;
				           		}
				           	}
				           	%>
				          </tbody>
				        </table>
				        </div>
				       <hr> 
					   <form>
					      <div class="form-group">
					         <label for="exampleInputPassword1">Potensi Sampah</label>
					         <% String enableformsampah="";
					         if(purchase.getStatusBoarding()!=null) enableformsampah="disabled"; %>
					         <textarea class="form-control" name="deskripsi_potensi_sampah" <% out.print(enableformsampah); %> rows="10" placeholder="Beri keterangan potensi sampah yang akan dihasilkan, agar mudah diverifikasi ketika turun/keluar pendakian.."><% out.print(valueformsampah); %></textarea>
					      </div>
					      
				         <% String enableformsertifikat="d-none";
				         if(purchase.getStatusBoarding()!=null)
				         if(purchase.getStatusBoarding().equals("1")){%>
					      <div class="form-group mt-3">
					         <label for="exampleInputPassword1">Kirim Sertifikat</label>
					         <select class="form-control" name="send_sertifikat">
					        	<option value="">TIDAK, JANGAN KIRIM </option>
					        	<% if(datenow.before(purchase.getEndDate()) || Utils.formatSqlDate(purchase.getEndDate()).equals(Utils.formatSqlDate(datenow))) %><option value="1">YA, KIRIM</option>
					         </select>
					      </div>
					      <% if(purchase.getEndDate().before(datenow)) { %>
					      <div id="form-keterangan-sertifikat" class="form-group mt-3">
					         <label for="">Keterangan</label>
					         <textarea class="form-control" name="description_message" rows="10" placeholder="Beri keterangan/alasan pendaki tidak menerima sertifikat.."></textarea>
					      </div>
					      <%} 
					      %>
					      <div id="form-keterangan-sertifikat" class="form-group mt-3">
					         <label for="">Banned Pendaki </label>
					         <select class="form-control" name="banned_pendaki">
					        	<option value="NO">TIDAK, JANGAN BANNED </option>
					        	<option value="YES">YA, LAKUKAN BANNED </option>
					         </select>
					          <span class="text-muted small">*Pilih Ya, apabila ingin melakukan blacklist pendaki</span>
					      </div>
					      <%
					      } %>
					   </form>
				     </div>
				  </div>
		         </div>
		      </div>
		   </div>
		</div>
		
		
	   <div class="row justify-content-center text-right pb-3">
		   <div class="col-md-8">
		   <%
	        if(purchase.getStatusBoarding()!=null){
	        	//out.print(purchase.getStartDate()+".after"+(datenow));
		 		//if(datenow.after(purchase.getStartDate())) {
	 			if(datenow.after(purchase.getEndDate())) {
			   		if(purchase.getStatusBoarding().equals("1") ) {%>
			   			<button onclick="doAction(<% out.print(purchase.getId());%>, 'KELUAR')" class="btn btn-success"><i class="fas fa-check-circle"></i> SUBMIT</button>
			   <%   }
		        }
         	}else if(purchase.getStatusBoarding()==null) {%>
		   		<button onclick="doAction(<% out.print(purchase.getId());%>, 'BOARDING')" class="btn btn-success"><i class="fas fa-check-circle"></i> SUBMIT</button>
		   <% } %>
		   	</div>
	   </div>
	   
		<!-- <div class="row justify-content-center text-center pb-3">
		   <div class="col-md-8"><button class="btn btn-success">SUBMIT <i class="fas fa-check-circle"></i></button></div>
	   </div> -->
   		
   		<% } %>
   		
         </c:when>
         
         <c:otherwise>
			  <div class="row justify-content-center text-center pb-3">  
			  <div class="col-md-12">
		      <div class="card mt-3">
		         <div class="card-body">
		         	<div class="card-title">Hanya ADMIN/OPERATOR yang dapat mengakses halaman ini...</div>
		         </div>
	         </div>
	         </div>
	         </div>
         </c:otherwise>
   </c:choose>
   

   <!-- Optional JavaScript; choose one of the two! -->
   <!-- Option 1: Bootstrap Bundle with Popper -->
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   <!-- Option 2: Separate Popper and Bootstrap JS -->
   <!--
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
   -->
   <%--  <%@ include file = "inc/footer-general.jsp" %> --%>
    
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
    
	<script src="${pageContext.request.contextPath}/vendor/loadingoverlay.min.js"></script>
	
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script>var tokenCookieName = "${cookieName}"</script>
	<script>var localeCookieName = "${localeCookieName}"</script>
   

    
    <script>
    var path = ctx + '/ticket/purchase';
    function getCookieValue(a) {
        var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
        return b ? b.pop() : '';
    }


    function ajaxPOST(url,obj,fnsuccess, fnerror){
    	$.ajax({
    	    url : url,
    	    method: "POST",
    	    crossDomain: true,
    	    contentType: false,
    	    processData: false,
    	    data : obj,
    	    cache: false,
    	    success : function (response) {
    		    	var fn = window[fnsuccess];
    	    		if(typeof fn === 'function') {
    	    		    fn(response);
    	    		}
    	    },
    	    error : function (response) {
    			var fn = window[fnerror];
    	    		if(typeof fn === 'function') {
    	    		    fn(response);
    	    		}
    	    },
    	});
    }
    function ajaxGET(url, fnsuccess, fnerror){
    	$.ajax({
    	    url: url,
    	    method: "GET",
    	    success: function (response) {
    		    	var fn = window[fnsuccess];
    	    		if(typeof fn === 'function') {
    	    		    fn(response);
    	    		}
    	    },
    	    error: function (response) {
    			var fn = window[fnerror];
    	    		if(typeof fn === 'function') {
    	    		    fn(response);
    	    		}
    	    }
    		
    	});	
    }
    
    $.ajaxSetup({
		headers : {
			'Authorization' : 'Bearer '+ getCookieValue(tokenCookieName)
		}
	});

	
    var selected_action="";
    function swall_view(src, phonenumber, address){
		var kontent;//='<p class="font-weigh-bold card-title">Informasi Keluarga yang bisa dihubungi</p><p>Nomor.HP : '+phonenumber+'</p> <p>Alamat Rumah : '+address+'</p>';
		
		if(phonenumber=="null") kontent="";
		Swal.fire({
		  //title: 'Informasi Keluarga yang bisa dihubungi',
		  html: kontent,
		  imageUrl: src,
		  imageWidth: 400,
		  imageHeight: 200,
		  imageAlt: 'Custom image',
		})
	}

    function onModalActionSuccess(response) {

		$.LoadingOverlay("hide");
	    Swal.fire(
	      'Success!',
	      'Boarding berhasil.',
	      'success'
	    ).then((result) => {
  		  if (result.isConfirmed) {
  			window.location.reload();
   		  }
   		})
    }
    function doAction(id, action, confirm = 0){
		if(action=='BOARDING'){
	        if(getDataArr().visitor.length<4){
	            alert("Minimal pendaki yg boleh naik adalah 4 orang.")
				return;
	        }
			 if(getDataArr().luggage.length==0){
	            alert("Barang bawaan pendaki, belum dicek list.")
				return;
	        }
		}
	        
		if($('[name=deskripsi_potensi_sampah]').val()==""){
            alert("Form potensi sampah masih kosong.")
			return;
        }
		$.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
    	selected_action = action;
    	if(confirm == 0) ajaxGET(path + '/'+id+'?action='+selected_action,'onPrepareModalActionSuccess','onActionError');
    	else {
    		var obj = new FormData();
    		obj.append('detil_visitor', JSON.stringify(getDataArr().visitor));
    		obj.append('detil_luggage', JSON.stringify(getDataArr().luggage));
    		obj.append('potensi_sampah', $('[name=deskripsi_potensi_sampah]').val());
    		obj.append('send_sertifikat', $('[name=send_sertifikat]').val());
    		obj.append('description_message', $('[name=description_message]').val());
    		obj.append('banned_pendaki', $('[name=banned_pendaki]').val());
        	ajaxPOST(path + '/'+id+'/'+selected_action,obj,'onModalActionSuccess','onActionError');
        }
    }

    function onActionError(){
    	$.LoadingOverlay("hide");
    }
    
    function onPrepareModalActionSuccess(response) {

		$.LoadingOverlay("hide");
    	var value = response.data;
    	selected_id = value.id;

    	swal.fire({
  		  title: 'Are you sure?',
  		  text: response.message,
  		  icon: 'warning',
  		  showCancelButton: true,
  		  confirmButtonColor: '#3085d6',
  		  cancelButtonColor: '#d33',
  		  confirmButtonText: 'Sudah, Benar!'
  		}).then((result) => {
  		  if (result.isConfirmed) {
  		  	doAction(selected_id, selected_action, 1);
  		  }
  		})
    	
    }

    function getDataArr(){
    	var obj = {
    		visitor: [],
    		luggage: [],
    	}
    	//visitor
    	$.each($("[data-visitor]"), function(key){
    		visitor = {
          		id: "",
       			id_visitor: "",
    			is_valid_visitor: "",
    		}
    		visitor['id'] = $(this).find('[name=id_visitor]:checked').data('idVisitor');
    		visitor['id_visitor'] = $(this).find('[name=id_visitor]:checked').val();
    		visitor['is_valid_visitor'] = "1";
    		if($(this).find('[name=id_visitor]:checked').val()!=undefined) obj['visitor'].push(visitor);
    	});
    	//luggage
    	$.each($("[data-luggage]"), function(key){
    		luggage = {
    			id_luggage: "",
    			qty_actual_luggage: "",
    		}
    		luggage['id_luggage'] = $(this).find('[name=id_luggage]:checked').val();
    		luggage['qty_actual_luggage'] = $(this).find('[name=qty_actual_luggage]').val();
    	
    		if($(this).find('[name=id_luggage]:checked').val()!=undefined) obj['luggage'].push(luggage);
        });
    	return obj;
    }

    $('[name=send_sertifikat]').on('change', function(){
		if(this.value!=1){
			$('[name=keterangan_sertifikat]').prop('disabled', false);
			$("#form-keterangan-sertifikat").show();
		}else {
			$('[name=keterangan_sertifikat]').prop('disabled', true);
			$("#form-keterangan-sertifikat").hide();
		}
    });
    
    $('[name=banned_pendaki]').on('change', function(){
		if(this.value=='YES'){
			$("#tab-pendaki").focus();
			$('#tab-pendaki').tooltip('show');

			$('[name=id_visitor]').prop('disabled',false);
			$('[name=id_visitor]').prop('checked',false);
		}
    });

   
    </script>
</div>

<div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header alert-primary">
        <h5 class="modal-title" id="">Detail Pendaki</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <p id="modal-alert-view-msg"></p>
      <div class="row">
     <div class="col-md-12">
        <table id="tbl-detil-individu" class="table table-responsive table-sma table-striped table-hover m-0" style="opacity: 0.8;overflow: auto; display: inline-table;">
           <thead class="bg-dark text-white">
              <tr>
                 <th scope="col">#</th>
                 <th scope="col">No.Identitas</th>
                 <th scope="col">Nama Lengkap</th>
                 <th scope="col">Jenis Kelamin</th>
                 <th scope="col">No.HP</th>
                 <th scope="col"></th>
              </tr>
           </thead>
           <tbody class="bg-light text-dark">
           <%-- <% int no=1;
           	for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
           		if(no==1){ %>
           			<tr class="data-row-identity bg-success" id="row-<% out.print(pdv.getId()); %>">   
	            		<td><% out.print(no); %></td>   
	            		<td><% out.print(pdv.getNoIdentity()); %></td>   
	            		<td class=""> <% out.print(pdv.getFullName()); %> </td>   
	            		<td class=""> <% out.print(pdv.getGender()); %> </td>   
	            		<td class=""> <% out.print(pdv.getPhoneNumber()); %> </td>  
	            		<td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getFileId()); %>?filename=<% out.print(pdv.getNameFile()); %>&download','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
	            	</tr>
           		<% }
           		if(no>2){ %>
           			<tr class="data-row-identity" id="row-<% out.print(pdv.getId()); %>">   
	            		<td><% out.print(no-1); %></td>   
	            		<td><% out.print(pdv.getNoIdentity()); %></td>   
	            		<td class=""> <% out.print(pdv.getFullName()); %> </td>   
	            		<td class=""> <% out.print(pdv.getGender()); %> </td>   
	            		<td class=""> <% out.print(pdv.getPhoneNumber()); %> </td>  
	            		<td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getFileId()); %>?filename=<% out.print(pdv.getNameFile()); %>&download','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td>
	            	</tr>
           		<% }
           		no++;
           	}
           %> --%>
           	
          </tbody>
        </table>
     </div>
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>