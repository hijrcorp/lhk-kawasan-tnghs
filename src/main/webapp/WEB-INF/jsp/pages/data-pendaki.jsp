<%@page import="id.co.hijr.sistem.common.Utils"%>
<%@page import="id.co.hijr.ticket.model.VisitorIdentity"%>
<%@page import="java.util.Date"%>
<%@page import="id.co.hijr.ticket.ref.StatusPurchase"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="java.util.Currency"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionPhotoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionPhoto"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
RegionPhotoMapper regionPhotoMapper = appCtx.getBean(RegionPhotoMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);

PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
PurchaseDetilVisitorMapper purchaseDetilVisitorMapper = appCtx.getBean(PurchaseDetilVisitorMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
System.out.print("PRINT====");

QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND "+Purchase.STATUS+" ='"+StatusPurchase.PAYMENT+"'");
param.setClause(param.getClause()+" AND "+Purchase.STATUS_BOARDING+" ='"+1+"'");

if(request.getParameter("filter_region")!=null) param.setClause(param.getClause()+" AND "+Purchase.REGION_ID+" ='"+request.getParameter("filter_region")+"'");
//if(request.getParameter("filter_visitor")!=null) param.setClause(param.getClause()+" AND "+Purchase.REGION_ID+" ='"+request.getParameter("filter_region")+"'");
if(request.getParameter("filter_start_date")!=null && request.getParameter("filter_end_date")!=null){
	param.setClause(param.getClause()+" AND "+Purchase.START_DATE+" between '"+request.getParameter("filter_start_date")+"' AND '"+request.getParameter("filter_end_date")+"'");
	System.out.print("POARAM: "+param.getClause());
}

if(request.getParameter("filter_keyword")!=null) param.setClause(param.getClause()+" AND "+VisitorIdentity.FULL_NAME+" ='"+request.getParameter("filter_keyword")+"'");
	
List<Purchase> dataPurchase= purchaseMapper.getListExtended(param);

String paramfilter="";
if(request.getParameter("filter_keyword")==null)paramfilter="";

//untuk format currency
NumberFormat formatter = NumberFormat.getCurrencyInstance();
Locale myIndonesianLocale = new Locale("in", "ID");
formatter.setCurrency(Currency.getInstance(myIndonesianLocale));

SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy", new Locale("in"));
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>

  <body id="page-top">
    <%@ include file = "inc/navbar.jsp" %>

    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper" class="pt-0">

		<c:if test="${param.id == 0 || param.id == null}">
		<%-- <header class="image-bg-fluid-height hm-stylish-strong w-100" style="background: url('${pageContext.request.contextPath}/images/header-tn-salak2.png')no-repeat center;background-size: cover;height:250px"></header> --%>
		<div class="row">
		   <div class="col-md-12">
		      <header class="image-bg-fluid-height hm-stylish-strong w-100 d-flex" style="background: url('https://imgsrv2.voi.id/ouC3N0Qug2QKl_2MhGzDaFbagCbRkXWYuhpwouOTeKw/auto/1200/675/sm/1/bG9jYWw6Ly8vcHVibGlzaGVycy8zNDQwNy8yMDIxMDIyMDE1NDUtbWFpbi5qcGc.jpg')no-repeat center;background-size: cover;height: 350px;background-color: #495057;/* color: white; */background-blend-mode: overlay;">
		         <div class="new-intro-banner__content container text-white align-self-center" data-v-654185b6="">
		            <div class="row">
		               <div class="col-md-10">
		                  <!----> 
		                  <h1 class="font-weight-bold"><span data-test-id="intro-banner-title" class="new-intro-banner__content-title" data-v-654185b6="" style="
		                     /* font-size: 5rem; */
		                     /* line-height: 5.25rem; */
		                     /* font-weight: 600; */
		                     "><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
		                     Taman Nasional Gunung Halimun Salak
		                     </font></font></span>
		                  </h1>
		                  <div class="new-intro-banner__datepicker-wrapper" data-v-654185b6="" style="/* display: inline-block; *//* min-width: 610px; */">
		                     <p class="new-intro-banner__content-abstract" data-v-654185b6="" style="
		                        font-size: 1.5rem;
		                        font-weight: 700;
		                        line-height: 1.875rem;
		                        margin: 0 0 24px;
		                        max-width: 640px;
		                        text-shadow: 1px 1px rgb(0 0 0 / 70%);
		                        "><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kagumi gunung paling ikonik di Jawa Barat saat Anda menemukan sejarahnya di tengah hutan dan objek wisata lainnya yang menakjubkan.</font></font></p>
		                  </div>
		               </div>
		               <div class="col-md-auto"><img src="/kawasan/images/logo-tnghs.png" class="align-self-start mr-3 d-none d-md-block" alt="..." style="width: 144px;">
		               </div>
		            </div>
		         </div>
		      </header>
		   </div>
		</div>
		
		<div class="row ">
			<div class="col-md-12 bg-white">
			 <div class="container-fluid">
			 <div class="row justify-content-center">
			   <!--  <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label> -->
			    <div class="col-sm-8">
			    <form id="search-form">
					<div class="input-group mb-3 mt-3">
					  <input name="filter_keyword" type="text" class="form-control" placeholder="Untuk mencari Ketik Nama Lengkap Pendaki " style="border-top-left-radius: 23px; border-bottom-left-radius: 23px; padding: 1rem;" value="<% out.print(paramfilter); %>">
					  <div class="input-group-append">
					    <button  style="border-top-right-radius: 23px!important;border-bottom-right-radius: 23px!important;" class="btn btn-outline-dark text-muted" type="submit" id="btn-search">Search!</button>
					  </div>
					  <c:if test="${param.view == 1}">
					  	<a href="${pageContext.request.contextPath}/page/general" class="btn btn-default ml-1"><i class="fas fa-th fa-2x text-muted"></i></a>
					  </c:if>
					  
	        		  <c:if test = "${fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN') || fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_OPERATOR')  || fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_SUPERVISOR')}">
		         		  <c:if test="${param.view == '' ||  param.view == null}">
						  	<a href="javascript:void(0)" onclick="filterPendaki()" class="btn border-rounded btn-outline-dark ml-1"><i class="fas fa-filter text-muted"></i></a>
						  </c:if>
				  	  </c:if>
					</div>
				</form>
			    </div>
			  </div>
			  </div>
			</div>
		</div>
		</c:if>
		
		<c:if test="${param.id == 0 || param.id == null}">
        <div class="container-fluid container-fluid-md">
        <div class="card mt-3">
	    <div class="card-header bg-default text-white">
		    <h5 class="card-titles">Daftar Pendaki</h5>
		</div>
		<div class="card-body p-1">
		<div class="table-responsive">
		
	   <c:choose>
	         <c:when test = "${!fn:containsIgnoreCase(roleList, 'ROLE_SUPER_ADMIN') && !fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_OPERATOR')  && !fn:containsIgnoreCase(roleList, 'ROLE_KAWASAN_SUPERVISOR')}">
	         	
				<table class="table table-hover table-sm table-striped">
				   <thead class="thead-light">
				      <tr>
				         <th scope="col" width="5%">No</th>
				         <th scope="col" width="10%">Jalur</th>
				         <th scope="col" width="20%">Tanggal Naik</th>
				         <th scope="col" width="20%">Tanggal Turun</th>
				         <th scope="col" width="25%">Nama Pendaki</th>
				         <th scope="col" width="15%">Status Pendaki</th>
				      </tr>
				   </thead>
				   <tbody>
				   <% 
				   	if(request.getParameter("filter_keyword")!=null) param.setClause(param.getClause()+" AND "+VisitorIdentity.FULL_NAME+" ='"+request.getParameter("filter_keyword")+"'");
				 	
				 	dataPurchase= purchaseMapper.getListExtended(param);
				 
				   	int no=1;
				   	if(request.getParameter("filter_keyword")!=null)
				   	for(Purchase op : dataPurchase) {
				         for(PurchaseDetilVisitor opdv : op.getPurchaseDetilVisitor()){
				        	 if(opdv.getResponsible().equals("0")){
					        	/* if(request.getParameter("filter_visitor")!=null && request.getParameter("filter_visitor").equals(opdv.getVisitorIdentity().getFullName())) {
					        		 
					        		out.print("<div class='bg-info'>- "+opdv.getVisitorIdentity().getFullName()+"</div>");
					         	
					        	}else{
					        		
					         		out.print("<div>- "+opdv.getVisitorIdentity().getFullName()+"</div>");
					        	
					        	} */
					        	%>
							      <tr>
							         <td><% out.print(no);%></td>
							         <td scope="row"><% out.print(op.getRegion().getName()+"<div class='d-none'>("+op.getTotalDays()+" Hari)</div>"); %></td>
							         <td><% out.print(sdf.format(op.getStartDate())); %></td>
							         <td><% out.print(sdf.format(op.getEndDate())); %></td>
							         <td><% out.print(opdv.getVisitorIdentity().getFullName()); %></td>
							         	<%
								         	String statusBor_name="";
											String statusBor_color="";
											String statusBor_icon="";
											if(op.getStatusBoarding().equals("1")) {
												statusBor_name="SEDANG_NAIK";
												if(op.getEndDate().after(new Date()) || Utils.formatDate(op.getEndDate()).equals(Utils.formatDate(new Date()))) {
													statusBor_color="primary";
												}else{
													statusBor_color="danger";
													statusBor_icon="exclamation-circle";
												}
											}else if(op.getStatusBoarding().equals("2")) {
												statusBor_name="SELESAI";
												statusBor_color="success";
											}else {
												statusBor_name="BELUM_BOARDING";
												if(op.getEndDate().before(new Date())) {
													statusBor_color="danger";
													statusBor_icon="exclamation-circle";
												}else{
													statusBor_color="warning";
												}
											}
									 	%>
							         <td><span class="badge badge-<%out.print(statusBor_color);%>"><% out.print(statusBor_name); %></span></td>
							      </tr>
					        	<%
					        	no++;
				        	 }
				         }
			         }
					%>
				   </tbody>
				</table>
				
	         </c:when>
	         <c:otherwise>
	         	
		      	<table class="table table-bordered table-sm table-striped">
				   <thead class="thead-light">
				      <tr>
				         <th scope="col" width="10%">Jalur</th>
				         <th scope="col" width="20%">Tanggal Naik</th>
				         <th scope="col" width="20%">Tanggal Turun</th>
				         <th scope="col" width="25%">Nama Pendaki</th>
				         <th scope="col" width="25%">Alamat Pendaki</th>
				      </tr>
				   </thead>
				   <tbody>
				   <% for(Purchase op : dataPurchase) { %>
				      <tr class="row-rtc" data-id_photo="1616597370374802" data-name_photo="Apuy TC I Sanghyang Rangkah.jpg" data-id="9" id="row-rtc-9" onclick="$('[name=id_master_transit]').val(9);setActiveRow(this);createFormTent(37, 21, 'SR', this)">
				         <td scope="row"><% out.print(op.getRegion().getName()+"<div class='d-none'>("+op.getTotalDays()+" Hari)</div>"); %></td>
				         <td><% out.print(sdf.format(op.getStartDate())); %></td>
				         <td><% out.print(sdf.format(op.getEndDate())); %></td>
				         <td>
					         <% 
						         for(PurchaseDetilVisitor opdv : op.getPurchaseDetilVisitor()){
						        	 if(opdv.getResponsible().equals("0")){
							        	if(request.getParameter("filter_visitor")!=null && request.getParameter("filter_visitor").equals(opdv.getVisitorIdentity().getFullName())) {
							        		 out.print("<div class='bg-info'>- "+opdv.getVisitorIdentity().getFullName()+"</div>");
							         	}else{
							         		out.print("<div>- "+opdv.getVisitorIdentity().getFullName()+"</div>");
							        	}
						        	 }
						         }
					         %>
				         </td>
				         <td class="">
				            <%
					         for(PurchaseDetilVisitor opdv : op.getPurchaseDetilVisitor()){
					        	 if(opdv.getResponsible().equals("0")){
					           		 String myaddress = opdv.getAddress()!=null?opdv.getAddress():"";
						           	 if(request.getParameter("filter_address")!=null && myaddress.contains(request.getParameter("filter_address"))) {
						         		out.print("<div class='bg-info'>- "+myaddress+"</div>");
						        	 }else{
						        		out.print("<div>- "+opdv.getAddress()+"</div>");
						        	 }
					        	 }
					        }
					        %>
				         </td>
				      </tr>
				      <% 
					   }
				      if(dataPurchase.size()==0) {
				    	  out.print("<tr class='text-center'><td colspan='5'>Data tidak ada!</td></tr>");
				      }
					   %>
				   </tbody>
				</table>
	         </c:otherwise>
	   </c:choose>
	   
		
		</div>
		</div>
		</div>
        </div>
       	</c:if>
        <!-- /.container-fluid -->

        <%-- <%@ include file = "inc/trademark.jsp" %> --%>  

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
    
	<!-- modal -->
	<div class="modal fade" id="modal-form-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-md" role="document">
	    <div class="modal-content  ">
     	<form id="form-filter">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Filter Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert" style="display: none;"></div>
			  
			  <div class="form-row">
                  <div class="form-group col-md-12">
                      <select name="filter_region" class="form-control">
                      	<option value="">Pilih Jalur</option>
                      	 <%
                      	 List<Region> dataRegion = regionMapper.getList(new QueryParameter());
				         for(Region reg : dataRegion){
							String myregion = reg.getId()!=null?reg.getId():"";
				        	if(request.getParameter("filter_region")!=null && request.getParameter("filter_region").equals(myregion)){
				            	out.print("<option selected value='"+reg.getId()+"'>"+reg.getName()+"</option>");
				        	}else{
				        		out.print("<option value='"+reg.getId()+"'>"+reg.getName()+"</option>");
				        	}
		         		 }
		         		%>
                      </select> 
                  </div>
              </div>
              
			  <div class="form-row ">
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="date" class="form-control "  name="filter_start_date" placeholder="Tanggal Pendakian"> 
                  </div>
                  <div class="form-group col-md-6">
                      <input autocomplete="off" type="date" class="form-control "  name="filter_end_date" placeholder="Tanggal Pendakian">
                  </div>
              </div>
			  <div class="form-row">
                  <div class="form-group col-md-12">
                      <input type="text" name="filter_visitor" class="form-control" placeholder="Nama Pendaki" value="<% out.print(request.getParameter("filter_visitor")!=null?request.getParameter("filter_visitor"):""); %>">
                  </div>
              </div>
			  <div class="form-row">
                  <div class="form-group col-md-12">
                      <textarea name="filter_address" class="form-control" placeholder="Alamat Pendaki"><% out.print(request.getParameter("filter_address")!=null?request.getParameter("filter_address"):""); %></textarea> 
                  </div>
              </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-dark pill pl-5 pr-5" data-dismiss="modal">Close</button>
	        <button type="button" onclick="filterPendaki(2)" class="btn btn-warning pill pl-5 pr-5" >Reset</button>
	        <button type="button" onclick="filterPendaki(1)" class="btn btn-primary pill pl-5 pr-5">Filter</button>
	      </div>
		</form>
	    </div>
	  </div>
	</div>
	
    <%@ include file = "inc/footer.jsp" %>
    <script src="${pageContext.request.contextPath}/js/general.js"></script>
   
	<script type="text/javascript">
		$("#search-form").submit(function(e){
			if($('[name=filter_keyword]').val()=="") {
				alert("fiter tidak boleh kosong.");
				e.preventDefault();
			}
		});
		
		function filterPendaki(mode){
			if(mode==1) {
				window.location="?"+$('#form-filter').serialize().replace(/[^&]+=\.?(?:&|$)/g, '');
			}else if(mode==2) {
				$('#form-filter')[0][1].value=""
				$('#form-filter')[0][2].value=""
				$('#form-filter')[0][3].value=""
				$('#form-filter')[0][4].value=""
				$('#form-filter')[0][5].value=""
					
			}else{
				$('#modal-form-filter').modal('show');
			}
		}

		var options;
	    function renderWidgetWhatsapp(){
	    	options = {
	            whatsapp: whatsapp, // WhatsApp number
	            call_to_action: "Kirimi kami pesan", // Call to action
	            position: "left", // Position may be 'right' or 'left'
	        };
	        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
	        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
	        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
	        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	      
	        setTimeout(()=>{
	            removeText();
	        },2000)
		}
		
	    function removeText(){
	    	  for (const a of document.querySelectorAll("a")) {
	        	  	if (a.textContent.includes("GetButton")) {
	        	    console.log(a.remove())
	        	  	}
        	  }
		}
	</script>
  </body>

</html>
