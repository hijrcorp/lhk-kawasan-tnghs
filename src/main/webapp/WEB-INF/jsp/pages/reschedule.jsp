<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransitCamp"%>
<%@page import="id.co.hijr.ticket.model.Luggage"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTransitCampMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionTransitCamp"%>
<%@page import="id.co.hijr.ticket.mapper.RegionPhotoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionPhoto"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="id.co.hijr.ticket.mapper.TourGuideMapper"%>
<%@page import="id.co.hijr.ticket.model.TourGuide"%>
<%@page import="java.util.HashSet"%>
<%@page import="id.co.hijr.ticket.model.TypeVehicle"%>
<%@page import="id.co.hijr.ticket.mapper.TypeVehicleMapper"%>
<%@page import="id.co.hijr.ticket.model.Vehicle"%>
<%@page import="id.co.hijr.ticket.mapper.VehicleMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
RegionPhotoMapper regionPhotoMapper = appCtx.getBean(RegionPhotoMapper.class);
RegionTransitCampMapper regionTransitCampMapper = appCtx.getBean(RegionTransitCampMapper.class);
PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper = appCtx.getBean(PurchaseDetilTransitCampMapper.class);
PurchaseDetilVisitorMapper purchaseDetilVisitorMapper = appCtx.getBean(PurchaseDetilVisitorMapper.class);


PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
VehicleMapper vehicleMapper = appCtx.getBean(VehicleMapper.class);
TypeVehicleMapper typeVehicleMapper = appCtx.getBean(TypeVehicleMapper.class);
TourGuideMapper tourGuideMapper = appCtx.getBean(TourGuideMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
Purchase purchase = new Purchase();
System.out.println("step: "+ request.getParameter("step"));
if(request.getParameter("step") != null){
	QueryParameter paramPurchase = new QueryParameter();
	paramPurchase.setClause(paramPurchase.getClause()+" AND "+ Purchase.ID+ " ='"+id+"'");
	purchase = purchaseMapper.getListExtended(paramPurchase).get(0); //getEntity(id);
	if(!purchase.getStatus().equals("DRAFT")){
		//response.sendRedirect(request.getContextPath()+"/page/payment?id="+id);
	}
	id=purchase.getRegionId();
}
//
QueryParameter param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.HEADER_ID + " = '"+purchase.getId()+"')");
List<PurchaseDetilVisitor> dataPurchaseDetilVisitor = purchaseDetilVisitorMapper.getList(param_detil);
//purchase.setPurchaseDetilVisitor(dataPurchaseDetilVisitor);


param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+purchase.getId()+"')");
List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param_detil);
purchase.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);

//
Region region = regionMapper.getEntity(id);
QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
List<File> list_photo = fileMapper.getList(param);
//region.setList_photo(list_photo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.getId()+"'");
List<RegionTodo> list_todo = regionTodoMapper.getList(param);
region.setList_todo(list_todo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");
List<RegionFacility> list_facility = regionFacilityMapper.getList(param);
if(request.getParameter("step") == null) region.setList_facility(list_facility);
//
param = new QueryParameter();
//param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");/*  */
List<Vehicle> list_vehicle = vehicleMapper.getList(param);


param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionPhoto.HEADER_ID+"='"+region.getId()+"'");
List<RegionPhoto> list_photo_transction  = regionPhotoMapper.getList(param);

param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionTransitCamp.HEADER_ID+"='"+region.getId()+"'");
List<RegionTransitCamp> list_transit  = regionTransitCampMapper.getList(param);

//untuk format tanggal atau waktu
SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy");
SimpleDateFormat sdf_MY = new SimpleDateFormat("MMMM yyyy");
SimpleDateFormat sdftime = new SimpleDateFormat("hh.mm aa");
SimpleDateFormat sdftimeleft = new SimpleDateFormat("hh:mm:ss");

//untuk format currency
NumberFormat formatter = NumberFormat.getCurrencyInstance();

List<PurchaseDetilTransitCamp> listTransitCamp = purchaseDetilTransitCampMapper.getList(new QueryParameter());
//add some objects to the list...


String jsonTransitCamp = new Gson().toJson(listTransitCamp);
request.setAttribute("kphan", jsonTransitCamp);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>
<style>
.popover {
	width: 30%;max-width: 50%;
}
</style>
  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">
	
    <%-- <%@ include file = "inc/navbar.jsp" %> --%>
	<!-- costume -->
	
	<c:if test="${param.step != null && param.step == 1}">		
		<!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid p-3" style="width:70%">
		  <a class="navbar-brand" href="#">
		    <img src="https://getbootstrap.com/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
		    Kawasan
		  </a>
		  
		  <div class="collapse navbar-collapse" id="navbarText">
			    <div class="navbar-text ml-auto">
			      <span><span class="badge badge-pill badge-primary">1</span> Book</span>
			      <span>-</span>
			      <span><span class="badge badge-pill badge-primary">2</span> Pay</span>
			      <span>-</span>
			      <span><span class="badge badge-pill badge-primary">3</span> Process</span>
			      <span>-</span>
			      <span><span class="badge badge-pill badge-primary">4</span> E-ticket</span>
			    </div>
		  </div>
		</div>
		</nav> -->
		<nav class="navbar navbar-expand-lg navbar-light bg-default ">
		<div class="container p-3">
		
		  <a class="navbar-brand text-white" href="${pageContext.request.contextPath}/page/general">
		    <img src="${pageContext.request.contextPath}/images/icon-ciremai.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
		    Kawasan
		  </a>
		  
		<ul class=" nav nav-pills ml-auto ">
		  <li class="nav-item">
		    <a id="btn-book-1" class="nav-link active text-white" data-toggle="pill" href="#pills-book-1">Schedule Time</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-1-1" class="nav-link  text-white" data-toggle="pill" href="#pills-book-1-1">Transit Camp</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-2" class="nav-link disabled d-none" data-toggle="pill" href="#pills-book-2">Vehicle</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-3" class="nav-link disabled d-none" data-toggle="pill" href="#pills-book-3">Tour Guide</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-4" class="nav-link  text-white" data-toggle="pill" href="#pills-book-4">Luggage</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-5" class="nav-link text-white" data-toggle="pill" href="#pills-book-5">Confirmation</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-6" class="nav-link disabled text-white d-none" data-toggle="pill" href="#pills-book-5">Pay</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link disabled text-white" href="#" tabindex="-1" aria-disabled="true">E-ticket</a>
		  </li>
		</ul>
		</div>
		</nav>
	</c:if>
	<!--  -->
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">
		
		<c:if test="${param.step != null && param.step == 1}">	
		<%
			
		%>	
		<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-book-1">
		<!-- start -->
		
        <div class="container">
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			
			
			</div>
		</div>
		
		
		<div class="row mt-3">
			<div class="col-md-8">
				<div class="card mb-3">
				    <div class="card-body">
					    <div class="font-weight-bold">Logged in as ${requestScope.realName}</div>
					    <div class="text-muted d-none">via google</div>
				    </div>
				</div>
				
				<h4 class="">Contact Details</h4>
				<div id="container-person-contact">
					<% 
				for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
					if(pdv.getResponsible().equals("1")) {
					%>
					
				<div class="card mb-3">
				   <div class="card-header">
				      <div class="row align-items-center">
				         <div class="col">
				            <div class="mb-0 font-weight-bold">Person in charge</div>
				         </div>
				         <div class="col-auto d-none">
				            <a class=" btn btn-falcon-default btn-sm" href="#!">
				               <span class="fas fa-pencil-alt fs--2 mr-1"></span> Update details
				            </a>
				         </div>
				      </div>
				   </div>
				   <div class="card-body bg-light border-top">
				      <div class="row">
				         <div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Identity</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getVisitorIdentity().getTypeIdentity()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Full Name</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getVisitorIdentity().getFullName()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Phone number</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getPhoneNumber()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Email</p>
				               </div>
				               <div class="col"><a href="mailto:tony@gmail.com"><% out.print(pdv.getEmail()); %></a></div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Alamat Lengkap</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getAddress()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Phone number family</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getPhoneNumberFamily()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Address family</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getAddressFamily()); %></p>
				               </div>
				            </div>
				         </div>
				      </div>
				   </div>
				</div>
					<%
					}
				}
			%>
				</div>
				
				<h4 class="">Traveler Details</h4>
			<div id="container-person">
			<% 
				int no=1;
				for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
					if(pdv.getResponsible().equals("0")) {
					%>
					
				<div class="card mb-3">
				   <div class="card-header">
				      <div class="row align-items-center">
				         <div class="col">
				            <div class="mb-0 font-weight-bold">Person <% out.print(no); %></div>
				         </div>
				         <div class="col-auto d-none">
				            <a class=" btn btn-falcon-default btn-sm" href="#!">
				               <span class="fas fa-pencil-alt fs--2 mr-1"></span> Update details
				            </a>
				         </div>
				      </div>
				   </div>
				   <div class="card-body bg-light border-top">
				      <div class="row">
				         <div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Identity</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getVisitorIdentity().getTypeIdentity()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Full Name</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getVisitorIdentity().getFullName()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Phone number</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getPhoneNumber()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Email</p>
				               </div>
				               <div class="col"><a href="mailto:tony@gmail.com"><% out.print(pdv.getEmail()); %></a></div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Alamat Lengkap</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getAddress()); %></p>
				               </div>
				            </div>
				         </div>
				      </div>
				   </div>
				</div>
					<%
					no++;
					}
				}
			%>
				
			</div>
			</div>
			<div class="col-md-4">
				<div class="card">
				   <div class="card-body">
				      <div class="row justify-content-between align-items-center">
				         <div class="col">
				            <div class="media">
				               <div class="media-body fs--1">
				                  <h5 id="label_region_name" class="fs-0">Apuy</h5>
				                  <p class="mb-0"><i class="fas fa-map-marker-alt"></i> Majalengka</p>
				               </div>
				            </div>
				         </div>
				      </div>
				      <hr>
				      <div class="row">
				         <div class="col">
				         <form id="entry-form-popover">
							   <div class="form-row">
							      <div class="form-group col-md-6">
							         <input autocomplete="off" type="text" class="form-control single-date-picker" id="start_date" name="start_date" placeholder="Check In"> 
							      </div>
							      <div class="form-group col-md-6">
							         <input autocomplete="off" type="text" class="form-control single-date-picker" id="end_date" name="end_date" placeholder="Check Out"> 
							      </div>
							   </div>
							   <hr>
							   <div class="form-group">
							      <label class="control-label"><strong>WNI</strong><br> <i>Price : IDR5000
							      /Days</i></label>										
							      <div class="row">
							         <div class="col-md-6">
							            <div class="input-group">
							               <div class="input-group-prepend">								                <button class="btn btn-outline-danger btn-minus demise" type="button">-</button>								            </div>
							               <input id="count-wni" type="number" class="form-control form-control-sm demise" data-price="5000
							                  " placeholder="Number of travelers/visitors" value="1" min="0" max="10">								            
							               <div class="input-group-append">								                <button class="btn btn-outline-success btn-plus demise" type="button">+</button>								            </div>
							            </div>
							         </div>
							         <div class="col-md-6 align-self-center">		                        		<label class="control-label">IDR <i id="label_price_wni" class="font-weight-bold">5000</i></label>								        </div>
							      </div>
							   </div>
							   <div class="form-group">
							      <label class="control-label"><strong>WNA</strong><br> <i>Price : IDR50000
							      /Days</i></label>										
							      <div class="row">
							         <div class="col-md-6">
							            <div class="input-group">
							               <div class="input-group-prepend">								                <button class="btn btn-outline-danger btn-minus demise" type="button">-</button>								            </div>
							               <input id="count-wna" type="number" class="form-control form-control-sm demise" data-price="50000
							                  " placeholder="Number of travelers/visitors" value="0" min="0" max="10">								            
							               <div class="input-group-append">								                <button class="btn btn-outline-success btn-plus demise" type="button">+</button>								            </div>
							            </div>
							         </div>
							         <div class="col-md-6 align-self-center">		                        		<label class="control-label">IDR <i id="label_price_wna" class="font-weight-bold">50000</i></label>								        </div>
							      </div>
							   </div>
							   <hr>
							</form>
				         </div>
				      </div>
				      <div class="row justify-content-between align-items-center">
                        <div class="form-groups col align-self-center">
		        			<span>Kuota Pendaki : <span id="sisa-kuota" class="text-success"></span></span>
                        </div>
		                <div class="col">
		                    <button id="btn-booking" onclick="save();" tabindex="0" class="btn btn-lg btn-block bg-orange example-popover" data-original-title="" title="">Next</button>
		                 </div>
		              </div>
				      <div class="row justify-content-between align-items-center d-none">
				         <div class="col">
				            <!-- <button id="btn-book" data-toggle="modal" data-target="#modal-form" class="btn btn-sm px-4 px-sm-5 btn-block bg-orange" type="button">Book Now</button> -->
				            <div class="alert alert-primary mt-3">
				               <p id="price-tiket" data-price="5000">Price : IDR5,000.00/Days</p>
				               <hr>
				               <div data-max-day="1" id="total-price-tiket">Rp.0 (0 x 0)</div>
				               <p id="total-days-tiket">0 hari 0 malam (0D0N) </p>
				            </div>
				         </div>
				      </div>
				   </div>
				</div>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		
		<div class="tab-pane fade " id="pills-book-1-1">
		<!-- start -->
		
        <div class="container">
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Transit Camp & Kapling Tenda</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-8">
				<div class="card mb-3"> <!-- data-key="0"  -->
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <div class="mb-0" id="">Pilih Transit Camp (TC)<span class="d-none d-sm-inline-block"></span></div>
	                </div>
	              </div>
				</div>
				    <div class="card-body">
				    	<div class="border mb-3 bg-light d-nones">
						  <img class="img-fluid" alt="" src="${pageContext.request.contextPath}/files/<% out.print(region.getPhoto()); %>?filename=<% out.print(region.getNameFile()); %>" usemap="#housemap">
							<!-- x,y,radius -->
							<!-- <map name="housemap">
							  <area shape="circle" coords="15,35,10" href="javascript:void(0)" alt="1">
							  <area shape="circle" coords="42,85,10" href="javascript:void(0)" alt="2">
							  <area shape="circle" coords="95,138,10" href="javascript:void(0)" alt="3">
							  <area shape="circle" coords="115,180,10" href="javascript:void(0)" alt="4">
							</map> -->					
						</div>
						  <table class="table table-bordered">
							  <thead class="thead-dark">
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">Lokasi Basecamp</th>
							      <th scope="col">Registrasi di<br/>Basecamp</th>
							      <th scope="col">Perkiraan Maks Tiba di<br/>Transit Camp</th>
							    </tr>
							  </thead>
							  <tbody>
							  <% for(RegionTransitCamp rtc : list_transit) {
									%>
									<tr data-code-tent="<% out.print(rtc.getCodeMasterTransitCamp()); %>" data-small-tent="<% out.print(rtc.getNumberOfSmallTents()); %>" data-large-tent="<% out.print(rtc.getNumberOfLargeTents()); %>" class="row-rtc" data-id_photo="<% out.print(rtc.getPhoto()); %>" data-name_photo="<% out.print(rtc.getNameFile()); %>" data-id="<% out.print(rtc.getIdMasterTransitCamp()); %>" id="row-rtc-<% out.print(rtc.getIdMasterTransitCamp()); %>" onclick="$('[name=id_master_transit]').val(<% out.print(rtc.getIdMasterTransitCamp()); %>);setActiveRow(this);createFormTent(<% out.print(rtc.getNumberOfSmallTents()); %>, <% out.print(rtc.getNumberOfLargeTents()); %>, '<% out.print(rtc.getCodeMasterTransitCamp()); %>', this)" >
								      <td scope="row"><% out.print(rtc.getCode()); %></td>
								      <td><% out.print(rtc.getNameMasterTransitCamp()); %></td>
								      <td><% out.print(sdftime.format(rtc.getStartRegister())); %>-<% out.print(sdftime.format(rtc.getEndRegister())); %></td>
								      <td><% out.print(sdftime.format(rtc.getExtimateArrived())); %></td>
								    </tr>
							 <%	 }
							  %>
							  </tbody>
							</table>
				    </div>
				</div>
				
				<div class="card mb-3"> <!-- data-key="0"  -->
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <div class="mb-0" id="">Pilih Kapling Tenda<span class="d-none d-sm-inline-block"></span></div>
	                </div>
	                <!-- <div class="col">
	                    <div class="row no-gutters float-right">
	                      <div class="col d-md-block d-none">
	                      	<select class="custom-select custom-select-sm ml-2" name="type_identity">
	                          <option value="">*Choose Type Identity</option>
	                          <option value="KTP">KTP</option>
	                          <option value="PASSPORT">Passport</option>
	                        </select>
	                      </div>
	                    </div>
	                </div> -->
	              </div>
				</div>
				    <div class="card-body">
						<div class="row">
			        	<div class="col-md-6">
			        	<div id="row-tenda-kecil" class="form-row">
			        	<%-- <% for(int i=1; i <= 45; i++) { %>
			        		<div class="border col-md-2 p-1 m-1 text-center bg-success">PGK <% out.print(i); %></div>
			       		<% } %> --%>
			        	</div>
			        	</div>
			        	<div class="col-md-6">
			        	<div id="row-tenda-besar" class="form-row">
			        	<%-- <% for(int i=1; i <= 5; i++) { %>
			        		<div class="border col-md-2 p-1 m-1 text-center bg-success">PGB <% out.print(i); %></div>
			       		<% } %> --%>
			        	</div>
			        	</div>
			        	</div>
						  
				    </div>
				</div>
		
	    	<form id="form-transit-camp-detil" class="d-none">
	    	<!-- <div class="form-row">
				<div class="form-group col">
				    <label for="">Tenda Kecil<span class="text-danger">*</span></label>
                     	<select class="form-control" name="type_camp">
                         <option value="">Choose </option>
                         <option value="KECIL1">1 Kecil</option>
                         <option value="KECIL2">2 Kecil</option>
					  <option value="KECIL3">3 Kecil</option>
                       </select>
				</div>
				<div class="form-group col">
				    <label for="">Tenda Besar<span class="text-danger">*</span></label>
                     	<select class="form-control" name="count_camp">
                         <option value="">Choose </option>
                         <option value="1">1 Besar</option>
                         <option value="2">2 Besar</option>
                         <option value="3">3 Besar</option>
                       </select>
				</div>
			  </div> -->
			  
			  <div class="form-row">
					<!-- <div class="form-group col">
					    <label for="">Pilih Ukuran Tenda<span class="text-danger">*</span></label>
                      	<select class="form-control" name="type_camp">
                          <option value="">Choose </option>
                          <option value="KECIL">KECIL</option>
                          <option value="BESAR">BESAR</option>
                        </select>
					</div> -->
					<div class="form-group col">
					    <label for="">Jumlah Tenda<span class="text-danger">*</span></label>
                      	<select class="form-control" name="count_camp">
                          <option value="">Choose </option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                        </select>
					</div>
			  </div>
			  <div class="form-row">
					<div class="col-md-6">
			    		<label for="">Pilih Tempat Transit<span class="text-danger">*</span></label>
			    		<select class="form-control" name="id_master_transit">
							<option value="">Choose</option>
							<% for(RegionTransitCamp rtc : list_transit) {
								out.print("<option value='"+rtc.getIdMasterTransitCamp()+"'>"+rtc.getCode()+" "+rtc.getNameMasterTransitCamp()+"</option>");
								}
							%>
						</select>
					</div>
					<div class="col-md-6">
			    		<!-- <span id="" class="form-text text-muted">Nomor Kapling</span>
			    		<span id="" class="form-text form-control text-muted"> -</span> -->
			    		<label for="">Nomor Kapling<span class="text-danger"></span></label>
                      	<select class="form-control" name="code_unique">
                        </select>
					</div>
			  </div>
			</form>
			
			<div class="row mt-3">
				<div class="col-md-6">
					<button type="button" onclick="$('#btn-book-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
				</div>
				<div class="col-md-6">
					<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
				</div>
			</div>
			</div>
			<div class="col-md-4">
			<div class="card">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				int key=0;
				  	String path=request.getContextPath()+"/images/notfound.png";
					for(RegionPhoto rp: list_photo_transction) {

						for(File o2 : list_photo){
							if(o2.getId().equals(rp.getFileId())){
								path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
								rp.setList_photo(o2);
							}
						}

						if(rp.isActive()){
							out.println("<img style='width:70px;height:70px;object-fit:cover' src='"+path+"' class='mr-3 img-fluid'>");
						}
					}
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 data-id_kawasan="<% out.print(region.getId()); %>" data-biaya_kawasan="<% out.print(purchase.getAmountTotalTicket()); %>" class="mt-0"><% out.print(region.getName()); %></h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Start Date</span> <span class="float-right small lbl_start_date"><% out.print(sdf.format(purchase.getStartDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">End Date</span> <span class="float-right small lbl_end_date"><% out.print(sdf.format(purchase.getEndDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span data-count_ticket="<% out.print(purchase.getCountTicket()); %>" class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			  </ul>
			  <div class="card-body">
			  <% if(region.getRefund().equals("1")) {
				    out.print("<span class='text-success'><i class='far fa-check-circle'></i> Reschedule</span>");
				 }else{
					out.print("<span class='text-muted'><i class='fas fa-ban'></i> No Reschedule</span>");
				 }
			    %>
			  </div>
			</div>
			
			
			<div class="card mt-3">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Denah Tenda</span> <a href="#" class="card-link float-right"></a></h5></div>
			  <div class="card-body">
			  	<img id="img-photo-tent" class="img-fluid" src="${pageContext.request.contextPath}/images/notfound.png">
			  </div>
			</div>
			</div>
			
		</div>
		
		
        </div>
		</div>
		
		<div class="tab-pane fade" id="pills-book-2" >
		<!-- start -->
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Vehicle Options</h4>
			<h4 class="mb-3"><small class="text-muted">Choose vehicle for you tour</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
			<ul class="nav nav-pills justify-content-center border-bottom border-info">
				
			 <%
			 List<String> str = new ArrayList<>();
			 for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){
				 for(Vehicle vehicle : list_vehicle){
					 if(tv.getId().equals(vehicle.getTypeId())){
					 	if(!str.contains(tv.getName()))
						 str.add(tv.getName());
					 }
				 }
			 }
			 System.out.print("V: "+str);
			 key=0;
				for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){ %>
					<% 
					for(int i=0; i < str.size(); i++) {
						if(str.get(i).equals(tv.getName())){
							%>
							<li class="nav-item">
					    	<a class="nav-link <% if(key==0) out.print("active"); %>" id="pills-car<%out.print(tv.getId());%>-tab" data-toggle="pill" href="#pills-car<%out.print(tv.getId());%>"><% out.print(tv.getName()); %></a>
						   	</li>
							<%
						}
					}
					%>
					
				   	
	 		 <% 
	 		 	key++;
	 		 	} 
	 		 %> 	
			</ul>
		</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="tab-content" id="pills-tabContent">
		<% key=0; for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){ %>
			<div class="tab-pane fade<% if(key==0) out.print("show active"); %>" id="pills-car<%out.print(tv.getId());%>"><!-- show active -->
				<div data-id="container" class="row">
				<%
					path=request.getContextPath()+"/images/notfound.png";
					for(Vehicle vehicle : list_vehicle){ 
						param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+vehicle.getId()+"'");
						System.out.println("si "+fileMapper.getList(param).size()+" "+vehicle.getId());
						

						//String path=request.getContextPath()+"/images/notfound.png";
						vehicle.setPhoto(path);
						for(File vehicle_file : fileMapper.getList(param)){ 
							//vehicle.setPhoto(path);
							
							if(tv.getId().equals(vehicle.getTypeId())){
								vehicle_file.setName(vehicle_file.getName().replace(" ", "%"));
							  	path=request.getContextPath()+"/files/"+vehicle_file.getId()+"?filename="+vehicle_file.getName()+"&download";
								vehicle.setPhoto(path);
							}
						}
						
						if(tv.getId().equals(vehicle.getTypeId())){
						%>
						<div class="col-md-4 mt-3">
						<div <% out.print("data-id_vehicle="+vehicle.getId()+""); %> class="card vehicle mb-3">
			  				<img src="<% out.print(vehicle.getPhoto()); %>" class="card-img-top" alt="...">
						    <div class="card-body">
							<div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title"><% out.print(vehicle.getName()+" ("+vehicle.getTypeName()+")"); %></h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<div class="row">
								    <div class="col">
								        <small id="" class="form-text text-primary"><% out.print(formatter.format(vehicle.getCost())); %> / Day</small>
								    </div>
								    <div class="col">
								       <!--  <div class="input-group mb-3 input-group-sm">
								            <div class="input-group-prepend">
								                <button class="btn btn-outline-secondary" type="button" id="button-addon1">-</button>
								            </div>
								            <input name="qty_vehicle" type="number" class="form-control form-control-sm" value="1" min="1">
								            <div class="input-group-append">
								                <button class="btn btn-outline-secondary" type="button" id="button-addon2">+</button>
								            </div>
								        </div> -->
								        <input name="qty_vehicle" type="number" class="form-control form-control-sm" value="1" min="1">
								    </div>
								</div>
							</div>
							</div>
						    </div>
						</div>
						</div>
						<%
						}
					} 
				%>
				</div>
			</div>
			<% key++;} %>
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-1-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-3').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-3" >
			
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Tour Guide</h4>
			<h4 class="mb-3"><small class="text-muted">Choose someone to become your tour guide</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="card bg-light">
			<div class="card-body bg-light p-0s">
	    	<div class="row no-gutterss text-center fs--1">
	    	<%
	    	
    		for(TourGuide tg : tourGuideMapper.getList(new QueryParameter())){
	    		param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+tg.getId()+"'");
			  	for(File tour_file : fileMapper.getList(param)){
					tour_file.setName(tour_file.getName().replace(" ", "%"));
				  	path=request.getContextPath()+"/files/"+tour_file.getId()+"?filename="+tour_file.getName()+"&download";
				  	//path=request.getContextPath()+"/files/"+vehicle_file.getId()+"?filename="+vehicle_file.getName()+"&download";
					if(tg.getPhoto()!=null){
				  		tg.setPhoto(path);
					}
	    		}
			  	path=request.getContextPath()+"/images/notfound.png";
			  	if(tg.getPhoto()==null){
			  		tg.setPhoto(path);
				}

               	out.print("<div class='col-6 col-md-4 col-xl-3 col-xxl-2 mb-1'>");
    			out.print("<div data-id_tour_guide="+tg.getId()+" class='tour-guide bg-whitse p-3 h-100'>");
    			
    			out.print("<a href='javascript:void(0)'><img class='img-thumbnail img-fluid rounded-circle mb-3 shadow-sm' src="+tg.getPhoto()+" width='100'></a>");
   				
    			out.print("<h6 class='mb-1'><span>"+tg.getFullName()+"</span></h6>");

   				out.print("<small id='' class='form-text text-muted'>Starting From</small>");
  					out.print("<small id='' class='form-text text-primary'>"+formatter.format(tg.getCostPerDay())+"/ Day</small>");
				//out.print("<p class='small mb-1'><span class='text-muted'>"+tg.getDescription()+"</span></p>");
               	out.print("</div>");
               	out.print("</div>");
    		}
           	%>
              </div>
	  		</div>
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-2').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-4" >
			
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">List Luggage</h4>
			<h4 class="mb-3"><small class="text-muted">Please make sure you bring this stuff</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="card bg-light">
		<div class="card-body bg-light p-0s">
		<form id="form-luggage">
		<div class="row no-gutters fs--1">
	    <div class="col-md-6">
	          <div class="form-group">
	            <label for="" class="font-weight-bold">Pribadi</label>
	            <% 
	            QueryParameter paramluggage= new QueryParameter();
	            		paramluggage.setClause(paramluggage.getClause()+" AND "+Luggage.TYPE+" ='"+"PRIBADI"+"'");
	            for(Luggage l : tourGuideMapper.getListLuggage(paramluggage)) { %>
				<div class="form-check" data-luggage="<% out.print(l.getId()); %>">
				  <input required name="name_luggage" class="form-check-input" type="checkbox" value="<% out.print(l.getName()); %>" id="defaultCheck<% out.print(l.getId()); %>">
				  <input required name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;"/>
				  <input name="type_luggage" type="hidden" value="PRIBADI"/>
				  <input name="desc_luggage" type="hidden" value=""/>
				  <label class="form-check-label" for="defaultCheck<% out.print(l.getId()); %>"><% out.print(l.getName()); %></label>
				</div>
	            <% } %>
	          </div>
	    </div>
	
		<div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1" class="font-weight-bold">Logistik</label>
            <% 
            paramluggage= new QueryParameter();
            		paramluggage.setClause(paramluggage.getClause()+" AND "+Luggage.TYPE+" ='"+"LOGISTIK"+"'");
            for(Luggage l : tourGuideMapper.getListLuggage(paramluggage)) { %>
			<div class="form-check" data-luggage="<% out.print(l.getId()); %>">
			  <input required name="name_luggage" class="form-check-input" type="checkbox" value="<% out.print(l.getName()); %>" id="defaultCheck<% out.print(l.getId()); %>">
			  <input required name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;"/>
				  <input name="type_luggage" type="hidden" value="LOGISTIK"/>
				  <input name="desc_luggage" type="hidden" value=""/>
			  <label class="form-check-label" for="defaultCheck<% out.print(l.getId()); %>"><% out.print(l.getName()); %></label>
			</div>
            <% } %>
          </div>
	    </div>
		</div>
		
		<div class="row no-gutters fs--1">
	    <div class="col-md-6">
	          <div class="form-group">
	            <label for="exampleInputEmail1" class="font-weight-bold">Kelompok</label>
	            <% 
	            paramluggage= new QueryParameter();
	            		paramluggage.setClause(paramluggage.getClause()+" AND "+Luggage.TYPE+" ='"+"KELOMPOK"+"'");
	            for(Luggage l : tourGuideMapper.getListLuggage(paramluggage)) { %>
				<div class="form-check" data-luggage="<% out.print(l.getId()); %>">
				  <input required name="name_luggage" class="form-check-input" type="checkbox" value="<% out.print(l.getName()); %>" id="defaultCheck<% out.print(l.getId()); %>">
				  <input required name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;"/>
				  <input name="type_luggage" type="hidden" value="KELOMPOK"/>
				  <input name="desc_luggage" type="hidden" value=""/>
				  <label class="form-check-label" for="defaultCheck<% out.print(l.getId()); %>"><% out.print(l.getName()); %></label>
				</div>
	            <% } %>
	          </div>
	    </div>
	
		<div class="col-md-6">
	          <div id="container-luggage" class="form-group">
	            <label for="exampleInputEmail1" class="font-weight-bold">Barang Bawaan Lainnya</label>
	    		<button type="button" class="btn btn-sm btn-secondary" onclick="addNewLuggage()" >Tambahkan</button>
	          </div>
	          
	    </div>
		</div>
		<button id="btn-luggage" type="submit" class="d-none">-</button>
		</form>
		</div>
		
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-1-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="goNext();" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		</div>
		
		
		<div class="tab-pane fade" id="pills-book-5">
		<div class="container">
		
		<div class="row mt-3">
			<div class="col-md-8">
			
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			
			</div>
		</div>
			
		<div class="row mt-3">
			<div class="col-md-8">
				<div class="card mb-3">
				    <div class="card-body">
					    <div class="font-weight-bold">Logged in as ${requestScope.realName}</div>
					    <div class="text-muted d-none">via google</div>
				    </div>
				</div>
				
				<div id="container-person-contact">
					<% 
				for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
					if(pdv.getResponsible().equals("1")) {
					%>
					
				<div class="card mb-3">
				   <div class="card-header">
				      <div class="row align-items-center">
				         <div class="col">
				            <div class="mb-0">Person in charge</div>
				         </div>
				         <div class="col-auto d-none">
				            <a class=" btn btn-falcon-default btn-sm" href="#!">
				               <span class="fas fa-pencil-alt fs--2 mr-1"></span> Update details
				            </a>
				         </div>
				      </div>
				   </div>
				   <div class="card-body bg-light border-top">
				      <div class="row">
				         <div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Identity</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print("pdv.getTypeIdentity()"); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Full Name</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getVisitorIdentity().getFullName()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Phone number</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getPhoneNumber()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Email</p>
				               </div>
				               <div class="col"><a href="mailto:tony@gmail.com"><% out.print(pdv.getEmail()); %></a></div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Alamat Lengkap</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getAddress()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Phone number family</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getPhoneNumberFamily()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Address family</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getAddressFamily()); %></p>
				               </div>
				            </div>
				         </div>
				      </div>
				   </div>
				</div>
					<%
					}
				}
			%>
				</div>
				
				<h4 class="">Traveler Details</h4>
			<div id="container-person">
			<% 
				 no=1;
				for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
					if(pdv.getResponsible().equals("0")) {
					%>
					
				<div class="card mb-3">
				   <div class="card-header">
				      <div class="row align-items-center">
				         <div class="col">
				            <div class="mb-0">Person <% out.print(no); %></div>
				         </div>
				         <div class="col-auto d-none">
				            <a class=" btn btn-falcon-default btn-sm" href="#!">
				               <span class="fas fa-pencil-alt fs--2 mr-1"></span> Update details
				            </a>
				         </div>
				      </div>
				   </div>
				   <div class="card-body bg-light border-top">
				      <div class="row">
				         <div class="col-lg col-xxl-5 mt-4 mt-lg-0 offset-xxl-1">
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Identity</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print("pdv.getTypeIdentity()"); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-0">Full Name</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getVisitorIdentity().getFullName()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Phone number</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getPhoneNumber()); %></p>
				               </div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Email</p>
				               </div>
				               <div class="col"><a href="mailto:tony@gmail.com"><% out.print(pdv.getEmail()); %></a></div>
				            </div>
				            <div class="row">
				               <div class="col-5 col-sm-4">
				                  <p class="font-weight-semi-bold mb-1">Alamat Lengkap</p>
				               </div>
				               <div class="col">
				                  <p class="font-weight-semi-bold mb-0"><% out.print(pdv.getAddress()); %></p>
				               </div>
				            </div>
				         </div>
				      </div>
				   </div>
				</div>
					<%
					no++;
					}
				}
			%>
				
			</div>
			</div>
			<div class="col-md-4">
			<div class="card">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				//key=0;
				/* param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+RegionPhoto.HEADER_ID+"='"+region.getId()+"'");
				List<RegionPhoto> list_photo_transction  = regionPhotoMapper.getList(param); */
				for(File o2 : list_photo){
				  	//out.println(request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download"); 
				  	/* o2.setName(o2.getName().replace(" ", "%"));
				  	String path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
				  	if(region.){
				  		//out.println("<img class='d-block w-100' src="+path+" alt='First slide'>");
				  		out.println("<img style='width:70px;height:70px;object-fit:cover' src="+path+" class='mr-3 img-fluid'>");
				  	} */
				  	//key++;

					for(RegionPhoto rp: list_photo_transction) {
						if(o2.getId().equals(rp.getFileId())){
							if(rp.isActive()){
								path=request.getContextPath()+"/files/"+o2.getId()+"?filename="+o2.getName()+"&download";
								rp.setList_photo(o2);
						  		out.println("<img style='width:70px;height:70px;object-fit:cover' src='"+path+"' class='mr-3 img-fluid'>");
							}
						}
						
					}
				}
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 class="mt-0"><% out.print(region.getName()); %></h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Start Date</span> <span class="float-right small lbl_start_date"><% out.print(sdf.format(purchase.getStartDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">End Date</span> <span class="float-right small lbl_end_date"><% out.print(sdf.format(purchase.getEndDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			  </ul>
			  <div class="card-body">
			  	<% if(region.getRefund().equals("1")) {
				    out.print("<span class='text-success'><i class='far fa-check-circle'></i> Reschedule</span>");
				 }else{
					out.print("<span class='text-muted'><i class='fas fa-ban'></i> No Reschedule</span>");
				 }
			    %>
			  </div>
			</div>
			<!-- <button type="button" onclick="save();" class="btn btn-outline-lights bg-orange btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 btn-block mt-3 continue-payment-popover">Continue to Payment</button> -->
			</div>
		</div>
		
		<div class="row justify-content-centesr">
			<div class="col-md-8">
			<div id="container-person"></div>
			
			<h4 class="">Transit Camp Details</h4>
			<div id="container-transit-camp"></div>
			
			<h4 class="d-none">Tour Guide Details</h4>
			<div class="d-none" id="container-guide"></div>
			
			
			<h4 class="d-none">Vehicle Details</h4>
			<div class="d-none" id="container-vehicle"></div>
			
			<hr>
			
			<h4 class="d-none">Price Details</h4>
			<div class="row mt-3 d-none">
				<div class="col-md-12">
					<div class="card-deck">
					  <div class="card">
					    <div class="card-body text-justify">
					      	<div class="row">
					      	<h5 class="card-title font-weight-bold col">Total Price to pay</h5>
					      	<h5 class="card-title font-weight-bold pull-right text-right col">IDR <span class="currency" id="total-price-pay">0</span><span class="d-none">(<% out.print(purchase.getCountTicket()); %>x)</span></h5>
					    	</div>
					   	</div>
					   	<div class="card-footer">
					   	This account <strong>(${requestScope.realName})</strong> will pay this, before get your ticket.
					   	</div>
					  </div>
					</div>
					<hr/>
					<!-- <button type="button" onclick="save();" class="btn btn-outline-lights bg-orange btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 btn-blocks mt-3 continue-payment-popover float-right">Continue to Payment</button> -->
				</div>
			</div>
		
			<div class="row mt-3">
				<div class="col-md-6">
					<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
				</div>
				<div class="col-md-6">
					<button type="button" onclick="reschedule();" class="btn btn-outline-lights bg-orange btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 btn-blocks mt-3 continue-payment-popover float-right">Continue to Update</button>
				</div>
			</div>
			
			</div>
			
		</div>
		
		</div>
		</div>
		
		<div class="tab-pane fade" id="pills-book-5">
		<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
			<div class="card mb-3">
            <div class="card-body">
              <div class="row align-items-center text-center mb-3">
                <div class="col-sm-6 text-sm-left"><img src="https://prium.github.io/falcon/assets/img/logos/logo-invoice.png" alt="invoice" width="150"></div>
                <div class="col text-sm-right mt-3 mt-sm-0">
                  <h2 class="mb-3">Invoice</h2>
                  <h5>Falcon Design Studio</h5>
                  <p class="small mb-0">156 University Ave, Toronto<br>On, Canada, M5H 2H7</p>
                </div>
                <div class="col-12">
                  <hr>
                </div>
              </div>
              <div class="row justify-content-between align-items-center">
                <div class="col">
                  <h6 class="text-muted">Invoice to</h6>
                  <h5>Antonio Banderas</h5>
                  <p class="small">1954 Bloor Street West<br>Torronto ON, M6P 3K9<br>Canada</p>
                  <p class="small"><a href="mailto:example@gmail.com">example@gmail.com</a><br><a href="tel:444466667777">+4444-6666-7777</a></p>
                </div>
                <div class="col-sm-auto ml-auto">
                  <div class="table-responsive">
                    <table class="table table-sm table-borderless fs--1">
                      <tbody>
                        <tr>
                          <th class="text-sm-right">Invoice No:</th>
                          <td>14</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Order Number:</th>
                          <td>AD20294</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Invoice Date:</th>
                          <td>2018-09-25</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Payment Due:</th>
                          <td>Upon receipt</td>
                        </tr>
                        <tr class="alert-success font-weight-bold">
                          <th class="text-sm-right">Amount Due:</th>
                          <td>$19688.40</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="table-responsive mt-4 fs--1">
                <table class="table table-striped border-bottom">
                  <thead>
                    <tr class="bg-primary text-white">
                      <th class="border-0">Items</th>
                      <th class="border-0 text-center">Quantity</th>
                      <th class="border-0 text-right">Price per item</th>
                      <th class="border-0 text-right">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Ticket</h6>
                        <p class="mb-0">Raja Ampat</p>
                      </td>
                      <td class="align-middle text-center">2</td>
                      <td class="align-middle text-right">$65.00</td>
                      <td class="align-middle text-right">$130.00</td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Vehicle</h6>
                        <p class="mb-0">Fortuner</p>
                      </td>
                      <td class="align-middle text-center">1</td>
                      <td class="align-middle text-right">$2,100.00</td>
                      <td class="align-middle text-right">$2,100.00</td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Tour Guide</h6>
                        <p class="mb-0">Mr.alaex</p>
                      </td>
                      <td class="align-middle text-center">6</td>
                      <td class="align-middle text-right">$2,000.00</td>
                      <td class="align-middle text-right">$12,000.00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="row no-gutters justify-content-end">
                <div class="col-auto">
                  <table class="table table-sm table-borderless fs--1 text-right">
                    <tbody>
                    <!-- <tr>
                      <th class="text-900">Subtotal:</th>
                      <td class="font-weight-semi-bold">$18,230.00 </td>
                    </tr>
                    <tr>
                      <th class="text-900">Tax 8%:</th>
                      <td class="font-weight-semi-bold">$1458.40</td>
                    </tr> -->
                    <tr class="border-top">
                      <th class="text-900">Total:</th>
                      <td class="font-weight-semi-bold">$19688.40</td>
                    </tr>
                    <tr class="border-top border-2x font-weight-bold text-900">
                      <th>Amount Due:</th>
                      <td>$19688.40</td>
                    </tr>
                  </tbody></table>
                </div>
              </div>
            </div>
            <div class="card-footer bg-light">
              <p class="fs--1 mb-0"><strong>Notes: </strong>We really appreciate your business and if there's anything else we can do, please let us know!</p>
            </div>
          </div>
			</div>
		</div>
		</div>
		</div>
		
		</div>
		</c:if>
      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<!-- Modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="max-width:">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Booking Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body bg-light pt-0">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form" class="mt-3">
		  	<div class="form-row">
				<div class="form-group col-md-12">
					<input type="number" class="form-control" name="count_ticket" placeholder="Number of travelers/visitors">
				</div>
			</div>
		  	<div class="form-row">
				<div class="form-group col-md-6">
					<input type="text" class="form-control single-date-picker" name="start_date" placeholder="Check In">
				</div>
				<div class="form-group col-md-6">
				      <input type="text" class="form-control single-date-picker" name="end_date" placeholder="Check Out">
				</div>
			</div>
			<input name="region_id" type="text" value="${param.id}">
			<!-- <input name="authorization" type="text" value="Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTc4MjE1MTIyLCJqdGkiOiJkM2NjYjA2Ny00YTM5LTQ4NjUtOGY1Ny02YzU4YTNmY2ZkMTAiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.JJNifAbsNIu7hkrpfjfkUOTRtI5iSVi-nqwfjrTzmWqwl32jQ9A9-jbcbSYvqIQH6y9zFa0Tn_iNcro-gtRr-qFGl5evVyqGrZq6AE776CNZ3irhOijNjV3LwRxgp-B3BJY7OdS8nqtW0plhA6IfdYB16wVCn7JCN3XYO6FzcfUzcgkDAp5Fi-mFUEyMSmtFGi7wS-pBN7wHF2ZRIuloAB7_hefeCxRI3hthQnz_VnJ6foADVqw6MSjhWBVVxYBd10_OkADrkP9XsXMCVnHbWHGZZXNVJQAdl-fcdo-ZocIETvCqmEddKQ05y4Vpgt-bPdwY62K4BG1RkdahqUpQKg"> -->
			
			<button type="submit" id="btn-submit">?</button>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" onclick="$('#btn-submit').click();" class="btn btn-success pill pl-3 pr-3"><i class="fa fa-check"></i> Done</button>
	      </div>
	    </div>
	  </div>
	</div>
    <%@ include file = "inc/footer.jsp" %>
    <script src="${pageContext.request.contextPath}/js/general.js"></script>
    
	<script>
	$('#btn-modal-confirm-yes').attr('id', 'btn-modal-reschedule-yes');

	/*
	
	$('#btn-modal-confirm-yes').attr('id', 'btn-modal-reschedule-yes');
	$('#btn-reschedule-confirm-yes').attr('onclick', 'reschedule()');
	*/
	
	var data = {
		    values: ${kphan}
		};
	console.log(data);
		$(function () {
		  	$('[data-toggle="popover"]').popover();
		  
		  	$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD-MM-YYYY'));
				$(this).val(picker.endDate.format('DD-MM-YYYY'));
				picker.autoUpdateInput = true;
				$(this).change();
				
			});
			$('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
			    $(this).val('');
			});
		})
	$('#checkAsLeader').click(function(){
        if($(this).prop("checked") == true){
        	$('[name=no_identity]')[1].value=$('[name=no_identity]')[0].value;
    		$('[name=full_name]')[1].value=$('[name=full_name]')[0].value;
    		$('[name=phone_number]')[1].value=$('[name=phone_number]')[0].value;
    		$('[name=email]')[1].value=$('[name=email]')[0].value;
    		$('[name=type_identity]')[1].value=$('[name=type_identity]')[0].value;
    		$('[name=birthdate]')[1].value=$('[name=birthdate]')[0].value;
    		$('[name=gender]')[1].value=$('[name=gender]')[0].value;
    		$('[name=file_identity]')[1].files=$('[name=file_identity]')[0].files;
    		$('[name=address]')[1].value=$('[name=address]')[0].value;
    	}else if($(this).prop("checked") == false){
        	$('[name=no_identity]')[1].value="";
    		$('[name=full_name]')[1].value="";
    		$('[name=phone_number]')[1].value="";
    		$('[name=email]')[1].value="";
    		$('[name=type_identity]')[1].value="";
    		$('[name=birthdate]')[1].value="";
    		$('[name=gender]')[1].value="";
    		$('[name=file_identity]')[1].value="";
    		$('[name=address]')[1].value="";
        }
    });
		
		
		function hide_t(){
			$(".example-popover").popover("hide");
		}

		function setdefaultvalue(){
			var identity=["1111111","1111111", "2222222", "3333333", "4444444"];
			var namefull=["satu","satu", "dua", "tiga", "empat"];
			for(var i=0; i < $('[name=no_identity]').length;i++){
				$('[name=no_identity]')[i].value=identity[i];
				$('[name=full_name]')[i].value=namefull[i];
				$('[name=phone_number]')[i].value="82"+identity[i];
				$('[name=email]')[i].value=namefull[i]+"@email.com";
			}
			$('[name=type_identity]').val("KTP");
		}

		function setActiveRow(e){
			$('.row-rtc').removeClass("bg-primary");
			$('#row-rtc-'+e.dataset.id).addClass("bg-primary");
		}

		function addNewLuggage(){
			Swal.fire({
			  input: 'textarea',
			  inputLabel: 'Tambahkan',
			  inputPlaceholder: 'Tambahkan barang lainnya...',
			  inputAttributes: {
			    'aria-label': 'Type your message here'
			  },
			  showCancelButton: true
			}).then((result) => {
		  		if (result.isConfirmed) {
			  		console.log(result);
			  		
				    Swal.fire(
				      'Berhasil!',
				      'Data berhasil ditambahkan.',
				      'success'
				    )
				    //
				    var num = $('#container-luggage').find(':input[type="number"]').length;
					var myvar = '<div class="form-check" data-luggage="'+(num)+'">'+
					'   <input required="" name="name_luggage" class="form-check-input" type="checkbox" value="'+result.value+'" id="'+(num)+'">'+ //id="defaultCheck8"
					'   <input required="" name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;">'+
					'   <input name="type_luggage" type="hidden" value="LAINNNYA">'+
					'   <input name="desc_luggage" type="hidden" value="">'+
					'   <label class="form-check-label" for="'+(num)+'">'+result.value+'</label>'+
					'</div>';

					$('#container-luggage').append(myvar);;

				    //
			  	}
			})

			/* if (text) {
			  Swal.fire(text)
			} */
		}

		function setNewData(){
			$('#btn-book-1-1').click();
			$('.lbl_start_date').text(moment($('[name=start_date]').val()).format("dddd, DD MMMM YYYY"));
			$('.lbl_end_date').text(moment($('[name=end_date]').val()).format("dddd, DD MMMM YYYY"));
		}

		function reschedule(do_action=0){
			var obj = new FormData();
			if(selected_id != '' && selected_step !='') {
				//start wajib untuk reschedule
				obj.append('id', selected_id);
				obj.append('detil_transit_camp', JSON.stringify(getDataArr().transit));
				obj.append('detil_luggage', JSON.stringify(getDataArr().luggage));
				obj.append('start_date', $('[name=start_date]').val());
				obj.append('end_date', $('[name=end_date]').val());
				//end wajib untuk reschedule
			}
			
			if(selected_id != '' && selected_step != ''){
				if(do_action==0){
					var msg = "<div><strong>Are your booking details correct?</strong></div>";
					msg+="<div class='mt-1'>You will not be able to change your booking details. Do you want to continue?</div>"
					$('#modal-confirm-msg').html(msg);
					$('#modal-confirm').modal('show');
			
					$('#modal-confirm').find('.btn-secondary').text("Check Again");
					$('#modal-confirm').find('.btn-primary').text("Yes, Continue to update");

					$('#btn-modal-reschedule-yes').attr('onclick', 'reschedule(1)');
					return false;
				}
			}
		    ajaxPOST(path_purchase + '/reschedule',obj,'onModalActionSuccess','onModalActionError');
		}
	</script>
  </body>

</html>
