<%@page import="id.co.hijr.ticket.model.VisitorIdentity"%>
<%@page import="id.co.hijr.sistem.service.VaultService"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilVisitor"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="id.co.hijr.ticket.model.PurchaseDetilTransitCamp"%>
<%@page import="id.co.hijr.ticket.model.Luggage"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTransitCampMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionTransitCamp"%>
<%@page import="id.co.hijr.ticket.mapper.RegionPhotoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionPhoto"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="id.co.hijr.ticket.mapper.TourGuideMapper"%>
<%@page import="id.co.hijr.ticket.model.TourGuide"%>
<%@page import="java.util.HashSet"%>
<%@page import="id.co.hijr.ticket.model.TypeVehicle"%>
<%@page import="id.co.hijr.ticket.mapper.TypeVehicleMapper"%>
<%@page import="id.co.hijr.ticket.model.Vehicle"%>
<%@page import="id.co.hijr.ticket.mapper.VehicleMapper"%>
<%@page import="id.co.hijr.ticket.mapper.PurchaseMapper"%>
<%@page import="id.co.hijr.ticket.model.Purchase"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.ticket.mapper.RegionFacilityMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionFacility"%>
<%@page import="id.co.hijr.ticket.mapper.RegionTodoMapper"%>
<%@page import="id.co.hijr.ticket.model.RegionTodo"%>
<%@page import="id.co.hijr.sistem.mapper.FileMapper"%>
<%@page import="id.co.hijr.sistem.model.File"%>
<%@page import="id.co.hijr.sistem.common.QueryParameter"%>
<%@page import="java.util.List"%>
<%@page import="id.co.hijr.ticket.model.Region"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="id.co.hijr.ticket.mapper.RegionMapper"%>
<%
ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());

RegionMapper regionMapper = appCtx.getBean(RegionMapper.class);
FileMapper fileMapper = appCtx.getBean(FileMapper.class);
RegionTodoMapper regionTodoMapper = appCtx.getBean(RegionTodoMapper.class);
RegionFacilityMapper regionFacilityMapper = appCtx.getBean(RegionFacilityMapper.class);
RegionPhotoMapper regionPhotoMapper = appCtx.getBean(RegionPhotoMapper.class);
RegionTransitCampMapper regionTransitCampMapper = appCtx.getBean(RegionTransitCampMapper.class);
PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper = appCtx.getBean(PurchaseDetilTransitCampMapper.class);


PurchaseMapper purchaseMapper = appCtx.getBean(PurchaseMapper.class);
VehicleMapper vehicleMapper = appCtx.getBean(VehicleMapper.class);
TypeVehicleMapper typeVehicleMapper = appCtx.getBean(TypeVehicleMapper.class);
TourGuideMapper tourGuideMapper = appCtx.getBean(TourGuideMapper.class);

String id = request.getParameter("id")!=null?request.getParameter("id"):"0";
Purchase purchase = new Purchase();
System.out.println("step: "+ request.getParameter("step"));
if(request.getParameter("step") != null){

QueryParameter param_detil = new QueryParameter();
param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
	purchase = purchaseMapper.getListExtended(param_detil).get(0);
	if(!purchase.getStatus().equals("DRAFT")){
		if(request.getServletPath().contains("new-booking")){
			
		}else{
			response.sendRedirect(request.getContextPath()+"/page/payment?id="+id);
		}
	}

	if(purchase.getStatusExpiredDate()==1){ 
		response.sendRedirect("/kawasan/page/expired?id="+id);
	} 
	id=purchase.getRegionId();
}
Region region = regionMapper.getEntity(id);
QueryParameter param = new QueryParameter();
param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+region.getId()+"'");
List<File> list_photo = fileMapper.getList(param);
//region.setList_photo(list_photo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+region.getId()+"'");
List<RegionTodo> list_todo = regionTodoMapper.getList(param);
region.setList_todo(list_todo);
param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");
List<RegionFacility> list_facility = regionFacilityMapper.getList(param);
if(request.getParameter("step") == null) region.setList_facility(list_facility);
//
param = new QueryParameter();
//param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+region.getId()+"'");/*  */
List<Vehicle> list_vehicle = vehicleMapper.getList(param);


param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionPhoto.HEADER_ID+"='"+region.getId()+"'");
List<RegionPhoto> list_photo_transction  = regionPhotoMapper.getList(param);

param = new QueryParameter();
param.setClause(param.getClause()+" AND "+RegionTransitCamp.HEADER_ID+"='"+region.getId()+"'");
List<RegionTransitCamp> list_transit  = regionTransitCampMapper.getList(param);

//untuk format tanggal atau waktu
SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy");
SimpleDateFormat sdf_MY = new SimpleDateFormat("MMMM yyyy");
SimpleDateFormat sdftime = new SimpleDateFormat("hh.mm aa");
SimpleDateFormat sdftimeleft = new SimpleDateFormat("hh:mm:ss");
SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
SimpleDateFormat formatDateHTML = new SimpleDateFormat("yyyy-MM-dd");

//untuk format currency
NumberFormat formatter = NumberFormat.getCurrencyInstance();

List<PurchaseDetilTransitCamp> listTransitCamp = purchaseDetilTransitCampMapper.getList(new QueryParameter());
//add some objects to the list...


String jsonTransitCamp = new Gson().toJson(listTransitCamp);
request.setAttribute("kphan", jsonTransitCamp);

VaultService vaultService = appCtx.getBean(VaultService.class);
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<%@ include file = "inc/header_khusus.jsp" %>
<style>
.popover {
	width: 30%;max-width: 50%;
}

.card.lock::before{
  content:'';
  position:absolute;
  z-index:999;
  top:0;
  right:0;
  bottom:0;
  left:0;
  background-color: #e9ecef;
  opacity: 1;
}



/* // Small devices (landscape phones, 576px and up) */
@media (min-width: 320px) {
	
}

@media (min-width: 576px){
	
}

/* // Medium devices (tablets, 768px and up) */
@media (min-width: 768px) {
	.sticky-detil {
		position: fixed;
		top: 30px; 
		width: 378px;
	}
	.sticky-region-detil {
		position: fixed;
		top: 30px; 
		width: 350px;
	}
	.sticky-price-detil {
		margin-top: 200px;
	    position: fixed;
	    width: 350px;
	}
}

/* // Large devices (desktops, 992px and up) */
@media (min-width: 992px) { 
	.sticky-detil {
		position: fixed;
		top: 30px; 
		width: 378px;
	}
	.sticky-region-detil {
		position: fixed;
		top: 30px; 
		width: 350px;
	}
	.sticky-price-detil {
		margin-top: 200px;
	    position: fixed;
	    width: 350px;
	}
}

/* // Extra large devices (large desktops, 1200px and up) */
@media (min-width: 1200px) {
	.sticky-detil {
		position: fixed;
		top: 30px; 
		width: 378px;
	}
	.sticky-region-detil {
		position: fixed;
		top: 30px; 
		width: 350px;
	}
	.sticky-price-detil {
		margin-top: 200px;
	    position: fixed;
	    width: 350px;
	}
}

.swal2-input[type=number] {
    max-width: none!important;
}
		
</style>
  <body id="page-top" style="background: linear-gradient(to bottom right, rgb(2,124, 161)0%, rgb(149, 169, 18)100%);height: 100%;background:#e6eaed!important">
	
    <%-- <%@ include file = "inc/navbar.jsp" %> --%>
	<!-- costume -->
	
	<c:if test="${param.step != null && param.step == 1}">		
		<nav class="navbar navbar-expand-lg navbar-light bg-default">
		<div class="container p-3">
		
		  <a class="navbar-brand text-white" href="${pageContext.request.contextPath}/page/general">
		    <img src="${contextPathPublic}/images/logo-tnghs.png" width="35" height="35" class="d-inline-block align-top rounded-circle" alt="">
		    Kawasan
		  </a>
		  
		<ul class=" nav nav-pills ml-auto small d-none d-sm-flex">
		  <li class="nav-item">
		    <a id="btn-book-1" class="nav-link active text-white rounded" data-toggle="pill" href="#pills-book-1">1. Information</a>
		  </li>
		  <!-- data-toggle="pill" href="#pills-book-1-1 disabled" -->
		  <li class="nav-item">
		    <a id="btn-book-1-1" class="nav-link text-white rounded" >2. Transit Camp</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-2" class="nav-link d-none" data-toggle="pill" href="#pills-book-2">Vehicle</a>
		  </li>
		  <li class="nav-item">
		    <a id="btn-book-3" class="nav-link d-none" data-toggle="pill" href="#pills-book-3">Tour Guide</a>
		  </li>
		  <!-- data-toggle="pill" href="#pills-book-4 disabled" -->
		  <li class="nav-item">
		    <a id="btn-book-4" class="nav-link text-white rounded" >3. Luggage</a>
		  </li>
		   <!-- data-toggle="pill" href="#pills-book-5 disabled" -->
		  <li class="nav-item">
		    <a id="btn-book-5" class="nav-link text-white rounded">4. Confirmation</a>
		  </li>
		   <!-- data-toggle="pill" href="#pills-book-5 disabled" -->
		  <li class="nav-item">
		    <a id="btn-book-6" class="nav-link text-white rounded">5. Pay</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link disabled text-white rounded" href="#" tabindex="-1" aria-disabled="true">E-ticket</a>
		  </li>
		</ul>
		</div>
		</nav>
	</c:if>
	<!--  -->
    <div id="wrapper">

    <%-- <%@ include file = "inc/sidebar.jsp" %>  --%> 

      <div id="content-wrapper">
		
		<c:if test="${param.step != null && param.step == 1}">	
		<%
			
		%>	
		<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-book-1">
		<!-- start -->
		
        <div class="container">
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			
			
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-8 hv-md-200">
			
				<div class="card mb-3">
				    <div class="card-body">
						  <!-- <div class="form-row"> -->
								<!-- <div class="form-group col">
								    <label for="">No. Identity<span class="text-danger">*</span></label>
								    <input type="text" class="form-control" name="no_identity_contact">
								</div>
								<div class="form-group col">
								    <label for="">Full Name<span class="text-danger">*</span></label>
								    <input type="text" class="form-control" name="full_name_contact">
								</div> -->
								<!-- <div class="form-group col"> -->
								    <div class=" text-primary" onclick="">Logged in as <span class="font-weight-bold">${requestScope.realName}</span></div>
								    <div class="text-muted d-none">via google</div>
								<!-- </div> -->
						  <!-- </div> -->
				    </div>
				</div>
			
			<% 
				//vaultService.decrypt(pdv.getVisitorIdentity().getNoIdentity())
				PurchaseDetilVisitor opdv = purchase.getPurchaseDetilVisitor().get(0);
				opdv.getVisitorIdentity().setNoIdentity(vaultService.decrypt(opdv.getVisitorIdentity().getNoIdentity()));
			%>
			<h4 class="">Contact Details</h4>
	    	<form id="form-contact-detil">
				<div data-key="0" class="card mb-3">
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <h5 class="mb-0" id=""><a data-toggle="collapse" href="#collapsePerson-0" class="collapsed" aria-expanded="false">Person in charge</a><span class="d-none d-sm-inline-block"></span></h5>
	                </div>
	              </div>
				</div>
				<div class="collapse" id="collapsePerson-0" style="">
				    <div class="card-body">
						  <div class="form-row">
								<div class="form-group col-md-6">
								    <label for="">No. Identity<span class="text-danger">*</span></label>
								    <input type="text" class="form-control" name="no_identity" value="<% out.print(opdv.getVisitorIdentity().getNoIdentity()); %>">    		
								    <small id="" class="form-text text-muted">*Sesuai KTP/paspor/SIM (tanpa tanda baca atau gelar)</small>
								</div>
								<div class="form-group col-md-6">
								    <label for="">Full Name<span class="text-danger">*</span></label>
								    <input type="text" class="form-control" name="full_name" value="<% out.print(opdv.getVisitorIdentity().getFullName()); %>">
								</div>
						  </div>
						  
						  <div class="form-row">
								<div class="form-group col">
								    <label for="">Type Identity<span class="text-danger">*</span></label>
			                      	<select class="form-control" name="type_identity">
			                          <option value="">Choose Type Identity</option>
			                          <option value="NIK" <% out.print(opdv.getVisitorIdentity().getTypeIdentity().equals("NIK")?"selected":""); %>>NIK (WNI)</option>
			                          <option value="PASSPORT" <% out.print(opdv.getVisitorIdentity().getTypeIdentity().equals("PASSPORT")?"selected":""); %>>Passport/Kitas (WNA)</option>
			                        </select>
								</div>
								<div class="form-group col">
								    <label for="">Upload Identity<span class="text-danger">*</span></label>
								    <input type="file" disabled="disabled" class="form-control" name="file_identity"  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">
								</div>
						  </div>
						  
						  
						  <div class="form-row ">
								<div class="col">
						    		<label for="">Birth Date<span class="text-danger">*</span></label>
								<div class="form-group">
								      <input type="date" class="form-control" name="birthdate" value="<% out.print(formatDateHTML.format(opdv.getVisitorIdentity().getBirthdate())); %>">
								</div>
								</div>
								<div class="col">
						    		<label for="">Gender<span class="text-danger">*</span></label>
									<div class="form-group ">
										<select class="form-control" name="gender">
											<option value="">-</option>
											<option value="L" <% out.print(opdv.getVisitorIdentity().getGender().equals("L")?"selected":""); %>>LAKI-LAKI</option>
											<option value="P" <% out.print(opdv.getVisitorIdentity().getGender().equals("P")?"selected":""); %>>PEREMPUAN</option>
										</select>
									</div>
								</div>
						  </div>
						  
						  <div class="form-row">
							<div class="col-md-6">
					    		<label for="">Mobile Number<span class="text-danger">*</span></label>
								<div class="input-group">		      
									<select class="form-control" name="phone_code">
								        <option value="+62">+62</option>
								    </select>
									<input type="number" class="form-control  w-auto" name="phone_number" autocomplete="false" value="<% out.print(opdv.getPhoneNumber().replace("+62", "")); %>">
								</div>
					    		<small id="" class="form-text text-muted text-justify">Contoh : 0812345678, Maka 812345678(tanpa 0 didepan)
					    		</small>
							</div>
							<div class="col-md-6">
					    		<label for="">Email<span class="text-danger">*</span></label>
							    <input type="text" class="form-control" name="email" value="<% out.print(opdv.getEmail()); %>">
							    <small id="" class="form-text text-muted">*Email anda yg aktif(Karena email akan menerima Invoice Pembayaran dan Tiket)</small>
							</div>
						  </div>
						  
						  <div class="form-row">
								<div class="form-group col-md-12">
						    		<label for="">Alamat<span class="text-danger">*</span></label>
									<textarea class="form-control" name="address" spellcheck="false"><% out.print(opdv.getVisitorIdentity().getAddress()); %></textarea>
								</div>
						  </div>
						  
						  <div class="form-row">
								<div class="col-md-12">
						    		<label class="font-weight-bold">Information of family who can be contacted<span class="text-danger">*</span></label>
								</div>
						  </div>
						  <div class="form-row ">
								<div class="form-group col-md-12">
									<input type="number" class="form-control" name="phone_number_family" placeholder="Phone number of family" value="<% out.print(opdv.getPhoneNumberFamily().replace("+62", "")); %>">
									<small id="" class="form-text text-muted">*Mohon Cantumkan, Nomor keluarga, agar memudahkan admin menghubungi keluarga anda, apabila terjadi sesuatu yang tidak dinginkan.</small>
								</div>
						  </div>
						  <div class="form-row ">
								<div class="form-group col-md-12">
								      <div class="input-group">		
										<textarea  class="form-control w-auto" name="address_family" placeholder="Entry Address of family"><% out.print(opdv.getAddress()); %></textarea>
									  </div>
								      <small id="" class="form-text text-muted">*Mohon Cantumkan, Alamat yang lengkap, agar memudahkan admin menghubungi keluarga anda, apabila terjadi sesuatu yang tidak dinginkan.</small>
								</div>
						  </div>
				    </div>
				    
					<div class="card-footer bg-light">
		              <!-- <div class="row align-items-center">
		                <div class="col">
		                  <h5 class="mb-0" id="">Person in charge<span class="d-none d-sm-inline-block"></span></h5>
		                </div>
		              </div> -->
		              
	                    <div class="row no-gutters float-right">
	                   	<!--  d-md-block d-none -->
	                      <div class="col ">
	                      	<div class="form-group form-check small">
							   <input type="checkbox" class="form-check-input form-control-label" id="checkAsLeader" checked disabled>
							   <label class="form-check-label font-weight-bold" for="checkAsLeader">Saya Sebagai Leader/Ketua TIM</label>
							</div>
	                      </div>
	                    </div>
	                    
					</div>
					
				  </div>
				</div>
		
			</form>
			
			<h4 class="">Traveler Details</h4>
	    	<form id="form-travel-detil">
	    	  <%
			  	int price_wna=0;
			  	int price_wni=0;
				for(RegionFacility o : list_facility){
					if(o.getCode().equals("WNA")) price_wna+=o.getPrice();
					if(o.getCode().equals("DEFAULT")) price_wni+=o.getPrice();
				}
			  %>
			<% 
			int no=1;
        	for(PurchaseDetilVisitor pdv : purchase.getPurchaseDetilVisitor()){
        		pdv.getVisitorIdentity().setNoIdentity(vaultService.decrypt(pdv.getVisitorIdentity().getNoIdentity()));
        		
        		if(pdv.getResponsible().equals("0")){
        		%>
        		<% if(no==1) { %>
        		<% } %>
					<div class="card mb-3">
					<div class="card-header bg-light">
		              <div class="row align-items-center">
		                <div class="col">
		                <% if(no==1) { %>
		                  <h5 class="mb-0" id="followers"><a data-toggle="collapse" href="#collapsePerson-<%out.print((no)); %>" class="collapsed" aria-expanded="false">Person <span class=""><%out.print((no)); %> (Leader/Ketua TIM)</span></a></h5>
	                  	<% }else{ %>
		                  <h5 class="mb-0" id="followers"><a data-toggle="collapse" href="#collapsePerson-<%out.print((no)); %>" class="collapsed" aria-expanded="false">Person <span class=""><%out.print((no)); %></span></a></h5>
	                  	<% } %>
		                </div>
		               
		              </div>
					</div>
					<div data-key="<%out.print((no)); %>" class="collapse" id="collapsePerson-<%out.print((no)); %>" style="">
					    <div class="card-body">
							  <div class="form-row">
									<div class="form-group col-md-6">
									    <label for="">No. Identity<span class="text-danger">*</span></label>
									    <input type="text" class="form-control" name="no_identity" value="<% out.print(pdv.getVisitorIdentity().getNoIdentity()); %>">
							    		<small id="" class="form-text text-muted">*Sesuai KTP/paspor/SIM (tanpa tanda baca atau gelar)</small>
									</div>
									<div class="form-group col-md-6">
									    <label for="">Full Name<span class="text-danger">*</span></label>
									    <input type="text" class="form-control" name="full_name" value="<% out.print(pdv.getVisitorIdentity().getFullName()); %>">
									</div>
							  </div>
							  <div class="form-row">
									<div class="form-group col">
									    <label for="">Type Identity<span class="text-danger">*</span></label>
				                      	<select class="form-control" name="type_identity" data-price_wna="<% out.print(price_wna); %>" data-price_wni="<% out.print(price_wni); %>">
				                          <option value="">Choose Type Identity</option>
				                          <option value="NIK" <% out.print(pdv.getVisitorIdentity().getTypeIdentity().equals("NIK")?"selected":""); %>>NIK (WNI)</option>
				                          <option value="PASSPORT" <% out.print(pdv.getVisitorIdentity().getTypeIdentity().equals("PASSPORT")?"selected":""); %>>Passport/Kitas (WNA)</option>
				                        </select>
									</div>
									<div class="form-group col">
									    <label for="">Upload Identity<span class="text-danger">*</span></label>
									    <div id="form-file-<%out.print((no)); %>"><input disabled="disabled" type="file" class="form-control" name="file_identity"  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" ></div>
										<%-- <td class="text-center"><a href="javascript:void(0)" onclick="swall_view('/kawasan/files/<% out.print(pdv.getVisitorIdentity().getIdFile()); %>?filename=<% out.print(pdv.getVisitorIdentity().getFileName()); %>&download&decrypt','<% out.print(pdv.getPhoneNumberFamily()); %>','<% out.print(pdv.getAddressFamily()); %>');" class="btn btn-sm small btn-square btn-info pill" type=""><i class="fas fa-fw fa-eye"></i></a></td> --%>
									</div>
							  </div>
							  <div class="form-row ">
									<div class="col">
							    		<label for="">Birth Date<span class="text-danger">*</span></label>
									<div class="form-group">
									      <input type="date" class="form-control" name="birthdate" value="<% out.print(formatDateHTML.format(pdv.getVisitorIdentity().getBirthdate())); %>">
									</div>
									</div>
									<div class="col">
							    		<label for="">Gender<span class="text-danger">*</span></label>
									<div class="form-group ">
										<select class="form-control" name="gender">
											<option value="">-</option>
											<option value="L" <% out.print(pdv.getVisitorIdentity().getGender().equals("L")?"selected":""); %>>LAKI-LAKI</option>
											<option value="P" <% out.print(pdv.getVisitorIdentity().getGender().equals("P")?"selected":""); %>>PEREMPUAN</option>
										</select>
									</div>
									</div>
							  </div>
							  
							  
							  <div class="form-row">
								<div class="col-md-6">
						    		<label for="">Mobile Number<span class="text-danger">*</span></label>
									<div class="input-group">		      
										<select class="form-control" name="phone_code">
									        <option value="+62">+62</option>
									    </select>
										<input type="number" class="form-control  w-auto" name="phone_number" autocomplete="false" value="<% out.print(pdv.getPhoneNumber().replace("+62", "")); %>">
									</div>
								</div>
								<div class="col-md-6">
						    		<label for="">Email<span class="text-danger">*</span></label>
								      <input type="text" class="form-control" name="email" value="<% out.print(pdv.getEmail()); %>">
								</div>
							  </div>
							  
							  <div class="form-row">
									<div class="form-group col-md-12">
							    		<label for="">Alamat<span class="text-danger">*</span></label>
										<textarea class="form-control" name="address" spellcheck="false"><% out.print(pdv.getAddress()); %></textarea>
									</div>
							  </div>
					    </div>
					    <div class="card-footer bg-light">
			              	<div class="row">
					      	<h5 class="card-title font-weight-bolsd col">Price to Pay</h5>
					      	<h5 class="card-title font-weight-bosld pull-right text-right col">IDR <span class="currency" id="total-price-pay-<%out.print((no)); %>">0</span><span class="d-none">(4x)</span></h5>
					    	</div>
						</div>
					</div>
					</div>
        		<%
          		no++;
        		}
        	}
        	%>
			</form>
			
			
			
			<h4 class="">
				<button onclick="addNewPerson()" class="btn " style="color: #fff;background-color: #3c465f;"><span class="fa fa-plus"></span> Tambahkan Pendaki Baru </button>
			</h4>
			
	    	<form id="form-new-travel-detil">
			</form>
			
			</div>
			<div class="col-md-4 p-0 hv-md-80" style="position: sticky;top: 2%;">
			<div class="card-detil">
			<div class="card">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right small">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				int key=0;

					String path=request.getContextPath()+"/images/notfound.png";
					  for(RegionPhoto rp: list_photo_transction) {
						path=request.getContextPath()+"/files/"+rp.getFileId()+"?filename="+rp.getNameFile()+"";
				  		

						if(rp.isActive()){
							out.println("<img style='width:70px;height:70px;object-fit:cover' src='"+path+"' class='mr-3 img-fluid rounded'>");
						}
					  }
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 data-id_kawasan="<% out.print(region.getId()); %>" data-biaya_kawasan="<% out.print(purchase.getAmountTotalTicket()); %>" class="mt-0"><% out.print(region.getName()); %></h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Start Date</span> <span id="start_date" class="float-right small"><% out.print(sdf.format(purchase.getStartDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">End Date</span> <span id="end_date" class="float-right small"><% out.print(sdf.format(purchase.getEndDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span id="total-visitor" data-price_wni="<% out.print(price_wni); %>" data-price_wna="<% out.print(price_wna); %>" data-count_wna="<% out.print(purchase.getCountTicketWna()); %>" data-count_wni="<% out.print(purchase.getCountTicketWni()); %>" class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total New Visitors</span> <span data-new_visitor_count_wni="0" data-new_visitor_count_wna="0" class="float-right small">General: 0</span></li>
			  </ul>
			  <div class="card-body">
			  <% if(region.getRefund().equals("1")) {
				    out.print("<span class='text-success'><i class='far fa-check-circle'></i> Refundable</span>");
				 }else{
					out.print("<span class='text-muted'><i class='fas fa-ban'></i> Non Refundable</span>");
				 }
			    %>
			  </div>
			</div>
			
			
			<div class="card mt-3" id="card-price-detil">
			<!-- <div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Rincian Harga</span> <a href="#" class="card-link float-right small">Details</a></h5></div> -->
			<div class="card-body">
				<div class="media">
				 <div class="media-body">
				    <h5 class="mt-0">Total Harga</h5>
				  </div>
				</div>
				
			 	<span id="price-detil-value" data-price-rebook="0" class='text-muted'><% out.print(formatter.format(0)); %></span>
			</div>
			</div>
			
			<div class="row justify-content-end mt-3">
				<div class="col-md-12">
					<button type="button" onclick="$('#btn-book-1-1').click();" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
				</div>
			</div>
			
			</div>
			</div>
			
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		
		<div class="tab-pane fade " id="pills-book-1-1">
		<!-- start -->
		
        <div class="container">
		<div class="row mt-3">
			<div class="col-md-8">
			<h4 class="">Transit Camp Dan Jalur Turun</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-8">
				<div class="card mb-3"> <!-- data-key="0"  -->
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <div class="mb-0 font-weight-bold" id="">Pilih Transit Camp (TC)<span class="d-none d-sm-inline-block"></span></div>
	                </div>
	              </div>
				</div>
				    <div class="card-body">
				    	<div class="border mb-3 bg-light d-nones" style="text-align: -webkit-center;">
						  <img class="img-fluid" alt="" src="${pageContext.request.contextPath}/files/<% out.print(region.getPhoto()); %>?filename=<% out.print(region.getNameFile()); %>" usemap="#housemap">
							<!-- x,y,radius -->
							<!-- <map name="housemap">
							  <area shape="circle" coords="15,35,10" href="javascript:void(0)" alt="1">
							  <area shape="circle" coords="42,85,10" href="javascript:void(0)" alt="2">
							  <area shape="circle" coords="95,138,10" href="javascript:void(0)" alt="3">
							  <area shape="circle" coords="115,180,10" href="javascript:void(0)" alt="4">
							</map> -->					
						</div>
						  <table class="table table-bordered small">
							  <thead class="thead-dark ">
							    <tr>
							      <th scope="col" class="align-middle d-none d-md-table-cell">#</th>
							      <th scope="col" class="align-middle">Registrasi di Basecamp</th>
							      <th scope="col" class="align-middle">Tujuan Basecamp</th>
							      <th scope="col" class="align-middle">Perkiraan Tiba di<br/>Transit Camp</th>
							    </tr>
							  </thead>
							  <tbody>
							  <% for(RegionTransitCamp rtc : list_transit) {
									%>
									<tr data-name-tent="<% out.print(rtc.getNameMasterTransitCamp()); %>" data-code-tent="<% out.print(rtc.getCodeMasterTransitCamp()); %>" data-small-tent="<% out.print(rtc.getNumberOfSmallTents()); %>" data-large-tent="<% out.print(rtc.getNumberOfLargeTents()); %>" class="row-rtc" data-id_photo="<% out.print(rtc.getPhoto()); %>" data-name_photo="<% out.print(rtc.getNameFile()); %>" data-id="<% out.print(rtc.getIdMasterTransitCamp()); %>" id="row-rtc-<% out.print(rtc.getIdMasterTransitCamp()); %>" onclick="$('[name=id_master_transit]').val(<% out.print(rtc.getIdMasterTransitCamp()); %>);setActiveRow(this);createFormTent(<% out.print(rtc.getNumberOfSmallTents()); %>, <% out.print(rtc.getNumberOfLargeTents()); %>, '<% out.print(rtc.getCodeMasterTransitCamp()); %>', this)" >
								      <td scope="row" class="d-none d-md-table-cell"><% out.print(rtc.getCode()); %></td>
								      <td><% out.print(sdftime.format(rtc.getStartRegister())); %>-<% out.print(sdftime.format(rtc.getEndRegister())); %></td>
								      <td><span class="d-md-none d-table-cell"><% out.print(rtc.getCode()); %></span><% out.print(rtc.getNameMasterTransitCamp()); %></td>
								      <td><% out.print(sdftime.format(rtc.getExtimateArrived())); %></td>
								    </tr>
							 <%	 }
							  %>
							  </tbody>
							</table>
				    </div>
				</div>
				
				<div class="card mb-3 d-none">
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <div class="mb-0">Pilih Kapling Tenda: <span id="title-choose-capling-tent" class="font-weight-bold d-sm-inline-block">-</span></div>
	                </div>
	              </div>
				</div>
				    <div class="card-body">
						<div class="row">
			        	<div class="col-md-6">
			        	<div id="row-tenda-kecil" class="form-row row-cols-5 justify-content-between">
			        	</div>
			        	</div>
			        	<div class="col-md-6">
			        	<div id="row-tenda-besar" class="form-row row-cols-5 justify-content-between">
			        	</div>
			        	</div>
			        	</div>
				    </div>
				</div>
				
				<div class="card mb-3"> <!-- data-key="0"  -->
				<div class="card-header bg-light">
	              <div class="row align-items-center">
	                <div class="col">
	                  <div class="mb-0 font-weight-bold">Pilih Pintu Turun/Keluar</div>
	                </div>
	              </div>
				</div>
				    
				    <div class="card-body">
						  <table class="table table-bordered small">
							  <thead class="thead-dark ">
							    <tr>
							      <th scope="col" class="align-middle d-none d-md-table-cell text-center">#</th>
							      <th scope="col" class="align-middle">Pintu Turun/Keluar</th>
							      <th scope="col" class="align-middle">Keterangan</th>
							    </tr>
							  </thead>
							  <tbody>
							  <% int nox=1; for(Region rg : regionMapper.getList(new QueryParameter())) {
									%>
									<tr data-name-region="<% out.print(rg.getName()); %>" class="row-region-turun" data-id="<% out.print(rg.getId()); %>" id="row-region-turun-<% out.print(rg.getId()); %>" onclick="setActiveRow(this, 'row-region-turun');" >
								      <td class='text-center'><% out.print(nox); %></td>
								      <td scope="row" class="d-none d-md-table-cell"><% out.print(rg.getName()); %></td>
								      <td class='text-truncate'>-</td>
								    </tr>
							 <%	nox++;
							 	}
							  %>
							  </tbody>
							</table>
				    </div>
				</div>
		
	    	<form id="form-transit-camp-detil" class="d-none">
			  
			  <div class="form-row">
					<div class="form-group col">
					    <label for="">Jumlah Tenda<span class="text-danger">*</span></label>
                      	<select class="form-control" name="count_camp">
                          <option value="">Choose </option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                        </select>
					</div>
			  </div>
			  <div class="form-row">
					<div class="col-md-6">
			    		<label for="">Pilih Tempat Transit<span class="text-danger">*</span></label>
			    		<select class="form-control" name="id_master_transit">
							<option value="">Choose</option>
							<% for(RegionTransitCamp rtc : list_transit) {
								out.print("<option value='"+rtc.getIdMasterTransitCamp()+"'>"+rtc.getCode()+" "+rtc.getNameMasterTransitCamp()+"</option>");
								}
							%>
						</select>
					</div>
					<div class="col-md-6">
			    		<label for="">Nomor Kapling<span class="text-danger"></span></label>
                      	<select class="form-control" name="code_unique">
                        </select>
					</div>
			  </div>
			</form>
			
			<div class="row mb-3 justify-content-between">
				<div class="col-6">
					<button type="button" onclick="$('#btn-book-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 ">Prev</button>
				</div>
				<div class="col-6 text-right">
					<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 ">Continue</button>
				</div>
			</div>
			
			</div>
			<div class="col-md-4 p-0 hv-md-80" style="position: sticky;top: 2%;">
			<div class="card-detil">
			<div class="card">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				key=0;
				  	path=request.getContextPath()+"/images/notfound.png";
					for(RegionPhoto rp: list_photo_transction) {
						
						path=request.getContextPath()+"/files/"+rp.getFileId()+"?filename="+rp.getNameFile()+"";
				  	
						if(rp.isActive()){
							out.println("<img style='width:70px;height:70px;object-fit:cover' src='"+path+"' class='mr-3 img-fluid rounded'>");
						}
					}
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 data-id_kawasan="<% out.print(region.getId()); %>" data-biaya_kawasan="<% out.print(purchase.getAmountTotalTicket()); %>" class="mt-0"><% out.print(region.getName()); %></h5>
				   <!--  nunc ac nisi . -->
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Start Date</span> <span data-start_date="<% out.print(sdfNew.format(purchase.getStartDate())); %>" class="float-right small"><% out.print(sdf.format(purchase.getStartDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">End Date</span> <span data-end_date="<% out.print(sdfNew.format(purchase.getEndDate())); %>" class="float-right small"><% out.print(sdf.format(purchase.getEndDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span data-count_ticket="<% out.print(purchase.getCountTicket()); %>" class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total New Visitors</span> <span data-new_visitor_count_wni="0" data-new_visitor_count_wna="0" class="float-right small">General: 0</span></li>
			  </ul>
			  <div class="card-body">
			  <% if(region.getRefund().equals("1")) {
				    out.print("<span class='text-success'><i class='far fa-check-circle'></i> Refundable</span>");
				 }else{
					out.print("<span class='text-muted'><i class='fas fa-ban'></i> Non Refundable</span>");
				 }
			    %>
			  </div>
			</div>
			
			<div class="card mt-3">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Denah Tenda</span> <a href="#" class="card-link float-right"></a></h5></div>
			  <div class="card-body">
			  	<img id="img-photo-tent" class="img-fluid" src="${pageContext.request.contextPath}/images/notfound.png">
			  </div>
			</div>
			</div>
			</div>
			
		</div>
		
		
        </div>
		</div>
		
		<div class="tab-pane fade" id="pills-book-2" >
		<!-- start -->
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Vehicle Options</h4>
			<h4 class="mb-3"><small class="text-muted">Choose vehicle for you tour</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
			<ul class="nav nav-pills justify-content-center border-bottom border-info">
				
			 <%
			 List<String> str = new ArrayList<>();
			 for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){
				 for(Vehicle vehicle : list_vehicle){
					 if(tv.getId().equals(vehicle.getTypeId())){
					 	if(!str.contains(tv.getName()))
						 str.add(tv.getName());
					 }
				 }
			 }
			 System.out.print("V: "+str);
			 key=0;
				for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){ %>
					<% 
					for(int i=0; i < str.size(); i++) {
						if(str.get(i).equals(tv.getName())){
							%>
							<li class="nav-item">
					    	<a class="nav-link <% if(key==0) out.print("active"); %>" id="pills-car<%out.print(tv.getId());%>-tab" data-toggle="pill" href="#pills-car<%out.print(tv.getId());%>"><% out.print(tv.getName()); %></a>
						   	</li>
							<%
						}
					}
					%>
					
				   	
	 		 <% 
	 		 	key++;
	 		 	} 
	 		 %> 	
			</ul>
		</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="tab-content" id="pills-tabContent">
		<% key=0; for(TypeVehicle tv : typeVehicleMapper.getList(new QueryParameter())){ %>
			<div class="tab-pane fade<% if(key==0) out.print("show active"); %>" id="pills-car<%out.print(tv.getId());%>"><!-- show active -->
				<div data-id="container" class="row">
				<%
					path=request.getContextPath()+"/images/notfound.png";
					for(Vehicle vehicle : list_vehicle){ 
						param = new QueryParameter();
						param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+vehicle.getId()+"'");
						System.out.println("si "+fileMapper.getList(param).size()+" "+vehicle.getId());
						

						//String path=request.getContextPath()+"/images/notfound.png";
						vehicle.setPhoto(path);
						for(File vehicle_file : fileMapper.getList(param)){ 
							//vehicle.setPhoto(path);
							
							if(tv.getId().equals(vehicle.getTypeId())){
								vehicle_file.setName(vehicle_file.getName().replace(" ", "%"));
							  	path=request.getContextPath()+"/files/"+vehicle_file.getId()+"?filename="+vehicle_file.getName()+"&download";
								vehicle.setPhoto(path);
							}
						}
						
						if(tv.getId().equals(vehicle.getTypeId())){
						%>
						<div class="col-md-4 mt-3">
						<div <% out.print("data-id_vehicle="+vehicle.getId()+""); %> class="card vehicle mb-3">
			  				<img src="<% out.print(vehicle.getPhoto()); %>" class="card-img-top" alt="...">
						    <div class="card-body">
							<div class="form-row">
							<div class="col-md-12">	
								<h5 class="card-title"><% out.print(vehicle.getName()+" ("+vehicle.getTypeName()+")"); %></h5>
					    		<small id="" class="form-text text-muted">Starting From</small>
					    		<div class="row">
								    <div class="col">
								        <small id="" class="form-text text-primary"><% out.print(formatter.format(vehicle.getCost())); %> / Day</small>
								    </div>
								    <div class="col">
								        <input name="qty_vehicle" type="number" class="form-control form-control-sm" value="1" min="1">
								    </div>
								</div>
							</div>
							</div>
						    </div>
						</div>
						</div>
						<%
						}
					} 
				%>
				</div>
			</div>
			<% key++;} %>
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-1-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-3').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
        </div>
        <!-- /.container-fluid -->
		<!-- end -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-3" >
			
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">Tour Guide</h4>
			<h4 class="mb-3"><small class="text-muted">Choose someone to become your tour guide</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="card bg-light">
			<div class="card-body bg-light p-0s">
	    	<div class="row no-gutterss text-center fs--1">
	    	<%
	    	
    		for(TourGuide tg : tourGuideMapper.getList(new QueryParameter())){
	    		param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+tg.getId()+"'");
			  	for(File tour_file : fileMapper.getList(param)){
					tour_file.setName(tour_file.getName().replace(" ", "%"));
				  	path=request.getContextPath()+"/files/"+tour_file.getId()+"?filename="+tour_file.getName()+"&download";
				  	//path=request.getContextPath()+"/files/"+vehicle_file.getId()+"?filename="+vehicle_file.getName()+"&download";
					if(tg.getPhoto()!=null){
				  		tg.setPhoto(path);
					}
	    		}
			  	path=request.getContextPath()+"/images/notfound.png";
			  	if(tg.getPhoto()==null){
			  		tg.setPhoto(path);
				}

               	out.print("<div class='col-6 col-md-4 col-xl-3 col-xxl-2 mb-1'>");
    			out.print("<div data-id_tour_guide="+tg.getId()+" class='tour-guide bg-whitse p-3 h-100'>");
    			
    			out.print("<a href='javascript:void(0)'><img class='img-thumbnail img-fluid rounded-circle mb-3 shadow-sm' src="+tg.getPhoto()+" width='100'></a>");
   				
    			out.print("<h6 class='mb-1'><span>"+tg.getFullName()+"</span></h6>");

   				out.print("<small id='' class='form-text text-muted'>Starting From</small>");
  					out.print("<small id='' class='form-text text-primary'>"+formatter.format(tg.getCostPerDay())+"/ Day</small>");
				//out.print("<p class='small mb-1'><span class='text-muted'>"+tg.getDescription()+"</span></p>");
               	out.print("</div>");
               	out.print("</div>");
    		}
           	%>
              </div>
	  		</div>
		</div>
		</div>
		</div>
		<div class="row mt-3">
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-2').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-left">Prev</button>
			</div>
			<div class="col-md-6">
				<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 float-right">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-4" >
			
        <div class="container">
		<div class="row mt-3 text-center">
			<div class="col-md-12">
			<h4 class="">List Luggage/Barang Bawaan</h4>
			<h4 class="mb-3"><small class="text-muted">Please make sure you bring this stuff</small></h4>
			</div>
		</div>
		<div class="row mt-3">
		<div class="col-md-12">
		<div class="card bg-light">
		<div class="card-body bg-light p-0s">
		<form id="form-luggage">
		<div class="row no-gutters fs--1">
	    <div class="col-md-6">
	          <div class="form-group">
	            <label for="" class="font-weight-bold">Pribadi</label>
	            <% 
	            QueryParameter paramluggage= new QueryParameter();
	            		paramluggage.setClause(paramluggage.getClause()+" AND "+Luggage.TYPE+" ='"+"PRIBADI"+"'");
	            for(Luggage l : tourGuideMapper.getListLuggage(paramluggage)) { %>
				<div class="form-check" data-luggage="<% out.print(l.getId()); %>">
				  <input required name="name_luggage" class="form-check-input" type="checkbox" value="<% out.print(l.getName()); %>" id="defaultCheck<% out.print(l.getId()); %>">
				  <input required  min="0" name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;"/>
				  <input name="type_luggage" type="hidden" value="PRIBADI"/>
				  <input name="desc_luggage" type="hidden" value=""/>
				  <label title="click to see detail.." ondblclick='swal.fire("", "<% out.print(l.getName()); %>")' class="form-check-label text-truncate w-75" for="defaultCheck<% out.print(l.getId()); %>"><% out.print(l.getName()); %></label>
				</div>
	            <% } %>
	          </div>
	    </div>
	
		<div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1" class="font-weight-bold">Logistik</label>
            <% 
            paramluggage= new QueryParameter();
            		paramluggage.setClause(paramluggage.getClause()+" AND "+Luggage.TYPE+" ='"+"LOGISTIK"+"'");
            for(Luggage l : tourGuideMapper.getListLuggage(paramluggage)) { %>
			<div class="form-check" data-luggage="<% out.print(l.getId()); %>">
			  <input required name="name_luggage" class="form-check-input" type="checkbox" value="<% out.print(l.getName()); %>" id="defaultCheck<% out.print(l.getId()); %>">
			  <input required  min="0" name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;"/>
				  <input name="type_luggage" type="hidden" value="LOGISTIK"/>
				  <input name="desc_luggage" type="hidden" value=""/>
			  <label title="click to see detail.." ondblclick='swal.fire("", "<% out.print(l.getName()); %>")' class="form-check-label text-truncate w-75" for="defaultCheck<% out.print(l.getId()); %>"><% out.print(l.getName()); %></label>
			</div>
            <% } %>
          </div>
	    </div>
		</div>
		
		<div class="row no-gutters fs--1">
	    <div class="col-md-6">
	          <div class="form-group">
	            <label for="exampleInputEmail1" class="font-weight-bold">Kelompok</label>
	            <% 
	            paramluggage= new QueryParameter();
	            		paramluggage.setClause(paramluggage.getClause()+" AND "+Luggage.TYPE+" ='"+"KELOMPOK"+"'");
	            for(Luggage l : tourGuideMapper.getListLuggage(paramluggage)) { %>
				<div class="form-check" data-luggage="<% out.print(l.getId()); %>">
				  <input required name="name_luggage" class="form-check-input" type="checkbox" value="<% out.print(l.getName()); %>" id="defaultCheck<% out.print(l.getId()); %>">
				  <input required  min="0" name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;"/>
				  <input name="type_luggage" type="hidden" value="KELOMPOK"/>
				  <input name="desc_luggage" type="hidden" value=""/>
				  <label title="click to see detail.." ondblclick='swal.fire("", "<% out.print(l.getName()); %>")' class="form-check-label text-truncate w-75" for="defaultCheck<% out.print(l.getId()); %>"><% out.print(l.getName()); %></label>
				</div>
	            <% } %>
	          </div>
	    </div>
	
		<div class="col-md-6">
	          <div id="container-luggage" class="form-group">
	            <label for="exampleInputEmail1" class="font-weight-bold">Barang Bawaan Lainnya</label>
	    		<button type="button" class="btn btn-sm btn-secondary" onclick="addNewLuggage()" >Tambahkan</button>
	          </div>
	          
	    </div>
		</div>
		<button id="btn-luggage" type="submit" class="d-none">-</button>
		</form>
		</div>
		
		</div>
		</div>
		</div>
			
		<div class="row mt-3 mb-3 justify-content-between">
			<div class="col-6">
				<button type="button" onclick="$('#btn-book-1-1').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12">Prev</button>
			</div>
			<div class="col-6 text-right">
				<button type="button" onclick="$('#btn-book-5').click();" class="btn btn-outline-primary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12">Continue</button>
			</div>
		</div>
		
		
        </div>
        <!-- /.container-fluid -->
		</div>
		
		<div class="tab-pane fade" id="pills-book-5">
		<div class="container">
		
		<div class="row mt-3">
			<div class="col-md-8">
			
			<h4 class="">Your Booking</h4>
			<h4 class="mb-3"><small class="text-muted">Fill in your details and review your booking</small></h4>
			
			</div>
		</div>
			
		<div class="row mt-3">
			<div class="col-md-8">
				<div class="card mb-3">
				    <div class="card-body bg-light">
					    <div class=" text-primary" onclick="">Logged in as <span class="font-weight-bold">${requestScope.realName}</span></div>
					    <div class="text-muted d-none">via google</div>
				    </div>
				</div>
				
				<h4 class="">Contact Details</h4>
				<div id="container-person-contact"></div>
				<h4 class="">Traveler Details</h4>
			</div>
			<div class="col-md-4">
			<div class="card-detil">
			<div class="card mb-3">
			<div class="card-header"><h5><i class="fas fa-map-marked-alt text-success"></i><span> Region/Kawasan</span> <a href="#" class="card-link float-right">Details</a></h5></div>
			<div class="card-body">
				<div class="media">
				<%
				key=0;
				path=request.getContextPath()+"/images/notfound.png";
				//for(File o2 : list_photo){
					for(RegionPhoto rp: list_photo_transction) {
						//if(o2.getId().equals(rp.getFileId())){
							if(rp.isActive()){
								out.println("<img style='width:70px;height:70px;object-fit:cover' src='"+path+"' class='mr-3 img-fluid rounded'>");
							}
						//}
						
					}
				//}
				%>
				  <!-- <img style="width:70px;height:70px;object-fit:cover" src="http://localhost:9910/kawasan/files/1553409926207736?filename=mount%bromo.jpg&download" class="mr-3 img-fluid" alt="..."> -->
				  <div class="media-body">
				    <h5 class="mt-0"><% out.print(region.getName()); %></h5>
				  </div>
				</div>
			</div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item"><span class="float-left text-muted small">Start Date</span> <span class="float-right small"><% out.print(sdf.format(purchase.getStartDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">End Date</span> <span class="float-right small"><% out.print(sdf.format(purchase.getEndDate())); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total Visitors</span> <span class="float-right small">General: <% out.print(purchase.getCountTicket()); %></span></li>
			    <li class="list-group-item"><span class="float-left text-muted small">Total New Visitors</span> <span data-new_visitor_count_wni="0" data-new_visitor_count_wna="0" class="float-right small">General: 0</span></li>
			  </ul>
			  <div class="card-body">
			  	<% if(region.getRefund().equals("1")) {
				    out.print("<span class='text-success'><i class='far fa-check-circle'></i> Refundable</span>");
				 }else{
					out.print("<span class='text-muted'><i class='fas fa-ban'></i> Non Refundable</span>");
				 }
			    %>
			  </div>
			</div>
			</div>
			</div>
		</div>
		
		<div class="row justify-content-centesr">
			<div class="col-md-8">
			<div id="container-person"></div>
			
			<h4 class="">Transit Camp Details</h4>
			<div id="container-transit-camp"></div>
			
			<h4 class="d-none">Tour Guide Details</h4>
			<div class="d-none" id="container-guide"></div>
			
			
			<h4 class="d-none">Vehicle Details</h4>
			<div class="d-none" id="container-vehicle"></div>
			
			<hr>
			
			<h4 class="">Price Details</h4>
			<div class="row mt-3">
				<div class="col-md-12">
					<div class="card-deck">
					  <div class="card">
					    <div class="card-body text-justify">
					      	<div class="row">
					      	<h5 class="card-title font-weight-bold col">Total Price to pay</h5>
					      	<h5 class="card-title font-weight-bold pull-right text-right col">IDR <span class="currency" id="total-price-pay">0</span><span class="d-none">(<% out.print(purchase.getCountTicket()); %>x)</span></h5>
					    	</div>
					   	</div>
					   	<div class="card-footer">
					   	Akun <strong>(${requestScope.realName})</strong> akan menerima ticket, setelah pembayaran dianggap valid.
					   	</div>
					  </div>
					</div>
					<hr/>
					<!-- <button type="button" onclick="save();" class="btn btn-outline-lights bg-orange btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 btn-blocks mt-3 continue-payment-popover float-right">Continue to Payment</button> -->
				</div>
			</div>
		
			<div class="row mb-3 justify-content-between no-gutters">
				<div class="col-4">
					<button type="button" onclick="$('#btn-book-4').click()" class="btn btn-outline-secondary btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 ">Prev</button>
				</div>
				<div class="col-8 text-right">
					<button type="button" onclick="save();" class="btn btn-outline-lights bg-orange btn-oblong bd-2 pd-x-30 pd-y-15 tx-uppercase tx-bold tx-spacing-6 tx-12 btn-blocks  continue-payment-popover text-truncate">Continue to Payment</button>
				</div>
			</div>
			
			</div>
			
		</div>
		
		</div>
		</div>
		
		<div class="tab-pane fade" id="pills-book-5">
		<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
			<div class="card mb-3">
            <div class="card-body">
              <div class="row align-items-center text-center mb-3">
                <div class="col-sm-6 text-sm-left"><img src="${pageContext.request.contextPath}/images/notfound.png" alt="invoice" width="150"></div>
                <div class="col text-sm-right mt-3 mt-sm-0">
                  <h2 class="mb-3">Invoice</h2>
                  <h5>Falcon Design Studio</h5>
                  <p class="small mb-0">156 University Ave, Toronto<br>On, Canada, M5H 2H7</p>
                </div>
                <div class="col-12">
                  <hr>
                </div>
              </div>
              <div class="row justify-content-between align-items-center">
                <div class="col">
                  <h6 class="text-muted">Invoice to</h6>
                  <h5>Antonio Banderas</h5>
                  <p class="small">1954 Bloor Street West<br>Torronto ON, M6P 3K9<br>Canada</p>
                  <p class="small"><a href="mailto:example@gmail.com">example@gmail.com</a><br><a href="tel:444466667777">+4444-6666-7777</a></p>
                </div>
                <div class="col-sm-auto ml-auto">
                  <div class="table-responsive">
                    <table class="table table-sm table-borderless fs--1">
                      <tbody>
                        <tr>
                          <th class="text-sm-right">Invoice No:</th>
                          <td>14</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Order Number:</th>
                          <td>AD20294</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Invoice Date:</th>
                          <td>2018-09-25</td>
                        </tr>
                        <tr>
                          <th class="text-sm-right">Payment Due:</th>
                          <td>Upon receipt</td>
                        </tr>
                        <tr class="alert-success font-weight-bold">
                          <th class="text-sm-right">Amount Due:</th>
                          <td>$19688.40</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="table-responsive mt-4 fs--1">
                <table class="table table-striped border-bottom">
                  <thead>
                    <tr class="bg-primary text-white">
                      <th class="border-0">Items</th>
                      <th class="border-0 text-center">Quantity</th>
                      <th class="border-0 text-right">Price per item</th>
                      <th class="border-0 text-right">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Ticket</h6>
                        <p class="mb-0">Raja Ampat</p>
                      </td>
                      <td class="align-middle text-center">2</td>
                      <td class="align-middle text-right">$65.00</td>
                      <td class="align-middle text-right">$130.00</td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Vehicle</h6>
                        <p class="mb-0">Fortuner</p>
                      </td>
                      <td class="align-middle text-center">1</td>
                      <td class="align-middle text-right">$2,100.00</td>
                      <td class="align-middle text-right">$2,100.00</td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        <h6 class="mb-0">Tour Guide</h6>
                        <p class="mb-0">Mr.alaex</p>
                      </td>
                      <td class="align-middle text-center">6</td>
                      <td class="align-middle text-right">$2,000.00</td>
                      <td class="align-middle text-right">$12,000.00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="row no-gutters justify-content-end">
                <div class="col-auto">
                  <table class="table table-sm table-borderless fs--1 text-right">
                    <tbody>
                    <!-- <tr>
                      <th class="text-900">Subtotal:</th>
                      <td class="font-weight-semi-bold">$18,230.00 </td>
                    </tr>
                    <tr>
                      <th class="text-900">Tax 8%:</th>
                      <td class="font-weight-semi-bold">$1458.40</td>
                    </tr> -->
                    <tr class="border-top">
                      <th class="text-900">Total:</th>
                      <td class="font-weight-semi-bold">$19688.40</td>
                    </tr>
                    <tr class="border-top border-2x font-weight-bold text-900">
                      <th>Amount Due:</th>
                      <td>$19688.40</td>
                    </tr>
                  </tbody></table>
                </div>
              </div>
            </div>
            <div class="card-footer bg-light">
              <p class="fs--1 mb-0"><strong>Notes: </strong>We really appreciate your business and if there's anything else we can do, please let us know!</p>
            </div>
          </div>
			</div>
		</div>
		</div>
		</div>
		
		</div>
		</c:if>
      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->
	
	<!-- modal -->
	<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="max-width:">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Booking Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body bg-light pt-0">
     		<div id="modal-form-msg" class="alert alert-danger" role="alert"></div>
		    <form id="entry-form" class="mt-3">
		  	<div class="form-row">
				<div class="form-group col-md-12">
					<input type="number" class="form-control" name="count_ticket" placeholder="Number of travelers/visitors">
				</div>
			</div>
		  	<div class="form-row">
				<div class="form-group col-md-6">
					<input type="text" class="form-control single-date-picker" name="start_date" placeholder="Check In">
				</div>
				<div class="form-group col-md-6">
				      <input type="text" class="form-control single-date-picker" name="end_date" placeholder="Check Out">
				</div>
			</div>
			<input name="region_id" type="text" value="${param.id}">
			<!-- <input name="authorization" type="text" value="Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjowMDM6OjMxMTAyMDE0MDAwMDAiLCJwb3NpdGlvbl9uYW1lIjoiRGlyZWt0dXIgT3BlcmFzaW9uYWwiLCJvcmdhbml6YXRpb25fbmFtZSI6IlBULkhJSlIgR0xPQkFMIFNPTFVUSU9OIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbImhpanJfY29yZSJdLCJvcmdhbml6YXRpb25faWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTc4MjE1MTIyLCJqdGkiOiJkM2NjYjA2Ny00YTM5LTQ4NjUtOGY1Ny02YzU4YTNmY2ZkMTAiLCJwb3NpdGlvbl9pZCI6IjAwMyIsImFjY291bnRfYXZhdGFyIjoiaHR0cHM6Ly9jbG91ZC5oaWpyLmNvLmlkL3Nzby9pbWFnZXMvdXNlci5wbmc_MTUyMDgxOTA0MjU0NCJ9.JJNifAbsNIu7hkrpfjfkUOTRtI5iSVi-nqwfjrTzmWqwl32jQ9A9-jbcbSYvqIQH6y9zFa0Tn_iNcro-gtRr-qFGl5evVyqGrZq6AE776CNZ3irhOijNjV3LwRxgp-B3BJY7OdS8nqtW0plhA6IfdYB16wVCn7JCN3XYO6FzcfUzcgkDAp5Fi-mFUEyMSmtFGi7wS-pBN7wHF2ZRIuloAB7_hefeCxRI3hthQnz_VnJ6foADVqw6MSjhWBVVxYBd10_OkADrkP9XsXMCVnHbWHGZZXNVJQAdl-fcdo-ZocIETvCqmEddKQ05y4Vpgt-bPdwY62K4BG1RkdahqUpQKg"> -->
			
			<button type="submit" id="btn-submit">?</button>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" onclick="$('#btn-submit').click();" class="btn btn-success pill pl-3 pr-3"><i class="fa fa-check"></i> Done</button>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal-form-view-visitor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="max-width:">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Form</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body bg-light pt-0">
		    <form class="mt-3">
			  	<div class="form-row">
					<div class="form-group col">
					    <label for="">No. Identity<span class="text-danger">*</span></label>
					    <input type="text" class="form-control" id="no_identity">
					    <!-- onkeyup="_parse(this.value)" -->
					</div>
					<div class="form-group col">
					    <label for="">Full Name<span class="text-danger">*</span></label>
					    <input type="text" class="form-control" id="full_name">
					</div>
			  </div>
			  
			  <div class="form-row">
					<div class="form-group col">
					    <label for="">Type Identity<span class="text-danger">*</span></label>
                      	<select class="form-control" id="type_identity">
                          <option value="">Choose Type Identity</option>
                          <option value="KTP">KTP</option>
                          <option value="PASSPORT">Passport</option>
                        </select>
					</div>
					<div class="form-group col">
					    <label for="">Upload Identity<span class="text-danger">*</span></label>
					    <input type="file" class="form-control" id="file_identity"  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">
					</div>
			  </div>
			  
			  <div class="form-row ">
					<div class="col-md-6">
			    		<label for="">Birth Date<span class="text-danger">*</span></label>
					<div class="form-group">
					      <input type="date" class="form-control" id="birthdate" placeholder="">
					</div>
					</div>
					<div class="col-md-6">
			    		<label for="">Gender<span class="text-danger">*</span></label>
					<div class="form-group ">
						<select class="form-control" id="gender">
							<option value="">-</option>
							<option value="L">LAKI-LAKI</option>
							<option value="P">PEREMPUAN</option>
						</select>
					</div>
					</div>
			  </div>
			  
			  <div class="form-row">
					<div class="col-md-6">
			    		<label for="">Mobile Number<span class="text-danger">*</span></label>
					</div>
					<div class="col-md-6">
			    		<label for="">Email<span class="text-danger">*</span></label>
					</div>
			  </div>
			  <div class="form-row">
					<div class="form-group col-md-2">
						<select class="form-control" id="phone_code">
							<option value="+62">+62</option>
						</select>
					</div>
					<div class="form-group col-md-4">
					      <input type="text" class="form-control" id="phone_number" autocomplete="false" placeholder="">
					</div>
					<div class="form-group col-md-6">
					      <input type="text" class="form-control" id="email" placeholder="">
					</div>
			  </div>
			  
			  <div class="form-row">
					<div class="form-group col-md-12">
			    		<label for="">Alamat<span class="text-danger">*</span></label>
						<textarea class="form-control" id="address" spellcheck="false"></textarea>
					</div>
			  </div>
			  
			  <div class="form-row">
					<div class="col-md-12">
			    		<label class="font-weight-bold">Information of family who can be contacted<span class="text-danger">*</span></label>
					</div>
			  </div>
			  <div class="form-row">
					<div class="form-group col-md-4">
					      <input type="text" class="form-control" id="phone_number_family" placeholder="Phone number of family">
					</div>
					<div class="form-group col-md-8">
					      <input type="text" class="form-control" id="address_family" placeholder="Entry Address of family">
					</div>
			  </div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" onclick="$('#btn-submit').click();" class="btn btn-success pill pl-3 pr-3"><i class="fa fa-check"></i> OK</button>
	      </div>
	    </div>
	  </div>
	</div>
	
    
    <c:set var = "jquery" value = "${true}"/>
    <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
   
    <script>
    	/* document.getElementsByTagName('button').disabled=true;
		for(var i = 0; i < document.getElementsByTagName('input').length; i++) {
	    	document.getElementsByTagName('input')[i].disabled=true;
		}*/
		$.LoadingOverlay("show", { textResizeFactor:0.3, text: "Mohon tunggu, halaman Anda sedang dipersiapkan.", image : ctx + "/images/loading-spinner.gif" });
		 
    </script>
    <%@ include file = "inc/footer.jsp" %>
   <script src="${pageContext.request.contextPath}/js/new-booking.js"></script>
    

	<% if(purchase.getDisclaimer()==null) { %>
		<script>$("#disclaimer-peraturan-modal").modal('show');</script>
	<% } %>
	
	<script>
	$("#form-contact-detil").find('input, select, textarea').attr('readonly', 'readonly');
	$("#form-travel-detil").find('input, select, textarea').attr('readonly', 'readonly');
	var data = {values: ${kphan}};
	
	$(function () {
	  	$('[data-toggle="popover"]').popover();
	  
	  	$('.single-date-picker').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD-MM-YYYY'));
			$(this).val(picker.endDate.format('DD-MM-YYYY'));
			picker.autoUpdateInput = true;
			$(this).change();
			
		});
		$('.single-date-picker').on('cancel.daterangepicker', function(ev, picker) {
		    $(this).val('');
		});
	})
	
	function checkFormCharge(){
		var pass=false;
		if($('[name=no_identity]')[0].value=="" || $('[name=full_name]')[0].value==""
			|| $('[name=email]')[0].value=="" || $('[name=phone_number]')[0].value==""
				|| $('[name=birthdate]')[0].value=="" || $('[name=type_identity]')[0].value==""
					|| $('[name=gender]')[0].value=="" || $('[name=address]')[0].value==""
						){
			pass=true
			
		}else if($('[name=file_identity]')[0].disabled==false) pass=false;
		return pass;
	}
	
	$('#checkAsLeader').click(function(){
		var form = document.forms[0],
		form = $("[data-key=1] :input"),
		form1 = $("[data-key=0] :input");
        if($(this).prop("checked") == true){
            if(checkFormCharge()) {
                alert("form belum lengkap.");
                $(this).prop("checked", false);
            }else{
	        	$('[name=no_identity]')[1].value=$('[name=no_identity]')[0].value;
	    		$('[name=full_name]')[1].value=$('[name=full_name]')[0].value;
	    		$('[name=email]')[1].value=$('[name=email]')[0].value;
	    		$('[name=phone_number]')[1].value=$('[name=phone_number]')[0].value;
	    		$('[name=birthdate]')[1].value=$('[name=birthdate]')[0].value;
	    		
	    		$('[name=type_identity]')[1].value=$('[name=type_identity]')[0].value;
				$('[name=type_identity] option[value=""]')[1].setAttribute("disabled",true);
				if($('[name=type_identity]')[0].value=="NIK"){
	    			$('[name=type_identity] option[value="PASSPORT"]')[1].setAttribute("disabled",true);
	    			$('[name=type_identity] option[value="NIK"]')[1].setAttribute("selected",true);
	    			$('[name=type_identity] option[value="NIK"]')[1].removeAttribute("disabled");
	    		}else if($('[name=type_identity]')[0].value=="PASSPORT"){
	    			$('[name=type_identity] option[value="NIK"]')[1].setAttribute("disabled",true);
	    			$('[name=type_identity] option[value="PASSPORT"]')[1].setAttribute("selected",true);
	    			$('[name=type_identity] option[value="PASSPORT"]')[1].removeAttribute("disabled");
	    		}
				$($('[name=type_identity]')[1]).trigger('change');
	    		$('[name=gender]')[1].value=$('[name=gender]')[0].value;
				$('[name=gender] option[value=""]')[1].setAttribute("disabled",true);
				if($('[name=gender]')[0].value=="L"){
	    			$('[name=gender] option[value="P"]')[1].setAttribute("disabled",true);
	    			$('[name=gender] option[value="L"]')[1].setAttribute("selected",true);
	    			$('[name=gender] option[value="L"]')[1].removeAttribute("disabled");
	    		}else if($('[name=gender]')[0].value=="P"){
	    			$('[name=gender] option[value="L"]')[1].setAttribute("disabled",true);
	    			$('[name=gender] option[value="P"]')[1].setAttribute("selected",true);
	    			$('[name=gender] option[value="P"]')[1].removeAttribute("disabled");
	    		}
	
				$("#form-file-1").html($('[name=file_identity]')[0].cloneNode(true));
				
				$('[name=file_identity]')[1].setAttribute("style","pointer-events: none;background-color: #e9ecef;");
				/* $('[name=file_identity]')[1].files=$('[name=file_identity]')[0].files;
				if($('[name=file_identity]')[0].disabled) {
					$('[name=file_identity]')[1].disabled= true;
				}else {
					$('[name=file_identity]')[1].disabled= false;
				} */
				
	    		$('[name=address]')[1].value=$('[name=address]')[0].value;
	
				//f lockForm()
				[].slice.call( form ).forEach(function(item){
				    item.readOnly = !item.readOnly;
				});
				[].slice.call( form1 ).forEach(function(item){
				    item.readOnly = !item.readOnly;
				});
            }
			
    	}else if($(this).prop("checked") == false){
        	$('[name=no_identity]')[1].value="";
    		$('[name=full_name]')[1].value="";
    		$('[name=phone_number]')[1].value="";
    		$('[name=email]')[1].value="";
    		$('[name=type_identity]')[1].value="";
    		$('[name=birthdate]')[1].value="";
    		$('[name=gender]')[1].value="";
    		$('[name=file_identity]')[1].value="";
    		$('[name=address]')[1].value="";
    		
    		$('[name=file_identity]')[1].disabled= false;
			$('[name=file_identity]')[1].setAttribute("style","");
			$("#form-file-1").html('<input type="file" class="form-control" name="file_identity"  accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps">');
			
    		//f unlockForm()
			[].slice.call( form ).forEach(function(item){
			    item.readOnly = !item.readOnly;
			});
			[].slice.call( form1 ).forEach(function(item){
			    item.readOnly = !item.readOnly;
			});
			//else{
   			$('[name=type_identity] option[value=""]')[1].removeAttribute("disabled");
   			$('[name=type_identity] option[value="PASSPORT"]')[1].removeAttribute("disabled");
   			$('[name=type_identity] option[value="NIK"]')[1].removeAttribute("disabled");
   			$('[name=gender] option[value=""]')[1].removeAttribute("disabled");
   			$('[name=gender] option[value="L"]')[1].removeAttribute("disabled");
   			$('[name=gender] option[value="P"]')[1].removeAttribute("disabled");
        	//}
			
        }
    });
		
	function hide_t(){
		$(".example-popover").popover("hide");
	}

	function setdefaultvalue(){
		var identity=["1111111","1111111", "2222222", "3333333", "4444444"];
		var namefull=["satu","satu", "dua", "tiga", "empat"];
		for(var i=0; i < $('[name=no_identity]').length;i++){
			$('[name=no_identity]')[i].value=identity[i];
			$('[name=full_name]')[i].value=namefull[i];
			$('[name=phone_number]')[i].value="82"+identity[i];
			$('[name=email]')[i].value=namefull[i]+"@email.com";
			$('[name=birthdate]')[i].value="199"+identity[i].substr(identity[i].length -1)+"-0"+identity[i].substr(identity[i].length -1)+"-02";
			$('[name=address]')[i].value="jl."+namefull[i];
		}
		$('[name=type_identity]').val("KTP");
		$('[name=gender]').val("L");
		$('[name=phone_number_family]').val("82119289393");
		$('[name=address_family]').val("satu@email.com");
		
	}

	function setActiveRow(e, attr){
		if(attr==undefined){
			$('.row-rtc').removeClass("bg-primary");
			$('#row-rtc-'+e.dataset.id).addClass("bg-primary");
		}else{
			$('.'+attr).removeClass("bg-primary");
			$('#'+attr+'-'+e.dataset.id).addClass("bg-primary");
		}
	}

	function addNewLuggage(){
		Swal.fire({
		  input: 'textarea',
		  inputLabel: 'Tambahkan',
		  inputPlaceholder: 'Tambahkan barang lainnya...',
		  inputAttributes: {
		    'aria-label': 'Type your message here'
		  },
		  showCancelButton: true
		}).then((result) => {
	  		if (result.isConfirmed) {
		  		console.log(result);
		  		
			    Swal.fire(
			      'Berhasil!',
			      'Data berhasil ditambahkan.',
			      'success'
			    )
			    //
			    var num = $('#container-luggage').find(':input[type="number"]').length;
				var myvar = '<div class="form-check" data-luggage="'+(num)+'">'+
				'   <input required="" name="name_luggage" class="form-check-input" type="checkbox" value="'+result.value+'" id="'+(num)+'">'+ //id="defaultCheck8"
				'   <input required="" min="0" name="qty_luggage" type="number" placeholder="QTY" style="width: 50px;">'+
				'   <input name="type_luggage" type="hidden" value="LAINNNYA">'+
				'   <input name="desc_luggage" type="hidden" value="">'+
				'   <label class="form-check-label" for="'+(num)+'">'+result.value+'</label>'+
				'</div>';

				$('#container-luggage').append(myvar);;

			    //
		  	}
		})
	}

	//hapus karena fungsi hanya dibutuhkan setelah create form
	/*$('[name=type_identity]').on('change', function(){
		var price=0;
		if(this.value=="NIK"){
			price=getPriceBooking('WNI')//this.dataset.price_wni;
		}else if(this.value=="PASSPORT"){
			price=getPriceBooking('WNA')//this.dataset.price_wna;
		}
		var key = this.parentElement.parentElement.parentElement.parentElement.dataset.key;
		console.log(key);
		$("#total-price-pay-"+key).text($.number(price,0));
	});*/
	
	</script>
	<script>
	//When the user scrolls the page, execute myFunction
	window.onscroll = function() {myFunction()};
	
	// Get the navbar
	var cardRD = $("#card-region-detil");
	var cardPD = $("#card-price-detil");
	var cardDef = $(".card-detil");
	
	// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
	function myFunction() {
		//console.log(window.pageYOffset);
		//alert(window.pageYOffset);
		if (window.pageYOffset >= 17) {
			cardDef.addClass("sticky-detil");
			//cardRD.addClass("sticky-region-detil");
			//cardPD.addClass("sticky-price-detil");
		} else {
			cardDef.removeClass("sticky-detil");
			//cardRD.removeClass("sticky-region-detil");
			//cardPD.removeClass("sticky-price-detil");
		}
	}
	$("#btn-tidak-mengerti").click(function(){
		window.location.href="./";
	});
	$("#btn-mengerti").click(function(){
		var obj = new FormData();
	    $.LoadingOverlay("show", { image : ctx + "/images/loading-spinner.gif" });
		var path = ctx + '/ticket/purchase';
		ajaxPOST(path + '/'+selected_id+'/'+'DISCLAIMER', obj, 'onDisclaimer','onActionError');
	});

	function onDisclaimer(response){
		alert(response.message);
		$('#disclaimer-peraturan-modal').modal('hide');
		$.LoadingOverlay("hide");
	}


	function getPriceBooking(mode){
		var new_price=0;
		var start = moment(moment($("#start_date").text()).format('DD-MM-YYYY'), 'DD/MM/YYYY');//.subtract(1, 'd')._d;
		var end = moment(moment($("#end_date").text()).format('DD-MM-YYYY'), 'DD/MM/YYYY');//.subtract(1, 'd')._d;
		var loop = new Date(start);

		while(loop <= end){
			
			if(is_weekend(moment(loop,'DD/MM/YYYY').format("MMM DD YYYY"))){

				if(mode=="WNI") new_price+=parseInt($('#total-visitor').data('price_wni')) + parseInt(weekend_price);
				else if(mode=="WNA") new_price+=parseInt($('#total-visitor').data('price_wna')) +parseInt(weekend_wna_price);
				
			}else{
				//cek agenda 
				var readDays=true;
				$.each(list_date, function(){
					if(loop.getTime()==this.getTime()){
						readDays=false;
						if(mode=="WNI") new_price+=parseInt($('#total-visitor').data('price_wni')) + parseInt(weekend_price);
						else if(mode=="WNA") new_price+=parseInt($('#total-visitor').data('price_wna')) +parseInt(weekend_wna_price);
					}
				});
				
				if(readDays) {
					if(mode=="WNI") new_price+=parseInt($('#total-visitor').data('price_wni'));
					else if(mode=="WNA") new_price+=parseInt($('#total-visitor').data('price_wna'));
				}
			}
		   	var newDate = loop.setDate(loop.getDate() + 1);
		   	loop = new Date(newDate);
		}
		
		return new_price;
	}
	</script>
  </body>

</html>
