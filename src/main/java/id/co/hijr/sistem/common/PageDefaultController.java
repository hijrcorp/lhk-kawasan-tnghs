package id.co.hijr.sistem.common;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.hijr.sistem.ref.AccountLoginInfo;


@Configuration
@Controller
public class PageDefaultController extends BaseController {
	
	@RequestMapping("/page/{jsp}")
    public String loadPage(Model model, Locale locale, String dir, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{

		if((appState.equals("development"))){
			model.addAttribute("contextPathPublic", "/kawasan");
		}else {
			model.addAttribute("contextPathPublic", "/public");
		}

		TimeZone obj = TimeZone.getDefault();
	    System.out.println("Default timezone object: \n" + obj);
	    System.out.println("Default timezone object: \n" + new Date());
		
		Device deviceFromRequest = DeviceUtils.getCurrentDevice(request);
		System.out.println(deviceFromRequest);
		Device device=null;
			String accessTokenValue = getCookieTokenValue(request,cookieName);
			boolean pass = isTokenValid(accessTokenValue);
			
			if((jsp.equals("general") || jsp.equals("booking") || jsp.equals("manage-user") || jsp.equals("daftar")) && pass) {
				HttpStatus statusHTTP = HttpStatus.OK;
		        RestTemplate restTemplate = new RestTemplate();
		        ObjectMapper mapper = new ObjectMapper();
				JsonNode root = null;
				
		        String workingURL = request.getRequestURL().toString().replaceAll("page/"+jsp,"");
		        System.out.println(">>"+appState);
		        if((appSsl.equals("yes"))){
		        	workingURL = workingURL.replaceAll("http", "https");
				}
				String urlNotification = workingURL + "restql/multi/entity?entity=configure;agenda&limit=agenda->100000&filter=configure->name.in('WEEKEND_PRICE','WHATSAPP','BOOKING_NOT_ALLOWED','DISCLAIMER_SOP')";
				System.out.println("url: " + urlNotification);
		        
				// create http headers and add authorization header we just created
		        HttpHeaders headers = new HttpHeaders();
		        headers.setContentType(MediaType.APPLICATION_JSON);
				headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + accessTokenValue);
				//set headers
				HttpEntity<String> requestHeader = new HttpEntity<String>(headers);
				//call the api
				ResponseEntity<String> responseTemp = restTemplate.exchange(urlNotification, HttpMethod.GET, requestHeader, String.class);
				System.out.println("hasil api: " + responseTemp);
				
				statusHTTP = responseTemp.getStatusCode();
				
				if(statusHTTP.equals(HttpStatus.OK)) {
					root = mapper.readTree(responseTemp.getBody()!=null?responseTemp.getBody():"");
					JsonNode dataConfigure = root.path("data").get("configure");
					JsonNode dataAgenda = root.path("data").get("agenda");;

				 	for(JsonNode jn : dataConfigure) {
				 		if(jn.path("name").textValue().equals("DISCLAIMER_SOP")) model.addAttribute("disclaimerSop", jn.path("value").textValue());
				 		if(jn.path("name").textValue().equals("WEEKEND_PRICE")) model.addAttribute("weekendPrice", jn.path("value").textValue());
				 		if(jn.path("name").textValue().equals("WHATSAPP")) model.addAttribute("whatsapp", jn.path("value").textValue());
				 		if(jn.path("name").textValue().equals("DISABLED_BOOKING")) {
				 			if(jn.path("value").textValue().equals("TRUE")) model.addAttribute("bookingNotAllowed", "Mohon Maaf saat ini Aplikasi Booking TNGHS sedang offline dalam menerima order tiket.");
				 		}
				 	}
				 	model.addAttribute("dataAgenda", dataAgenda);
				 	
				}else if(statusHTTP.equals(HttpStatus.MOVED_PERMANENTLY)){
					model.addAttribute("pageNotComplete", "301 Moved Permanently");
				}else {
					model.addAttribute("pageNotComplete", "Mohon Maaf proses transfer data pada aplikasi kurang lengkap, seperti jaringan anda sedang tidak stabil, mohon lakukan refresh ulang halaman Anda hingga pesan ini tidak tampil kembali.");
				}
			}
			//model.addAttribute("pageNotComplete", "Mohon Maaf proses transfer data pada aplikasi kurang lengkap, seperti jaringan anda sedang tidak stabil, mohon lakukan refresh ulang halaman Anda hingga pesan ini tidak tampil kembali.");
			model.addAttribute("nameJSP", jsp);
			
			if(!pass && jsp.equals("general")) {
				return "pages/"+jsp;
			}else if(!pass && jsp.equals("mapas")) {
				return "pages/"+jsp;
			}if(!pass && jsp.equals("index")) {
				return "pages/"+jsp;
			}else if(!pass && jsp.equals("general-profile")) {
				return "pages/"+jsp;
			}else if(!pass && jsp.equals("booking")) {
				return "pages/"+jsp;
			}else if(!pass && jsp.equals("daftar")) {
				return "pages/"+jsp;
			}else if(!pass && jsp.equals("payment")) {
				return "pages/"+jsp;
			}else if(!pass && jsp.equals("data-pendaki")) {
				return "pages/"+jsp;
			}else if(!pass) {
				String requestPage = request.getRequestURL().toString();
		    		if(request.getQueryString() != null) {
		    			requestPage += "?"+request.getQueryString();
		    		}
		    		
		    		String resultPath = getConstructURL(request, "login-perform?page="+requestPage); 
		    		
		    		String params = "response_type=code";
				params += "&client_id=" + clientId;
				params += "&redirect_uri=" + resultPath + "";
				System.out.println(getWorkingUrl() + "/oauth/authorize?" + params);
				response.sendRedirect(getWorkingUrl() + "/oauth/authorize?" + params);
			}else {
	
				DecodedJWT jwt = JWT.decode(accessTokenValue);
			
				//start	
					//System.out.println("hihi dir: " + dir + " | jsp: " + jsp);
					//System.out.println("request URL: " + request.getRequestURL());
					addModelTokenInfo(request, model, device, locale, accessTokenValue);
					//System.out.println("device: " + device);
		
					System.out.println("inikah " +jsp);
					System.out.println("mungkin "+ jwt.getClaim(Token.AUTHORITIES).asList(String.class).contains("ROLE_SUPER_ADMIN"));
					
				//end
				
				System.out.println("since: "+(request.getRequestURL().substring(request.getRequestURL().length() - 1).equals("/") && !jsp.equals("index")));
				
				if(request.getRequestURL().substring(request.getRequestURL().length() - 1).equals("/") && !jsp.equals("index")) {
					model.addAttribute("assetPath", request.getContextPath() + "/" + jsp);
					return "pages/" + jsp + "/index";
				} else {
	//				if(jwt.getClaim(Token.AUTHORITIES).asList(String.class).contains("ROLE_SUPER_ADMIN")) {
	//					dir="admin";
	//					jsp="kphan";
	//				}
					if(dir != null) {
						System.out.println("really: "+"pages/" + dir + "/" + jsp);
						model.addAttribute("assetPath", request.getContextPath() + "/" + dir);
						return "pages/" + dir + "/" + jsp;
					}else {
						model.addAttribute("assetPath", request.getContextPath());
						return "pages/" + jsp;	
					}
				}
			}
			
	        return "error";

    }
    @RequestMapping("/page/{dir}/{jsp}")
    public String loadPageWithFolder(Model model, Locale locale, @PathVariable String dir, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{
		//System.out.println("ini ada di dir: " + dir + " | jsp: " + jsp);
    	return loadPage(model, locale, dir, jsp, request, response);
    }
    
    @RequestMapping(value={"/page", "/page/"})
    public String loadMainNoSlashAndSlash(Model model, Locale locale, HttpServletRequest request,HttpServletResponse response) throws Exception{
		String jsp = "index"; // default page, please check
		return loadPage(model, locale, null, jsp, request, response);
    }
    
}


