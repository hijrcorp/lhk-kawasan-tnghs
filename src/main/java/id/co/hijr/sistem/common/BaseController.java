package id.co.hijr.sistem.common;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import id.co.hijr.sistem.helper.BasicAuthRestTemplate;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.ticket.model.VisitorIdentity;

@Controller
public class BaseController {

	@Autowired
    private MessageSource messageSource;

	@Value("${security.oauth2.client.id}")
	protected String clientId;
	
	@Value("${security.oauth2.client.secret}")
	protected String clientSecret;

	@Value("${app.state}")
	protected String appState;
	
	@Value("${app.ssl}")
	protected String appSsl;

	@Value("${app.newurl.production.sso}")
	protected String ssoEndpointUrlProd;
	
	@Value("${app.newurl.development.sso}")
	protected String ssoEndpointUrlDev;
	
	@Value("${app.cookie.name}")
	protected String cookieName;
	
	@Value("${locale.cookie.name}")
	protected String localeCookieName;
	
	protected String getWorkingUrl() {
		return (appState.equals("development")?ssoEndpointUrlDev:ssoEndpointUrlProd);
	}
	
	protected String extractAccountLogin(HttpServletRequest request, AccountLoginInfo type) {
		String[] info = request.getRemoteUser().split("::");
		if(info.length == 3) {
			return info[type.getId()];
		}
		
		return request.getRemoteUser();
	}
	
	protected String extractHeaderToken(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaders("Authorization");
		while (headers.hasMoreElements()) { // typically there is only one (most servers enforce that)
			String value = headers.nextElement();
			if ((value.toLowerCase().startsWith(OAuth2AccessToken.BEARER_TYPE.toLowerCase()))) {
				String authHeaderValue = value.substring(OAuth2AccessToken.BEARER_TYPE.length()).trim();
				// Add this here for the auth details later. Would be better to change the signature of this method.
				request.setAttribute(OAuth2AuthenticationDetails.ACCESS_TOKEN_TYPE,
						value.substring(0, OAuth2AccessToken.BEARER_TYPE.length()).trim());
				int commaIndex = authHeaderValue.indexOf(',');
				if (commaIndex > 0) {
					authHeaderValue = authHeaderValue.substring(0, commaIndex);
				}
				return authHeaderValue;
			}
		}

		return "";
	}
	
	protected String getCookieTokenValue(HttpServletRequest request, String cookieName) {
		Cookie[] cookies = request.getCookies();

		String accessTokenValue = "";
		if(cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
			  String name = cookies[i].getName();
			  String value = cookies[i].getValue();
			  //System.out.println(name + "--" + value);
			  if(name.equals(cookieName)){
				  accessTokenValue= value;
			  }
			}
		}
		
		return accessTokenValue;
	}
	
	 protected void setCookie(HttpServletRequest request,HttpServletResponse response, Token token) {
 		
 		Cookie accessTokenCookie = new Cookie(cookieName, token.getAccessToken());
 		accessTokenCookie.setMaxAge(token.getExpiresIn().intValue()); 
		accessTokenCookie.setPath("/");
		System.out.println("host: " + request.getServerName());
		if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
			accessTokenCookie.setDomain(request.getServerName());
		}else {
			accessTokenCookie.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
		}
		
		response.addCookie(accessTokenCookie);
		
		Cookie localInfoCookie = new Cookie(localeCookieName, "id_ID");
		localInfoCookie.setMaxAge(token.getExpiresIn().intValue()); 
		localInfoCookie.setPath("/");
		if(Utils.ip(request.getServerName()) || request.getServerName().equals("localhost")) {
			localInfoCookie.setDomain(request.getServerName());
		}else {
			localInfoCookie.setDomain("." + request.getServerName().replaceAll(".*\\.(?=.*\\..*\\.)", ""));
		}
		
		response.addCookie(localInfoCookie);
 }
 

 
 
 	protected boolean isTokenValid(String accessTokenValue) {
 		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
		ResponseEntity<String> result = null;
		String resp = "";
		boolean pass = true;
		if(accessTokenValue.equals("")) return false;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("token", accessTokenValue);

			result = restTemplate.postForEntity( getWorkingUrl() + "/oauth/check_token", map , String.class );
//			Token token = result.getBody();
//			System.out.println(token.getAccessToken());
			
			resp = result.getBody();
		} catch (HttpClientErrorException e) {
			resp = e.getResponseBodyAsString();
			pass = false;
		 } catch (Exception e) {
		// TODO: handle exception
			 pass = false;
		 }
		return pass;
 	}
 	
 	protected boolean isTokenValid(HttpServletRequest request) {
 		String accessTokenValue = getCookieTokenValue(request, cookieName);
 		BasicAuthRestTemplate restTemplate = new BasicAuthRestTemplate(clientId, clientSecret);
		ResponseEntity<String> result = null;
		String resp = "";
		boolean pass = true;
		if(accessTokenValue.equals("")) return false;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("token", accessTokenValue);

			result = restTemplate.postForEntity( getWorkingUrl() + "/oauth/check_token", map , String.class );
//			Token token = result.getBody();
//			System.out.println(token.getAccessToken());
			
			resp = result.getBody();
		} catch (HttpClientErrorException e) {
			resp = e.getResponseBodyAsString();
			pass = false;
		 } catch (Exception e) {
		// TODO: handle exception
			 pass = false;
		 }
		return pass;
 	}
 	
 	protected Model addModelTokenInfo(HttpServletRequest request, Model model, Device device, Locale locale, String accessTokenValue) {
 		
 		String deviceType = "Desktop";
 		if(device!=null) {
			if (device.isMobile()) deviceType = "Mobile";
			else if (device.isTablet()) deviceType = "Tablet";
 		}
		model.addAttribute("device", deviceType);
		model.addAttribute("deviceType", deviceType);
 		
 		if(!accessTokenValue.equals("")) {
			try {
			    DecodedJWT jwt = JWT.decode(accessTokenValue);
			    
    			System.out.println(locale.toString());
    			System.out.println("first_name: "+messageSource.getMessage("account.kolom.label.firstname", null, locale));
    			
				model.addAttribute("username",jwt.getClaim(Token.ACCOUNT_ID).asString());
				model.addAttribute("realName",jwt.getClaim(Token.FULL_NAME).asString());
				model.addAttribute("cookieName", this.cookieName);
				model.addAttribute("localeCookieName", this.localeCookieName);
				model.addAttribute("locale", locale.toString());
				model.addAttribute("position", jwt.getClaim(Token.POSITION_NAME).asString());
				model.addAttribute("organization", jwt.getClaim(Token.ORGANIZATION_NAME).asString());
				model.addAttribute("roleList", jwt.getClaim(Token.AUTHORITIES).asList(String.class));
				model.addAttribute("organization", jwt.getClaim(Token.ORGANIZATION_NAME).asString());
				model.addAttribute("avatar", jwt.getClaim(Token.ACCOUNT_AVATAR).asString());
			} catch (JWTDecodeException exception){
			    //Invalid token
				exception.printStackTrace();
			}
			
			
		}
 		return model;
 	}
 	
 	protected String getConstructURL(HttpServletRequest request, String param) {
 		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int serverPort = request.getServerPort();
		String contextPath = request.getContextPath();  // includes leading forward slash
		
		
		String url = scheme + "://" + serverName + ":" + serverPort + contextPath + "/";
		if(serverPort == 80 || serverPort == 443) {
			url = scheme + "://" + serverName + contextPath + "/";
		}
		url += param;
		
 		return url;
 	}
 	
 	protected String getIdOauthUser(HttpServletRequest request) {
 		String accessTokenValue = getCookieTokenValue(request, cookieName);
 		if(!accessTokenValue.equals("")) {
			try {
				DecodedJWT jwt = JWT.decode(accessTokenValue);
				return jwt.getClaim(Token.ACCOUNT_ID).asString();
			} catch (JWTDecodeException exception){
				exception.printStackTrace();
			}
 		}
 		return null;
 	}
 

}
