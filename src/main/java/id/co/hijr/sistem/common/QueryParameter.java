package id.co.hijr.sistem.common;

public class QueryParameter {

	private String locale = "en_US";
	private String clause = "1";
	private String innerClause = "1";
	private String innerClause2 = "1";
	private String innerClause3 = "1";
	private String innerClause4 = "1";
	private String innerClause5 = "1";
	private String values = "";
	private String order = "";
	private String group = "";
	private Integer limit = 100;
	private Integer offset = 0;

	public QueryParameter() {
		// TODO Auto-generated constructor stub
	}

	public String getClause() {
		return clause;
	}

	public void setClause(String clause) {
		this.clause = clause;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Integer getLimit() {
		//if(this.limit > 200) return 200;
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getInnerClause() {
		return innerClause;
	}

	public void setInnerClause(String innerClause) {
		this.innerClause = innerClause;
	}
	
	public String getInnerClause2() {
		return innerClause2;
	}

	public void setInnerClause2(String innerClause2) {
		this.innerClause2 = innerClause2;
	}

	public String getInnerClause3() {
		return innerClause3;
	}

	public void setInnerClause3(String innerClause3) {
		this.innerClause3 = innerClause3;
	}

	public String getInnerClause4() {
		return innerClause4;
	}

	public void setInnerClause4(String innerClause4) {
		this.innerClause4 = innerClause4;
	}

	public String getInnerClause5() {
		return innerClause5;
	}

	public void setInnerClause5(String innerClause5) {
		this.innerClause5 = innerClause5;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

}
