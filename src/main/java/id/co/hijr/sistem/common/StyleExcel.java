package id.co.hijr.sistem.common;

import java.awt.Color;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class StyleExcel {

	// start dynamic style
	public XSSFCellStyle styleAlign(XSSFWorkbook wb, String fn) {
		XSSFCellStyle style = wb.createCellStyle();
		if(fn.equals("center")) {
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		}else if(fn.equals("verticalTop")) {
			style.setAlignment(HorizontalAlignment.LEFT);
			style.setWrapText(true);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		}else if(fn.equals("wrap")) {
			style.setWrapText(true);
		}else if(fn.equals("title")) {
			style.setWrapText(true);
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		}
		return style;
	}
	public XSSFCellStyle fontStyle(XSSFWorkbook wb, String fn) {
		XSSFCellStyle styleHead=wb.createCellStyle();
        XSSFFont font = wb.createFont();
        font.setFontHeightInPoints((short) 12);
        if(fn.equals("bold")) {
        	font.setBold(true);
        }
        styleHead.setFont(font);
        return styleHead;
	}
	public XSSFCellStyle styleHeader(XSSFWorkbook workbook,String fn) {
		XSSFCellStyle styleSection=workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font = workbook.createFont();
        font.setBold(true);
        styleSection.setFont(font);
        styleSection.setFillForegroundColor(new XSSFColor(new java.awt.Color(196, 215, 155)));
        styleSection.setFillPattern(FillPatternType.SOLID_FOREGROUND);    
        styleSection.setAlignment(HorizontalAlignment.CENTER);
        styleSection.setBorderBottom(BorderStyle.THIN);
        styleSection.setBorderTop(BorderStyle.THIN);
        styleSection.setBorderRight(BorderStyle.THIN);
        styleSection.setBorderLeft(BorderStyle.THIN);
        styleSection.setVerticalAlignment(VerticalAlignment.CENTER);
        if(fn == "wrap")styleSection.setWrapText(true);
        return styleSection;
	}
	public XSSFCellStyle styleHeader(XSSFWorkbook workbook) {
		XSSFCellStyle styleSection=workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font = workbook.createFont();
		font.setBold(true);
        styleSection.setFont(font);
        styleSection.setFillForegroundColor(new XSSFColor(new java.awt.Color(196, 215, 155)));
        styleSection.setFillPattern(FillPatternType.SOLID_FOREGROUND);    
        styleSection.setAlignment(HorizontalAlignment.CENTER);
        styleSection.setBorderBottom(BorderStyle.THIN);
        styleSection.setBorderTop(BorderStyle.THIN);
        styleSection.setBorderRight(BorderStyle.THIN);
        styleSection.setBorderLeft(BorderStyle.THIN);
        styleSection.setVerticalAlignment(VerticalAlignment.CENTER);
        return styleSection;
	}
	public CellStyle styleHeader(SXSSFWorkbook workbook) {
		CellStyle styleSection=workbook.createCellStyle();
		Font font = workbook.createFont();
		font = workbook.createFont();
		font.setBold(true);
        styleSection.setFont(font);
        //styleSection.setFillForegroundColor(new XSSFColor(new java.awt.Color(196, 215, 155)));
        styleSection.setFillPattern(FillPatternType.SOLID_FOREGROUND);    
        styleSection.setAlignment(HorizontalAlignment.CENTER);
        styleSection.setBorderBottom(BorderStyle.THIN);
        styleSection.setBorderTop(BorderStyle.THIN);
        styleSection.setBorderRight(BorderStyle.THIN);
        styleSection.setBorderLeft(BorderStyle.THIN);
        styleSection.setVerticalAlignment(VerticalAlignment.CENTER);
        return styleSection;
	}
	public XSSFCellStyle styleBody(XSSFWorkbook workbook,Map<String, Boolean> fn) {
		XSSFCellStyle style=workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
        XSSFDataFormat format = workbook.createDataFormat();
		
        try {
        	style.setAlignment(HorizontalAlignment.LEFT);
    		style.setVerticalAlignment(VerticalAlignment.TOP);
    		if(fn.get("header") != null) {
    			font = workbook.createFont();
    			font.setBold(true);
    			style.setFont(font);
    			style.setFillForegroundColor(new XSSFColor(new java.awt.Color(196, 215, 155)));
    			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);    
    			style.setAlignment(HorizontalAlignment.CENTER);
    			style.setBorderBottom(BorderStyle.THIN);
    	        style.setBorderTop(BorderStyle.THIN);
    	        style.setBorderRight(BorderStyle.THIN);
    	        style.setBorderLeft(BorderStyle.THIN);
    	        style.setVerticalAlignment(VerticalAlignment.CENTER);
    		}
            if(fn.get("center") != null) {
            	style.setAlignment(HorizontalAlignment.CENTER);
        		style.setVerticalAlignment(VerticalAlignment.CENTER);
            }
            if(fn.get("top-center") != null) {
            	style.setAlignment(HorizontalAlignment.CENTER);
        		style.setVerticalAlignment(VerticalAlignment.TOP);
            }
            if(fn.get("wrap") != null)style.setWrapText(true);
            
            if(fn.get("bold") != null) {
            	font.setBold(true);
            }
            if(fn.get("title") != null) {
                font.setFontHeightInPoints((short) 14);
            }
            if(fn.get("green") != null) {
    			font = workbook.createFont();
    			font.setColor(new XSSFColor(new Color(0,200,100)));
    			style.setFont(font);
            }
            if(fn.get("red") != null) {
    			font = workbook.createFont();
    			font.setColor(new XSSFColor(new Color(255,0,0)));
    			style.setFont(font);
            }
            if(fn.get("orange") != null) {
    			font = workbook.createFont();
    			font.setColor(new XSSFColor(new Color(255,165,0)));
    			style.setFont(font);
            }
            if(fn.get("curr") != null){

            	style.setAlignment(HorizontalAlignment.RIGHT);
        		style.setVerticalAlignment(VerticalAlignment.CENTER);
            	style.setDataFormat(format.getFormat("#,###,###,###,###.00"));
            }
            style.setFont(font);
            if(fn.get("border") == null) {
	    		style.setBorderBottom(BorderStyle.THIN);
	    		style.setBorderTop(BorderStyle.THIN);
	    		style.setBorderRight(BorderStyle.THIN);
	    		style.setBorderLeft(BorderStyle.THIN);
            }
            if(fn.get("no-border") != null) {
	    		style.setBorderBottom(BorderStyle.NONE);
	    		style.setBorderTop(BorderStyle.NONE);
	    		style.setBorderRight(BorderStyle.NONE);
	    		style.setBorderLeft(BorderStyle.NONE);
            }
            if(fn.get("no-border-right") != null) {
	    		style.setBorderRight(BorderStyle.NONE);
            }
            if(fn.get("no-border-left") != null) {
	    		style.setBorderLeft(BorderStyle.NONE);
            }
		} catch(Exception e) {
		    e.printStackTrace();
		}
		return style;
	}
	
	public CellStyle styleBodynew(SXSSFWorkbook workbook,Map<String, Boolean> fn) {
		CellStyle style=workbook.createCellStyle();
		Font font = workbook.createFont();
        DataFormat format = workbook.createDataFormat();
		
        try {
        	style.setAlignment(HorizontalAlignment.LEFT);
    		style.setVerticalAlignment(VerticalAlignment.TOP);
    		if(fn.get("header") != null) {
    			font = workbook.createFont();
    			font.setBold(true);
    			style.setFont(font);
    			//style.setFillForegroundColor(new Color(new Color(196, 215, 155)));
    			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);    
    			style.setAlignment(HorizontalAlignment.CENTER);
    			style.setBorderBottom(BorderStyle.THIN);
    	        style.setBorderTop(BorderStyle.THIN);
    	        style.setBorderRight(BorderStyle.THIN);
    	        style.setBorderLeft(BorderStyle.THIN);
    	        style.setVerticalAlignment(VerticalAlignment.CENTER);
    		}
            if(fn.get("center") != null) {
            	style.setAlignment(HorizontalAlignment.CENTER);
        		style.setVerticalAlignment(VerticalAlignment.CENTER);
            }
            if(fn.get("top-center") != null) {
            	style.setAlignment(HorizontalAlignment.CENTER);
        		style.setVerticalAlignment(VerticalAlignment.TOP);
            }
            if(fn.get("wrap") != null)style.setWrapText(true);
            
            if(fn.get("bold") != null) {
            	font.setBold(true);
            }
            if(fn.get("title") != null) {
                font.setFontHeightInPoints((short) 14);
            }
//            if(fn.get("green") != null) {
//    			font = workbook.createFont();
//    			font.setColor(new Color(new Color(0,200,100)));
//    			style.setFont(font);
//            }
//            if(fn.get("red") != null) {
//    			font = workbook.createFont();
//    			font.setColor(new XSSFColor(new Color(255,0,0)));
//    			style.setFont(font);
//            }
//            if(fn.get("orange") != null) {
//    			font = workbook.createFont();
//    			font.setColor(new XSSFColor(new Color(255,165,0)));
//    			style.setFont(font);
//            }
            if(fn.get("curr") != null){

            	style.setAlignment(HorizontalAlignment.RIGHT);
        		style.setVerticalAlignment(VerticalAlignment.CENTER);
            	style.setDataFormat(format.getFormat("#,###,###,###,###.00"));
            }
            style.setFont(font);
            if(fn.get("border") == null) {
	    		style.setBorderBottom(BorderStyle.THIN);
	    		style.setBorderTop(BorderStyle.THIN);
	    		style.setBorderRight(BorderStyle.THIN);
	    		style.setBorderLeft(BorderStyle.THIN);
            }
            if(fn.get("no-border") != null) {
	    		style.setBorderBottom(BorderStyle.NONE);
	    		style.setBorderTop(BorderStyle.NONE);
	    		style.setBorderRight(BorderStyle.NONE);
	    		style.setBorderLeft(BorderStyle.NONE);
            }
            if(fn.get("no-border-right") != null) {
	    		style.setBorderRight(BorderStyle.NONE);
            }
            if(fn.get("no-border-left") != null) {
	    		style.setBorderLeft(BorderStyle.NONE);
            }
		} catch(Exception e) {
		    e.printStackTrace();
		}
		return style;
	}
	public XSSFCellStyle formatNumber(XSSFWorkbook workbook) {
        XSSFCellStyle styleNumber=workbook.createCellStyle();
        XSSFDataFormat format = workbook.createDataFormat();
        styleNumber.setVerticalAlignment(VerticalAlignment.CENTER);
        styleNumber.setDataFormat(format.getFormat("#,###,###,###,###.00"));
        return styleNumber;
	}
}
