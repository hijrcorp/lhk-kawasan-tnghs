package id.co.hijr.sistem.common;

public class QueryNewParameter {

	private String locale = "en_US";
	private String clause = "1";
	private String innerClause = "1";
	private String values = "";
	private String order = "";
	private String group = "";
	private Integer limit = 100;
	private Integer offset = 0;

	public QueryNewParameter() {
		// TODO Auto-generated constructor stub
	}

	public String getClause() {
		return clause;
	}

	public void setClause(String clause) {
		this.clause = clause;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Integer getLimit() {
		if(this.limit > 200) return 200;
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getInnerClause() {
		return innerClause;
	}

	public void setInnerClause(String innerClause) {
		this.innerClause = innerClause;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

}
