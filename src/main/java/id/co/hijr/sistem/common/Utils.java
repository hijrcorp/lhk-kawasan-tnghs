package id.co.hijr.sistem.common;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

public class Utils {

	private static final long NUM_100NS_INTERVALS_SINCE_UUID_EPOCH = 0x01b21dd213814000L;

	public static String getUUIDString() {
		NoArgGenerator timeBasedGenerator = Generators.timeBasedGenerator();

		// Generate time based UUID
		UUID firstUUID = timeBasedGenerator.generate();
		return firstUUID.toString();
	}

	public static Date getUUIDDate(String uuidString) {
		return new Date((UUID.fromString(uuidString).timestamp() - NUM_100NS_INTERVALS_SINCE_UUID_EPOCH) / 10000);
	}

	public static long getDateDiffFromNow(Date d1) {
		Date d2 = new Date();
		long seconds = (d2.getTime() - d1.getTime()) / 1000;
		return seconds;
	}

	public static Dimension getImageDimension(File imgFile) throws IOException {
		int pos = imgFile.getName().lastIndexOf(".");
		if (pos == -1)
			throw new IOException("No extension for file: " + imgFile.getAbsolutePath());
		String suffix = imgFile.getName().substring(pos + 1);
		Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
		while (iter.hasNext()) {
			ImageReader reader = iter.next();
			try {
				ImageInputStream stream = new FileImageInputStream(imgFile);
				reader.setInput(stream);
				int width = reader.getWidth(reader.getMinIndex());
				int height = reader.getHeight(reader.getMinIndex());
				return new Dimension(width, height);
			} catch (IOException e) {
				System.out.println("Error reading: " + imgFile.getAbsolutePath());
			} finally {
				reader.dispose();
			}
		}

		throw new IOException("Not a known image file: " + imgFile.getAbsolutePath());
	}
	public static String codeBooking() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
	}
	public static int randomPlus() {
		int min = 100;
		int max = 999;

		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}
	
	public static String getLongNumberID() {
		return (new Date().getTime() + "" + randomPlus());
	}
	
	public static Integer getLongNumberPayment(int patern) {
		return Integer.valueOf((int) Math.floor(patern + Math.random() * 999));
	}
	
	public static String callCodePayment(){
		  long def2 = + (new Date().getTime()/1000);
		  int other_random1 = (getLongNumberPayment(100).toString().length() < 3?Integer.valueOf(getLongNumberPayment(100)+"0"):getLongNumberPayment(100));
		  int other_random2 = (getLongNumberPayment(200).toString().length() < 3?Integer.valueOf(getLongNumberPayment(200)+"0"):getLongNumberPayment(200));
		  int other_random3 = (getLongNumberPayment(300).toString().length() < 3?Integer.valueOf(getLongNumberPayment(300)+"0"):getLongNumberPayment(300));
		  
		  String code = other_random1+""+other_random2+""+other_random3;
		  //console.log(timestamp3);
		  if(code.toString().length() < 10) {
			  code=code+"0";
		  }else{
			  code= code.substring(0, 10);
		  }
		  return code;
	}

	public static String getFullURL(HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();
		String queryString = request.getQueryString();

		if (queryString == null) {
			return requestURL.toString() + "?1";
		} else {
			return requestURL.append('?').append(queryString).toString();
		}
	}

	public static String formatSqlDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		return sdf.format(date);
	}

	public static Date formatDate(String date) throws ParseException {
		DateFormat format = new SimpleDateFormat("dd MMMM yyyy");
		Date dateFormat = null;
		if (!date.equals("")) {
			dateFormat = format.parse(date);
		}
		return dateFormat;
	}

	public static Date formatDate(String date, String strformat) throws ParseException {
		DateFormat format = new SimpleDateFormat(strformat);
		Date dateFormat = null;
		if (!date.equals("")) {
			dateFormat = format.parse(date);
		}
		return dateFormat;
	}

	public static String getFullFormattedDate(Date date) {
		try {
			String strDate = new SimpleDateFormat("dd MMMM yyyy").format(date);
			strDate = strDate.replace("January", "Januari");
			strDate = strDate.replace("February", "Febuari");
			strDate = strDate.replace("March", "Maret");
			strDate = strDate.replace("April", "April");
			strDate = strDate.replace("May", "Mei");
			strDate = strDate.replace("June", "Juni");
			strDate = strDate.replace("July", "Juli");
			strDate = strDate.replace("August", "Agustus");
			strDate = strDate.replace("September", "September");
			strDate = strDate.replace("October", "Oktober");
			strDate = strDate.replace("November", "November");
			strDate = strDate.replace("December", "Desember");
			return strDate;
		} catch (Exception e) {
			return "";
		}
	}
	
	public static boolean ip(String text) {
	    Pattern p = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
	    Matcher m = p.matcher(text);
	    return m.find();
	}
	
	public String formatDecimal(float number) {
	  float epsilon = 0.004f; // 4 tenths of a cent
	  if (Math.abs(Math.round(number) - number) < epsilon) {
	     return String.format("%10.0f", number); // sdb
	  } else {
	     return String.format("%10.2f", number); // dj_segfault
	  }
	}
	

	// Copy to TMPDIR
	public static void copyToTmpDir(InputStream inputStream, String filename) throws IOException {
		File file = new File(System.getProperty("java.io.tmpdir"), filename);
        FileUtils.copyInputStreamToFile(inputStream, file);
        System.out.println("filename: " +filename+ " was copied to java.io.tmpdir");
	}
}
