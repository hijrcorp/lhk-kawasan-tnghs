package id.co.hijr.sistem.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Token {
	
	public static String ACCOUNT_ID = "account_id";
	public static String FULL_NAME = "full_name";
	public static String ACCOUNT_AVATAR = "account_avatar";
	public static String ORGANIZATION_ID = "organization_id";
	public static String ORGANIZATION_NAME = "organization_name";
	public static String POSITION_ID = "position_id";
	public static String POSITION_NAME = "position_name";
	public static String AUTHORITIES= "authorities";

	@JsonProperty("access_token")
	private String accessToken;
	@JsonProperty("refresh_token")
	private String refreshToken;
	@JsonProperty("token_type")
	private String tokenType;
	@JsonProperty("real_name")
	private String realName;
	private String picture;
	@JsonProperty("expires_in")
	private Long expiresIn;
	
	public Token() {
		// TODO Auto-generated constructor stub
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}
	
	
	
}
