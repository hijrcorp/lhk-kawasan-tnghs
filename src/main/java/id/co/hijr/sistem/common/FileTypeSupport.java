package id.co.hijr.sistem.common;

public enum FileTypeSupport {
	DOCUMENT("DOCUMENT", new String[]{"pdf","doc","docx","xls","xlsx","ppt","pptx"}),
	IMAGE("IMAGE", new String[]{"jpg", "jpeg", "png"})
    ;

    private final String text;
    private final String[] extensions;

    /**
     * @param text
     */
    private FileTypeSupport(final String text, final String[] extensions) {
        this.text = text;
        this.extensions = extensions;
    }
    
    public String[] extensions() {
    		return this.extensions;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
