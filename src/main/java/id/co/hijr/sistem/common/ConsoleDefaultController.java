package id.co.hijr.sistem.common;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import id.co.hijr.sistem.mapper.ApplicationMapper;
import id.co.hijr.sistem.model.Application;


@Configuration
@Controller
public class ConsoleDefaultController extends BaseController {
	
	@Autowired
	private ApplicationMapper applicationMapper;
	

    @RequestMapping("/{app}/console/{jsp}")
    public String loadPage(Model model, Device device, Locale locale, @PathVariable String app, @PathVariable String jsp, HttpServletRequest request,HttpServletResponse response) throws Exception{
		
    	
		String accessTokenValue = getCookieTokenValue(request,cookieName);
		boolean pass = isTokenValid(accessTokenValue);
		
		if(!pass) {
			String requestPage = request.getRequestURL().toString();
	    		if(request.getQueryString() != null) {
	    			requestPage += "?"+request.getQueryString();
	    		}
	    		
	    		String resultPath = getConstructURL(request, "login-perform?page="+requestPage); 
	    		
	    		String params = "response_type=code";
			params += "&client_id=" + clientId;
			params += "&redirect_uri=" + resultPath + "";
			System.out.println(getWorkingUrl() + "/oauth/authorize?" + params);
			response.sendRedirect(getWorkingUrl() + "/oauth/authorize?" + params);
		}else {
			
			addModelTokenInfo(request, model, device, locale, accessTokenValue);
			
			model.addAttribute("app", app);
			
			QueryParameter param = new QueryParameter();
	    		param.setClause(param.getClause() + " AND (" + Application.CODE + " = '"+app+"')");
	    		
	    		model.addAttribute("appname",applicationMapper.getList(param).get(0).getName());
			
			return "console/"+jsp;
		}
		
		
		
        return "error";
        
    }
    
    /*
    @RequestMapping("/{app}/console/")
    public String loadMain(Model model, @PathVariable String app, HttpServletRequest request,HttpServletResponse response) throws Exception{
    		String jsp = "index"; // default page, please check
		return loadPage(model, app, jsp, request, response);
    }
    */
    
    @RequestMapping("/{app}/console")
    public String loadMainNoSlash(Model model, Device device, Locale locale, @PathVariable String app, HttpServletRequest request,HttpServletResponse response) throws Exception{
    		String jsp = "index"; // default page, please check
		return loadPage(model, device, locale, app, jsp, request, response);
    }
    
}


