package id.co.hijr.sistem.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class File {
	public static final String ID = "id_file";
	public static final String NAME = "name_file";
	public static final String FOLDER = "folder_file";
	public static final String SIZE = "size_file";
	public static final String TYPE = "type_file";
	public static final String REFERENCE_ID = "reference_id_file";
	public static final String MODULE = "module_file";
	public static final String PERSON_ADDED = "person_added_file";
	public static final String TIME_ADDED = "time_added_file";
	public static final String LOAD = "load_file";

	private String id;
	private String name;
	private String folder;
	private Long size;
	private String type;
	private String referenceId;
	private String module;
	private String personAdded;
	private Date timeAdded;
	private Long load;

	public File() {

	}

	public File(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("folder")
	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	@JsonProperty("size")
	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("reference_id")
	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@JsonProperty("module")
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	@JsonProperty("person_added")
	public String getPersonAdded() {
		return personAdded;
	}

	public void setPersonAdded(String personAdded) {
		this.personAdded = personAdded;
	}

	@JsonProperty("time_added")
	public Date getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}

	@JsonProperty("load")
	public Long getLoad() {
		return load;
	}

	public void setLoad(Long load) {
		this.load = load;
	}




	/**********************************************************************/


}
