package id.co.hijr.sistem.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.hijr.sistem.common.Entity;

public class Authorities extends Entity {
	public static final String ID = "id_authorities";
	public static final String ACCOUNT_ID = "account_id_authorities";
	public static final String GROUP_ID = "group_id_authorities";
	public static final String APPLICATION_ID = "application_id_authorities";
	public static final String PERSON_ADDED = "person_added_authorities";
	public static final String TIME_ADDED = "time_added_authorities";

	private String id;
	private String accountId;
	private String groupId;
	private String applicationId;
	private String personAdded;
	private Date timeAdded;

	public Authorities() {

	}

	public Authorities(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonProperty("group_id")
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@JsonProperty("application_id")
	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	@JsonProperty("person_added")
	public String getPersonAdded() {
		return personAdded;
	}

	public void setPersonAdded(String personAdded) {
		this.personAdded = personAdded;
	}

	@JsonProperty("time_added")
	public Date getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}



	/**********************************************************************/
	
	private Account account = new Account();
	private AccountGroup group = new AccountGroup();
	private Application application = new Application();
	private Position position = new Position();

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public AccountGroup getGroup() {
		return group;
	}

	public void setGroup(AccountGroup group) {
		this.group = group;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

}
