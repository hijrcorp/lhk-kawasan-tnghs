package id.co.hijr.sistem.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PositionGroup {
	public static final String ID = "id_position_group";
	public static final String NAME = "name_position_group";
	public static final String ORGANIZATION_ID = "organization_id_position_group";

	private String id;
	private String name;
	private String organizationId;

	public PositionGroup() {

	}

	public PositionGroup(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}



	/**********************************************************************/
}
