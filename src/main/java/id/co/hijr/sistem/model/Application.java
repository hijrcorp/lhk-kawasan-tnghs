package id.co.hijr.sistem.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Application {
	public static final String ID = "id_application";
	public static final String CODE = "code_application";
	public static final String NAME = "name_application";
	public static final String PREFIX_ROLE = "prefix_role_application";
	public static final String ORGANIZATION_ID = "organization_id_application";
	public static final String LINK = "link_application";
	public static final String DEFAULT_ROLE = "default_role_application";
	public static final String PERSON_ADDED = "person_added_application";
	public static final String TIME_ADDED = "time_added_application";
	public static final String PERSON_MODIFIED = "person_modified_application";
	public static final String TIME_MODIFIED = "time_modified_application";

	private String id;
	private String code;
	private String name;
	private String prefixRole;
	private String organizationId;
	private String link;
	private String defaultRole;
	private String personAdded;
	private Date timeAdded;
	private String personModified;
	private Date timeModified;

	public Application() {

	}

	public Application(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("prefix_role")
	public String getPrefixRole() {
		return prefixRole;
	}

	public void setPrefixRole(String prefixRole) {
		this.prefixRole = prefixRole;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@JsonProperty("default_role")
	public String getDefaultRole() {
		return defaultRole;
	}

	public void setDefaultRole(String defaultRole) {
		this.defaultRole = defaultRole;
	}

	@JsonProperty("person_added")
	public String getPersonAdded() {
		return personAdded;
	}

	public void setPersonAdded(String personAdded) {
		this.personAdded = personAdded;
	}

	@JsonProperty("time_added")
	public Date getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}

	@JsonProperty("person_modified")
	public String getPersonModified() {
		return personModified;
	}

	public void setPersonModified(String personModified) {
		this.personModified = personModified;
	}

	@JsonProperty("time_modified")
	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}



	/**********************************************************************/
	
	private Organization organization = new Organization();


	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
