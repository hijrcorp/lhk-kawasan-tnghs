package id.co.hijr.sistem.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.hijr.sistem.common.Entity;

public class Account extends Entity {
	public static final String ID = "id_account";
	public static final String USERNAME = "username_account";
	public static final String PASSWORD = "password_account";
	public static final String EMAIL = "email_account";
	public static final String EMAIL_STATUS = "email_status_account";
	public static final String FIRST_NAME = "first_name_account";
	public static final String LAST_NAME = "last_name_account";
	public static final String MOBILE = "mobile_account";
	public static final String MOBILE_STATUS = "mobile_status_account";
	public static final String MALE_STATUS = "male_status_account";
	public static final String BIRTH_DATE = "birth_date_account";
	public static final String ENABLED = "enabled_account";
	public static final String ACCOUNT_NON_EXPIRED = "account_non_expired_account";
	public static final String CREDENTIALS_NON_EXPIRED = "credentials_non_expired_account";
	public static final String ACCOUNT_NON_LOCKED = "account_non_locked_account";
	public static final String PERSON_ADDED = "person_added_account";
	public static final String TIME_ADDED = "time_added_account";
	public static final String PERSON_MODIFIED = "person_modified_account";
	public static final String TIME_MODIFIED = "time_modified_account";
	public static final String LOGIN = "login_account";

	private String id;
	private String username;
	private String password;
	private String email;
	private Boolean emailStatus;
	private String firstName;
	private String lastName;
	private String mobile;
	private Boolean mobileStatus;
	private Boolean maleStatus;
	private Date birthDate;
	private boolean enabled;
	private boolean accountNonExpired;
	private boolean credentialsNonExpired;
	private boolean accountNonLocked;
	private String personAdded;
	private Date timeAdded;
	private String personModified;
	private Date timeModified;
	private Long login;

	public Account() {

	}

	public Account(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("email_status")
	public Boolean isEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(Boolean emailStatus) {
		this.emailStatus = emailStatus;
	}

	@JsonProperty("first_name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("last_name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("mobile")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@JsonProperty("mobile_status")
	public Boolean isMobileStatus() {
		return mobileStatus;
	}

	public void setMobileStatus(Boolean mobileStatus) {
		this.mobileStatus = mobileStatus;
	}

	@JsonProperty("male_status")
	public Boolean isMaleStatus() {
		return maleStatus;
	}

	public void setMaleStatus(Boolean maleStatus) {
		this.maleStatus = maleStatus;
	}

	@JsonProperty("birth_date")
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@JsonProperty("enabled")
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@JsonProperty("account_non_expired")
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	@JsonProperty("credentials_non_expired")
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	@JsonProperty("account_non_locked")
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@JsonProperty("person_added")
	public String getPersonAdded() {
		return personAdded;
	}

	public void setPersonAdded(String personAdded) {
		this.personAdded = personAdded;
	}

	@JsonProperty("time_added")
	public Date getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}

	@JsonProperty("person_modified")
	public String getPersonModified() {
		return personModified;
	}

	public void setPersonModified(String personModified) {
		this.personModified = personModified;
	}

	@JsonProperty("time_modified")
	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	@JsonProperty("login")
	public Long getLogin() {
		return login;
	}

	public void setLogin(Long login) {
		this.login = login;
	}



	/**********************************************************************/
	
	public String getName() {
		if(this.lastName != null && !this.lastName.equals("")) {
			return this.firstName + " " + this.lastName;
		}
			
		return this.firstName;
	}

	private String regionId;

	@JsonProperty("region_id")
	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	
}
