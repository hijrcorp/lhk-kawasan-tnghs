package id.co.hijr.sistem.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Assignment {
	public static final String ID = "id_assignment";
	public static final String ACCOUNT_ID = "account_id_assignment";
	public static final String POSITION_ID = "position_id_assignment";
	public static final String ORGANIZATION_ID = "organization_id_assignment";
	public static final String STATUS = "status_assignment";
	public static final String START_DATE = "start_date_assignment";
	public static final String END_DATE = "end_date_assignment";
	public static final String PARENT_ID = "parent_id_assignment";
	public static final String ACTIVE = "active_assignment";

	private String id;
	private String accountId;
	private String positionId;
	private String organizationId;
	private String status;
	private Date startDate;
	private Date endDate;
	private String parentId;
	private Boolean active;

	public Assignment() {

	}

	public Assignment(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonProperty("position_id")
	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("start_date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonProperty("end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@JsonProperty("parent_id")
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@JsonProperty("active")
	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}



	/**********************************************************************/
	
	private Account account = new Account();
	private Position position = new Position();
	private Organization organization = new Organization();
	private String additionalRoles;
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@JsonProperty("additional_roles")
	public String getAdditionalRoles() {
		return additionalRoles;
	}

	public void setAdditionalRoles(String additionalRoles) {
		this.additionalRoles = additionalRoles;
	}
	
	@JsonProperty("super_admin")
	public boolean isSuperAdmin() {
		if(this.additionalRoles != null) {
			return this.additionalRoles.contains("ROLE_SUPER_ADMIN");
		}
		return false;
	}
	
	public boolean isAdmin() {
		if(this.additionalRoles != null && this.organization.getAdminRole() != null) {
			return this.additionalRoles.contains(this.organization.getAdminRole());
		}
		return false;
	}

}
