package id.co.hijr.sistem.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApplicationClient {
	public static final String ID = "id_application_client";
	public static final String HEADER_ID = "header_id_application_client";
	public static final String CODE = "code_application_client";
	public static final String TYPE = "type_application_client";
	public static final String RESOURCE = "resource_application_client";
	public static final String ORGANIZATION_ID = "organization_id_application_client";

	private String id;
	private String headerId;
	private String code;
	private String type;
	private String resource;
	private String organizationId;

	public ApplicationClient() {

	}

	public ApplicationClient(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("resource")
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}





	/**********************************************************************/

}
