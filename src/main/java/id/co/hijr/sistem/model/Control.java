package id.co.hijr.sistem.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Control {
	public static final String ID = "id_control";
	public static final String ACCOUNT_ID = "account_id_control";
	public static final String SOURCE_ID = "source_id_control";
	public static final String IS_ADMIN = "is_admin_control";
	
	private String id;
	private String accountId;
	private String sourceId;
	private Boolean isAdmin;
	
	public Control() {
	
	}
	
	public Control(String id) {
		this.id = id;
	}
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	@JsonProperty("source_id")
	public String getSourceId() {
		return sourceId;
	}
	
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
	@JsonProperty("is_admin")
	public Boolean isIsAdmin() {
		return isAdmin;
	}
	
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	
	/**********************************************************************/
}
