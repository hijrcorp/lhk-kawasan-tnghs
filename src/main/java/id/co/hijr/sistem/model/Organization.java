package id.co.hijr.sistem.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Organization {
	public static final String ID = "id_organization";
	public static final String NAME = "name_organization";
	public static final String POSITION_ID = "position_id_organization";
	public static final String PARENT_ID = "parent_id_organization";
	public static final String ADMIN_ROLE = "admin_role_organization";
	public static final String SEQUENCE = "sequence_organization";

	private String id;
	private String name;
	private String positionId;
	private String parentId;
	private String adminRole;
	private Integer sequence;

	public Organization() {

	}

	public Organization(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("position_id")
	public String getPositionId() {
		return positionId;
	}

	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}

	@JsonProperty("parent_id")
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@JsonProperty("admin_role")
	public String getAdminRole() {
		return adminRole;
	}

	public void setAdminRole(String adminRole) {
		this.adminRole = adminRole;
	}

	@JsonProperty("sequence")
	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}




	/**********************************************************************/
	
	private Position position = new Position();
	
	@JsonProperty("code")
	public String getCode() {
		if(this.adminRole != null) {
			return this.adminRole.substring(5,this.adminRole.indexOf("_ADMIN"));
		}
		return null;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

}
