package id.co.hijr.sistem.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.hijr.sistem.MainApplication;
import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.PositionGroupMapper;
import id.co.hijr.sistem.mapper.PositionMapper;
import id.co.hijr.sistem.model.Application;
import id.co.hijr.sistem.model.Position;
import id.co.hijr.sistem.ref.AccountLoginInfo;

@Controller
@RequestMapping("/position")
public class PositionRestController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private PositionMapper positionMapper;
	
	@Autowired
	private PositionGroupMapper positionGroupMapper;

	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		@RequestParam("filter_group") Optional<String> filterGroup,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
	    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Position.NAME + " LIKE '%"+filterKeyword.get()+"%')");
	    	}
	    	
	    	if(filterGroup.isPresent() && !filterGroup.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Position.GROUP_ID + " = '"+filterGroup.get()+"')");
	    	}
	    	
	    	param.setClause(param.getClause() + " AND " + Position.ORGANIZATION_ID + "='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
	    	
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Position> data = positionMapper.getList(param);
		
		resp.setCount(positionMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Position data = positionMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getName()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("name") Optional<String> name,
    		@RequestParam("description") Optional<String> description,
    		@RequestParam("group_id") Optional<String> groupId,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Position data = new Position(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = positionMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(name.isPresent()) data.setName(name.get());
    		if(description.isPresent()) data.setDescription(description.get());
    		if(groupId.isPresent()) data.setGroupId(groupId.get());
    		
    		data.setOrganizationId(extractAccountLogin(request, AccountLoginInfo.SOURCE));

    		boolean pass = true;
    		if( data.getName() == null || data.getGroupId() == null || data.getOrganizationId() == null) {
    			pass = false;
    		}else {
    			if(data.getName().equals("") || data.getGroupId().equals("") || data.getOrganizationId().equals("")) {
        			pass = false;
        		}
    		}
    		
    		data.setGroup(positionGroupMapper.getEntity(data.getGroupId()));

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			positionMapper.update(data);
    		}else {
    			positionMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Position data = positionMapper.getEntity(id);
			resp.setData(data);
			positionMapper.delete(data);
			
			resp.setMessage("Data '"+data.getName()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
