package id.co.hijr.sistem.controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.FileTypeSupport;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.StorageService;



@RestController
@RequestMapping("/files")
public class FilesRestController  extends BaseController  {
	
	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	
	@Autowired
	private FileMapper fileMapper;
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public void loadFile(
			@PathVariable String id,
			@RequestParam("filename") Optional<String> filename,
			@RequestParam("download") Optional<String> download,
			@RequestParam("decrypt") Optional<String> decrypt,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		
			
			id.co.hijr.sistem.model.File data = fileMapper.getEntity(id);
			
			String name = request.getServletPath();
			if(filename.isPresent()) name = filename.get();
		
			String ext = FilenameUtils.getExtension(name);
			String type = "application";
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				type="image";
				if(!download.isPresent()) {
					id = "small_"+id;
				}
			}else {
				if(!download.isPresent()) {
					response.setHeader("Content-Disposition", "filename=\""+name+"\"");
				}else {
					response.setHeader("Content-Disposition", "attachment; filename=\""+name+"\"");
				}
			}
			response.setContentType(type+"/"+ext);
			
			File file = storageService.load(id + "." + ext);
			if(decrypt.isPresent()) {
				file = storageService.loadWithDecrypt(id);
			}
			
			FileInputStream fileInputStream = new FileInputStream(file);
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fileInputStream.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			fileInputStream.close();
			
			fileMapper.updateLoad(data);

			if(decrypt.isPresent()) FileSystemUtils.deleteRecursively(file);
	}
	
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		@RequestParam("filter_folder") Optional<String> filterFolder,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
	    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + id.co.hijr.sistem.model.File.NAME + " LIKE '%"+filterKeyword.get()+"%')");
	    	}
	    	
	    	if(filterFolder.isPresent() && !filterFolder.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + id.co.hijr.sistem.model.File.FOLDER + " = '"+filterFolder.get()+"')");
	    	}	    	
	    	
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<id.co.hijr.sistem.model.File> data = fileMapper.getList(param);
		
		resp.setCount(fileMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	
	@RequestMapping(value="/upload", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> upload(
    		@RequestParam("file") MultipartFile file,
    		@RequestParam("folder") Optional<String> folder,
    		@RequestParam("reference") Optional<String> reference,
    		@RequestParam("decrypt") Optional<String> decrypt,
    		HttpServletRequest request
    		) throws Exception {
		
		ResponseWrapper resp = new ResponseWrapper();
		
		id.co.hijr.sistem.model.File data = new id.co.hijr.sistem.model.File(Utils.getLongNumberID());
		if(folder.isPresent()) {
			data.setFolder("/" + folder.get());
		}else {
			data.setFolder("/");
		}
		data.setSize(file.getSize());
		data.setType(file.getContentType());
		data.setName(file.getOriginalFilename());
		//data.setPersonAdded(this.extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
		data.setTimeAdded(new Date());
		if(reference.isPresent()) data.setReferenceId(reference.get());
		
		String ext = FilenameUtils.getExtension(data.getName());
    	
		if(decrypt.isPresent()) {
			storageService.storeWithEncrypted(file, data.getId() + "."+ext, data.getId());
		}else {
			storageService.store(file, data.getId() + "."+ext);
		}
			
    	fileMapper.insert(data);
		
 		resp.setMessage("File telah berhasil diupload.");
		resp.setData(data);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		
	}
	
	@RequestMapping(value="/{id}/remove", method = RequestMethod.POST, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> remove(
			@PathVariable String id
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		id.co.hijr.sistem.model.File data = fileMapper.getEntity(id);

		
		
		String ext = FilenameUtils.getExtension(data.getName());
		storageService.delete(id + "." + ext);
		
		fileMapper.delete(data);
		
//		Thread.sleep(1000);
		resp.setData(data);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
}
