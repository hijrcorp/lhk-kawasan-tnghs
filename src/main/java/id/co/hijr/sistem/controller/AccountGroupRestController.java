package id.co.hijr.sistem.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.hijr.sistem.MainApplication;
import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountGroupMapper;
import id.co.hijr.sistem.mapper.ApplicationMapper;
import id.co.hijr.sistem.model.AccountGroup;
import id.co.hijr.sistem.model.Application;

@Controller
@RequestMapping("/account/group")
public class AccountGroupRestController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	private AccountGroupMapper accountGroupMapper;

	
	@Autowired
	private ApplicationMapper applicationMapper;
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		@RequestParam("filter_application") Optional<String> filterApplication,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
	    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + AccountGroup.NAME + " LIKE '%"+filterKeyword.get()+"%')");
	    	}
	    	
	    	if(filterApplication.isPresent() && !filterApplication.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Application.ID + " = '"+filterApplication.get()+"' ");
	    		param.setClause(param.getClause() + " OR " + Application.CODE + " = '"+filterApplication.get()+"')");
	    	}
	    	
    	
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<AccountGroup> data = accountGroupMapper.getList(param);
		
		resp.setCount(accountGroupMapper.getCount(param));
		resp.setData(data);
		
		
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		AccountGroup data = accountGroupMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getName()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("name") Optional<String> name,
    		@RequestParam("description") Optional<String> description,
    		@RequestParam("application_id") Optional<String> applicationId,
    		@RequestParam("sequence") Optional<Integer> sequence,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		AccountGroup data = new AccountGroup(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = accountGroupMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(name.isPresent()) data.setName(name.get());
    		if(description.isPresent()) data.setDescription(description.get());
    		if(sequence.isPresent()) data.setSequence(sequence.get());

    		boolean pass = true;
    		if( data.getName() == null) {
    			pass = false;
    		}else {
    			if(data.getName().equals("")) {
        			pass = false;
        		}
    		}
    		
    		QueryParameter param = new QueryParameter();
    		
	    	if(applicationId.isPresent() && !applicationId.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Application.ID + " = '"+applicationId.get()+"' ");
	    		param.setClause(param.getClause() + " OR " + Application.CODE + " = '"+applicationId.get()+"')");
	    	}else {
	    		pass = false;
	    	}
    		
	    	List<Application> lst = applicationMapper.getList(param);
	    	if(lst.size() > 0) {
	    		data.setApplicationId(lst.get(0).getId());
	    		data.setApplication(lst.get(0));
	    	}else {
	    		pass = false;
	    	}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			accountGroupMapper.update(data);
    		}else {
    			accountGroupMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			AccountGroup data = accountGroupMapper.getEntity(id);
			resp.setData(data);
			accountGroupMapper.delete(data);
			
			resp.setMessage("Data '"+data.getName()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
