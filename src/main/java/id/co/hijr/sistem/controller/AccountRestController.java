package id.co.hijr.sistem.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.hijr.sistem.MainApplication;
import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountGroupMapper;
import id.co.hijr.sistem.mapper.AccountMapper;
import id.co.hijr.sistem.mapper.ApplicationMapper;
import id.co.hijr.sistem.mapper.AssignmentMapper;
import id.co.hijr.sistem.mapper.AuthoritiesMapper;
import id.co.hijr.sistem.mapper.OrganizationMapper;
import id.co.hijr.sistem.mapper.PositionMapper;
import id.co.hijr.sistem.model.Account;
import id.co.hijr.sistem.model.Application;
import id.co.hijr.sistem.model.Assignment;
import id.co.hijr.sistem.model.Authorities;
import id.co.hijr.sistem.model.Control;
import id.co.hijr.sistem.ref.AccountLoginInfo;

import id.co.hijr.sistem.mapper.ControlMapper;

//import id.co.hijr.sistem.domain.MappingJabatanUnitAuditi;
//import id.co.hijr.sistem.domain.PertanyaanJawaban;
//import id.co.hijr.sistem.mapper.MappingJabatanUnitAuditiMapper;
//import id.co.hijr.sistem.mapper.SesiMapper;
//import id.co.hijr.sistem.mapper.UserMapper;

@Controller
@RequestMapping("/account")
public class AccountRestController extends BaseController {

	public final static String DELETE = "delete";
	public final static String RESET = "reset";
	
	@Autowired
	private AccountMapper accountMapper;
	
//	@Autowired
//	private UserMapper userMapper;
//	
//	@Autowired
//	private MappingJabatanUnitAuditiMapper mappingJabatanUnitAuditiMapper;
	
	@Autowired
	private AssignmentMapper assignmentMapper;
	
	@Autowired
	private AuthoritiesMapper authoritiesMapper;
	
	@Autowired
	private AccountGroupMapper accountGroupMapper;
	
	@Autowired
	private ApplicationMapper applicationMapper;
	
	@Autowired
	private ControlMapper controlMapper;

	@Autowired
    private PasswordEncoder passwordEncoder;
		
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
	    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
	    		param.setClause(param.getClause() + " AND (" + Account.FIRST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
	    		param.setClause(param.getClause() + " OR " + Account.LAST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
	    		param.setClause(param.getClause() + " OR " + Account.USERNAME + " LIKE '%"+filterKeyword.get()+"%')");
	    	}
	    	
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Account> data = accountMapper.getList(param);
		
		resp.setCount(accountMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action,
			HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		Account data = accountMapper.getEntity(id+extractAccountLogin(request, AccountLoginInfo.SOURCE));
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getUsername()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
		@RequestParam("id") Optional<String> id,
		@RequestParam("username") Optional<String> username,
		@RequestParam("password") Optional<String> password,
		@RequestParam("password2") Optional<String> password2,
		@RequestParam("email") Optional<String> email,
		@RequestParam("email_status") Optional<Boolean> emailStatus,
		@RequestParam("first_name") Optional<String> firstName,
		@RequestParam("last_name") Optional<String> lastName,
		@RequestParam("mobile") Optional<String> mobile,
		@RequestParam("mobile_status") Optional<Boolean> mobileStatus,
		@RequestParam("male_status") Optional<Boolean> maleStatus,
		@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birthDate,
		@RequestParam("enabled") Optional<Boolean> enabled,
		@RequestParam("account_non_expired") Optional<Boolean> accountNonExpired,
		@RequestParam("credentials_non_expired") Optional<Boolean> credentialsNonExpired,
		@RequestParam("account_non_locked") Optional<Boolean> accountNonLocked,
		@RequestParam("unit_auditi_id") Optional<String> unitAuditiId,
		@RequestParam("region_id") Optional<String> regionId,
		@RequestParam("id_kelompok_jabatan") Optional<String> idKelompokJabatan,
		@RequestParam("id_jabatan") Optional<String> idJabatan,
		@RequestParam("organization_id") Optional<String> organizationId,
		@RequestParam("group_id") Optional<String> groupId,
		@RequestParam("application_id") Optional<String> applicationId,
		HttpServletRequest request
		) throws Exception {
	
		ResponseWrapper resp = new ResponseWrapper();
		
		Account data = new Account(Utils.getLongNumberID());
		if(id.isPresent()) {
			data.setId(id.get());
			data = accountMapper.getEntity(id.get());
		}
		if(data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if(id.isPresent()) data.setId(id.get());
		if(username.isPresent()) data.setUsername(username.get());
		if(password.isPresent()) {
			if(!password.get().equals(password2.get())) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Kata kunci yang anda masukkan tidak sama.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
			data.setPassword(passwordEncoder.encode(password.get()));
		}

		if(email.isPresent()) data.setEmail(email.get());
		if(emailStatus.isPresent()) data.setEmailStatus(emailStatus.get());
		if(firstName.isPresent()) data.setFirstName(firstName.get());
		if(lastName.isPresent()) data.setLastName(lastName.get());
		if(mobile.isPresent()) data.setMobile(mobile.get());
		if(mobileStatus.isPresent()) data.setMobileStatus(mobileStatus.get());
		if(maleStatus.isPresent()) data.setMaleStatus(maleStatus.get());
		if(birthDate.isPresent()) data.setBirthDate(new Date());
		
		if(enabled.isPresent()) {
			data.setEnabled(false);
		}else {
			data.setEnabled(true);
		}
		//default
		data.setAccountNonExpired(true);
		data.setCredentialsNonExpired(true);
		data.setAccountNonLocked(true);
		/*if(accountNonExpired.isPresent()) 
			data.setAccountNonExpired(accountNonExpired.get());
		if(credentialsNonExpired.isPresent()) 
			data.setCredentialsNonExpired(credentialsNonExpired.get());
		if(accountNonLocked.isPresent()) 
			data.setAccountNonLocked(accountNonLocked.get());*/
		
		boolean pass = true;
		if( data.getUsername() == null || data.getFirstName() == null || data.getLastName() == null) {
			pass = false;
		}else {
			if(data.getUsername().equals("") || data.getFirstName().equals("") || data.getLastName().equals("")) {
    			pass = false;
    		}
		}
		
		String akunTemp="";
		String sourceTemp="";
		try {
			akunTemp=this.extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			sourceTemp=this.extractAccountLogin(request, AccountLoginInfo.SOURCE);
		} catch (Exception e) {
			// TODO: handle exception
			akunTemp="";
			sourceTemp="3110201400001";
		}
		
		if(applicationId.isPresent() && !applicationId.get().equals("")) {
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
    		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
    		
    		List<Application> lst = applicationMapper.getList(param);
    		if(lst.size() == 0) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Aplikasi KAWASAN belum terdaftar didatabase.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
		}
		
		if(groupId.isPresent() && !groupId.get().equals("")) {
    		if(accountGroupMapper.getEntity(groupId.get()) == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Group "+groupId.get()+" belum terdaftar didatabase.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
		}

		if(!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		

		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Account.USERNAME+" ='"+data.getUsername()+"'");
		if(accountMapper.getCount(param) > 0 && !id.isPresent()) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Duplication entry for username '"+data.getUsername()+"'");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		
		Authorities authorities = new Authorities(Utils.getLongNumberID());
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
		
		List<Application> lst = applicationMapper.getList(param);
		
		if(lst.size() > 0) {
			Application app = lst.get(0);
			
			QueryParameter authparam = new QueryParameter();
			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+data.getId()+"'");
			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
			List<Authorities> authlst = authoritiesMapper.getList(authparam);
			if(authlst.size() > 0) {
				authorities = authlst.get(0);
			}
			
			authorities.setAccountId(data.getId());
			authorities.setAccount(data);
			authorities.setGroup(accountGroupMapper.getEntity(groupId.get()));
			authorities.setGroupId(groupId.get());
			authorities.setApplication(app);
			authorities.setApplicationId(app.getId());
			
			authorities.setPersonAdded(akunTemp);
			authorities.setTimeAdded(new Date());
	
			if(authlst.size() > 0) {
				authoritiesMapper.update(authorities);
			}else {
				authoritiesMapper.insert(authorities);	
			}
		}
		

		if(id.isPresent()) {
			data.setPersonModified(akunTemp);
			data.setTimeModified(new Date());
			
			accountMapper.update(data);
		}else {
			data.setPersonAdded(akunTemp);
			data.setTimeAdded(new Date());
			
			accountMapper.insert(data);	
		}
		
		//mapping region here
		if(regionId.isPresent() && !regionId.get().equals("")) {
			accountMapper.deleteAllUnitAuditiUser(data.getId());
			accountMapper.insertUnitAuditiUser(regionId.get(), data.getId());
		}
		
		param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Control.ACCOUNT_ID+" ='"+data.getId()+"'");
		param.setClause(param.getClause()+" AND "+Control.SOURCE_ID+" ='"+sourceTemp+"'");
		controlMapper.deleteBatch(param);
		
		Control control = new Control(Utils.getLongNumberID());
		control.setAccountId(data.getId());
		control.setSourceId(sourceTemp);
		if(authorities.getGroupId().equals("2002200") || authorities.getGroupId().equals("2002400")) {
			control.setIsAdmin(false);
//				if(authorities.getGroupId().equals("2002400")) {
//    				accountMapper.insertAccountRole(data.getId(), "ROLE_SUPERVISIOR");
//    			}
		}else {
			control.setIsAdmin(true);
		}
		controlMapper.insert(control);
		
		//use this function only if we have one command
		//cause the transaction we used, not will work
		/*try {
		} catch (DuplicateKeyException e) {
			// TODO: handle exception
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Duplication entry for username '"+data.getUsername()+"'");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		} catch (Exception e) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("FATAL ERROR!!! Contact administrator");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}*/
		resp.setData(authorities);
		resp.setMessage("Data telah berhasil disimpan.");
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

	
    @RequestMapping(value="/update", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> update(
		@RequestParam("id") Optional<String> id,
		@RequestParam("username") Optional<String> username,
		@RequestParam("password") Optional<String> password,
		@RequestParam("password2") Optional<String> password2,
		@RequestParam("email") Optional<String> email,
		@RequestParam("email_status") Optional<Boolean> emailStatus,
		@RequestParam("first_name") Optional<String> firstName,
		@RequestParam("last_name") Optional<String> lastName,
		@RequestParam("mobile") Optional<String> mobile,
		@RequestParam("mobile_status") Optional<Boolean> mobileStatus,
		@RequestParam("male_status") Optional<Boolean> maleStatus,
		@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> birthDate,
		@RequestParam("enabled") Optional<Boolean> enabled,
		@RequestParam("account_non_expired") Optional<Boolean> accountNonExpired,
		@RequestParam("credentials_non_expired") Optional<Boolean> credentialsNonExpired,
		@RequestParam("account_non_locked") Optional<Boolean> accountNonLocked,
		@RequestParam("unit_auditi_id") Optional<String> unitAuditiId,
		@RequestParam("region_id") Optional<String> regionId,
		@RequestParam("id_kelompok_jabatan") Optional<String> idKelompokJabatan,
		@RequestParam("id_jabatan") Optional<String> idJabatan,
		@RequestParam("organization_id") Optional<String> organizationId,
		@RequestParam("group_id") Optional<String> groupId,
		@RequestParam("application_id") Optional<String> applicationId,
		HttpServletRequest request
		) throws Exception {
	
		ResponseWrapper resp = new ResponseWrapper();
		
		Account data = new Account(Utils.getLongNumberID());
		if(id.isPresent()) {
			data.setId(id.get());
			data = accountMapper.getEntity(id.get());
		}
		if(data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
		}

		if(id.isPresent()) data.setId(id.get());
		if(username.isPresent()) data.setUsername(username.get());
		if(password.isPresent()) {
			if(!password.get().equals(password2.get())) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Kata kunci yang anda masukkan tidak sama.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
			data.setPassword(passwordEncoder.encode(password.get()));
		}

		if(email.isPresent()) data.setEmail(email.get());
		if(emailStatus.isPresent()) data.setEmailStatus(emailStatus.get());
		if(firstName.isPresent()) data.setFirstName(firstName.get());
		if(lastName.isPresent()) data.setLastName(lastName.get());
		if(mobile.isPresent()) data.setMobile(mobile.get());
		if(mobileStatus.isPresent()) data.setMobileStatus(mobileStatus.get());
		if(maleStatus.isPresent()) data.setMaleStatus(maleStatus.get());
		if(birthDate.isPresent()) data.setBirthDate(new Date());
		
		if(enabled.isPresent()) {
			data.setEnabled(false);
		}else {
			data.setEnabled(true);
		}
		//default
		data.setAccountNonExpired(true);
		data.setCredentialsNonExpired(true);
		data.setAccountNonLocked(true);

		boolean pass = true;
		if( data.getUsername() == null || data.getFirstName() == null || data.getLastName() == null) {
			pass = false;
		}else {
			if(data.getUsername().equals("") || data.getFirstName().equals("") || data.getLastName().equals("")) {
    			pass = false;
    		}
		}
		
		String akunTemp="";
		String sourceTemp="";
		try {
			akunTemp=this.extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
			sourceTemp=this.extractAccountLogin(request, AccountLoginInfo.SOURCE);
		} catch (Exception e) {
			// TODO: handle exception
			akunTemp="";
			sourceTemp="3110201400001";
		}
		
		if(applicationId.isPresent() && !applicationId.get().equals("")) {
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
    		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
    		
    		List<Application> lst = applicationMapper.getList(param);
    		if(lst.size() == 0) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Aplikasi KAWASAN belum terdaftar didatabase.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
		}
		
		if(groupId.isPresent() && !groupId.get().equals("")) {
    		if(accountGroupMapper.getEntity(groupId.get()) == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Group "+groupId.get()+" belum terdaftar didatabase.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
		}

		if(!pass) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		

		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Account.USERNAME+" ='"+data.getUsername()+"'");
		if(accountMapper.getCount(param) > 0 && !id.isPresent()) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Duplication entry for username '"+data.getUsername()+"'");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		
		Authorities authorities = new Authorities(Utils.getLongNumberID());
		param = new QueryParameter();
		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
		
		List<Application> lst = applicationMapper.getList(param);
		
		if(lst.size() > 0) {
			Application app = lst.get(0);
			
			QueryParameter authparam = new QueryParameter();
			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+data.getId()+"'");
			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
			List<Authorities> authlst = authoritiesMapper.getList(authparam);
			if(authlst.size() > 0) {
				authorities = authlst.get(0);
			}
			
			authorities.setAccountId(data.getId());
			authorities.setAccount(data);
			authorities.setGroup(accountGroupMapper.getEntity(authorities.getGroupId()));
			authorities.setApplication(app);
			authorities.setApplicationId(app.getId());
			
		}
		

		accountMapper.update(data);
		//mapping region here
		if(regionId.isPresent() && !regionId.get().equals("") && authorities.getGroup().getName().equals("Supervisor")) {
			accountMapper.deleteAllUnitAuditiUser(data.getId());
			accountMapper.insertUnitAuditiUser(regionId.get(), data.getId());
		}
		
		
		resp.setData(authorities);
		resp.setMessage("Data telah berhasil diupdate.");
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
    @RequestMapping(value="/savemulti", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> saveMulti(
    		@RequestParam("group_id") Optional<String> groupId,
    		@RequestParam("application_id") Optional<String> applicationId,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		//check aplikasi
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
    		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
    		List<Application> lst = applicationMapper.getList(param);
    		if(lst == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Aplikasi "+applicationId.get()+" tidak dikenal!");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		//
    		//
    		int i = 1;
    		String[] ids = request.getParameterValues("id");
    		String[] usernames = request.getParameterValues("username");
    		String[] names = request.getParameterValues("nama");
    		String[] jabatans = request.getParameterValues("id_jabatan");
    		String[] unitAuditis = request.getParameterValues("id_unit_auditi");
    		List<Account> arr_users = new ArrayList<Account>();
    		System.out.println("jumlah user "+usernames.length);
    		if (usernames.length > 0) {
    			System.out.println("TRY SUBMIT");
    			int ke = 0;
    			for (String o : usernames) {
					//first
	    			Account account = new Account(Utils.getLongNumberID());
    				if(!ids[ke].equals("")) {
    					account.setId(ids[ke]);
    					account = accountMapper.getEntity(ids[ke]);
    	    		}
    	    		if(account == null) {
    	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    	    			resp.setMessage("Data tidak ditemukan.");
    	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    	    		}
					account.setUsername(usernames[ke]);
					String []mynames=names[ke].split(" ");
            		account.setFirstName(mynames[0]);
            		String lastname="";
					if(mynames.length>1) {
						for(int istr=1; istr < mynames.length;istr++) {
							lastname+=" "+mynames[istr];
						}
					}
					System.out.println("SNAMA "+lastname);
					account.setLastName(lastname);
					account.setEnabled(false);
		    		//default
					account.setAccountNonExpired(true);
					account.setCredentialsNonExpired(true);
					account.setAccountNonLocked(true);
            		arr_users.add(account);

					if(usernames[ke].equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Input username belum lengkap, harap dilengkapi terlebih dahulu.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		    		}
					if(jabatans[ke].equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Input jabatan belum lengkap, harap dilengkapi terlebih dahulu.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		    		}
					if(unitAuditis[ke].equals("")) {
		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
		    			resp.setMessage("Input jabatan belum lengkap, harap dilengkapi terlebih dahulu.");
		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		    		}

//		    		if(sesiMapper.getEntityActive()==null) {
//		    			resp.setCode(HttpStatus.BAD_REQUEST.value());
//		    			resp.setMessage("Tidak dapat membuat user,karena sesi ujian yang aktif tidak tersedia.");
//		    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//					}
					//second
		    		Authorities authorities = new Authorities(Utils.getLongNumberID());
        			Application app = lst.get(0);
        			QueryParameter authparam = new QueryParameter();
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+account.getId()+"'");
        			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
            		List<Authorities> authlst = authoritiesMapper.getList(authparam);
            		if(authlst.size() > 0) {
            			authorities = authlst.get(0);
            		}
        			authorities.setAccountId(account.getId());
            		authorities.setAccount(account);
            		authorities.setGroup(accountGroupMapper.getEntity(groupId.get()));
            		authorities.setGroupId(groupId.get());
            		authorities.setApplication(app);
            		authorities.setApplicationId(app.getId());
            		authorities.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
            		authorities.setTimeAdded(new Date());
					//
            		try {
            			if(!ids[ke].equals("")) {
            				account.setPersonModified(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
            				account.setTimeModified(new Date());
                			accountMapper.update(account);
                		}else {
                			account.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
                			account.setTimeAdded(new Date());
                			accountMapper.insert(account);	
                		}

//            			if(!jabatans[ke].equals("") && !unitAuditis[ke].equals("")) {
//            				userMapper.deleteAllUnitAuditiUser(account.getId());
//            				//userMapper.insertUnitAuditiUser(userMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT)).get(0), account.getId());
//    						userMapper.insertUnitAuditiUser(unitAuditis[ke], account.getId());
//            				
//            				MappingJabatanUnitAuditi mju = new MappingJabatanUnitAuditi(Utils.getLongNumberID());
//            				mju.setIdJabatan(jabatans[ke]);
//            				mju.setIdAccount(account.getId());
//            				mju.setIdUnitAuditi(unitAuditis[ke]);
//            				mju.setIdSesi(sesiMapper.getEntityActive().getId());
//            				mappingJabatanUnitAuditiMapper.insert(mju);
//            			}
                		resp.setMessage("Data telah berhasil disimpan.");
            			
        			} catch (DuplicateKeyException e) {
        				// TODO: handle exception
        				resp.setCode(HttpStatus.BAD_REQUEST.value());
        				resp.setMessage("Duplication entry for username '"+account.getUsername()+"'");
        				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
        			} catch (Exception e) {
        				resp.setCode(HttpStatus.BAD_REQUEST.value());
        				resp.setMessage("FATAL ERROR!!! Contact administrator");
        				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
        			}
            		
            		
            		if(authlst.size() > 0) {
            			authoritiesMapper.update(authorities);
            		}else {
            			authoritiesMapper.insert(authorities);	
            		}
            		
        			param = new QueryParameter();
        			param.setClause(param.getClause()+" AND "+Control.ACCOUNT_ID+" ='"+account.getId()+"'");
        			param.setClause(param.getClause()+" AND "+Control.SOURCE_ID+" ='"+extractAccountLogin(request, AccountLoginInfo.SOURCE)+"'");
        			controlMapper.deleteBatch(param);
        			
        			Control control = new Control(Utils.getLongNumberID());
        			control.setAccountId(account.getId());
        			control.setSourceId(extractAccountLogin(request, AccountLoginInfo.SOURCE));
        			
        			//2002200 is responden
        			if(authorities.getGroupId().equals("2002200")) {
        				control.setIsAdmin(false);
        			}else {
        				control.setIsAdmin(true);
        			}
        			controlMapper.insert(control);
					//
        			ke++;
    			}
    		}
    		//
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action,
			HttpServletRequest request
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Account data = accountMapper.getEntity(id+extractAccountLogin(request, AccountLoginInfo.SOURCE));
			resp.setData(data);
			accountMapper.delete(data);
			
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Assignment.ACCOUNT_ID + "='"+data.getId()+"'");
			//assignmentMapper.deleteBatch(param);
			
			resp.setMessage("Data '"+data.getUsername()+"' telah dihapus.");
		}
		
		if(action.equals("UPDATE")) {
			String username = request.getParameter("username");
			String firstname = request.getParameter("first_name");
			String lastname = request.getParameter("last_name");
			String email = request.getParameter("email");
			String mobile = request.getParameter("mobile");
			System.out.println("ACTION: "+action);
			System.out.println(username);
			System.out.println("GET ID "+id+extractAccountLogin(request, AccountLoginInfo.SOURCE));
			
			Account data = accountMapper.getEntity(id);
			data.setUsername(username);
			data.setFirstName(firstname);
			data.setLastName(lastname);
			data.setEmail(email);
			data.setMobile(mobile);
			accountMapper.update(data);
			
			resp.setData(data);
			resp.setMessage("Data '"+data.getUsername()+"' telah diupdate.");
		}
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
