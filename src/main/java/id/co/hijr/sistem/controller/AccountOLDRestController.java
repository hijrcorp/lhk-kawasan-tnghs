//package id.co.hijr.sistem.controller;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Optional;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DuplicateKeyException;
//import org.springframework.format.annotation.DateTimeFormat;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import id.co.hijr.sistem.MainApplication;
//import id.co.hijr.sistem.common.BaseController;
//import id.co.hijr.sistem.common.QueryParameter;
//import id.co.hijr.sistem.common.ResponseWrapper;
//import id.co.hijr.sistem.common.ResponseWrapperList;
//import id.co.hijr.sistem.common.Utils;
//import id.co.hijr.sistem.mapper.AccountGroupMapper;
//import id.co.hijr.sistem.mapper.AccountMapper;
//import id.co.hijr.sistem.mapper.ApplicationMapper;
//import id.co.hijr.sistem.mapper.AssignmentMapper;
//import id.co.hijr.sistem.mapper.AuthoritiesMapper;
//import id.co.hijr.sistem.mapper.OrganizationMapper;
//import id.co.hijr.sistem.mapper.PositionMapper;
//import id.co.hijr.sistem.model.Account;
//import id.co.hijr.sistem.model.Application;
//import id.co.hijr.sistem.model.Assignment;
//import id.co.hijr.sistem.model.Authorities;
//import id.co.hijr.sistem.ref.AccountLoginInfoOL;
//import id.co.hijr.sistem.ref.StatusAssignment;
//
//@Controller
//@RequestMapping("/accountOLD")
//public class AccountOLDRestController extends BaseController {
//
//	public final static String DELETE = "delete";
//	
//	@Autowired
//	private AccountMapper accountMapper;
//	
//	@Autowired
//	private PositionMapper positionMapper;
//	
//	@Autowired
//	private OrganizationMapper organizationMapper;
//	
//	@Autowired
//	private AssignmentMapper assignmentMapper;
//	
//	@Autowired
//	private AuthoritiesMapper authoritiesMapper;
//	
//	@Autowired
//	private AccountGroupMapper accountGroupMapper;
//	
//	@Autowired
//	private ApplicationMapper applicationMapper;
//
//	
//	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
//	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> getList(
//    		@RequestParam("limit") Optional<Integer> limit,
//    		@RequestParam("no_offset") Optional<Boolean> noOffset,
//    		@RequestParam("page") Optional<Integer> page,
//    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
//    		HttpServletRequest request
//    		)throws Exception {
//    	
//		QueryParameter param = new QueryParameter();
//		
//	    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
//	    		param.setClause(param.getClause() + " AND (" + Account.FIRST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
//	    		param.setClause(param.getClause() + " OR " + Account.LAST_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
//	    		param.setClause(param.getClause() + " OR " + Account.USERNAME + " LIKE '%"+filterKeyword.get()+"%')");
//	    	}
//	    	
//	    	
//	    	int pPage = 1;
//	    	if(page.isPresent()) {
//	    		pPage = page.get();
//	    		int offset = (pPage-1)*param.getLimit();
//	    		param.setOffset(offset);
//	    		
//	    		if(noOffset.isPresent()) {
//	    			param.setOffset(0);
//	    			param.setLimit(pPage*param.getLimit());
//	    		}
//	    	}
//    	
//    	
//		ResponseWrapperList resp = new ResponseWrapperList();
//		List<Account> data = accountMapper.getList(param);
//		
//		resp.setCount(accountMapper.getCount(param));
//		resp.setData(data);
//		if(noOffset.isPresent()) {
//			resp.setNextMore(data.size() < resp.getCount());
//		}else {
//			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
//		}
//		
//		String qryString = "?page="+(pPage+1);
//		if(limit.isPresent()){
//			qryString += "&limit="+limit.get();
//		}
//		resp.setNextPageNumber(pPage+1);
//		resp.setNextPage(request.getRequestURL().toString()+qryString);
//		
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
//	
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
//	public ResponseEntity<ResponseWrapper> getById(
//			@PathVariable String id,
//			@RequestParam("action") Optional<String> action,
//			HttpServletRequest request
//			) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		
//		Account data = accountMapper.getEntity(id+extractAccountLogin(request, AccountLoginInfoOL.ORGANIZATION));
//		
//		if(action.isPresent()) {
//			if(action.get().toLowerCase().equals(DELETE)) {
//				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getUsername()+"'?");
//			}
//			
//		}
//		
//		resp.setData(data);
//		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//	}
//	
//    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
//	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> save(
//    		@RequestParam("id") Optional<String> id,
//    		@RequestParam("username") Optional<String> username,
//    		@RequestParam("password") Optional<String> password,
//    		@RequestParam("email") Optional<String> email,
//    		@RequestParam("email_status") Optional<Boolean> emailStatus,
//    		@RequestParam("first_name") Optional<String> firstName,
//    		@RequestParam("last_name") Optional<String> lastName,
//    		@RequestParam("mobile") Optional<String> mobile,
//    		@RequestParam("mobile_status") Optional<Boolean> mobileStatus,
//    		@RequestParam("male_status") Optional<Boolean> maleStatus,
//    		@RequestParam("birth_date")  @DateTimeFormat(pattern = "dd-MMM-yyyy") Optional<Date> birthDate,
//    		@RequestParam("enabled") Optional<Boolean> enabled,
//    		@RequestParam("account_non_expired") Optional<Boolean> accountNonExpired,
//    		@RequestParam("credentials_non_expired") Optional<Boolean> credentialsNonExpired,
//    		@RequestParam("account_non_locked") Optional<Boolean> accountNonLocked,
//    		@RequestParam("position_id") Optional<String> positionId,
//    		@RequestParam("organization_id") Optional<String> organizationId,
//    		@RequestParam("group_id") Optional<String> groupId,
//    		@RequestParam("application_id") Optional<String> applicationId,
//    		HttpServletRequest request
//    		) throws Exception {
//    	
//    		ResponseWrapper resp = new ResponseWrapper();
//    		
//    		Account data = new Account(Utils.getLongNumberID());
//    		if(id.isPresent()) {
//    			data.setId(id.get());
//    			data = accountMapper.getEntity(id.get());
//    		}
//    		if(data == null) {
//    			resp.setCode(HttpStatus.BAD_REQUEST.value());
//    			resp.setMessage("Data tidak ditemukan.");
//    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    		}
//    		
////    		Thread.sleep(1000);
//    		
//    		if(id.isPresent()) data.setId(id.get());
//    		if(username.isPresent()) data.setUsername(username.get());
//    		if(password.isPresent()) data.setPassword(password.get());
//    		if(email.isPresent()) data.setEmail(email.get());
//    		if(emailStatus.isPresent()) data.setEmailStatus(emailStatus.get());
//    		if(firstName.isPresent()) data.setFirstName(firstName.get());
//    		if(lastName.isPresent()) data.setLastName(lastName.get());
//    		if(mobile.isPresent()) data.setMobile(mobile.get());
//    		if(mobileStatus.isPresent()) data.setMobileStatus(mobileStatus.get());
//    		if(maleStatus.isPresent()) data.setMaleStatus(maleStatus.get());
//    		if(birthDate.isPresent()) data.setBirthDate(birthDate.get());
//    		// KHUSUS, karena di formnya di balik
//    		if(enabled.isPresent()) {
//    			data.setEnabled(false);
//    		}else {
//    			data.setEnabled(true);
//    		}
//    		if(accountNonExpired.isPresent()) data.setAccountNonExpired(accountNonExpired.get());
//    		if(credentialsNonExpired.isPresent()) data.setCredentialsNonExpired(credentialsNonExpired.get());
//    		if(accountNonLocked.isPresent()) data.setAccountNonLocked(accountNonLocked.get());
//    		
//    		boolean pass = true;
//    		if( data.getUsername() == null || data.getFirstName() == null || data.getLastName() == null) {
//    			pass = false;
//    		}else {
//    			if(data.getUsername().equals("") || data.getFirstName().equals("") || data.getLastName().equals("")) {
//        			pass = false;
//        		}
//    		}
//    		
//    		Authorities authorities = null;
//    		if(groupId.isPresent() && applicationId.isPresent() && !groupId.get().equals("") && !applicationId.get().equals("")) {
//    			authorities = new Authorities(Utils.getLongNumberID());
//    			
//    			
//        		
//    			QueryParameter param = new QueryParameter();
//        		param.setClause(param.getClause() + " AND (" + Application.ID + "='"+applicationId.get()+"'");
//        		param.setClause(param.getClause() + " OR " + Application.CODE + "='"+applicationId.get()+"')");
//        		
//        		List<Application> lst = applicationMapper.getList(param);
//        		if(lst.size() > 0) {
//        			Application app = lst.get(0);
//        			
//        			
//        			QueryParameter authparam = new QueryParameter();
//        			authparam.setClause(authparam.getClause() + " AND " + Authorities.ACCOUNT_ID + "='"+data.getId()+"'");
//        			authparam.setClause(authparam.getClause() + " AND " + Authorities.APPLICATION_ID + "='"+app.getId()+"'");
//            		List<Authorities> authlst = authoritiesMapper.getList(authparam);
//            		if(authlst.size() > 0) {
//            			authorities = authlst.get(0);
//            		}
//            		
//            		if(positionId.isPresent()) {
//            			authorities.setPosition(positionMapper.getEntity(positionId.get()));
//            		}
//        			
//        			authorities.setAccountId(data.getId());
//            		authorities.setAccount(data);
//            		authorities.setGroup(accountGroupMapper.getEntity(groupId.get()));
//            		authorities.setGroupId(groupId.get());
//            		authorities.setApplication(app);
//            		authorities.setApplicationId(app.getId());
//            		
//            		authorities.setPersonAdded(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
//            		authorities.setTimeAdded(new Date());
//            		
//            		if(authlst.size() > 0) {
//            			authoritiesMapper.update(authorities);
//            		}else {
//            			authoritiesMapper.insert(authorities);	
//            		}
//        			
//        		}else {
//        			pass = false;
//        		}
//    			
//    		}
//    		
//    		Assignment assignment = null;
//    		if(positionId.isPresent() && !positionId.get().equals("")) {
//    			assignment = new Assignment(Utils.getLongNumberID());
//    			
//        		String parentId = extractAccountLogin(request, AccountLoginInfoOL.ORGANIZATION);
//        		if(organizationId.isPresent() && !organizationId.get().equals("")) {
//        			parentId = organizationId.get();
//        		}
//        		
//        		QueryParameter param = new QueryParameter();
//        		param.setClause(param.getClause() + " AND " + Assignment.ACCOUNT_ID + "='"+data.getId()+"'");
//        		param.setClause(param.getClause() + " AND " + Assignment.PARENT_ID + "='"+extractAccountLogin(request, AccountLoginInfoOL.ORGANIZATION)+"'");
//        		param.setClause(param.getClause() + " AND " + Assignment.STATUS + "='"+StatusAssignment.PERMANENT.name()+"'");
//        		param.setClause(param.getClause() + " AND " + Assignment.ACTIVE + "=1");
//        		List<Assignment> lst = assignmentMapper.getList(param);
//        		if(lst.size() > 0) {
//        			assignment = lst.get(0);
//        		}
//        		
//    			
//        		assignment.setAccountId(data.getId());
//        		assignment.setAccount(data);
//        		assignment.setPosition(positionMapper.getEntity(positionId.get()));
//        		assignment.setPositionId(positionId.get());
//        		assignment.setOrganizationId(parentId);
//        		assignment.setOrganization(organizationMapper.getEntity(parentId));
//        		assignment.setParentId(extractAccountLogin(request, AccountLoginInfoOL.ORGANIZATION));
//        		assignment.setStatus(StatusAssignment.PERMANENT.name());
//        		assignment.setActive(true);
//        		
//        		if(lst.size() > 0) {
//        			assignmentMapper.update(assignment);
//        		}else {
//        			assignmentMapper.insert(assignment);
//        		}
//        		
//        		
//    		}else {
//    			pass = false;
//    		}
//
//    		if(!pass) {
//    			resp.setCode(HttpStatus.BAD_REQUEST.value());
//    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
//    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//    		}
//    		
//    		try {
//    			if(id.isPresent()) {
//    				data.setPersonModified(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
//    				data.setTimeModified(new Date());
//    				
//        			accountMapper.update(data);
//        		}else {
//        			data.setPersonAdded(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
//    				data.setTimeAdded(new Date());
//    				
//        			accountMapper.insert(data);	
//        		}
//		} catch (DuplicateKeyException e) {
//			// TODO: handle exception
//			resp.setCode(HttpStatus.BAD_REQUEST.value());
//			resp.setMessage("Duplication entry for username '"+data.getUsername()+"'");
//			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//		} catch (Exception e) {
//			resp.setCode(HttpStatus.BAD_REQUEST.value());
//			resp.setMessage("FATAL ERROR!!! Contact administrator");
//			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//		}
//    		
//    		
//    		if(authorities != null) {
//    			resp.setData(authorities);
//    		}else if(assignment != null) {
//    			resp.setData(assignment);
//    		}else {
//    			resp.setData(data);
//    		}
//    		resp.setMessage("Data telah berhasil disimpan.");
//   
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
//    
//   
//
//    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
//	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
//    public ResponseEntity<ResponseWrapper> doAction(
//			@PathVariable String id,
//			@PathVariable String action,
//			HttpServletRequest request
//    		) throws Exception {
//		ResponseWrapper resp = new ResponseWrapper();
//		
//		if(action.equals(DELETE)) {
//			
//			Account data = accountMapper.getEntity(id+extractAccountLogin(request, AccountLoginInfoOL.ORGANIZATION));
//			resp.setData(data);
//			accountMapper.delete(data);
//			
//			QueryParameter param = new QueryParameter();
//			param.setClause(param.getClause() + " AND " + Assignment.ACCOUNT_ID + "='"+data.getId()+"'");
//			assignmentMapper.deleteBatch(param);
//			
//			resp.setMessage("Data '"+data.getUsername()+"' telah dihapus.");
//		}
//		
//		
//		
//        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
//    }
//    
//
//}
