package id.co.hijr.sistem.service;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestQLTriggerService {

//	@Autowired
//	private JabatanManajerialMapper jabatanManajerialMapper;
//
//	@Autowired
//	private PertanyaanJawabanMapper pertanyaanJawabanMapper;
//
//	@Autowired
//	private JabatanKompetensiTeknisMapper jabatanKompetensiTeknisMapper;
	
	public void onAfterInsert(String entity, Map<String, Object> data) {
		System.out.println("onAfterInsert: >> call another service method: " + data.get("id"));
		
	}
	
	public void onAfterUpdate(String entity, Map<String, Object> data, Map<String, Object> prev) {
		System.out.println("onAfterUpdate: >> call another service method");
		System.out.println(prev.get("alamat_rumah") + " >> DIFF << " + data.get("alamat_rumah"));
	}
	
	public void onAfterDelete(String entity, Map<String, Object> data) {
//		System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
//		if(entity.equals("jabatan")) {
//			QueryParameter param = new QueryParameter();
//			param.setClause(param.getClause()+" AND "+JabatanManajerial.ID_MASTER_JABATAN+" ='"+data.get("id")+"'");
//			jabatanManajerialMapper.deleteBatch(param);
//		}
//		if(entity.equals("pertanyaan")) {
//			QueryParameter param = new QueryParameter();
//			param.setClause(param.getClause()+" AND "+PertanyaanJawaban.ID_HEADER+" ='"+data.get("id")+"'");
//			pertanyaanJawabanMapper.deleteBatch(param);
//		}
	}
	
	public boolean onCheck(String entity, Map<String, Object> data) {
		//System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
		boolean pass=true;
		if(entity.equals("unit_kompetensi_teknis")) {
			if(data.get("kode").toString().equals("")) {
				pass=false;
			}
		}
		if(entity.equals("kelompok_jabatan")) {
			if(data.get("nama").toString().equals("")) {
				pass=false;
			}
		}
		if(entity.equals("tingkat_jabatan")) {
			if(data.get("nama").toString().equals("")) {
				pass=false;
			}
		}
		if(entity.equals("jabatan_kompetensi_teknis")) {
			if(data.get("id_master_jabatan").toString().equals("") || data.get("id_unit_kompetensi_teknis").toString().equals("")) {
				pass=false;
			}
		}
		return pass;
	}
	
	public boolean onNewCheck(String[] data) {
		boolean pass=true;
		//dataMainColumn
		for(String column : data) {
			if(column=="") pass=false;
		}
		return pass;
	}
	
	public boolean onCheckDuplicate(String entity, Map<String, Object> data) {
		//System.out.println("onAfterDelete: >> call another service method: " + data.get("id"));
		boolean pass=true;
//		if(entity.equals("jabatan_kompetensi_teknis")) {
//			QueryParameter param = new QueryParameter();
//			param.setClause(param.getClause()+" AND "+JabatanKompetensiTeknis.ID_MASTER_JABATAN+" ='"+data.get("id_master_jabatan").toString()+"'");
//			param.setClause(param.getClause()+" AND "+JabatanKompetensiTeknis.ID_BIDANG_TEKNIS+" ='"+data.get("id_bidang_teknis").toString()+"'");
//			param.setClause(param.getClause()+" AND "+JabatanKompetensiTeknis.ID_UNIT_KOMPETENSI_TEKNIS+" ='"+data.get("id_unit_kompetensi_teknis").toString()+"'");
//			if(jabatanKompetensiTeknisMapper.getCount(param) > 0) {
//			//data.get("id_master_jabatan").toString().equals("") || data.get("id_kelompok_teknis").toString().equals("") || data.get("id_unit_kompetensi_teknis").toString().equals("")) 
//				pass=false;
//			}
//		}
		return pass;
	}
	
}
