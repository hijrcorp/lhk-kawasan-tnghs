package id.co.hijr.sistem.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.vault.core.VaultOperations;
import org.springframework.vault.core.VaultTransitOperations;
import org.springframework.vault.support.VaultTransitKeyCreationRequest;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class VaultService {
	private static final String KEY_TYPE = "aes128-gcm96";
    private static final String KEY_ENCRYPT_KTP = "KEY_ENCRYPT_KTP";

    private VaultTransitOperations vaultTransit;

    public VaultService(VaultOperations vaultOperations) {
        vaultTransit = vaultOperations.opsForTransit();
        vaultTransit.createKey(KEY_ENCRYPT_KTP,
                VaultTransitKeyCreationRequest.ofKeyType(KEY_TYPE));
    }
    
    //string
    public String encrypt(String plaintext) {
        return vaultTransit.encrypt(KEY_ENCRYPT_KTP, plaintext);
    } 
    public String decrypt(String cipherText) {
    	String s=null;
    	try {
    		s = vaultTransit.decrypt(KEY_ENCRYPT_KTP, cipherText);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
        return s;
    }
    //end string
    
    //file
    public File encrypt(File plainFile) {
    	try {
    		String base64Encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(plainFile));
    		String base64Encrypted = encrypt(base64Encoded);
    		File result = File.createTempFile(plainFile.getName()+"%", "-enc.txt");
    		FileUtils.writeStringToFile(result, base64Encrypted, StandardCharsets.UTF_8, false);
    		return result;
    	} catch(IOException e) {
    		//e.getMessage(), e
    		e.printStackTrace();
    	}
    	return null;
    }
    public File decrypt(File chiperFile) {
    	try {
    		String cipherFileContent = FileUtils.readFileToString(chiperFile, StandardCharsets.UTF_8);
    		String base64Encoded = decrypt(cipherFileContent);
    		File result = File.createTempFile(UUID.randomUUID().toString(), ".png");
    		byte[] decryptedFileContent = Base64.getDecoder().decode(base64Encoded);
    		FileUtils.writeByteArrayToFile(result, decryptedFileContent);
    		return result;
    	} catch(IOException e) {
    		//e.getMessage(), e
    		e.printStackTrace();
    	}
    	return null;
    }
    //end file
    

    //bytes
    public String encrypt(byte[] fileContent) {
        return encrypt(Base64.getEncoder().encodeToString(fileContent));
    } 
    public byte[] decryptFile(String encryptedFileContent) {
        return Base64.getDecoder().decode(decrypt(encryptedFileContent));
    }
    //end string
}
