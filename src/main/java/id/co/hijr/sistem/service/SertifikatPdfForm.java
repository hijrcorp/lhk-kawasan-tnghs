package id.co.hijr.sistem.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;

 
/**
 *
 * @author iText
 */
public class SertifikatPdfForm extends PdfPageEventHelper {
    // initialized in constructor
    protected PdfReader reader;
    protected Rectangle pageSize;
    protected Rectangle body;
    protected float mLeft, mRight, mTop, mBottom;
    protected Rectangle to;
    protected Rectangle from;
    protected Rectangle date;
    protected Rectangle footer;
    protected BaseFont basefont;
    protected Font font;
    // data


	protected Rectangle jalur;
	protected Rectangle namaPendaki;
	protected Rectangle tglNaik;
	protected Rectangle codeBook;
    
    // initialized upon opening the document
    protected PdfTemplate background;
    protected PdfTemplate total;

    protected Purchase purchase;
    protected List<PurchaseDetilVisitor> listDetailPurchaseDetilVisitor;
    protected String strJalur, strNamaPendaki, strTglNaik, strCodeBooking;
    

    public SertifikatPdfForm(
    		String AA, 
    		Integer MAXIMAL_LIST, 
    		String HTML, 
    		Purchase purchase, 
    		List<PurchaseDetilVisitor>listDetailPurchaseDetilVisitor
    		) throws IOException, DocumentException {

		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		Locale myIndonesianLocale = new Locale("in", "ID");
		formatter.setCurrency(Currency.getInstance(myIndonesianLocale));
		//untuk format tanggal atau waktu
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", myIndonesianLocale);

    	this.purchase = purchase;
    	this.listDetailPurchaseDetilVisitor = purchase.getPurchaseDetilVisitor();//listDetailPurchaseDetilVisitor;
    	this.strJalur = purchase.getRegion().getName().toString().toUpperCase();
    	this.strTglNaik = sdf.format(purchase.getStartDate());
    	this.strNamaPendaki = "strNamaPendaki";
    	this.strCodeBooking = purchase.getCodeBooking();
    	
        reader = new PdfReader(AA);
        AcroFields fields = reader.getAcroFields();
        pageSize = reader.getPageSize(1);
        body = fields.getFieldPositions("body").get(0).position;
        mLeft = body.getLeft() - pageSize.getLeft();
        mRight = pageSize.getRight() - body.getRight();
        mTop = pageSize.getTop() - body.getTop();
        mBottom = body.getBottom() - pageSize.getBottom();
        System.out.println("fields.getFields() "+fields.getFields());
        System.out.println("fields.getFields().tanggalPendakian "+fields.getField("tanggalPendakian"));
        System.out.println("fields.getFields().jalur "+fields.getFieldPositions("jalur").get(0));
        jalur = fields.getFieldPositions("jalur").get(0).position;
        tglNaik = fields.getFieldPositions("tglNaik").get(0).position;
        codeBook = fields.getFieldPositions("codeBook").get(0).position;
    	
        
        basefont = BaseFont.createFont();
        font = new Font(basefont, 13);
		
        System.out.println("DISINI : ");
        
		File html = new File(HTML);
		html.getParentFile().mkdirs();
		PrintWriter writer = new PrintWriter(HTML, "UTF-8");
		writer.println("<html>");
		writer.println("<head><title>Form </title></head>");
		writer.println("<body>");
		//writer.println("<table border='0' width='100%' style='color:#343a40'>");
		//writer.println("<tr><td class='td0'></td></tr>");
//		/*if(listDetailSatwaPermohonan.size() <= MAXIMAL_LIST_IN_AA) {*/
			int i = 1;
			for(PurchaseDetilVisitor det: purchase.getPurchaseDetilVisitor()){
				
//				DetilSatwaPermohonan dsp = det.getDetilSatwaYangDiperiksa();
//				
//				String satwa = dsp.getNamaSatwaPermohonan();
//				if(satwa.length() > 28) satwa = satwa.substring(0, 25) + "...";
//				
//				String latin = dsp.getNamaLatinSatwaPermohonan();
//				if(latin.length() > 30) latin = latin.substring(0, 27) + "...";
//				
//				String inggris = dsp.getNamaInggrisSatwaPermohonan();
//				if(inggris.length() > 30) inggris = inggris.substring(0, 27) + "...";
//				
//				String jumlah = det.getJumlahSatwaYangDiperiksa()+" "+dsp.getSatuanJumlahSatwaPermohonan();
//				if(jumlah.length() > 15) jumlah = jumlah.substring(0, 13) + "...";
//				
//				String desc = det.getKeteranganSatwaYangDiperiksa();
//				if(desc != null && desc.length() > 23) desc = desc.substring(0, 20) + "...";
//				else if(desc == null) desc = "";
				if(det.getResponsible().equals("0")) {
					writer.println("<p>");
					writer.println(" <h4 align='center' style='font-style: italic;'>"+det.getVisitorIdentity().getFullName().toString().toUpperCase()+"</h4>");
					writer.println("</p>");
				}
			}
//		/*}*/
		//writer.println("</table>");
//		if(listDetailSatwaPermohonan.size() > MAXIMAL_LIST_IN_AA) {
//			writer.println("<div class=\"terlampir\">(Terlampir)</div>");
//		}
		writer.println("</body>");
		writer.println("</html>");
		writer.close();
        
    }
 
    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        background = writer.getImportedPage(reader, 1);
        total = writer.getDirectContent().createTemplate(30, 15);
        Calendar c = Calendar.getInstance();
        c.set(2015, 9, 13);
        
        
    }
    
    public String getMonth(Date date){
    	try{ return new SimpleDateFormat("MM").format(date); }
    	catch(Exception e){ return ""; }
    }
    
    public String getYear(Date date){
    	try{ return new SimpleDateFormat("yyyy").format(date); }
    	catch(Exception e){ return ""; }
    }
    
    public String getFormattedDate(Date date){
    	try{ return new SimpleDateFormat("MMMM yyyy").format(date); }
    	catch(Exception e){ return ""; }
    }
    
    public String getFormattedDateMasaBerlaku(Date date){
    	try{ return new SimpleDateFormat("-MM-yyyy").format(date); }
    	catch(Exception e){ return ""; }
    }
 
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContentUnder();
        // background
        canvas.addTemplate(background, 0, 0);
        try {
            ColumnText ct = new ColumnText(canvas);
            // footer (page X of Y)
            //ct.setSimpleColumn(footer);
            //ct.addText(new Chunk("page " + writer.getPageNumber(), font));
            //ct.addText(new Chunk(Image.getInstance(total), 0, 0));
            //sct.go();
            
            /*
            basefont = BaseFont.createFont();
            font = new Font(basefont, 13);
            */
            // set konten
            //if(strJalur.length() == 4) strJalur = "         "+strJalur;
            ct.setSimpleColumn(jalur);
            Paragraph p = new Paragraph(strJalur, new Font(basefont, 23));
            p = new Paragraph(strJalur, new
           		 Font(FontFamily.HELVETICA, 23, Font.BOLDITALIC) );
            p.setAlignment(Element.ALIGN_CENTER);
            ct.addElement(p);
            ct.go();

            ct.setSimpleColumn(tglNaik);
            p = new Paragraph(strTglNaik, new
            		 Font(FontFamily.HELVETICA, 20, Font.BOLDITALIC) ); // new BaseColor(255, 165, 0)
            p.setAlignment(Element.ALIGN_CENTER);
            ct.addElement(p);
            //ct.addElement(new Paragraph(strTglNaik, new Font(basefont, 19)));
            //ct.setAlignment(Element.ALIGN_CENTER);
            ct.go();
            
            ct.setSimpleColumn(codeBook);
            ct.addElement(new Paragraph(strCodeBooking, new Font(basefont, 14)));
            ct.setAlignment(Element.ALIGN_CENTER);
            ct.go();

        } catch (DocumentException e) {
            // can never happen, but if it does, we want to know!
            throw new ExceptionConverter(e);
        }
    }
 
    @Override
    public void onCloseDocument(PdfWriter writer, Document document) {
        // we only know the total number of pages at the moment the document is closed.
        String s = "/" + (writer.getPageNumber() - 1);
        Phrase p = new Phrase(12, s, font);
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, p, 0.5f, 0, 0);
    }
 
    public static ElementList parseHtml(String content, String style, TagProcessorFactory tagProcessors) throws IOException {
        // CSS
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        CssFile cssFile = XMLWorkerHelper.getCSS(new FileInputStream(style));
        cssResolver.addCss(cssFile);
        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(tagProcessors);
        htmlContext.autoBookmark(false);
        // Pipelines
        ElementList elements = new ElementList();
        ElementHandlerPipeline end = new ElementHandlerPipeline(elements, null);
        HtmlPipeline html = new HtmlPipeline(htmlContext, end);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
        // XML Worker
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser p = new XMLParser(worker);
 
        p.parse(new FileInputStream(content),Charset.forName("cp1252"));
        return elements;
    }
 
    public Rectangle getPageSize() {
        return pageSize;
    }
 
    public float getmLeft() {
        return mLeft;
    }
 
    public float getmRight() {
        return mRight;
    }
 
    public float getmTop() {
        return mTop;
    }
 
    public float getmBottom() {
        return mBottom;
    }
 
    public Rectangle getBody() {
        return body;
    }
}