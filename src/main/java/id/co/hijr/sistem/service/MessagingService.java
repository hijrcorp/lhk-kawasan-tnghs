package id.co.hijr.sistem.service;

import java.util.Map;

public interface MessagingService {

	public void send(String destination, Map<String,Object> payload)  throws Exception;

}
