package id.co.hijr.sistem.service;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

	public void store(MultipartFile file, String filename) throws Exception;
	public void storeWithEncrypted(MultipartFile file, String filename, String filenameWithoutExt, boolean isMultiple) throws Exception;
	public void storeWithEncrypted(MultipartFile file, String filename, String filenameWithoutExt) throws Exception;
	public File load(String filename) throws Exception;
	public File loadWithDecrypt(String filename) throws Exception;
	public void delete(String filename) throws Exception;
}
