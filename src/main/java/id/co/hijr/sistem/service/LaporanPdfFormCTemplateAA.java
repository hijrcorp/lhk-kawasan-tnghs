package id.co.hijr.sistem.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;

 
/**
 *
 * @author iText
 */
public class LaporanPdfFormCTemplateAA extends PdfPageEventHelper {
    // initialized in constructor
    protected PdfReader reader;
    protected Rectangle pageSize;
    protected Rectangle body;
    protected float mLeft, mRight, mTop, mBottom;
    protected Rectangle to;
    protected Rectangle from;
    protected Rectangle date;
    protected Rectangle footer;
    protected BaseFont basefont;
    protected Font font;
    // data


	protected Rectangle jalur;
	protected Rectangle tanggalPendakian;
	protected Rectangle transitCamp;
	protected Rectangle reportTime;
	protected Rectangle codeUnique;
	protected Rectangle countTicket;
	protected Rectangle namaKetua;
	protected Rectangle jumlahPembayaran;
	protected Rectangle codeBooking;
	protected Rectangle idBooking;
	protected Rectangle emailAdmin;
	protected Rectangle whatsappAdmin;
    
    // initialized upon opening the document
    protected PdfTemplate background;
    protected PdfTemplate total;

    protected Purchase purchase;
    protected List<PurchaseDetilLuggage> listDetailPurchaseDetilLuggage;
    protected String strJalur, strTanggalPendakian, strTransitCamp, strReportTime, strCodeUnique, strCountTicket, strNamaKetua, strJumlahPembayaran, strCodeBooking, strIdBooking, strWhatsappAdmin, strEmailAdmin;
    
    public LaporanPdfFormCTemplateAA(
    		String AA, 
    		Integer MAXIMAL_LIST_IN_AA, 
    		String HTML_PERMOHONAN_SATWA, 
    		Purchase purchase, 
    		List<PurchaseDetilLuggage>listDetilLuggage,
    		Purchase purchaseNullNoParent, 
    		Purchase purchaseNotNullNoParent
    		) throws IOException, DocumentException {

		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		Locale myIndonesianLocale = new Locale("in", "ID");
		formatter.setCurrency(Currency.getInstance(myIndonesianLocale));
		//untuk format tanggal atau waktu
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy", myIndonesianLocale);

    	this.purchase = purchase;
    	this.listDetailPurchaseDetilLuggage = listDetilLuggage;
    	this.strJalur = ": "+purchase.getRegion().getName() + " - " + purchase.getRegionEndName();
    	this.strTanggalPendakian = ": "+sdf.format(purchase.getStartDateSerialize());
    	
    	 try {
        	purchase.getPurchaseDetilTransitCamp().get(0);
		} catch (Exception e) {
			List<PurchaseDetilTransitCamp> pdtc = new ArrayList<>();
			PurchaseDetilTransitCamp p = new PurchaseDetilTransitCamp();
			p.setCodeUnique("");
			p.setNameTransitCamp("-");
			p.setReportTimeCamp("-");
			pdtc.add(p);
			purchase.setPurchaseDetilTransitCamp(pdtc);
			// TODO: handle exception
		}
    	
    	if(purchase.getPurchaseDetilTransitCamp().size()>0) {
    		this.strTransitCamp = ": "+purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp();
    		this.strReportTime = ": "+purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp();
    		String strCodeUnique = purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique();
    		this.strCodeUnique = ": "+(strCodeUnique.equals("")?"-":strCodeUnique);
    	}else {
    		//purchase.setPurchaseDetilVisitor(purchaseNotNullNoParent.getPurchaseDetilVisitor());
			purchase.setPurchaseDetilTransitCamp(purchaseNotNullNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
         	
    		this.strTransitCamp = ": "+purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp();
    		this.strReportTime = ": "+purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp();
    		String strCodeUnique = purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique();
    		this.strCodeUnique = ": "+(strCodeUnique.equals("")?"-":strCodeUnique);
    	}

        String ketuaTim="";
         if(purchase.getParentId()!=null){
			
			//manipulate this for childeren purchase cause the parent have what this need
			//purchase.setPurchaseDetilVisitor(purchaseNullNoParent.getPurchaseDetilVisitor());
			purchase.setPurchaseDetilTransitCamp(purchaseNullNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
         	

        	ketuaTim=purchaseNotNullNoParent.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName();
		}else{
        	ketuaTim=purchase.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName();
         }
    	this.strCountTicket = ": "+purchase.getCountTicket();
    	
    	this.strNamaKetua = ": "+ketuaTim;
    	
    	this.strJumlahPembayaran = ": "+formatter.format(purchase.getAmountTicket());
    	this.strCodeBooking = ": "+purchase.getCodeBooking();
    	this.strIdBooking = purchase.getId();
    	this.strWhatsappAdmin = purchase.getWhatsappAdmin();
    	this.strEmailAdmin = purchase.getEmailAdmin();
    	
        reader = new PdfReader(AA);
        AcroFields fields = reader.getAcroFields();
        pageSize = reader.getPageSize(1);
        body = fields.getFieldPositions("body").get(0).position;
        mLeft = body.getLeft() - pageSize.getLeft();
        mRight = pageSize.getRight() - body.getRight();
        mTop = pageSize.getTop() - body.getTop();
        mBottom = body.getBottom() - pageSize.getBottom();
        
        jalur = fields.getFieldPositions("jalur").get(0).position;
        tanggalPendakian = fields.getFieldPositions("tanggalPendakian").get(0).position;
        transitCamp = fields.getFieldPositions("transitCamp").get(0).position;
        reportTime = fields.getFieldPositions("reportTime").get(0).position;
        codeUnique = fields.getFieldPositions("codeUnique").get(0).position;
        countTicket = fields.getFieldPositions("countTicket").get(0).position;
        namaKetua = fields.getFieldPositions("namaKetua").get(0).position;
        jumlahPembayaran = fields.getFieldPositions("jumlahPembayaran").get(0).position;
        codeBooking = fields.getFieldPositions("codeBooking").get(0).position;
        idBooking = fields.getFieldPositions("idBooking").get(0).position;
        whatsappAdmin = fields.getFieldPositions("whatsappAdmin").get(0).position;
        emailAdmin = fields.getFieldPositions("emailAdmin").get(0).position;
    	
        
        basefont = BaseFont.createFont();
        font = new Font(basefont, 13);
		
		File html = new File(HTML_PERMOHONAN_SATWA);
		html.getParentFile().mkdirs();
		PrintWriter writer = new PrintWriter(HTML_PERMOHONAN_SATWA, "UTF-8");
		writer.println("<html>");
		writer.println("<head><title>Form </title></head>");
		writer.println("<body>");
		writer.println("<table border='0' width='100%' style='color:#343a40'>");
		writer.println("<tr><td class='td0'></td></tr>");
//		/*if(listDetailSatwaPermohonan.size() <= MAXIMAL_LIST_IN_AA) {*/
			int i = 1;
			for(PurchaseDetilVisitor det: purchase.getPurchaseDetilVisitor()){
				
//				DetilSatwaPermohonan dsp = det.getDetilSatwaYangDiperiksa();
//				
//				String satwa = dsp.getNamaSatwaPermohonan();
//				if(satwa.length() > 28) satwa = satwa.substring(0, 25) + "...";
//				
//				String latin = dsp.getNamaLatinSatwaPermohonan();
//				if(latin.length() > 30) latin = latin.substring(0, 27) + "...";
//				
//				String inggris = dsp.getNamaInggrisSatwaPermohonan();
//				if(inggris.length() > 30) inggris = inggris.substring(0, 27) + "...";
//				
//				String jumlah = det.getJumlahSatwaYangDiperiksa()+" "+dsp.getSatuanJumlahSatwaPermohonan();
//				if(jumlah.length() > 15) jumlah = jumlah.substring(0, 13) + "...";
//				
//				String desc = det.getKeteranganSatwaYangDiperiksa();
//				if(desc != null && desc.length() > 23) desc = desc.substring(0, 20) + "...";
//				else if(desc == null) desc = "";
				if(det.getResponsible().equals("0")) {
				writer.println("<tr>");
				writer.println(" <td class='td1' valign='top'><div class='div1'>"+(i++)+".</div></td>");
				writer.println(" <td class='td2' valign='top'><div class='div2'>"+det.getVisitorIdentity().getNoIdentity()+"</div></td>");
				writer.println(" <td class='td3' valign='top'><div class='div3'>"+det.getVisitorIdentity().getFullName()+"</div></td>");
				writer.println(" <td class='td4' valign='top'><div class='div4'>"+(det.getVisitorIdentity().getGender().equals("L")?"LAKI-LAKI":"PEREMPUAN")+"</div></td>");
				writer.println(" <td class='td5' valign='top'><div class='div5'>"+det.getPhoneNumber()+"</div></td>");
//				writer.println(" <td class='td6' valign='top'><div class='div5'>"+desc+"</div></td>");
				writer.println("</tr>");
				}
			}
//		/*}*/
		writer.println("</table>");
//		if(listDetailSatwaPermohonan.size() > MAXIMAL_LIST_IN_AA) {
			//writer.println("<div class=\"terlampir\">(Terlampir)</div>");
//		}
		writer.println("</body>");
		writer.println("</html>");
		writer.close();
        
    }
 
    @Override
    public void onOpenDocument(PdfWriter writer, Document document) {
        background = writer.getImportedPage(reader, 1);
        total = writer.getDirectContent().createTemplate(30, 15);
        Calendar c = Calendar.getInstance();
        c.set(2015, 9, 13);
        
        
    }
    
    public String getMonth(Date date){
    	try{ return new SimpleDateFormat("MM").format(date); }
    	catch(Exception e){ return ""; }
    }
    
    public String getYear(Date date){
    	try{ return new SimpleDateFormat("yyyy").format(date); }
    	catch(Exception e){ return ""; }
    }
    
    public String getFormattedDate(Date date){
    	try{ return new SimpleDateFormat("MMMM yyyy").format(date); }
    	catch(Exception e){ return ""; }
    }
    
    public String getFormattedDateMasaBerlaku(Date date){
    	try{ return new SimpleDateFormat("-MM-yyyy").format(date); }
    	catch(Exception e){ return ""; }
    }
 
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContentUnder();
        // background
        canvas.addTemplate(background, 0, 0);
        try {
            ColumnText ct = new ColumnText(canvas);
            // footer (page X of Y)
            //ct.setSimpleColumn(footer);
            //ct.addText(new Chunk("page " + writer.getPageNumber(), font));
            //ct.addText(new Chunk(Image.getInstance(total), 0, 0));
            //sct.go();

            // set konten
            ct.setSimpleColumn(jalur);
            ct.addElement(new Paragraph(strJalur, font));
            ct.go();

            ct.setSimpleColumn(tanggalPendakian);
            ct.addElement(new Paragraph(strTanggalPendakian, font));
            ct.go();

            ct.setSimpleColumn(transitCamp);
            ct.addElement(new Paragraph(strTransitCamp, font));
            ct.go();

            ct.setSimpleColumn(reportTime);
            ct.addElement(new Paragraph(strReportTime, font));
            ct.go();

            ct.setSimpleColumn(codeUnique);
            ct.addElement(new Paragraph(strCodeUnique, font));
            ct.go();

            ct.setSimpleColumn(countTicket);
            ct.addElement(new Paragraph(strCountTicket, font));
            ct.go();

            ct.setSimpleColumn(namaKetua);
            ct.addElement(new Paragraph(strNamaKetua, font));
            ct.go();

            ct.setSimpleColumn(jumlahPembayaran);
            ct.addElement(new Paragraph(strJumlahPembayaran, font));
            ct.go();

            ct.setSimpleColumn(codeBooking);
            ct.addElement(new Paragraph(strCodeBooking, font));
            ct.go();

            ct.setSimpleColumn(idBooking);
            ct.addElement(new Paragraph(strIdBooking, new Font(basefont, 9)));
            ct.go();

            ct.setSimpleColumn(whatsappAdmin);
            ct.addElement(new Paragraph(strWhatsappAdmin, new Font(basefont, 9)));
            ct.go();

            ct.setSimpleColumn(emailAdmin);
            ct.addElement(new Paragraph(strEmailAdmin, new Font(basefont, 9)));
            ct.go();

           
        } catch (DocumentException e) {
            // can never happen, but if it does, we want to know!
            throw new ExceptionConverter(e);
        }
    }
 
    @Override
    public void onCloseDocument(PdfWriter writer, Document document) {
        // we only know the total number of pages at the moment the document is closed.
        String s = "/" + (writer.getPageNumber() - 1);
        Phrase p = new Phrase(12, s, font);
        ColumnText.showTextAligned(total, Element.ALIGN_LEFT, p, 0.5f, 0, 0);
    }
 
    public static ElementList parseHtml(String content, String style, TagProcessorFactory tagProcessors) throws IOException {
        // CSS
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        CssFile cssFile = XMLWorkerHelper.getCSS(new FileInputStream(style));
        cssResolver.addCss(cssFile);
        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(tagProcessors);
        htmlContext.autoBookmark(false);
        // Pipelines
        ElementList elements = new ElementList();
        ElementHandlerPipeline end = new ElementHandlerPipeline(elements, null);
        HtmlPipeline html = new HtmlPipeline(htmlContext, end);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
        // XML Worker
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser p = new XMLParser(worker);
 
        p.parse(new FileInputStream(content),Charset.forName("cp1252"));
        return elements;
    }
 
    public Rectangle getPageSize() {
        return pageSize;
    }
 
    public float getmLeft() {
        return mLeft;
    }
 
    public float getmRight() {
        return mRight;
    }
 
    public float getmTop() {
        return mTop;
    }
 
    public float getmBottom() {
        return mBottom;
    }
 
    public Rectangle getBody() {
        return body;
    }
}