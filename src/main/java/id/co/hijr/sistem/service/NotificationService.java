package id.co.hijr.sistem.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import id.co.hijr.sistem.common.Utils;


@Service
//implements UserDetailsService
public class NotificationService  {
	
	@Value("${app.organization.id}")
	private String organizationId;
	
	@Value("${app.organization.code}")
	private String organizationCode;
    
    @Value("${app.folder.dev}")
    private String appFolderDev;
    
    @Value("${app.folder.production}")
    private String appFolderProd;
    
	@Value("${app.url.ntfc}")
    private String appUrlNtfc;
	
	@Value("${email.setting}")
    private String emailSetting;

    @Value("${app.state}")
    private String appState;
	
	@Value("${app.newurl.production.ntfc}")
    private String appUrlNtfcProd;
	
	@Value("${app.newurl.development.ntfc}")
    private String appUrlNtfcDevel;
	
	
	protected String getWorkingUrl() {
		return (appState.equals("development")?appUrlNtfcDevel:appUrlNtfcProd);
	}
	
	protected String getWorkingFolder() {
		return (appState.equals("development")?appFolderDev:appFolderProd);
	}

	private SimpleDateFormat sda = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
    static String capitailizeFirstWord(String str) {
    	//str = str.toLowerCase();
        StringBuffer s = new StringBuffer();
        char ch = ' '; 
        for (int i = 0; i < str.length(); i++) { 
            if (ch == ' ' && str.charAt(i) != ' ') 
                s.append(Character.toUpperCase(str.charAt(i))); 
            else
                s.append(str.charAt(i)); 
            ch = str.charAt(i); 
        }
        return s.toString().trim(); 
    } 

    public freemarker.template.Configuration getConfiguration() throws IOException{
    	freemarker.template.Configuration cfg = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_27);

        // Where do we load the templates from:
		String templateFolder = getWorkingFolder() + "mail/";
        
        cfg.setTemplateLoader(new FileTemplateLoader(new File(templateFolder)));

        // Some other recommended settings:
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return cfg;
    }
    
    public void sendMailReport(String email, String filename, String password, File file) throws Exception {
    	sendMailReport(email, null, filename, password, file);
    }
        
    public void sendMailReport(String email, String name, String filename, String password, File file) throws Exception {
    	
    	Date date = new Date();
    	String datetime = sda.format(date);
		
        Template template = getConfiguration().getTemplate("template_laporan_alhp.html");
        StringWriter stringWriter = new StringWriter();
        Map<String, Object> input = new HashMap<String, Object>();
        input.put("filename", name);
        input.put("password", password);
        input.put("datetime", datetime);
		template.process(input, stringWriter);
        
		String reportFileName = filename.replaceAll(".pdf", "").replaceAll(".xlsx", "").replaceAll(".xls", "");
		String emails = "EMAIL_GROUP_TO:ubaycreative@gmail.com";//+email;
        String subject = "Laporan LHA - " + (name!=null? (name.equals("datetime")?"(" + datetime + ")":name) : reportFileName);
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] password: " + password);
        System.out.println("[sendMailReport] datetime: " + datetime);
        //System.out.println("[sendMailReport] message: " + message);
        System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        map.add("email_attachments", new FileSystemResource(file));
        
        System.out.println("file.exist(): " + file.exists());
        
        map.add("email_recipients", emails);
        callSendEmailService(map);
		
    }
    
    public void sendMailPurchaseConfirm(Map<String, String> data, File file) throws Exception {
    	
    	Date date = new Date();
    	String datetime = sda.format(date);
		
        Template template = getConfiguration().getTemplate("template_confirm_purchase.html");
		String emails = "EMAIL_SINGLE_TO:"+data.get("email");
		emails=data.get("email");
		String subject="Konfirmasi Pemesanan TIKET - Nomor Booking "+data.get("id");
        StringWriter stringWriter = new StringWriter();
       
        data.put("datetime", datetime);
		template.process(data, stringWriter);
		
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] datetime: " + datetime);
        
        System.out.println("[sendMailReport] message: " + message);
       
        System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        
        map.add("email_attachments", new FileSystemResource(file));
        
        System.out.println("file.exist(): " + file.exists());
        
        map.add("email_recipients", emails);
        callSendEmailService(map);
    }
    
    public void sendMailTicket(Map<String, String> data, File file) throws Exception {
    	
    	Date date = new Date();
    	String datetime = sda.format(date);
		
        Template template = getConfiguration().getTemplate("template_ticket.html");
		String emails = "EMAIL_SINGLE_TO:"+data.get("email");
		//String emails = "EMAIL_SINGLE_TO:kayumiman@gmail.com";//+email;
		emails=data.get("email");
		String subject="E-Tiket TNGH(Salak) Anda - Nomor Booking "+data.get("id");
        StringWriter stringWriter = new StringWriter();
       /* Map<String, Object> input = new HashMap<String, Object>();
        input.put("nama", data);
        input.put("nip", nip);
        input.put("noTicket", noTicket);
        input.put("datetime", datetime);*/
        
        if(data.get("mode") !=null) {
        	template = getConfiguration().getTemplate("template_ticket-anggota-tim.html");
        	String htmlPertanyaan="";
        	//QueryParameter param = new QueryParameter();
        	if(data.get("mode").equals("DISTRIBUSI")) {
	        	subject="Distribusi Pertanyaan - No. Tiket "+data.get("noTicket");
	        	if(data.get("for").equals("KETUA_TIM")) 
	        		data.put("teksPembuka", "Ketua Tim, <br/>Mohon follow up pertanyaan berikut dibawah ini");
	        	else
	        		data.put("teksPembuka", "Anggota Tim, <br/>Mohon follow up pertanyaan berikut dibawah ini");
	        }
        	if(data.get("mode").equals("APPROVAL")) {
	        	subject="Reviu Pertanyaan - No. Tiket "+data.get("noTicket");
	        	data.put("teksPembuka", "Ketua Tim, <br/>Mohon reviu dan persetujuannya untuk jawaban dari pertanyaan berikut ini");
	        }
        	
        	//param.setClause(param.getClause()+" AND "+Pertanyaan.TICKET_ID_PERTANYAAN+"='"+data.get("noTicket")+"'");
        	int no=1;
//        	for(Pertanyaan pertanyaan : pertanyaanMapper.getList(param)) {
//        		htmlPertanyaan+= "<tr>"+
//				"    <td style='width: 2.7076%; vertical-align: top; text-align: left;'>"+no+"</td>"+
//				"    <td style='width: 81.1372%; vertical-align: top; text-align: left;'>"+pertanyaan.getJudulPertanyaan()+"</td>"+
//				"</tr>";
//        		no++;
//        	}
        	data.put("tabelPertanyaan", htmlPertanyaan);
        }
        
        data.put("datetime", datetime);
		template.process(data, stringWriter);
		
//		Ticket ticket = ticketMapper.getEntity(data.get("noTicket"));
//		if(ticket.getStatusTicket().equals("SELESAI")) {
//			subject="Pertanyaan No. Tiket "+data.get("noTicket")+" Telah dijawab";
//		}
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] datetime: " + datetime);
        
        System.out.println("[sendMailReport] message: " + message);
        //
        /*
        String name = "New Profil LHP (" + code + ")";
		String filename = name.replaceAll("/", "_") + ".pdf";

		FileInputStream inStream = appManagerService.exportNewProfilLhp(id);
		
		id.co.hijr.sistem.common.Utils.copyToTmpDir(inStream, filename);
		//
		*/
        System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        
        map.add("email_attachments", new FileSystemResource(file));

        System.out.println("file.exist(): " + file.exists());
        System.out.println("notification_app: " + organizationCode);
        System.out.println("notification_title: " + subject);
        System.out.println("notification_body_text: " + message);
        System.out.println("notification_settings: " + emailSetting);
        System.out.println("notification_user_added: " + organizationId);
        
        map.add("email_recipients", emails);
        callSendEmailService(map);

//        HttpHeaders headers = new HttpHeaders();
//        //headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//
//        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);
//        
    }
    


    public void sendMailInvoice(Map<String, String> data) throws Exception {
    	Date date = new Date();
    	String datetime = sda.format(date);
        Template template = getConfiguration().getTemplate("template_invoice.html");
        if(data.get("typeBank").equals("VA")) {
        	template = getConfiguration().getTemplate("template_invoice_va.html");
        }
		String emails = "EMAIL_SINGLE_TO:"+data.get("email");
		//String emails = "EMAIL_SINGLE_TO:kayumiman@gmail.com";//+email;
		emails=data.get("email");
		String subject="Panduan Pembayaran untuk Pemesanan Anda (TNGH SALAK)";
        StringWriter stringWriter = new StringWriter();
 
        /*if(data.get("mode") !=null) {
        	template = getConfiguration().getTemplate("template_ticket-anggota-tim.html");
        	String htmlPertanyaan="";
        	int no=1;
        	for(Pertanyaan pertanyaan : pertanyaanMapper.getList(param)) {
        		htmlPertanyaan+= "<tr>"+
				"    <td style='width: 2.7076%; vertical-align: top; text-align: left;'>"+no+"</td>"+
				"    <td style='width: 81.1372%; vertical-align: top; text-align: left;'>"+pertanyaan.getJudulPertanyaan()+"</td>"+
				"</tr>";
        		no++;
        	}
        	data.put("tabelPertanyaan", htmlPertanyaan);
        }*/
        
        data.put("datetime", datetime);
		template.process(data, stringWriter);

        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] datetime: " + datetime);
        System.out.println("[sendMailReport] message: " + message);
        
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", Long.valueOf(data.get("userAdded")));
        map.add("email_recipients", emails);
        
        callSendEmailService(map);
    }
    
    public void sendMailSertifikat(Map<String, String> data, File file) throws Exception {
    	
    	Date date = new Date();
    	String datetime = sda.format(date);
		
        Template template = getConfiguration().getTemplate("template_sertifikat.html");
		String emails = "EMAIL_SINGLE_TO:"+data.get("email");
		//String emails = "EMAIL_SINGLE_TO:kayumiman@gmail.com";//+email;
		emails=data.get("email");
        ///
        String subject="Sertifikat TNGH(Salak) Anda";
        StringWriter stringWriter = new StringWriter();
        String html = "";
        
        if(data.get("keterangan_sertifikat")!=null) {
        	html = "Mohon Maaf Anda tidak mencapai kriteria untuk mendapatkan sertifikat pendakian, "+
			""+data.get("keterangan_sertifikat")+
			""+
			"<p>Terima Kasih.</p>"+
			""+
			""+
			"<p>"+
			"  <br>"+
			"</p>"+
			""+
			"<p><strong>Catatan</strong>:</p>"+
			""+
			"<p>Email ini dikirim oleh sistem secara otomatis dan tidak untuk dibalas, terimakasih.</p>";
        }else {
			html = "Berikut kami lampirkan sertifikat Pendakian Gunung Salak via jalur <strong>"+data.get("nameRegion")+"</strong>"+
			""+
			"<p>Terima Kasih.</p>"+
			""+
			""+
			"<p>"+
			"  <br>"+
			"</p>"+
			""+
			"<p><strong>Catatan</strong>:</p>"+
			""+
			"<p>Email ini dikirim oleh sistem secara otomatis dan tidak untuk dibalas, terimakasih.</p>";
        }
    	data.put("stringHtml", html);
        ///
        
        data.put("datetime", datetime);
		template.process(data, stringWriter);
		
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] datetime: " + datetime);
        System.out.println("[sendMailReport] message: " + message);
        //
        
        if(file!=null) System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        
        if(file!=null) map.add("email_attachments", new FileSystemResource(file));
        
        if(file!=null) System.out.println("file.exist(): " + file.exists());
        
        map.add("email_recipients", emails);
        
        callSendEmailService(map);  
    }
    
    //use this method, if the message only some general string, so not need create template anymore
    public void sendMailGeneral(Map<String, String> data, File file) throws Exception {
    	Date date = new Date();
    	String datetime = sda.format(date);
		
        Template template = getConfiguration().getTemplate("template_general.html");
		String emails = data.get("email");
        String subject="Notifikasi TNGH(Salak)";
        StringWriter stringWriter = new StringWriter();
        
        String htmlMessage = "";
        //u can create this html script use some plugin in internet, if u familiar with javascript,
        //this will be easy for u
        htmlMessage = data.get("description_message")+
		""+
		"<p>Terima Kasih.</p>"+
		""+
		""+
		"<p>"+
		"  <br>"+
		"</p>"+
		""+
		"<p><strong>Catatan</strong>:</p>"+
		""+
		"<p>Email ini dikirim oleh sistem secara otomatis dan tidak untuk dibalas, terimakasih.</p>";
        
    	data.put("stringHtml", htmlMessage);
        data.put("datetime", datetime);
		template.process(data, stringWriter);
		
        String message = stringWriter.toString();

        System.out.println("[sendMailReport] email: " + emails);
        System.out.println("[sendMailReport] subject: " + subject);
        System.out.println("[sendMailReport] datetime: " + datetime);
        System.out.println("[sendMailReport] message: " + message);
        if(file!=null) System.out.println("[sendMailReport] file: " + file.getAbsolutePath());
		
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("notification_app", organizationCode);
        map.add("notification_title", subject);
        map.add("notification_body_text", message);
        map.add("notification_settings", emailSetting);
        map.add("notification_user_added", organizationId);
        if(file!=null) map.add("email_attachments", new FileSystemResource(file));
        
        if(file!=null) System.out.println("file.exist(): " + file.exists());
        
        map.add("email_recipients", emails);
        
        callSendEmailService(map);  
    }
    
    public void createVA(Map<String, String> data) throws Exception {
    	
        //call api
        String yourString = "";
		HttpStatus status = HttpStatus.OK;
		
        String baseurl = "https://sandbox.partner.api.bri.co.id";
 
        RestTemplate restTemplate = new RestTemplate();
        String urlToken = baseurl + "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        String urlCreateVA = baseurl + "/v1/briva";
        //set body
		LinkedMultiValueMap<Object, Object> payload = new LinkedMultiValueMap<Object, Object>();
        //set headers
        HttpHeaders headers = new HttpHeaders();
		
		if(status.equals(HttpStatus.OK)) {
			//set body
    		/*{
    	        	"institutionCode": "J104408",
	    	        "brivaNo": "77777",
	    	        "custCode": "16266792700",
	    	        "nama": "Kayum Munajir",
	    	        "amount": "50000",
	    	        "keterangan": "",
	    	        "expiredDate": "2021-08-29 09:57:26"
	    	}*/
    		
    		payload.add("institutionCode", "J104408"); //nanti ganti code ini dari yg diberikan oleh BRI
    		payload.add("brivaNo", "77777"); //nomor unik perusahaan
    		payload.add("custCode", data.get("accountNumberBank")); 
    		payload.add("nama", data.get("accountHolderName"));
    		payload.add("amount", data.get("totalAmount"));
    		payload.add("keterangan", data.get("keterangan"));
    		payload.add("expiredDate", data.get("expiredDate"));;
    		
    		JsonObject object = new JsonObject();
    		//JSONObject object = new JSONObject();
    		object.addProperty("institutionCode", "J104408"); 
    		object.addProperty("brivaNo", "77777"); 
    		object.addProperty("custCode", data.get("accountNumberBank")); 
    		object.addProperty("nama", data.get("accountHolderName"));
    		object.addProperty("amount", data.get("totalAmount"));
    		object.addProperty("keterangan", data.get("keterangan"));
    		object.addProperty("expiredDate", data.get("expiredDate"));;

            System.out.println("before :"+object.toString());
            // set headers
            headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("BRI-Timestamp", data.get("BRI-Timestamp"));
            headers.set("BRI-Signature", data.get("BRI-Signature"));
            headers.set("Authorization", data.get("access_token"));
            
            //
            String requestJson = "{\"queriedQuestion\":\"Is there pain in your hand?\"}";

            HttpEntity<String> request = new HttpEntity<String>(object.toString(),headers);
            //
            
            System.out.println("failed in here");
            //HttpEntity<LinkedMultiValueMap<Object, Object>> request = new HttpEntity<LinkedMultiValueMap<Object, Object>>(payload, headers);
            System.out.println(request);
            ResponseEntity<String> response = restTemplate.postForEntity(urlCreateVA, request, String.class);
            System.out.println("hasil create VA api: " + response);
        }
        
    }
    
    public void callSendEmailService(LinkedMultiValueMap<String, Object> payload) {

		String yourString = "";
		HttpStatus status = HttpStatus.OK;
        // harus ada auth header
        //ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        //@RequestHeader("Authorization") String authHeader
		String authHeader = "";
        String tokenValue = authHeader.replace("Bearer", "").trim();
        //BearerAuthRestTemplate restTemplate = new BearerAuthRestTemplate(tokenValue);
        RestTemplate restTemplate = new RestTemplate();
        String urlNotificationSave = getWorkingUrl() + "/notification/save";
        System.out.println("url: " + urlNotificationSave);
        //restTemplate.post

        // set headers
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity<LinkedMultiValueMap<String, Object>>(payload, headers);
        
        ResponseEntity<String> loginResponse = restTemplate.postForEntity(urlNotificationSave, request, String.class);
        //ResponseEntity<String> result = restTemplate.exchange(urlNotificationSave, HttpMethod.POST, request, String.class);
        
        System.out.println("hasil api: " + loginResponse);
        
	}

   
}
