package id.co.hijr.sistem.service;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.html.table.Table;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import id.co.hijr.sistem.common.QRCodeGenerator;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;

@Service
@Configuration
public class AppManagerService {

    public static String SOURCE_SATSDN = "resources/docx/satsdn_source.docx";
    public static String DEST_SATSDN = "resources/docx/satsdn_dest.docx";
    public static Integer MAXIMAL_LIST_IN_AA  = 1;
    

    public static String SOURCE_FORM_C_AA = "resources/pdfs/nama_jalur_kode_booking.pdf";
    public static String SOURCE_FORM_C_AB = "resources/pdfs/form-c-template-acrobat-win-ab-result.pdf";
    public static String DEST_FORM_C_AA = "results/xmlworker/formc-result-aa.pdf";
    public static String DEST_FORM_C_AB = "results/xmlworker/formc-result-ab.pdf";
    public static String DEST_FORM_C_NO_QRCODE = "results/xmlworker/formc-result-no-qrcode.pdf";
    public static String DEST_FORM_C_WITH_QRCODE = "results/xmlworker/formc-result-with-qrcode.pdf";
    public static String HTML_PERMOHONAN_FORM_C = "resources/xml/permohonan-form-c.html";
    public static String CSS_FORM_C_AA  = "resources/xml/style-form-c-aa.css";
    public static String CSS_FORM_C_AB  = "resources/xml/style-form-c-ab.css";
    public static Integer MAXIMAL_LIST_FORM_C_IN_AA  = 3;
    
    //
    public static String SOURCE_FORM_SERTIFIKAT = "resources/pdfs/sertifikat-booking-online.pdf";
    
    public static String DEST_FORM_SERTIFIKAT_NO_QRCODE = "results/xmlworker/formsertifikat-result-no-qrcode.pdf";
    public static String DEST_FORM_SERTIFIKAT_WITH_QRCODE = "results/xmlworker/formsertifikat-result-with-qrcode.pdf";

    public static String DEST_FORM_SERTIFIKAT_AA = "results/xmlworker/formsertifikat-result-aa.pdf";
    public static Integer MAXIMAL_LIST_FORM_SERTIFIKAT_IN_AA  = 3;
    public static String HTML_FORM_SERTIFIKAT = "resources/xml/form-sertifikat.html";
    public static String CSS_FORM_SERTIFIKAT  = "resources/xml/style-form-sertifikat.css";
    //
    
    public static Boolean IS_SETUP_COMPLETE = false;
	
	@Value("${email.setting}")
    private String emailSetting;

    @Value("${app.url.xapi}")
    private String appUrlXapi;
    
    @Value("${app.url.boarding}")
    private String appUrlBoarding;
    
	@Value("${simponi.billing.module}")
    private String simponiBillingModule;
	
	@Value("${simponi.billing.param}")
    private String simponiBillingParam;
	
	@Value("${simponi.billing.callback}")
    private String simponiBillingCallback;
	
	@Value("${app.organization.id}")
	private String organizationId;
	
	@Value("${app.organization.code}")
	private String organizationCode;
	
	
	@Autowired
	@Qualifier("emailMessaging")
	protected MessagingService messagingService;

	@Autowired
	private PurchaseMapper purchaseMapper;

	@Autowired
	private PurchaseDetilVisitorMapper purchaseDetilVisitorMapper;

	@Autowired
	private PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper;


	@Autowired
	private PurchaseDetilLuggageMapper purchaseDetilLuggageMapper;
	


	@Autowired
	private VaultService vaultService;
    
    @Value("${app.state}")
    private String appState;
    
    @Value("${app.folder.production}")
    private String appFolder;
    
    @Value("${app.folder.dev}")
    private String appFolderDev;

    private void setup(String printer) {
    	System.out.println("[SETUP] printer: " + printer);
    	
    	if(IS_SETUP_COMPLETE) return;
    	
    	String folder = appFolderDev;
		if(appState.equals("production")) {
			folder = appFolder;
		}
		
		SOURCE_SATSDN = folder + SOURCE_SATSDN;
		DEST_SATSDN = folder + DEST_SATSDN;
		
		SOURCE_FORM_C_AA = folder + SOURCE_FORM_C_AA;
		SOURCE_FORM_C_AB = folder + SOURCE_FORM_C_AB;
		DEST_FORM_C_AA = folder + DEST_FORM_C_AA;
		DEST_FORM_C_AB = folder + DEST_FORM_C_AB;
		DEST_FORM_C_NO_QRCODE = folder + DEST_FORM_C_NO_QRCODE;
		DEST_FORM_C_WITH_QRCODE = folder + DEST_FORM_C_WITH_QRCODE;
		HTML_PERMOHONAN_FORM_C = folder + HTML_PERMOHONAN_FORM_C;
		CSS_FORM_C_AA = folder + CSS_FORM_C_AA;
		CSS_FORM_C_AB = folder + CSS_FORM_C_AB;
		
		//
		SOURCE_FORM_SERTIFIKAT = folder + SOURCE_FORM_SERTIFIKAT;
		DEST_FORM_SERTIFIKAT_NO_QRCODE = folder + DEST_FORM_SERTIFIKAT_NO_QRCODE;
		DEST_FORM_SERTIFIKAT_WITH_QRCODE = folder + DEST_FORM_SERTIFIKAT_WITH_QRCODE;
		DEST_FORM_SERTIFIKAT_AA = folder + DEST_FORM_SERTIFIKAT_AA;
		HTML_FORM_SERTIFIKAT = folder + HTML_FORM_SERTIFIKAT;
		CSS_FORM_SERTIFIKAT = folder + CSS_FORM_SERTIFIKAT;
		//
		
		IS_SETUP_COMPLETE = true;
    }

    public String changeText(String text, String before, String after) {
		if (text.contains(before)) text = text.replace(before, after);
		return text;
    }
    
    public FileInputStream reportPermohonanFormC(String id) {
    	
    	setup("DEFAULT");
    	
    	try {
            File file = new File(DEST_FORM_C_NO_QRCODE);
            file.getParentFile().mkdirs();
            File fileResult = new File(DEST_FORM_C_WITH_QRCODE);
            file.getParentFile().mkdirs();
        	
            QueryParameter param = new QueryParameter();
            param.setClause(param.getClause() + " AND " + Purchase.ID + "=" + id);
            Purchase purchase = purchaseMapper.getListExtended(param).get(0); //getEntity(id);
            Purchase purchaseNullNoParent = new Purchase();
            Purchase purchaseNotNullNoParent = new Purchase();
            if(purchase.getParentId()!=null){
           	 	QueryParameter param_detil = new QueryParameter();
	   			param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
	   			param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is not null)");
	   			purchaseNotNullNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
	   			
	   			param_detil = new QueryParameter();
	   			param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
	   			param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is null)");
	   			purchaseNullNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
	   			
	   			param_detil = new QueryParameter();
	   			param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
	   			param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.PARENT_HEADER_ID + " ='"+purchase.getId()+"')");
	   			Purchase purchaseDetilVisitorWithHeader= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
	   			
	   			purchase.setPurchaseDetilVisitor(purchaseDetilVisitorWithHeader.getPurchaseDetilVisitor());
            }else {
            	QueryParameter param_detil = new QueryParameter();
	   			param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getId()+"')");
	   			param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.PARENT_HEADER_ID + " is "+" null "+")");
	   			Purchase purchaseDetilVisitorWithHeader= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
	   			
	   			purchase.setPurchaseDetilVisitor(purchaseDetilVisitorWithHeader.getPurchaseDetilVisitor());
            }
            
            for(PurchaseDetilVisitor p : purchase.getPurchaseDetilVisitor()) {
            	p.getVisitorIdentity().setNoIdentity(vaultService.decrypt(p.getVisitorIdentity().getNoIdentity())); 
            }
            System.out.println("purchase: " + purchase);
            System.out.println("purchase.getRegion().getName(): " + purchase.getRegion().getName());

			param = new QueryParameter();
			if(purchase.getParentId()!=null) {
				param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.HEADER_ID + "=" + purchase.getParentId());
			}else {
				param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.HEADER_ID + "=" + purchase.getId());
			}
			param.setOrder(PurchaseDetilLuggage.TYPE_LUGGAGE);
			List<PurchaseDetilLuggage> listDetilLuggage= purchaseDetilLuggageMapper.getList(param);
			
        	System.out.println("SOURCE_FORM_C_AA: " + SOURCE_FORM_C_AA);
        	System.out.println("SOURCE_FORM_C_AB: " + SOURCE_FORM_C_AB);
        	System.out.println("HTML_PERMOHONAN_FORM_C: " + HTML_PERMOHONAN_FORM_C);
        	System.out.println("listDetilLuggage.size(): " + listDetilLuggage.size());
        	System.out.println("DEST_FORM_C_AA: " + DEST_FORM_C_AA);
        	System.out.println("DEST_FORM_C_AB: " + DEST_FORM_C_AB);

            File fileAA = new File(DEST_FORM_C_AA);
            fileAA.getParentFile().mkdirs();
        	LaporanPdfFormCTemplateAA templateAA = new LaporanPdfFormCTemplateAA(
        			SOURCE_FORM_C_AA,
        			MAXIMAL_LIST_FORM_C_IN_AA, 
        			HTML_PERMOHONAN_FORM_C,
        			purchase,
        			listDetilLuggage,
        			purchaseNotNullNoParent,
        			purchaseNullNoParent
        			);
        
            // step 1
            Document documentAA = new Document(templateAA.getPageSize(),
                templateAA.getmLeft(), templateAA.getmRight(), 
                templateAA.getmTop(), templateAA.getmBottom());
            // step 2
            PdfWriter writerAA = PdfWriter.getInstance(documentAA, new FileOutputStream(DEST_FORM_C_AA));
            writerAA.setPageEvent(templateAA);
            writerAA.close();
            // step 3
        	documentAA.open();
        	// step 4
            ElementList elementsAA = LaporanPdfFormCTemplateAA.parseHtml(HTML_PERMOHONAN_FORM_C, CSS_FORM_C_AA, Tags.getHtmlTagProcessorFactory());
            for (Element e : elementsAA) {
            	documentAA.add(e);
            }
            // step 5
            documentAA.close();
            
            
            File fileAB = new File(DEST_FORM_C_AB);
            fileAB.getParentFile().mkdirs();
        	LaporanPdfFormCTemplateAB templateAB = new LaporanPdfFormCTemplateAB(
        			SOURCE_FORM_C_AB,
        			MAXIMAL_LIST_FORM_C_IN_AA, 
        			HTML_PERMOHONAN_FORM_C,
        			purchase,
        			listDetilLuggage,
        			purchaseNotNullNoParent,
        			purchaseNullNoParent
        			);
            // step 1
            Document documentAB = new Document(templateAB.getPageSize(),
                templateAB.getmLeft(), templateAB.getmRight(), 
                templateAB.getmTop(), templateAB.getmBottom());
            // step 2
            PdfWriter writerAB = PdfWriter.getInstance(documentAB, new FileOutputStream(DEST_FORM_C_AB));
            writerAB.setPageEvent(templateAB);
            writerAB.close();
            // step 3
            documentAB.open();
            // step 4
            ElementList elementsAB = LaporanPdfFormCTemplateAA.parseHtml(HTML_PERMOHONAN_FORM_C, CSS_FORM_C_AB, Tags.getHtmlTagProcessorFactory());
            for (Element e : elementsAB) {
            	documentAB.add(e);
            }
            /*
            // Creating a table       
            float [] pointColumnWidths = {150F, 150F, 150F};   
            com.itextpdf.layout.element.Table table = new  com.itextpdf.layout.element.Table(pointColumnWidths);    
            
            // Adding cells to the table       
            Cell cell1 = new Cell();   // Creating a cell 
            cell1.add("Name");         // Adding content to the cell 
            table.addCell(cell1);      // Adding cell to the table       

            // Adding cell 2 to the table Cell 
            Cell cell2 = new Cell();       // Creating a cell 
            cell2.add("Raju");        // Adding content to the cell 
            table.addCell(cell2);     // Adding cell to the table 
            documentAB.add(table);
            */
            // step 5
            documentAB.close();
            
            
            PdfReader aa = new PdfReader(DEST_FORM_C_AA);
            PdfReader ab = new PdfReader(DEST_FORM_C_AB);
            Document document = new Document();
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(DEST_FORM_C_NO_QRCODE));
            document.open();
            copy.addDocument(aa);
            copy.addDocument(ab);
            document.close();
            aa.close();
            ab.close();
            
            // tambah barcode
            String pathurl=appUrlBoarding+"?id="+purchase.getId();
            addQrCode(DEST_FORM_C_NO_QRCODE, DEST_FORM_C_WITH_QRCODE, pathurl);

        	System.out.println("DEST_FORM_C_NO_QRCODE: " + DEST_FORM_C_NO_QRCODE);
        	System.out.println("DEST_FORM_C_WITH_QRCODE: " + DEST_FORM_C_WITH_QRCODE);
    		
    		return new FileInputStream(fileResult);
    	}
    	catch(Exception e){
    		System.out.println("ada error");
    		e.printStackTrace();
    	}
		return null;
    }
    

    
    public FileInputStream sertifikat(String id) {
    	
    	setup("DEFAULT");
    	
    	try {
            File file = new File(DEST_FORM_SERTIFIKAT_NO_QRCODE); //DEST_FORM_C_NO_QRCODE
            file.getParentFile().mkdirs();
            File fileResult = new File(DEST_FORM_SERTIFIKAT_WITH_QRCODE); //DEST_FORM_C_WITH_QRCODE
            file.getParentFile().mkdirs();
        	

            QueryParameter param = new QueryParameter();
            param.setClause(param.getClause() + " AND " + Purchase.ID + "=" + id);
            Purchase purchase = purchaseMapper.getListExtended(param).get(0); //getEntity(id);
            System.out.println("purchase: " + purchase);
            System.out.println("purchase.getRegion().getName(): " + purchase.getRegion().getName());


            param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilVisitor.HEADER_ID + "=" + purchase.getId());
			param.setClause(param.getClause() + " AND " + PurchaseDetilVisitor.IS_VALID + "=" + 1);
			List<PurchaseDetilVisitor> listDetilVisitor = purchaseDetilVisitorMapper.getList(param);
			//purchase.setPurchaseDetilVisitor(listDetilVisitor);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilTransitCamp.HEADER_ID + "=" + purchase.getId());
			List<PurchaseDetilTransitCamp> listDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param);
			//purchase.setPurchaseDetilTransitCamp(listDetilTransitCamp);

        	System.out.println("SOURCE_FORM_C_AA: " + SOURCE_FORM_C_AA);
        	System.out.println("SOURCE_FORM_C_AB: " + SOURCE_FORM_C_AB);
        	System.out.println("HTML_PERMOHONAN_FORM_C: " + HTML_PERMOHONAN_FORM_C);
        	System.out.println("listDetilVisitor.size(): " + listDetilVisitor.size());
        	System.out.println("DEST_FORM_C_AA: " + DEST_FORM_C_AA);
        	System.out.println("DEST_FORM_C_AB: " + DEST_FORM_C_AB);

            File fileAA = new File(DEST_FORM_SERTIFIKAT_AA); //DEST_FORM_C_AA
            fileAA.getParentFile().mkdirs();
            //SOURCE_FORM_C_AA, MAXIMAL_LIST_FORM_C_IN_AA, HTML_PERMOHONAN_FORM_C
            SertifikatPdfForm templateAA = new SertifikatPdfForm(SOURCE_FORM_SERTIFIKAT, MAXIMAL_LIST_FORM_SERTIFIKAT_IN_AA, HTML_FORM_SERTIFIKAT, purchase, listDetilVisitor);
        
            // step 1
            Document documentAA = new Document(templateAA.getPageSize(),
                templateAA.getmLeft(), templateAA.getmRight(), 
                templateAA.getmTop(), templateAA.getmBottom());
            // step 2
            PdfWriter writerAA = PdfWriter.getInstance(documentAA, new FileOutputStream(DEST_FORM_SERTIFIKAT_AA)); //DEST_FORM_C_AA
            writerAA.setPageEvent(templateAA);
            writerAA.close();
            // step 3
        	documentAA.open();
        	// step 4
        	//HTML_PERMOHONAN_FORM_C, CSS_FORM_C_AA
            ElementList elementsAA = SertifikatPdfForm.parseHtml(HTML_FORM_SERTIFIKAT, CSS_FORM_SERTIFIKAT, Tags.getHtmlTagProcessorFactory());
            for (Element e : elementsAA) {
            	documentAA.add(e);
            }
            // step 5
            documentAA.close();
            
            PdfReader aa = new PdfReader(DEST_FORM_SERTIFIKAT_AA); //DEST_FORM_C_AA
            Document document = new Document();
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(DEST_FORM_SERTIFIKAT_NO_QRCODE)); //DEST_FORM_C_NO_QRCODE
            document.open();
            copy.addDocument(aa);
            document.close();
            aa.close();
            
            // tambah barcode
            addQrCodeAny(DEST_FORM_SERTIFIKAT_NO_QRCODE, DEST_FORM_SERTIFIKAT_WITH_QRCODE, purchase.getCodeBooking());
            
            //
            
        	System.out.println("DEST_FORM_C_NO_QRCODE: " + DEST_FORM_C_NO_QRCODE);
        	System.out.println("DEST_FORM_C_WITH_QRCODE: " + DEST_FORM_C_WITH_QRCODE);
    		
    		return new FileInputStream(fileResult);
    	}
    	catch(Exception e){
    		System.out.println("ada error");
    		e.printStackTrace();
    	}
		return null;
    }

    // tambah barcode pojok kanan bawah
    public FileOutputStream addQrCode(String src, String dest, String text) throws DocumentException, IOException {
    	
    	PdfReader reader = new PdfReader(src);
		FileOutputStream fos = new FileOutputStream(dest);

		PdfStamper pdfStamper = new PdfStamper(reader, fos);
		int size = 75;
		Image image = Image.getInstance(QRCodeGenerator.QRCode(text, size, "png").getAbsolutePath());

		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			PdfContentByte content = pdfStamper.getUnderContent(i);
			Rectangle r = reader.getPageSize(i);
			System.out.println("i "+i);
			System.out.println("r.getWidth() "+r.getWidth());
			if(i == 1 || i == 2) {
				image.setAbsolutePosition((r.getWidth() / 2) + 170, 680 );
			} else {
				image.setAbsolutePosition((r.getWidth() / 2) + 170, 680 );
				//image.setAbsolutePosition((r.getWidth() / 2) - 40, 50 );
			}
			content.addImage(image);
		}

		pdfStamper.close();
		return fos;
    }
    


    // tambah barcode any
    public FileOutputStream addQrCodeAny(String src, String dest, String text) throws DocumentException, IOException {
    	
    	PdfReader reader = new PdfReader(src);
		FileOutputStream fos = new FileOutputStream(dest);

		PdfStamper pdfStamper = new PdfStamper(reader, fos);
		int size = 68;
		Image image = Image.getInstance(QRCodeGenerator.QRCode(text, size, "png").getAbsolutePath());

		System.out.println("crying "+image.getUrl());
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			PdfContentByte content = pdfStamper.getUnderContent(i);
			Rectangle r = reader.getPageSize(i);
			System.out.println("i "+i);
			System.out.println("r.getWidth() "+r.getWidth());
			//if(i == 1) {
				image.setAbsolutePosition((r.getWidth() / 2) + 150, 520 );
				//image.setAbsolutePosition(100f, 150f);
				image.setBorderColor(BaseColor.BLACK);
				image.setBackgroundColor(BaseColor.BLACK);
			//} else {
			//	image.setAbsolutePosition((r.getWidth() / 2) - 40, 50 );
			//}
			content.addImage(image);
		}

		pdfStamper.close();
		return fos;
    }
}
