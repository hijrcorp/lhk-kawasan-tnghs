package id.co.hijr.sistem.service;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.ticket.controller.PurchaseRestController;
import id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RequestPurchaseMapper;
import id.co.hijr.ticket.model.Bank;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.RequestPurchase;

@Service
public class BankService {

    private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	PurchaseMapper purchaseMapper;
	
	@Autowired
	PurchaseDetilTransactionMapper purchaseDetilTransactionMapper;

	@Autowired
	RequestPurchaseMapper requestPurchaseMapper;
	
	@Autowired
	PurchaseRestController purchaseRestController;

    public void executeSampleJob() {

        logger.info("BRI API has begun...");
        try {
        	/** Begin Get Notification Setting **/
            
            /*List<NotificationSettings> listNotificationSettings = notificationSettingsMapper.getList();
            HashMap<String, HashMap<String, String>> settings = new HashMap<String, HashMap<String, String>>();
            
            String currentSettingType = null;
        	HashMap<String, String> setting = new HashMap<String, String>();
            for(NotificationSettings notificationSettings : listNotificationSettings) {
            	if(currentSettingType == null) currentSettingType = notificationSettings.getNotificationSettingsType();
            	
            	if(!notificationSettings.getNotificationSettingsType().equals(currentSettingType)) {
            		settings.put(currentSettingType, setting);
            		setting = new HashMap<String, String>();
            		currentSettingType = notificationSettings.getNotificationSettingsType();
            	}
            	
            	setting.put(notificationSettings.getNotificationSettingsName(), notificationSettings.getNotificationSettingsValue());
            	
            }
            settings.put(currentSettingType, setting);*/
            
            //System.out.println(settings.get("DEFAULT"));
            //System.out.println(settings.get("HIJRCORP"));
            
            /** End Get Notification Setting **/
            
        	//call api
            String baseurl = "https://sandbox.partner.api.bri.co.id";
            String urlCreateVA = baseurl + "/v1/briva";
            String pathGetVA = "/v1/briva";
            // String urlToken = baseurl + "/oauth/client_credential/accesstoken?grant_type=client_credentials";
             //String urlGetVA = urlCreateVA + "/J104408/77777/custCode";
           
            QueryParameter param = new QueryParameter();
            param.setClause(param.getClause()+" AND "+Purchase.STATUS_BOOKING+" NOT IN('DONE')");
           // param.setClause(param.getClause()+" AND "+Purchase.STATUS_BOOKING+" NOT IN('EXPIRED','CANCEL','DRAFT','PAYMENT')");
            param.setClause(param.getClause()+" AND "+PurchaseDetilTransaction.PAYMENT_METHOD+" ='"+"VA"+"'");
            List<PurchaseDetilTransaction> listPurchaseDetilTransaction = purchaseDetilTransactionMapper.getList(param);

            List<String> arrUrlGetVA = new ArrayList<String>();
            List<String> arrPathGetVA = new ArrayList<String>();
            for(PurchaseDetilTransaction opdt : listPurchaseDetilTransaction) {
            	arrUrlGetVA.add(urlCreateVA + "/J104408/77777/"+opdt.getPaymentCode());
            	arrPathGetVA.add(pathGetVA + "/J104408/77777/"+opdt.getPaymentCode());
            }
            try {
                String token = createToken().get("token").toString();
            	arrPathGetVA.forEach(path -> {
            		String urlGet = baseurl+path;
            		String Timestamp = "";
            		String Signature = "";
            		String Authorization = "";
            		Map<String, Object> createSignature=null;
					try {
						createSignature = createSignature(path, "GET", token);
						Timestamp = createSignature.get("timestamp")+"";
	            		Signature = createSignature.get("signature")+"";
	            		Authorization = createSignature.get("token")+"";
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

                    System.out.println("HERE URL:"+urlGet);
                    HttpHeaders headers = new HttpHeaders();
                    headers.set("BRI-Timestamp", Timestamp);
                    headers.set("BRI-Signature", Signature);
                    headers.set("Authorization", Authorization);
                    
                    HttpEntity request = new HttpEntity(headers);
                    try {
                        RestTemplate restTemplate = new RestTemplate();
                        //RestTemplate restTemplate = urlGet.startsWith("https") ? getRestTemplateForSelfSsl() : new RestTemplate();
                        ResponseEntity<String> response = restTemplate.exchange(urlGet, HttpMethod.GET, request, String.class);
                        HttpStatus statusCode = response.getStatusCode();
                        logger.info("STATUS GET - {} : {}", urlGet, statusCode);
                        logger.info("BODY GET - {} : {}", response.getBody());
                        //
                        System.out.println("Let's Do something here..");
                      
                		ObjectMapper mapper = new ObjectMapper();
                		JsonNode  root = mapper.readTree(response.getBody()!=null?response.getBody():"");
                		
                		if(root.path("responseCode").textValue().equals("00")) {
                			JSONObject data = new JSONObject(root.path("data").toString());
                			if(data.get("statusBayar").equals("Y")) {
		                        for(PurchaseDetilTransaction opdt : listPurchaseDetilTransaction) {
		                        	System.out.println("WE DO PAID");
		                        	//Purchase purchase = purchaseMapper.getEntity(opdt.getHeaderId());
		                            purchaseRestController.doAction(opdt.getHeaderId(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), 
		                            		"PAYMENT");
		                        }
                			}
                		}
                    } catch (HttpStatusCodeException e) {
                        logger.error(e.getMessage());
                    } catch (Exception e) {
                    	logger.info("SOMETHING PROBLEM IN CONTROLLER - {} : {}");
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                });
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
            
            //set headers
            
           /* System.out.println("failed in here");
            //HttpEntity<LinkedMultiValueMap<Object, Object>> request = new HttpEntity<LinkedMultiValueMap<Object, Object>>(payload, headers);
            System.out.println(headers);
            ResponseEntity<String> response = restTemplate.getForEntity(urlCreateVA, String.class);
            System.out.println("hasil create VA api: " + response);*/

            //assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
            //ObjectMapper mapper = new ObjectMapper();
            //JsonNode root = mapper.readTree(response.getBody());
            //JsonNode name = root.path("name");
            //assertThat(name.asText(), notNullValue());
            
            Thread.sleep(0);
            
        } catch (InterruptedException e) {
            logger.error("Error while executing BRI API", e);
        } finally {
            logger.info("BRI API has finished...");
        }
    }
    
    public Map<String, Object> createSignature(String path, String verb, String token) throws Exception {
	 	SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	 	isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	 	String timestamp = isoFormat.format(new Date());
	 	String secret_key = "FT7Xus6yCCsvbe7g";
		String body = "";
		String strpayload = "path="+path+"&"+"verb="+verb+"&"+"token="+token+"&"+"timestamp="+timestamp+"&"+"body="+body;
		System.out.println(strpayload);
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("nashorn");
		
		engine.eval(Files.newBufferedReader(Paths.get("/Users/kayumiman/git/lhk-kawasan/src/main/resources/public/js/common-bri/hmac-sha256.js"), StandardCharsets.UTF_8));
		engine.eval(Files.newBufferedReader(Paths.get("/Users/kayumiman/git/lhk-kawasan/src/main/resources/public/js/common-bri/enc_base64.js"),StandardCharsets.UTF_8));
		
		Invocable inv = (Invocable) engine;
		// call function from script file
		Object params[] = {strpayload, secret_key};
		Object json = engine.eval("CryptoJS"); 
		Object datas = inv.invokeMethod(json, "HmacSHA256", params); 
		
		json = engine.eval("{CryptoJS.enc.Base64}"); 
		Object datas2 = inv.invokeMethod(json, "stringify", datas); 
		System.out.println(datas2);
		System.out.println(timestamp);
		
		//inv.invokeFunction("CryptoJS.enc.Base64.stringify", inv.invokeFunction("CryptoJS.HmacSHA256", params));
		Map<String, Object> allPayload = new HashMap<>();
		allPayload.put("signature", datas2);
		allPayload.put("timestamp", timestamp);
		allPayload.put("token", token);
		
        return allPayload;
    }

    public static Map<String, Object> createToken() throws Exception {
    	
        String baseurl = "https://sandbox.partner.api.bri.co.id";
        String key = "CKHdlsSZfdJm68PeiyGYrG31XkYgcSed";
        String secret = "FT7Xus6yCCsvbe7g";
        
        RestTemplate restTemplate = new RestTemplate();
        String urlToken = baseurl + "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        //set body
		LinkedMultiValueMap<String, Object> payload = new LinkedMultiValueMap<String, Object>();
		payload.add("client_id", key);
		payload.add("client_secret", secret);
        //set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        
        //init body and headers
        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity<LinkedMultiValueMap<String, Object>>(payload, headers);
        //submit the data
        ResponseEntity<String> response = restTemplate.postForEntity(urlToken, request, String.class);
        
        //logger.info("result token VA api - {} : {}", response);

		ObjectMapper mapper = new ObjectMapper();
		JsonNode  root = mapper.readTree(response.getBody()!=null?response.getBody():"");
		
		Map<String, Object> data = new HashMap<String, Object>();
		if(response.getStatusCode().equals(HttpStatus.OK)) { //this actully not need, cause the BRI is big company it's mean this will be always OK
			data.put("token", "Bearer "+root.path("access_token").textValue());
		}
        return data;
    }
    
    
    
}