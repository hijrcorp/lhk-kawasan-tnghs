package id.co.hijr.sistem.service;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.FileTypeSupport;
import id.co.hijr.sistem.common.Utils;

@Service("fileSystemStorage")
@Configuration
public class FileSystemStorageImpl implements StorageService  {


	@Autowired VaultService vaultService;
	
	@Value("${app.folder.production}")
	private String appFolderProd;
	
	@Value("${app.folder.dev}")
	private String appFolderDev;

	@Value("${imagemagick.path.production}")
	private String imagemagickPathProd;
	
	@Value("${imagemagick.path.dev}")
	private String imagemagickPathDev;
	
	@Value("${app.state}")
	private String appState;
	
	private String appFolder;
	private String imagemagickPath;

	private String uploads = "uploads/";
	
	public void store(MultipartFile file) throws Exception{
		store(file, file.getName());
	}
	
	private String getWorkingFolder() {
		return (appState.equals("development")?appFolderDev:appFolderProd);
	}
	
	@Override
	public File load(String filename) throws Exception {
		
		appFolder = getWorkingFolder()+ uploads;
		
		File file =  new File(appFolder + filename);
		
		System.out.println("load from file system: "+ file.getAbsolutePath());
		
		if(!file.exists() || file.isDirectory()){
			String[] partName = filename.split("\\.");
			String ext = partName[partName.length-1];
			
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				file =  new File(getWorkingFolder() + "notfound.png");
			}else if(Arrays.asList(FileTypeSupport.DOCUMENT.extensions()).contains(ext)) {
				file =  new File(getWorkingFolder() + "notfound.pdf");
			}
		}
		return file;
	}
	
	@Override
	public void delete(String filename) throws Exception {
		
		appFolder = getWorkingFolder()+ uploads;
		
		File file = new File(appFolder + filename);
		File file1 = new File(appFolder + "small_"+filename);
		
		if(file.exists() || file1.exists()){
			FileCopyUtils.copy(file, new File(getWorkingFolder() + "trash/" + filename));
			
			FileSystemUtils.deleteRecursively(file);
			FileSystemUtils.deleteRecursively(file1);
			System.out.println("delete from file system");
		}
	}

	@Override
	public void store(MultipartFile file, String filename) throws Exception {

		appFolder = getWorkingFolder()+ uploads;
		imagemagickPath = (appState.equals("development")?imagemagickPathDev:imagemagickPathProd);
		
		System.out.println("store in file system: " + appFolder);
	
		  
		if (!file.isEmpty()) {
            try {
            	
        		File f = new File(appFolder + filename);
            		
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
                stream.write(bytes);
                stream.close();
                if(file.getContentType().startsWith("image/")){
                	
            		Dimension d = Utils.getImageDimension(f);
            		// generate smaller image if resolution more than 640px
            		if(d.getHeight() > 640 || d.getWidth() > 640) {
            			String w = "640x";
                		if(d.getHeight() > d.getWidth()) {
                			w = "480x";
                		}
                	
            			if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
	                		String[] s = imagemagickPath.split("/");
	                		Process p = Runtime.getRuntime().exec(imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename, null, new File(imagemagickPath.replace(s[s.length-1], "")));
	    	                p.waitFor();
	                	}else{
	                		Process p = Runtime.getRuntime().exec(new String[]{"sh","-c",imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename});
	    	                p.waitFor();
	                	}

            		}else {
            			File fd = new File(appFolder + "small_"+filename);
            			FileSystemUtils.copyRecursively(f, fd);
            		}
            		
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Failed store file.");
            }
        } else {
            throw new Exception("Failed store file.");
        }
	}




	@Override
	public void storeWithEncrypted(MultipartFile file, String filename, String filenameWithoutExt, boolean isMultiple) throws Exception {

		appFolder = getWorkingFolder()+ uploads+"ktp/";
		imagemagickPath = (appState.equals("development")?imagemagickPathDev:imagemagickPathProd);
		
		//System.out.println("store in file system: " + appFolder);
        //System.out.println("Jenis apa INI? "+file.getContentType());
		if (!file.isEmpty()) {
//            try {
        		File f = new File(appFolder + filename);
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
                stream.write(bytes);
                stream.close();
                
                if(file.getContentType().startsWith("image/")){
                	BufferedImage d = ImageIO.read(f);
            		// generate smaller image if resolution more than 640px
            		if(d.getHeight() > 640 || d.getWidth() > 640) {
            			String w = "640x";
                		if(d.getHeight() > d.getWidth()) {
                			w = "480x";
                		}
                	
            			if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
	                		String[] s = imagemagickPath.split("/");
	                		Process p = Runtime.getRuntime().exec(imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename, null, new File(imagemagickPath.replace(s[s.length-1], "")));
	    	                p.waitFor();
	                	}else{
	                		Process p = Runtime.getRuntime().exec(new String[]{"sh","-c",imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename});
	    	                p.waitFor();
	                	}
            		}else {
            			File fd = new File(appFolder + "small_"+filename);
            			FileSystemUtils.copyRecursively(f, fd);
            		}
            		String destinationFilename=appFolder + File.separator + filenameWithoutExt;
            		String destinationFilenameSM=appFolder + File.separator + "small_"+filenameWithoutExt;
            		
            		if(!isMultiple) {
                		File fd = new File(destinationFilename);
                		byte[] bytes1 = FileUtils.readFileToByteArray(new File(appFolder + "small_"+filename));
            			FileUtils.writeStringToFile(fd, vaultService.encrypt(bytes1), StandardCharsets.UTF_8, false);
            		}else {
                		File fd = new File(destinationFilenameSM);
                		byte[] bytes1 = FileUtils.readFileToByteArray(new File(appFolder + "small_"+filename));
            			FileUtils.writeStringToFile(fd, vaultService.encrypt(bytes1), StandardCharsets.UTF_8, false);
                		
            			File fd2 = new File(destinationFilename);
                		byte[] bytes2 = FileUtils.readFileToByteArray(new File(appFolder + filename));
            			FileUtils.writeStringToFile(fd2, vaultService.encrypt(bytes2), StandardCharsets.UTF_8, false);
            		}
            		
        			//delete real file
        			if(new File(appFolder + "small_"+filename).exists()){
        				FileSystemUtils.deleteRecursively(new File(appFolder + "small_"+filename));
        			}
        			if(new File(appFolder + filename).exists()){
        				FileSystemUtils.deleteRecursively(new File(appFolder + filename));
        			}
//        	        File encryptedKtp = vaultService.encrypt(fileKtpPlain.getFile());
//        	        System.out.println("Encrypted : " + encryptedKtp);
                }else {
                	String destinationFilename=appFolder + File.separator + filenameWithoutExt;
            		
            		File fd3 = new File(destinationFilename);
            		byte[] bytes3 = FileUtils.readFileToByteArray(new File(appFolder + filename));
        			FileUtils.writeStringToFile(fd3, vaultService.encrypt(bytes3), StandardCharsets.UTF_8, false);
            		
        			if(new File(appFolder + filename).exists()){
        				FileSystemUtils.deleteRecursively(new File(appFolder + filename));
        			}
                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } else {
            throw new Exception("Failed store file.");
        }
	}
	
	@Override
	public void storeWithEncrypted(MultipartFile file, String filename, String filenameWithoutExt) throws Exception {

		appFolder = getWorkingFolder()+ uploads+"ktp/";
		imagemagickPath = (appState.equals("development")?imagemagickPathDev:imagemagickPathProd);
		
		System.out.println("store in file system: " + appFolder);

        System.out.println("Jenis apa INI? "+file.getContentType());
		if (!file.isEmpty()) {
//            try {
        		File f = new File(appFolder + filename);
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
                stream.write(bytes);
                stream.close();
                
                if(file.getContentType().startsWith("image/")){
                	BufferedImage d = ImageIO.read(f);
            		// generate smaller image if resolution more than 640px
            		if(d.getHeight() > 640 || d.getWidth() > 640) {
            			String w = "640x";
                		if(d.getHeight() > d.getWidth()) {
                			w = "480x";
                		}
                	
            			if(System.getProperty("os.name").toLowerCase().startsWith("windows")){
	                		String[] s = imagemagickPath.split("/");
	                		Process p = Runtime.getRuntime().exec(imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename, null, new File(imagemagickPath.replace(s[s.length-1], "")));
	    	                p.waitFor();
	                	}else{
	                		Process p = Runtime.getRuntime().exec(new String[]{"sh","-c",imagemagickPath+" "+appFolder+filename+" -resize "+w+" "+appFolder+"small_"+filename});
	    	                p.waitFor();
	                	}
            		}else {
            			File fd = new File(appFolder + "small_"+filename);
            			FileSystemUtils.copyRecursively(f, fd);
            		}
            		String destinationFilename=appFolder + File.separator + filenameWithoutExt;
            		String destinationFilenameSM=appFolder + File.separator + "small_"+filenameWithoutExt;
            		//filename=file.getOriginalFilename();
            		File fd = new File(destinationFilenameSM);
            		byte[] bytes1 = FileUtils.readFileToByteArray(new File(appFolder + "small_"+filename));
        			FileUtils.writeStringToFile(fd, vaultService.encrypt(bytes1), StandardCharsets.UTF_8, false);
            		
        			File fd2 = new File(destinationFilename);
            		byte[] bytes2 = FileUtils.readFileToByteArray(new File(appFolder + filename));
        			FileUtils.writeStringToFile(fd2, vaultService.encrypt(bytes2), StandardCharsets.UTF_8, false);
        			
        			//delete real file
        			if(new File(appFolder + "small_"+filename).exists()){
        				FileSystemUtils.deleteRecursively(new File(appFolder + "small_"+filename));
        			}
        			if(new File(appFolder + filename).exists()){
        				FileSystemUtils.deleteRecursively(new File(appFolder + filename));
        			}
//        	        File encryptedKtp = vaultService.encrypt(fileKtpPlain.getFile());
//        	        System.out.println("Encrypted : " + encryptedKtp);
                }else {
                	String destinationFilename=appFolder + File.separator + filenameWithoutExt;
            		
            		File fd3 = new File(destinationFilename);
            		byte[] bytes3 = FileUtils.readFileToByteArray(new File(appFolder + filename));
        			FileUtils.writeStringToFile(fd3, vaultService.encrypt(bytes3), StandardCharsets.UTF_8, false);
            		
        			if(new File(appFolder + filename).exists()){
        				FileSystemUtils.deleteRecursively(new File(appFolder + filename));
        			}
                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        } else {
            throw new Exception("Failed store file.");
        }
	}
	
	@Override
	public File loadWithDecrypt(String filename) throws Exception {

		appFolder = getWorkingFolder()+ uploads+"ktp/";
		
		File file =  vaultService.decrypt(new File(appFolder + filename));
		
		System.out.println("load from file system: "+ file.getAbsolutePath());
		
		if(!file.exists() || file.isDirectory()){
			String[] partName = filename.split("\\.");
			String ext = partName[partName.length-1];
			
			if(Arrays.asList(FileTypeSupport.IMAGE.extensions()).contains(ext)) {
				file =  new File(getWorkingFolder() + "notfound.png");
			}else if(Arrays.asList(FileTypeSupport.DOCUMENT.extensions()).contains(ext)) {
				file =  new File(getWorkingFolder() + "notfound.pdf");
			}
		}
		return file;
	}
}
