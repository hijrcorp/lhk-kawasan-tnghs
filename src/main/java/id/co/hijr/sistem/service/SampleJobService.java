package id.co.hijr.sistem.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.VisitorIdentityMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.VisitorIdentity;

@Service
public class SampleJobService {

    private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	VisitorIdentityMapper visitorIdentityMapper;

    public void executeSampleJob() {

        logger.info("The sample job has begun...");
        try {
            
            Thread.sleep(0);
            
            
        } catch (InterruptedException e) {
            logger.error("Error while executing sample job", e);
        } finally {
            logger.info("Sample job has finished...");
        }
    }
    
    public void executeKawasanJob() {
    	 logger.info("KAWASAN API has begun...");
         try {
        	 QueryParameter param = new QueryParameter();
             param.setClause(param.getClause()+" AND "+VisitorIdentity.STATUS_VALID+" IN('BANNED')");
             //param.setClause(param.getClause()+" AND "+PurchaseDetilTransaction.PAYMENT_METHOD+" ='"+"VA"+"'");
             List<VisitorIdentity> data = visitorIdentityMapper.getList(param);
             
             for(VisitorIdentity vi : data) {
            	 
            	 System.out.println(vi.getFullName());
            	 System.out.println(vi.getStatusValid());
            	 System.out.println("DT:"+vi.getDateUnlock()+"==="+new Date());
            	 System.out.println(vi.getDateUnlock().equals(new Date()));
            	 
            	 if(vi.getDateUnlock().equals(new Date())) {
             		vi.setStatusValid(null);
             		vi.setDateBanned(null);
             	 	vi.setDaysBanned(null);
            		System.out.println("DO UNLOCK");
               	 	visitorIdentityMapper.update(vi);
            	 }
             }
             Thread.sleep(0);
             
         } catch (InterruptedException e) {
             logger.error("Error while executing KAWASAN API", e);
         } finally {
             logger.info("KAWASAN API has finished...");
         }
    }
}