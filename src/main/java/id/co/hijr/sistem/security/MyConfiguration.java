//package id.co.hijr.sistem.security;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author Mark Paluch
// */
////@ConfigurationProperties("example")
//@Configuration
//public class MyConfiguration {
//  @Value("${username}")
//  private String username;
//
//  @Value("${password}")
//  private String password;
//
//  public String getUsername() {
//    return username;
//  }
//
//  public void setUsername(String username) {
//    this.username = username;
//  }
//
//  public String getPassword() {
//    return password;
//  }
//
//  public void setPassword(String password) {
//    this.password = password;
//  }
//}