package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.Position;

@Mapper
public interface PositionMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_position (id_position, name_position, description_position, group_id_position, organization_id_position) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{description:VARCHAR}, #{groupId:VARCHAR}, #{organizationId:VARCHAR})")
	void insert(Position position);

	@Update("UPDATE hijr_position SET id_position=#{id:VARCHAR}, name_position=#{name:VARCHAR}, description_position=#{description:VARCHAR}, group_id_position=#{groupId:VARCHAR}, organization_id_position=#{organizationId:VARCHAR} WHERE id_position=#{id}")
	void update(Position position);

	@Delete("DELETE FROM hijr_position WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_position WHERE id_position=#{id}")
	void delete(Position position);

	List<Position> getList(QueryParameter param);

	Position getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
