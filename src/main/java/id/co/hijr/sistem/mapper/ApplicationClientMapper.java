package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.ApplicationClient;

@Mapper
public interface ApplicationClientMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_application_client (id_application_client, header_id_application_client, code_application_client, type_application_client, resource_application_client, organization_id_application_client) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{code:VARCHAR}, #{type:VARCHAR}, #{resource:VARCHAR}, #{organizationId:VARCHAR})")
	void insert(ApplicationClient applicationClient);

	@Update("UPDATE hijr_application_client SET id_application_client=#{id:VARCHAR}, header_id_application_client=#{headerId:VARCHAR}, code_application_client=#{code:VARCHAR}, type_application_client=#{type:VARCHAR}, resource_application_client=#{resource:VARCHAR}, organization_id_application_client=#{organizationId:VARCHAR} WHERE id_application_client=#{id}")
	void update(ApplicationClient applicationClient);

	@Delete("DELETE FROM hijr_application_client WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_application_client WHERE id_application_client=#{id}")
	void delete(ApplicationClient applicationClient);

	List<ApplicationClient> getList(QueryParameter param);

	ApplicationClient getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
