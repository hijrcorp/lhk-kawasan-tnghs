package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.Organization;

@Mapper
public interface OrganizationMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_organization (id_organization, name_organization, position_id_organization, parent_id_organization, admin_role_organization, sequence_organization) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{positionId:VARCHAR}, #{parentId:VARCHAR}, #{adminRole:VARCHAR}, #{sequence:NUMERIC})")
	void insert(Organization organization);

	@Update("UPDATE hijr_organization SET id_organization=#{id:VARCHAR}, name_organization=#{name:VARCHAR}, position_id_organization=#{positionId:VARCHAR}, parent_id_organization=#{parentId:VARCHAR}, admin_role_organization=#{adminRole:VARCHAR}, sequence_organization=#{sequence:NUMERIC} WHERE id_organization=#{id}")
	void update(Organization organization);

	@Delete("DELETE FROM hijr_organization WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_organization WHERE id_organization=#{id}")
	void delete(Organization organization);

	List<Organization> getList(QueryParameter param);

	Organization getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
