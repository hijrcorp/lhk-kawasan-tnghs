package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.Authorities;

@Mapper
public interface AuthoritiesMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_authorities (id_authorities, account_id_authorities, group_id_authorities, application_id_authorities, person_added_authorities, time_added_authorities) VALUES (#{id:VARCHAR}, #{accountId:VARCHAR}, #{groupId:VARCHAR}, #{applicationId:VARCHAR}, #{personAdded:VARCHAR}, #{timeAdded:TIMESTAMP})")
	void insert(Authorities authorities);

	@Update("UPDATE hijr_authorities SET id_authorities=#{id:VARCHAR}, account_id_authorities=#{accountId:VARCHAR}, group_id_authorities=#{groupId:VARCHAR}, application_id_authorities=#{applicationId:VARCHAR}, person_added_authorities=#{personAdded:VARCHAR}, time_added_authorities=#{timeAdded:TIMESTAMP} WHERE id_authorities=#{id}")
	void update(Authorities authorities);

	@Delete("DELETE FROM hijr_authorities WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_authorities WHERE id_authorities=#{id}")
	void delete(Authorities authorities);

	List<Authorities> getList(QueryParameter param);

	Authorities getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
