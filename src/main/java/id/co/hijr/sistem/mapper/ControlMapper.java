package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.Control;


@Mapper
public interface ControlMapper {
	@Insert("INSERT INTO hijr_control (id_control, account_id_control, source_id_control, is_admin_control) VALUES (#{id:VARCHAR}, #{accountId:VARCHAR}, #{sourceId:VARCHAR}, #{isAdmin:BOOLEAN})")
	void insert(Control control);

	@Update("UPDATE hijr_control SET id_control=#{id:VARCHAR}, account_id_control=#{accountId:VARCHAR}, source_id_control=#{sourceId:VARCHAR}, is_admin_control=#{isAdmin:BOOLEAN} WHERE id_control=#{id}")
	void update(Control control);

	@Delete("DELETE FROM hijr_control WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_control WHERE id_control=#{id}")
	void delete(Control control);

	List<Control> getList(QueryParameter param);

	Control getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

}