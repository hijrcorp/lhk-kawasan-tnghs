package id.co.hijr.sistem.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;

@Mapper
public interface SqlMapper {
	static class PureSqlProvider {
        public String sql(String sql) {
            return sql;
        }

        public String count(String from) {
            return "SELECT count(*) FROM (" + from + " ) AS t";
        }
    }
	
	@SelectProvider(type = PureSqlProvider.class, method = "sql")
    public Map<String, Object> selectOne(String sql);

    @SelectProvider(type = PureSqlProvider.class, method = "sql")
    public List<Map<String, Object>> select(String sql);

    @SelectProvider(type = PureSqlProvider.class, method = "count")
    public Integer count(String from);

    @SelectProvider(type = PureSqlProvider.class, method = "sql")
    public Integer execute(String query);
}
