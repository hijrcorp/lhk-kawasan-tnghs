package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.File;

@Mapper
public interface FileMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_file (id_file, name_file, folder_file, size_file, type_file, reference_id_file, module_file, person_added_file, time_added_file, load_file) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{folder:VARCHAR}, #{size:NUMERIC}, #{type:VARCHAR}, #{referenceId:VARCHAR}, #{module:VARCHAR}, #{personAdded:VARCHAR}, #{timeAdded:TIMESTAMP}, #{load:NUMERIC})")
	void insert(File file);

	@Update("UPDATE hijr_file SET id_file=#{id:VARCHAR}, name_file=#{name:VARCHAR}, folder_file=#{folder:VARCHAR}, size_file=#{size:NUMERIC}, type_file=#{type:VARCHAR}, reference_id_file=#{referenceId:VARCHAR}, module_file=#{module:VARCHAR}, person_added_file=#{personAdded:VARCHAR}, time_added_file=#{timeAdded:TIMESTAMP}, load_file=#{load:NUMERIC} WHERE id_file=#{id}")
	void update(File file);

	@Delete("DELETE FROM hijr_file WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_file WHERE id_file=#{id}")
	void delete(File file);

	List<File> getList(QueryParameter param);

	File getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
	
	@Update("UPDATE hijr_file SET load_file=ifnull(load_file,0)+1 WHERE id_file=#{id}")
	void updateLoad(File file);
	
}
