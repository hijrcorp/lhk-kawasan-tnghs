package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.Assignment;

@Mapper
public interface AssignmentMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_assignment (id_assignment, account_id_assignment, position_id_assignment, organization_id_assignment, status_assignment, start_date_assignment, end_date_assignment, parent_id_assignment, active_assignment) VALUES (#{id:VARCHAR}, #{accountId:VARCHAR}, #{positionId:VARCHAR}, #{organizationId:VARCHAR}, #{status:VARCHAR}, #{startDate:DATE}, #{endDate:DATE}, #{parentId:VARCHAR}, #{active:BOOLEAN})")
	void insert(Assignment assignment);

	@Update("UPDATE hijr_assignment SET id_assignment=#{id:VARCHAR}, account_id_assignment=#{accountId:VARCHAR}, position_id_assignment=#{positionId:VARCHAR}, organization_id_assignment=#{organizationId:VARCHAR}, status_assignment=#{status:VARCHAR}, start_date_assignment=#{startDate:DATE}, end_date_assignment=#{endDate:DATE}, parent_id_assignment=#{parentId:VARCHAR}, active_assignment=#{active:BOOLEAN} WHERE id_assignment=#{id}")
	void update(Assignment assignment);

	@Delete("DELETE FROM hijr_assignment WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_assignment WHERE id_assignment=#{id}")
	void delete(Assignment assignment);

	List<Assignment> getList(QueryParameter param);

	Assignment getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
