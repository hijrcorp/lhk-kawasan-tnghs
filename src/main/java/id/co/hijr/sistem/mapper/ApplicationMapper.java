package id.co.hijr.sistem.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.model.Application;

@Mapper
public interface ApplicationMapper {

	/********************************** - Begin Generate - ************************************/
	
	@Insert("INSERT INTO hijr_application (id_application, code_application, name_application, prefix_role_application, organization_id_application, link_application, default_role_application, person_added_application, time_added_application, person_modified_application, time_modified_application) VALUES (#{id:VARCHAR}, #{code:VARCHAR}, #{name:VARCHAR}, #{prefixRole:VARCHAR}, #{organizationId:VARCHAR}, #{link:VARCHAR}, #{defaultRole:VARCHAR}, #{personAdded:VARCHAR}, #{timeAdded:TIMESTAMP}, #{personModified:VARCHAR}, #{timeModified:TIMESTAMP})")
	void insert(Application application);

	@Update("UPDATE hijr_application SET id_application=#{id:VARCHAR}, code_application=#{code:VARCHAR}, name_application=#{name:VARCHAR}, prefix_role_application=#{prefixRole:VARCHAR}, organization_id_application=#{organizationId:VARCHAR}, link_application=#{link:VARCHAR}, default_role_application=#{defaultRole:VARCHAR}, person_added_application=#{personAdded:VARCHAR}, time_added_application=#{timeAdded:TIMESTAMP}, person_modified_application=#{personModified:VARCHAR}, time_modified_application=#{timeModified:TIMESTAMP} WHERE id_application=#{id}")
	void update(Application application);

	@Delete("DELETE FROM hijr_application WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM hijr_application WHERE id_application=#{id}")
	void delete(Application application);

	List<Application> getList(QueryParameter param);

	Application getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/********************************** - End Generate - ************************************/
}
