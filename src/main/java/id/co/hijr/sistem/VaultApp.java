//package id.co.hijr.sistem;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.ConfigurableApplicationContext;
//
//import id.co.hijr.sistem.security.MyConfiguration;
//
//
//@SpringBootApplication
//public class VaultApp {
//
//	private static Logger log=  LoggerFactory.getLogger(VaultApp.class);
//	
//	public static void main(String[] args) {
//		ConfigurableApplicationContext context = SpringApplication.run(VaultApp.class, args);
//		MyConfiguration configuration = context.getBean(MyConfiguration.class);
//	    //Logger log = LoggerFactory.getLogger(MainApplication.class);
//		log.info("----------------------------------------");
//		log.info("Configuration properties");
//		log.info("   example.username is {}", configuration.getUsername());
//	    log.info("   example.password is {}", configuration.getPassword());
//	    log.info("----------------------------------------");
//		
//	}
//	
//
//}
