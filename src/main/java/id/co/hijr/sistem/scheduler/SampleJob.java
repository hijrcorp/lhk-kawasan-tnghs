package id.co.hijr.sistem.scheduler;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import id.co.hijr.sistem.service.BankService;
import id.co.hijr.sistem.service.SampleJobService;

@Component
public class SampleJob extends QuartzJobBean {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SampleJobService jobService;

    @Autowired
    private BankService bankService;

    public void executeInternal(JobExecutionContext context) throws JobExecutionException {

        logger.info("Job ** {} ** fired @ {}", context.getJobDetail().getKey().getName(), context.getFireTime());

        jobService.executeSampleJob();
        bankService.executeSampleJob();
        jobService.executeKawasanJob();

        logger.info("Next job scheduled @ {}", context.getNextFireTime());
    }
}