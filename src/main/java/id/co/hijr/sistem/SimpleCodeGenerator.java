package id.co.hijr.sistem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

public class SimpleCodeGenerator {
	
	/**
	 * TODO: (none)
	 */

	
	public static void main(String[] args) {
		
		String dbSchemaName = "kawasandb";
		String dbUserName = "kawasandb";
		String dbPassword = "kawasandb";
		String entityName = "purchase_detil_payment";
//	    String tblPrefix = "simpeg_view_";
	    String tblPrefix = "tbl_";
	    String tblName = tblPrefix + entityName;
//	    String columnSufix = "";
	    String columnSufix = "_"+entityName;
//	    String primaryKey = "nip";
	    String primaryKey = "id";
		
		Connection conn = null;
		try {
		    conn =
		       DriverManager.getConnection("jdbc:mysql://localhost/"+dbSchemaName+"?user="+dbUserName+"&password="+dbPassword);

		    // Do something with the Connection
		    //System.out.println(conn);
		    
		    
		    String selectTableSQL = "SELECT REPLACE(COLUMN_NAME,'"+columnSufix+"','') AS `Field`, COLUMN_TYPE AS `Type`, IS_NULLABLE AS `NULL`, \n" + 
		    		"       COLUMN_KEY AS `Key`, COLUMN_DEFAULT AS `Default`, EXTRA AS `Extra`, TABLE_NAME AS `table`\n" + 
		    		"FROM information_schema.COLUMNS  \n"+
		    		"WHERE TABLE_SCHEMA = '"+dbSchemaName+"' AND TABLE_NAME ='"+tblName+"';";
		    Statement statement = conn.createStatement();
		    ResultSet rs = statement.executeQuery(selectTableSQL);
		    String tmp = "";
		    String sqlUpdate = "";
		    String sqlInsert = "";
		    String sqlInsertValues = "";
		    String type = "";
		    String javaType = "";
		    String requestParam = "";
		    String setParam = "";
		    String columnMapping = "";
		    String modelAttributes = "";
		    String field ="";
		    String table ="";
		    String classMethod = "";
		    String fieldCap = "";
		    String xmlMapping = "";
		    String str = "";
		    
		    while (rs.next()) {
			    	
	    			if(rs.getString("TYPE").startsWith("varchar") || rs.getString("TYPE").startsWith("text")) {
					type = ":VARCHAR";
					javaType = "String";
	    			}else if(rs.getString("TYPE").startsWith("decimal") || rs.getString("TYPE").startsWith("int") || rs.getString("TYPE").startsWith("bigint")) {
	    				type = ":NUMERIC";
	    				if(rs.getString("TYPE").startsWith("bigint")) {
	    					javaType = "Long";
	    				}else if(rs.getString("TYPE").startsWith("decimal") || rs.getString("TYPE").startsWith("double")) {
	    					javaType = "Double";
	    				}else {
	    					javaType = "Integer";
	    				}
	    				
	    			}else if(rs.getString("TYPE").startsWith("tinyint")) {
	    				if(rs.getString("NULL").equals("YES")) {
	    					javaType = "Boolean";
	    				}else {
	    					javaType = "boolean";
	    				}
	    				
	    				type = ":BOOLEAN";
	    				
	    			
		    		}else if(rs.getString("TYPE").equals("date")) {
	    				type = ":DATE";
	    				javaType = "Date";
		    		}else if(rs.getString("TYPE").equals("datetime")) {
	    				type = ":TIMESTAMP";
	    				javaType = "Date";
	    			}else if(rs.getString("TYPE").equals("blob")) {
	    				type = ":BLOB";
	    				javaType = "byte[]";
	    			}else {
	    				type = "";
	    			}
	    			
	    			
	    			fieldCap = WordUtils.capitalizeFully(rs.getString("FIELD").replace("_", " ")).replace(" ", "");
	    			field = StringUtils.uncapitalize(fieldCap);
	    			table = WordUtils.capitalizeFully(tmp.replace(tblPrefix, "").replace("_", " ")).replace(" ", "");
	    			
	    			if(rs.getString("FIELD").equals(primaryKey) || rs.getString("FIELD").startsWith(primaryKey+"_") || rs.getString("FIELD").startsWith("user_")) {
	    				javaType = "String";
	    				type = ":VARCHAR";
	    			}
	    			
	    			String requestType = "Optional<"+javaType+">";
	    			if(rs.getString("TYPE").equals("date")) {
	    				requestType = " @DateTimeFormat(pattern = \"dd-MM-yyyy\") Optional<"+javaType+">";
	    			}else if(rs.getString("TYPE").startsWith("decimal")) {
	    				requestType = " @NumberFormat(pattern = \"#,###.##\") Optional<"+javaType+">";
	    			}
	    			
	    			
		    	
		    		if(tmp.equals("") || !rs.getString("TABLE").equals(tmp)) {
		    			if(!tmp.equals("")) {
		    				sqlInsert += ")";
		    				sqlInsertValues += ")";
//		    				System.out.println("@Insert(\""+sqlInsert+sqlInsertValues+"\")");
//		    				System.out.println("void insert("+table+" "+StringUtils.uncapitalize(table)+");\n");
//			    			System.out.println("@Update(\""+sqlUpdate+" WHERE id=#{id}\")");
//			    			System.out.println("void update("+table+" "+StringUtils.uncapitalize(table)+");\n");
//			    			System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE ${clause}\")");
//			    			System.out.println("void deleteBatch(QueryParameter param);\n");
//			    			System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE id=#{id}\")");
//			    			System.out.println("void delete("+table+" "+StringUtils.uncapitalize(table)+");\n");
//			    			
//			    			System.out.println("List<"+table+"> getList(QueryParameter param);\n");
//			    			System.out.println(table+" getEntity(String id);\n");
//			    			System.out.println("long getCount(QueryParameter param);\n");
//			    			
//			    			System.out.println(requestParam);
//			    			System.out.println(columnMapping);
//			    			System.out.println(modelAttributes);
//			    			System.out.println(classMethod);
			    			
		    				str += "@Insert(\""+sqlInsert+sqlInsertValues+"\")\n";
		    				str += "void insert("+table+" "+StringUtils.uncapitalize(table)+");\n\n";
		    				str += "@Update(\""+sqlUpdate+" WHERE "+primaryKey+"_"+tmp.replace(tblPrefix, "")+"=#{"+primaryKey+"}\")\n";
		    				str += "void update("+table+" "+StringUtils.uncapitalize(table)+");\n\n";
		    				str += "@Delete(\"DELETE FROM "+tmp+" WHERE ${clause}\")\n";
		    				str += "void deleteBatch(QueryParameter param);\n\n";
		    				str += "@Delete(\"DELETE FROM "+tmp+" WHERE "+primaryKey+"_"+tmp.replace(tblPrefix, "")+"=#{"+primaryKey+table+"}\")\n";
		    				str += "void delete("+table+" "+StringUtils.uncapitalize(table)+");\n\n";
//		    				str += "@Delete(\"INSERT INTO "+tmp+" SELECT * FROM "+tmp+"_test\")\n";
//		    				str += "void copyTest();\n\n";
			    			
			    			str += "List<"+table+"> getList(QueryParameter param);\n\n";
			    			str += table+" getEntity(String "+primaryKey+");\n\n";
			    			str += "long getCount(QueryParameter param);\n\n";
			    			str += "String getNewId();\n\n";
			    			
			    			
			    			str += xmlMapping+"\n\n";
			    			
			    			str += setParam+"\n";
			    			str += requestParam+"\n";
			    			str += columnMapping+"\n";
			    			str += modelAttributes+"\n";
			    			str += "public "+table+"() {\n\n}"+"\n";
			    			str += "public "+table+"(String "+primaryKey+") {\n\tthis."+primaryKey+" = "+primaryKey+";\n}\n\n";
			    			str += classMethod+"\n";
			    			str += "\n\n\n/**********************************************************************/\n\n";
			    			
			    			columnMapping = "";
			    		    modelAttributes = "";
			    		    classMethod = "";
		    			}
		    			
		    			tmp = rs.getString("TABLE");
		    			sqlUpdate = "UPDATE " + rs.getString("TABLE") + " SET "+rs.getString("FIELD")+columnSufix + "=#{"+ field + type + "}";
		    			
		    			sqlInsert = "INSERT INTO " + rs.getString("TABLE") + " ("+rs.getString("FIELD")+columnSufix;
		    			
		    			sqlInsertValues = " VALUES (#{"+ field + type + "}";
		    			
		    			if(requestType.contains("Optional")) {
		    				setParam = "if("+field+".isPresent()) data.set"+fieldCap+"("+field+".get());\n";	
		    			}else {
		    				setParam = "data.set"+fieldCap+"("+field+");\n";
		    			}
		    			
		    			requestParam = "@RequestParam(\"" + rs.getString("FIELD") + "\") "+requestType+" "+field+",\n";
		    			
		    			xmlMapping = "<result column=\""+rs.getString("FIELD")+columnSufix+"\" property=\""+field+"\"/>\n";
		    		}else {
		    			
		    			sqlUpdate += ", "+rs.getString("FIELD")+columnSufix + "=#{"+ field + type + "}";
		    			
		    			sqlInsert += ", "+ rs.getString("FIELD")+columnSufix;
		    			sqlInsertValues += ", #{"+ field + type + "}";
		    			
		    			if(requestType.contains("Optional")) {
		    				setParam += "if("+field+".isPresent()) data.set"+fieldCap+"("+field+".get());\n";	
		    			}else {
		    				setParam += "data.set"+fieldCap+"("+field+");\n";
		    			}
		    			
		    			requestParam += "@RequestParam(\"" + rs.getString("FIELD") + "\") "+requestType+" "+field+",\n";
		    			
		    			xmlMapping += "<result column=\""+rs.getString("FIELD")+columnSufix+"\" property=\""+field+"\"/>\n";
		    		}
		    		
		    		if(javaType.toLowerCase().equals("boolean")){
		    			classMethod += "@JsonProperty(\""+rs.getString("FIELD")+"\")\npublic " + javaType + " is"+fieldCap+"() {\n\treturn "+field+";\n}\n\n";
		    		}else {
		    			classMethod += "@JsonProperty(\""+rs.getString("FIELD")+"\")\npublic " + javaType + " get"+fieldCap+"() {\n\treturn "+field+";\n}\n\n";
		    		}
		    		
		    		classMethod += "public void set"+fieldCap+"("+javaType+" "+field+") {\n\tthis."+field+" = "+field+";\n}\n\n";
		    		modelAttributes += "private " + javaType + " " + field + ";\n";
		    		columnMapping += "public static final String " + rs.getString("FIELD").toUpperCase() + " = \""+rs.getString("FIELD")+columnSufix+"\";\n";
		    }
		    
		    sqlInsert += ")";
		    sqlInsertValues += ")";
//			System.out.println("@Insert(\""+sqlInsert+sqlInsertValues+"\")");
//			System.out.println("void insert("+table+" "+StringUtils.uncapitalize(table)+");\n");
//			System.out.println("@Update(\""+sqlUpdate+" WHERE id=#{id}\")");
//			System.out.println("void update("+table+" "+StringUtils.uncapitalize(table)+");\n");
//			System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE ${clause}\")");
//			System.out.println("void deleteBatch(QueryParameter param);\n");
//			System.out.println("@Delete(\"DELETE FROM "+tmp+" WHERE id=#{id}\")");
//			System.out.println("void delete("+table+" "+StringUtils.uncapitalize(table)+");\n");
//			
//			System.out.println("List<"+table+"> getList(QueryParameter param);\n");
//			System.out.println(table+" getEntity(String id);\n");
//			System.out.println("long getCount(QueryParameter param);\n");
//			
//			System.out.println(requestParam);
//			System.out.println(columnMapping);
//			System.out.println(modelAttributes);
//			System.out.println(classMethod);
			
		    str += "@Insert(\""+sqlInsert+sqlInsertValues+"\")\n";
			str += "void insert("+table+" "+StringUtils.uncapitalize(table)+");\n\n";
			str += "@Update(\""+sqlUpdate+" WHERE "+primaryKey+"_"+tmp.replace(tblPrefix, "")+"=#{"+primaryKey+"}\")\n";
			str += "void update("+table+" "+StringUtils.uncapitalize(table)+");\n\n";
			str += "@Delete(\"DELETE FROM "+tmp+" WHERE ${clause}\")\n";
			str += "void deleteBatch(QueryParameter param);\n\n";
			str += "@Delete(\"DELETE FROM "+tmp+" WHERE "+primaryKey+"_"+tmp.replace(tblPrefix, "")+"=#{"+primaryKey+"}\")\n";
			str += "void delete("+table+" "+StringUtils.uncapitalize(table)+");\n\n";
//			str += "@Delete(\"INSERT INTO "+tmp+" SELECT * FROM "+tmp+"_test\")\n";
//			str += "void copyTest();\n\n";
			
			str += "List<"+table+"> getList(QueryParameter param);\n\n";
			str += table+" getEntity(String "+primaryKey+");\n\n";
			str += "long getCount(QueryParameter param);\n\n";
			str += "String getNewId();\n\n";
			
			str += xmlMapping+"\n\n";
			
			str += setParam+"\n";
			str += requestParam+"\n";
			str += columnMapping+"\n";
			str += modelAttributes+"\n";
			str += "public "+table+"() {\n\n}\n\n";
			str += "public "+table+"(String "+primaryKey+") {\n\tthis."+primaryKey+" = "+primaryKey+";\n}\n\n";
			str += classMethod+"\n";
			str += "\n/**********************************************************************/\n\n";
			
			System.out.println(str);
			
			/*
			FileOutputStream outputStream = new FileOutputStream("/Users/gustaroska/workspace-sts/tsl-sats-dn/output.txt");
			
		    byte[] strToBytes = str.getBytes();
		    outputStream.write(strToBytes);
		 
		    outputStream.close();
		    */

		} catch (Exception ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		}
	}
}
