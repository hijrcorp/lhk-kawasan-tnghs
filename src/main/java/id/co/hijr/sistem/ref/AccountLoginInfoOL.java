package id.co.hijr.sistem.ref;

public enum AccountLoginInfoOL {
	ACCOUNT(0),POSITION(1), ORGANIZATION(2);

	private int id;

	private AccountLoginInfoOL(final int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}

}
