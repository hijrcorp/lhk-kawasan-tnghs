package id.co.hijr.sistem.ref;

public enum StatusAssignment {
	PERMANENT("PERMANENT"), TEMPORARY("TEMPORARY"),ACTING("ACTING");

	private String id;

	private StatusAssignment(final String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
