package id.co.hijr.sistem.ref;

public enum TypeApplicationClient {
	WEB("web"), MOBILE("mobile"),DESKTOP("desktop");

	private String id;

	private TypeApplicationClient(final String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
