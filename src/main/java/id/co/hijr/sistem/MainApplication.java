package id.co.hijr.sistem;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@SpringBootApplication
@ComponentScan({"id.co.hijr.sistem","id.co.hijr.ticket"})
@EnableAsync
@MapperScan(value={"id.co.hijr.sistem.mapper","id.co.hijr.ticket.mapper"}, sqlSessionTemplateRef="primarySessionTemplate")

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableResourceServer
public class MainApplication extends SpringBootServletInitializer {

	public static final String DATASOURCE = "PrimaryDS";
	public static final String SESSION_TEMPLATE = "primarySessionTemplate";
	public static final String SESSION_FACTORY = "primarySessionFactory";
	public static final String TRANSACTION_MANAGAER = "primaryTransactionManager";

	
	@Value("${locale.cookie.name}")
	protected String localeCookieName;

	
	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
	
	@Bean(name = DATASOURCE, destroyMethod = "")
	@ConfigurationProperties(prefix = "spring.datasource.primary")
	@Primary
	public DataSource primaryDataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name = SESSION_TEMPLATE, destroyMethod = "")
	@Primary
	public SqlSessionTemplate primarySessionTemplate(@Named(DATASOURCE) final DataSource primaryDataSource) throws Exception {
		return new SqlSessionTemplate(primarySqlSessionFactory(primaryDataSource));
	}
	

	@Bean(name = TRANSACTION_MANAGAER, destroyMethod = "")
	@Primary
	public DataSourceTransactionManager primaryTransactionManager(@Named(DATASOURCE) final DataSource sistemDataSource) {
		return new DataSourceTransactionManager(sistemDataSource);
	}
	

	@Bean(name = SESSION_FACTORY, destroyMethod = "")
	@Primary
	public SqlSessionFactory primarySqlSessionFactory(@Named(DATASOURCE) final DataSource primaryDataSource) throws Exception {
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(primaryDataSource);
		sessionFactory.setConfigLocation(new ClassPathResource("sistem-mybatis-config.xml"));
		sessionFactory.getObject().getConfiguration().setMapUnderscoreToCamelCase(true);
		sessionFactory.getObject().getConfiguration().setDefaultFetchSize(100);
		sessionFactory.getObject().getConfiguration().setDefaultStatementTimeout(30);
		return sessionFactory.getObject();
	}
	

	@Bean
	public InternalResourceViewResolver resolver() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setPrefix("/WEB-INF/jsp/");
		vr.setSuffix(".jsp");
		//vr.setViewClass(JstlView.class);
		return vr;
	}
	
	@Bean
    public LocaleResolver localeResolver () {
        CookieLocaleResolver r = new CookieLocaleResolver();
        r.setDefaultLocale(Locale.US);
        r.setCookieName(localeCookieName);
        //r.setCookieMaxAge(24*60*60);
        return r;
    }
	
	@Bean(name = "messageSource")
	public MessageSource messageSource() {
		
	     ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
	     messageSource.setBasename("classpath:msgs/msg");
	     return messageSource;
	}

	
	@Bean
    public WebMvcConfigurer configurer () {
        return new WebMvcConfigurer() {
            @Override
            public void addInterceptors (InterceptorRegistry registry) {
                LocaleChangeInterceptor l = new LocaleChangeInterceptor();
                l.setParamName("lang");
                registry.addInterceptor(l);
            }
        };
    }
	
	/**
	 * di pake untuk deploy jadi war
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MainApplication.class);
	}


    @PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));   // It will set UTC timezone
        System.out.println("Spring boot application running in UTC timezone :"+new Date());   // It will print UTC timezone
    }
    
	public static void main(String[] args) throws Exception {
		SpringApplication.run(MainApplication.class, args);
	}
}
