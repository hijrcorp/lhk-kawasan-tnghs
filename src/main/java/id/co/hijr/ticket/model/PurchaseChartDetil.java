package id.co.hijr.ticket.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

import id.co.hijr.ticket.ref.StatusPurchase;

public class PurchaseChartDetil {
	private Integer monthYear;
	private Integer countGroup;
	private Integer countTicket;
	private Integer amountTicket;
	
	public PurchaseChartDetil() {
		
	}
	public PurchaseChartDetil(Integer monthYear, Integer countGroup, Integer countTicket, Integer amountTicket) {
		this.monthYear = monthYear;
		this.countGroup = countGroup;
		this.countTicket = countTicket;
		this.amountTicket = amountTicket;
	}
	@JsonProperty("month_year")
	public Integer getMonthYear() {
		return monthYear;
	}

	@JsonProperty("count_group")
	public Integer getCountGroup() {
		return countGroup;
	}

	@JsonProperty("count_ticket")
	public Integer getCountTicket() {
		return countTicket;
	}

	@JsonProperty("amount_ticket")
	public Integer getAmountTicket() {
		return amountTicket;
	}

	public void setMonthYear(Integer monthYear) {
		this.monthYear = monthYear;
	}

	public void setCountGroup(Integer countGroup) {
		this.countGroup = countGroup;
	}

	public void setCountTicket(Integer countTicket) {
		this.countTicket = countTicket;
	}

	public void setAmountTicket(Integer amountTicket) {
		this.amountTicket = amountTicket;
	}
	
	
}
