package id.co.hijr.ticket.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegionFacility {
	
	public static final String ID = "id_region_facility";
	public static final String HEADER_ID = "header_id_region_facility";
	public static final String CODE = "code_region_facility";
	public static final String ITEM = "item_region_facility";
	public static final String PRICE = "price_region_facility";
	public static final String INCLUDE = "include_region_facility";

	private String id;
	private String headerId;
	private String code;
	private String item;
	private Double price;
	private String include;

	public RegionFacility() {

	}

	public RegionFacility(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}
	
	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("item")
	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@JsonProperty("price")
	public Double getPrice() {
		return price;
	}
	
	public void setPrice(Double price) {
		this.price = price;
	}

	@JsonProperty("include")
	public String isInclude() {
		return include;
	}

	public void setInclude(String include) {
		this.include = include;
	}


	/**********************************************************************/


}
