package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Luggage {

	public static final String ID = "id_luggage";
	public static final String NAME = "name_luggage";
	public static final String TYPE = "type_luggage";

	private String id;
	private String name;
	private String type;

	public Luggage() {

	}

	public Luggage(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	/**********************************************************************/

}
