package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.sistem.common.JsonDateTimeSerializer;

public class PurchaseDetilPayment {

	public static final String ID = "id_purchase_detil_payment";
	public static final String ID_HEADER = "id_header_purchase_detil_payment";
	public static final String CODE = "code_purchase_detil_payment";
	public static final String NAME = "name_purchase_detil_payment";
	public static final String PRICE = "price_purchase_detil_payment";
	public static final String STATUS = "status_purchase_detil_payment";

	private String id;
	private String idHeader;
	private String code;
	private String name;
	private Double price;
	private Double status;

	public PurchaseDetilPayment() {

	}

	public PurchaseDetilPayment(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_header")
	public String getIdHeader() {
		return idHeader;
	}

	public void setIdHeader(String idHeader) {
		this.idHeader = idHeader;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@JsonProperty("status")
	public Double getStatus() {
		return status;
	}

	public void setStatus(Double status) {
		this.status = status;
	}


	/**********************************************************************/

	private Purchase purchase;

	
	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	
}
