package id.co.hijr.ticket.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.sistem.common.JsonTimeSerializer;
import id.co.hijr.sistem.model.File;

public class RegionPhoto {
	public static final String ID = "id_region_photo";
	public static final String HEADER_ID = "header_id_region_photo";
	public static final String FILE_ID = "file_id_region_photo";
	public static final String ACTIVE = "active_region_photo";

	private String id;
	private String headerId;
	private String fileId;
	private Boolean active;

	public RegionPhoto() {

	}

	public RegionPhoto(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("file_id")
	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	@JsonProperty("active")
	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}



	/**********************************************************************/
	private File list_photo;

	private String nameFile;


	@JsonProperty("name_file")
	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	public File getList_photo() {
		return list_photo;
	}

	public void setList_photo(File list_photo) {
		this.list_photo = list_photo;
	}
	
//	@JsonSerialize(using=JsonTimeSerializer.class)
//	@JsonProperty("time_serialize")
//	public Date getTimeSerialize() {
//		return time;
//	}
}
