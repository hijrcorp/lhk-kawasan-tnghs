package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseDetilVisitor {

	public static final String ID = "id_purchase_detil_visitor";
	public static final String HEADER_ID = "header_id_purchase_detil_visitor";
	public static final String RESPONSIBLE = "responsible_purchase_detil_visitor";
	public static final String ID_VISITOR_IDENTITY = "id_visitor_identity_purchase_detil_visitor";
	public static final String PHONE_NUMBER = "phone_number_purchase_detil_visitor";
	public static final String EMAIL = "email_purchase_detil_visitor";
	public static final String ADDRESS = "address_purchase_detil_visitor";
	public static final String PHONE_NUMBER_FAMILY = "phone_number_family_purchase_detil_visitor";
	public static final String ADDRESS_FAMILY = "address_family_purchase_detil_visitor";
	public static final String IS_VALID = "is_valid_purchase_detil_visitor";
	public static final String STATUS = "status_purchase_detil_visitor";
	public static final String PARENT_HEADER_ID = "parent_header_id_purchase_detil_visitor";
	
//	public static final String TYPE_IDENTITY = "type_identity_purchase_detil_visitor";
//	public static final String FULL_NAME = "full_name_purchase_detil_visitor";
//	public static final String FILE_ID = "file_id_purchase_detil_visitor";
//	public static final String BIRTHDATE = "birthdate_purchase_detil_visitor";
//	public static final String GENDER = "gender_purchase_detil_visitor";

	private String id;
	private String headerId;
	private String responsible;
	private String idVisitorIdentity;
	private String phoneNumber;
	private String email;
	private String address;
	private String phoneNumberFamily;
	private String addressFamily;
	private Boolean isValid;
	private String status;
	private String parentHeaderId;

	public PurchaseDetilVisitor() {

	}

	public PurchaseDetilVisitor(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("id_visitor_identity")
	public String getIdVisitorIdentity() {
		return idVisitorIdentity;
	}
	
	public void setIdVisitorIdentity(String idVisitorIdentity) {
		this.idVisitorIdentity = idVisitorIdentity;
	}

	@JsonProperty("phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonProperty("responsible")
	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	@JsonProperty("phone_number_family")
	public String getPhoneNumberFamily() {
		return phoneNumberFamily;
	}
	
	public void setPhoneNumberFamily(String phoneNumberFamily) {
		this.phoneNumberFamily = phoneNumberFamily;
	}
	
	@JsonProperty("address_family")
	public String getAddressFamily() {
		return addressFamily;
	}
	
	public void setAddressFamily(String addressFamily) {
		this.addressFamily = addressFamily;
	}
	
	@JsonProperty("is_valid")
	public Boolean isIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@JsonProperty("parent_header_id")
	public String getParentHeaderId() {
		return parentHeaderId;
	}

	public void setParentHeaderId(String parentHeaderId) {
		this.parentHeaderId = parentHeaderId;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
//	@JsonProperty("type_identity")
//	public String getTypeIdentity() {
//		return typeIdentity;
//	}
//
//	public void setTypeIdentity(String typeIdentity) {
//		this.typeIdentity = typeIdentity;
//	}
//
//	@JsonProperty("full_name")
//	public String getFullName() {
//		return fullName;
//	}
//
//	public void setFullName(String fullName) {
//		this.fullName = fullName;
//	}
//	
//	@JsonProperty("file_id")
//	public String getFileId() {
//		return fileId;
//	}
//	
//	public void setFileId(String fileId) {
//		this.fileId = fileId;
//	}
//	
//	
//	@JsonProperty("birthdate")
//	public Date getBirthdate() {
//		return birthdate;
//	}
//
//	public void setBirthdate(Date birthdate) {
//		this.birthdate = birthdate;
//	}
//	
//	@JsonProperty("gender")
//	public String getGender() {
//		return gender;
//	}
//
//	public void setGender(String gender) {
//		this.gender = gender;
//	}

	/**********************************************************************/
	VisitorIdentity visitorIdentity = new VisitorIdentity();

	public VisitorIdentity getVisitorIdentity() {
		return visitorIdentity;
	}

	public void setVisitorIdentity(VisitorIdentity visitorIdentity) {
		this.visitorIdentity = visitorIdentity;
	}
	
	
//	private String nameFile;
//	
//	@JsonProperty("name_file")
//	public String getNameFile() {
//		return nameFile;
//	}
//
//	public void setNameFile(String nameFile) {
//		this.nameFile = nameFile;
//	}

}
