package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseDetilTransitCamp {

	public static final String ID = "id_purchase_detil_transit_camp";
	public static final String HEADER_ID = "header_id_purchase_detil_transit_camp";
	public static final String ID_TRANSIT_CAMP = "id_transit_camp_purchase_detil_transit_camp";
	public static final String NAME_TRANSIT_CAMP = "name_transit_camp_purchase_detil_transit_camp";
	public static final String REPORT_TIME_CAMP = "report_time_camp_purchase_detil_transit_camp";
	
	public static final String TYPE_CAMP = "type_camp_purchase_detil_transit_camp";
	public static final String COUNT_CAMP = "count_camp_purchase_detil_transit_camp";
	public static final String CODE_UNIQUE = "code_unique_purchase_detil_transit_camp";
	public static final String STATUS = "status_purchase_detil_transit_camp";

	private String id;
	private String headerId;
	private String idTransitCamp;
	private String nameTransitCamp;
	private String reportTimeCamp;
	private String typeCamp;
	private Integer countCamp;
	private String codeUnique;
	private String status;

	public PurchaseDetilTransitCamp() {

	}

	public PurchaseDetilTransitCamp(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("id_transit_camp")
	public String getIdTransitCamp() {
		return idTransitCamp;
	}

	public void setIdTransitCamp(String idTransitCamp) {
		this.idTransitCamp = idTransitCamp;
	}

	@JsonProperty("name_transit_camp")
	public String getNameTransitCamp() {
		return nameTransitCamp;
	}

	public void setNameTransitCamp(String nameTransitCamp) {
		this.nameTransitCamp = nameTransitCamp;
	}
	
	@JsonProperty("report_time_camp")
	public String getReportTimeCamp() {
		return reportTimeCamp;
	}

	public void setReportTimeCamp(String reportTimeCamp) {
		this.reportTimeCamp = reportTimeCamp;
	}

	@JsonProperty("type_camp")
	public String getTypeCamp() {
		return typeCamp;
	}

	public void setTypeCamp(String typeCamp) {
		this.typeCamp = typeCamp;
	}

	@JsonProperty("count_camp")
	public Integer getCountCamp() {
		return countCamp;
	}

	public void setCountCamp(Integer countCamp) {
		this.countCamp = countCamp;
	}

	@JsonProperty("code_unique")
	public String getCodeUnique() {
		return codeUnique;
	}

	public void setCodeUnique(String codeUnique) {
		this.codeUnique = codeUnique;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	/**********************************************************************/
	

//	private Purchase purchase;
//	
//
//	public Purchase getPurchase() {
//		return purchase;
//	}
//
//	public void setPurchase(Purchase purchase) {
//		this.purchase = purchase;
//	}

}
