package id.co.hijr.ticket.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

import id.co.hijr.ticket.ref.StatusPurchase;

public class RequestPurchase {

	public static final String ID = "id_request_purchase";
	public static final String ACCESS_TOKEN = "access_token_request_purchase";
	public static final String PAYMENT_CODE = "payment_code_request_purchase";
	public static final String ID_HEADER_PURCHASE = "id_header_purchase_request_purchase";
	public static final String SUBMIT_DATE = "submit_date_request_purchase";

	private String id;
	private String accessToken;
	private String paymentCode;
	private String idHeaderPurchase;
	private Date submitDate;

	public RequestPurchase() {

	}

	public RequestPurchase(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("access_token")
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	@JsonProperty("payment_code")
	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	@JsonProperty("id_header_purchase")
	public String getIdHeaderPurchase() {
		return idHeaderPurchase;
	}

	public void setIdHeaderPurchase(String idHeaderPurchase) {
		this.idHeaderPurchase = idHeaderPurchase;
	}

	@JsonProperty("submit_date")
	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}



	/**********************************************************************/

	
}
