package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.itextpdf.text.List;

import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

public class MasterPayment {

	public static final String ID = "id_master_payment";
	public static final String CODE = "code_master_payment";
	public static final String NAME = "name_master_payment";
	public static final String PRICE = "price_master_payment";
	public static final String STATUS = "status_master_payment";

	private String id;
	private String code;
	private String name;
	private Double price;
	private String status;
	private Integer qty;

	public MasterPayment() {

	}

	public MasterPayment(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("qty")
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	
}
