package id.co.hijr.ticket.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

import id.co.hijr.ticket.ref.StatusPurchase;

public class PurchaseChart {

	public static final String ID = "id_purchase";
	public static final String REGION_ID = "region_id_purchase";
	public static final String REGION_NAME = "region_name_purchase";

	private String regionName;
	
	List<PurchaseChartDetil> regionPurchase = new ArrayList<>();

	public PurchaseChart() {

	}
	
	@JsonProperty("region_name")
	public String getRegionName() {
		return regionName;
	}
	
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	
	@JsonProperty("region_purchase")
	public List<PurchaseChartDetil> getRegionPurchase() {
		return regionPurchase;
	}

	public void setRegionPurchase(List<PurchaseChartDetil> regionPurchase) {
		this.regionPurchase = regionPurchase;
	}
	
	
}
