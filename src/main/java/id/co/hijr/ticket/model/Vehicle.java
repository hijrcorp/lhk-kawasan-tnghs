package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Vehicle {

	public static final String ID = "id_vehicle";
	public static final String NAME = "name_vehicle";
	public static final String TYPE_ID = "type_id_vehicle";
	public static final String COST = "cost_vehicle";
	public static final String PHOTO = "photo_vehicle";
	public static final String SEAT_CAPACITY = "seat_capacity_vehicle";
	public static final String SUITCASE = "suitcase_vehicle";
	public static final String DESCRIPTION = "description_vehicle";
	public static final String PROVIDED = "provided_vehicle";
	public static final String ORGANIZATION_ID = "organization_id_vehicle";

	private String id;
	private String name;
	private String typeId;
	private Double cost;
	private String photo;
	private Integer seatCapacity;
	private Integer suitcase;
	private String description;
	private String provided;
	private String organizationId;

	public Vehicle() {

	}

	public Vehicle(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type_id")
	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	@JsonProperty("cost")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	@JsonProperty("photo")
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@JsonProperty("seat_capacity")
	public Integer getSeatCapacity() {
		return seatCapacity;
	}

	public void setSeatCapacity(Integer seatCapacity) {
		this.seatCapacity = seatCapacity;
	}

	@JsonProperty("suitcase")
	public Integer getSuitcase() {
		return suitcase;
	}

	public void setSuitcase(Integer suitcase) {
		this.suitcase = suitcase;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@JsonProperty("provided")
	public String getProvided() {
		return provided;
	}

	public void setProvided(String provided) {
		this.provided = provided;
	}



	/**********************************************************************/

	private String typeName;
	@JsonProperty("type_name")
	public String getTypeName() {
		return typeName;
	}
	
}
