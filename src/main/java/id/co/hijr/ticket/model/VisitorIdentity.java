package id.co.hijr.ticket.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

import id.co.hijr.ticket.ref.StatusPurchase;

public class VisitorIdentity {

	public static final String ID = "id_visitor_identity";
	public static final String ID_HEADER = "id_header_visitor_identity";
	public static final String NO_IDENTITY = "no_identity_visitor_identity";
	public static final String TYPE_IDENTITY = "type_identity_visitor_identity";
	public static final String FULL_NAME = "full_name_visitor_identity";
	public static final String GENDER = "gender_visitor_identity";
	public static final String BIRTHDATE = "birthdate_visitor_identity";
	public static final String ADDRESS = "address_visitor_identity";
	public static final String ID_FILE = "id_file_visitor_identity";
	public static final String PHONE_NUMBER = "phone_number_visitor_identity";
	public static final String EMAIL = "email_visitor_identity";
	public static final String PHONE_NUMBER_FAMILY = "phone_number_family_visitor_identity";
	public static final String ADDRESS_FAMILY = "address_family_visitor_identity";
	public static final String STATUS_VALID = "status_valid_visitor_identity";
	public static final String DATE_BANNED = "date_banned_visitor_identity";
	public static final String DAYS_BANNED = "days_banned_visitor_identity";

	private String id;
	private String idHeader;
	private String noIdentity;
	private String typeIdentity;
	private String fullName;
	private String gender;
	private Date birthdate;
	private String address;
	private String idFile;
	private String phoneNumber;
	private String email;
	private String phoneNumberFamily;
	private String addressFamily;
	private String statusValid;
	//new
	private Date dateBanned;
	private Integer daysBanned;

	public VisitorIdentity() {

	}

	public VisitorIdentity(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("id_header")
	public String getIdHeader() {
		return idHeader;
	}

	public void setIdHeader(String idHeader) {
		this.idHeader = idHeader;
	}

	@JsonProperty("no_identity")
	public String getNoIdentity() {
		return noIdentity;
	}

	public void setNoIdentity(String noIdentity) {
		this.noIdentity = noIdentity;
	}

	@JsonProperty("type_identity")
	public String getTypeIdentity() {
		return typeIdentity;
	}

	public void setTypeIdentity(String typeIdentity) {
		this.typeIdentity = typeIdentity;
	}

	@JsonProperty("full_name")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("birthdate")
	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("id_file")
	public String getIdFile() {
		return idFile;
	}

	public void setIdFile(String idFile) {
		this.idFile = idFile;
	}

	@JsonProperty("phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("phone_number_family")
	public String getPhoneNumberFamily() {
		return phoneNumberFamily;
	}

	public void setPhoneNumberFamily(String phoneNumberFamily) {
		this.phoneNumberFamily = phoneNumberFamily;
	}

	@JsonProperty("address_family")
	public String getAddressFamily() {
		return addressFamily;
	}

	public void setAddressFamily(String addressFamily) {
		this.addressFamily = addressFamily;
	}

	@JsonProperty("status_valid")
	public String getStatusValid() {
		return statusValid;
	}

	public void setStatusValid(String statusValid) {
		this.statusValid = statusValid;
	}
	
	@JsonProperty("date_banned")
	public Date getDateBanned() {
		return dateBanned;
	}

	public void setDateBanned(Date dateBanned) {
		this.dateBanned = dateBanned;
	}

	@JsonProperty("days_banned")
	public Integer getDaysBanned() {
		return daysBanned;
	}

	public void setDaysBanned(Integer daysBanned) {
		this.daysBanned = daysBanned;
	}
	
	public Date getDateUnlock() {
		int interval = (daysBanned==null?0:daysBanned);
		Date dt = new Date();
	   	 Calendar c = Calendar.getInstance(); 
	   	 c.setTime(dt); 
	   	 c.add(Calendar.DATE, interval);
	   	 dt = c.getTime();
	   	 return dt;
	}

	/**********************************************************************/


	
	
	private String fileName;

	
	@JsonProperty("file_name")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
