package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeVehicle {

	public static final String ID = "id_type_vehicle";
	public static final String NAME = "name_type_vehicle";

	private String id;
	private String name;

	public TypeVehicle() {

	}

	public TypeVehicle(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	/**********************************************************************/

}
