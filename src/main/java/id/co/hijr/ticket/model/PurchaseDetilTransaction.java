package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.sistem.common.JsonDateTimeSerializer;

public class PurchaseDetilTransaction {

	public static final String ID = "id_purchase_detil_transaction";
	public static final String HEADER_ID = "header_id_purchase_detil_transaction";
	public static final String PAYMENT_METHOD = "payment_method_purchase_detil_transaction";
	public static final String PAYMENT_CODE = "payment_code_purchase_detil_transaction";
	public static final String BANK_ID = "bank_id_purchase_detil_transaction";
	public static final String SUBMITED_DATE = "submited_date_purchase_detil_transaction";
	public static final String CONFIRMATION_PAYMENT = "confirmation_payment_purchase_detil_transaction";
	public static final String FILE_TRANSACTION = "file_transaction_purchase_detil_transaction";
	public static final String TRANSACTION_DATE = "transaction_date_purchase_detil_transaction";

	public static final String ACCOUNT_NAME_SENDER = "account_name_sender_purchase_detil_transaction";
	public static final String ACCOUNT_NUMBER_SENDER = "account_number_sender_purchase_detil_transaction";
	public static final String BANK_NAME_SENDER = "bank_name_sender_purchase_detil_transaction";
	public static final String ACCOUNT_NAME_RECEIVER = "account_name_receiver_purchase_detil_transaction";
	public static final String ACCOUNT_NUMBER_RECEIVER = "account_number_receiver_purchase_detil_transaction";
	public static final String BANK_NAME_RECEIVER = "bank_name_receiver_purchase_detil_transaction";

	private String id;
	private String headerId;
	private String paymentMethod;
	private String paymentCode;
	private String bankId;
	private Date submitedDate;
	private Integer confirmationPayment;
	private String fileTransaction;
	private Date transactionDate;

	private String accountNameSender;
	private String accountNumberSender;
	private String bankNameSender;
	private String accountNameReceiver;
	private String accountNumberReceiver;
	private String bankNameReceiver;

	public PurchaseDetilTransaction() {

	}

	public PurchaseDetilTransaction(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("payment_method")
	public String getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	@JsonProperty("payment_code")
	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	@JsonProperty("bank_id")
	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	@JsonProperty("submited_date")
	public Date getSubmitedDate() {
		return submitedDate;
	}

	public void setSubmitedDate(Date submitedDate) {
		this.submitedDate = submitedDate;
	}

	@JsonProperty("confirmation_payment")
	public Integer getConfirmationPayment() {
		return confirmationPayment;
	}

	public void setConfirmationPayment(Integer confirmationPayment) {
		this.confirmationPayment = confirmationPayment;
	}

	@JsonProperty("file_transaction")
	public String getFileTransaction() {
		return fileTransaction;
	}

	public void setFileTransaction(String fileTransaction) {
		this.fileTransaction = fileTransaction;
	}

	@JsonSerialize(using=JsonDateTimeSerializer.class)
	@JsonProperty("transaction_date_serialize")
	public Date getTransactionDateSerialize() {
		return transactionDate;
	}
	
	@JsonProperty("transaction_date")
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@JsonProperty("account_name_sender")
	public String getAccountNameSender() {
		return accountNameSender;
	}
	
	public void setAccountNameSender(String accountNameSender) {
		this.accountNameSender = accountNameSender;
	}
	
	@JsonProperty("account_number_sender")
	public String getAccountNumberSender() {
		return accountNumberSender;
	}
	
	public void setAccountNumberSender(String accountNumberSender) {
		this.accountNumberSender = accountNumberSender;
	}
	
	@JsonProperty("bank_name_sender")
	public String getBankNameSender() {
		return bankNameSender;
	}
	
	public void setBankNameSender(String bankNameSender) {
		this.bankNameSender = bankNameSender;
	}
	
	@JsonProperty("account_name_receiver")
	public String getAccountNameReceiver() {
		return accountNameReceiver;
	}
	
	public void setAccountNameReceiver(String accountNameReceiver) {
		this.accountNameReceiver = accountNameReceiver;
	}
	
	@JsonProperty("account_number_receiver")
	public String getAccountNumberReceiver() {
		return accountNumberReceiver;
	}
	
	public void setAccountNumberReceiver(String accountNumberReceiver) {
		this.accountNumberReceiver = accountNumberReceiver;
	}
	
	@JsonProperty("bank_name_receiver")
	public String getBankNameReceiver() {
		return bankNameReceiver;
	}
	
	public void setBankNameReceiver(String bankNameReceiver) {
		this.bankNameReceiver = bankNameReceiver;
	}


	/**********************************************************************/


	public static final String FILE_ID = "file_id_purchase_detil_transaction";
	public static final String FILE_UPLOAD_DATE = "file_upload_date_purchase_detil_transaction";

	private Purchase purchase;
	private Bank bank;

	private Date expiredDate;
	private Date timeleft;
	private Integer timeleftSecond;
	private Integer timeleftSecond2;
	private Integer statusExpired;
	

	private String fileId;
	private Date fileUploadDate;
	private String fileName;

	
	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Date getTimeleft() {
		return timeleft;
	}

	public void setTimeleft(Date timeleft) {
		this.timeleft = timeleft;
	}
	
	public Integer getTimeleftSecond() {
		return timeleftSecond;
	}

	public void setTimeleftSecond(Integer timeleftSecond) {
		this.timeleftSecond = timeleftSecond;
	}
	
	public Integer getTimeleftSecond2() {
		return timeleftSecond2;
	}

	public void setTimeleftSecond2(Integer timeleftSecond2) {
		this.timeleftSecond2 = timeleftSecond2;
	}

	public Integer getStatusExpired() {
		return statusExpired;
	}

	public void setStatusExpired(Integer statusExpired) {
		this.statusExpired = statusExpired;
	}

	@JsonProperty("file_id")
	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	@JsonProperty("file_upload_date")
	public Date getFileUploadDate() {
		return fileUploadDate;
	}

	public void setFileUploadDate(Date fileUploadDate) {
		this.fileUploadDate = fileUploadDate;
	}

	@JsonProperty("file_name")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
