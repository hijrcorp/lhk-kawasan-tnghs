package id.co.hijr.ticket.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

import id.co.hijr.ticket.ref.StatusPurchase;

public class Purchase {

	public static final String ID = "id_purchase";
	public static final String PARENT_ID = "parent_id_purchase";
	public static final String REGION_ID = "region_id_purchase";
	public static final String REGION_NAME = "region_name_purchase";
	public static final String START_DATE = "start_date_purchase";
	public static final String END_DATE = "end_date_purchase";
	public static final String DATETIME = "datetime_purchase";
	public static final String ACCOUNT_ID = "account_id_purchase";
	public static final String COUNT_TICKET = "count_ticket_purchase";
	public static final String AMOUNT_TICKET = "amount_ticket_purchase";
	public static final String AMOUNT_TOTAL_TICKET = "amount_total_ticket_purchase";
	public static final String NUMBER_UNIQUE = "number_unique_purchase";
	public static final String STATUS = "status_purchase";
	public static final String STATUS_BOOKING = "status_booking_purchase";
	public static final String CODE_BOOKING = "code_booking_purchase";
	public static final String STATUS_BOARDING = "status_boarding_purchase";
	public static final String STATUS_RESCHEDULE = "status_reschedule_purchase";

	private String id;
	private String parentId;
	private String regionId;
	private String regionName;
	private Date startDate;
	private Date endDate;
	private Date datetime;
	private String accountId;
	private Integer countTicket;
	private Integer countTicketWni;
	private Integer countTicketWna;
	private Integer totalDays;
	private Double amountTicket;
	private Double amountTicketWni;
	private Double amountTicketWna;
	private Double amountTotalTicket;
	private Integer numberUnique;
	private String status;
	private String statusBooking;
	private String codeBooking;
	private String statusBoarding;
	private String statusReschedule;
	private Integer disclaimer;
	private String regionEndId;
	private String regionEndName;

	public Purchase() {

	}

	public Purchase(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	@JsonProperty("parent_id")
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("region_id")
	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	@JsonProperty("region_name")
	public String getRegionName() {
		return regionName;
	}
	
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@JsonSerialize(using=JsonDateTimeSerializer.class)
	@JsonProperty("start_date_serialize")
	public Date getStartDateSerialize() {
		return startDate;
	}
	
	@JsonProperty("start_date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonSerialize(using=JsonDateTimeSerializer.class)
	@JsonProperty("end_date_serialize")
	public Date getEndDateSerialize() {
		return endDate;
	}
	
	@JsonProperty("end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@JsonSerialize(using=JsonDateTimeSerializer.class)
	@JsonProperty("datetime_serialize")
	public Date getDatetimeSerialize() {
		return datetime;
	}
	
	@JsonProperty("datetime")
	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	@JsonProperty("account_id")
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonProperty("count_ticket")
	public Integer getCountTicket() {
		return (countTicketWna!=null?countTicketWna:0)+(countTicketWni!=null?countTicketWni:0);
	}

	public void setCountTicket(Integer countTicket) {
		this.countTicket = countTicket;
	}
	
	
	@JsonProperty("count_ticket_wni")
	public Integer getCountTicketWni() {
		return countTicketWni;
	}

	public void setCountTicketWni(Integer countTicketWni) {
		this.countTicketWni = countTicketWni;
	}

	@JsonProperty("count_ticket_wna")
	public Integer getCountTicketWna() {
		return countTicketWna;
	}

	public void setCountTicketWna(Integer countTicketWna) {
		this.countTicketWna = countTicketWna;
	}

	@JsonProperty("total_days")
	public Integer getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(Integer totalDays) {
		this.totalDays = totalDays;
	}

	@JsonProperty("amount_ticket")
	public Double getAmountTicket() {
		return amountTicket;
	}

	public void setAmountTicket(Double amountTicket) {
		this.amountTicket = amountTicket;
	}
	
	@JsonProperty("amount_ticket_wni")
	public Double getAmountTicketWni() {
		return amountTicketWni;
	}

	public void setAmountTicketWni(Double amountTicketWni) {
		this.amountTicketWni = amountTicketWni;
	}

	@JsonProperty("amount_ticket_wna")
	public Double getAmountTicketWna() {
		return amountTicketWna;
	}

	public void setAmountTicketWna(Double amountTicketWna) {
		this.amountTicketWna = amountTicketWna;
	}

	@JsonProperty("amount_total_ticket")
	public Double getAmountTotalTicket() {
		return amountTotalTicket;
	}
	
	public void setAmountTotalTicket(Double amountTotalTicket) {
		this.amountTotalTicket = amountTotalTicket;
	}
	
	@JsonProperty("number_unique")
	public Integer getNumberUnique() {
		return numberUnique;
	}

	public void setNumberUnique(Integer numberUnique) {
		this.numberUnique = numberUnique;
	}
	
	@JsonProperty("total_amount")
	public Double getTotalAmount() {
		if(amountTicket!=null)
		return amountTicket;//+numberUnique;
		else return 0.0;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonProperty("status_booking")
	public String getStatusBooking() {
		return statusBooking;
	}

	public void setStatusBooking(String statusBooking) {
		this.statusBooking = statusBooking;
	}

	@JsonProperty("code_booking")
	public String getCodeBooking() {
		return codeBooking;
	}

	public void setCodeBooking(String codeBooking) {
		this.codeBooking = codeBooking;
	}
	
	@JsonProperty("status_boarding")
	public String getStatusBoarding() {
		return statusBoarding;
	}

	public void setStatusBoarding(String statusBoarding) {
		this.statusBoarding = statusBoarding;
	}

	@JsonProperty("status_reschedule")
	public String getStatusReschedule() {
		return statusReschedule;
	}
	
	public void setStatusReschedule(String statusReschedule) {
		this.statusReschedule = statusReschedule;
	}

	
	public Integer getDisclaimer() {
		return disclaimer;
	}

	public void setDisclaimer(Integer disclaimer) {
		this.disclaimer = disclaimer;
	}

	public String getRegionEndId() {
		return regionEndId;
	}

	public void setRegionEndId(String regionEndId) {
		this.regionEndId = regionEndId;
	}

	public String getRegionEndName() {
		return regionEndName;
	}

	public void setRegionEndName(String regionEndName) {
		this.regionEndName = regionEndName;
	}



	/**********************************************************************/

	
	private String statusName;
	
	@JsonProperty("status_name")
	public String getStatusName() {
		//
		String[] arr = {"", "", "", ""};
		if(StatusPurchase.DRAFT.name().equals(status)) {
			arr[0] = status;
			arr[1] = StatusPurchase.DRAFT.color();
			arr[2] = StatusPurchase.DRAFT.icon();
		}
		if(StatusPurchase.WAITING.name().equals(status)) {
			arr[0] = status;
			arr[1] = StatusPurchase.WAITING.color();
			arr[2] = StatusPurchase.WAITING.icon();
		}
		if(StatusPurchase.VERIFYING.name().equals(status)) {
			arr[0] =this. status;
			arr[1] = StatusPurchase.VERIFYING.color();
			arr[2] = StatusPurchase.VERIFYING.icon();
		}
		if(StatusPurchase.PAYMENT.name().equals(status)) {
			arr[0] = status;
			arr[1] = StatusPurchase.PAYMENT.color();
			arr[2] = StatusPurchase.PAYMENT.icon();
		}
		if(StatusPurchase.CANCEL.name().equals(status)) {
			arr[0] = status;
			arr[1] = StatusPurchase.CANCEL.color();
			arr[2] = StatusPurchase.CANCEL.icon();
		}
		if(this.getStatusExpiredDate()!=null) {
			if(this.getStatusExpiredDate()==1 && !StatusPurchase.PAYMENT.name().equals(status) && !StatusPurchase.CANCEL.name().equals(status)) {
				arr[0] =StatusPurchase.EXPIRED.name();
				arr[1] = StatusPurchase.EXPIRED.color();
				arr[2] = StatusPurchase.EXPIRED.icon();
			}
		}
		if(!arr[0].equals("") && !arr[1].equals("") && !arr[2].equals("")) statusName = arr[0]+","+arr[1]+","+arr[2];
		//
		return statusName;
	}

	public void setStatusName(String status) {
		this.statusName = status;
	}
	
	private Integer statusExpiredDate;
	private Date expiredDate;
	private Account account;
	private Region region;
	private Bank bank;

	List<Purchase> purchaseDetil = new ArrayList<>();
	List<PurchaseDetilVisitor> purchaseDetilVisitor;
	List<PurchaseDetilTransaction> purchaseDetilTransaction;
	List<PurchaseDetilTransitCamp> purchaseDetilTransitCamp;
	List<PurchaseDetilLuggage> purchaseDetilLuggage;
	List<PurchaseDetilPayment> purchaseDetilPayment;
	
	@JsonProperty("status_expired_date")
	public Integer getStatusExpiredDate() {
		return statusExpiredDate;
	}

	public void setStatusExpiredDate(Integer statusExpiredDate) {
		this.statusExpiredDate = statusExpiredDate;
	}
	
	@JsonProperty("expired_date")
	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
	
	@JsonProperty("purchase_detil")
	public List<Purchase> getPurchaseDetil() {
		return purchaseDetil;
	}

	public void setPurchaseDetil(List<Purchase> purchaseDetil) {
		this.purchaseDetil = purchaseDetil;
	}

	@JsonProperty("purchase_detil_visitor")
	public List<PurchaseDetilVisitor> getPurchaseDetilVisitor() {
		return purchaseDetilVisitor;
	}

	public void setPurchaseDetilVisitor(List<PurchaseDetilVisitor> purchaseDetilVisitor) {
		this.purchaseDetilVisitor = purchaseDetilVisitor;
	}
	
	@JsonProperty("purchase_detil_transaction")
	public List<PurchaseDetilTransaction> getPurchaseDetilTransaction() {
		return purchaseDetilTransaction;
	}

	public void setPurchaseDetilTransaction(List<PurchaseDetilTransaction> purchaseDetilTransaction) {
		this.purchaseDetilTransaction = purchaseDetilTransaction;
	}

	@JsonProperty("purchase_detil_transit_camp")
	public List<PurchaseDetilTransitCamp> getPurchaseDetilTransitCamp() {
		return purchaseDetilTransitCamp;
	}

	public void setPurchaseDetilTransitCamp(List<PurchaseDetilTransitCamp> purchaseDetilTransitCamp) {
		this.purchaseDetilTransitCamp = purchaseDetilTransitCamp;
	}

	@JsonProperty("purchase_detil_luggage")
	public List<PurchaseDetilLuggage> getPurchaseDetilLuggage() {
		return purchaseDetilLuggage;
	}

	public void setPurchaseDetilLuggage(List<PurchaseDetilLuggage> purchaseDetilLuggage) {
		this.purchaseDetilLuggage = purchaseDetilLuggage;
	}

	@JsonProperty("purchase_detil_payment")
	public List<PurchaseDetilPayment> getPurchaseDetilPayment() {
		return purchaseDetilPayment;
	}

	public void setPurchaseDetilPayment(List<PurchaseDetilPayment> purchaseDetilPayment) {
		this.purchaseDetilPayment = purchaseDetilPayment;
	}

	@JsonProperty("account")
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	@JsonProperty("region")
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@JsonProperty("bank")
	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}
	

	private String whatsappAdmin;
	private String emailAdmin;

	public String getWhatsappAdmin() {
		return whatsappAdmin;
	}

	public void setWhatsappAdmin(String whatsappAdmin) {
		this.whatsappAdmin = whatsappAdmin;
	}

	public String getEmailAdmin() {
		return emailAdmin;
	}

	public void setEmailAdmin(String emailAdmin) {
		this.emailAdmin = emailAdmin;
	}
	

	private Integer countTicketBoarding;
	private Double amountTicketBoarding;
	private Double amountTotalTicketBoarding;

	@JsonProperty("count_ticket_boarding")
	public Integer getCountTicketBoarding() {
		return countTicketBoarding;
	}

	public void setCountTicketBoarding(Integer countTicketBoarding) {
		this.countTicketBoarding = countTicketBoarding;
	}

	@JsonProperty("amount_ticket_boarding")
	public Double getAmountTicketBoarding() {
		return amountTicketBoarding;
	}

	public void setAmountTicketBoarding(Double amountTicketBoarding) {
		this.amountTicketBoarding = amountTicketBoarding;
	}

	@JsonProperty("amount_total_ticket_boarding")
	public Double getAmountTotalTicketBoarding() {
		return amountTotalTicketBoarding;
	}

	public void setAmountTotalTicketBoarding(Double amountTotalTicketBoarding) {
		this.amountTotalTicketBoarding = amountTotalTicketBoarding;
	}
	
	
	
}
