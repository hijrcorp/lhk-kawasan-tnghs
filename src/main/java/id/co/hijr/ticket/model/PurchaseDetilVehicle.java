package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseDetilVehicle {

	public static final String ID = "id_purchase_detil_vehicle";
	public static final String HEADER_ID = "header_id_purchase_detil_vehicle";
	public static final String NAME = "name_purchase_detil_vehicle";
	public static final String TYPE = "type_purchase_detil_vehicle";
	public static final String PRICE = "price_purchase_detil_vehicle";
	public static final String QTY = "qty_purchase_detil_vehicle";

	private String id;
	private String headerId;
	private String name;
	private String type;
	private Integer price;
	private Integer qty;

	public PurchaseDetilVehicle() {

	}

	public PurchaseDetilVehicle(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("price")
	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	@JsonProperty("qty")
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}



	/**********************************************************************/

}
