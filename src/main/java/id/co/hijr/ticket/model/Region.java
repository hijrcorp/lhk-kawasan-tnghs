package id.co.hijr.ticket.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.hijr.sistem.model.File;

public class Region {
	public static final String ID = "id_region";
	public static final String NAME = "name_region";
	public static final String TYPE = "type_region";
	public static final String DESCRIPTION = "description_region";
	public static final String LOCATION = "location_region";
	public static final String LOCATION_DESC = "location_desc_region";
	public static final String ORGANIZATION_ID = "organization_id_region";
	public static final String REFUND = "refund_region";
	public static final String KUOTA = "kuota_region";
	public static final String MAX_DAY = "max_day_region";
	public static final String PHOTO = "photo_region";

	private String id;
	private String name;
	private String type;
	private String description;
	private String location;
	private String locationDesc;
	private String organizationId;
	private String refund;
	private Integer kuota;
	private Integer maxDay;
	private String photo;

	public Region() {

	}

	public Region(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@JsonProperty("location_desc")
	public String getLocationDesc() {
		return locationDesc;
	}

	public void setLocationDesc(String locationDesc) {
		this.locationDesc = locationDesc;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@JsonProperty("refund")
	public String getRefund() {
		return refund;
	}

	public void setRefund(String refund) {
		this.refund = refund;
	}

	@JsonProperty("kuota")
	public Integer getKuota() {
		return kuota;
	}

	public void setKuota(Integer kuota) {
		this.kuota = kuota;
	}

	@JsonProperty("max_day")
	public Integer getMaxDay() {
		return maxDay;
	}

	public void setMaxDay(Integer maxDay) {
		this.maxDay = maxDay;
	}

	@JsonProperty("photo")
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**********************************************************************/

	
	
	private Map<String, Object> list_photo = new HashMap<>();
	private List<RegionPhoto> list_photo_new = new ArrayList<>();
	private List<RegionFacility> list_facility = new ArrayList<>();
	private List<RegionTransitCamp> list_transit_camp = new ArrayList<>();
	private List<RegionTodo> list_todo = new ArrayList<>();

	public Map<String, Object> getList_photo() {
		return list_photo;
	}
	public void setList_photo(Map<String, Object> list_photo) {
		this.list_photo = list_photo;
	}
	
	public List<RegionPhoto> getList_photo_new() {
		return list_photo_new;
	}

	public void setList_photo_new(List<RegionPhoto> list_photo_new) {
		this.list_photo_new = list_photo_new;
	}

	public List<RegionFacility> getList_facility() {
		return list_facility;
	}

	public void setList_facility(List<RegionFacility> list_facility) {
		this.list_facility = list_facility;
	}

	
	
	public List<RegionTransitCamp> getList_transit_camp() {
		return list_transit_camp;
	}

	public void setList_transit_camp(List<RegionTransitCamp> list_transit_camp) {
		this.list_transit_camp = list_transit_camp;
	}

	public List<RegionTodo> getList_todo() {
		return list_todo;
	}

	public void setList_todo(List<RegionTodo> list_todo) {
		this.list_todo = list_todo;
	}

	private Integer priceRegion;
	private Integer sisaKuota;
	private String nameFile;
	
	@JsonProperty("price_region")
	public Integer getPriceRegion() {
		return priceRegion;
	}

	public void setPriceRegion(Integer priceRegion) {
		this.priceRegion = priceRegion;
	}
	
	@JsonProperty("sisa_kuota")
	public Integer getSisaKuota() {
		return sisaKuota;
	}

	public void setSisaKuota(Integer sisaKuota) {
		this.sisaKuota = sisaKuota;
	}
	
	@JsonProperty("name_file")
	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

}
