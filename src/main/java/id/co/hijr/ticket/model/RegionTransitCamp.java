package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.sistem.common.JsonTimeSerializer;

public class RegionTransitCamp {
	
	public static final String ID = "id_region_transit_camp";
	public static final String HEADER_ID = "header_id_region_transit_camp";
	public static final String CODE = "code_region_transit_camp";
	public static final String ID_MASTER_TRANSIT_CAMP = "id_master_transit_camp_region_transit_camp";
	public static final String START_REGISTER = "start_register_region_transit_camp";
	public static final String END_REGISTER = "end_register_region_transit_camp";
	public static final String EXTIMATE_ARRIVED = "extimate_arrived_region_transit_camp";
	public static final String NUMBER_OF_LARGE_TENTS = "number_of_large_tents_region_transit_camp";
	public static final String NUMBER_OF_SMALL_TENTS = "number_of_small_tents_region_transit_camp";
	public static final String PHOTO = "photo_region_transit_camp";
	
	private String id;
	private String headerId;
	private String code;
	private String idMasterTransitCamp;
	private Date startRegister;
	private Date endRegister;
	private Date extimateArrived;
	private Integer numberOfLargeTents;
	private Integer numberOfSmallTents;
	private String photo;


	public RegionTransitCamp() {

	}

	public RegionTransitCamp(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("id_master_transit_camp")
	public String getIdMasterTransitCamp() {
		return idMasterTransitCamp;
	}

	public void setIdMasterTransitCamp(String idMasterTransitCamp) {
		this.idMasterTransitCamp = idMasterTransitCamp;
	}

	@JsonProperty("start_register")
	public Date getStartRegister() {
		return startRegister;
	}

	public void setStartRegister(Date startRegister) {
		this.startRegister = startRegister;
	}

	@JsonProperty("end_register")
	public Date getEndRegister() {
		return endRegister;
	}

	public void setEndRegister(Date endRegister) {
		this.endRegister = endRegister;
	}

	@JsonProperty("extimate_arrived")
	public Date getExtimateArrived() {
		return extimateArrived;
	}

	public void setExtimateArrived(Date extimateArrived) {
		this.extimateArrived = extimateArrived;
	}


	/**********************************************************************/

	@JsonSerialize(using=JsonTimeSerializer.class)
	@JsonProperty("start_register_serialize")
	public Date getStartRegisterSerialize() {
		return startRegister;
	}

	@JsonSerialize(using=JsonTimeSerializer.class)
	@JsonProperty("end_register_serialize")
	public Date getEndRegisterSerialize() {
		return endRegister;
	}
	
	@JsonSerialize(using=JsonTimeSerializer.class)
	@JsonProperty("extimate_arrived_serialize")
	public Date getExtimateArrivedSerialize() {
		return extimateArrived;
	}

	@JsonProperty("number_of_large_tents")
	public Integer getNumberOfLargeTents() {
		return numberOfLargeTents;
	}
	
	public void setNumberOfLargeTents(Integer numberOfLargeTents) {
		this.numberOfLargeTents = numberOfLargeTents;
	}
	
	@JsonProperty("number_of_small_tents")
	public Integer getNumberOfSmallTents() {
		return numberOfSmallTents;
	}
	
	public void setNumberOfSmallTents(Integer numberOfSmallTents) {
		this.numberOfSmallTents = numberOfSmallTents;
	}

	@JsonProperty("photo")
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	///////

	private String nameFile;
	private String namePhoto;
	
	private String nameMasterTransitCamp;

	private String codeMasterTransitCamp;
	
	
	@JsonProperty("name_file")
	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	
	@JsonProperty("name_photo")
	public String getNamePhoto() {
		return namePhoto;
	}

	public void setNamePhoto(String namePhoto) {
		this.namePhoto = namePhoto;
	}

	@JsonProperty("name_master_transit_camp")
	public String getNameMasterTransitCamp() {
		return nameMasterTransitCamp;
	}

	public void setNameMasterTransitCamp(String nameMasterTransitCamp) {
		this.nameMasterTransitCamp = nameMasterTransitCamp;
	}


	@JsonProperty("code_master_transit_camp")
	public String getCodeMasterTransitCamp() {
		return codeMasterTransitCamp;
	}

	public void setCodeMasterTransitCamp(String codeMasterTransitCamp) {
		this.codeMasterTransitCamp = codeMasterTransitCamp;
	}
	
	

}
