package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.itextpdf.text.List;

import id.co.hijr.sistem.common.JsonDateTimeSerializer;
import id.co.hijr.sistem.model.Account;

public class Bank {

	public static final String ID = "id_bank";
	public static final String NAME = "name_bank";
	public static final String CODE = "code_bank";
	public static final String TYPE = "type_bank";
	public static final String ACCOUNT_NUMBER = "account_number_bank";
	public static final String ACCOUNT_HOLDER_NAME = "account_holder_name_bank";

	private String id;
	private String name;
	private String code;
	private String type;
	private String accountNumber;
	private String accountHolderName;

	public Bank() {

	}

	public Bank(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("account_number")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonProperty("account_holder_name")
	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	
}
