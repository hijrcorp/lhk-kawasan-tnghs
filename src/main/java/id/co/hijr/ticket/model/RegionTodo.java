package id.co.hijr.ticket.model;

import java.util.Date;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.sistem.common.JsonTimeSerializer;

public class RegionTodo {
	public static final String ID = "id_region_todo";
	public static final String HEADER_ID = "header_id_region_todo";
	public static final String DAY = "day_region_todo";
	public static final String TIME = "time_region_todo";
	public static final String ACTIVITY = "activity_region_todo";
	public static final String PARENT = "parent_region_todo";

	private String id;
	private String headerId;
	private Integer day;
	private Date time;
	private String activity;
	private String parent;

	public RegionTodo() {

	}

	public RegionTodo(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("day")
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	@JsonProperty("time")
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@JsonProperty("activity")
	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	@JsonProperty("parent")
	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}



	/**********************************************************************/
	
	
	@JsonSerialize(using=JsonTimeSerializer.class)
	@JsonProperty("time_serialize")
	public Date getTimeSerialize() {
		return time;
	}
}
