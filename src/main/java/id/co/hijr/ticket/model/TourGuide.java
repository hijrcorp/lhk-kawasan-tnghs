package id.co.hijr.ticket.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TourGuide {
	public static final String ID = "id_tour_guide";
	public static final String FULL_NAME = "full_name_tour_guide";
	public static final String GENDER = "gender_tour_guide";
	public static final String PHOTO = "photo_tour_guide";
	public static final String DESCRIPTION = "description_tour_guide";
	public static final String PHONE_NUMBER = "phone_number_tour_guide";
	public static final String EMAIL = "email_tour_guide";
	public static final String COST_PER_DAY = "cost_per_day_tour_guide";
	public static final String ORGANIZATION_ID = "organization_id_tour_guide";

	private String id;
	private String fullName;
	private String gender;
	private String photo;
	private String description;
	private String phoneNumber;
	private String email;
	private Double costPerDay;
	private String organizationId;

	public TourGuide() {

	}

	public TourGuide(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("full_name")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@JsonProperty("photo")
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("cost_per_day")
	public Double getCostPerDay() {
		return costPerDay;
	}

	public void setCostPerDay(Double costPerDay) {
		this.costPerDay = costPerDay;
	}

	@JsonProperty("organization_id")
	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}



	/**********************************************************************/
}
