package id.co.hijr.ticket.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseDetilLuggage {

	public static final String ID = "id_purchase_detil_luggage";
	public static final String HEADER_ID = "header_id_purchase_detil_luggage";
	public static final String TYPE_LUGGAGE = "type_luggage_purchase_detil_luggage";
	public static final String NAME_LUGGAGE = "name_luggage_purchase_detil_luggage";
	public static final String QTY_LUGGAGE = "qty_luggage_purchase_detil_luggage";
	public static final String DESC_LUGGAGE = "desc_luggage_purchase_detil_luggage";
	public static final String QTY_ACTUAL_LUGGAGE = "qty_actual_luggage_purchase_detil_luggage";
	public static final String IS_VALID_LUGGAGE = "is_valid_luggage_purchase_detil_luggage";
	public static final String STATUS = "status_purchase_detil_luggage";

	private String id;
	private String headerId;
	private String typeLuggage;
	private String nameLuggage;
	private Integer qtyLuggage;
	private String descLuggage;
	private Integer qtyActualLuggage;
	private Boolean isValidLuggage;
	private String status;

	public PurchaseDetilLuggage() {

	}

	public PurchaseDetilLuggage(String id) {
		this.id = id;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("header_id")
	public String getHeaderId() {
		return headerId;
	}

	public void setHeaderId(String headerId) {
		this.headerId = headerId;
	}

	@JsonProperty("type_luggage")
	public String getTypeLuggage() {
		return typeLuggage;
	}

	public void setTypeLuggage(String typeLuggage) {
		this.typeLuggage = typeLuggage;
	}

	@JsonProperty("name_luggage")
	public String getNameLuggage() {
		return nameLuggage;
	}

	public void setNameLuggage(String nameLuggage) {
		this.nameLuggage = nameLuggage;
	}

	@JsonProperty("qty_luggage")
	public Integer getQtyLuggage() {
		return qtyLuggage;
	}

	public void setQtyLuggage(Integer qtyLuggage) {
		this.qtyLuggage = qtyLuggage;
	}

	@JsonProperty("desc_luggage")
	public String getDescLuggage() {
		return descLuggage;
	}

	public void setDescLuggage(String descLuggage) {
		this.descLuggage = descLuggage;
	}

	@JsonProperty("qty_actual_luggage")
	public Integer getQtyActualLuggage() {
		return qtyActualLuggage;
	}
	
	public void setQtyActualLuggage(Integer qtyActualLuggage) {
		this.qtyActualLuggage = qtyActualLuggage;
	}
	
	@JsonProperty("is_valid_luggage")
	public Boolean isIsValidLuggage() {
		return isValidLuggage;
	}
	
	public void setIsValidLuggage(Boolean isValidLuggage) {
		this.isValidLuggage = isValidLuggage;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	/**********************************************************************/
}
