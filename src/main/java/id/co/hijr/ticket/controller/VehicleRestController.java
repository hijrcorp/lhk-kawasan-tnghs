package id.co.hijr.ticket.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.VehicleMapper;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Vehicle;

@Controller
@RequestMapping("/ticket/vehicle")
public class VehicleRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private VehicleMapper vehicleMapper;

	@Autowired
	private FileMapper fileMapper;

	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;

	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
//		if(startDate.isPresent() && !endDate.isPresent()) {
//    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
//    	}else if(!startDate.isPresent() && endDate.isPresent()) {
//    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
//    	}else if(startDate.isPresent() && endDate.isPresent()) {
//    		
//    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
//    	}
//    	
//    	
//    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
//    		param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
//    	}
    	
    	
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Vehicle> data = vehicleMapper.getList(param);
		String path;
		for(Vehicle o : data) {
			QueryParameter param_own = new QueryParameter();
			param_own.setClause(param_own.getClause()+" AND "+File.REFERENCE_ID+"='"+o.getId()+"'");
		  	for(File vehicle_file : fileMapper.getList(param_own)){
		  		vehicle_file.setName(vehicle_file.getName().replace(" ", "%"));
			  	path=request.getContextPath()+"/files/"+vehicle_file.getId()+"?filename="+vehicle_file.getName()+"&download";
			  	if(o.getPhoto()!=null) o.setPhoto(path);
				
    		}
		  	path=request.getContextPath()+"/images/notfound.png";
		  	if(o.getPhoto()==null) o.setPhoto(path);
		}
		
		resp.setCount(vehicleMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Vehicle data = vehicleMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("name") Optional<String> name,
    		@RequestParam("type_id") Optional<String> typeId,
    		@RequestParam("cost")  @NumberFormat(pattern = "#,###.##") Optional<Double> cost,
//    		@RequestParam("photo") Optional<String> photo,
    		@RequestParam("photo") Optional<MultipartFile> file_photo,
    		@RequestParam("seat_capacity") Optional<Integer> seatCapacity,
    		@RequestParam("suitcase") Optional<Integer> suitcase,
    		@RequestParam("description") Optional<String> description,
    		@RequestParam("provided") Optional<String> provided,
    		HttpServletRequest request,HttpServletResponse response
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Vehicle data = new Vehicle(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = vehicleMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		if(id.isPresent()) data.setId(id.get());
    		if(name.isPresent()) data.setName(name.get());
    		if(typeId.isPresent()) data.setTypeId(typeId.get());
    		if(cost.isPresent()) data.setCost(cost.get());
    		
    		
//    		if(photo.isPresent()) data.setPhoto(photo.get());

    		if(file_photo.isPresent() && !file_photo.get().isEmpty()) {
    			if(file_photo.get().getContentType().startsWith("image")) {
//    				String ext = FilenameUtils.getExtension(data.getPhoto());
    				//String ext = data.getNamaFileSatwaPermohonan().split("\\.")[1];
        			
        			File file = new File(Utils.getLongNumberID());

    				data.setPhoto(file.getId());
        			//
//        			if(folder.isPresent()) {
//        				data.setFolder("/" + folder.get());
//        			}else {
        				file.setFolder("/");
//        			}
        				file.setSize(file_photo.get().getSize());
        				file.setType(file_photo.get().getContentType());
        				file.setName(file_photo.get().getOriginalFilename());
        				file.setPersonAdded(this.extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
        				file.setTimeAdded(new Date());
        				file.setReferenceId(data.getId());
//        			if(reference.isPresent()) data.setReferenceId(reference.get());
        			
        			String ext = FilenameUtils.getExtension(file.getName());
        			storageService.store(file_photo.get(), file.getId() + "."+ext);
    	    		
    	    		fileMapper.insert(file);
        			
//        	 		resp.setMessage("File telah berhasil diupload.");
        			//
    			}else {
//    				System.out.println("heeee");
//    				System.out.println(getConstructURL(request, ""));
//    				String loginError = getConstructURL(request, "") + "files/upload";
//    				response.sendRedirect(loginError);
    				resp.setCode(HttpStatus.BAD_REQUEST.value());
    				resp.setMessage("Harap unggah file Gambar (JPG/PNG/GIF).");
    				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    			}
    			
    		}
    		
    		if(seatCapacity.isPresent()) data.setSeatCapacity(seatCapacity.get());
    		if(suitcase.isPresent()) data.setSuitcase(suitcase.get());
    		if(description.isPresent()) data.setDescription(description.get());
    		if(provided.isPresent()) data.setProvided(provided.get());
    		
    		data.setOrganizationId(extractAccountLogin(request, AccountLoginInfo.SOURCE));

    		
    		boolean pass = true;
    		if( data.getName() == null) {
    			pass = false;
    		}else {
    			if(data.getName().equals("")) {
        			pass = false;
        		}
    		}
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			vehicleMapper.update(data);
    		}else {
    			vehicleMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Vehicle data = vehicleMapper.getEntity(id);
			resp.setData(data);
			vehicleMapper.delete(data);
			if(data.getPhoto() != null) {
				File file = fileMapper.getEntity(data.getPhoto());
				fileMapper.delete(file);
			}else {
				QueryParameter param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+data.getId()+"'");
				List<File> file = fileMapper.getList(param);
				if(file.size()>1) {
					resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Something wrong.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}else {
					fileMapper.delete(file.get(0));
				}
			}
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
