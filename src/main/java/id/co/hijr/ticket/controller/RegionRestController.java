package id.co.hijr.ticket.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RegionFacilityMapper;
import id.co.hijr.ticket.mapper.RegionMapper;
import id.co.hijr.ticket.mapper.RegionPhotoMapper;
import id.co.hijr.ticket.mapper.RegionTodoMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.Region;
import id.co.hijr.ticket.model.RegionFacility;
import id.co.hijr.ticket.model.RegionPhoto;
import id.co.hijr.ticket.model.RegionTodo;
import id.co.hijr.ticket.model.RegionTransitCamp;

@Controller
@RequestMapping("/ticket/region")
public class RegionRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private RegionMapper regionMapper;

	@Autowired
	private RegionFacilityMapper regionFacilityMapper;

	@Autowired
	private RegionTodoMapper regionTodoMapper;

	@Autowired
	private RegionPhotoMapper regionPhotoMapper;
	
	@Autowired
	private FileMapper fileMapper;

	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	

	@RequestMapping(value="/checkKuota/{id}", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getCheckKuota(
			@PathVariable String id,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		HttpServletRequest request
    		)throws Exception {
    	
		ResponseWrapperList resp = new ResponseWrapperList();

		QueryParameter param = new QueryParameter();
		
		
		if(startDate.isPresent()) {
			param.setInnerClause(param.getInnerClause() + " AND start_date_configure_kuota"+ " = '"+Utils.formatSqlDate(startDate.get())+"'");
			param.setInnerClause(param.getInnerClause() + " OR end_date_configure_kuota"+ " = '"+Utils.formatSqlDate(startDate.get())+"'");

    		param.setInnerClause2(param.getInnerClause2() + " AND DATE(" + Purchase.START_DATE + ") = '"+Utils.formatSqlDate(startDate.get())+"'");
    	}
    	
		param.setInnerClause2(param.getInnerClause2() + " AND (" + Purchase.REGION_ID + " = '"+id+"')");
		param.setClause(param.getClause() + " AND (" + Region.ID + " = '"+id+"')");
    	System.out.println("filter:"+param.getInnerClause());
    	System.out.println("filter:"+param.getInnerClause2());
    	System.out.println("filter:"+param.getClause());
		Region data = regionMapper.getList(param).get(0);
		
		resp.setData(data);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
		if(startDate.isPresent() && !endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
    	}else if(!startDate.isPresent() && endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
    	}else if(startDate.isPresent() && endDate.isPresent()) {
    		
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
    	}
    	
    	
    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
    	}
    	
    	if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Region.NAME + " LIKE '%"+filterKeyword.get()+"%')");
    	}
    	
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Region> data = regionMapper.getList(param);
		resp.setCount(regionMapper.getCount(param));
		for (Region o : data) {
			//old
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+File.REFERENCE_ID+"='"+o.getId()+"'");
			List<File> list_photo_master = fileMapper.getList(param);
			// 
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+RegionPhoto.HEADER_ID+"='"+o.getId()+"'");
			List<RegionPhoto> list_photo_transction  = regionPhotoMapper.getList(param);
			ArrayList<Object> list_temp_photo = new ArrayList<>();
			Map<String, Object> list_arr_photo = new HashMap<>();
			list_arr_photo.put("master", list_photo_master);
			list_arr_photo.put("transaction", list_photo_transction);
			
			for(File file: list_photo_master) {
				for(RegionPhoto rp: list_photo_transction) {
					if(file.getId().equals(rp.getFileId()))
						rp.setList_photo(file);
				}
			}
			list_arr_photo.put("use_this", list_temp_photo);
//			list_arr_photo.put("use_this", list_photo_transction);
			o.setList_photo(list_arr_photo);
			
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+RegionFacility.HEADER_ID+"='"+o.getId()+"'");
			List<RegionFacility> list_facility = regionFacilityMapper.getList(param);
			o.setList_facility(list_facility);
			
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+RegionTodo.HEADER_ID+"='"+o.getId()+"'");
//			param.setOrder("time_region_todo");
			List<RegionTodo> list_todo = regionTodoMapper.getList(param);
			o.setList_todo(list_todo);
			
			for(RegionTransitCamp rt : o.getList_transit_camp()) {
				if(rt.getId()==null) o.setList_transit_camp(null);
			}
			
		}
		
//		Map<String, Object> map = new HashMap<>();
//		map.put("master", data);
//		map.put("photo", null);
//		map.put("facility", null);
//		map.put("todo", null);
		
		resp.setData(data);
		
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Region data = regionMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("name") Optional<String> name,
    		@RequestParam("type") Optional<String> type,
    		@RequestParam("description") Optional<String> description,
    		@RequestParam("location") Optional<String> location,
    		@RequestParam("location_desc") Optional<String> locationDesc,
    		@RequestParam("refund") Optional<String> refund,
    		@RequestParam("kuota") Optional<Integer> kuota,
    		@RequestParam("max_day") Optional<Integer> maxDay,
    		@RequestParam("file") Optional<MultipartFile> filePhoto,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Region data = new Region(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = regionMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(name.isPresent()) data.setName(name.get());
    		if(type.isPresent()) data.setType(type.get());
    		if(description.isPresent()) data.setDescription(description.get());
    		if(location.isPresent()) data.setLocation(location.get());
    		if(locationDesc.isPresent()) data.setLocationDesc(locationDesc.get());
    		if(refund.isPresent()) {
    			String refunds = refund.get().replace("on", "1");
    			data.setRefund(refunds);
    		}else {
    			data.setRefund("0");
    		}
    		
    		data.setOrganizationId(extractAccountLogin(request, AccountLoginInfo.SOURCE));

    		if(kuota.isPresent()) {
    			data.setKuota(kuota.get());
    		}
    		if(maxDay.isPresent()) {
    			data.setMaxDay(maxDay.get());
    		}
    		
    		boolean pass = true;
    		if( data.getName() == null || data.getType() == null || data.getKuota() == null) {
    			pass = false;
    		}else {
    			if(data.getName().equals("") || data.getType().equals("")) {
        			pass = false;
        		}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(filePhoto.isPresent() && !filePhoto.get().isEmpty()) {
    			if(filePhoto.get().getContentType().startsWith("image")) {
        			File file = new File(Utils.getLongNumberID());

    				data.setPhoto(file.getId());
    					
    				file.setFolder("/");
        				file.setSize(filePhoto.get().getSize());
        				file.setType(filePhoto.get().getContentType());
        				file.setName(filePhoto.get().getOriginalFilename());
        				file.setPersonAdded(this.extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
        				file.setTimeAdded(new Date());
        				file.setReferenceId(data.getId());
        			
        				String ext = FilenameUtils.getExtension(file.getName());
        			storageService.store(filePhoto.get(), file.getId() + "."+ext);
    	    		
    	    		fileMapper.insert(file);
    			}else {
    				resp.setCode(HttpStatus.BAD_REQUEST.value());
    				resp.setMessage("Harap unggah file Gambar (JPG/PNG/GIF).");
    				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    			}
    		}
    		
    		if(id.isPresent()) {
    			regionMapper.update(data);
    		}else {
    			regionMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Region data = regionMapper.getEntity(id);
			resp.setData(data);
			regionMapper.delete(data);
			
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
