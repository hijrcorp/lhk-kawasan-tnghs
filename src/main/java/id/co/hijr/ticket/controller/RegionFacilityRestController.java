package id.co.hijr.ticket.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RegionFacilityMapper;
import id.co.hijr.ticket.mapper.RegionMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.Region;
import id.co.hijr.ticket.model.RegionFacility;

@Controller
@RequestMapping("/ticket/region-facility")
public class RegionFacilityRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private RegionFacilityMapper regionFacilityMapper;
	
	

	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
		if(startDate.isPresent() && !endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
    	}else if(!startDate.isPresent() && endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
    	}else if(startDate.isPresent() && endDate.isPresent()) {
    		
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
    	}
    	
    	
    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
    	}
    	
    	
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<RegionFacility> data = regionFacilityMapper.getList(param);
		
		resp.setCount(regionFacilityMapper.getCount(param));
		
		resp.setData(data);
		
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		RegionFacility data = regionFacilityMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("header_id") Optional<String> headerId,
    		@RequestParam("code") Optional<String> code,
    		@RequestParam("item") Optional<String> item,
    		@RequestParam("price")  @NumberFormat(pattern = "#,###.##") Optional<Double> price,
    		@RequestParam("include") Optional<String> include,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		RegionFacility data = new RegionFacility(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = regionFacilityMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(headerId.isPresent()) data.setHeaderId(headerId.get());
    		if(code.isPresent()) data.setCode(code.get());
    		if(item.isPresent()) data.setItem(item.get());
    		if(price.isPresent()) data.setPrice(price.get());
    		if(include.isPresent()) data.setInclude(include.get());
    
    		
    		boolean pass = true;
    		if( data.getItem() == null) {
    			pass = false;
    		}else {
    			if(data.getItem().equals("")) {
        			pass = false;
        		}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			regionFacilityMapper.update(data);
    		}else {
    			regionFacilityMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			RegionFacility data = regionFacilityMapper.getEntity(id);
			resp.setData(data);
			regionFacilityMapper.delete(data);
			
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
