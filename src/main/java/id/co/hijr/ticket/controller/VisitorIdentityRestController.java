package id.co.hijr.ticket.controller;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.StyleExcel;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountMapper;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.Account;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.service.VaultService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTourGuideMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVehicleMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RegionMapper;
import id.co.hijr.ticket.mapper.TourGuideMapper;
import id.co.hijr.ticket.mapper.VehicleMapper;
import id.co.hijr.ticket.mapper.VisitorIdentityMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;
import id.co.hijr.ticket.model.PurchaseDetilTourGuide;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;
import id.co.hijr.ticket.model.PurchaseDetilVehicle;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;
import id.co.hijr.ticket.model.Region;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Vehicle;
import id.co.hijr.ticket.model.VisitorIdentity;
import id.co.hijr.ticket.ref.StatusBooking;
import id.co.hijr.ticket.ref.StatusPurchase;
import id.co.hijr.sistem.service.AppManagerService;
import id.co.hijr.sistem.service.NotificationService;

@Controller
@RequestMapping("/visitor/identity")
public class VisitorIdentityRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private RegionMapper regionMapper;

	@Autowired
	private PurchaseMapper purchaseMapper;

	@Autowired
	private PurchaseDetilVisitorMapper purchaseDetilVisitorMapper;
	
	@Autowired
	private VisitorIdentityMapper visitorIdentityMapper;

	@Autowired
	private PurchaseDetilVehicleMapper purchaseDetilVehicleMapper;

	@Autowired
	private PurchaseDetilTourGuideMapper purchaseDetilTourGuideMapper;

	@Autowired
	private PurchaseDetilTransactionMapper purchaseDetilTransactionMapper;

	@Autowired
	private PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper;

	@Autowired
	private PurchaseDetilLuggageMapper purchaseDetilLuggageMapper;

	@Autowired
	private VehicleMapper vehicleMapper;

	@Autowired
	private TourGuideMapper tourGuideMapper;

	@Autowired
	private AccountMapper accountMapper;

	@Autowired
	private FileMapper fileMapper;
	
	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	
	@Autowired
	protected AppManagerService appManagerService;

	@Autowired
	NotificationService notificationService;

	@Autowired 
	VaultService vaultService;
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_tanggal_pendakian") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakian,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		@RequestParam("filter_status_booking") Optional<String> filterStatusBooking,
    		@RequestParam("filter_status_boarding") Optional<String> filterStatusBoarding,
    		@RequestParam("filter_no_booking") Optional<String> filterNoBooking,
    		@RequestParam("filter_no_purchase") Optional<String> filteNoPurchase,
    		@RequestParam("filter_id_region") Optional<String> filterIdRegion,
    		@RequestParam("filter_keyword") Optional<String> filterKeyword,
    		@RequestParam("filter_status_valid") Optional<String> filterStatusValid,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();

		if(filterKeyword.isPresent() && !filterKeyword.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + VisitorIdentity.FULL_NAME + " LIKE '%"+filterKeyword.get()+"%' ");
    		param.setClause(param.getClause() + " OR " + VisitorIdentity.PHONE_NUMBER + " LIKE '%"+filterKeyword.get()+"%' ");
    		param.setClause(param.getClause() + " OR " + VisitorIdentity.EMAIL + " LIKE '%"+filterKeyword.get()+"%' ");
    		param.setClause(param.getClause() + " OR " + " data_hash " + " LIKE '%"+DigestUtils.sha256Hex(filterKeyword.get())+"%')");
    	}
    	
    	System.out.println(param.getClause());
		
		if(startDate.isPresent() && endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
    	}
    	
    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
    		if(filterStatus.get().equals("NOT_DRAFT")) {
    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'DRAFT' ");
    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'CANCEL' ");
    		}else if(filterStatus.get().equals("NOT_PAYMENT")) {
    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'PAYMENT' ");
    		}
//	    		else if(filterStatus.get().equals("EXPIRED")) {
//	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
//	    			param.setClause(param.getClause() + " OR (" + "status_expired_date_purchase" + " = '"+1+"')");
//	    		}
    		else {
    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
    		}
    		
    	}
    	
    	if(filterStatusBooking.isPresent() && !filterStatusBooking.get().equals("")) {
    		if(filterStatusBooking.get().equals("DONE_DRAFT")) {
    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOOKING + " IN ('DONE') OR status_booking_purchase IS NULL)");
    		}else {
    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOOKING + " = '"+filterStatusBooking.get()+"')");
    		}
    	}
    	
    	if(filterStatusValid.isPresent()) {
    		if(filterStatusValid.get().equals("0")) {
    			//param.setClause(param.getClause() + " AND " + VisitorIdentity.STATUS_VALID + " is not null ");
    		}else{
    			
    			
    			if(filterStatusValid.get().equals("BANNED")) {
    				param.setClause(param.getClause() + " AND (" + VisitorIdentity.STATUS_VALID + " = '"+filterStatusValid.get()+"')");
    			}else if(!filterStatusValid.get().equals("0")) {
    				//param.setClause(param.getClause() + " AND (" + VisitorIdentity.STATUS_VALID + " = '"+filterStatusValid.get()+"')");
    				param.setClause(param.getClause() + " AND (" + VisitorIdentity.STATUS_VALID + " is null )");
    				//param.setClause(param.getClause() + " AND (" + VisitorIdentity.STATUS_VALID + " != '"+filterStatusValid.get()+"')");
    			}
    			
    		}
    	}
    	
    	//
		List<String> lstUnitAuditi = accountMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
		String clauseUnit = "";
		for(String u: lstUnitAuditi) {clauseUnit += ",'" + u +"'";}
    		
		//clauseUnit.substring(1)
		if (lstUnitAuditi.size() > 0) param.setClause(param.getClause()+ " AND "+Purchase.REGION_ID+" ='"+lstUnitAuditi.get(0)+"'");
    	//

		if(request.isUserInRole("ROLE_KAWASAN_PENDAKI")) {
			param.setClause(param.getClause() + " AND (" + Purchase.ACCOUNT_ID + " = '"+extractAccountLogin(request, AccountLoginInfo.ACCOUNT)+"')");
		}
    	if(filterIdRegion.isPresent()) param.setClause(param.getClause() + " AND " + Purchase.REGION_ID + " = '"+filterIdRegion.get()+"'");
    	if(tanggalPendakian.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.START_DATE + " = '"+Utils.formatSqlDate(tanggalPendakian.get())+"'");
    	}
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
	    	
	    //param.setOrder("datetime_purchase desc");
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		
		if(filterNoBooking.isPresent()) {
			param.setClause(param.getClause() + " AND (" + Purchase.CODE_BOOKING + " = '"+filterNoBooking.get()+"')");
			param.setClause(param.getClause() + " OR (" + Purchase.ID + " = '"+filterNoBooking.get()+"')");
		}
		List<VisitorIdentity> data = visitorIdentityMapper.getList(param);
		//read the KTP
		for(VisitorIdentity o : data) {
			String nik = vaultService.decrypt(o.getNoIdentity());
			o.setNoIdentity(nik.substring(0, nik.length()-3)+"XXX");
		}
		resp.setCount(visitorIdentityMapper.getCount(param));
		
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		VisitorIdentity data = visitorIdentityMapper.getEntity(id);
		
		//read the KTP
		data.setNoIdentity(vaultService.decrypt(data.getNoIdentity()));
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("no_identity") Optional<String> noIdentity,
    		@RequestParam("full_name") Optional<String> fullName,
    		@RequestParam("gender") Optional<String> gender,
    		@RequestParam("birthdate") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> birthdate,
    		@RequestParam("address") Optional<String> address,
    		@RequestParam("phone_number") Optional<String> phoneNumber,
    		@RequestParam("email") Optional<String> email,
    		@RequestParam("status_valid") Optional<String> statusValid,
    		@RequestParam("date_banned") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> dateBanned,
    		@RequestParam("days_banned") Optional<Integer> daysBanned,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		VisitorIdentity data = new VisitorIdentity(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = visitorIdentityMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(noIdentity.isPresent()) data.setNoIdentity(vaultService.encrypt(noIdentity.get()));
    		if(fullName.isPresent()) data.setFullName(fullName.get());
    		if(gender.isPresent()) data.setGender(gender.get());
    		if(birthdate.isPresent()) data.setBirthdate(birthdate.get());
    		if(address.isPresent()) data.setAddress(address.get());
    		if(phoneNumber.isPresent()) data.setPhoneNumber(phoneNumber.get());
    		if(email.isPresent()) data.setEmail(email.get());
    		if(statusValid.isPresent()) data.setStatusValid((statusValid.get().equals("")?null:statusValid.get()));
    		
    		
    		boolean pass = true;
    		if(data.getNoIdentity() == null || data.getFullName() == null 
    				|| data.getGender()==null || data.getBirthdate()==null
    			|| data.getAddress()==null || data.getPhoneNumber()==null
			|| data.getEmail()==null) { // || data.getStatusValid()==null
    			pass = false;
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		System.out.println("GOLDAN??"+data.getStatusValid());
    		if(data.getStatusValid()!=null) {
    			if(data.getStatusValid().equals("BANNED")) {
	        		if(dateBanned.isPresent()) data.setDateBanned(dateBanned.get());
	        		if(daysBanned.isPresent()) data.setDaysBanned(daysBanned.get());
	        		
	        		if(data.getDateBanned()==null || data.getDaysBanned()==null) {
	        			resp.setCode(HttpStatus.BAD_REQUEST.value());
	        			resp.setMessage("Tanggal dan durasi banned, belum disi.");
	        			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
	        		}
    			}
    		}else {
    			data.setDateBanned(null);
        		data.setDaysBanned(null);
    		}
    		
    		if(id.isPresent()) {
    			visitorIdentityMapper.update(data);
    		}else {
    			visitorIdentityMapper.insert(data);
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		String nik = vaultService.decrypt(data.getNoIdentity());
    		data.setNoIdentity(nik.substring(0, nik.length()-3)+"XXX");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
    @RequestMapping(value="/upload", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> upload(
    		@RequestParam("id") Optional<String> id,
			@RequestParam("file_identity") Optional<MultipartFile>[] fileIdentity,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		VisitorIdentity data = new VisitorIdentity(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = visitorIdentityMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		
    		boolean pass = true;
    		/*if(data.getNoIdentity() == null) { 
    			pass = false;
    		}*/
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("File belum ada yang dipilih, mohon periksa kembali.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + File.REFERENCE_ID + "=" + data.getId());
			
			if(fileMapper.getCount(param) > 0) {
				fileMapper.deleteBatch(param);
			}
			
			//not need cause will be replace with same name
			//storageService.delete(file.getId() + "." + ext);
			
    		if(fileIdentity.length > 0 && !fileIdentity[0].get().isEmpty()) {
				File file = new File(Utils.getLongNumberID());
				data.setIdFile(file.getId());
				file.setFolder("/");
				file.setSize(fileIdentity[0].get().getSize());
				file.setType(fileIdentity[0].get().getContentType());
				file.setName(fileIdentity[0].get().getOriginalFilename());
				file.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
				file.setTimeAdded(new Date());
				file.setReferenceId(data.getId());
			
				String ext = FilenameUtils.getExtension(file.getName());
				
				storageService.storeWithEncrypted(fileIdentity[0].get(), file.getId() + "."+ext, file.getId());
	    		
				
	    		fileMapper.insert(file);
			}
    		
    		visitorIdentityMapper.update(data);
    		
    		resp.setMessage("Data telah berhasil diupload.");
    		//data.setNoIdentity(vaultService.decrypt(data.getNoIdentity()));
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
    		@RequestParam("detil_luggage") Optional<String> detilLuggage,
    		@RequestParam("detil_visitor") Optional<String> detilVisitor,
    		@RequestParam("potensi_sampah") Optional<String> potensiSampah,
    		@RequestParam("send_sertifikat") Optional<String> sendSertifikat,
    		@RequestParam("description_message") Optional<String> descriptionMessage,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();

		VisitorIdentity data = visitorIdentityMapper.getEntity(id);
		
		resp.setData(data);
		
		if(action.equals(DELETE)) {
			visitorIdentityMapper.delete(data);

			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + File.REFERENCE_ID + "=" + data.getId());
			fileMapper.deleteBatch(param);
			
			if(fileMapper.getCount(param) > 0) {
				String ext = FilenameUtils.getExtension(data.getFileName());
				storageService.delete(id + "." + ext);
			}
			
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		/*
		if(action.equals("RESCHEDULE_NON_ACTIVE") || action.equals("RESCHEDULE_ACTIVE")) {
			data.setStatusReschedule(action);
			if(action.equals("RESCHEDULE_NON_ACTIVE")) data.setStatusReschedule(null);
			purchaseMapper.update(data);
			resp.setMessage("Data '"+data.getId()+"' telah "+action+".");
		}
		
		if(action.equals("PAYMENT") || action.equals("CANCEL") || action.equals("BOARDING")  || action.equals("KELUAR")) {
			if(!action.equals("BOARDING") && !action.equals("KELUAR")) data.setStatus(action);
			
			Map<String, String> dataEmail = new HashMap<>();
			dataEmail.put("id", data.getId());
			dataEmail.put("nameRegion", data.getRegion().getName());

			SimpleDateFormat sda = new SimpleDateFormat("dd-MM-yyyy"); // HH:mm:ss
			dataEmail.put("startDate", sda.format(data.getStartDate()).toString());
			dataEmail.put("endDate", sda.format(data.getEndDate()).toString());
			
			dataEmail.put("countTicket", data.getCountTicket()+"");
			dataEmail.put("codeBooking", data.getCodeBooking());
			dataEmail.put("nameTransitCamp", data.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
			dataEmail.put("reportTime", data.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp());
			
			dataEmail.put("codeUnique", data.getPurchaseDetilTransitCamp().get(0).getCodeUnique());
			dataEmail.put("fullName", data.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName());
			String email = "kayumiman@yahoo.co.id";
			email = data.getPurchaseDetilVisitor().get(0).getEmail();
			
			dataEmail.put("email", "EMAIL_SINGLE_TO:"+email);
			//only send mail, when status payment
			if(action.equals("PAYMENT")) {
		        FileInputStream inStream = appManagerService.reportPermohonanFormC(id);		
		    	String filename = id.replaceAll("/", "_") + ".pdf";
		    	Utils.copyToTmpDir(inStream, filename);
		    	java.io.File file = new java.io.File(System.getProperty("java.io.tmpdir"), filename);
		    	
				notificationService.sendMailTicket(dataEmail, file);
				data.setStatusBooking(StatusBooking.DONE.name());
			}else if(action.equals("CANCEL")) {
				if(descriptionMessage.isPresent()) {
					dataEmail.put("description_message", descriptionMessage.get());
					notificationService.sendMailGeneral(dataEmail, null);
				}
			}

			ObjectMapper mapper = new ObjectMapper();
			if(action.equals("BOARDING")) {
				data.setStatusBoarding("1");
				
				JsonNode js = mapper.readTree(detilVisitor.get());
				//new JSONArray(detilVisitor.get());
    			List<String> ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
        			PurchaseDetilVisitor o = purchaseDetilVisitorMapper.getEntity(js.get(i).path("id_visitor").asText());
        			o.setIsValid(true);
        			purchaseDetilVisitorMapper.update(o);
    			}
    			
    			// luggage
    			js = mapper.readTree(detilLuggage.get());//.get(0);
    			//new JSONArray(detilLuggage.get());
    			ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
    				PurchaseDetilLuggage olg = purchaseDetilLuggageMapper.getEntity(js.get(i).path("id_luggage").asText());
        			
        			olg.setQtyActualLuggage(js.get(i).path("qty_actual_luggage").asInt());
        			olg.setIsValidLuggage(true);
        		
    				purchaseDetilLuggageMapper.update(olg);
    			}
    			//
    			PurchaseDetilLuggage olg = new PurchaseDetilLuggage(Utils.getLongNumberID());
    			olg.setHeaderId(data.getId());
    			olg.setDescLuggage(potensiSampah.get());
    			purchaseDetilLuggageMapper.insert(olg);
    			
    			//
			}
			
			if(action.equals("KELUAR")) {
				data.setStatusBoarding("2");
				if(sendSertifikat.isPresent()) {
					if(sendSertifikat.get().equals("1")) {
						FileInputStream inStream = appManagerService.sertifikat(id);		
				    	String filename = id.replaceAll("/", "_") + ".pdf";
				    	Utils.copyToTmpDir(inStream, "sertifikat"+filename);
				    	java.io.File file = new java.io.File(System.getProperty("java.io.tmpdir"), "sertifikat"+filename);
				    	
						notificationService.sendMailSertifikat(dataEmail, file);
					}else {
						dataEmail.put("description_message", descriptionMessage.get());
						notificationService.sendMailGeneral(dataEmail, null);
					}
						
				}
			}
			
			purchaseMapper.update(data);
			resp.setMessage("Data '"+data.getId()+"' telah diupdate.");
		}
		if(action.equals("DISCLAIMER")) {
			data.setDisclaimer(1);
			purchaseMapper.update(data);
			resp.setMessage("Terimakasih atas disclaimer anda.");
			resp.setData(data);
		}*/
		System.out.println("action: "+action);
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
	@RequestMapping(value = "/preview-export", method = RequestMethod.GET)
	public void exportOutput(
			@RequestParam("status") Optional<String> status,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		QueryParameter param = new QueryParameter();
		//param.setClause(param.getClause()+" AND "+UnitKompetensiTeknis.ID+" IN("+temp.substring(1)+")");
		
		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true); 
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		StyleExcel style = new StyleExcel();
		
		//XSSFSheet sheet = workbook.createSheet("Master");
		//for(UnitKompetensiTeknis ukt : listUnitKompTeknis) {
		XSSFSheet sheet = workbook.createSheet("Laporan Purchase"); //name sheet

			// create header row
			CellRangeAddress range = null;
			int idxRow = 0;
			XSSFRow title = sheet.createRow(idxRow);
			title.createCell(0).setCellValue("Laporan Purchase");
			title.getCell(0).setCellStyle(style.styleAlign(workbook, "center"));
			//styleBody(workbook, mapBody)
			range = new CellRangeAddress(idxRow, idxRow, 0, 5);
			sheet.addMergedRegion(range);

			idxRow++;
			idxRow++;

			XSSFRow header = sheet.createRow(idxRow++);
			int idx = 0;
			String[] headerArr = {"NO:1:1","Nomor Purchase:1:1","Kode Booking:1:1","Jalur Pendakian:1:1","Tanggal Pendakian:1:1","Pendaki:1:1","Nama Lengkap:1:1","Jenis Kelamin:1:1","No.HP:1:1","Harga Total:1:1","Tanggal Booking:1:1","Status Booking:1:1","Status Bayar:1:1"};
			List<String> listHeader = new ArrayList<String>();
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			int maxBaris = 1;
			for(String namaHeader : listHeader) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0];
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
			
			param = new QueryParameter();
			if(status.isPresent() && !status.get().equals("")) param.setClause(param.getClause()+" AND "+Purchase.STATUS+" ='"+status.get()+"'");
			param.setLimit(100000000);
			List<Purchase> listPurchase = purchaseMapper.getListExtended(param);

			idx=3;
			int nomor=1;

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			for(Purchase p : listPurchase) {
				XSSFRow aRow = sheet.createRow(idx);
				aRow.createCell(0).setCellValue(nomor);
				aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));

				aRow.createCell(1).setCellValue(p.getId());
				aRow.getCell(1).setCellStyle(style.styleBody(workbook, mapBody));
				
				aRow.createCell(2).setCellValue(p.getCodeBooking());
				aRow.getCell(2).setCellStyle(style.styleBody(workbook, mapBody));
			
				aRow.createCell(3).setCellValue(p.getRegion().getName());
				aRow.getCell(3).setCellStyle(style.styleBody(workbook, mapBody));
			
				aRow.createCell(4).setCellValue(sdf.format((p.getStartDate())));
				aRow.getCell(4).setCellStyle(style.styleBody(workbook, mapBody));
			
				aRow.createCell(5).setCellValue(p.getCountTicket());
				aRow.getCell(5).setCellStyle(style.styleBody(workbook, mapBody));
				//

				int idxsub=6;
				for(int k=0; k < 4; k++) {
					aRow.createCell(idxsub).setCellValue("-");
					aRow.getCell(idxsub).setCellStyle(style.styleBody(workbook, mapBody));
					
					String fullName=" -", gender=" -", phoneNumber=" -";
					int start=0;
					for(PurchaseDetilVisitor pdv : p.getPurchaseDetilVisitor()) {
						if(pdv.getResponsible().equals("0") && pdv.getVisitorIdentity().getGender()!=null) {
							fullName+=(start>0?"\n -":"")+pdv.getVisitorIdentity().getFullName();
							gender+=(start>0?"\n -":"")+(pdv.getVisitorIdentity().getGender().equals("L")?"LAKI-LAKI":"PEREMPUAN");
							phoneNumber+=(start>0?"\n -":"")+pdv.getVisitorIdentity().getPhoneNumber();
							start++;
						}
					}
					aRow.getCell(6).setCellValue(fullName);
					if(aRow.getCell(7)!=null) aRow.getCell(7).setCellValue(gender);
					if(aRow.getCell(8)!=null) aRow.getCell(8).setCellValue(phoneNumber);
					
					//aRow.getCell(idxsub).setCellStyle(style.styleBody(workbook, mapBody));
					idxsub++;
				}
				//
				aRow.createCell(6+idxsub-7).setCellValue((p.getAmountTicket()!=null?p.getAmountTicket():0.0));
				aRow.getCell(6+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				//style.formatNumber(workbook)
			
				aRow.createCell(7+idxsub-7).setCellValue(sdf.format((p.getDatetime())));
				aRow.getCell(7+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				String status_name=p.getStatusName().split(",")[0];
				String status_color=p.getStatusName().split(",")[1];
				String status_icon=p.getStatusName().split(",")[2];

				if(status_name.equals("WAITING")) status_name="Waiting for Payment";
				if(status_name.equals("VERIFYING")) status_name="Verifying Payment";
				
				aRow.createCell(8+idxsub-7).setCellValue(status_name);
				aRow.getCell(8+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				if(p.getStatusBooking()==null || p.getStatusBooking().equals("") || p.getStatusBooking().isEmpty() || p.getStatusBooking().isBlank()) {
					p.setStatusBooking("DRAFT");
					status_color="warning";
					status_icon="exclamation-circle";
				}

				aRow.createCell(9+idxsub-7).setCellValue(p.getStatusBooking());
				aRow.getCell(9+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				idx++;
				nomor++;
			}
			

			//too width
			//sheet.setColumnWidth(2, (75  * 256) + 200);
			sheet.setColumnWidth(0, (5  * 256) + 200); 
			sheet.setColumnWidth(1, (20  * 256) + 200); 
			sheet.setColumnWidth(2, (20  * 256) + 200); 
			sheet.setColumnWidth(3, (20  * 256) + 200); 
			sheet.setColumnWidth(4, (15  * 256) + 200); 
			sheet.setColumnWidth(6, (15  * 256) + 400); 
			sheet.setColumnWidth(7, (15  * 256) + 200); 
			sheet.setColumnWidth(8, (20  * 256) + 200); 
			sheet.setColumnWidth(9, (20  * 256) + 200); 
			sheet.setColumnWidth(10, (20  * 256) + 200); 
			sheet.setColumnWidth(11, (20  * 256) + 200); 
			sheet.setColumnWidth(12, (20  * 256) + 200); 
		//}

		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename=KAWASAN-"+"purchase-laporan"+"-"+new Date()+".xlsx"); //name file
		
		workbook.write(response.getOutputStream());
		workbook.close();
	}
	
	@RequestMapping(value = "/history_visitor/{ktp}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getCheckNIK(
			@PathVariable String ktp,
			@RequestParam("action") Optional<String> action,
    		HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		VisitorIdentity data = decryptVisitorIdentity(ktp, request);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	 private VisitorIdentity decryptVisitorIdentity(String valueKTP, HttpServletRequest request) {
		VisitorIdentity data = null;
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+ " AND "+VisitorIdentity.ID_HEADER+" ='"+extractAccountLogin(request, AccountLoginInfo.ACCOUNT)+"'");
		param.setLimit(10000000);
		for(VisitorIdentity o : visitorIdentityMapper.getList(param)) {
			//System.out.println("before>>>"+vaultService.decrypt(o.getNoIdentity())+".equals"+(valueKTP));
			//boolean pass= vaultService.decrypt(o.getNoIdentity())!=null;
			String nik = vaultService.decrypt(o.getNoIdentity());
			if(nik.equals(valueKTP)) {
				data=visitorIdentityMapper.getEntity(o.getId());
				//param = new QueryParameter();
				//param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+"='"+o.getNoIdentity()+"'");
			}
		}
		//System.out.println("other"+ param.getClause());
		//if(!param.getClause().equals("1")) data=visitorIdentityMapper.getList(param).get(0);
		return data;
	 }

}
