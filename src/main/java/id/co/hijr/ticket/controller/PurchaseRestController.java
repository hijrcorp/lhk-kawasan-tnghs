package id.co.hijr.ticket.controller;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.StyleExcel;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountMapper;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.Account;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.service.VaultService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTourGuideMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVehicleMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RegionMapper;
import id.co.hijr.ticket.mapper.TourGuideMapper;
import id.co.hijr.ticket.mapper.VehicleMapper;
import id.co.hijr.ticket.mapper.VisitorIdentityMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseChart;
import id.co.hijr.ticket.model.PurchaseChartDetil;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;
import id.co.hijr.ticket.model.PurchaseDetilPayment;
import id.co.hijr.ticket.model.PurchaseDetilTourGuide;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;
import id.co.hijr.ticket.model.PurchaseDetilVehicle;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;
import id.co.hijr.ticket.model.Region;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Vehicle;
import id.co.hijr.ticket.model.VisitorIdentity;
import id.co.hijr.ticket.ref.StatusBooking;
import id.co.hijr.ticket.ref.StatusPurchase;
import id.co.hijr.sistem.service.AppManagerService;
import id.co.hijr.sistem.service.NotificationService;

@Controller
@RequestMapping("/ticket/purchase")
public class PurchaseRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private RegionMapper regionMapper;

	@Autowired
	private PurchaseMapper purchaseMapper;

	@Autowired
	private PurchaseDetilVisitorMapper purchaseDetilVisitorMapper;
	
	@Autowired
	private VisitorIdentityMapper visitorIdentityMapper;

	@Autowired
	private PurchaseDetilVehicleMapper purchaseDetilVehicleMapper;

	@Autowired
	private PurchaseDetilTourGuideMapper purchaseDetilTourGuideMapper;

	@Autowired
	private PurchaseDetilTransactionMapper purchaseDetilTransactionMapper;

	@Autowired
	private PurchaseDetilTransitCampMapper purchaseDetilTransitCampMapper;

	@Autowired
	private PurchaseDetilLuggageMapper purchaseDetilLuggageMapper;

	@Autowired
	private VehicleMapper vehicleMapper;

	@Autowired
	private TourGuideMapper tourGuideMapper;

	@Autowired
	private AccountMapper accountMapper;

	@Autowired
	private FileMapper fileMapper;
	
	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	
	@Autowired
	protected AppManagerService appManagerService;

	@Autowired
	NotificationService notificationService;

	@Autowired 
	VaultService vaultService;
	
	@Value("${app.state}")
    private String appState;
	
	@Value("${app.newurl.production.linkPurchase}")
    private String appUrlPurchaseProd;
	
	@Value("${app.newurl.development.linkPurchase}")
    private String appUrlPurchaseDevel;
	
	protected String getLinkPurchaseUrl() {
		return (appState.equals("development")?appUrlPurchaseDevel:appUrlPurchaseProd);
	}
	
	@RequestMapping(value="/list-boarding", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getListBoarding(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_tanggal_pendakian") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakian,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		@RequestParam("filter_status_booking") Optional<String> filterStatusBooking,
    		@RequestParam("filter_status_boarding") Optional<String> filterStatusBoarding,
    		@RequestParam("filter_no_booking") Optional<String> filterNoBooking,
    		@RequestParam("filter_no_purchase") Optional<String> filteNoPurchase,
    		@RequestParam("filter_id_region") Optional<String> filterIdRegion,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
			if(startDate.isPresent() && !endDate.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
	    	}else if(!startDate.isPresent() && endDate.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
	    	}else if(startDate.isPresent() && endDate.isPresent()) {
	    		
	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
	    	}
	    	
	    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
	    		if(filterStatus.get().equals("NOT_DRAFT")) {
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'DRAFT' ");
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'CANCEL' ");
	    		}else if(filterStatus.get().equals("NOT_PAYMENT")) {
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'PAYMENT' ");
	    		}
//	    		else if(filterStatus.get().equals("EXPIRED")) {
//	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
//	    			param.setClause(param.getClause() + " OR (" + "status_expired_date_purchase" + " = '"+1+"')");
//	    		}
	    		else {
	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
	    		}
	    		
	    	}
	    	
	    	if(filterStatusBooking.isPresent() && !filterStatusBooking.get().equals("")) {
	    		if(filterStatusBooking.get().equals("DONE_DRAFT")) {
	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOOKING + " IN ('DONE') OR status_booking_purchase IS NULL)");
	    		}else {
	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOOKING + " = '"+filterStatusBooking.get()+"')");
	    		}
	    	}
	    	
	    	if(filterStatusBoarding.isPresent()) {
	    		if(filterStatusBoarding.get().equals("")) {
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS_BOARDING + " is not null ");
	    		}else {
	    			if(filterStatusBoarding.get().equals("0")) {
	    				param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOARDING + " is null )");
	    			}else {
	    				param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOARDING + " = '"+filterStatusBoarding.get()+"')");
	    			}
	    			
	    		}
	    	}
	    	
	    	//
    		List<String> lstUnitAuditi = accountMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    		String clauseUnit = "";
    		for(String u: lstUnitAuditi) {clauseUnit += ",'" + u +"'";}
	    		
			//clauseUnit.substring(1)
			if (lstUnitAuditi.size() > 0) param.setClause(param.getClause()+ " AND "+Purchase.REGION_ID+" ='"+lstUnitAuditi.get(0)+"'");
	    	//

			if(request.isUserInRole("ROLE_KAWASAN_PENDAKI")) {
				param.setClause(param.getClause() + " AND (" + Purchase.ACCOUNT_ID + " = '"+extractAccountLogin(request, AccountLoginInfo.ACCOUNT)+"')");
			}
	    	if(filterIdRegion.isPresent()) param.setClause(param.getClause() + " AND " + Purchase.REGION_ID + " = '"+filterIdRegion.get()+"'");
	    	if(tanggalPendakian.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Purchase.START_DATE + " = '"+Utils.formatSqlDate(tanggalPendakian.get())+"'");
	    	}
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
	    param.setOrder("datetime_purchase desc");
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		
		if(filterNoBooking.isPresent()) {
			param.setClause(param.getClause() + " AND (" + Purchase.CODE_BOOKING + " = '"+filterNoBooking.get()+"')");
			param.setClause(param.getClause() + " OR (" + Purchase.ID + " = '"+filterNoBooking.get()+"')");
		}
		List<Purchase> data = purchaseMapper.getListBoarding(param);

		for(Purchase o : data) {
			Account account = accountMapper.getEntity(o.getAccountId());
			o.setAccount(account);

			QueryParameter param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.HEADER_ID + " = '"+o.getId()+"')");
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.RESPONSIBLE + " = '"+1+"')");
			List<PurchaseDetilVisitor> dataPurchaseDetilVisitor = purchaseDetilVisitorMapper.getList(param_detil);
			o.setPurchaseDetilVisitor(dataPurchaseDetilVisitor);

			param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+o.getId()+"')");
			List<PurchaseDetilTransaction> dataPurchaseDetilTransaction = purchaseDetilTransactionMapper.getList(param_detil);
			o.setPurchaseDetilTransaction(dataPurchaseDetilTransaction);
			
			for(Purchase o2 : o.getPurchaseDetil()) {
				param_detil = new QueryParameter();
				param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+o2.getId()+"')");
				List<PurchaseDetilTransaction> dataPurchaseDetilTransaction2 = purchaseDetilTransactionMapper.getList(param_detil);
				
				o2.setPurchaseDetilTransaction(dataPurchaseDetilTransaction2);
			}
			
			param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+o.getId()+"')");
			List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param_detil);
			o.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);
		}
			
		resp.setCount(purchaseMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_tanggal_pendakian") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakian,
    		@RequestParam("filter_tanggal_pendakian_s") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakianS,
    		@RequestParam("filter_tanggal_pendakian_e") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakianE,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		@RequestParam("filter_status_booking") Optional<String> filterStatusBooking,
    		@RequestParam("filter_status_boarding") Optional<String> filterStatusBoarding,
    		@RequestParam("filter_no_booking") Optional<String> filterNoBooking,
    		@RequestParam("filter_no_purchase") Optional<String> filteNoPurchase,
    		@RequestParam("filter_id_region") Optional<String> filterIdRegion,
    		@RequestParam("order") Optional<String> order,
    		HttpServletRequest request
    		)throws Exception {

		ResponseWrapperList resp = new ResponseWrapperList();
		
		QueryParameter param = new QueryParameter();
		
		
			if(startDate.isPresent() && !endDate.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
	    	}else if(!startDate.isPresent() && endDate.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
	    	}else if(startDate.isPresent() && endDate.isPresent()) {
	    		
	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
	    	}
			
			if(tanggalPendakianS.isPresent() && tanggalPendakianE.isPresent()) {
				param.setClause(param.getClause() + " AND " + Purchase.START_DATE + " BETWEEN '"+Utils.formatSqlDate(tanggalPendakianS.get())+"' AND '"+Utils.formatSqlDate(tanggalPendakianE.get())+"'");
			}
	    	
	    	/*if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
	    		if(filterStatus.get().equals("NOT_DRAFT")) {
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'DRAFT' ");
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'CANCEL' ");
	    		}else if(filterStatus.get().equals("NOT_PAYMENT")) {
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'PAYMENT' ");
	    		}else {
	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
	    		}
	    	}*/

			String strFilterStatus="";
			String strFilterStatusBoarding="";
			if(request.getParameterValues("filter_status")!=null) {
		    	String[] listFilterStatus = request.getParameterValues("filter_status");
	    		for(int i=0; i < listFilterStatus.length; i++) {
					strFilterStatus+=",'"+listFilterStatus[i]+"'";
				}
			}
			if(!strFilterStatus.equals("") && !filterStatus.get().equals("NOT_DRAFT")) param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " in ("+strFilterStatus.substring(1)+"))");
			
			else if(!strFilterStatus.equals("") && filterStatus.get().equals("NOT_DRAFT")) {
    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'DRAFT' ");
    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'EXPIRED' ");
    			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " != 'CANCEL' ");
			}
	    	
			System.out.println("strFilterStatus:"+strFilterStatus);
	    	
	    	if(filterStatusBooking.isPresent() && !filterStatusBooking.get().equals("")) {
	    		if(filterStatusBooking.get().equals("DONE_DRAFT")) {
	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOOKING + " IN ('DONE') OR status_booking_purchase IS NULL)");
	    		}else {
	    			param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOOKING + " = '"+filterStatusBooking.get()+"')");
	    		}
	    	}
	    	
	    	if(filterStatusBoarding.isPresent()) {
	    		if(filterStatusBoarding.get().equals("")) {
	    			param.setClause(param.getClause() + " AND " + Purchase.STATUS_BOARDING + " is not null ");
	    		}else {
	    			if(filterStatusBoarding.get().equals("0")) {
	    				param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOARDING + " is null )");
	    			}else {
	    				param.setClause(param.getClause() + " AND (" + Purchase.STATUS_BOARDING + " = '"+filterStatusBoarding.get()+"')");
	    			}
	    			
	    		}
	    	}
	    	
	    	//
    		List<String> lstUnitAuditi = accountMapper.findUnitAuditiByUserId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    		String clauseUnit = "";
    		for(String u: lstUnitAuditi) {clauseUnit += ",'" + u +"'";}
	    		
			//clauseUnit.substring(1)
			if (lstUnitAuditi.size() > 0) param.setClause(param.getClause()+ " AND "+Purchase.REGION_ID+" ='"+lstUnitAuditi.get(0)+"'");
	    	//

			if(request.isUserInRole("ROLE_KAWASAN_PENDAKI")) {
				param.setClause(param.getClause() + " AND (" + Purchase.ACCOUNT_ID + " = '"+extractAccountLogin(request, AccountLoginInfo.ACCOUNT)+"')");
			}
	    	if(filterIdRegion.isPresent()) param.setClause(param.getClause() + " AND " + Purchase.REGION_ID + " = '"+filterIdRegion.get()+"'");
	    	if(tanggalPendakian.isPresent()) {
	    		param.setClause(param.getClause() + " AND " + Purchase.START_DATE + " = '"+Utils.formatSqlDate(tanggalPendakian.get())+"'");
	    	}
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
	    param.setOrder("datetime_purchase desc");
    	
		
		if(filterNoBooking.isPresent()) {
			param.setClause(param.getClause() + " AND (" + Purchase.CODE_BOOKING + " = '"+filterNoBooking.get()+"')");
			param.setClause(param.getClause() + " OR (" + Purchase.ID + " = '"+filterNoBooking.get()+"')");
		}
		if(order.isPresent()) param.setOrder(order.get());
		List<Purchase> data = purchaseMapper.getList(param);

		for(Purchase o : data) {
//			if(o.getStatusExpiredDate()==1) {
//				o.setStatus("EXPIRED");
//				purchaseMapper.update(o);
//			}
			Account account = accountMapper.getEntity(o.getAccountId());
			o.setAccount(account);

			QueryParameter param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.HEADER_ID + " = '"+o.getId()+"')");
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilVisitor.RESPONSIBLE + " = '"+1+"')");
			List<PurchaseDetilVisitor> dataPurchaseDetilVisitor = purchaseDetilVisitorMapper.getList(param_detil);
			o.setPurchaseDetilVisitor(dataPurchaseDetilVisitor);

			param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+o.getId()+"')");
			List<PurchaseDetilTransaction> dataPurchaseDetilTransaction = purchaseDetilTransactionMapper.getList(param_detil);
			o.setPurchaseDetilTransaction(dataPurchaseDetilTransaction);
			
			for(Purchase o2 : o.getPurchaseDetil()) {
				param_detil = new QueryParameter();
				param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransaction.HEADER_ID+ " = '"+o2.getId()+"')");
				List<PurchaseDetilTransaction> dataPurchaseDetilTransaction2 = purchaseDetilTransactionMapper.getList(param_detil);
				
				o2.setPurchaseDetilTransaction(dataPurchaseDetilTransaction2);
			}
			
			param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+o.getId()+"')");
			List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param_detil);
			o.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);
		}
			
		resp.setCount(purchaseMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+" AND "+Purchase.ID+"='"+id+"'");
		Purchase data = purchaseMapper.getListExtended(param).get(0);
		
		if(data.getParentId()!=null) {
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+Purchase.ID+"='"+data.getParentId()+"'");
			param.setInnerClause(param.getInnerClause()+" AND "+PurchaseDetilVisitor.PARENT_HEADER_ID+"='"+data.getId()+"'");
			Purchase dataParent = purchaseMapper.getListExtended(param).get(0);
			data.setPurchaseDetilVisitor(dataParent.getPurchaseDetilVisitor());
		}else {
			param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+Purchase.ID+"='"+data.getId()+"'");
			param.setInnerClause(param.getInnerClause()+" AND "+PurchaseDetilVisitor.PARENT_HEADER_ID+" is null");
			Purchase dataParent = purchaseMapper.getListExtended(param).get(0);
			data.setPurchaseDetilVisitor(dataParent.getPurchaseDetilVisitor());
		}
		
		if(action.isPresent() && action.get().equals("detil-identity-boarding")) {
	           param = new QueryParameter();
	           param.setClause(param.getClause() + " AND (" + Purchase.ID + " = '"+id+"')");
	           param.setInnerClause(param.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is "+" NULL "+")");
	           Purchase purchaseParent= purchaseMapper.getListExtended(param).get(0);
	           
	           data.setPurchaseDetilVisitor(purchaseParent.getPurchaseDetilVisitor());
		}
		
		Account account = accountMapper.getEntity(data.getAccountId());
		data.setAccount(account);
		
		//read the KTP
		for(PurchaseDetilVisitor o : data.getPurchaseDetilVisitor()) {o.getVisitorIdentity().setNoIdentity(vaultService.decrypt(o.getVisitorIdentity().getNoIdentity()));}
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}

			if(action.get().equals("RESCHEDULE_ACTIVE") || action.get().equals("RESCHEDULE_NON_ACTIVE")) {
				resp.setMessage("Apakah anda yakin akan <strong>"+action.get()+"</strong> data '"+data.getId()+"'?");
			}

			if(action.get().equals("BOARDING")) {
				resp.setMessage("Pastikan boarding data Masuk sudah sesuai/valid.");
			}
			if(action.get().equals("KELUAR")) {
				resp.setMessage("Pastikan boarding data Keluar sudah sesuai/valid.");
			}
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	@RequestMapping(value = "/status/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getStatusId(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action,
			HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Purchase data = purchaseMapper.getEntity(id);
		if(data==null) {
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + Purchase.ID + "=" + id);
			data = purchaseMapper.getListExtended(param).get(0);
		}
		if(request.isUserInRole("ROLE_KAWASAN_PENDAKI")|| true) {
			
		}
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("region_id") Optional<String> regionId,
    		@RequestParam("region_end_id") Optional<String> regionEndId,
    		@RequestParam("region_end_name") Optional<String> regionEndName,
    		@RequestParam("region_name") Optional<String> regionName,
    		@RequestParam("start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("datetime") Optional<Date> datetime,
    		@RequestParam("account_id") Optional<String> accountId,
    		@RequestParam("count_ticket_wni") Optional<Integer> countTicketWni,
    		@RequestParam("count_ticket_wna") Optional<Integer> countTicketWna,
    		@RequestParam("amount_ticket")  @NumberFormat(pattern = "#,###.##") Optional<Double> amountTicket,
    		@RequestParam("amount_total_ticket")  @NumberFormat(pattern = "#,###.##") Optional<Double> amountTotalTicket,
    		@RequestParam("status") Optional<String> status,
//    		@RequestParam("detil_visitor") ArrayList<PurchaseDetilVisitor> detilVisitor,
    		@RequestParam("detil_visitor") Optional<String> detilVisitor,
    		@RequestParam("detil_guide") Optional<String> detilGuide,
    		@RequestParam("detil_vehicle") Optional<String> detilVehicle,
    		@RequestParam("detil_transit_camp") Optional<String> detilTransitCamp,
    		@RequestParam("detil_luggage") Optional<String> detilLuggage,
    		@RequestParam("other_luggage") Optional<String> otherLuggage,
			@RequestParam("file_identity") Optional<MultipartFile>[] fileIdentity,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Purchase data = new Purchase(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = purchaseMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(regionId.isPresent()) data.setRegionId(regionId.get());
    		if(regionName.isPresent()) data.setRegionName(regionName.get());
    		if(startDate.isPresent()) data.setStartDate(startDate.get());
    		if(endDate.isPresent()) data.setEndDate(endDate.get());
//    		if(datetime.isPresent()) data.setDatetime(datetime.get());
//    		if(accountId.isPresent()) data.setAccountId(accountId.get());
    		
    		if(countTicketWni.isPresent()) data.setCountTicketWni(countTicketWni.get());
    		if(countTicketWna.isPresent()) data.setCountTicketWna(countTicketWna.get());
    		
    		if(amountTicket.isPresent()) data.setAmountTicket(amountTicket.get());
    		if(amountTotalTicket.isPresent()) data.setAmountTotalTicket(amountTotalTicket.get());
    		
    		//alternative validasi harga
//			Region regionKuota = regionMapper.getEntityCheckPrice(data.getRegionId(), Utils.formatSqlDate(data.getStartDate()));
//			int intAmountTotalTicket=regionKuota.getPriceRegion()*data.getCountTicket();
//			
//			if(intAmountTotalTicket != data.getAmountTotalTicket()) {
//    			resp.setCode(HttpStatus.BAD_REQUEST.value());
//    			resp.setMessage("HARGA TIDAK SESUAI DENGAN YANG TERDAFTAR OLEH SISTEM, SISTEM MENDETEKSI ANDA MENGUBAH HARGA <strong>"+(regionKuota.getPriceRegion()*data.getCountTicket())+ "</strong> Menjadi <strong>"+data.getAmountTotalTicket()+"</strong>");
//    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//    		}
    		
//    		if(status.isPresent()) data.setStatus(status.get());
    		String akunTemp="";
    		if(!getCookieTokenValue(request, cookieName).equals("")) {
    			akunTemp=extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
    			data.setAccountId(akunTemp);
    		}else {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Maaf perangkat anda belum Login.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		//if(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT)!=null) data.setAccountId(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
    		data.setDatetime(new Date());
    		
    		boolean pass = true;
    		if(data.getStartDate() == null || data.getEndDate() == null || data.getCountTicket()==null) {
    			pass = false;
    		}else {
    			if(data.getCountTicket()==0) {
        			pass = false;
        		}
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(data.getCountTicket()<4) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Mohon maaf, minimal pendaki adalah 4 dan maximal 10 orang.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		if(id.isPresent()) {
    			if(!data.getStatus().equals(StatusPurchase.DRAFT.name())) {
    				resp.setCode(HttpStatus.BAD_REQUEST.value());
    				resp.setMessage("Mohon maaf, sistem mendeteksi anda telah melakukan submit sebelumnya, pastikan anda tidak login menggunakan dua device. silahkan refresh halaman ini untuk lanjut apabila anda merasa telah melakukan submit dan apabila belum silahkan menghubungi Costumer Service.");
    				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    			}

    			//testa
    			ObjectMapper mapper = new ObjectMapper();
        		JsonNode js = mapper.readTree(detilVisitor.get());
        		System.out.println(js);
        		System.out.println("TRY REDAD2"+ js.get(0).path("type_identity"));
        		System.out.println("TRY REDAD3"+ js.get(0).path("full_name").textValue());
    			//
    			//call > amount_price
    			double priceWni=0;
    			double priceWna=0;
        		//
    			List<String> ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
    				if(js.get(i).path("type_identity").textValue().equals("NIK")) {
    					priceWni+=js.get(i).path("amount_price").asInt();
    				}
    				
    				if(js.get(i).path("type_identity").textValue().equals("PASSPORT")) {
    					priceWna+=js.get(i).path("amount_price").asInt();
    				}
    				ls.add(js.get(i).path("full_name").textValue());
    				
        			QueryParameter param = new QueryParameter();
        			
        			if(decryptVisitorIdentity(js.get(i).path("no_identity").textValue(), request)!=null) param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+" ='"+decryptVisitorIdentity(js.get(i).path("no_identity").textValue(),request).getNoIdentity()+"'");
        			else {
        				param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+" ='"+null+"'");
        			}
        			System.out.println("param"+param.getClause());
        			VisitorIdentity visitorIdentity = new VisitorIdentity(Utils.getLongNumberID());
        			
        			if(visitorIdentityMapper.getCount(param) == 0) {
    					//System.out.println("1.DID U @_@");
	        			visitorIdentity.setIdHeader(akunTemp);
	        			visitorIdentity.setNoIdentity(vaultService.encrypt(js.get(i).path("no_identity").textValue()));
	        			visitorIdentity.setFullName(js.get(i).path("full_name").textValue());
	        			visitorIdentity.setTypeIdentity(js.get(i).path("type_identity").textValue());
	        			visitorIdentity.setGender(js.get(i).path("gender").textValue());
	        			visitorIdentity.setBirthdate(Utils.formatDate(js.get(i).path("birthdate").textValue(), "yyyy-MM-dd"));
	        			visitorIdentity.setAddress(js.get(i).path("address").textValue());
	        			visitorIdentity.setPhoneNumber(js.get(i).path("phone_number").textValue());
	        			visitorIdentity.setEmail(js.get(i).path("email").textValue());
	        			
	        			if(fileIdentity.length > 0 && !fileIdentity[i].get().isEmpty()) {
	        				
	    					File file = new File(Utils.getLongNumberID());
	    					visitorIdentity.setIdFile(file.getId());
	        				file.setFolder("/");
	        				file.setSize(fileIdentity[i].get().getSize());
	        				file.setType(fileIdentity[i].get().getContentType());
	        				file.setName(fileIdentity[i].get().getOriginalFilename());
	        				file.setPersonAdded(akunTemp);
	        				file.setTimeAdded(new Date());
	        				file.setReferenceId(visitorIdentity.getId());
	        			
	        				String ext = FilenameUtils.getExtension(file.getName());
	            			
	        				storageService.storeWithEncrypted(fileIdentity[i].get(), file.getId() + "."+ext, file.getId(), false);
	        	    		
	        	    		fileMapper.insert(file);
	    				}
	        			visitorIdentityMapper.insert(visitorIdentity);
        			}else {
    					//System.out.println("2/DID U @_@");
    					//visitorIdentity.setId(visitorIdentityMapper.getList(param).get(0).getId());
        				visitorIdentity = visitorIdentityMapper.getList(param).get(0);
	        			visitorIdentity.setIdHeader(akunTemp);
	        			//visitorIdentity.setNoIdentity(vaultService.encrypt(js.get(i).path("no_identity")));
	        			visitorIdentity.setFullName(js.get(i).path("full_name").textValue());
	        			visitorIdentity.setTypeIdentity(js.get(i).path("type_identity").textValue());
	        			visitorIdentity.setGender(js.get(i).path("gender").textValue());
	        			visitorIdentity.setBirthdate(Utils.formatDate(js.get(i).path("birthdate").textValue(), "yyyy-MM-dd"));
	        			visitorIdentity.setAddress(js.get(i).path("address").textValue());
	        			visitorIdentity.setPhoneNumber(js.get(i).path("phone_number").textValue());
	        			visitorIdentity.setEmail(js.get(i).path("email").textValue());
	        			
	        			if(fileIdentity!=null && fileIdentity.length > 0) {
        					//System.out.println("DID U @_@ "+fileIdentity[i].get().toString().equals("null"));
        					//System.out.println(fileIdentity[i].get().toString().equals("null"));
        					//System.out.println(fileIdentity[i].get().toString().equals("null"));
	        				if(!fileIdentity[i].get().getOriginalFilename().equals("NONE")) {
		        				File file = fileMapper.getEntity(visitorIdentity.getIdFile());
		    					visitorIdentity.setIdFile(file.getId());
		        				file.setFolder("/");
		        				file.setSize(fileIdentity[i].get().getSize());
		        				file.setType(fileIdentity[i].get().getContentType());
		        				file.setName(fileIdentity[i].get().getOriginalFilename());
		        				file.setPersonAdded(akunTemp);
		        				file.setTimeAdded(new Date());
		        				file.setReferenceId(visitorIdentity.getId());
		        			
		        				String ext = FilenameUtils.getExtension(file.getName());
		            			
		        				//storageService.store(fileIdentity[i].get(), file.getId() + "."+ext);
		        				storageService.storeWithEncrypted(fileIdentity[i].get(), file.getId() + "."+ext, file.getId(), false);
		        	    		
		        	    		fileMapper.update(file);
	        				}
	    				}
	        			visitorIdentityMapper.update(visitorIdentity);
        			}
        			
        			PurchaseDetilVisitor o = new PurchaseDetilVisitor(Utils.getLongNumberID());
        			o.setHeaderId(data.getId());
        			o.setIdVisitorIdentity(visitorIdentity.getId());
        			o.setPhoneNumber(js.get(i).path("phone_number").textValue());
        			o.setEmail(js.get(i).path("email").textValue());
        			o.setResponsible(js.get(i).path("responsible").asText());
        			
        			if(i==0) {
	        			o.setPhoneNumberFamily(js.get(i).path("phone_number_family").textValue());
	        			o.setAddressFamily(js.get(i).path("address_family").textValue());
        			}
        			o.setAddress(js.get(i).path("address").textValue());

        			purchaseDetilVisitorMapper.insert(o);
    			}
    			
    			//save transitCamp
    			JsonNode jsObj = mapper.readTree(detilTransitCamp.get());//.get(0);
    					//new JSONObject(detilTransitCamp.get());
    			PurchaseDetilTransitCamp otc = new PurchaseDetilTransitCamp(Utils.getLongNumberID());
    			otc.setHeaderId(data.getId());
    			otc.setIdTransitCamp(jsObj.path("id_transit_camp").asText());
    			otc.setNameTransitCamp(jsObj.path("name_transit_camp").asText());
    			otc.setReportTimeCamp(jsObj.path("transit_camp").asText());
    			otc.setCountCamp(jsObj.path("count_camp").asInt());
    			otc.setCodeUnique(jsObj.path("code_unique").asText());
    			//purchaseDetilTransitCampMapper.insert(otc);
    			//end save transit camp
    			
    			//save luggage
    			js = mapper.readTree(detilLuggage.get());
    			ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
    				//ls.add(js.get(i).path("full_name"));
        			//other_luggage
    				PurchaseDetilLuggage olg = new PurchaseDetilLuggage(Utils.getLongNumberID());
        			olg.setHeaderId(data.getId());
    				olg.setTypeLuggage(js.get(i).path("type_luggage").textValue());
        			olg.setNameLuggage(js.get(i).path("name_luggage").textValue());
        			olg.setQtyLuggage(js.get(i).path("qty_luggage").asInt());
        			olg.setDescLuggage(js.get(i).path("desc_luggage").asText());
        		
    				purchaseDetilLuggageMapper.insert(olg);
    			}
    			
    			//end save luggage
    			
    			js = mapper.readTree(detilVehicle.get());
    			ls = new ArrayList<String>();
    			
    			List<Vehicle> lst_vehicle = vehicleMapper.getList(new QueryParameter());
    			for(int i=0; i < js.size(); i++) {
        			for(Vehicle o : lst_vehicle) {
        				if(o.getId().equals(js.get(i).path("vehicle_id").asText())) {
        					PurchaseDetilVehicle p = new PurchaseDetilVehicle(Utils.getLongNumberID());
                			p.setHeaderId(data.getId());
                			p.setName(o.getName());
                			p.setType(o.getTypeId());
                			p.setPrice((int) Math.round(o.getCost()));
                			p.setQty(js.get(i).path("qty_vehicle").asInt());
                			//purchaseDetilVehicleMapper.insert(p);
        				}
        			}
    			}
    			
    			js = mapper.readTree(detilGuide.get());
    			ls = new ArrayList<String>();
    			
    			List<TourGuide> lst_guide = tourGuideMapper.getList(new QueryParameter());
    			for(int i=0; i < js.size(); i++) {
        			for(TourGuide o : lst_guide) {
        				if(o.getId().equals(js.get(i).path("guide_id").toString())) {
        					PurchaseDetilTourGuide p = new PurchaseDetilTourGuide(Utils.getLongNumberID());
                			p.setHeaderId(data.getId());
                			p.setFullName(o.getFullName());
                			p.setGender(o.getGender());
                			p.setPhoto(o.getPhoto());
                			p.setDescription(o.getDescription());
                			p.setPhoneNumber(o.getPhoneNumber());
                			p.setEmail(o.getEmail());
                			p.setCostPerDay(o.getCostPerDay());
                			p.setOrganization(o.getOrganizationId());
                			//purchaseDetilTourGuideMapper.insert(p);
        				}
        			}
    			}

        		Random random = new Random();
        		data.setNumberUnique(random.nextInt(900) + 100);
    			data.setStatus(StatusPurchase.WAITING.name());
    			//set end region
    			data.setRegionEndId(regionEndId.get());
    			data.setRegionEndName(regionEndName.get());
    			
    			data.setAmountTicketWna(priceWna);
    			data.setAmountTicketWni(priceWni);
    			purchaseMapper.update(data);
    		}else {
    			QueryParameter param = new QueryParameter();
    			if(startDate.isPresent()) {
    	    		param.setInnerClause2(param.getInnerClause2() + " AND " + Purchase.START_DATE + " = '"+Utils.formatSqlDate(startDate.get())+"'");
    	    	}
    	    	
    			param.setInnerClause2(param.getInnerClause2() + " AND (" + Purchase.REGION_ID + " = '"+data.getRegionId()+"')");
    			param.setClause(param.getClause() + " AND (" + Region.ID + " = '"+data.getRegionId()+"')");
    	    	
    			Region region = regionMapper.getList(param).get(0);
        		if(region.getSisaKuota()< data.getCountTicket()) {
        			resp.setCode(HttpStatus.BAD_REQUEST.value());
        			resp.setMessage("Mohon Maaf Kuota tidak cukup.");
        			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
        		}
        		
    			//data.setStatusBooking(StatusBooking.);
    			data.setCodeBooking(Utils.codeBooking());
    			data.setStatus(StatusPurchase.DRAFT.name());
    			purchaseMapper.insert(data);
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    
    @RequestMapping(value="/rebooking", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> reBooking(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("parent_id") Optional<String> parentId,
    		@RequestParam("region_id") Optional<String> regionId,
    		@RequestParam("region_name") Optional<String> regionName,
    		@RequestParam("region_end_id") Optional<String> regionEndId,
    		@RequestParam("region_end_name") Optional<String> regionEndName,
    		@RequestParam("start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("datetime") Optional<Date> datetime,
    		@RequestParam("account_id") Optional<String> accountId,
    		@RequestParam("count_ticket") Optional<Integer> countTicket,
    		@RequestParam("count_ticket_wni") Optional<Integer> countTicketWni,
    		@RequestParam("count_ticket_wna") Optional<Integer> countTicketWna,
    		@RequestParam("amount_ticket")  @NumberFormat(pattern = "#,###.##") Optional<Double> amountTicket,
    		@RequestParam("amount_total_ticket")  @NumberFormat(pattern = "#,###.##") Optional<Double> amountTotalTicket,
    		@RequestParam("status") Optional<String> status,
    		@RequestParam("detil_visitor") Optional<String> detilVisitor,
    		@RequestParam("detil_guide") Optional<String> detilGuide,
    		@RequestParam("detil_vehicle") Optional<String> detilVehicle,
    		@RequestParam("detil_transit_camp") Optional<String> detilTransitCamp,
    		@RequestParam("detil_luggage") Optional<String> detilLuggage,
    		@RequestParam("other_luggage") Optional<String> otherLuggage,
			@RequestParam("file_identity") Optional<MultipartFile>[] fileIdentity,
    		@RequestParam("booking_not_allowed") Optional<String> bookingNotAllowed,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		Purchase data = new Purchase(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = purchaseMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(parentId.isPresent()) data.setParentId(parentId.get());
    		if(regionEndId.isPresent()) data.setRegionEndId(regionEndId.get());
    		if(regionEndName.isPresent()) data.setRegionEndName(regionEndName.get());
    		
//    		if(startDate.isPresent()) data.setStartDate(startDate.get());
//    		if(endDate.isPresent()) data.setEndDate(endDate.get());
//    		if(datetime.isPresent()) data.setDatetime(datetime.get());
//    		if(accountId.isPresent()) data.setAccountId(accountId.get());
    		
    		if(countTicketWni.isPresent()) data.setCountTicketWni(countTicketWni.get());
    		if(countTicketWna.isPresent()) data.setCountTicketWna(countTicketWna.get());
    		
    		if(amountTicket.isPresent()) data.setAmountTicket(amountTicket.get());
    		if(amountTotalTicket.isPresent()) data.setAmountTotalTicket(amountTotalTicket.get());
    		
    		//alternative validasi harga
//			Region regionKuota = regionMapper.getEntityCheckPrice(data.getRegionId(), Utils.formatSqlDate(data.getStartDate()));
//			int intAmountTotalTicket=regionKuota.getPriceRegion()*data.getCountTicket();
//			
//			if(intAmountTotalTicket != data.getAmountTotalTicket()) {
//    			resp.setCode(HttpStatus.BAD_REQUEST.value());
//    			resp.setMessage("HARGA TIDAK SESUAI DENGAN YANG TERDAFTAR OLEH SISTEM, SISTEM MENDETEKSI ANDA MENGUBAH HARGA <strong>"+(regionKuota.getPriceRegion()*data.getCountTicket())+ "</strong> Menjadi <strong>"+data.getAmountTotalTicket()+"</strong>");
//    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//    		}
    		
//    		if(status.isPresent()) data.setStatus(status.get());
    		
			data.setAccountId(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    		data.setDatetime(new Date());
    		
    		boolean pass = true;
    		/*if(data.getStartDate() == null || data.getEndDate() == null || data.getCountTicket()==null) {
    			pass = false;
    		}else {
    			if(data.getCountTicket()==0) {
        			pass = false;
        		}
    		}*/

    		if(data.getParentId()==null || data.getRegionEndId()==null || data.getRegionEndName()==null) {
    			pass = false;
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
//			if(!data.getStatus().equals(StatusPurchase.DRAFT.name())) {
//				resp.setCode(HttpStatus.BAD_REQUEST.value());
//				resp.setMessage("Mohon maaf, sistem mendeteksi anda telah melakukan submit sebelumnya, pastikan anda tidak login menggunakan dua device. silahkan refresh halaman ini untuk lanjut apabila anda merasa telah melakukan submit dan apabila belum silahkan menghubungi Costumer Service.");
//				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
//			}
			//data.setAmountTicket(data.getAmountTicket()+);
    		
			Purchase purchaseBefore = purchaseMapper.getEntity(parentId.get());
    		Random random = new Random();
    		data.setNumberUnique(random.nextInt(900) + 100);
			data.setStatus(StatusPurchase.WAITING.name());
			//set end region
			data.setRegionId(purchaseBefore.getRegionId());
			data.setRegionName(purchaseBefore.getRegionName());
			data.setStartDate(purchaseBefore.getStartDate());
			data.setEndDate(purchaseBefore.getEndDate());
			data.setCodeBooking(purchaseBefore.getCodeBooking());
			purchaseMapper.insert(data);
    		
			ObjectMapper mapper = new ObjectMapper();
    		JsonNode js = mapper.readTree(detilVisitor.get());
    		System.out.println(js);
    		System.out.println("TRY REDAD2"+ js.get(0).path("type_identity"));
    		System.out.println("TRY REDAD3"+ js.get(0).path("full_name").textValue());

			List<String> ls = new ArrayList<String>();
			for(int i=0; i < js.size(); i++) {
				ls.add(js.get(i).path("full_name").textValue());
				
    			QueryParameter param = new QueryParameter();
    			
    			if(decryptVisitorIdentity(js.get(i).path("no_identity").textValue(), request)!=null) param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+" ='"+decryptVisitorIdentity(js.get(i).path("no_identity").textValue(), request).getNoIdentity()+"'");
    			else {
    				param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+" ='"+null+"'");
    			}
    			
    			System.out.println("param"+param.getClause());
    			
    			VisitorIdentity visitorIdentity = new VisitorIdentity(Utils.getLongNumberID());
    			//cek apa identis ada, jika ada maka get data sebelumnya
    			if(visitorIdentityMapper.getCount(param) > 0) {
    				visitorIdentity = visitorIdentityMapper.getList(param).get(0);
    			}
    			//set value
    			visitorIdentity.setIdHeader(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    			visitorIdentity.setNoIdentity(vaultService.encrypt(js.get(i).path("no_identity").textValue()));
    			visitorIdentity.setFullName(js.get(i).path("full_name").textValue());
    			visitorIdentity.setTypeIdentity(js.get(i).path("type_identity").textValue());
    			visitorIdentity.setGender(js.get(i).path("gender").textValue());
    			visitorIdentity.setBirthdate(Utils.formatDate(js.get(i).path("birthdate").textValue(), "yyyy-MM-dd"));
    			visitorIdentity.setAddress(js.get(i).path("address").textValue());
    			visitorIdentity.setPhoneNumber(js.get(i).path("phone_number").textValue());
    			visitorIdentity.setEmail(js.get(i).path("email").textValue());
    			
				File file = new File(Utils.getLongNumberID());
    			//cek apa identis ada, jika ada maka get data sebelumnya
				if(visitorIdentityMapper.getCount(param) > 0) {
    				if(fileIdentity!=null && fileIdentity.length > 0) {
    					if(!fileIdentity[i].get().getOriginalFilename().equals("NONE")) {
	        				file = fileMapper.getEntity(visitorIdentity.getIdFile());
    					}
    				}
    			}
				file.setFolder("/");
				file.setSize(fileIdentity[i].get().getSize());
				file.setType(fileIdentity[i].get().getContentType());
				file.setName(fileIdentity[i].get().getOriginalFilename());
				file.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
				file.setTimeAdded(new Date());
				file.setReferenceId(visitorIdentity.getId());
			
				String ext = FilenameUtils.getExtension(file.getName());
    			
    			if(visitorIdentityMapper.getCount(param) > 0) {
    				if(fileIdentity!=null && fileIdentity.length > 0) {
    					if(!fileIdentity[i].get().getOriginalFilename().equals("NONE")) {
    						storageService.storeWithEncrypted(fileIdentity[i].get(), file.getId() + "."+ext, file.getId(), false);
    	    	    		fileMapper.update(file);
    					}
    				}
    			}else if(fileIdentity.length > 0 && !fileIdentity[i].get().isEmpty()) {
    				storageService.storeWithEncrypted(fileIdentity[i].get(), file.getId() + "."+ext, file.getId(), false);
    	    		fileMapper.insert(file);
				}

				visitorIdentity.setIdFile(file.getId());
				
    			if(visitorIdentityMapper.getCount(param) == 0) {
    				visitorIdentityMapper.insert(visitorIdentity);
    			}else {
    				visitorIdentityMapper.update(visitorIdentity);
    			}
    			
    			
    			PurchaseDetilVisitor o = new PurchaseDetilVisitor(Utils.getLongNumberID());
    			o.setHeaderId(data.getParentId());
    			o.setIdVisitorIdentity(visitorIdentity.getId());
    			o.setPhoneNumber(js.get(i).path("phone_number").textValue());
    			o.setEmail(js.get(i).path("email").textValue());
    			o.setResponsible(js.get(i).path("responsible").asText());
    			
    			if(i==0) {
        			o.setPhoneNumberFamily(js.get(i).path("phone_number_family").textValue());
        			o.setAddressFamily(js.get(i).path("address_family").textValue());
    			}
    			o.setAddress(js.get(i).path("address").textValue());
    			o.setStatus("TEMP");
    			o.setParentHeaderId(data.getId());
    			purchaseDetilVisitorMapper.insert(o);
			}
			
			
			//save transitCamp
			JsonNode jsObj = mapper.readTree(detilTransitCamp.get());//.get(0);
			//new JSONObject(detilTransitCamp.get());
			PurchaseDetilTransitCamp otc = new PurchaseDetilTransitCamp(Utils.getLongNumberID());
			otc.setHeaderId(data.getParentId());
			otc.setIdTransitCamp(jsObj.path("id_transit_camp").asText());
			otc.setNameTransitCamp(jsObj.path("name_transit_camp").asText());
			otc.setReportTimeCamp(jsObj.path("transit_camp").asText());
			otc.setCountCamp(jsObj.path("count_camp").asInt());
			//otc.setCodeUnique(jsObj.path("code_unique").asText());
			otc.setCodeUnique(jsObj.path("code_unique").asText().replaceAll("\n", ""));
			otc.setStatus("TEMP");
			purchaseDetilTransitCampMapper.insert(otc);
			//end save transit camp
			
			//save luggage
			js = mapper.readTree(detilLuggage.get());
			ls = new ArrayList<String>();
			for(int i=0; i < js.size(); i++) {
				//ls.add(js.get(i).path("full_name"));
    			//other_luggage
				PurchaseDetilLuggage olg = new PurchaseDetilLuggage(Utils.getLongNumberID());
    			olg.setHeaderId(data.getParentId());
				olg.setTypeLuggage(js.get(i).path("type_luggage").textValue());
    			olg.setNameLuggage(js.get(i).path("name_luggage").textValue());
    			olg.setQtyLuggage(js.get(i).path("qty_luggage").asInt());
    			olg.setDescLuggage(js.get(i).path("desc_luggage").asText());
    			olg.setStatus("TEMP");
				purchaseDetilLuggageMapper.insert(olg);
			}
			
    		
    		resp.setMessage("Data Rebooking telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
   
    @RequestMapping(value="/resave", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> reSave(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("region_id") Optional<String> regionId,
    		@RequestParam("region_name") Optional<String> regionName,
    		@RequestParam("start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("datetime") Optional<Date> datetime,
    		@RequestParam("account_id") Optional<String> accountId,
    		@RequestParam("count_ticket") Optional<Integer> countTicket,
    		@RequestParam("amount_ticket")  @NumberFormat(pattern = "#,###.##") Optional<Double> amountTicket,
    		@RequestParam("amount_total_ticket")  @NumberFormat(pattern = "#,###.##") Optional<Double> amountTotalTicket,
    		@RequestParam("status") Optional<String> status,
    		@RequestParam("detil_visitor") Optional<String> detilVisitor,
    		@RequestParam("detil_guide") Optional<String> detilGuide,
    		@RequestParam("detil_vehicle") Optional<String> detilVehicle,
    		@RequestParam("detil_transit_camp") Optional<String> detilTransitCamp,
    		@RequestParam("detil_luggage") Optional<String> detilLuggage,
    		@RequestParam("other_luggage") Optional<String> otherLuggage,
			@RequestParam("file_identity") Optional<MultipartFile>[] fileIdentity,
    		@RequestParam("booking_not_allowed") Optional<String> bookingNotAllowed,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Purchase data = new Purchase(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = purchaseMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Halaman yang anda akses sudah tidak valid, mohon lakukan pemesanan ulang, terima kasih.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(regionId.isPresent()) data.setRegionId(regionId.get());
    		if(regionName.isPresent()) data.setRegionName(regionName.get());
    		if(startDate.isPresent()) data.setStartDate(startDate.get());
    		if(endDate.isPresent()) data.setEndDate(endDate.get());
//    		if(datetime.isPresent()) data.setDatetime(datetime.get());
//    		if(accountId.isPresent()) data.setAccountId(accountId.get());
    		if(countTicket.isPresent()) data.setCountTicket(countTicket.get());
    		
    		//bocor ada case user iseng, ngubah harga dari frontend
	    	if(amountTicket.isPresent()) data.setAmountTicket(amountTicket.get());
	    	if(amountTotalTicket.isPresent()) data.setAmountTotalTicket(amountTotalTicket.get());
    		
	    	//alternative validasi harga
			/*Region regionKuota = regionMapper.getEntityCheckPrice(data.getRegionId(), Utils.formatSqlDate(data.getStartDate()));
			int intAmountTotalTicket=regionKuota.getPriceRegion()*data.getCountTicket();
			
			if(intAmountTotalTicket != data.getAmountTotalTicket()) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("HARGA TIDAK SESUAI DENGAN YANG TERDAFTAR OLEH SISTEM, SISTEM MENDETEKSI ANDA MENGUBAH HARGA <strong>"+(regionKuota.getPriceRegion()*data.getCountTicket())+ "</strong> Menjadi <strong>"+data.getAmountTotalTicket()+"</strong>");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}*/
			
    		String akunTemp="";
    		System.out.println("ikiding: "+request.getRemoteUser());
    		if(!getCookieTokenValue(request, cookieName).equals("")) {
    			akunTemp=extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
    			data.setAccountId(akunTemp);
    		}else {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Maaf perangkat anda belum Login.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		//if(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT)!=null) data.setAccountId(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
    		data.setDatetime(new Date());
    		
    		boolean pass = true;
    		if(data.getStartDate() == null || data.getEndDate() == null || data.getCountTicket()==null) {
    			pass = false;
    		}else {
    			if(data.getCountTicket()==0) {
        			pass = false;
        		}
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			if(!data.getStatus().equals(StatusPurchase.DRAFT.name())) {
    				resp.setCode(HttpStatus.BAD_REQUEST.value());
    				resp.setMessage("Mohon maaf, sistem mendeteksi anda telah melakukan submit sebelumnya, pastikan anda tidak login menggunakan dua device. silahkan refresh halaman ini untuk lanjut apabila anda merasa telah melakukan submit dan apabila belum silahkan menghubungi Costumer Service.");
    				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    			}
    			//data.setAmountTicket(data.getAmountTicket()+);
        		Random random = new Random();
        		data.setNumberUnique(random.nextInt(900) + 100);
    			data.setStatus(StatusPurchase.WAITING.name());
    			purchaseMapper.update(data);


    			//testa
    			ObjectMapper mapper = new ObjectMapper();
        		JsonNode js = mapper.readTree(detilVisitor.get());
        		System.out.println(js);
        		System.out.println("TRY REDAD2"+ js.get(0).path("type_identity"));
        		System.out.println("TRY REDAD3"+ js.get(0).path("full_name").textValue());
    			//new tsta
    			//JSONArray js = new JSONArray(detilVisitor.get());
    			List<String> ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
    				ls.add(js.get(i).path("full_name").textValue());
    				
        			QueryParameter param = new QueryParameter();
        			
        			if(decryptVisitorIdentity(js.get(i).path("no_identity").textValue(), request)!=null) param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+" ='"+decryptVisitorIdentity(js.get(i).path("no_identity").textValue(), request).getNoIdentity()+"'");
        			else {
        				param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+" ='"+null+"'");
        			}
        			System.out.println("param"+param.getClause());
        			VisitorIdentity visitorIdentity = new VisitorIdentity(Utils.getLongNumberID());
        			
        			if(visitorIdentityMapper.getCount(param) == 0) {
    					System.out.println("1.DID U @_@");
	        			visitorIdentity.setIdHeader(akunTemp);
	        			visitorIdentity.setNoIdentity(vaultService.encrypt(js.get(i).path("no_identity").textValue()));
	        			visitorIdentity.setFullName(js.get(i).path("full_name").textValue());
	        			visitorIdentity.setTypeIdentity(js.get(i).path("type_identity").textValue());
	        			visitorIdentity.setGender(js.get(i).path("gender").textValue());
	        			visitorIdentity.setBirthdate(Utils.formatDate(js.get(i).path("birthdate").textValue(), "yyyy-MM-dd"));
	        			visitorIdentity.setAddress(js.get(i).path("address").textValue());
	        			visitorIdentity.setPhoneNumber(js.get(i).path("phone_number").textValue());
	        			visitorIdentity.setEmail(js.get(i).path("email").textValue());
	        			
	        			if(fileIdentity.length > 0 && !fileIdentity[i].get().isEmpty()) {
	        				
	    					File file = new File(Utils.getLongNumberID());
	    					visitorIdentity.setIdFile(file.getId());
	        				file.setFolder("/");
	        				file.setSize(fileIdentity[i].get().getSize());
	        				file.setType(fileIdentity[i].get().getContentType());
	        				file.setName(fileIdentity[i].get().getOriginalFilename());
	        				file.setPersonAdded(akunTemp);
	        				file.setTimeAdded(new Date());
	        				file.setReferenceId(visitorIdentity.getId());
	        			
	        				String ext = FilenameUtils.getExtension(file.getName());
	            			
	        				storageService.storeWithEncrypted(fileIdentity[i].get(), file.getId() + "."+ext, file.getId(), false);
	        	    		
	        	    		fileMapper.insert(file);
	    				}
	        			visitorIdentityMapper.insert(visitorIdentity);
        			}else {
    					System.out.println("2/DID U @_@");
    					//visitorIdentity.setId(visitorIdentityMapper.getList(param).get(0).getId());
        				visitorIdentity = visitorIdentityMapper.getList(param).get(0);
	        			visitorIdentity.setIdHeader(akunTemp);
	        			//visitorIdentity.setNoIdentity(vaultService.encrypt(js.get(i).path("no_identity")));
	        			visitorIdentity.setFullName(js.get(i).path("full_name").textValue());
	        			visitorIdentity.setTypeIdentity(js.get(i).path("type_identity").textValue());
	        			visitorIdentity.setGender(js.get(i).path("gender").textValue());
	        			visitorIdentity.setBirthdate(Utils.formatDate(js.get(i).path("birthdate").textValue(), "yyyy-MM-dd"));
	        			visitorIdentity.setAddress(js.get(i).path("address").textValue());
	        			visitorIdentity.setPhoneNumber(js.get(i).path("phone_number").textValue());
	        			visitorIdentity.setEmail(js.get(i).path("email").textValue());
	        			
	        			if(fileIdentity!=null && fileIdentity.length > 0) {
        					System.out.println("DID U @_@ "+fileIdentity[i].get().toString().equals("null"));
        					System.out.println(fileIdentity[i].get().toString().equals("null"));
        					System.out.println(fileIdentity[i].get().toString().equals("null"));
	        				if(!fileIdentity[i].get().getOriginalFilename().equals("NONE")) {
		        				File file = fileMapper.getEntity(visitorIdentity.getIdFile());
		    					visitorIdentity.setIdFile(file.getId());
		        				file.setFolder("/");
		        				file.setSize(fileIdentity[i].get().getSize());
		        				file.setType(fileIdentity[i].get().getContentType());
		        				file.setName(fileIdentity[i].get().getOriginalFilename());
		        				file.setPersonAdded(akunTemp);
		        				file.setTimeAdded(new Date());
		        				file.setReferenceId(visitorIdentity.getId());
		        			
		        				String ext = FilenameUtils.getExtension(file.getName());
		            			
		        				//storageService.store(fileIdentity[i].get(), file.getId() + "."+ext);
		        				storageService.storeWithEncrypted(fileIdentity[i].get(), file.getId() + "."+ext, file.getId(), false);
		        	    		
		        	    		fileMapper.update(file);
	        				}
	    				}
	        			visitorIdentityMapper.update(visitorIdentity);
        			}
        			
        			PurchaseDetilVisitor o = new PurchaseDetilVisitor(Utils.getLongNumberID());
        			o.setHeaderId(data.getId());
        			o.setIdVisitorIdentity(visitorIdentity.getId());
        			o.setPhoneNumber(js.get(i).path("phone_number").textValue());
        			o.setEmail(js.get(i).path("email").textValue());
        			o.setResponsible(js.get(i).path("responsible").asText());
        			
        			if(i==0) {
	        			o.setPhoneNumberFamily(js.get(i).path("phone_number_family").textValue());
	        			o.setAddressFamily(js.get(i).path("address_family").textValue());
        			}
        			o.setAddress(js.get(i).path("address").textValue());

        			purchaseDetilVisitorMapper.insert(o);
    			}
    			
    			//save transitCamp
    			JsonNode jsObj = mapper.readTree(detilTransitCamp.get());//.get(0);
    					//new JSONObject(detilTransitCamp.get());
    			PurchaseDetilTransitCamp otc = new PurchaseDetilTransitCamp(Utils.getLongNumberID());
    			otc.setHeaderId(data.getId());
    			otc.setIdTransitCamp(jsObj.path("id_transit_camp").asText());
    			otc.setNameTransitCamp(jsObj.path("name_transit_camp").asText());
    			otc.setReportTimeCamp(jsObj.path("transit_camp").asText());
    			otc.setCountCamp(jsObj.path("count_camp").asInt());
    			//otc.setCodeUnique(jsObj.path("code_unique").asText());
    			otc.setCodeUnique(jsObj.path("code_unique").asText().replaceAll("\n", ""));
    			purchaseDetilTransitCampMapper.insert(otc);
    			//end save transit camp
    			
    			//save luggage
    			js = mapper.readTree(detilLuggage.get());
    			ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
    				//ls.add(js.get(i).path("full_name"));
        			//other_luggage
    				PurchaseDetilLuggage olg = new PurchaseDetilLuggage(Utils.getLongNumberID());
        			olg.setHeaderId(data.getId());
    				olg.setTypeLuggage(js.get(i).path("type_luggage").textValue());
        			olg.setNameLuggage(js.get(i).path("name_luggage").textValue());
        			olg.setQtyLuggage(js.get(i).path("qty_luggage").asInt());
        			olg.setDescLuggage(js.get(i).path("desc_luggage").asText());
        		
    				purchaseDetilLuggageMapper.insert(olg);
    			}
    			
    			//end save luggage
    			
    		}
    		
    		resp.setMessage("Data Rebooking telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
	@RequestMapping(value="/reschedule", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> reschedule(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("region_id") Optional<String> regionId,
    		@RequestParam("start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("detil_transit_camp") Optional<String> detilTransitCamp,
    		@RequestParam("detil_luggage") Optional<String> detilLuggage,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Purchase data = new Purchase(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = purchaseMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(regionId.isPresent()) data.setRegionId(regionId.get());
    		if(startDate.isPresent()) data.setStartDate(startDate.get());
    		if(endDate.isPresent()) data.setEndDate(endDate.get());
    		
//    		if(status.isPresent()) data.setStatus(status.get());
    		String akunTemp="";
    		System.out.println("ikiding: "+request.getRemoteUser());
    		if(!getCookieTokenValue(request, cookieName).equals("")) {
    			akunTemp=extractAccountLogin(request, AccountLoginInfo.ACCOUNT);
    			data.setAccountId(akunTemp);
    		}else {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Maaf perangkat anda belum Login.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		//if(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT)!=null) data.setAccountId(extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
    		data.setDatetime(new Date());
    		
    		boolean pass = true;
    		if(data.getStartDate() == null || data.getEndDate() == null || data.getCountTicket()==null) {
    			pass = false;
    		}else {
    			if(data.getCountTicket()==0) {
        			pass = false;
        		}
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			System.out.println("TRY RE UPDATE");
    			data.setStatusReschedule(null);
    			purchaseMapper.update(data);
    			
    			//delete transit camp
    			QueryParameter paramDetil = new QueryParameter();
    			paramDetil.setClause(paramDetil.getClause()+" AND "+PurchaseDetilTransitCamp.HEADER_ID+" ='"+data.getId()+"'");
    			purchaseDetilTransitCampMapper.deleteBatch(paramDetil);
    			//delete luggage
    			paramDetil = new QueryParameter();
    			paramDetil.setClause(paramDetil.getClause()+" AND "+PurchaseDetilLuggage.HEADER_ID+" ='"+data.getId()+"'");
    			purchaseDetilLuggageMapper.deleteBatch(paramDetil);
    			
    			ObjectMapper mapper = new ObjectMapper();
    			//save transitCamp
    			//JSONObject jsObj = new JSONObject(detilTransitCamp.get());
    			JsonNode jsObj = mapper.readTree(detilTransitCamp.get());//.get(0);
    			PurchaseDetilTransitCamp otc = new PurchaseDetilTransitCamp(Utils.getLongNumberID());
    			otc.setHeaderId(data.getId());
    			otc.setIdTransitCamp(jsObj.path("id_transit_camp").asText());
    			otc.setNameTransitCamp(jsObj.path("name_transit_camp").asText());
    			otc.setReportTimeCamp(jsObj.path("transit_camp").asText());
    			otc.setCountCamp(jsObj.path("count_camp").asInt());
    			otc.setCodeUnique(jsObj.path("code_unique").asText());
    			purchaseDetilTransitCampMapper.insert(otc);
    			//end save transit camp
    			
    			//save luggage
    			//JSONArray js = new JSONArray(detilLuggage.get());
    			JsonNode js = mapper.readTree(detilLuggage.get());//.get(0);
    			for(int i=0; i < js.size(); i++) {
    				PurchaseDetilLuggage olg = new PurchaseDetilLuggage(Utils.getLongNumberID());
        			olg.setHeaderId(data.getId());
    				olg.setTypeLuggage(js.get(i).path("type_luggage").asText());
        			olg.setNameLuggage(js.get(i).path("name_luggage").asText());
        			olg.setQtyLuggage(js.get(i).path("qty_luggage").asInt());
        			olg.setDescLuggage(js.get(i).path("desc_luggage").asText());
        		
    				purchaseDetilLuggageMapper.insert(olg);
    			}
    			//end save luggage
    			
    		}else {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("TERJADI RE-SAVE, Pada halaman ini tidak diizinkan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
    		@RequestParam("detil_luggage") Optional<String> detilLuggage,
    		@RequestParam("detil_visitor") Optional<String> detilVisitor,
    		@RequestParam("potensi_sampah") Optional<String> potensiSampah,
    		@RequestParam("send_sertifikat") Optional<String> sendSertifikat,
    		@RequestParam("description_message") Optional<String> descriptionMessage,
    		@RequestParam("banned_pendaki") Optional<String> bannedPendaki,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();

		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause() + " AND " + Purchase.ID + "=" + id);
		Purchase data = purchaseMapper.getListExtended(param).get(0);

        if(data.getParentId()!=null){
       	 QueryParameter param_detil = new QueryParameter();
			param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+data.getParentId()+"')");
			//param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is not null)");
			Purchase purchaseNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
			
			//purchase = purchaseNoParent;
			//manipulate this for childeren purchase cause the parent have what this need
			data.setPurchaseDetilVisitor(purchaseNoParent.getPurchaseDetilVisitor());
			data.setPurchaseDetilTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
			//data.getPurchaseDetilVisitor().get(0).setEmail(purchaseNoParent.getPurchaseDetilVisitor());
        }
        
		resp.setData(data);
		
		if(action.equals(DELETE)) {
			purchaseMapper.delete(data);
			
			//visitor
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilVisitor.HEADER_ID + "=" + data.getId());
			purchaseDetilVisitorMapper.deleteBatch(param);
			
			//luggage
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.HEADER_ID + "=" + data.getId());
			purchaseDetilLuggageMapper.deleteBatch(param);
			
			//transit camp
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilTransitCamp.HEADER_ID + "=" + data.getId());
			purchaseDetilTransitCampMapper.deleteBatch(param);
			
			//transaction
			PurchaseDetilTransaction transaction = purchaseDetilTransactionMapper.getEntityHeader(data.getId());
			if(transaction != null) {
				param = new QueryParameter();
				param.setClause(param.getClause() + " AND " + PurchaseDetilTransaction.HEADER_ID + "=" + data.getId());
				purchaseDetilTransactionMapper.deleteBatch(param);
				
				param = new QueryParameter();
				param.setClause(param.getClause() + " AND " + File.REFERENCE_ID + "=" + transaction.getId());
				fileMapper.deleteBatch(param);
				
				if(fileMapper.getCount(param) > 0) {
					String ext = FilenameUtils.getExtension(transaction.getFileName());
					storageService.delete(id + "." + ext);
				}
				
			}

			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilVehicle.HEADER_ID + "=" + data.getId());
			purchaseDetilVehicleMapper.deleteBatch(param);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilTourGuide.HEADER_ID + "=" + data.getId());
			purchaseDetilTourGuideMapper.deleteBatch(param);
			
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		
		if(action.equals("RESCHEDULE_NON_ACTIVE") || action.equals("RESCHEDULE_ACTIVE")) {
			data.setStatusReschedule(action);
			if(action.equals("RESCHEDULE_NON_ACTIVE")) data.setStatusReschedule(null);
			purchaseMapper.update(data);
			resp.setMessage("Data '"+data.getId()+"' telah "+action+".");
		}
		
		if(action.equals("PAYMENT") || action.equals("CANCEL") || action.equals("BOARDING")  || action.equals("KELUAR")) {
			if(!action.equals("BOARDING") && !action.equals("KELUAR")) data.setStatus(action);
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilVisitor.HEADER_ID + "=" + data.getId());
			List<PurchaseDetilVisitor> visitor = purchaseDetilVisitorMapper.getList(param);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilVehicle.HEADER_ID + "=" + data.getId());
			List<PurchaseDetilVehicle> vehicle = purchaseDetilVehicleMapper.getList(param);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilTourGuide.HEADER_ID + "=" + data.getId());
			List<PurchaseDetilTourGuide> guide = purchaseDetilTourGuideMapper.getList(param);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND " + PurchaseDetilTransaction.HEADER_ID + "=" + data.getId());
			List<PurchaseDetilTransaction> transaction = purchaseDetilTransactionMapper.getList(param);
			
			param = new QueryParameter();
			param.setClause(param.getClause() + " AND (" + PurchaseDetilTransitCamp.HEADER_ID+ " = '"+data.getId()+"')");
			List<PurchaseDetilTransitCamp> dataPurchaseDetilTransitCamp = purchaseDetilTransitCampMapper.getList(param);
			
			//data.setPurchaseDetilTransitCamp(dataPurchaseDetilTransitCamp);
			//data.setPurchaseDetilTransaction(transaction);
			//data.setPurchaseDetilVisitor(visitor);
			//data.setPurchaseDetilVehicle(vehicle);
			//data.setPurchaseDetilGuid(guide);
			
			try {
				data.getPurchaseDetilTransitCamp().get(0);
			} catch (Exception e) {
				List<PurchaseDetilTransitCamp> pdtc = new ArrayList<>();
				PurchaseDetilTransitCamp p = new PurchaseDetilTransitCamp();
				p.setCodeUnique("");
				p.setNameTransitCamp("-");
				p.setReportTimeCamp("-");
				pdtc.add(p);
				data.setPurchaseDetilTransitCamp(pdtc);
				// TODO: handle exception
			}
			
			//init data for email
			Map<String, String> dataEmail = new HashMap<>();
			dataEmail.put("id", data.getId());
			dataEmail.put("nameRegion", data.getRegionName()+" - "+data.getRegionEndName());

			SimpleDateFormat sda = new SimpleDateFormat("dd-MM-yyyy"); // HH:mm:ss
			dataEmail.put("startDate", sda.format(data.getStartDate()).toString());
			dataEmail.put("endDate", sda.format(data.getEndDate()).toString());
			
			dataEmail.put("countTicket", data.getCountTicket()+"");
			dataEmail.put("codeBooking", data.getCodeBooking());
			dataEmail.put("nameTransitCamp", data.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
			dataEmail.put("reportTime", data.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp());
			
			String codeUnique = data.getPurchaseDetilTransitCamp().get(0).getCodeUnique();
			dataEmail.put("codeUnique", codeUnique.equals("")?"-":codeUnique);
			
			/*//get configure
			RestTemplate restTemplate = new RestTemplate();
	        String workingURL = getLinkPurchaseUrl();
	        System.out.println(">>"+appState);
	        if((appState.equals("production"))){
	        	workingURL = workingURL.replaceAll("http", "https");
			}
			String urlRest = workingURL + "restql/multi/entity?entity=configure;agenda&limit=agenda->100000&filter=configure->name.in('TEMPLATE_EMAIL_ATTENTION')";
			
			// create http headers and add authorization header we just created
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        
	        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + accessTokenValue);
			HttpEntity<String> requestHeader = new HttpEntity<String>(headers);
			
			//call the api
			ResponseEntity<String> responseTemp = restTemplate.exchange(urlRest, HttpMethod.GET, requestHeader, String.class);
			System.out.println("hasil api: " + responseTemp);
			
			String templateEmailAttention="";
			if(responseTemp.getStatusCode().equals(HttpStatus.OK)) {
		        ObjectMapper mapper = new ObjectMapper();
				JsonNode root = mapper.readTree(responseTemp.getBody()!=null?responseTemp.getBody():"");
				JsonNode dataConfigure = root.path("data").get("configure");

			 	for(JsonNode jn : dataConfigure) {
			 		if(jn.path("name").textValue().equals("TEMPLATE_EMAIL_ATTENTION")) templateEmailAttention=jn.path("value").textValue();
			 		
			 	}
			}
			//*/
			String templateEmailAttention = "<div><strong>Perlu diperhatikan:</strong></div>\n"
			        + "<div>1. Harap melapor pada <strong>BASE CAMP</strong> jalur pendakian pada waktu yang telah ditentukan dan apabila melebihi waktu lapor maka pendakian tidak diijinkan untuk naik.</div>\n"
			        + "<div>2. Siapkan surat keterangan sehat dari dokter <strong>pada hari melakuan pendakian</strong> baik dari puskesmes maupun klinik, pada basecamp juga disediakan jasa pemeriksaan kesehatan.</div>";
			        
			dataEmail.put("templateEmailAttention", templateEmailAttention);
			
			dataEmail.put("fullName", data.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName());
			String email = "kayumiman@yahoo.co.id";
			email = data.getPurchaseDetilVisitor().get(0).getEmail();
			
			dataEmail.put("email", "EMAIL_SINGLE_TO:"+email);
			//only send mail, when status payment
			if(action.equals("PAYMENT")) {
				//do this first before send mail
				if(data.getParentId()!=null){ 
		       	 	//1.LUGGAGE
					//remove luggage with status null
					param = new QueryParameter();
					param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.HEADER_ID + "=" + data.getParentId());
					param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.STATUS + " is null");
					purchaseDetilLuggageMapper.deleteBatch(param);
					
					//update luggage status TEMP to null
					param = new QueryParameter();
					param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.HEADER_ID + "=" + data.getParentId());
					param.setClause(param.getClause() + " AND " + PurchaseDetilLuggage.STATUS + "=" + "'TEMP'");
					purchaseDetilLuggageMapper.updateStatusBatch(param);
					
		       	 	//2.TRANSIT CAMP
					//remove transit camp with status null
					param = new QueryParameter();
					param.setClause(param.getClause() + " AND " + PurchaseDetilTransitCamp.HEADER_ID + "=" + data.getParentId());
					param.setClause(param.getClause() + " AND " + PurchaseDetilTransitCamp.STATUS + " is null");
					purchaseDetilTransitCampMapper.deleteBatch(param);
					
					//update transit camp status TEMP to null
					param = new QueryParameter();
					param.setClause(param.getClause() + " AND " + PurchaseDetilTransitCamp.HEADER_ID + "=" + data.getParentId());
					param.setClause(param.getClause() + " AND " + PurchaseDetilTransitCamp.STATUS + "="+ "'TEMP'");
					purchaseDetilTransitCampMapper.updateStatusBatch(param);
					
		       	 	//3.VISITOR
					//update visigtor status TEMP to null
					param = new QueryParameter();
					param.setClause(param.getClause() + " AND " + PurchaseDetilVisitor.HEADER_ID + "=" + data.getParentId());
					param.setClause(param.getClause() + " AND " + PurchaseDetilVisitor.STATUS + "="+ "'TEMP'");
					purchaseDetilVisitorMapper.updateStatusBatch(param);
					
					/*QueryParameter param_detil = new QueryParameter();
					param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+data.getParentId()+"')");
					param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is not null)");
					Purchase purchaseNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
					
					data.setPurchaseDetilVisitor(purchaseNoParent.getPurchaseDetilVisitor());
					data.setPurchaseDetilTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
					*/
		        }
				System.out.println("where" + System.getProperty("java.io.tmpdir"));
		        FileInputStream inStream = appManagerService.reportPermohonanFormC(id);		
		    	String filename = id.replaceAll("/", "_") + ".pdf";
		    	Utils.copyToTmpDir(inStream, filename);
		    	java.io.File file = new java.io.File(System.getProperty("java.io.tmpdir"), filename);
		    	
				notificationService.sendMailTicket(dataEmail, file);
				data.setStatusBooking(StatusBooking.DONE.name());
				
			}else if(action.equals("CANCEL")) {
				if(descriptionMessage.isPresent()) {
					dataEmail.put("description_message", descriptionMessage.get());
					notificationService.sendMailGeneral(dataEmail, null);
				}
			}

			ObjectMapper mapper = new ObjectMapper();
			if(action.equals("BOARDING")) {
				data.setStatusBoarding("1");
				
				JsonNode js = mapper.readTree(detilVisitor.get());
				//new JSONArray(detilVisitor.get());
    			List<String> ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
        			PurchaseDetilVisitor o = purchaseDetilVisitorMapper.getEntity(js.get(i).path("id_visitor").asText());
        			o.setIsValid(true);
        			purchaseDetilVisitorMapper.update(o);
    			}
    			
    			// luggage
    			js = mapper.readTree(detilLuggage.get());//.get(0);
    			//new JSONArray(detilLuggage.get());
    			ls = new ArrayList<String>();
    			for(int i=0; i < js.size(); i++) {
    				PurchaseDetilLuggage olg = purchaseDetilLuggageMapper.getEntity(js.get(i).path("id_luggage").asText());
        			
        			olg.setQtyActualLuggage(js.get(i).path("qty_actual_luggage").asInt());
        			olg.setIsValidLuggage(true);
        		
    				purchaseDetilLuggageMapper.update(olg);
    			}
    			//
    			PurchaseDetilLuggage olg = new PurchaseDetilLuggage(Utils.getLongNumberID());
    			olg.setHeaderId(data.getId());
    			olg.setDescLuggage(potensiSampah.get());
    			purchaseDetilLuggageMapper.insert(olg);
    			
    			//
			}
			
			if(action.equals("RESCHEDULE")) {
//    			JSONObject jsObj = new JSONObject(detilTransitCamp.get());
//    			PurchaseDetilTransitCamp otc = new PurchaseDetilTransitCamp(Utils.getLongNumberID());
//    			otc.setHeaderId(data.getId());
//    			otc.setIdTransitCamp(jsObj.path("id_transit_camp"));
//    			otc.setNameTransitCamp(jsObj.path("name_transit_camp"));
//    			otc.setReportTimeCamp(jsObj.path("transit_camp"));
//    			otc.setCountCamp(Integer.valueOf(jsObj.path("count_camp")));
//    			otc.setCodeUnique(jsObj.path("code_unique"));
//    			purchaseDetilTransitCampMapper.update(otc);
			}
			
			if(action.equals("KELUAR")) {
				data.setStatusBoarding("2");
				if(sendSertifikat.isPresent()) {
					if(sendSertifikat.get().equals("1")) {
						FileInputStream inStream = appManagerService.sertifikat(id);		
				    	String filename = id.replaceAll("/", "_") + ".pdf";
				    	Utils.copyToTmpDir(inStream, "sertifikat"+filename);
				    	java.io.File file = new java.io.File(System.getProperty("java.io.tmpdir"), "sertifikat"+filename);
				    	
						notificationService.sendMailSertifikat(dataEmail, file);
					}else {
						dataEmail.put("description_message", descriptionMessage.get());
						notificationService.sendMailGeneral(dataEmail, null);
					}
				}
				
				if(bannedPendaki.isPresent()) {
					if(bannedPendaki.get().equals("YES")) {
						List<PurchaseDetilVisitor> listPDV = purchaseDetilVisitorMapper.getEntityHeader(data.getId());
		    			for(PurchaseDetilVisitor o : listPDV) {
		    				JsonNode js = mapper.readTree(detilVisitor.get());
		        			for(int i=0; i < js.size(); i++) {
		        				//System.out.println(js.get(i).path("id_visitor").asText()+"=="+o.getIdVisitorIdentity());
		        				if(js.get(i).path("id").asText().equals(o.getIdVisitorIdentity())) {
				        			VisitorIdentity visitorIdentity = visitorIdentityMapper.getEntity(o.getIdVisitorIdentity());
				        			visitorIdentity.setStatusValid("BANNED");
				        			visitorIdentity.setDateBanned(new Date());
				        			visitorIdentity.setDaysBanned(180);
				        			System.out.println("TRY BANNED:"+visitorIdentity.getFullName());
				        			visitorIdentityMapper.update(visitorIdentity);
		        				}
		        			}
		    			}
					}
				}
			}
			//System.out.println("AND TRY UPDATE:");
			purchaseMapper.update(data);
			resp.setMessage("Data '"+data.getId()+"' telah diupdate.");
		}
		System.out.println("action: "+action);
		if(action.equals("DISCLAIMER")) {
			data.setDisclaimer(1);
			purchaseMapper.update(data);
			resp.setMessage("Terimakasih atas disclaimer anda.");
			resp.setData(data);
		}
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
    @RequestMapping(value="/submit", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> submit(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("status") Optional<String> status,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Purchase data = new Purchase(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = purchaseMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		boolean pass = true;
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			if(status.isPresent()) {
    				data.setStatus(StatusPurchase.CANCEL.name());
    			}else {
    				data.setStatus(StatusPurchase.DRAFT.name());
    			}

    			purchaseMapper.update(data);
    		}
    		
    		resp.setMessage("Data telah berhasil diubah.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
	@RequestMapping(value = "/preview-export", method = RequestMethod.GET)
	public void exportOutput(
			@RequestParam("status") Optional<String> status,
    		@RequestParam("filter_tanggal_pendakian_s") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakianS,
    		@RequestParam("filter_tanggal_pendakian_e") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakianE,
			HttpServletRequest request, HttpServletResponse response
			) throws Exception {
		
		XSSFWorkbook workbook = new XSSFWorkbook();
		QueryParameter param = new QueryParameter();
		//param.setClause(param.getClause()+" AND "+UnitKompetensiTeknis.ID+" IN("+temp.substring(1)+")");
		
		Map<String, Boolean> map = new HashMap<>();
		map.put("center", true);
		Map<String, Boolean> mapTopCenter = new HashMap<>();
		mapTopCenter.put("top-center", true);
		Map<String, Boolean> mapBody = new HashMap<>();
		mapBody.put("wrap", true); 
		Map<String, Boolean> mapHeaderNoBorderRight = new HashMap<>();
		mapHeaderNoBorderRight.put("header", true);
		mapHeaderNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapHeaderNoBorderLeft = new HashMap<>();
		mapHeaderNoBorderLeft.put("header", true);
		mapHeaderNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapBodyNoBorderRight = new HashMap<>();
		mapBodyNoBorderRight.put("wrap", true);
		mapBodyNoBorderRight.put("no-border-right", true);
		Map<String, Boolean> mapBodyNoBorderLeft = new HashMap<>();
		mapBodyNoBorderLeft.put("wrap", true);
		mapBodyNoBorderLeft.put("no-border-left", true);
		Map<String, Boolean> mapTitle = new HashMap<>();
		mapTitle.put("title", true);
		mapTitle.put("center", true);
		mapTitle.put("no-border", true);
		StyleExcel style = new StyleExcel();
		
		//XSSFSheet sheet = workbook.createSheet("Master");
		//for(UnitKompetensiTeknis ukt : listUnitKompTeknis) {
		XSSFSheet sheet = workbook.createSheet("Laporan Purchase"); //name sheet

			// create header row
			CellRangeAddress range = null;
			int idxRow = 0;
			XSSFRow title = sheet.createRow(idxRow);
			title.createCell(0).setCellValue("Laporan Purchase");
			title.getCell(0).setCellStyle(style.styleAlign(workbook, "center"));
			//styleBody(workbook, mapBody)
			range = new CellRangeAddress(idxRow, idxRow, 0, 5);
			sheet.addMergedRegion(range);

			idxRow++;
			idxRow++;

			XSSFRow header = sheet.createRow(idxRow++);
			int idx = 0;
			String[] headerArr = {"NO:1:1","Nomor Purchase:1:1","Kode Booking:1:1","Jalur Pendakian:1:1","Tanggal Pendakian:1:1","Pendaki:1:1","Nama Lengkap:1:1","Alamat:1:1","No.HP:1:1","Rincian Biaya:1:1", "Total Biaya:1:1","Tanggal Booking:1:1","Status Booking:1:1","Status Bayar:1:1"};
			List<String> listHeader = new ArrayList<String>();
			for(String namaHeader : headerArr) { listHeader.add(namaHeader); }
			int maxBaris = 1;
			for(String namaHeader : listHeader) {
				if(namaHeader.split(":").length > 1) {
					String nama = namaHeader.split(":")[0];
					int jumlahBaris = Integer.parseInt(namaHeader.split(":")[1]);
					int jumlahKolom = Integer.parseInt(namaHeader.split(":")[2]);
					maxBaris = (jumlahBaris>maxBaris?jumlahBaris:maxBaris);
					header.createCell(idx).setCellValue(nama);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					
					if(jumlahBaris == 1 && jumlahKolom == 1) {} else {
						range = new CellRangeAddress(header.getRowNum(), header.getRowNum()+jumlahBaris-1, idx, idx+jumlahKolom-1);
						RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
						RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
						sheet.addMergedRegion(range);
					}
					idx += jumlahKolom;
				}else {
					header.createCell(idx).setCellValue(namaHeader);
					header.getCell(idx).setCellStyle(style.styleHeader(workbook));
					idx++;
				}
			}
			
			param = new QueryParameter();
			if(status.isPresent() && !status.get().equals("")) param.setClause(param.getClause()+" AND "+Purchase.STATUS+" ='"+status.get()+"'");
			param.setLimit(100000000);

			
			if(tanggalPendakianS.isPresent() && tanggalPendakianE.isPresent()) {
				param.setClause(param.getClause() + " AND " + Purchase.START_DATE + " BETWEEN '"+Utils.formatSqlDate(tanggalPendakianS.get())+"' AND '"+Utils.formatSqlDate(tanggalPendakianE.get())+"'");
			}
			
			List<Purchase> listPurchase = purchaseMapper.getListExtended(param);
			System.out.println("listPurchase.size():"+listPurchase.size());
			idx=3;
			int nomor=1;

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			for(Purchase p : listPurchase) {
				XSSFRow aRow = sheet.createRow(idx);
				aRow.createCell(0).setCellValue(nomor);
				aRow.getCell(0).setCellStyle(style.styleBody(workbook, mapBody));

				aRow.createCell(1).setCellValue(p.getId());
				aRow.getCell(1).setCellStyle(style.styleBody(workbook, mapBody));
				
				aRow.createCell(2).setCellValue(p.getCodeBooking());
				aRow.getCell(2).setCellStyle(style.styleBody(workbook, mapBody));
			
				aRow.createCell(3).setCellValue(p.getRegion().getName());
				aRow.getCell(3).setCellStyle(style.styleBody(workbook, mapBody));
			
				aRow.createCell(4).setCellValue(sdf.format((p.getStartDate())));
				aRow.getCell(4).setCellStyle(style.styleBody(workbook, mapBody));
			
				aRow.createCell(5).setCellValue(p.getCountTicket());
				aRow.getCell(5).setCellStyle(style.styleBody(workbook, mapBody));
				//

				int idxsub=6;
				for(int k=0; k < 4; k++) {
					aRow.createCell(idxsub).setCellValue("-");
					aRow.getCell(idxsub).setCellStyle(style.styleBody(workbook, mapBody));
					
					String fullName=" -", gender=" -", phoneNumber=" -", alamat=" -";
					int start=0;
					for(PurchaseDetilVisitor pdv : p.getPurchaseDetilVisitor()) {
						if(pdv.getResponsible().equals("0") && pdv.getVisitorIdentity().getGender()!=null) {
							fullName+=(start>0?"\n -":"")+pdv.getVisitorIdentity().getFullName()+"("+(pdv.getVisitorIdentity().getGender().equals("L")?"LAKI-LAKI":"PEREMPUAN")+")";
							phoneNumber+=(start>0?"\n -":"")+pdv.getVisitorIdentity().getPhoneNumber();
							alamat+=(start>0?"\n -":"")+pdv.getVisitorIdentity().getAddress();
							start++;
						}
					}
					aRow.getCell(6).setCellValue(fullName);
					if(aRow.getCell(7)!=null) aRow.getCell(7).setCellValue(alamat);
					if(aRow.getCell(8)!=null) aRow.getCell(8).setCellValue(phoneNumber);
					
					idxsub++;
				}
				//
				aRow.createCell(7+idxsub-7).setCellValue((p.getAmountTicket()!=null?p.getAmountTicket():0.0));
				aRow.getCell(7+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				String rincianBiaya=" PNBP:"+p.getAmountTotalTicket();
				for(PurchaseDetilPayment pdv : p.getPurchaseDetilPayment()) {
					rincianBiaya+="\n "+pdv.getName()+":"+pdv.getPrice();
				}
				
				aRow.createCell(6+idxsub-7).setCellValue(rincianBiaya);
				aRow.getCell(6+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
			
				aRow.createCell(8+idxsub-7).setCellValue(sdf.format((p.getDatetime())));
				aRow.getCell(8+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				String status_name=p.getStatusName().split(",")[0];
				String status_color=p.getStatusName().split(",")[1];
				String status_icon=p.getStatusName().split(",")[2];

				if(status_name.equals("WAITING")) status_name="Waiting for Payment";
				if(status_name.equals("VERIFYING")) status_name="Verifying Payment";
				
				aRow.createCell(9+idxsub-7).setCellValue(status_name);
				aRow.getCell(9+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				if(p.getStatusBooking()==null || p.getStatusBooking().equals("") || p.getStatusBooking().isEmpty() || p.getStatusBooking().isBlank()) {
					p.setStatusBooking("DRAFT");
					status_color="warning";
					status_icon="exclamation-circle";
				}

				aRow.createCell(10+idxsub-7).setCellValue(p.getStatusBooking());
				aRow.getCell(10+idxsub-7).setCellStyle(style.styleBody(workbook, mapBody));
				
				idx++;
				nomor++;
			}
			

			//too width
			//sheet.setColumnWidth(2, (75  * 256) + 200);
			sheet.setColumnWidth(0, (5  * 256) + 200); 
			sheet.setColumnWidth(1, (20  * 256) + 200); 
			sheet.setColumnWidth(2, (20  * 256) + 200); 
			sheet.setColumnWidth(3, (20  * 256) + 200); 
			sheet.setColumnWidth(4, (15  * 256) + 200); 
			sheet.autoSizeColumn(6);
			sheet.autoSizeColumn(7);
			sheet.setColumnWidth(8, (20  * 256) + 200); 
			sheet.setColumnWidth(9, (20  * 256) + 200); 
			sheet.setColumnWidth(10, (20  * 256) + 200); 
			sheet.setColumnWidth(11, (20  * 256) + 200); 
			sheet.setColumnWidth(12, (20  * 256) + 200); 
			sheet.setColumnWidth(13, (20  * 256) + 200); 
		//}

		response.setContentType("application/xlsx");
		response.setHeader("Content-Disposition", "attachment; filename=KAWASAN-"+"purchase-laporan"+"-"+new Date()+".xlsx"); //name file
		
		workbook.write(response.getOutputStream());
		workbook.close();
	}
	
	@RequestMapping(value = "/history_visitor/{ktp}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getCheckNIK(
			@PathVariable String ktp,
			@RequestParam("action") Optional<String> action,
    		HttpServletRequest request
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		VisitorIdentity data = decryptVisitorIdentity(ktp, request);
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
	 private VisitorIdentity decryptVisitorIdentity(String valueKTP, HttpServletRequest request) {
		VisitorIdentity data = null;
		QueryParameter param = new QueryParameter();
		param.setClause(param.getClause()+ " AND "+VisitorIdentity.ID_HEADER+" ='"+extractAccountLogin(request, AccountLoginInfo.ACCOUNT)+"'");
		param.setLimit(10000000);
		for(VisitorIdentity o : visitorIdentityMapper.getList(param)) {
			//System.out.println("before>>>"+vaultService.decrypt(o.getNoIdentity())+".equals"+(valueKTP));
			//boolean pass= vaultService.decrypt(o.getNoIdentity())!=null;
			if(vaultService.decrypt(o.getNoIdentity()).equals(valueKTP)) {
				data=visitorIdentityMapper.getEntity(o.getId());
				//param = new QueryParameter();
				//param.setClause(param.getClause()+" AND "+VisitorIdentity.NO_IDENTITY+"='"+o.getNoIdentity()+"'");
			}
		}
		//System.out.println("other"+ param.getClause());
		//if(!param.getClause().equals("1")) data=visitorIdentityMapper.getList(param).get(0);
		return data;
	 }
		
	@RequestMapping(value="/report/{type}", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> reportHarian(
    		@PathVariable String type,
    		@RequestParam("filter_tanggal_pendakian") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> tanggalPendakian,
    		@RequestParam("filter_tanggal_pendakian_s") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> tanggalPendakianS,
    		@RequestParam("filter_tanggal_pendakian_e") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> tanggalPendakianE,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		@RequestParam("filter_year_s") Optional<Integer> filterYearS,
    		@RequestParam("filter_year_e") Optional<Integer> filterYearE,
    		@RequestParam("filter_year") Optional<Integer> filterYear,
    		@RequestParam("filter_status_booking") Optional<String> filterStatusBooking,
    		@RequestParam("filter_status_boarding") Optional<String> filterStatusBoarding,
    		@RequestParam("filter_no_booking") Optional<String> filterNoBooking,
    		@RequestParam("filter_no_purchase") Optional<String> filteNoPurchase,
    		@RequestParam("filter_id_region") Optional<String> filterIdRegion,
    		HttpServletRequest request
    		)throws Exception {

		ResponseWrapperList resp = new ResponseWrapperList();
		
		QueryParameter param = new QueryParameter();
		List<Map> data = null;
		if(type.equals("harian")) {
			if(tanggalPendakianS.isPresent() && tanggalPendakianE.isPresent()) {
				param.setClause(param.getClause() + " AND " + "date_purchase" + " BETWEEN '"+Utils.formatSqlDate(tanggalPendakianS.get())+"' AND '"+Utils.formatSqlDate(tanggalPendakianE.get())+"'");
			}
			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " = 'PAYMENT' ");
			data = purchaseMapper.getRekapHarian(param);
			resp.setData(data);
		}else if(type.equals("bulanan")) {
			if(filterYear.isPresent()) {
				param.setClause(param.getClause() + " AND " + "year_purchase" + " BETWEEN '"+filterYear.get()+"' AND '"+filterYear.get()+"'");
			}
			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " = 'PAYMENT' ");
			param.setLimit(1000000000);
			List<PurchaseChart> listPurchaseChart = purchaseMapper.getRekapBulanan(param);
			// Default months (January to December)
	        Map<Integer, PurchaseChartDetil> defaultMonths = new HashMap<>();
	        for (int i = 1; i <= 12; i++) {
	            defaultMonths.put(i, new PurchaseChartDetil(i, 0, 0, 0));
	        }
	        // Combine default months with existing data
			for(PurchaseChart x1 : listPurchaseChart) {
				Map<Integer, PurchaseChartDetil> combinedPurchases = new HashMap<>(defaultMonths);
				for(PurchaseChartDetil x2 : x1.getRegionPurchase()) {
					combinedPurchases.put(x2.getMonthYear(), x2);
				}
				//x1.setRegionPurchase(new ArrayList<>(combinedPurchases.values()));
				List<PurchaseChartDetil> sortedList = new ArrayList<>(combinedPurchases.values());
				//sortedList.sort(Comparator.comparingInt(PurchaseChartDetil::getMonthYear));
				x1.setRegionPurchase(sortedList);
			}
			resp.setData(listPurchaseChart);
		}else if(type.equals("tahunan")) {
			if(filterYearS.isPresent() && filterYearE.isPresent()) {
				param.setClause(param.getClause() + " AND " + "month_year" + " BETWEEN '"+filterYearS.get()+"' AND '"+filterYearE.get()+"'");
			}
			param.setClause(param.getClause() + " AND " + Purchase.STATUS + " = 'PAYMENT' ");
			param.setLimit(1000000000);
			List<PurchaseChart> listPurchaseChart = purchaseMapper.getRekapTahunan(param);
			//List<Map> listMap = purchaseMapper.getRekapTahunan(param);
			// Default Year from we choice
	        Map<Integer, PurchaseChartDetil> defaultYears = new HashMap<>();
	        for (int i = filterYearS.get(); i <= filterYearE.get(); i++) {
	        	defaultYears.put(i, new PurchaseChartDetil(i, 0, 0, 0));
	        }
	        // Combine default year with existing data
			for(PurchaseChart x1 : listPurchaseChart) {
				Map<Integer, PurchaseChartDetil> combinedPurchases = new HashMap<>(defaultYears);
				for(PurchaseChartDetil x2 : x1.getRegionPurchase()) {
					combinedPurchases.put(x2.getMonthYear(), x2);
				}
				List<PurchaseChartDetil> sortedList = new ArrayList<>(combinedPurchases.values());
	            //sortedList.sort(Comparator.comparingInt(PurchaseChartDetil::getMonthYear));
				x1.setRegionPurchase(sortedList);
			}
			// Sort region_purchase by month_year
			for(PurchaseChart x1 : listPurchaseChart) {
		        Collections.sort(x1.getRegionPurchase(), new Comparator<PurchaseChartDetil>() {
		            @Override
		            public int compare(PurchaseChartDetil p1, PurchaseChartDetil p2) {
		            	 Integer monthYear1 = p1.getMonthYear();
		                 Integer monthYear2 = p2.getMonthYear();
		                 
		                 // Handle null values
		                 if (monthYear1 == null && monthYear2 == null) {
		                     return 0;
		                 } else if (monthYear1 == null) {
		                     return -1; // Put null values at the beginning
		                 } else if (monthYear2 == null) {
		                     return 1; // Put null values at the beginning
		                 }
		                 return monthYear1.compareTo(monthYear2);
		            }
		        });
			}
			resp.setData(listPurchaseChart);
		}else {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Tipe report yang diminta tidak dikenal..");
			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
		}
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
}
