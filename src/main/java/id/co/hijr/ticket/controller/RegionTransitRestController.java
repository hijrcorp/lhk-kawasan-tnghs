package id.co.hijr.ticket.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RegionTransitCampMapper;
import id.co.hijr.ticket.mapper.RegionMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.Region;
import id.co.hijr.ticket.model.RegionTransitCamp;

@Controller
@RequestMapping("/ticket/region-transit")
public class RegionTransitRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private RegionTransitCampMapper regionTransitCampMapper;

	@Autowired
	private FileMapper fileMapper;

	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;

	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
		if(startDate.isPresent() && !endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
    	}else if(!startDate.isPresent() && endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
    	}else if(startDate.isPresent() && endDate.isPresent()) {
    		
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
    	}
    	
    	
    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
    	}
    	
    	
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<RegionTransitCamp> data = regionTransitCampMapper.getList(param);
		
		resp.setCount(regionTransitCampMapper.getCount(param));
		
		resp.setData(data);
		
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		RegionTransitCamp data = regionTransitCampMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("header_id") Optional<String> headerId,
    		@RequestParam("code") Optional<String> code,
    		@RequestParam("id_master_transit_camp") Optional<String> idMasterTransitCamp,
    		@RequestParam("start_register") @DateTimeFormat(pattern = "HH:mm")  Optional<Date> startRegister,
    		@RequestParam("end_register") @DateTimeFormat(pattern = "HH:mm")  Optional<Date> endRegister,
    		@RequestParam("extimate_arrived") @DateTimeFormat(pattern = "HH:mm")  Optional<Date> extimateArrived,
    		@RequestParam("tent_small") Optional<Integer> numberOfSmallTents,
    		@RequestParam("tent_large") Optional<Integer> numberOfLargeTents,
    		@RequestParam("file") Optional<MultipartFile> filePhoto,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		RegionTransitCamp data = new RegionTransitCamp(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = regionTransitCampMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(headerId.isPresent()) data.setHeaderId(headerId.get());
    		if(code.isPresent()) data.setCode(code.get());
    		if(idMasterTransitCamp.isPresent()) data.setIdMasterTransitCamp(idMasterTransitCamp.get());
    		if(startRegister.isPresent()) data.setStartRegister(startRegister.get());
    		if(endRegister.isPresent()) data.setEndRegister(endRegister.get());
    		if(extimateArrived.isPresent()) data.setExtimateArrived(extimateArrived.get());
			if(numberOfLargeTents.isPresent()) data.setNumberOfLargeTents(numberOfLargeTents.get());
			if(numberOfSmallTents.isPresent()) data.setNumberOfSmallTents(numberOfSmallTents.get());
    		
    		boolean pass = true;
    		if( data.getIdMasterTransitCamp() == null || data.getStartRegister() == null || data.getEndRegister() == null || data.getExtimateArrived()== null) {
    			pass = false;
    		}else {
    			if(data.getIdMasterTransitCamp() .equals("")) {
        			pass = false;
        		}
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(filePhoto.isPresent() && !filePhoto.get().isEmpty()) {
    			if(filePhoto.get().getContentType().startsWith("image")) {
        			File file = new File(Utils.getLongNumberID());

    				data.setPhoto(file.getId());
    	    		data.setNamePhoto(filePhoto.get().getOriginalFilename());
    	    		
    				file.setFolder("/");
        				file.setSize(filePhoto.get().getSize());
        				file.setType(filePhoto.get().getContentType());
        				file.setName(filePhoto.get().getOriginalFilename());
        				file.setPersonAdded(this.extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
        				file.setTimeAdded(new Date());
        				file.setReferenceId(data.getId());
        			
        				String ext = FilenameUtils.getExtension(file.getName());
        			
        				storageService.store(filePhoto.get(), file.getId() + "."+ext);
    	    		
    	    		fileMapper.insert(file);
    			}else {
    				resp.setCode(HttpStatus.BAD_REQUEST.value());
    				resp.setMessage("Harap unggah file Gambar (JPG/PNG/GIF).");
    				return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    			}
    		}
    		
    		if(id.isPresent()) {
    			regionTransitCampMapper.update(data);
    		}else {
    			regionTransitCampMapper.insert(data);
    		}
    		data.setNameMasterTransitCamp(regionTransitCampMapper.getEntity(data.getId()).getNameMasterTransitCamp());
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			RegionTransitCamp data = regionTransitCampMapper.getEntity(id);
			resp.setData(data);
			regionTransitCampMapper.delete(data);
			//
			File file = fileMapper.getEntity(data.getPhoto());
			if(file!=null) {
				String ext = FilenameUtils.getExtension(file.getName());
				storageService.delete(file.getId() + "." + ext);
				fileMapper.delete(file);
			}
			
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
