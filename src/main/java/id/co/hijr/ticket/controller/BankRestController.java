package id.co.hijr.ticket.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountMapper;
import id.co.hijr.sistem.model.Account;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseDetilTourGuideMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVehicleMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper;
import id.co.hijr.ticket.mapper.BankMapper;
import id.co.hijr.ticket.mapper.TourGuideMapper;
import id.co.hijr.ticket.mapper.VehicleMapper;
import id.co.hijr.ticket.model.Bank;
import id.co.hijr.ticket.model.PurchaseDetilTourGuide;
import id.co.hijr.ticket.model.PurchaseDetilVehicle;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Vehicle;
import id.co.hijr.ticket.ref.StatusPurchase;

@Controller
@RequestMapping("/ticket/bank")
public class BankRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private BankMapper bankMapper;
	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
//			if(startDate.isPresent() && !endDate.isPresent()) {
//	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
//	    	}else if(!startDate.isPresent() && endDate.isPresent()) {
//	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
//	    	}else if(startDate.isPresent() && endDate.isPresent()) {
//	    		
//	    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
//	    	}
	    	
	    	
//	    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
//	    		param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
//	    	}
//	    	
	    	
	    	if(limit.isPresent()) {
	    		param.setLimit(limit.get());
	    	}
	    	
	    	int pPage = 1;
	    	if(page.isPresent()) {
	    		pPage = page.get();
	    		int offset = (pPage-1)*param.getLimit();
	    		param.setOffset(offset);
	    		
	    		if(noOffset.isPresent()) {
	    			param.setOffset(0);
	    			param.setLimit(pPage*param.getLimit());
	    		}
	    	}
	    //param.setOrder("datetime_purchase desc");
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<Bank> data = bankMapper.getList(param);

		resp.setCount(bankMapper.getCount(param));
		resp.setData(data);
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Bank data = bankMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("bank_name") Optional<String> name,
    		@RequestParam("bank_code") Optional<String> code,
    		@RequestParam("type") Optional<String> type,
//    		@RequestParam("start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("account_number") Optional<String> accountNumber,
    		@RequestParam("account_holder_name") Optional<String> accountHolderName,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		Bank data = new Bank(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = bankMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(name.isPresent()) data.setName(name.get());
    		if(code.isPresent()) data.setCode(code.get());
    		if(type.isPresent()) data.setType(type.get());
    		if(accountNumber.isPresent()) data.setAccountNumber(accountNumber.get());
    		if(accountHolderName.isPresent()) data.setAccountHolderName(accountHolderName.get());
    		
    		//data.setAccountId("1520819042544");//extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
    		//data.setDatetime(new Date());
    		
    		boolean pass = true;
    		if(data.getType() == null || data.getType().equals("")) {
    			pass = false;
    		}
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		if(data.getType().equals("ATM")) {
	    		if(data.getName() == null || data.getCode() == null || data.getAccountNumber()==null || data.getAccountHolderName()==null) {
	    			pass = false;
	    		}else {
	    			if(data.getName().equals("") || data.getCode().equals("") || data.getAccountNumber().equals("") || data.getAccountHolderName().equals("")) {
	        			pass = false;
	        		}
	    		}
    		}else if(data.getType().equals("VA")) {
	    		if(data.getName() == null || data.getCode() == null) {
	    			pass = false;
	    		}else {
	    			if(data.getName().equals("") || data.getCode().equals("")) {
	        			pass = false;
	        		}
	    		}
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		if(id.isPresent()) {
    			//data.setStatus(StatusPurchase.WAITING.name());
    			bankMapper.update(data);
    		}else {
    			//data.setStatus(StatusPurchase.DRAFT.name());
    			bankMapper.insert(data);
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		
		if(action.equals(DELETE)) {
			
			Bank data = bankMapper.getEntity(id);
			resp.setData(data);
			bankMapper.delete(data);
			
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		
		
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }

}
