package id.co.hijr.ticket.controller;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountMapper;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.Account;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseDetilLuggageMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTourGuideMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransitCampMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVehicleMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.TourGuideMapper;
import id.co.hijr.ticket.mapper.VehicleMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;
import id.co.hijr.ticket.model.PurchaseDetilTourGuide;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;
import id.co.hijr.ticket.model.PurchaseDetilVehicle;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Vehicle;
import id.co.hijr.ticket.ref.StatusBooking;
import id.co.hijr.ticket.ref.StatusPurchase;
import id.co.hijr.sistem.service.AppManagerService;
import id.co.hijr.sistem.service.NotificationService;

@Controller
@RequestMapping("/export")
public class ExportRestController extends BaseController {

	public final static String DELETE = "delete";
	
	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	
	@Autowired
	protected AppManagerService appManagerService;

	@Autowired
	NotificationService notificationService;
	
    @RequestMapping(value = "/e-ticket/{id}", method = RequestMethod.GET)
   	public ResponseEntity<ResponseWrapper> getETicketById(
   			HttpServletRequest request, 
   			HttpServletResponse response,
   			@PathVariable String id) throws Exception {
    	FileInputStream inStream = appManagerService.reportPermohonanFormC(id);		
    	String filename = id.replaceAll("/", "_") + ".pdf";
    	Utils.copyToTmpDir(inStream, filename);
    	
    	ResponseWrapper resp = new ResponseWrapper();
		resp.setMessage("File \"" + filename + "\" ditelah dikirim ke email anda.");
		resp.setData(filename);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   	}
	
    @RequestMapping(value = "/e-sertifikat/{id}", method = RequestMethod.GET)
   	public ResponseEntity<ResponseWrapper> getESertifikatById(
   			HttpServletRequest request, 
   			HttpServletResponse response,
   			@PathVariable String id) throws Exception {
    	FileInputStream inStream = appManagerService.sertifikat(id);		
    	String filename = id.replaceAll("/", "_") + ".pdf";
    	Utils.copyToTmpDir(inStream, "sertifikat"+filename);
    	
    	ResponseWrapper resp = new ResponseWrapper();
		resp.setMessage("File \"" + filename + "\" ditelah dikirim ke email anda.");
		resp.setData("sertifikat"+filename);

		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
   	}
    
    @RequestMapping(value = "/download", method = RequestMethod.GET)
	public void download(HttpServletResponse response, @RequestParam("filename") String filename) throws Exception {
		java.io.File file = new java.io.File(System.getProperty("java.io.tmpdir"), filename);
		generateReport(response, file.getName(), new FileInputStream(file));
		//file.delete();
	}

	public void generateReport(HttpServletResponse response, String filename, FileInputStream inStream) throws Exception {
		String contentType = "application/xlsx";
		if(filename.contains(".pdf")) contentType = "application/pdf";
		response.setContentType(contentType);
		response.setHeader("Content-Disposition", "filename="+filename);
		
		OutputStream outStream = response.getOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
         
        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }
        inStream.close();
        outStream.close();
	}
}
