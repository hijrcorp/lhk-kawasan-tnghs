package id.co.hijr.ticket.controller;

import java.io.FileInputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.TimeZone;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.AccountMapper;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.Account;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfo;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.AppManagerService;
import id.co.hijr.sistem.service.NotificationService;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.BankMapper;
import id.co.hijr.ticket.mapper.MasterPaymentMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilPaymentMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTourGuideMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilTransactionMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVehicleMapper;
import id.co.hijr.ticket.mapper.PurchaseDetilVisitorMapper;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RequestPurchaseMapper;
import id.co.hijr.ticket.mapper.TourGuideMapper;
import id.co.hijr.ticket.mapper.VehicleMapper;
import id.co.hijr.ticket.model.Bank;
import id.co.hijr.ticket.model.MasterPayment;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;
import id.co.hijr.ticket.model.PurchaseDetilPayment;
import id.co.hijr.ticket.model.PurchaseDetilTourGuide;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;
import id.co.hijr.ticket.model.PurchaseDetilVehicle;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;
import id.co.hijr.ticket.model.RequestPurchase;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Vehicle;
import id.co.hijr.ticket.ref.StatusBooking;
import id.co.hijr.ticket.ref.StatusPurchase;

@Controller
@RequestMapping("/ticket/payment")
public class PaymentRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private PurchaseMapper purchaseMapper;

	@Autowired
	private RequestPurchaseMapper requestPurchaseMapper;

	@Autowired
	private BankMapper bankMapper;

	@Autowired
	private MasterPaymentMapper masterPaymentMapper;

	@Autowired
	private PurchaseDetilPaymentMapper purchaseDetilPaymentMapper;

	@Autowired
	private PurchaseDetilTransactionMapper purchaseDetilTransactionMapper;

	
	@Autowired
	protected AppManagerService appManagerService;

	@Autowired
	NotificationService notificationService;


	@Autowired
	private FileMapper fileMapper;
	
	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	
    @Value("${app.state}")
    private String appState;
	
	@Value("${app.newurl.production.ntfc}")
    private String appUrlNtfcProd;
	
	@Value("${app.newurl.development.ntfc}")
    private String appUrlNtfcDevel;


	protected String getWorkingUrl() {
		return (appState.equals("development")?appUrlNtfcDevel:appUrlNtfcProd);
	}
	

	
	@Value("${app.newurl.production.linkPurchase}")
    private String appUrlPurchaseProd;
	
	@Value("${app.newurl.development.linkPurchase}")
    private String appUrlPurchaseDevel;
	
	protected String getLinkPurchaseUrl() {
		return (appState.equals("development")?appUrlPurchaseDevel:appUrlPurchaseProd);
	}

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapperList> doVirtualAccount(
			@PathVariable String id,
	    		//@RequestParam("id") Optional<String> id,
	    		//@RequestParam("header_id") Optional<String> headerId,
	    		@RequestParam("bank_id") Optional<String> bankId,
	    		//@RequestParam("bank_type") Optional<String> bankType,
	    		//@RequestParam("submited_date") Optional<Date> submitedDate,
	    		//@RequestParam("confirmation_payment") Optional<Integer> confirmationPayment,
	    		//@RequestParam("file_id") Optional<String> fileId,
	    		//@RequestParam("bri_signature") Optional<String> briSignature,
	    		//@RequestParam("bri_timestamp") Optional<String> briTimestamp,
	    		//@RequestParam("file_upload_date") Optional<Date> fileUploadDate,
			@PathVariable String action
    		) throws Exception {
    	ResponseWrapperList resp = new ResponseWrapperList();
    	
    	QueryParameter param = new QueryParameter();
    	param.setClause(param.getClause()+" AND "+Purchase.ID+" ='"+id+"'");
		Purchase data = purchaseMapper.getListExtended(param).get(0);
		if(data == null) {
			resp.setCode(HttpStatus.BAD_REQUEST.value());
			resp.setMessage("Data tidak ditemukan."+data.getId()+"<<");
			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
		}
		resp.setData(data);
		
		if(action.equals("CREATE_VA")) {
			//call api
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = null;
	        String yourString = "";
			HttpStatus status = HttpStatus.OK;
			
	        String baseurl = "https://sandbox.partner.api.bri.co.id";
	        String key = "CKHdlsSZfdJm68PeiyGYrG31XkYgcSed";
	        String secret = "FT7Xus6yCCsvbe7g";
	        
	        RestTemplate restTemplate = new RestTemplate();
	        String urlToken = baseurl + "/oauth/client_credential/accesstoken?grant_type=client_credentials";
	        String urlCreateVA = baseurl + "/v1/briva";
	        
	        //set body
			LinkedMultiValueMap<String, Object> payload = new LinkedMultiValueMap<String, Object>();
			payload.add("client_id", key);
			payload.add("client_secret", secret);
	        
	        //set headers
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	        
	        //init body and headers
	        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity<LinkedMultiValueMap<String, Object>>(payload, headers);
	        //submit the data
	        ResponseEntity<String> response = restTemplate.postForEntity(urlToken, request, String.class);
	        
	        System.out.println("hasil token VA api: " + response);
	        
	        status = response.getStatusCode();
			yourString = response.getBody()!=null?response.getBody():"";
			root = mapper.readTree(yourString);
			
			//if this okay, thats mean then TOKEN BRI hasbeen created with SUCCESS
			//and do generate signature-code
			if(status.equals(HttpStatus.OK)) {
			 	String path = "/v1/briva";
			 	String verb = "POST";
			 	String token = "Bearer "+root.path("access_token").textValue();
			 	SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			 	isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			 	String timestamp = isoFormat.format(new Date());
			 	//
	    		//Map object = new HashMap();
	    		JsonObject object = new JsonObject();
	    		//JSONObject object = new JSONObject();
	    		object.addProperty("institutionCode", "J104408"); //nanti ganti code ini dari yg diberikan oleh BRI
	    		object.addProperty("brivaNo", "77777"); //nomor unik perusahaan
	    		Bank bank = bankMapper.getEntity(bankId.get());
	    		String kode = Utils.callCodePayment();
	    		object.addProperty("custCode", kode); 
	    		object.addProperty("nama", bank.getAccountHolderName());
	    		object.addProperty("amount", String.valueOf(data.getAmountTotalTicket().intValue()));
	    		object.addProperty("keterangan", "");
    			SimpleDateFormat sdftime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("in"));
	    		object.addProperty("expiredDate", sdftime.format(data.getExpiredDate()));
	    		////
			    //org.json.simple.JSONObject jsona = (org.json.simple.JSONObject) object;
			    System.out.println(object);
	    		////
			 	//
			 	String body = object.toString();
	    		System.out.println("object.toString()"+body);
			 	String secret_key = secret;
				 	
				String strpayload = "path="+path+"&"+"verb="+verb+"&"+"token="+token+"&"+"timestamp="+timestamp+"&"+"body="+body;
				System.out.println(strpayload);
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("nashorn");
				
				engine.eval(Files.newBufferedReader(Paths.get("/Users/kayumiman/git/lhk-kawasan/src/main/resources/public/js/common-bri/hmac-sha256.js"), StandardCharsets.UTF_8));
				engine.eval(Files.newBufferedReader(Paths.get("/Users/kayumiman/git/lhk-kawasan/src/main/resources/public/js/common-bri/enc_base64.js"),StandardCharsets.UTF_8));
				///engine.eval(Files.newBufferedReader(Paths.get("/Users/kayumiman/git/lhk-kawasan/src/main/resources/public/js/common-bri/axios.min.js"),StandardCharsets.UTF_8));
				
				Invocable inv = (Invocable) engine;
				// call function from script file
				Object params[] = {strpayload, secret_key};
				Object json = engine.eval("CryptoJS"); 
				Object datas = inv.invokeMethod(json, "HmacSHA256", params); 
				
				
				Object params2[] = {"CryptoJS", "enc", "Base64"};
				json = engine.eval("{CryptoJS.enc.Base64}"); 
				Object datas2 = inv.invokeMethod(json, "stringify", datas); 
				System.out.println(datas2);
				System.out.println(timestamp);
				
				//inv.invokeFunction("CryptoJS.enc.Base64.stringify", inv.invokeFunction("CryptoJS.HmacSHA256", params));
				Map<String, Object> allPayload = new HashMap<>();
				allPayload.put("root", root);
				allPayload.put("signature", datas2);
				allPayload.put("timestamp", timestamp);
				resp.setData(allPayload);
				
		        //save this token, cause we need this token to continue create VA
				param = new QueryParameter();
				param.setClause(param.getClause()+" AND "+RequestPurchase.ID_HEADER_PURCHASE+" ='"+data.getId()+"'");
				List<RequestPurchase> lrp= requestPurchaseMapper.getList(param);
		        RequestPurchase requestPurchase = new RequestPurchase(Utils.getLongNumberID());
		        if(requestPurchaseMapper.getCount(param) > 0) requestPurchase = lrp.get(0);
		        requestPurchase.setIdHeaderPurchase(data.getId());
		        requestPurchase.setSubmitDate(new Date());
		        requestPurchase.setAccessToken(token);
		        requestPurchase.setPaymentCode(kode);
		        if(requestPurchaseMapper.getCount(param) > 0) {
		        	requestPurchaseMapper.update(requestPurchase);
		        }else {
		        	requestPurchaseMapper.insert(requestPurchase);
		        }
		        
			}
		}
        return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapperList> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("header_id") Optional<String> headerId,
    		@RequestParam("bank_id") Optional<String> bankId,
    		@RequestParam("bank_type") Optional<String> bankType,
    		@RequestParam("submited_date") Optional<Date> submitedDate,
    		@RequestParam("confirmation_payment") Optional<Integer> confirmationPayment,
    		@RequestParam("file_id") Optional<String> fileId,
    		@RequestParam("bri_signature") Optional<String> briSignature,
    		@RequestParam("bri_timestamp") Optional<String> briTimestamp,
    		@RequestParam("file_upload_date") Optional<Date> fileUploadDate,
    		HttpServletRequest request
    		) throws Exception {
    	
    	ResponseWrapperList resp = new ResponseWrapperList();
    		
    		PurchaseDetilTransaction data = new PurchaseDetilTransaction(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = purchaseDetilTransactionMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan."+data.getPaymentMethod()+"<<");
    			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		
    		//if(id.isPresent()) data.setId(id.get());
    		if(headerId.isPresent()) data.setHeaderId(headerId.get());
    		if(bankType.isPresent()) data.setPaymentMethod(bankType.get());
    		
    		if(bankId.isPresent()) data.setBankId(bankId.get());
    		if(submitedDate.isPresent()) data.setSubmitedDate(submitedDate.get());
    		if(confirmationPayment.isPresent()) data.setConfirmationPayment(confirmationPayment.get());
    		
    		//if(fileId.isPresent()) data.setFileId(fileId.get());
    		//if(fileUploadDate.isPresent()) data.setFileUploadDate(fileUploadDate.get());
    		//System.out.println("check my rose: "+new Date());
    		data.setSubmitedDate(new Date());
    		
    		boolean pass = true;
    		if(data.getHeaderId() == null || data.getBankId() == null) {
    			pass = false;
    		}
    		
    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Bank belum dipilih.");
    			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		System.out.println("GOLD?"+data.getHeaderId());
    		System.out.println("??"+purchaseMapper.getEntity(data.getHeaderId()));
    		
    		if(purchaseMapper.getEntity(data.getHeaderId())==null) {
    			QueryParameter param = new QueryParameter();
    			param.setClause(param.getClause()+" AND "+Purchase.ID+ "='"+data.getHeaderId()+"'");
    			if(purchaseMapper.getListExtended(param).size()==0) {
    				resp.setCode(HttpStatus.BAD_REQUEST.value());
    				resp.setMessage("Sesi tidak valid.");
    				return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
    			}
    		}
    		
    		if(data.getPaymentMethod().equals("VA")) {
	    		if(!briSignature.isPresent() && !briTimestamp.isPresent()) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Tidak Valid, Mohon coba bebera saat lagi.");
	    			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
	    		}
    		}
    		
    		if(id.isPresent()) {
        		//Random random = new Random();
        		//data.setNumberUnique(random.nextInt(900) + 100);
    			//data.setStatus(StatusPurchase.WAITING.name());
    			if(data.getConfirmationPayment()==1) {
    				Purchase purchase = purchaseMapper.getEntity(data.getHeaderId());
    				if(purchase==null) {
    					QueryParameter param = new QueryParameter();
    	    			param.setClause(param.getClause()+" AND "+Purchase.ID+ "='"+data.getHeaderId()+"'");
    	    			purchase = purchaseMapper.getListExtended(param).get(0);
    				}
    				purchase.setStatus(StatusPurchase.VERIFYING.name());
    				purchase.setStatusBooking(StatusBooking.ACTIVE.name());
    				purchaseMapper.update(purchase);
    			}
    			purchaseDetilTransactionMapper.update(data);
    		}else {
    			//update
    			QueryParameter param = new QueryParameter();
    			param.setClause(param.getClause()+" AND "+PurchaseDetilTransaction.HEADER_ID+" ='"+data.getHeaderId()+"'");
    			if(purchaseDetilTransactionMapper.getCount(param)>0) {
        			resp.setCode(HttpStatus.BAD_REQUEST.value());
        			resp.setMessage("Mohon maaf, sistem mendeteksi anda telah melakukan submit sebelumnya, pastikan anda tidak login menggunakan dua device. silahkan refresh halaman ini untuk lanjut apabila anda merasa telah melakukan submit dan apabila belum silahkan menghubungi Costumer Service.");
        			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));	
        		}
    			
    			param = new QueryParameter();
    			param.setClause(param.getClause()+" AND "+Purchase.ID+" ='"+data.getHeaderId()+"'");
    			Purchase purchase = purchaseMapper.getListExtended(param).get(0);
				purchase.setStatus(StatusPurchase.VERIFYING.name());
				purchase.setStatusBooking(StatusBooking.ACTIVE.name());

    			//data.setStatus(StatusPurchase.DRAFT.name());
				//String kode = Utils.callCodePayment();
				param = new QueryParameter();
    			param.setClause(param.getClause()+" AND "+RequestPurchase.ID_HEADER_PURCHASE+" ='"+data.getHeaderId()+"'");
    			List<RequestPurchase> rp = requestPurchaseMapper.getList(param);
    			if(requestPurchaseMapper.getCount(param)>0) {
        			data.setPaymentCode(rp.get(0).getPaymentCode());
    			}
    			purchaseDetilTransactionMapper.insert(data);
    			//
    			List<MasterPayment> listMasterPayment = masterPaymentMapper.getList(new QueryParameter());
    			int sumDetailPayment=0;
    			for(MasterPayment masterPayment : listMasterPayment) {
    				PurchaseDetilPayment pdp = new PurchaseDetilPayment(Utils.getLongNumberID());
    				pdp.setIdHeader(data.getHeaderId());
    				pdp.setName(masterPayment.getName());
    				
    				if(masterPayment.getName().equals("Jasa")) {
                      	pdp.setPrice(masterPayment.getPrice()*(purchase.getCountTicket()));
        				purchaseDetilPaymentMapper.insert(pdp);
                	}else if(masterPayment.getName().equals("Asuransi")) {
                      	pdp.setPrice(masterPayment.getPrice()*(purchase.getCountTicketWni())*(purchase.getTotalDays()));
        				purchaseDetilPaymentMapper.insert(pdp);
                    }else if(masterPayment.getName().equals("Asuransi WNA")) {
                    	pdp.setPrice(masterPayment.getPrice()*(purchase.getCountTicketWna())*(purchase.getTotalDays()));
                      	purchaseDetilPaymentMapper.insert(pdp);
                    }else if(masterPayment.getName().equals("Traking")) {
                    	pdp.setPrice(masterPayment.getPrice()*(purchase.getCountTicket()));
                      	purchaseDetilPaymentMapper.insert(pdp);
                    }
    				
    				sumDetailPayment+=pdp.getPrice();
    				
    				/*
    				int priceMasterPayment=0;
					if(masterPayment.getName().equals("PNBP Pendakian")) {
                      	//mp.getPrice()*purchase.getCountTicket()
                    } else if(masterPayment.getName().equals("Jasa")) {
                      	priceMasterPayment+=masterPayment.getPrice()*purchase.getCountTicket();
                	}else if(masterPayment.getName().equals("Asuransi")) {
                      	priceMasterPayment+=masterPayment.getPrice()*purchase.getCountTicketWni()*purchase.getTotalDays();
                    }else if(masterPayment.getName().equals("Asuransi WNA")) {
                      	priceMasterPayment+=masterPayment.getPrice()*purchase.getCountTicketWna()*purchase.getTotalDays();
                    }
    				*/
    			}
    			//set update payment with detail payment
    			purchase.setAmountTicket(purchase.getAmountTotalTicket()+Double.valueOf(sumDetailPayment));
				purchaseMapper.update(purchase);
    			//
    			//reInit to call some function value
				PurchaseDetilTransaction pdt = purchaseDetilTransactionMapper.getEntityHeader(purchase.getId());
    			System.out.println("data.getExpiredDate() "+pdt.getExpiredDate());
    			
    			//send invoice pembayaran
    			Map<String, String> dataEmail = new HashMap<>();
    			dataEmail.put("id", data.getId());
    			dataEmail.put("nameRegion", purchase.getRegionName()+" - "+purchase.getRegionEndName());

    			SimpleDateFormat sda = new SimpleDateFormat("dd-MM-yyyy"); // HH:mm:ss
    			dataEmail.put("startDate", sda.format(purchase.getStartDate()).toString());
    			dataEmail.put("endDate", sda.format(purchase.getEndDate()).toString());
    			
    			dataEmail.put("countTicket", purchase.getCountTicket()+"");

		         if(purchase.getParentId()!=null){
		        	QueryParameter param_detil = new QueryParameter();
					param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
					Purchase purchaseNoParent= purchaseMapper.getListExtended(param_detil).get(0);
					
					purchase.setPurchaseDetilVisitor(purchaseNoParent.getPurchaseDetilVisitor());
					purchase.setPurchaseDetilTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp());
		         }
		         
		        try {
		        	purchase.getPurchaseDetilTransitCamp().get(0);
				} catch (Exception e) {
					List<PurchaseDetilTransitCamp> pdtc = new ArrayList<>();
					PurchaseDetilTransitCamp p = new PurchaseDetilTransitCamp();
					p.setCodeUnique("");
					p.setNameTransitCamp("-");
					pdtc.add(p);
					purchase.setPurchaseDetilTransitCamp(pdtc);
					// TODO: handle exception
				}
    			dataEmail.put("nameTransitCamp", purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
    			dataEmail.put("reportTime", purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp());
    			
    			String codeUnique = purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique();
    			dataEmail.put("codeUnique", codeUnique.equals("")?"-":codeUnique);
    			dataEmail.put("fullName", purchase.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName());
    			String email = "kayumiman@yahoo.co.id";
    			email = purchase.getPurchaseDetilVisitor().get(0).getEmail();
    			
    			dataEmail.put("email", "EMAIL_SINGLE_TO:"+email);
    			//digit tiga terakhir adalah kode aplikasi
    			String kodeSplit=purchase.getAccountId();
    			dataEmail.put("userAdded", data.getId());
    			
    			//bank
    			dataEmail.put("numberUnique", purchase.getNumberUnique()+"");
    			
    			java.text.NumberFormat formatter = java.text.NumberFormat.getCurrencyInstance();
				Locale myIndonesianLocale = new Locale("in", "ID");
				formatter.setCurrency(Currency.getInstance(myIndonesianLocale));
    			dataEmail.put("totalAmount", formatter.format(purchase.getAmountTicket()));
    			dataEmail.put("typeBank", pdt.getBank().getType());
    			dataEmail.put("nameBank", pdt.getBank().getName());
    			dataEmail.put("codeBank", pdt.getBank().getCode());
    			
    			if(pdt.getBank().getType().equals("VA")) {
        			SimpleDateFormat sdftime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("in"));
        			dataEmail.put("expiredDate", sdftime.format(purchase.getExpiredDate()));
        			dataEmail.put("keterangan", "");
        			dataEmail.put("accountHolderName", pdt.getBank().getAccountHolderName());

        			param = new QueryParameter();
        			param.setClause(param.getClause()+" AND "+RequestPurchase.ID_HEADER_PURCHASE+" ='"+data.getHeaderId()+"'");
        			rp = requestPurchaseMapper.getList(param);
        			if(requestPurchaseMapper.getCount(param)>0) {
        				dataEmail.put("access_token", rp.get(0).getAccessToken());
            			dataEmail.put("accountNumberBank", rp.get(0).getPaymentCode());
        			}
        			//Map<String, String> payload = new HashMap<>();
        			dataEmail.put("BRI-Timestamp", briTimestamp.get());
        			dataEmail.put("BRI-Signature", briSignature.get());
        			
        			
        			int amount = purchase.getAmountTicket().intValue();
        			dataEmail.put("totalAmount", amount+"");
        			notificationService.createVA(dataEmail);

        			sdftime = new SimpleDateFormat("EEEE, dd MMM yyyy hh:mm", new Locale("in"));
        			String b = (sdftime.format(pdt.getExpiredDate()));
        			dataEmail.put("expiredDate", b);
        			dataEmail.put("totalAmount", formatter.format(purchase.getAmountTicket()));
    			}else if(pdt.getBank().getType().equals("ATM")) {
        			SimpleDateFormat sdftime = new SimpleDateFormat("EEEE, dd MMM yyyy hh:mm", new Locale("in"));
        			String b = (sdftime.format(pdt.getExpiredDate()));
        			dataEmail.put("expiredDate", b);
        			
        			dataEmail.put("accountNumberBank", pdt.getBank().getAccountNumber());
        			dataEmail.put("accountHolderName", pdt.getBank().getAccountHolderName());
        			
    			}
    			dataEmail.put("numberPurchase", data.getHeaderId());
    			//always send mail when create payemnt
		       notificationService.sendMailInvoice(dataEmail);
		       
		       //always send url callback, daripada harus modif di client lagi
		       //'http://localhost:7078/ntfc/notification/check/'+params
		       resp.setNextPage(getWorkingUrl()+"/notification/app/kawasan/check/"+data.getId());
    		}
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
    }
   

	
    @RequestMapping(value="/submit-provement", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapperList> submitProvement(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("account_name_sender") Optional<String> accountNameSender,
    		@RequestParam("account_number_sender") Optional<String> accountNumberSender,
    		@RequestParam("bank_name_sender") Optional<String> bankNameSender,
    		@RequestParam("account_name_receiver") Optional<String> accountNameReceiver,
    		@RequestParam("account_number_receiver") Optional<String> accountNumberReceiver,
    		@RequestParam("bank_name_receiver") Optional<String> bankNameReceiver,
    		@RequestParam("transaction_date") @DateTimeFormat(pattern = "yyyy-MM-dd") Optional<Date> transactionDate,
			@RequestParam("file_transaction") Optional<MultipartFile> fileTransaction,
    		@RequestParam("email_notification") Optional<String> emailNotification,
    		HttpServletRequest request
    		) throws Exception {
    	
    	ResponseWrapperList resp = new ResponseWrapperList();
    		
    		PurchaseDetilTransaction data = purchaseDetilTransactionMapper.getEntity(id.get());
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan."+data.getPaymentMethod()+"<<");
    			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
    		}

    		if(accountNameSender.isPresent()) data.setAccountNameSender(accountNameSender.get());
    		if(accountNumberSender.isPresent()) data.setAccountNumberSender(accountNumberSender.get());
    		if(bankNameSender.isPresent()) data.setBankNameSender(bankNameSender.get());
    		if(accountNameReceiver.isPresent()) data.setAccountNameReceiver(accountNameReceiver.get());
    		if(accountNumberReceiver.isPresent()) data.setAccountNumberReceiver(accountNumberReceiver.get());
    		if(bankNameReceiver.isPresent()) data.setBankNameReceiver(bankNameReceiver.get());
    		
    		if(transactionDate.isPresent()) data.setTransactionDate(transactionDate.get());
    		
    		boolean pass = true;
    		if(data.getAccountNumberSender() == null || data.getAccountNumberSender() == null || data.getBankNameSender() == null
    			|| data.getAccountNumberReceiver() == null || data.getAccountNumberReceiver() == null || data.getBankNameReceiver() == null 
    			|| data.getTransactionDate() == null)
    		{
    			pass = false;
    		}else {
    			if(data.getAccountNumberSender().equals("") || data.getAccountNumberSender().equals("") || data.getBankNameSender().equals("")
    	    			|| data.getAccountNumberReceiver().equals("") || data.getAccountNumberReceiver().equals("") || data.getBankNameReceiver().equals(""))
    	    		{
    	    			pass = false;
    	    		}
    		}

    		if(fileTransaction.isPresent()  && !fileTransaction.get().isEmpty()) {}else {
    			pass = false;
    		}
    		
    		if(!emailNotification.isPresent()) {
    			pass = false;
    		}else {
    			if(emailNotification.get().equals("")) pass = false;
    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Mohon maaf, Inputan Anda belum lengkap.");
    			return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		File file = new File(Utils.getLongNumberID());
    		if(fileTransaction.isPresent()  && !fileTransaction.get().isEmpty()) {
    			//data.setFileTransaction(file.getId());
    			data.setFileTransaction(fileTransaction.get().getOriginalFilename());
    			file.setFolder("/");
    			file.setSize(fileTransaction.get().getSize());
    			file.setType(fileTransaction.get().getContentType());
    			file.setName(fileTransaction.get().getOriginalFilename());
    			file.setPersonAdded(extractAccountLogin(request, AccountLoginInfo.ACCOUNT));
    			file.setTimeAdded(new Date());
    			file.setReferenceId(data.getId());
    		
    			String ext = FilenameUtils.getExtension(file.getName());
    			
    			storageService.store(fileTransaction.get(), file.getId() + "."+ext);
        		
        		fileMapper.insert(file);
			}
    		
			purchaseDetilTransactionMapper.update(data);

			//send confirm mail
    		QueryParameter param = new QueryParameter();
    		param.setClause(param.getClause()+" AND "+Purchase.ID+" ='"+data.getHeaderId()+"'");
			Purchase purchase = purchaseMapper.getListExtended(param).get(0);
			

	         if(purchase.getParentId()!=null){
	        	 QueryParameter param_detil = new QueryParameter();
				param_detil.setClause(param_detil.getClause() + " AND (" + Purchase.ID + " = '"+purchase.getParentId()+"')");
				param_detil.setInnerClause(param_detil.getInnerClause() + " AND (" + PurchaseDetilVisitor.STATUS + " is not null)");
				Purchase purchaseNoParent= purchaseMapper.getListExtended(param_detil).get(0); //getEntity(id);
				
				//purchase = purchaseNoParent;
				//manipulate this for childeren purchase cause the parent have what this need
				purchase.setPurchaseDetilVisitor(purchaseNoParent.getPurchaseDetilVisitor());
				purchase.setPurchaseDetilTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp());//.get(0).setNameTransitCamp(purchaseNoParent.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
	         }
	         
	        try {
		        purchase.getPurchaseDetilTransitCamp().get(0);
			} catch (Exception e) {
				List<PurchaseDetilTransitCamp> pdtc = new ArrayList<>();
				PurchaseDetilTransitCamp p = new PurchaseDetilTransitCamp();
				p.setCodeUnique("");
				p.setNameTransitCamp("-");
				pdtc.add(p);
				purchase.setPurchaseDetilTransitCamp(pdtc);
				// TODO: handle exception
			}
			
			Map<String, String> dataEmail = new HashMap<>();
			dataEmail.put("id", purchase.getId());
			dataEmail.put("nameRegion", purchase.getRegionName()+" - "+purchase.getRegionEndName());

			SimpleDateFormat sda = new SimpleDateFormat("dd-MM-yyyy"); // HH:mm:ss
			dataEmail.put("startDate", sda.format(purchase.getStartDate()).toString());
			dataEmail.put("endDate", sda.format(purchase.getEndDate()).toString());
			
			dataEmail.put("countTicket", purchase.getCountTicket()+"");
			dataEmail.put("codeBooking", purchase.getCodeBooking());
			dataEmail.put("nameTransitCamp", purchase.getPurchaseDetilTransitCamp().get(0).getNameTransitCamp());
			dataEmail.put("reportTime", purchase.getPurchaseDetilTransitCamp().get(0).getReportTimeCamp());
			
			String codeUnique = purchase.getPurchaseDetilTransitCamp().get(0).getCodeUnique();
			dataEmail.put("codeUnique", codeUnique.equals("")?"-":codeUnique);
			dataEmail.put("fullName", purchase.getPurchaseDetilVisitor().get(0).getVisitorIdentity().getFullName());
			String email = "";
			if(emailNotification.isPresent()) {
				for(String emailStr : emailNotification.get().split(",")) {
					email+=",EMAIL_SINGLE_TO:"+emailStr;
				}
			}
			dataEmail.put("email", email.substring(1));
			//"EMAIL_SINGLE_TO:"+email+",EMAIL_SINGLE_TO:"+"carbonibag@gmail.com");
			dataEmail.put("userAdded", data.getId());
			dataEmail.put("linkPurchase", getLinkPurchaseUrl()+"?kode_booking="+purchase.getCodeBooking());
			
			//fileTransaction.get()
			
	    	//fileTransaction.get()
			String filename = file.getId() + "."+FilenameUtils.getExtension(file.getName());
			FileInputStream inStream = new FileInputStream(storageService.load(filename));		
	    	
	    	Utils.copyToTmpDir(inStream, filename);

	    	java.io.File files = new java.io.File(System.getProperty("java.io.tmpdir"), filename);
	    	
			
			notificationService.sendMailPurchaseConfirm(dataEmail, files);
    		resp.setMessage("Data telah berhasil disimpan.");
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapperList>(resp, HttpStatus.valueOf(resp.getCode()));
    }

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.GET, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		PurchaseDetilTransaction data = purchaseDetilTransactionMapper.getEntity(id);
		if(action.equals("CHECK_TIMELEFT")) {

			
			//resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}
		resp.setData(data);
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }


}
