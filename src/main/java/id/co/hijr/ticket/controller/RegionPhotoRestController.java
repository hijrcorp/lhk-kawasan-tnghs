package id.co.hijr.ticket.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import id.co.hijr.sistem.common.BaseController;
import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.sistem.common.ResponseWrapper;
import id.co.hijr.sistem.common.ResponseWrapperList;
import id.co.hijr.sistem.common.Utils;
import id.co.hijr.sistem.mapper.FileMapper;
import id.co.hijr.sistem.model.File;
import id.co.hijr.sistem.ref.AccountLoginInfoOL;
import id.co.hijr.sistem.service.StorageService;
import id.co.hijr.sistem.MainApplication;
import id.co.hijr.ticket.mapper.PurchaseMapper;
import id.co.hijr.ticket.mapper.RegionFacilityMapper;
import id.co.hijr.ticket.mapper.RegionMapper;
import id.co.hijr.ticket.mapper.RegionPhotoMapper;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.Region;
import id.co.hijr.ticket.model.RegionFacility;
import id.co.hijr.ticket.model.RegionPhoto;

@Controller
@RequestMapping("/ticket/region-photo")
public class RegionPhotoRestController extends BaseController {

	public final static String DELETE = "delete";

	@Autowired
	private RegionPhotoMapper regionPhotoMapper;

	
	@Autowired
	@Qualifier("fileSystemStorage")
	protected StorageService storageService;
	
	@Autowired
	private FileMapper fileMapper;
	

	
	@RequestMapping(value="/list", method = RequestMethod.GET, produces = "application/json")
	@Transactional(rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> getList(
    		@RequestParam("limit") Optional<Integer> limit,
    		@RequestParam("no_offset") Optional<Boolean> noOffset,
    		@RequestParam("page") Optional<Integer> page,
    		@RequestParam("filter_start_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> startDate,
    		@RequestParam("filter_end_date") @DateTimeFormat(pattern = "dd-MM-yyyy") Optional<Date> endDate,
    		@RequestParam("filter_status") Optional<String> filterStatus,
    		HttpServletRequest request
    		)throws Exception {
    	
		QueryParameter param = new QueryParameter();
		
		
		if(startDate.isPresent() && !endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " >= '"+Utils.formatSqlDate(startDate.get())+"'");
    	}else if(!startDate.isPresent() && endDate.isPresent()) {
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " <= '"+Utils.formatSqlDate(endDate.get())+"'");
    	}else if(startDate.isPresent() && endDate.isPresent()) {
    		
    		param.setClause(param.getClause() + " AND " + Purchase.DATETIME + " BETWEEN '"+Utils.formatSqlDate(startDate.get())+"' AND '"+Utils.formatSqlDate(endDate.get())+"'");
    	}
    	
    	
    	if(filterStatus.isPresent() && !filterStatus.get().equals("")) {
    		param.setClause(param.getClause() + " AND (" + Purchase.STATUS + " = '"+filterStatus.get()+"')");
    	}
    	
    	
    	if(limit.isPresent()) {
    		param.setLimit(limit.get());
    	}
    	
    	int pPage = 1;
    	if(page.isPresent()) {
    		pPage = page.get();
    		int offset = (pPage-1)*param.getLimit();
    		param.setOffset(offset);
    		
    		if(noOffset.isPresent()) {
    			param.setOffset(0);
    			param.setLimit(pPage*param.getLimit());
    		}
    	}
    	
    	
		ResponseWrapperList resp = new ResponseWrapperList();
		List<RegionPhoto> data = regionPhotoMapper.getList(param);
		
		resp.setCount(regionPhotoMapper.getCount(param));
		
		resp.setData(data);
		
		if(noOffset.isPresent()) {
			resp.setNextMore(data.size() < resp.getCount());
		}else {
			resp.setNextMore(data.size()+((pPage-1)*param.getLimit()) < resp.getCount());
		}
		
		String qryString = "?page="+(pPage+1);
		if(limit.isPresent()){
			qryString += "&limit="+limit.get();
		}
		resp.setNextPageNumber(pPage+1);
		resp.setNextPage(request.getRequestURL().toString()+qryString);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ResponseWrapper> getById(
			@PathVariable String id,
			@RequestParam("action") Optional<String> action
			) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		RegionPhoto data = regionPhotoMapper.getEntity(id);
		
		if(action.isPresent()) {
			if(action.get().toLowerCase().equals(DELETE)) {
				if(data.isActive()) {
	    			resp.setCode(HttpStatus.BAD_REQUEST.value());
	    			resp.setMessage("Anda tidak dapat menghapus data ini, karena sedang aktif.");
	    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
				}
				resp.setMessage("Apakah anda yakin akan menghapus data '"+data.getId()+"'?");
			}
			
		}
		
		resp.setData(data);
		return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
	}
	
    @RequestMapping(value="/save", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> save(
    		@RequestParam("id") Optional<String> id,
    		@RequestParam("header_id") Optional<String> headerId,
    		@RequestParam("file_id") Optional<String> fileId,
    		@RequestParam("file") MultipartFile files,
    		@RequestParam("active") Optional<Boolean> active,
    		HttpServletRequest request
    		) throws Exception {
    	
    		ResponseWrapper resp = new ResponseWrapper();
    		
    		RegionPhoto data = new RegionPhoto(Utils.getLongNumberID());
    		if(id.isPresent()) {
    			data.setId(id.get());
    			data = regionPhotoMapper.getEntity(id.get());
    		}
    		if(data == null) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Data tidak ditemukan.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    		}
    		
    		if(id.isPresent()) data.setId(id.get());
    		if(headerId.isPresent()) data.setHeaderId(headerId.get());
    		if(fileId.isPresent()) data.setFileId(fileId.get());
    		if(active.isPresent()) data.setActive(active.get());
    
    		
    		boolean pass = true;
//    		if( data.getActivity() == null) {
//    			pass = false;
//    		}else {
//    			if(data.getActivity().equals("")) {
//        			pass = false;
//        		}
//    		}

    		if(!pass) {
    			resp.setCode(HttpStatus.BAD_REQUEST.value());
    			resp.setMessage("Input belum lengkap, harap dilengkapi terlebih dahulu.");
    			return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));	
    		}
    		
    		QueryParameter paramPhoto = new QueryParameter();
    		paramPhoto.setClause(paramPhoto.getClause()+" AND "+RegionPhoto.HEADER_ID+" ='"+data.getHeaderId()+"'");
    		List<RegionPhoto> photo = regionPhotoMapper.getList(paramPhoto);
    		if(id.isPresent()) {
    			regionPhotoMapper.update(data);
    		}else {
    			//update status sebelumnya menjadi non aktif
    			for(RegionPhoto rp : photo) {
					rp.setActive(false);
					regionPhotoMapper.update(rp);
    			}
    			data.setActive(true);
    			
    			//upload
    			File ofile = new File(Utils.getLongNumberID());
    			ofile.setFolder("/");
    			ofile.setSize(files.getSize());
    			ofile.setType(files.getContentType());
    			ofile.setName(files.getOriginalFilename());
    			//data.setPersonAdded(this.extractAccountLogin(request, AccountLoginInfoOL.ACCOUNT));
    			ofile.setTimeAdded(new Date());
    			ofile.setReferenceId(data.getId());
    			
    			String ext = FilenameUtils.getExtension(ofile.getName());
	    		storageService.store(files, ofile.getId() + "."+ext);
	    		
	    		fileMapper.insert(ofile);
	    		
	    		data.setFileId(ofile.getId());
    			regionPhotoMapper.insert(data);	
    		}
    		
    		resp.setMessage("Data telah berhasil disimpan.");

    		
    		/*for(File file: fileMapper.getList(new QueryParameter())) {
				if(file.getId().equals(data.getFileId())) {
	    			data.setList_photo(file);
		    		data.setNameFile(file.getName());
				}
			}*/
    		File file = fileMapper.getEntity(data.getFileId());
    		data.setList_photo(file);
    		data.setNameFile(file.getName());
    		
    		resp.setData(data);
   
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    
   

    @RequestMapping(value="/{id}/{action}", method = RequestMethod.POST, produces = "application/json")
	@Transactional(value=MainApplication.TRANSACTION_MANAGAER, rollbackFor=Exception.class, propagation = Propagation.REQUIRED)
    public ResponseEntity<ResponseWrapper> doAction(
			@PathVariable String id,
			@PathVariable String action
    		) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();

		
		RegionPhoto data = regionPhotoMapper.getEntity(id);
		if(action.equals(DELETE)) {
			regionPhotoMapper.delete(data);
			//
			File file = fileMapper.getEntity(data.getFileId());
			if(file!=null) {
				String ext = FilenameUtils.getExtension(file.getName());
				storageService.delete(file.getId() + "." + ext);
				fileMapper.delete(file);
			}
			//
			resp.setMessage("Data '"+data.getId()+"' telah dihapus.");
		}

		if(action.equals("edit-status")) {
			//remove the ols status
			QueryParameter param = new QueryParameter();
			param.setClause(param.getClause()+" AND "+RegionPhoto.HEADER_ID+" = '"+data.getHeaderId()+"'");
			List<RegionPhoto> listRPhoto = regionPhotoMapper.getList(param);
			for(RegionPhoto rp : listRPhoto) {
				if(rp.getId().equals(data.getId())) {
					rp.setActive(true);
					regionPhotoMapper.update(rp);
				}else {
					rp.setActive(false);
					regionPhotoMapper.update(rp);
				}
				
			}
			//end here
			/*
			regionPhotoMapper.delete(data);
			File file = fileMapper.getEntity(data.getFileId());
			if(file!=null) {
				String ext = FilenameUtils.getExtension(file.getName());
				storageService.delete(file.getId() + "." + ext);
				fileMapper.delete(file);
			}
			*/
			resp.setMessage("Data '"+data.getId()+"' berhasil di update.");
			data.setActive(true);
		}
		

		resp.setData(data);
		
        return new ResponseEntity<ResponseWrapper>(resp, HttpStatus.valueOf(resp.getCode()));
    }
    

}
