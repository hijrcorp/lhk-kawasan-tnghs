package id.co.hijr.ticket.ref;

public enum StatusPurchase {

	WAITING("WAITING", "info", "clock"), 
	VERIFYING("VERIFYING", "primary", "clock"), 
	PAYMENT("PAYMENT", "success", "check-circle"),

	CANCEL("CANCEL", "danger","ban"), 
	DRAFT("DRAFT", "warning", "exclamation-circle"), 
	EXPIRED("EXPIRED", "danger","ban"), 
	DONE("DONE", "success","check-circle"), 
	;

	private String id;
	private String color;
	private String icon;

	private StatusPurchase(final String id, final String color, final String icon) {
		this.id = id;
		this.color = color;
		this.icon = icon;
	}
	public String color() {
        return color;
    }
	public String icon() {
        return icon;
    }
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
