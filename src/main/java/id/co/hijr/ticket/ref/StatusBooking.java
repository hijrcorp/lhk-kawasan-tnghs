package id.co.hijr.ticket.ref;

public enum StatusBooking {

	WAITING("WAITING", "info", "clock"), 
	VERIFYING("VERIFYING", "primary", "clock"), 
	PAYMENT("PAYMENT", "success", "check-circle"),
	PENDING("PENDING", "warning", "exclamation-circle"), 

	ACTIVE("ACTIVE", "primary", "clock"), 
	DONE("DONE", "success", "check-circle"),
	CANCEL("CANCEL", "warning", "exclamation-circle"), 
	;

	private String id;
	private String color;
	private String icon;

	private StatusBooking(final String id, final String color, final String icon) {
		this.id = id;
		this.color = color;
		this.icon = icon;
	}
	public String color() {
        return color;
    }
	public String icon() {
        return icon;
    }
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id;
	}
}
