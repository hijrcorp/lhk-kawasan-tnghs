package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;
import id.co.hijr.ticket.model.PurchaseDetilVisitor;

@Mapper
public interface PurchaseDetilVisitorMapper {
	@Insert("INSERT INTO tbl_purchase_detil_visitor (id_purchase_detil_visitor, header_id_purchase_detil_visitor, responsible_purchase_detil_visitor, id_visitor_identity_purchase_detil_visitor, phone_number_purchase_detil_visitor, email_purchase_detil_visitor, address_purchase_detil_visitor, phone_number_family_purchase_detil_visitor, address_family_purchase_detil_visitor, is_valid_purchase_detil_visitor, status_purchase_detil_visitor, parent_header_id_purchase_detil_visitor) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{responsible:BOOLEAN}, #{idVisitorIdentity:VARCHAR}, #{phoneNumber:VARCHAR}, #{email:VARCHAR}, #{address:VARCHAR}, #{phoneNumberFamily:VARCHAR}, #{addressFamily:VARCHAR}, #{isValid:BOOLEAN}, #{status:VARCHAR}, #{parentHeaderId:VARCHAR})")
	void insert(PurchaseDetilVisitor purchaseDetilVisitor);

	@Update("UPDATE tbl_purchase_detil_visitor SET id_purchase_detil_visitor=#{id:VARCHAR}, header_id_purchase_detil_visitor=#{headerId:VARCHAR}, responsible_purchase_detil_visitor=#{responsible:BOOLEAN}, id_visitor_identity_purchase_detil_visitor=#{idVisitorIdentity:VARCHAR}, phone_number_purchase_detil_visitor=#{phoneNumber:VARCHAR}, email_purchase_detil_visitor=#{email:VARCHAR}, address_purchase_detil_visitor=#{address:VARCHAR}, phone_number_family_purchase_detil_visitor=#{phoneNumberFamily:VARCHAR}, address_family_purchase_detil_visitor=#{addressFamily:VARCHAR}, is_valid_purchase_detil_visitor=#{isValid:BOOLEAN}, status_purchase_detil_visitor=#{status:VARCHAR} WHERE id_purchase_detil_visitor=#{id}")
	void update(PurchaseDetilVisitor purchaseDetilVisitor);

//	@Insert("INSERT INTO tbl_purchase_detil_visitor (id_purchase_detil_visitor, header_id_purchase_detil_visitor, no_identity_purchase_detil_visitor, type_identity_purchase_detil_visitor, full_name_purchase_detil_visitor, phone_number_purchase_detil_visitor, email_purchase_detil_visitor, responsible_purchase_detil_visitor, file_id_purchase_detil_visitor, birthdate_purchase_detil_visitor, gender_purchase_detil_visitor, phone_number_family_purchase_detil_visitor, address_family_purchase_detil_visitor, is_valid_purchase_detil_visitor, address_purchase_detil_visitor) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{noIdentity:VARCHAR}, #{typeIdentity:VARCHAR}, #{fullName:VARCHAR}, #{phoneNumber:VARCHAR}, #{email:VARCHAR}, #{responsible:BOOLEAN}, #{fileId:VARCHAR}, #{birthdate:DATE}, #{gender:VARCHAR}, #{phoneNumberFamily:VARCHAR}, #{addressFamily:VARCHAR}, #{isValid:BOOLEAN}, #{address:VARCHAR})")
//	void insert(PurchaseDetilVisitor purchaseDetilVisitor);
//
//	@Update("UPDATE tbl_purchase_detil_visitor SET id_purchase_detil_visitor=#{id:VARCHAR}, header_id_purchase_detil_visitor=#{headerId:VARCHAR}, no_identity_purchase_detil_visitor=#{noIdentity:VARCHAR}, type_identity_purchase_detil_visitor=#{typeIdentity:VARCHAR}, full_name_purchase_detil_visitor=#{fullName:VARCHAR}, phone_number_purchase_detil_visitor=#{phoneNumber:VARCHAR}, email_purchase_detil_visitor=#{email:VARCHAR}, responsible_purchase_detil_visitor=#{responsible:BOOLEAN}, file_id_purchase_detil_visitor=#{fileId:VARCHAR}, birthdate_purchase_detil_visitor=#{birthdate:DATE}, gender_purchase_detil_visitor=#{gender:VARCHAR}, phone_number_family_purchase_detil_visitor=#{phoneNumberFamily:VARCHAR}, address_family_purchase_detil_visitor=#{addressFamily:VARCHAR}, is_valid_purchase_detil_visitor=#{isValid:BOOLEAN}, address_purchase_detil_visitor=#{address:VARCHAR} WHERE id_purchase_detil_visitor=#{id}")
//	void update(PurchaseDetilVisitor purchaseDetilVisitor);

	@Delete("DELETE FROM tbl_purchase_detil_visitor WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_visitor WHERE id_purchase_detil_visitor=#{id}")
	void delete(PurchaseDetilVisitor purchaseDetilVisitor);

	List<PurchaseDetilVisitor> getList(QueryParameter param);

	PurchaseDetilVisitor getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

	//costum
	List<PurchaseDetilVisitor> getEntityHeader(String id);

	@Update("UPDATE tbl_purchase_detil_visitor SET status_purchase_detil_visitor=null WHERE ${clause}")
	void updateStatusBatch(QueryParameter param);
}
