package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.TypeRegion;

@Mapper
public interface TypeRegionMapper {

	@Insert("INSERT INTO tbl_type_region (id_type_region, name_type_region) VALUES (#{id:VARCHAR}, #{name:VARCHAR})")
	void insert(TypeRegion typeRegion);

	@Update("UPDATE tbl_type_region SET id_type_region=#{id:VARCHAR}, name_type_region=#{name:VARCHAR} WHERE id_type_region=#{id}")
	void update(TypeRegion typeRegion);

	@Delete("DELETE FROM tbl_type_region WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_type_region WHERE id_type_region=#{id}")
	void delete(TypeRegion typeRegion);

	List<TypeRegion> getList(QueryParameter param);

	TypeRegion getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
