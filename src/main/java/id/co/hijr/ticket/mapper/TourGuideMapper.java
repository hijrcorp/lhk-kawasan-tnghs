package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.TourGuide;
import id.co.hijr.ticket.model.Luggage;

@Mapper
public interface TourGuideMapper {

	@Insert("INSERT INTO tbl_tour_guide (id_tour_guide, full_name_tour_guide, gender_tour_guide, photo_tour_guide, description_tour_guide, phone_number_tour_guide, email_tour_guide, cost_per_day_tour_guide, organization_id_tour_guide) VALUES (#{id:VARCHAR}, #{fullName:VARCHAR}, #{gender:VARCHAR}, #{photo:VARCHAR}, #{description:VARCHAR}, #{phoneNumber:VARCHAR}, #{email:VARCHAR}, #{costPerDay:NUMERIC}, #{organizationId:VARCHAR})")
	void insert(TourGuide tourGuide);

	@Update("UPDATE tbl_tour_guide SET id_tour_guide=#{id:VARCHAR}, full_name_tour_guide=#{fullName:VARCHAR}, gender_tour_guide=#{gender:VARCHAR}, photo_tour_guide=#{photo:VARCHAR}, description_tour_guide=#{description:VARCHAR}, phone_number_tour_guide=#{phoneNumber:VARCHAR}, email_tour_guide=#{email:VARCHAR}, cost_per_day_tour_guide=#{costPerDay:NUMERIC}, organization_id_tour_guide=#{organizationId:VARCHAR} WHERE id_tour_guide=#{id}")
	void update(TourGuide tourGuide);

	@Delete("DELETE FROM tbl_tour_guide WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_tour_guide WHERE id_tour_guide=#{id}")
	void delete(TourGuide tourGuide);

	List<TourGuide> getList(QueryParameter param);

	TourGuide getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	

	//costume
	List<Luggage> getListLuggage(QueryParameter param);
}
