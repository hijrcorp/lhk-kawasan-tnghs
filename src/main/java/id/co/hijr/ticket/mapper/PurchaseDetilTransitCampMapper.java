package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.PurchaseDetilTransitCamp;

@Mapper
public interface PurchaseDetilTransitCampMapper {

	@Insert("INSERT INTO tbl_purchase_detil_transit_camp (id_purchase_detil_transit_camp, header_id_purchase_detil_transit_camp, id_transit_camp_purchase_detil_transit_camp, name_transit_camp_purchase_detil_transit_camp, report_time_camp_purchase_detil_transit_camp, type_camp_purchase_detil_transit_camp, count_camp_purchase_detil_transit_camp, code_unique_purchase_detil_transit_camp, status_purchase_detil_transit_camp) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{idTransitCamp:VARCHAR}, #{nameTransitCamp:VARCHAR}, #{reportTimeCamp:VARCHAR}, #{typeCamp:VARCHAR}, #{countCamp:NUMERIC}, #{codeUnique:VARCHAR}, #{status:VARCHAR})")
	void insert(PurchaseDetilTransitCamp purchaseDetilTransitCamp);

	@Update("UPDATE tbl_purchase_detil_transit_camp SET id_purchase_detil_transit_camp=#{id:VARCHAR}, header_id_purchase_detil_transit_camp=#{headerId:VARCHAR}, id_transit_camp_purchase_detil_transit_camp=#{idTransitCamp:VARCHAR}, name_transit_camp_purchase_detil_transit_camp=#{nameTransitCamp:VARCHAR}, report_time_camp_purchase_detil_transit_camp=#{reportTimeCamp:VARCHAR}, type_camp_purchase_detil_transit_camp=#{typeCamp:VARCHAR}, count_camp_purchase_detil_transit_camp=#{countCamp:NUMERIC}, code_unique_purchase_detil_transit_camp=#{codeUnique:VARCHAR}, status_purchase_detil_transit_camp=#{status:VARCHAR} WHERE id_purchase_detil_transit_camp=#{id}")
	void update(PurchaseDetilTransitCamp purchaseDetilTransitCamp);

	@Delete("DELETE FROM tbl_purchase_detil_transit_camp WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_transit_camp WHERE id_purchase_detil_transit_camp=#{id}")
	void delete(PurchaseDetilTransitCamp purchaseDetilTransitCamp);

	List<PurchaseDetilTransitCamp> getList(QueryParameter param);

	PurchaseDetilTransitCamp getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();

	//xostume
	@Update("UPDATE tbl_purchase_detil_transit_camp SET status_purchase_detil_transit_camp=null WHERE ${clause}")
	void updateStatusBatch(QueryParameter param);
}
