package id.co.hijr.ticket.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseChart;

@Mapper
public interface PurchaseMapper {

//	@Insert("INSERT INTO tbl_purchase (id_purchase, region_id_purchase, region_name_purchase, start_date_purchase, end_date_purchase, datetime_purchase, account_id_purchase, count_ticket_purchase, amount_ticket_purchase, amount_total_ticket_purchase, number_unique_purchase, status_purchase, status_booking_purchase, code_booking_purchase, status_boarding_purchase) VALUES (#{id:VARCHAR}, #{regionId:VARCHAR}, #{regionName:VARCHAR}, #{startDate:TIMESTAMP}, #{endDate:TIMESTAMP}, #{datetime:TIMESTAMP}, #{accountId:VARCHAR}, #{countTicket:NUMERIC}, #{amountTicket:NUMERIC}, #{amountTotalTicket:NUMERIC}, #{numberUnique:NUMERIC}, #{status:VARCHAR}, #{statusBooking:VARCHAR}, #{codeBooking:VARCHAR}, #{statusBoarding:VARCHAR})")
//	void insert(Purchase purchase);
//
//	@Update("UPDATE tbl_purchase SET id_purchase=#{id:VARCHAR}, region_id_purchase=#{regionId:VARCHAR}, region_name_purchase=#{regionName:VARCHAR}, start_date_purchase=#{startDate:TIMESTAMP}, end_date_purchase=#{endDate:TIMESTAMP}, datetime_purchase=#{datetime:TIMESTAMP}, account_id_purchase=#{accountId:VARCHAR}, count_ticket_purchase=#{countTicket:NUMERIC}, amount_ticket_purchase=#{amountTicket:NUMERIC}, amount_total_ticket_purchase=#{amountTotalTicket:NUMERIC}, number_unique_purchase=#{numberUnique:NUMERIC}, status_purchase=#{status:VARCHAR}, status_booking_purchase=#{statusBooking:VARCHAR}, code_booking_purchase=#{codeBooking:VARCHAR}, status_boarding_purchase=#{statusBoarding:VARCHAR} WHERE id_purchase=#{id}")
//	void update(Purchase purchase);
	
	@Insert("INSERT INTO tbl_purchase (id_purchase, parent_id_purchase, region_id_purchase, region_name_purchase, region_end_id_purchase, region_end_name_purchase, start_date_purchase, end_date_purchase, datetime_purchase, account_id_purchase, count_ticket_wna_purchase, count_ticket_wni_purchase, amount_ticket_purchase, amount_total_ticket_purchase, number_unique_purchase, status_purchase, status_booking_purchase, code_booking_purchase, status_boarding_purchase, status_reschedule_purchase) VALUES (#{id:VARCHAR}, #{parentId:VARCHAR}, #{regionId:VARCHAR}, #{regionName:VARCHAR}, #{regionEndId:VARCHAR}, #{regionEndName:VARCHAR}, #{startDate:TIMESTAMP}, #{endDate:TIMESTAMP}, #{datetime:TIMESTAMP}, #{accountId:VARCHAR}, #{countTicketWna:NUMERIC}, #{countTicketWni:NUMERIC}, #{amountTicket:NUMERIC}, #{amountTotalTicket:NUMERIC}, #{numberUnique:NUMERIC}, #{status:VARCHAR}, #{statusBooking:VARCHAR}, #{codeBooking:VARCHAR}, #{statusBoarding:VARCHAR}, #{statusReschedule:VARCHAR})")
	void insert(Purchase purchase);

	@Update("UPDATE tbl_purchase SET id_purchase=#{id:VARCHAR}, region_id_purchase=#{regionId:VARCHAR}, region_name_purchase=#{regionName:VARCHAR}, region_end_id_purchase=#{regionEndId:VARCHAR}, region_end_name_purchase=#{regionEndName:VARCHAR}, start_date_purchase=#{startDate:TIMESTAMP}, end_date_purchase=#{endDate:TIMESTAMP}, datetime_purchase=#{datetime:TIMESTAMP}, account_id_purchase=#{accountId:VARCHAR}, count_ticket_wna_purchase=#{countTicketWna:NUMERIC}, count_ticket_wni_purchase=#{countTicketWni:NUMERIC}, amount_ticket_purchase=#{amountTicket:NUMERIC}, amount_total_ticket_purchase=#{amountTotalTicket:NUMERIC}, number_unique_purchase=#{numberUnique:NUMERIC}, status_purchase=#{status:VARCHAR}, status_booking_purchase=#{statusBooking:VARCHAR}, code_booking_purchase=#{codeBooking:VARCHAR}, status_boarding_purchase=#{statusBoarding:VARCHAR}, status_reschedule_purchase=#{statusReschedule:VARCHAR}, disclaimer_purchase=#{disclaimer:NUMERIC}, amount_ticket_wna_purchase=#{amountTicketWna:NUMERIC}, amount_ticket_wni_purchase=#{amountTicketWni:NUMERIC} WHERE id_purchase=#{id}")
	void update(Purchase purchase);

	@Delete("DELETE FROM tbl_purchase WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase WHERE id_purchase=#{id}")
	void delete(Purchase purchase);

	List<Purchase> getList(QueryParameter param);

	Purchase getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/***************************/

	List<Purchase> getListExtended(QueryParameter param);
	
	List<Purchase> getListBoarding(QueryParameter param);
	

	/*@Select("select region_name_purchase, count(*) count_group, sum(count_ticket_wni_purchase)+sum(count_ticket_wna_purchase) count_ticket, sum(amount_ticket_purchase) amount_ticket, sum(amount_total_ticket_purchase) from(\n"
			+ "	select * from tbl_purchase  WHERE ${clause}\n"
			+ ") x\n"
			+ "group by region_name_purchase")
	List<Map> getRekapHarian(QueryParameter clause);*/
	

	@Select("select a.name_region, IFNULL(count_group,0) count_group, IFNULL(count_ticket,0) count_ticket, IFNULL(amount_ticket,0) amount_ticket from tbl_region a\n"
			+ "left join(\n"
			+ "	select region_name_purchase, count(*) count_group, sum(count_ticket_wni_purchase)+sum(count_ticket_wna_purchase) count_ticket, \n"
			+ "	sum(amount_ticket_purchase) amount_ticket, 	sum(amount_total_ticket_purchase) \n"
			+ "	from(\n"
			+ "		select * from (select DATE(start_date_purchase) date_purchase, a.* from tbl_purchase a) a WHERE ${clause} LIMIT 10000000\n"
			+ "	) x\n"
			+ "	group by region_name_purchase\n"
			+ ") b on b.region_name_purchase = a.name_region "
			+ "where satker_id_region=26 order by name_region")
	List<Map> getRekapHarian(QueryParameter clause);
	

	/*@Select("select month_year, a.name_region, IFNULL(count_group,0) count_group, IFNULL(count_ticket,0) count_ticket, IFNULL(amount_ticket,0) amount_ticket from tbl_region a\n"
			+ "left join(\n"
			+ "	select month_year, region_name_purchase, count(*) count_group, sum(count_ticket_wni_purchase)+sum(count_ticket_wna_purchase) count_ticket, \n"
			+ "	sum(amount_ticket_purchase) amount_ticket, 	sum(amount_total_ticket_purchase)\n"
			+ "	from(\n"
			+ "		select * from (select YEAR(start_date_purchase) month_year, MONTH(start_date_purchase) month_purchase, a.* from tbl_purchase a) a\n"
			+ "		where ${clause}\n"
			+ "	) x\n"
			+ "	group by region_name_purchase, month_year\n"
			+ ") b on b.region_name_purchase = a.name_region\n"
			+ "where satker_id_region=26 order by name_region")
	List<Map> getRekapTahunan(QueryParameter clause);*/
	List<PurchaseChart> getRekapTahunan(QueryParameter param);

	/*@Select("select month_purchase, a.name_region, IFNULL(count_group,0) count_group, IFNULL(count_ticket,0) count_ticket, IFNULL(amount_ticket,0) amount_ticket from tbl_region a\n"
			+ "left join(\n"
			+ "	select month_purchase, region_name_purchase, count(*) count_group, sum(count_ticket_wni_purchase)+sum(count_ticket_wna_purchase) count_ticket, \n"
			+ "	sum(amount_ticket_purchase) amount_ticket, 	sum(amount_total_ticket_purchase)\n"
			+ "	from(\n"
			+ "		select * from (select YEAR(start_date_purchase) year_purchase, MONTH(start_date_purchase) month_purchase, a.* from tbl_purchase a) a\n"
			+ "		where ${clause}\n"
			+ "	) x\n"
			+ "	group by region_name_purchase, month_purchase\n"
			+ ") b on b.region_name_purchase = a.name_region\n"
			+ "where satker_id_region=26 order by name_region")
	List<Map> getRekapBulanan(QueryParameter clause);*/
	
	List<PurchaseChart> getRekapBulanan(QueryParameter param);
}
