package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.MasterPayment;

@Mapper
public interface MasterPaymentMapper {

	@Insert("INSERT INTO tbl_master_payment (id_master_payment, code_master_payment, name_master_payment, price_master_payment, status_master_payment) VALUES (#{id:VARCHAR}, #{code:VARCHAR}, #{name:VARCHAR}, #{price:NUMERIC}, #{status})")
	void insert(MasterPayment masterPayment);

	@Update("UPDATE tbl_master_payment SET id_master_payment=#{id:VARCHAR}, code_master_payment=#{code:VARCHAR}, name_master_payment=#{name:VARCHAR}, price_master_payment=#{price:NUMERIC}, status_master_payment=#{status} WHERE id_master_payment=#{id}")
	void update(MasterPayment masterPayment);

	@Delete("DELETE FROM tbl_master_payment WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_master_payment WHERE id_master_payment=#{id}")
	void delete(MasterPayment masterPayment);

	List<MasterPayment> getList(QueryParameter param);

	MasterPayment getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
