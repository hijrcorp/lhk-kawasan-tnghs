package id.co.hijr.ticket.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Region;

public interface RegionMapper {
	@Insert("INSERT INTO tbl_region (id_region, name_region, type_region, description_region, location_region, location_desc_region, organization_id_region, refund_region, kuota_region, max_day_region, photo_region, satker_id_region) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{type:VARCHAR}, #{description:VARCHAR}, #{location:VARCHAR}, #{locationDesc:VARCHAR}, #{organizationId:VARCHAR}, #{refund:BOOLEAN}, #{kuota:NUMERIC}, #{maxDay:NUMERIC}, #{photo:VARCHAR}, 26)")
	void insert(Region region);

	@Update("UPDATE tbl_region SET id_region=#{id:VARCHAR}, name_region=#{name:VARCHAR}, type_region=#{type:VARCHAR}, description_region=#{description:VARCHAR}, location_region=#{location:VARCHAR}, location_desc_region=#{locationDesc:VARCHAR}, organization_id_region=#{organizationId:VARCHAR}, refund_region=#{refund:BOOLEAN}, kuota_region=#{kuota:NUMERIC}, max_day_region=#{maxDay:NUMERIC}, photo_region=#{photo:VARCHAR} WHERE id_region=#{id}")
	void update(Region region);

	@Delete("DELETE FROM tbl_region WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_region WHERE id_region=#{id}")
	void delete(Region region);

	List<Region> getList(QueryParameter param);

	Region getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	//for public only
	List<Region> getListPublic(Map<String, Object> params);
}
