package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.PurchaseDetilPayment;

@Mapper
public interface PurchaseDetilPaymentMapper {

	@Insert("INSERT INTO tbl_purchase_detil_payment (id_purchase_detil_payment, id_header_purchase_detil_payment, code_purchase_detil_payment, name_purchase_detil_payment, price_purchase_detil_payment, status_purchase_detil_payment) VALUES (#{id:VARCHAR}, #{idHeader:VARCHAR}, #{code:VARCHAR}, #{name:VARCHAR}, #{price:NUMERIC}, #{status})")
	void insert(PurchaseDetilPayment purchaseDetilPayment);

	@Update("UPDATE tbl_purchase_detil_payment SET id_purchase_detil_payment=#{id:VARCHAR}, id_header_purchase_detil_payment=#{idHeader:VARCHAR}, code_purchase_detil_payment=#{code:VARCHAR}, name_purchase_detil_payment=#{name:VARCHAR}, price_purchase_detil_payment=#{price:NUMERIC}, status_purchase_detil_payment=#{status} WHERE id_purchase_detil_payment=#{id}")
	void update(PurchaseDetilPayment purchaseDetilPayment);

	@Delete("DELETE FROM tbl_purchase_detil_payment WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_payment WHERE id_purchase_detil_payment=#{id}")
	void delete(PurchaseDetilPayment purchaseDetilPayment);

	List<PurchaseDetilPayment> getList(QueryParameter param);

	PurchaseDetilPayment getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
