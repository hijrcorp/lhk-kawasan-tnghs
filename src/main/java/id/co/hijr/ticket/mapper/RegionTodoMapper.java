package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.RegionTodo;

public interface RegionTodoMapper {
	@Insert("INSERT INTO tbl_region_todo (id_region_todo, header_id_region_todo, day_region_todo, time_region_todo, activity_region_todo, parent_region_todo) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{day:NUMERIC}, #{time:TIMESTAMP}, #{activity:VARCHAR}, #{parent:VARCHAR})")
	void insert(RegionTodo regionTodo);

	@Update("UPDATE tbl_region_todo SET id_region_todo=#{id:VARCHAR}, header_id_region_todo=#{headerId:VARCHAR}, day_region_todo=#{day:NUMERIC}, time_region_todo=#{time:TIMESTAMP}, activity_region_todo=#{activity:VARCHAR}, parent_region_todo=#{parent:VARCHAR} WHERE id_region_todo=#{id}")
	void update(RegionTodo regionTodo);

	@Delete("DELETE FROM tbl_region_todo WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_region_todo WHERE id_region_todo=#{id}")
	void delete(RegionTodo regionTodo);

	List<RegionTodo> getList(QueryParameter param);

	RegionTodo getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
