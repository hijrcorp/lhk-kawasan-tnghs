package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilTourGuide;

@Mapper
public interface PurchaseDetilTourGuideMapper {

	@Insert("INSERT INTO tbl_purchase_detil_tour_guide (id_purchase_detil_tour_guide, header_id_purchase_detil_tour_guide, full_name_purchase_detil_tour_guide, gender_purchase_detil_tour_guide, photo_purchase_detil_tour_guide, description_purchase_detil_tour_guide, phone_number_purchase_detil_tour_guide, email_purchase_detil_tour_guide, cost_per_day_purchase_detil_tour_guide, organization_purchase_detil_tour_guide) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{fullName:VARCHAR}, #{gender:VARCHAR}, #{photo:VARCHAR}, #{description:VARCHAR}, #{phoneNumber:VARCHAR}, #{email:VARCHAR}, #{costPerDay:NUMERIC}, #{organization:VARCHAR})")
	void insert(PurchaseDetilTourGuide purchaseDetilTourGuide);

	@Update("UPDATE tbl_purchase_detil_tour_guide SET id_purchase_detil_tour_guide=#{id:VARCHAR}, header_id_purchase_detil_tour_guide=#{headerId:VARCHAR}, full_name_purchase_detil_tour_guide=#{fullName:VARCHAR}, gender_purchase_detil_tour_guide=#{gender:VARCHAR}, photo_purchase_detil_tour_guide=#{photo:VARCHAR}, description_purchase_detil_tour_guide=#{description:VARCHAR}, phone_number_purchase_detil_tour_guide=#{phoneNumber:VARCHAR}, email_purchase_detil_tour_guide=#{email:VARCHAR}, cost_per_day_purchase_detil_tour_guide=#{costPerDay:NUMERIC}, organization_purchase_detil_tour_guide=#{organization:VARCHAR} WHERE id_purchase_detil_tour_guide=#{id}")
	void update(PurchaseDetilTourGuide purchaseDetilTourGuide);

	@Delete("DELETE FROM tbl_purchase_detil_tour_guide WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_tour_guide WHERE id_purchase_detil_tour_guide=#{id}")
	void delete(PurchaseDetilTourGuide purchaseDetilTourGuide);

	List<PurchaseDetilTourGuide> getList(QueryParameter param);

	PurchaseDetilTourGuide getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
