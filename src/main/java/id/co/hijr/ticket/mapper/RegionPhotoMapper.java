package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.RegionPhoto;

public interface RegionPhotoMapper {
	@Insert("INSERT INTO tbl_region_photo (id_region_photo, header_id_region_photo, file_id_region_photo, active_region_photo) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{fileId:VARCHAR}, #{active:BOOLEAN})")
	void insert(RegionPhoto regionPhoto);

	@Update("UPDATE tbl_region_photo SET id_region_photo=#{id:VARCHAR}, header_id_region_photo=#{headerId:VARCHAR}, file_id_region_photo=#{fileId:VARCHAR}, active_region_photo=#{active:BOOLEAN} WHERE id_region_photo=#{id}")
	void update(RegionPhoto regionPhoto);

	@Delete("DELETE FROM tbl_region_photo WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_region_photo WHERE id_region_photo=#{id}")
	void delete(RegionPhoto regionPhoto);

	List<RegionPhoto> getList(QueryParameter param);

	RegionPhoto getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
