package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Purchase;
import id.co.hijr.ticket.model.PurchaseDetilVehicle;

@Mapper
public interface PurchaseDetilVehicleMapper {

	@Insert("INSERT INTO tbl_purchase_detil_vehicle (id_purchase_detil_vehicle, header_id_purchase_detil_vehicle, name_purchase_detil_vehicle, type_purchase_detil_vehicle, price_purchase_detil_vehicle, qty_purchase_detil_vehicle) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{name:VARCHAR}, #{type:VARCHAR}, #{price:NUMERIC}, #{qty:NUMERIC})")
	void insert(PurchaseDetilVehicle purchaseDetilVehicle);

	@Update("UPDATE tbl_purchase_detil_vehicle SET id_purchase_detil_vehicle=#{id:VARCHAR}, header_id_purchase_detil_vehicle=#{headerId:VARCHAR}, name_purchase_detil_vehicle=#{name:VARCHAR}, type_purchase_detil_vehicle=#{type:VARCHAR}, price_purchase_detil_vehicle=#{price:NUMERIC}, qty_purchase_detil_vehicle=#{qty:NUMERIC} WHERE id_purchase_detil_vehicle=#{id}")
	void update(PurchaseDetilVehicle purchaseDetilVehicle);

	@Delete("DELETE FROM tbl_purchase_detil_vehicle WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_vehicle WHERE id_purchase_detil_vehicle=#{id}")
	void delete(PurchaseDetilVehicle purchaseDetilVehicle);

	List<PurchaseDetilVehicle> getList(QueryParameter param);

	PurchaseDetilVehicle getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
