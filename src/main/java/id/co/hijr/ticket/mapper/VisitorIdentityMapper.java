package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.VisitorIdentity;

@Mapper
public interface VisitorIdentityMapper {

	@Insert("INSERT INTO tbl_visitor_identity (id_visitor_identity, id_header_visitor_identity, no_identity_visitor_identity, type_identity_visitor_identity, full_name_visitor_identity, gender_visitor_identity, birthdate_visitor_identity, address_visitor_identity, id_file_visitor_identity, phone_number_visitor_identity, email_visitor_identity, phone_number_family_visitor_identity, address_family_visitor_identity, status_valid_visitor_identity, date_added_visitor_identity, date_banned_visitor_identity, days_banned_visitor_identity) VALUES (#{id:VARCHAR}, #{idHeader:VARCHAR}, #{noIdentity:VARCHAR}, #{typeIdentity:VARCHAR}, #{fullName:VARCHAR}, #{gender:VARCHAR}, #{birthdate:DATE}, #{address:VARCHAR}, #{idFile:VARCHAR}, #{phoneNumber:VARCHAR}, #{email:VARCHAR}, #{phoneNumberFamily:VARCHAR}, #{addressFamily:VARCHAR}, #{statusValid:VARCHAR}, now(),  #{dateBanned:DATE}, #{daysBanned:NUMERIC})")
	void insert(VisitorIdentity visitorIdentity);

	@Update("UPDATE tbl_visitor_identity SET id_visitor_identity=#{id:VARCHAR}, id_header_visitor_identity=#{idHeader:VARCHAR}, no_identity_visitor_identity=#{noIdentity:VARCHAR}, type_identity_visitor_identity=#{typeIdentity:VARCHAR}, full_name_visitor_identity=#{fullName:VARCHAR}, gender_visitor_identity=#{gender:VARCHAR}, birthdate_visitor_identity=#{birthdate:DATE}, address_visitor_identity=#{address:VARCHAR}, id_file_visitor_identity=#{idFile:VARCHAR}, phone_number_visitor_identity=#{phoneNumber:VARCHAR}, email_visitor_identity=#{email:VARCHAR}, phone_number_family_visitor_identity=#{phoneNumberFamily:VARCHAR}, address_family_visitor_identity=#{addressFamily:VARCHAR}, status_valid_visitor_identity=#{statusValid:VARCHAR}, date_banned_visitor_identity=#{dateBanned:DATE}, days_banned_visitor_identity=#{daysBanned:NUMERIC} WHERE id_visitor_identity=#{id}")
	void update(VisitorIdentity visitorIdentity);

	@Delete("DELETE FROM tbl_visitor_identity WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_visitor_identity WHERE id_visitor_identity=#{id}")
	void delete(VisitorIdentity visitorIdentity);

	List<VisitorIdentity> getList(QueryParameter param);

	VisitorIdentity getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
