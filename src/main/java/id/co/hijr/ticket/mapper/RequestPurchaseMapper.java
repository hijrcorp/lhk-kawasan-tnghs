package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.RequestPurchase;

@Mapper
public interface RequestPurchaseMapper {
	
	@Insert("INSERT INTO tbl_request_purchase (id_request_purchase, access_token_request_purchase, payment_code_request_purchase, id_header_purchase_request_purchase, submit_date_request_purchase) VALUES (#{id:VARCHAR}, #{accessToken:VARCHAR}, #{paymentCode:VARCHAR}, #{idHeaderPurchase:VARCHAR}, #{submitDate:TIMESTAMP})")
	void insert(RequestPurchase requestPurchase);

	@Update("UPDATE tbl_request_purchase SET id_request_purchase=#{id:VARCHAR}, access_token_request_purchase=#{accessToken:VARCHAR}, payment_code_request_purchase=#{paymentCode:VARCHAR}, id_header_purchase_request_purchase=#{idHeaderPurchase:VARCHAR}, submit_date_request_purchase=#{submitDate:TIMESTAMP} WHERE id_request_purchase=#{id}")
	void update(RequestPurchase requestPurchase);

	@Delete("DELETE FROM tbl_request_purchase WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_request_purchase WHERE id_request_purchase=#{id}")
	void delete(RequestPurchase requestPurchase);

	List<RequestPurchase> getList(QueryParameter param);

	RequestPurchase getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	/***************************/

	//List<Purchase> getListExtended(QueryParameter param);
}
