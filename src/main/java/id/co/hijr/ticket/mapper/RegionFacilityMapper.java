package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.RegionFacility;

public interface RegionFacilityMapper {
	@Insert("INSERT INTO tbl_region_facility (id_region_facility, header_id_region_facility, code_region_facility, item_region_facility, price_region_facility, include_region_facility) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{code:VARCHAR}, #{item:VARCHAR}, #{price:NUMERIC}, #{include:VARCHAR})")
	void insert(RegionFacility regionFacility);

	@Update("UPDATE tbl_region_facility SET id_region_facility=#{id:VARCHAR}, header_id_region_facility=#{headerId:VARCHAR}, code_region_facility=#{code:VARCHAR}, item_region_facility=#{item:VARCHAR}, price_region_facility=#{price:NUMERIC}, include_region_facility=#{include:VARCHAR} WHERE id_region_facility=#{id}")
	void update(RegionFacility regionFacility);

	@Delete("DELETE FROM tbl_region_facility WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_region_facility WHERE id_region_facility=#{id}")
	void delete(RegionFacility regionFacility);

	List<RegionFacility> getList(QueryParameter param);

	RegionFacility getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
