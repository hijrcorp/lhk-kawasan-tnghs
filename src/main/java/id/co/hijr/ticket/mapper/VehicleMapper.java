package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Vehicle;

@Mapper
public interface VehicleMapper {

	@Insert("INSERT INTO tbl_vehicle (id_vehicle, name_vehicle, type_id_vehicle, cost_vehicle, photo_vehicle, seat_capacity_vehicle, suitcase_vehicle, description_vehicle, provided_vehicle, organization_id_vehicle) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{typeId:VARCHAR}, #{cost:NUMERIC}, #{photo:VARCHAR}, #{seatCapacity:NUMERIC}, #{suitcase:NUMERIC}, #{description:VARCHAR}, #{provided:VARCHAR}, #{organizationId:VARCHAR})")
	void insert(Vehicle vehicle);

	@Update("UPDATE tbl_vehicle SET id_vehicle=#{id:VARCHAR}, name_vehicle=#{name:VARCHAR}, type_id_vehicle=#{typeId:VARCHAR}, cost_vehicle=#{cost:NUMERIC}, photo_vehicle=#{photo:VARCHAR}, seat_capacity_vehicle=#{seatCapacity:NUMERIC}, suitcase_vehicle=#{suitcase:NUMERIC}, description_vehicle=#{description:VARCHAR}, provided_vehicle=#{provided:VARCHAR}, organization_id_vehicle=#{organizationId:VARCHAR} WHERE id_vehicle=#{id}")
	void update(Vehicle vehicle);

	@Delete("DELETE FROM tbl_vehicle WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_vehicle WHERE id_vehicle=#{id}")
	void delete(Vehicle vehicle);

	List<Vehicle> getList(QueryParameter param);

	Vehicle getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
