package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.PurchaseDetilLuggage;

@Mapper
public interface PurchaseDetilLuggageMapper {

	@Insert("INSERT INTO tbl_purchase_detil_luggage (id_purchase_detil_luggage, header_id_purchase_detil_luggage, type_luggage_purchase_detil_luggage, name_luggage_purchase_detil_luggage, qty_luggage_purchase_detil_luggage, desc_luggage_purchase_detil_luggage, qty_actual_luggage_purchase_detil_luggage, is_valid_luggage_purchase_detil_luggage, status_purchase_detil_luggage) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{typeLuggage:VARCHAR}, #{nameLuggage:VARCHAR}, #{qtyLuggage:NUMERIC}, #{descLuggage:VARCHAR}, #{qtyActualLuggage:NUMERIC}, #{isValidLuggage:BOOLEAN}, #{status:VARCHAR})")
	void insert(PurchaseDetilLuggage purchaseDetilLuggage);

	@Update("UPDATE tbl_purchase_detil_luggage SET id_purchase_detil_luggage=#{id:VARCHAR}, header_id_purchase_detil_luggage=#{headerId:VARCHAR}, type_luggage_purchase_detil_luggage=#{typeLuggage:VARCHAR}, name_luggage_purchase_detil_luggage=#{nameLuggage:VARCHAR}, qty_luggage_purchase_detil_luggage=#{qtyLuggage:NUMERIC}, desc_luggage_purchase_detil_luggage=#{descLuggage:VARCHAR}, qty_actual_luggage_purchase_detil_luggage=#{qtyActualLuggage:NUMERIC}, is_valid_luggage_purchase_detil_luggage=#{isValidLuggage:BOOLEAN}, status_purchase_detil_luggage=#{status:VARCHAR} WHERE id_purchase_detil_luggage=#{id}")
	void update(PurchaseDetilLuggage purchaseDetilLuggage);

	@Delete("DELETE FROM tbl_purchase_detil_luggage WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_luggage WHERE id_purchase_detil_luggage=#{id}")
	void delete(PurchaseDetilLuggage purchaseDetilLuggage);

	List<PurchaseDetilLuggage> getList(QueryParameter param);

	PurchaseDetilLuggage getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	//
	@Update("UPDATE tbl_purchase_detil_luggage SET status_purchase_detil_luggage=null WHERE ${clause}")
	void updateStatusBatch(QueryParameter param);
}
