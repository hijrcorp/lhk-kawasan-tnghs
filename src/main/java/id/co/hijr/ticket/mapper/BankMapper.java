package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.Bank;

@Mapper
public interface BankMapper {

	@Insert("INSERT INTO tbl_bank (id_bank, name_bank, code_bank, type_bank, account_number_bank, account_holder_name_bank) VALUES (#{id:VARCHAR}, #{name:VARCHAR}, #{code:VARCHAR}, #{type:VARCHAR}, #{accountNumber:VARCHAR}, #{accountHolderName:VARCHAR})")
	void insert(Bank bank);

	@Update("UPDATE tbl_bank SET id_bank=#{id:VARCHAR}, name_bank=#{name:VARCHAR}, code_bank=#{code:VARCHAR}, type_bank=#{type:VARCHAR}, account_number_bank=#{accountNumber:VARCHAR}, account_holder_name_bank=#{accountHolderName:VARCHAR} WHERE id_bank=#{id}")
	void update(Bank bank);

	@Delete("DELETE FROM tbl_bank WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_bank WHERE id_bank=#{id}")
	void delete(Bank bank);

	List<Bank> getList(QueryParameter param);

	Bank getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
