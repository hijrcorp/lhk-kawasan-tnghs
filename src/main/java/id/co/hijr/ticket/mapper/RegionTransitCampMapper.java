package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.RegionTransitCamp;

public interface RegionTransitCampMapper {
	@Insert("INSERT INTO tbl_region_transit_camp (id_region_transit_camp, header_id_region_transit_camp, code_region_transit_camp, id_master_transit_camp_region_transit_camp, start_register_region_transit_camp, end_register_region_transit_camp, extimate_arrived_region_transit_camp, number_of_large_tents_region_transit_camp, number_of_small_tents_region_transit_camp, photo_region_transit_camp) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{code:VARCHAR}, #{idMasterTransitCamp:VARCHAR}, #{startRegister:TIMESTAMP}, #{endRegister:TIMESTAMP}, #{extimateArrived:TIMESTAMP}, #{numberOfLargeTents:NUMERIC}, #{numberOfSmallTents:NUMERIC}, #{photo:VARCHAR})")
	void insert(RegionTransitCamp regionTransitCamp);

	@Update("UPDATE tbl_region_transit_camp SET id_region_transit_camp=#{id:VARCHAR}, header_id_region_transit_camp=#{headerId:VARCHAR}, code_region_transit_camp=#{code:VARCHAR}, id_master_transit_camp_region_transit_camp=#{idMasterTransitCamp:VARCHAR}, start_register_region_transit_camp=#{startRegister:TIMESTAMP}, end_register_region_transit_camp=#{endRegister:TIMESTAMP}, extimate_arrived_region_transit_camp=#{extimateArrived:TIMESTAMP}, number_of_large_tents_region_transit_camp=#{numberOfLargeTents:NUMERIC}, number_of_small_tents_region_transit_camp=#{numberOfSmallTents:NUMERIC}, photo_region_transit_camp=#{photo:VARCHAR} WHERE id_region_transit_camp=#{id}")
	void update(RegionTransitCamp regionTransitCamp);

	@Delete("DELETE FROM tbl_region_transit_camp WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_region_transit_camp WHERE id_region_transit_camp=#{id}")
	void delete(RegionTransitCamp regionTransitCamp);

	List<RegionTransitCamp> getList(QueryParameter param);

	RegionTransitCamp getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
}
