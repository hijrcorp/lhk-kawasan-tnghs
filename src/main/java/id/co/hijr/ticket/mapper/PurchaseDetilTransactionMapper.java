package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.PurchaseDetilTransaction;

@Mapper
public interface PurchaseDetilTransactionMapper {

	@Insert("INSERT INTO tbl_purchase_detil_transaction (id_purchase_detil_transaction, header_id_purchase_detil_transaction, payment_method_purchase_detil_transaction, payment_code_purchase_detil_transaction, bank_id_purchase_detil_transaction, submited_date_purchase_detil_transaction, confirmation_payment_purchase_detil_transaction, account_name_sender_purchase_detil_transaction, account_number_sender_purchase_detil_transaction, bank_name_sender_purchase_detil_transaction, account_name_receiver_purchase_detil_transaction, account_number_receiver_purchase_detil_transaction, bank_name_receiver_purchase_detil_transaction, file_transaction_purchase_detil_transaction, transaction_date_purchase_detil_transaction) VALUES (#{id:VARCHAR}, #{headerId:VARCHAR}, #{paymentMethod:VARCHAR}, #{paymentCode:VARCHAR}, #{bankId:VARCHAR}, #{submitedDate:TIMESTAMP}, #{confirmationPayment:NUMERIC}, #{accountNameSender:VARCHAR}, #{accountNumberSender:VARCHAR}, #{bankNameSender:VARCHAR}, #{accountNameReceiver:VARCHAR}, #{accountNumberReceiver:VARCHAR}, #{bankNameReceiver:VARCHAR}, #{fileTransaction:VARCHAR}, #{transactionDate:TIMESTAMP})")
	void insert(PurchaseDetilTransaction purchaseDetilTransaction);

	@Update("UPDATE tbl_purchase_detil_transaction SET id_purchase_detil_transaction=#{id:VARCHAR}, header_id_purchase_detil_transaction=#{headerId:VARCHAR}, payment_method_purchase_detil_transaction=#{paymentMethod:VARCHAR}, payment_code_purchase_detil_transaction=#{paymentCode:VARCHAR}, bank_id_purchase_detil_transaction=#{bankId:VARCHAR}, submited_date_purchase_detil_transaction=#{submitedDate:TIMESTAMP}, confirmation_payment_purchase_detil_transaction=#{confirmationPayment:NUMERIC}, account_name_sender_purchase_detil_transaction=#{accountNameSender:VARCHAR}, account_number_sender_purchase_detil_transaction=#{accountNumberSender:VARCHAR}, bank_name_sender_purchase_detil_transaction=#{bankNameSender:VARCHAR}, account_name_receiver_purchase_detil_transaction=#{accountNameReceiver:VARCHAR}, account_number_receiver_purchase_detil_transaction=#{accountNumberReceiver:VARCHAR}, bank_name_receiver_purchase_detil_transaction=#{bankNameReceiver:VARCHAR}, file_transaction_purchase_detil_transaction=#{fileTransaction:VARCHAR}, transaction_date_purchase_detil_transaction=#{transactionDate:TIMESTAMP} WHERE id_purchase_detil_transaction=#{id}")
	void update(PurchaseDetilTransaction purchaseDetilTransaction);

	@Delete("DELETE FROM tbl_purchase_detil_transaction WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_purchase_detil_transaction WHERE id_purchase_detil_transaction=#{id}")
	void delete(PurchaseDetilTransaction purchaseDetilTransaction);

	List<PurchaseDetilTransaction> getList(QueryParameter param);

	PurchaseDetilTransaction getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
	//costum
	PurchaseDetilTransaction getEntityHeader(String id);
}
