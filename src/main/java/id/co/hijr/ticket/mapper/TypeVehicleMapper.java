package id.co.hijr.ticket.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import id.co.hijr.sistem.common.QueryParameter;
import id.co.hijr.ticket.model.TypeVehicle;
import id.co.hijr.ticket.model.Vehicle;

@Mapper
public interface TypeVehicleMapper {

	@Insert("INSERT INTO tbl_type_vehicle (id_type_vehicle, name_type_vehicle) VALUES (#{id:VARCHAR}, #{name:VARCHAR})")
	void insert(TypeVehicle typeVehicle);

	@Update("UPDATE tbl_type_vehicle SET id_type_vehicle=#{id:VARCHAR}, name_type_vehicle=#{name:VARCHAR} WHERE id_type_vehicle=#{id}")
	void update(TypeVehicle typeVehicle);

	@Delete("DELETE FROM tbl_type_vehicle WHERE ${clause}")
	void deleteBatch(QueryParameter param);

	@Delete("DELETE FROM tbl_type_vehicle WHERE id_type_vehicle=#{id}")
	void delete(TypeVehicle typeVehicle);

	List<TypeVehicle> getList(QueryParameter param);

	TypeVehicle getEntity(String id);

	long getCount(QueryParameter param);

	String getNewId();
	
}
