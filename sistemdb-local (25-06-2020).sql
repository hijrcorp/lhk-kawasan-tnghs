/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.31-MariaDB : Database - sistemdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistemdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistemdb`;

/*Table structure for table `hijr_account` */

DROP TABLE IF EXISTS `hijr_account`;

CREATE TABLE `hijr_account` (
  `id_account` varchar(50) NOT NULL DEFAULT '',
  `username_account` varchar(30) DEFAULT NULL,
  `password_account` varchar(500) DEFAULT NULL,
  `email_account` varchar(100) DEFAULT NULL,
  `email_status_account` tinyint(1) DEFAULT NULL,
  `first_name_account` varchar(100) NOT NULL DEFAULT '',
  `last_name_account` varchar(100) NOT NULL DEFAULT '',
  `mobile_account` varchar(30) DEFAULT NULL,
  `mobile_status_account` tinyint(1) DEFAULT NULL,
  `male_status_account` tinyint(1) DEFAULT NULL,
  `birth_date_account` date DEFAULT NULL,
  `enabled_account` tinyint(1) NOT NULL,
  `account_non_expired_account` tinyint(1) NOT NULL,
  `credentials_non_expired_account` tinyint(1) NOT NULL,
  `account_non_locked_account` tinyint(1) NOT NULL,
  `person_added_account` varchar(50) DEFAULT NULL,
  `time_added_account` datetime DEFAULT NULL,
  `person_modified_account` varchar(50) DEFAULT NULL,
  `time_modified_account` datetime DEFAULT NULL,
  `login_account` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_account`),
  UNIQUE KEY `username` (`username_account`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_account` */

insert  into `hijr_account`(`id_account`,`username_account`,`password_account`,`email_account`,`email_status_account`,`first_name_account`,`last_name_account`,`mobile_account`,`mobile_status_account`,`male_status_account`,`birth_date_account`,`enabled_account`,`account_non_expired_account`,`credentials_non_expired_account`,`account_non_locked_account`,`person_added_account`,`time_added_account`,`person_modified_account`,`time_modified_account`,`login_account`) values 
('1520819042544','admin','$2a$10$/fbRpiHqNXGbxucT6FL9huz6OYvQJAReIKt0sf5MK57.09SNCvRs2','gustaroska@gmail.com',NULL,'Super','Admin','6281281823697',NULL,NULL,NULL,1,1,1,1,NULL,NULL,NULL,NULL,22),
('1520819042545','gustaroska','$2a$10$/fbRpiHqNXGbxucT6FL9huz6OYvQJAReIKt0sf5MK57.09SNCvRs2','gustaroska@hijr.co.id',NULL,'Refit','Gustaroska','081281823697',NULL,NULL,NULL,1,1,1,1,NULL,NULL,NULL,NULL,1),
('1520819042546','hamdan','$2a$10$XjRiigh1vuwSDhfXuUnlFO9GRlOnyNz.m0nAGkGkBOji1uJh.Vxji','hasyuba.sys@gmail.com',NULL,'Hamdan','Batubara','08111111202',NULL,NULL,NULL,1,1,1,1,NULL,NULL,'1520819042544','2018-11-17 10:20:29',0),
('1542070924987','aparaturlhk','$2a$10$MxBZFSTwu1vfmCW.45iKUeGCzHo2FyYEtn5iRk/ijLGKHHG..Q8Vy','',NULL,'Admin','Aparatur','',NULL,NULL,NULL,1,1,1,1,NULL,NULL,'1520819042544','2018-11-16 08:25:41',0),
('1542881261761','oprsoal',NULL,NULL,NULL,'Operator','Bank Soal',NULL,NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-22 17:07:41',NULL,NULL,0),
('1543475745102','pelayanan','$2a$10$0EsNDiAZV1Gbjrql.w4kpuNExxKTRORDtF2mYmi8weGutEubCbev6','',NULL,'Seksi','Pelayanan','',NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-29 15:15:44','1543544826379','2018-12-09 15:10:44',0),
('1543478734417','permohonan','$2a$10$xd/6NHpqB.VCddYqBlqdJ.CYgIHAQKpB/TWcF31xWyfBCbpfc8Kui','',NULL,'Bagian','Permohonan','',NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-29 16:05:34','1543544826379','2018-11-30 17:11:26',0),
('1543544826379','adminpps','$2a$10$wlk1kO5gtUP7dkbverGsrOsFIoNZQGe51dKWJi0znwvWS8VBsd3qm','admin@hijr.co.id',NULL,'Admin','PPS','081281823697',NULL,NULL,NULL,1,1,1,1,'1520819042544','2018-11-30 10:27:05','1543544826379','2018-12-06 08:12:34',0),
('1544058068121','udin',NULL,'',NULL,'Udin','Jaenudin','',NULL,NULL,NULL,0,1,1,1,'1543544826379','2018-12-06 09:01:07',NULL,NULL,0),
('1544058089624','samsul',NULL,'',NULL,'Samsul','Bahri','',NULL,NULL,NULL,0,1,1,1,'1543544826379','2018-12-06 09:01:29',NULL,NULL,0),
('1544058156175','agus',NULL,'',NULL,'drh. Agus','Susanto','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-06 09:02:35',NULL,NULL,0),
('1544058184434','mukidi',NULL,'',NULL,'drh. Mukidi','Jaelani','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-06 09:03:04',NULL,NULL,0),
('1544578764544','opr','$2a$10$Q92W65aezPINQ8GYeBSsIemLwEXLr4u0tmTET.3WYIleJzTQ8E0xG','',NULL,'Operator','Magang','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-12 09:39:24','1543544826379','2018-12-12 09:39:43',0),
('1544664228345','tu','$2a$10$ljc8mheFhv/2EI8n5UcZ5.x59QbKmJmElkIop3lFBD37WQeNCEORC','',NULL,'Tata','Usaha','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-13 08:23:47',NULL,NULL,0),
('1544664544556','kepala','$2a$10$6hqEcJvR65gSxTfXr46..u/8GF3/SpowX/MEzrrGKrt44GJwr.cpy','',NULL,'Kepala','Balai','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-13 08:29:03',NULL,NULL,0),
('1544790230958','asd',NULL,'asd',NULL,'asd','qwe','',NULL,NULL,NULL,1,1,1,1,'1543544826379','2018-12-14 19:23:51','1543544826379','2018-12-14 19:24:01',0),
('1579048263839648','refit','$2a$10$/fbRpiHqNXGbxucT6FL9huz6OYvQJAReIKt0sf5MK57.09SNCvRs2','refit.gustaroska@gmail.com',NULL,'Refit','Gustaroska','081808236637',NULL,NULL,NULL,1,1,1,1,NULL,NULL,NULL,NULL,3);

/*Table structure for table `hijr_account_group` */

DROP TABLE IF EXISTS `hijr_account_group`;

CREATE TABLE `hijr_account_group` (
  `id_account_group` varchar(50) NOT NULL DEFAULT '',
  `name_account_group` varchar(200) NOT NULL DEFAULT '',
  `description_account_group` varchar(500) DEFAULT NULL,
  `application_id_account_group` varchar(50) NOT NULL DEFAULT '',
  `sequence_account_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_account_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_account_group` */

insert  into `hijr_account_group`(`id_account_group`,`name_account_group`,`description_account_group`,`application_id_account_group`,`sequence_account_group`) values 
('1001','Administrator',NULL,'001',1),
('1002','Supervisor',NULL,'001',2),
('1003','Analis',NULL,'001',3),
('1004','Operator',NULL,'001',4),
('1005','Tata Usaha',NULL,'001',5),
('1543474689117','Kepala Balai','Approval','007',1),
('1543474707783','Tata Usaha','Verifikasi dan Validasi','007',2),
('1543474714617','Pelayanan','Penerimaan dan Pengeluaran','007',4),
('1543474751393','Operator','','007',5),
('1543475100552','Permohonan','','007',3),
('1543544803085','Administrator','Full Akses','007',6),
('1544057998367','Keeper','Operator Log Keeper','007',7),
('1544058020139','Dokter','Operator Log Dokter','007',8),
('2001','Administrator',NULL,'002',1),
('2002','Supervisor',NULL,'002',2),
('2003','Analis',NULL,'002',3),
('2004','Operator Satker',NULL,'002',4),
('3001','Administrator',NULL,'003',1),
('3002','Inspektur / Kepala Bagian',NULL,'003',2),
('3003','Supervisor Bagian / RAPIM',NULL,'003',3),
('3004','Operator / TU Wilayah / Investigasi',NULL,'003',4),
('4001','Administrator',NULL,'004',1),
('4002','Seksi P2',NULL,'004',2),
('4003','Keuangan',NULL,'004',3),
('4004','Tata Usaha',NULL,'004',4),
('4005','Bidang Wilayah',NULL,'004',5),
('4006','Pemegang Izin',NULL,'004',6),
('5001','Administrator',NULL,'005',1),
('5002','Pemegang Izin',NULL,'005',2),
('6001','Administrator',NULL,'006',1),
('6002','Operator',NULL,'006',2);

/*Table structure for table `hijr_account_group_role` */

DROP TABLE IF EXISTS `hijr_account_group_role`;

CREATE TABLE `hijr_account_group_role` (
  `group_id_account_group_role` varchar(50) NOT NULL DEFAULT '',
  `role_code_account_group_role` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`group_id_account_group_role`,`role_code_account_group_role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_account_group_role` */

insert  into `hijr_account_group_role`(`group_id_account_group_role`,`role_code_account_group_role`) values 
('1543474689117','KEPALA'),
('1543474707783','TU'),
('1543474714617','PELAYANAN'),
('1543474751393','OPERATOR'),
('1543475100552','PERMOHONAN'),
('1543544803085','ADMIN'),
('1543544803085','KEPALA'),
('1543544803085','OPERATOR'),
('1543544803085','PELAYANAN'),
('1543544803085','PERMOHONAN'),
('1543544803085','TU'),
('6001','ADMIN'),
('6001','OPERATOR'),
('6002','OPERATOR');

/*Table structure for table `hijr_account_role` */

DROP TABLE IF EXISTS `hijr_account_role`;

CREATE TABLE `hijr_account_role` (
  `id_account_role` varchar(50) DEFAULT NULL,
  `code_account_role` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_account_role` */

insert  into `hijr_account_role`(`id_account_role`,`code_account_role`) values 
('1520819042544','ROLE_SUPER_ADMIN'),
('1542070924987','ROLE_LHK_ADMIN'),
('1579048263839648','ROLE_USER'),
('1520819042545','ROLE_ME');

/*Table structure for table `hijr_application` */

DROP TABLE IF EXISTS `hijr_application`;

CREATE TABLE `hijr_application` (
  `id_application` varchar(50) NOT NULL DEFAULT '',
  `code_application` varchar(50) NOT NULL DEFAULT '',
  `name_application` varchar(100) NOT NULL DEFAULT '',
  `prefix_role_application` varchar(20) NOT NULL DEFAULT '',
  `organization_id_application` varchar(50) NOT NULL DEFAULT '',
  `link_application` varchar(500) NOT NULL DEFAULT '',
  `default_role_application` varchar(50) NOT NULL DEFAULT '',
  `person_added_application` varchar(50) DEFAULT NULL,
  `time_added_application` datetime DEFAULT NULL,
  `person_modified_application` varchar(50) DEFAULT NULL,
  `time_modified_application` datetime DEFAULT NULL,
  PRIMARY KEY (`id_application`),
  UNIQUE KEY `code_application` (`code_application`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_application` */

insert  into `hijr_application`(`id_application`,`code_application`,`name_application`,`prefix_role_application`,`organization_id_application`,`link_application`,`default_role_application`,`person_added_application`,`time_added_application`,`person_modified_application`,`time_modified_application`) values 
('001','ALHP','Analisis Laporan Hasil Pemeriksaan','ROLE_ALHP_','3110201400001','https://lhk.hijr.co.id/lha/','ROLE_ALHP_USER',NULL,NULL,NULL,NULL),
('002','PTL','Pemantauan Tindak Lanjut','ROLE_PTL_','3110201400001','https://lhk.hijr.co.id/ptl/','ROLE_PTL_USER',NULL,NULL,NULL,NULL),
('003','PKPT','Program Kerja Pengawasan Tahunan','ROLE_PKPT_','3110201400001','https://lhk.hijr.co.id/pkpt/','ROLE_PKPT_USER',NULL,NULL,NULL,NULL),
('004','SATSDN','Surat Angkut TSL Dalam Negeri','ROLE_SATSDN_','3110201400001','https://www.bbksdajabar.com/satsdn/','ROLE_SATSDN_USER',NULL,NULL,NULL,NULL),
('005','PKR','Database Penangkaran TSL','ROLE_PKR_','3110201400001','https://www.bbksdajabar.com/penangkaran/','ROLE_PKR_USER',NULL,NULL,NULL,NULL),
('006','APR','Aparatur Survey Jabatan','ROLE_APR_','3110201400001','','ROLE_APR_USER',NULL,NULL,NULL,NULL),
('007','PPS','Pusat Penyelamatan Satwa','ROLE_PPS_','3110201400001','','ROLE_PPS_USER',NULL,NULL,NULL,NULL),
('1542357831118','TEST','test','ROLE_TEST_','3110201400000','','ROLE_TEST_USER','1520819042544','2018-11-16 15:43:50',NULL,NULL),
('1542357885057','CUTI','Aplikasi Cuti Online','ROLE_CUTI_','3110201400000','','ROLE_CUTI_USER','1520819042544','2018-11-16 15:44:44',NULL,NULL),
('1542358039259','MAAL','Aplikasi Manajemen Keuangan','ROLE_MAAL_','3110201400000','','ROLE_MAAL_USER','1520819042544','2018-11-16 15:47:18',NULL,NULL),
('1542445618488','QWE','qwe','ROLE_QWE_','3110201400000','','ROLE_QWE_USER','1520819042544','2018-11-17 16:06:58',NULL,NULL),
('1542445624628','ASD','asd','ROLE_ASD_','3110201400000','','ROLE_ASD_USER','1520819042544','2018-11-17 16:07:03',NULL,NULL);

/*Table structure for table `hijr_authorities` */

DROP TABLE IF EXISTS `hijr_authorities`;

CREATE TABLE `hijr_authorities` (
  `id_authorities` varchar(50) NOT NULL,
  `account_id_authorities` varchar(50) NOT NULL DEFAULT '',
  `group_id_authorities` varchar(50) NOT NULL DEFAULT '',
  `application_id_authorities` varchar(50) NOT NULL DEFAULT '',
  `person_added_authorities` varchar(50) NOT NULL DEFAULT '',
  `time_added_authorities` datetime NOT NULL,
  PRIMARY KEY (`account_id_authorities`,`group_id_authorities`,`application_id_authorities`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_authorities` */

insert  into `hijr_authorities`(`id_authorities`,`account_id_authorities`,`group_id_authorities`,`application_id_authorities`,`person_added_authorities`,`time_added_authorities`) values 
('15208190425441001','1520819042544','1001','001','1520819042544','2018-11-17 16:21:57'),
('15208190425442001','1520819042544','2001','002','1520819042544','2018-11-17 16:21:57'),
('15208190425443001','1520819042544','3001','003','1520819042544','2018-11-17 16:21:57'),
('15208190425444001','1520819042544','4001','004','1520819042544','2018-11-17 16:21:57'),
('15208190425445001','1520819042544','5001','005','1520819042544','2018-11-17 16:21:57'),
('15208190425446001','1520819042544','6001','006','1520819042544','2018-11-17 16:21:57'),
('15420709249876001','1542070924987','6001','006','1520819042544','2018-11-17 16:21:57'),
('1543475744602','1543475745102','1543474714617','007','1543544826379','2018-12-09 15:10:44'),
('1543478734883','1543478734417','1543475100552','007','1543544826379','2018-11-30 17:11:26'),
('1543544826615','1543544826379','1543544803085','007','1543544826379','2018-12-06 08:12:34'),
('1544058068098','1544058068121','1544057998367','007','1543544826379','2018-12-06 09:01:07'),
('1544058089506','1544058089624','1544057998367','007','1543544826379','2018-12-06 09:01:29'),
('1544058156758','1544058156175','1544058020139','007','1543544826379','2018-12-06 09:02:35'),
('1544058184886','1544058184434','1544058020139','007','1543544826379','2018-12-06 09:03:04'),
('1544578764139','1544578764544','1543474751393','007','1543544826379','2018-12-12 09:39:43'),
('1544664228560','1544664228345','1543474707783','007','1543544826379','2018-12-13 08:23:47'),
('1544664544239','1544664544556','1543474689117','007','1543544826379','2018-12-13 08:29:03'),
('1579048263839671','1579048263839648','1543474751393','007','1520819042544','2020-01-15 08:31:03'),
('1542509690497','196810161994031002','6002','006','1520819042544','2018-11-18 09:54:50');

/*Table structure for table `hijr_control` */

DROP TABLE IF EXISTS `hijr_control`;

CREATE TABLE `hijr_control` (
  `id_control` varchar(50) NOT NULL DEFAULT '',
  `account_id_control` varchar(50) DEFAULT NULL,
  `source_id_control` varchar(50) DEFAULT NULL,
  `is_admin_control` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_control`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_control` */

insert  into `hijr_control`(`id_control`,`account_id_control`,`source_id_control`,`is_admin_control`) values 
('1580461791000','1520819042544','3110201400000',1),
('1580462123000','1520819042544','3110201400001',1),
('1580462146000','1520819042545','3110201400000',0),
('1580462183000','1520819042546','3110201400001',0);

/*Table structure for table `hijr_file` */

DROP TABLE IF EXISTS `hijr_file`;

CREATE TABLE `hijr_file` (
  `id_file` varchar(50) NOT NULL DEFAULT '',
  `name_file` varchar(260) NOT NULL DEFAULT '',
  `folder_file` varchar(260) NOT NULL DEFAULT '',
  `size_file` bigint(20) NOT NULL,
  `type_file` varchar(50) DEFAULT '',
  `reference_id_file` varchar(50) DEFAULT '',
  `module_file` varchar(50) DEFAULT NULL,
  `person_added_file` varchar(200) NOT NULL DEFAULT '',
  `time_added_file` datetime NOT NULL,
  `load_file` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_file` */

/*Table structure for table `hijr_login` */

DROP TABLE IF EXISTS `hijr_login`;

CREATE TABLE `hijr_login` (
  `id_login` varchar(50) NOT NULL DEFAULT '',
  `access_token_login` text NOT NULL,
  `client_id_login` varchar(50) NOT NULL DEFAULT '',
  `username_login` varchar(50) NOT NULL DEFAULT '',
  `refresh_token_login` text NOT NULL,
  `created_time_login` datetime NOT NULL,
  `expire_time_login` datetime NOT NULL,
  `token_object_login` blob NOT NULL,
  `account_id_login` varchar(50) DEFAULT NULL,
  `source_id_login` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_login` */

insert  into `hijr_login`(`id_login`,`access_token_login`,`client_id_login`,`username_login`,`refresh_token_login`,`created_time_login`,`expire_time_login`,`token_object_login`,`account_id_login`,`source_id_login`) values 
('1566c8a0-8edf-11ea-a538-81a64814b5e8','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjIwMjI1OTM3LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI0MjNlNWU1MS0wOGNmLTQzODEtOTI2Ny1lMWNiMDI3OTA3N2UiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.RUaZIfNT5L6SyhZoStSXaXvpXyhX4VGN5AwUfhd1MtCUiI-uEYKDg68WuXjTWhap6sGCS6KzND2LO2cms2nktciOTKug2GSfluljnxUcTyeAd83erobfDkvQQn-C3HGXtynAFHc8deaJDAb0eTRa-8JcndLLa9zPNiHW_qHIpLZ7wQtYC8wg1buhJLxjBnG7YgKH-kYQ04Fjw9dM3S4EPn_w2pHaFFbEApRy8QN_KX8G5I4FA1QoLyyL0q7qg3WqXlxCNiiH9iI7cO0wMgUCDVSlsrLOSkskxXlhf0W4yooXD6mOl-ZzlGKM36olI4myrB41Q8mMJ6fIElKqJA-wUw','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI0MjNlNWU1MS0wOGNmLTQzODEtOTI2Ny1lMWNiMDI3OTA3N2UiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4NzE5OTM3LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI0NzM2MTk1NS0xOWQ2LTRlMTctYmE5Ni05YzkwM2ZhNzMxODciLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.enwu2xDhejG6T3eKL6ELTId-_od8MXpMtyMTd26iUoBSUHSFah_L8Qu32ikQTbs4ye0pbRfk-WE5eJS7AzejucE55vLwLY8ZKNdAgNoie4e5EDWf5nU7CTPJEWZKSlZU6V29UZcGAHhGG3stbJplnscJVqK-bHYSZTGsQKH9CTyLl6R20hm4l5nBwYCdltCHHO2v-eW8krgJu1uGg4MrYE9q1YHG6ZqBPwShsZCObTB-u-giGmuVCdkfv7_ujl0Ipv11PH2o6ql7huke0Fw6numg_8Xt8RgpMlTNWknktu5sn8ly6m_f3R4EwsDbrGKdKCb9Rf4fnf2wpp4HFM33sQ','2020-05-05 00:00:00','2021-05-05 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$423e5e51-08cf-4381-9267-e1cb0279077ex\0sr\0java.util.Datehj�KYt\0\0xpw\0\0y<�Ѕxsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptseyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI0MjNlNWU1MS0wOGNmLTQzODEtOTI2Ny1lMWNiMDI3OTA3N2UiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4NzE5OTM3LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI0NzM2MTk1NS0xOWQ2LTRlMTctYmE5Ni05YzkwM2ZhNzMxODciLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.enwu2xDhejG6T3eKL6ELTId-_od8MXpMtyMTd26iUoBSUHSFah_L8Qu32ikQTbs4ye0pbRfk-WE5eJS7AzejucE55vLwLY8ZKNdAgNoie4e5EDWf5nU7CTPJEWZKSlZU6V29UZcGAHhGG3stbJplnscJVqK-bHYSZTGsQKH9CTyLl6R20hm4l5nBwYCdltCHHO2v-eW8krgJu1uGg4MrYE9q1YHG6ZqBPwShsZCObTB-u-giGmuVCdkfv7_ujl0Ipv11PH2o6ql7huke0Fw6numg_8Xt8RgpMlTNWknktu5sn8ly6m_f3R4EwsDbrGKdKCb9Rf4fnf2wpp4HFM33sQsq\0~\0w\0\0q�hxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert7eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjIwMjI1OTM3LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI0MjNlNWU1MS0wOGNmLTQzODEtOTI2Ny1lMWNiMDI3OTA3N2UiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.RUaZIfNT5L6SyhZoStSXaXvpXyhX4VGN5AwUfhd1MtCUiI-uEYKDg68WuXjTWhap6sGCS6KzND2LO2cms2nktciOTKug2GSfluljnxUcTyeAd83erobfDkvQQn-C3HGXtynAFHc8deaJDAb0eTRa-8JcndLLa9zPNiHW_qHIpLZ7wQtYC8wg1buhJLxjBnG7YgKH-kYQ04Fjw9dM3S4EPn_w2pHaFFbEApRy8QN_KX8G5I4FA1QoLyyL0q7qg3WqXlxCNiiH9iI7cO0wMgUCDVSlsrLOSkskxXlhf0W4yooXD6mOl-ZzlGKM36olI4myrB41Q8mMJ6fIElKqJA-wUw',NULL,'3110201400000'),
('1581a3a1-8edf-11ea-a538-79346cc0b3b7','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MjAyMjU5MzcsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImEyZTI4MzZjLWFhNTUtNGI2Ny1hMWMyLTJlMTI5OWI0OWE2NiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.QmMWFd-_GAipKeoDFhBiovSTC3O5xy8RzqlGaidketvstjoZfuyZkHrN6BLS1I9wpwj7lA-9TytlZ6VL-vyaZOnQ12OCCWmk-dePucGHRDoFt-3kvas9MF1qThebCIW2ecQsxajsWuX0rKm9CdxGYoiq2NVcHEM0miPUHNTWfWQXAcZ59LDfNEgl3v0bS-civ2O1AOG07R2Knml_kzU1m08m-Wdv9fabwJOxzmxIPmBB0JZRRAWbGodKOrBLmVMOIMC9EIzLxJYQADw_9QkKAkX23s0LgRGm9bf0vPkQhitDmTESs3hooXVcxhh5sF-8KcVmowmBjuJkXlvr-bQ8jQ','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJhMmUyODM2Yy1hYTU1LTRiNjctYTFjMi0yZTEyOTliNDlhNjYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4NzE5OTM3LCJqdGkiOiI4YTRmN2I4ZS0zNmIyLTRiMjYtYmNlNi02OGZhN2ZiY2U0YzkiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.OFXCEJ9WjmpHr-8Sv2_S1JFn5ldQQh59CwwvvQ-7DTme8AgmsBg4nR_AsE1c9CM9MUIkNDDDOmI577-OYYcX-Pb7IXGgdGtmtOpCfkVkjruJ60nJIA-Iidu4Zc4pDq4Aq2RmtqCQhZuTCEukYsZURsfhGagSWBR0n0z_M7Z0r5X2Ol_q9hg8uZKGEkw3UkVHFUXVVxhyi6oVE5Xu0oQV33qw-dgE9rL4Xvt2yTuQRBXrRORQ7xz5G5Hy4IB1tdnaycWI4wDNGM9w1JKeJWbjAc9gxLSo-ypNnLbiatx1YaN8CkZkZc9XGN1XSg_rFU_kWtcj50Sbwo1XaMe6sm83ig','2020-05-05 00:00:00','2021-05-05 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$a2e2836c-aa55-4b67-a1c2-2e1299b49a66x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0y<��Qxsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJhMmUyODM2Yy1hYTU1LTRiNjctYTFjMi0yZTEyOTliNDlhNjYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4NzE5OTM3LCJqdGkiOiI4YTRmN2I4ZS0zNmIyLTRiMjYtYmNlNi02OGZhN2ZiY2U0YzkiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.OFXCEJ9WjmpHr-8Sv2_S1JFn5ldQQh59CwwvvQ-7DTme8AgmsBg4nR_AsE1c9CM9MUIkNDDDOmI577-OYYcX-Pb7IXGgdGtmtOpCfkVkjruJ60nJIA-Iidu4Zc4pDq4Aq2RmtqCQhZuTCEukYsZURsfhGagSWBR0n0z_M7Z0r5X2Ol_q9hg8uZKGEkw3UkVHFUXVVxhyi6oVE5Xu0oQV33qw-dgE9rL4Xvt2yTuQRBXrRORQ7xz5G5Hy4IB1tdnaycWI4wDNGM9w1JKeJWbjAc9gxLSo-ypNnLbiatx1YaN8CkZkZc9XGN1XSg_rFU_kWtcj50Sbwo1XaMe6sm83igsq\0~\0w\0\0q�h�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MjAyMjU5MzcsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImEyZTI4MzZjLWFhNTUtNGI2Ny1hMWMyLTJlMTI5OWI0OWE2NiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.QmMWFd-_GAipKeoDFhBiovSTC3O5xy8RzqlGaidketvstjoZfuyZkHrN6BLS1I9wpwj7lA-9TytlZ6VL-vyaZOnQ12OCCWmk-dePucGHRDoFt-3kvas9MF1qThebCIW2ecQsxajsWuX0rKm9CdxGYoiq2NVcHEM0miPUHNTWfWQXAcZ59LDfNEgl3v0bS-civ2O1AOG07R2Knml_kzU1m08m-Wdv9fabwJOxzmxIPmBB0JZRRAWbGodKOrBLmVMOIMC9EIzLxJYQADw_9QkKAkX23s0LgRGm9bf0vPkQhitDmTESs3hooXVcxhh5sF-8KcVmowmBjuJkXlvr-bQ8jQ',NULL,'3110201400000'),
('1e12a30d-8f68-11ea-8f8b-2d10454b80ce','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjIwMjg0NzkzLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiIwOGFhN2I1My1jMjI0LTRhZDAtOTU1Zi1lYjMzMTRiNzdiMzciLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.bkJ5C3ELCd4130prMZdnAk-rSNiiuqcyep6DmIgqIejQDj77XU1V4_-EvVUsGLAKrqygooXT09jdyWJPNpwz6S2q8kFljDYQa0iSFkQQcAQU6uHV6OvvWzstSsf77aJm-CC7Iyk1DAgX5Of6DtjLsL_K_c9OBH8hIv5TptARvEb7x2HOWhNB7oQkFZvpag0sd-Bi-dcCUZl6T2NSBUYbEM-mviPlQlbEkPCnWT3vdzTNedMp8LOrbbIEmhhlaJh-9AJR3XoCteZXAsswoG75uesOK-I8Vj_-0PeONYQLVFOsUMBTDVaijsbmxSE608I50U8u8it1d1t3yDB5_B1dRw','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiIwOGFhN2I1My1jMjI0LTRhZDAtOTU1Zi1lYjMzMTRiNzdiMzciLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4Nzc4NzkzLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiJmZTAwMzgzYy01YTA4LTQzZDEtOTliYS1iOTY5MTE4ZWVkNDgiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.Fx8v7Y7F_KyHYFvXmBGSQ4Qxcuv2qekLYUUDQgFtQfghOOPZWvKf8EcrjGe1CXOPyDawQIN03lp4ahTlVxwYs9oVGiM5Ll7uA1PQ1V0T9Zl0cbJWViupccKEvAYpzqTYhYg4-IbhhURalSQXxD8IML5jqmlBHX86yvk5ObDCF76021pyqyMKED2FTYxzawEAjs95BGMMrnkkrybdZ-eKdFKTshGbAxYqJIHjozhyCb5QqPevr0QOgQ_VXJH0EF9MxZX59A5EILZS8ep8mMtZt1WgtTDvykNvNo_Z1i7J-N3NGbIfaHhDgu91EjeHLeSdAfK6D6aMbStNq6NkHO5NyQ','2020-05-06 00:00:00','2021-05-06 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$08aa7b53-c224-4ad0-955f-eb3314b77b37x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0y@�xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptseyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiIwOGFhN2I1My1jMjI0LTRhZDAtOTU1Zi1lYjMzMTRiNzdiMzciLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4Nzc4NzkzLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiJmZTAwMzgzYy01YTA4LTQzZDEtOTliYS1iOTY5MTE4ZWVkNDgiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.Fx8v7Y7F_KyHYFvXmBGSQ4Qxcuv2qekLYUUDQgFtQfghOOPZWvKf8EcrjGe1CXOPyDawQIN03lp4ahTlVxwYs9oVGiM5Ll7uA1PQ1V0T9Zl0cbJWViupccKEvAYpzqTYhYg4-IbhhURalSQXxD8IML5jqmlBHX86yvk5ObDCF76021pyqyMKED2FTYxzawEAjs95BGMMrnkkrybdZ-eKdFKTshGbAxYqJIHjozhyCb5QqPevr0QOgQ_VXJH0EF9MxZX59A5EILZS8ep8mMtZt1WgtTDvykNvNo_Z1i7J-N3NGbIfaHhDgu91EjeHLeSdAfK6D6aMbStNq6NkHO5NyQsq\0~\0w\0\0q�x�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert7eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjIwMjg0NzkzLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiIwOGFhN2I1My1jMjI0LTRhZDAtOTU1Zi1lYjMzMTRiNzdiMzciLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.bkJ5C3ELCd4130prMZdnAk-rSNiiuqcyep6DmIgqIejQDj77XU1V4_-EvVUsGLAKrqygooXT09jdyWJPNpwz6S2q8kFljDYQa0iSFkQQcAQU6uHV6OvvWzstSsf77aJm-CC7Iyk1DAgX5Of6DtjLsL_K_c9OBH8hIv5TptARvEb7x2HOWhNB7oQkFZvpag0sd-Bi-dcCUZl6T2NSBUYbEM-mviPlQlbEkPCnWT3vdzTNedMp8LOrbbIEmhhlaJh-9AJR3XoCteZXAsswoG75uesOK-I8Vj_-0PeONYQLVFOsUMBTDVaijsbmxSE608I50U8u8it1d1t3yDB5_B1dRw',NULL,'3110201400000'),
('1e56b0ee-8f68-11ea-8f8b-458f87b2eb26','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MjAyODQ3OTMsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjIwNGZmYmNlLWEzYjYtNDFiZC1iZTdkLTVmOTdjZjFlODVkMiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.MPcoWlhtlXcXYVnzKDI5ncnKj_IbzO1VI86OYeSzp2oAiii1o_fRPBIOD4uhepEZrkZZzl6gVwQ5bAtOzwzb0N9F_tTWsZx2ZRRbKi_i-Xzie-BBjwHM69Fh4W0dJuVZTmmY1fQTSwum5UQmQt2YrzGm-TzDRyq57u_sy-9-fiEAwRBqvfz_13dCatg4dJmDgEsDBqDsN84WnEMNzYTgQIxgoAxuWHn7fRkj_H6FaucsTroBBiHVSzr5l2xOekbOBS0g73r3T2hzEyByM_yhYtEFLYZHnLwny-Gbp1tE9VvdnYrlfB1-i1QvFfyYUN0Q4awIGD_1uMV34CCpMMkc-A','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIyMDRmZmJjZS1hM2I2LTQxYmQtYmU3ZC01Zjk3Y2YxZTg1ZDIiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4Nzc4NzkzLCJqdGkiOiJjMDFiODg4Ny1lZTYwLTRjNjAtOTI0YS1jYmZjZWNmODBiODUiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.UiSGs8ISzx10cKoHdwtBrUXcdd6w-Dr61FXeUERH3X6dTWeNKXgDb0PzGZd-Sc5FfsrTPUgDl73EIBbI0lGmqMht2HQpflmFBShcRJmz2L4PyzDCF3eF9_pw6um5RcC4liIEpI17fvrlh4hFgGnz7iKpn4g2n-OwEduEoH31PX7Dl7UmyEB_CI2NgaV5-7tTLeNZABT3fWi6LETPwvjXvUsTwPuG25y9aRLQXP1wYZlgeP8lL-lECtTye-_EgrBkF_Xm7Ccou4pADHW2nuPlfdAm6a6lSgTWlKw0V2LXAC5rHkUC71digK6Dg8ArVcA2_gIxeShDRjnmTMTixVg4cg','2020-05-06 00:00:00','2021-05-06 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$204ffbce-a3b6-41bd-be7d-5f97cf1e85d2x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0y@�xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIyMDRmZmJjZS1hM2I2LTQxYmQtYmU3ZC01Zjk3Y2YxZTg1ZDIiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg4Nzc4NzkzLCJqdGkiOiJjMDFiODg4Ny1lZTYwLTRjNjAtOTI0YS1jYmZjZWNmODBiODUiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.UiSGs8ISzx10cKoHdwtBrUXcdd6w-Dr61FXeUERH3X6dTWeNKXgDb0PzGZd-Sc5FfsrTPUgDl73EIBbI0lGmqMht2HQpflmFBShcRJmz2L4PyzDCF3eF9_pw6um5RcC4liIEpI17fvrlh4hFgGnz7iKpn4g2n-OwEduEoH31PX7Dl7UmyEB_CI2NgaV5-7tTLeNZABT3fWi6LETPwvjXvUsTwPuG25y9aRLQXP1wYZlgeP8lL-lECtTye-_EgrBkF_Xm7Ccou4pADHW2nuPlfdAm6a6lSgTWlKw0V2LXAC5rHkUC71digK6Dg8ArVcA2_gIxeShDRjnmTMTixVg4cgsq\0~\0w\0\0q�z�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MjAyODQ3OTMsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjIwNGZmYmNlLWEzYjYtNDFiZC1iZTdkLTVmOTdjZjFlODVkMiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.MPcoWlhtlXcXYVnzKDI5ncnKj_IbzO1VI86OYeSzp2oAiii1o_fRPBIOD4uhepEZrkZZzl6gVwQ5bAtOzwzb0N9F_tTWsZx2ZRRbKi_i-Xzie-BBjwHM69Fh4W0dJuVZTmmY1fQTSwum5UQmQt2YrzGm-TzDRyq57u_sy-9-fiEAwRBqvfz_13dCatg4dJmDgEsDBqDsN84WnEMNzYTgQIxgoAxuWHn7fRkj_H6FaucsTroBBiHVSzr5l2xOekbOBS0g73r3T2hzEyByM_yhYtEFLYZHnLwny-Gbp1tE9VvdnYrlfB1-i1QvFfyYUN0Q4awIGD_1uMV34CCpMMkc-A',NULL,'3110201400000'),
('33c03ac4-4411-11ea-89f0-97f5ebab6398','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyMDAxMTI2LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjQ3NmVhODg3LWNkMWYtNDBlNC1hZjBmLTNiNjcxN2E0MTE3MSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.BTLy30xPl2QUrH0wPNy83UMvrFHXDezRFdxWI93x3luusB3BVob_EBV6OD_3AYV3zHksk4ScddYlWzKWM9vTlyDiHBuLutIsQRSgQsWAjxq5V5-Dxsr3kX71DVv6UBQ_KIR-vy-VEG0pLzYF0mjWPnqfXWtJ_yLP0heLP9OvSQXAQtuKdpmxgvgGD4QAIgO87_l_vyAwHvLuxHhhfIr9rrGJLOfHpexhqGkQxVcuRwBHGvkTzsyExlnZ-wIdrAzePaNvoJbrAuDDqlloq9EmsOCTqcRgKH0X-niIlJb2i6ezZs98otzyiLSb7JZj6aYPJakQfE2UUGqDzYHdcSMnyg','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI0NzZlYTg4Ny1jZDFmLTQwZTQtYWYwZi0zYjY3MTdhNDExNzEiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwNDk1MTI2LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImE2ZTQ5ZTJjLTIzNjMtNDgxYy1iNTAzLTQ4Njg3ZGU0MDVlMCIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.fmTfrE78v5i10M00BHpvu_r5Q3bQrOIQ42oGnFxwGF3CaVkguHVezMHIHboUlSIw8laopa_jnDCqkXGfDqKF5l1m7lqeutFcA59iu1a5YV2AfdgsWukw7gKl10KKDqp2ay384LQ2sVmFUgzuAQjQFegkcz2JUxsxedLJCAQ1d7JT-98CNmFLVZzGxdaQQu2osLbS3IW5Om3fFabINDoiV2gvaH6zQnJ4PfIQQwsjSJAmonjQOxfud7nDU4wsnMCbG3FLOHmR_e2KPAI3t5xoEL_GPbKyC2VRfs_D9sf7Svf0zzBad1_3MtsKy4EePbSSILtiESRU2bFNNNgAM2xjmw','2020-01-31 00:00:00','2021-01-30 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$476ea887-cd1f-40e4-af0f-3b6717a41171x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0wR�&�xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptbeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI0NzZlYTg4Ny1jZDFmLTQwZTQtYWYwZi0zYjY3MTdhNDExNzEiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwNDk1MTI2LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImE2ZTQ5ZTJjLTIzNjMtNDgxYy1iNTAzLTQ4Njg3ZGU0MDVlMCIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.fmTfrE78v5i10M00BHpvu_r5Q3bQrOIQ42oGnFxwGF3CaVkguHVezMHIHboUlSIw8laopa_jnDCqkXGfDqKF5l1m7lqeutFcA59iu1a5YV2AfdgsWukw7gKl10KKDqp2ay384LQ2sVmFUgzuAQjQFegkcz2JUxsxedLJCAQ1d7JT-98CNmFLVZzGxdaQQu2osLbS3IW5Om3fFabINDoiV2gvaH6zQnJ4PfIQQwsjSJAmonjQOxfud7nDU4wsnMCbG3FLOHmR_e2KPAI3t5xoEL_GPbKyC2VRfs_D9sf7Svf0zzBad1_3MtsKy4EePbSSILtiESRU2bFNNNgAM2xjmwsq\0~\0w\0\0o�پ_xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert&eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyMDAxMTI2LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjQ3NmVhODg3LWNkMWYtNDBlNC1hZjBmLTNiNjcxN2E0MTE3MSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.BTLy30xPl2QUrH0wPNy83UMvrFHXDezRFdxWI93x3luusB3BVob_EBV6OD_3AYV3zHksk4ScddYlWzKWM9vTlyDiHBuLutIsQRSgQsWAjxq5V5-Dxsr3kX71DVv6UBQ_KIR-vy-VEG0pLzYF0mjWPnqfXWtJ_yLP0heLP9OvSQXAQtuKdpmxgvgGD4QAIgO87_l_vyAwHvLuxHhhfIr9rrGJLOfHpexhqGkQxVcuRwBHGvkTzsyExlnZ-wIdrAzePaNvoJbrAuDDqlloq9EmsOCTqcRgKH0X-niIlJb2i6ezZs98otzyiLSb7JZj6aYPJakQfE2UUGqDzYHdcSMnyg',NULL,'3110201400000'),
('3696d161-47f3-11ea-ad0e-19a9fc721727','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI0MjgwNTAsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImIzNGVmY2VmLWU0ZWEtNDk5OS1iMTUxLWM0NWVlNTRiMzllNiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.M1BBFzsq2kspRxqm-iOW_JD-oNljjPQW7W5xtKh9cHo9tlhYJ3DOBVoaqQm5KfPbuQdnO04FikxYQiDTwCDVnzOoxq7P78gztDSfsBOTNrb1SCh2vyei7tI-Fg2zDOEYFCNJZQ15QFphwwOHS-g8bH9il6AMlHGuQAmSlrMBmTV6M_ObIRoM7yguUKYAHZvP6VlK15eF2fjB_ipCIU0NaPR_NjUq_E2e3mJYlQfMW9PIkF2HGSWp0hA1q8nAdR0vGM99NZv9BR-Y-hi8zXm9_jG9xBohfawFMa5v7WgbQJ7wWhdNWntCLv-h3whkf1D9p1T5HKjG-TxSmgu_UiC6rQ','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJiMzRlZmNlZi1lNGVhLTQ5OTktYjE1MS1jNDVlZTU0YjM5ZTYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwOTIyMDUwLCJqdGkiOiI4YzRkOTAyOC1lMjdhLTRjNzYtYmM0OC1iYjZmMzZjMzk5MWMiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.h_yBKJGKLOllVHzLO0zcdW5cvlowpnQUVA-OVIcuC486SZ4z64_MDI-I_yt8Z1QgHGvXpf-IsVYKvTE1S0TgF6n8ZwbM4XB7ms91uhNs60p91l5RoQupjypxXFaagr7JNAT-QOpokuQR5XYVpSjy4b_BX77lRQ-uXMD1pXaJ_qhlD_dOscrN9rCcdVpRaAoHQT8aOtgqd8TsFuEjtIZIT38htp5vKyOK1rg5flhPSEf5UDRd8NZXzLk6an8fnr63z8RO8WVtkQYbP_dtPMJLs_wa3BmVTAazjqY7GpV186RB7sc49OjXxRSoX8pTQED2J1M7dn_RBRf82CI46hjMTw','2020-02-05 00:00:00','2021-02-04 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$b34efcef-e4ea-4999-b151-c45ee54b39e6x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0wl3��xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJiMzRlZmNlZi1lNGVhLTQ5OTktYjE1MS1jNDVlZTU0YjM5ZTYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwOTIyMDUwLCJqdGkiOiI4YzRkOTAyOC1lMjdhLTRjNzYtYmM0OC1iYjZmMzZjMzk5MWMiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.h_yBKJGKLOllVHzLO0zcdW5cvlowpnQUVA-OVIcuC486SZ4z64_MDI-I_yt8Z1QgHGvXpf-IsVYKvTE1S0TgF6n8ZwbM4XB7ms91uhNs60p91l5RoQupjypxXFaagr7JNAT-QOpokuQR5XYVpSjy4b_BX77lRQ-uXMD1pXaJ_qhlD_dOscrN9rCcdVpRaAoHQT8aOtgqd8TsFuEjtIZIT38htp5vKyOK1rg5flhPSEf5UDRd8NZXzLk6an8fnr63z8RO8WVtkQYbP_dtPMJLs_wa3BmVTAazjqY7GpV186RB7sc49OjXxRSoX8pTQED2J1M7dn_RBRf82CI46hjMTwsq\0~\0w\0\0pLxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI0MjgwNTAsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImIzNGVmY2VmLWU0ZWEtNDk5OS1iMTUxLWM0NWVlNTRiMzllNiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.M1BBFzsq2kspRxqm-iOW_JD-oNljjPQW7W5xtKh9cHo9tlhYJ3DOBVoaqQm5KfPbuQdnO04FikxYQiDTwCDVnzOoxq7P78gztDSfsBOTNrb1SCh2vyei7tI-Fg2zDOEYFCNJZQ15QFphwwOHS-g8bH9il6AMlHGuQAmSlrMBmTV6M_ObIRoM7yguUKYAHZvP6VlK15eF2fjB_ipCIU0NaPR_NjUq_E2e3mJYlQfMW9PIkF2HGSWp0hA1q8nAdR0vGM99NZv9BR-Y-hi8zXm9_jG9xBohfawFMa5v7WgbQJ7wWhdNWntCLv-h3whkf1D9p1T5HKjG-TxSmgu_UiC6rQ',NULL,'3110201400000'),
('4e546dc5-4411-11ea-89f0-d952249239da','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyMDAxMTcwLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI2MDNlZTcyOS03YWY3LTQ0YjEtYWQxMy02ZDc5OGU2ZTgyODUiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.Rq6gWXPbFcL4hC4_V2tRtiAt_i7vfsXsJ5r_rOUslIvbNnNr2E8Eb0vPzSmlZKSeEbv5kag6AskPfWDmFQCekuUefV6ayfTCEUdim05acPT1wkuPCj-exPQhqxopczwzdxH-SEtlJdRfzRc5e8ZOrSJel2Ru2ZErGgiQxWnYCcV-qw8g3sBX2J_TPRFc-lvxf-60z2ws0pSK8fHWZPfUCXbMH_9y1Fdr2PXTdKEFn3henS76Cz5wmLZwVdRRflXIF-u_Lz9auAcLOLtEFkAIbdLiEtRoMjAhmHKLaP0kDm1POKsGDmOZUEhjRZ8kVu_jZLVT5G09Py_VaDawzOtnFw','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI2MDNlZTcyOS03YWY3LTQ0YjEtYWQxMy02ZDc5OGU2ZTgyODUiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwNDk1MTcwLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI1NzIwNzkwOC0zZGZhLTRhN2MtODRhMy1iNzQ4NzgxYjY5NWEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.dnNL8d4WAbY3bCVEHhddCxUJUXv6BgxPOvJfEG6peMrdA3f8Bfo56FONWTebussrRNr3hRaD6kbLi0ZlImcwtblfujFIpPJ7yeZo_ywxXfJOauNlLB9L8EJyEV2k3gLQ96VBR2Xog8ES-ppIwDmJCadhOr_nddl3SFU6h2j7UfiOgLJVci4BrOFaVZ0fURNQfTAZ3ldB2BX4177gzKXshd3TVKbMdTvlxfPuIYa0LdamQ4hf0cEo8vHgOuM-lT-GmjzoAim1snAR3665trsRiCmexYd6ZyFsWXg8u4CORzdXAF8O1hgV2nwoMq3SRcRIZicmQFfkat_rxCp8i2R8YQ','2020-01-31 00:00:00','2021-01-30 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$603ee729-7af7-44b1-ad13-6d798e6e8285x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0wR��xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptseyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI2MDNlZTcyOS03YWY3LTQ0YjEtYWQxMy02ZDc5OGU2ZTgyODUiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwNDk1MTcwLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI1NzIwNzkwOC0zZGZhLTRhN2MtODRhMy1iNzQ4NzgxYjY5NWEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.dnNL8d4WAbY3bCVEHhddCxUJUXv6BgxPOvJfEG6peMrdA3f8Bfo56FONWTebussrRNr3hRaD6kbLi0ZlImcwtblfujFIpPJ7yeZo_ywxXfJOauNlLB9L8EJyEV2k3gLQ96VBR2Xog8ES-ppIwDmJCadhOr_nddl3SFU6h2j7UfiOgLJVci4BrOFaVZ0fURNQfTAZ3ldB2BX4177gzKXshd3TVKbMdTvlxfPuIYa0LdamQ4hf0cEo8vHgOuM-lT-GmjzoAim1snAR3665trsRiCmexYd6ZyFsWXg8u4CORzdXAF8O1hgV2nwoMq3SRcRIZicmQFfkat_rxCp8i2R8YQsq\0~\0w\0\0o��l�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert7eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyMDAxMTcwLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI2MDNlZTcyOS03YWY3LTQ0YjEtYWQxMy02ZDc5OGU2ZTgyODUiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.Rq6gWXPbFcL4hC4_V2tRtiAt_i7vfsXsJ5r_rOUslIvbNnNr2E8Eb0vPzSmlZKSeEbv5kag6AskPfWDmFQCekuUefV6ayfTCEUdim05acPT1wkuPCj-exPQhqxopczwzdxH-SEtlJdRfzRc5e8ZOrSJel2Ru2ZErGgiQxWnYCcV-qw8g3sBX2J_TPRFc-lvxf-60z2ws0pSK8fHWZPfUCXbMH_9y1Fdr2PXTdKEFn3henS76Cz5wmLZwVdRRflXIF-u_Lz9auAcLOLtEFkAIbdLiEtRoMjAhmHKLaP0kDm1POKsGDmOZUEhjRZ8kVu_jZLVT5G09Py_VaDawzOtnFw',NULL,'3110201400000'),
('96c0d9c1-653e-11ea-a977-93b63b7357b4','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTU2NDkwMDcsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjNhYmY3NzM0LTE2YTgtNDY3OC04OTI4LTFiYmZlMGUwZTIwZSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.HgiyVc0dvBKCSct54bEFwwuQ0yguUft6VSm8DE8GPMDLeMpNQzqEgUxfMbeaW4U2WU-Uw35AszA11RjQqt-sGXjWiimgRneh3Sk4vS3nNT4TKCvwLe90YvV7K68v5-8DnZ6S7caF17twGWTkSoSelGUv_aHaDdxVJKlT-on6SK2YTs9ow-B8Y631UjrUYMsRMEQK2wztJrrDAHVkUutwf9aQPurbaPYijV96zJHsIR4fhse8cYdDuv9W9EFdbYLfW7ddClcGfQaEd6xzksS9QPNGi-0Z_qybnIpvGFcWAlJJlmePhUbsxIZL_DWF0giAvy3NaewICnEpkrgX3CAEhg','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIzYWJmNzczNC0xNmE4LTQ2NzgtODkyOC0xYmJmZTBlMGUyMGUiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg0MTQzMDA3LCJqdGkiOiJmMmY2NmQzMi01M2NmLTRjZTMtOGY4MS1jMWMyMGMzMGVjODMiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.QOV_7oQQ4A_7Lwt3IWcJ2NJdYx5i5_UtATyq02K4CLSPVxtZ68bEZ4NTh0XBl7Jcoj0husxXrnAFMv6EszHULsTZonst9TsvqcRCNk4AS0VTalrED49pphR0dC_vcWRQkKY5FGZB1GGOVmaDCo4_W764LdSQMLkTxICKD6Wg6G926a9hhyydPG6g3-StImAWiYe5G-yQ-TOQ-amrY5lbPtBzPQVE4vLJX9XFm1ebWMMtADs9z4rQraCOMRJJ0dP5NRv27hSco5fcbt16NMkTvsxaJfaRQsjasB8TnvWEocSC08plQYAGy6pC_Qyo76H3SumeFuT13rfX9g7QepX5LQ','2020-03-13 00:00:00','2021-03-13 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$3abf7734-16a8-4678-8928-1bbfe0e0e20ex\0sr\0java.util.Datehj�KYt\0\0xpw\0\0x,/ixsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIzYWJmNzczNC0xNmE4LTQ2NzgtODkyOC0xYmJmZTBlMGUyMGUiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg0MTQzMDA3LCJqdGkiOiJmMmY2NmQzMi01M2NmLTRjZTMtOGY4MS1jMWMyMGMzMGVjODMiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.QOV_7oQQ4A_7Lwt3IWcJ2NJdYx5i5_UtATyq02K4CLSPVxtZ68bEZ4NTh0XBl7Jcoj0husxXrnAFMv6EszHULsTZonst9TsvqcRCNk4AS0VTalrED49pphR0dC_vcWRQkKY5FGZB1GGOVmaDCo4_W764LdSQMLkTxICKD6Wg6G926a9hhyydPG6g3-StImAWiYe5G-yQ-TOQ-amrY5lbPtBzPQVE4vLJX9XFm1ebWMMtADs9z4rQraCOMRJJ0dP5NRv27hSco5fcbt16NMkTvsxaJfaRQsjasB8TnvWEocSC08plQYAGy6pC_Qyo76H3SumeFuT13rfX9g7QepX5LQsq\0~\0w\0\0p�H\0�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTU2NDkwMDcsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjNhYmY3NzM0LTE2YTgtNDY3OC04OTI4LTFiYmZlMGUwZTIwZSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.HgiyVc0dvBKCSct54bEFwwuQ0yguUft6VSm8DE8GPMDLeMpNQzqEgUxfMbeaW4U2WU-Uw35AszA11RjQqt-sGXjWiimgRneh3Sk4vS3nNT4TKCvwLe90YvV7K68v5-8DnZ6S7caF17twGWTkSoSelGUv_aHaDdxVJKlT-on6SK2YTs9ow-B8Y631UjrUYMsRMEQK2wztJrrDAHVkUutwf9aQPurbaPYijV96zJHsIR4fhse8cYdDuv9W9EFdbYLfW7ddClcGfQaEd6xzksS9QPNGi-0Z_qybnIpvGFcWAlJJlmePhUbsxIZL_DWF0giAvy3NaewICnEpkrgX3CAEhg',NULL,'3110201400000'),
('a8a9f716-71e2-11ea-b07e-13838dfd64d4','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjE3MDM4ODg5LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiIyMDdlNmZiMS1hYjNjLTQ3Y2UtYmVhZi1lY2Y1YmYwMTZkM2EiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.RtZkAbdKvROf_JsMg9Hz5ykYPO3bvkoKPL1_L8F0uXDhQDGqQ7dt57WGiWFx7rzd3kbSxy4HkGpvgx60toLWzHDqvmQ-4uSgnKRjReZQ2eGQnLmhatvz98vgMGEji_d5It5Bw3DAoed-ZNcPqcGnd-aW1pU0t9kR2IKI43lDHNcj-sdgyjI9Eor75NyNiPrmiCjyUzrsg0jW7icYphh36yXcCN7laPWtFHYJQiVBxXzYijPK3Qwa7blfmCAc-Xl4ygo0KWjpyz5WmnSOFyt0ZodWzPyc8aYC6Gz5Hz6P1w0UVOpSpIC0Yxi7trfUCHrlPwZ4huaZ3CkW352ZBZ9oKA','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiIyMDdlNmZiMS1hYjNjLTQ3Y2UtYmVhZi1lY2Y1YmYwMTZkM2EiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg1NTMyODg5LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiJiNmMwZDQyNC0xMzFkLTQ2YWQtODI4My01MTE2YzBjNzAyNzQiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.MvslOoUeHnSetPax1pCAYjtKRXkGZDPqyRt6gqtki6US9MZmsjeGO0EtMfrUujgpCzTpv7QmmB0tVDFpZvkfuHy20ec9o46Ap07vS2twdNsmUvebeUc-AtjVmS40cf97TiaDKOe87Qs8O8Cz1YRh5n8yn4l3WFYZJPbemY_hnuZDLwt0S9n1UoKa9WzDpZxi8Clp2GAOIKd2deMyIjptW4YA7jp-_YGLXWGr8EvFHDy5AKM4UI-lc5OUAvzzkx4MRwskijSJ6qoWG51vlc3oOjjI9ILaitpn5Xkr7UnEdvoaQvkO4Ar01VDJq_k0YbtZXf6IQ05-ba7JZgS0gudmcw','2020-03-30 00:00:00','2021-03-30 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$207e6fb1-ab3c-47ce-beaf-ecf5bf016d3ax\0sr\0java.util.Datehj�KYt\0\0xpw\0\0xQ�xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptseyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiIyMDdlNmZiMS1hYjNjLTQ3Y2UtYmVhZi1lY2Y1YmYwMTZkM2EiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg1NTMyODg5LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiJiNmMwZDQyNC0xMzFkLTQ2YWQtODI4My01MTE2YzBjNzAyNzQiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.MvslOoUeHnSetPax1pCAYjtKRXkGZDPqyRt6gqtki6US9MZmsjeGO0EtMfrUujgpCzTpv7QmmB0tVDFpZvkfuHy20ec9o46Ap07vS2twdNsmUvebeUc-AtjVmS40cf97TiaDKOe87Qs8O8Cz1YRh5n8yn4l3WFYZJPbemY_hnuZDLwt0S9n1UoKa9WzDpZxi8Clp2GAOIKd2deMyIjptW4YA7jp-_YGLXWGr8EvFHDy5AKM4UI-lc5OUAvzzkx4MRwskijSJ6qoWG51vlc3oOjjI9ILaitpn5Xkr7UnEdvoaQvkO4Ar01VDJq_k0YbtZXf6IQ05-ba7JZgS0gudmcwsq\0~\0w\0\0q)�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert7eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjE3MDM4ODg5LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiIyMDdlNmZiMS1hYjNjLTQ3Y2UtYmVhZi1lY2Y1YmYwMTZkM2EiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.RtZkAbdKvROf_JsMg9Hz5ykYPO3bvkoKPL1_L8F0uXDhQDGqQ7dt57WGiWFx7rzd3kbSxy4HkGpvgx60toLWzHDqvmQ-4uSgnKRjReZQ2eGQnLmhatvz98vgMGEji_d5It5Bw3DAoed-ZNcPqcGnd-aW1pU0t9kR2IKI43lDHNcj-sdgyjI9Eor75NyNiPrmiCjyUzrsg0jW7icYphh36yXcCN7laPWtFHYJQiVBxXzYijPK3Qwa7blfmCAc-Xl4ygo0KWjpyz5WmnSOFyt0ZodWzPyc8aYC6Gz5Hz6P1w0UVOpSpIC0Yxi7trfUCHrlPwZ4huaZ3CkW352ZBZ9oKA',NULL,'3110201400000'),
('a8dbb577-71e2-11ea-b07e-6bbca650f95a','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTcwMzg4ODksImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjhhODVhMDM0LTU1ZDUtNDA0YS05NTQ0LThjNmVmZjI3ZWRhNiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.Xkkcc-DcXS6H9JzmrcFrVHUS9ebY1TXm1dm9P7_iyg3Hd-rP7mzanHT0fS1t5v-cObJSKl3St3PrS2Y6fLQaeNRv-7P_DmWe1PTtc9LQzrBm25xQYCk6iM9dVWx6LEczeJJZUokTCkC4PbiE3QpXP8Fey1zSL_OQnTQKbXmZd5xjgvYsqlHqonziHTkDUMw7osW3S6ztbVmryEt0lj_jGuG9DM_HcBVsLNgRlobn5ji5A8SsZB5fApCDlmz9Emp-e5qnJqsiJX9e5GxaMJu-ztlpUQz0uhfMbwk_WDwe8LDXQAytEs_2zPGrvqNLLjRq0jmZ2ZVA8-zzOmU4q8BDOQ','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI4YTg1YTAzNC01NWQ1LTQwNGEtOTU0NC04YzZlZmYyN2VkYTYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg1NTMyODg5LCJqdGkiOiJiMjY4NzU3Zi1iODQ4LTRlZjctODNhNy0yOGFiMzY3ODY3MTciLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.KVXyHeVqqqYeaKwFWJAGwumpDv11tHySRIMAGVHBxYFvDKt0220qMvknNLd_UgsPAFZbEe1IIQuzmsVzhqVwqAdAtZl0lj8bLH7_vCqqgVLDyV6vzmK1U5_bxhSiaUwaqi5dBVgnRtbvy8nEo6MJVePIh-xlYHAIwfBmPQ9DtzdFtrniKAaUnAnxFjtXeLnX-DNHBjA1UEU4j8sNHjyDt5ShwO5mU0wgK0gU7XJKQ6un0FimbkNbI5VncaMtOwm-lYie_T1Eax_aAXvM3VbfYFVoqLL1yHX9P3G0OG9wW3pR7f4HAddprbqTgi2MCIJrnYpPZintvc093dbLV5y4UQ','2020-03-30 00:00:00','2021-03-30 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$8a85a034-55d5-404a-9544-8c6eff27eda6x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0xSxsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI4YTg1YTAzNC01NWQ1LTQwNGEtOTU0NC04YzZlZmYyN2VkYTYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTg1NTMyODg5LCJqdGkiOiJiMjY4NzU3Zi1iODQ4LTRlZjctODNhNy0yOGFiMzY3ODY3MTciLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.KVXyHeVqqqYeaKwFWJAGwumpDv11tHySRIMAGVHBxYFvDKt0220qMvknNLd_UgsPAFZbEe1IIQuzmsVzhqVwqAdAtZl0lj8bLH7_vCqqgVLDyV6vzmK1U5_bxhSiaUwaqi5dBVgnRtbvy8nEo6MJVePIh-xlYHAIwfBmPQ9DtzdFtrniKAaUnAnxFjtXeLnX-DNHBjA1UEU4j8sNHjyDt5ShwO5mU0wgK0gU7XJKQ6un0FimbkNbI5VncaMtOwm-lYie_T1Eax_aAXvM3VbfYFVoqLL1yHX9P3G0OG9wW3pR7f4HAddprbqTgi2MCIJrnYpPZintvc093dbLV5y4UQsq\0~\0w\0\0q)�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTcwMzg4ODksImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjhhODVhMDM0LTU1ZDUtNDA0YS05NTQ0LThjNmVmZjI3ZWRhNiIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.Xkkcc-DcXS6H9JzmrcFrVHUS9ebY1TXm1dm9P7_iyg3Hd-rP7mzanHT0fS1t5v-cObJSKl3St3PrS2Y6fLQaeNRv-7P_DmWe1PTtc9LQzrBm25xQYCk6iM9dVWx6LEczeJJZUokTCkC4PbiE3QpXP8Fey1zSL_OQnTQKbXmZd5xjgvYsqlHqonziHTkDUMw7osW3S6ztbVmryEt0lj_jGuG9DM_HcBVsLNgRlobn5ji5A8SsZB5fApCDlmz9Emp-e5qnJqsiJX9e5GxaMJu-ztlpUQz0uhfMbwk_WDwe8LDXQAytEs_2zPGrvqNLLjRq0jmZ2ZVA8-zzOmU4q8BDOQ',NULL,'3110201400000'),
('ba0f1d76-4be8-11ea-abed-c1359f01363f','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI4NjMzNTEsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImJlMzFhNmI3LTUyZjItNDRjOS1hY2NjLTE3OTBlYzkyYWY2MSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.ItkPeaZ1EyUoQwwRl1s0HVRE6RG9VEuEKkLjSPR8KvG05MdOr44_IYb_lz9-dp2PkyJwFMPNuah3mn_t7CU6-UPyp8I0KbadY8fu0SovRiqi-ZoEzoY7CcGw3fa6v8Dv1Kou6-slKz3o_-FV9Iw-c4XadFWnlsXd_sPpP3KGYpDcjmQGWl3jlkVatpu8SKl77BxtbGUa-PCLGanQSYwbnkU-cC5cUpH0eLnSt4wPsmuYZUQJEXEvYpLSQtpkvcLMP_0YruedzMZihfuz1G0S8SE_WzZpZjmf0XvSrAmYdqzOmWPJCL7K07hUaJkTIDSaJmW5j-R-4y4CLRo96HXfCA','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJiZTMxYTZiNy01MmYyLTQ0YzktYWNjYy0xNzkwZWM5MmFmNjEiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzU3MzUxLCJqdGkiOiI2MmZjMWFjOC1iZGY0LTQxYjctYTFlOC05NmM5YjE0NWNkYjYiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.fjzZIVn1cgXrnmVOYaZ2OExuxyMs3B-e56sGHH0IDLvrXuNJHUS2g1GdZWnNri3cj-weS_5-_YlC9t_D3jHzgHnywgyHpP3s8fAKecRfuJPzFHUxLekfGhRDRgQJEavLbSKW44QzB4WYnGcSDptdCtCMd1O4GYUNXdueO_WGh-9gr75MV79bDtcuDfRjq2C9M2_yE65PMza8MHeY3NDQk8aSUCrAyIR5De2gPqiOZfin4WJOQRz9AbGVJbQUGxYb8DQxbXBQb3Cg6PjHpvGCfkL1tPNUCsQ_7uvaMcH77M5KGFRCDuPxWTj8CsxnJhiPLr4ZB6dTiHC-NJcmVK0qgQ','2020-02-10 00:00:00','2021-02-09 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$be31a6b7-52f2-44c9-accc-1790ec92af61x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0w�%�Exsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiJiZTMxYTZiNy01MmYyLTQ0YzktYWNjYy0xNzkwZWM5MmFmNjEiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzU3MzUxLCJqdGkiOiI2MmZjMWFjOC1iZGY0LTQxYjctYTFlOC05NmM5YjE0NWNkYjYiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.fjzZIVn1cgXrnmVOYaZ2OExuxyMs3B-e56sGHH0IDLvrXuNJHUS2g1GdZWnNri3cj-weS_5-_YlC9t_D3jHzgHnywgyHpP3s8fAKecRfuJPzFHUxLekfGhRDRgQJEavLbSKW44QzB4WYnGcSDptdCtCMd1O4GYUNXdueO_WGh-9gr75MV79bDtcuDfRjq2C9M2_yE65PMza8MHeY3NDQk8aSUCrAyIR5De2gPqiOZfin4WJOQRz9AbGVJbQUGxYb8DQxbXBQb3Cg6PjHpvGCfkL1tPNUCsQ_7uvaMcH77M5KGFRCDuPxWTj8CsxnJhiPLr4ZB6dTiHC-NJcmVK0qgQsq\0~\0w\0\0p0>A�xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI4NjMzNTEsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImJlMzFhNmI3LTUyZjItNDRjOS1hY2NjLTE3OTBlYzkyYWY2MSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.ItkPeaZ1EyUoQwwRl1s0HVRE6RG9VEuEKkLjSPR8KvG05MdOr44_IYb_lz9-dp2PkyJwFMPNuah3mn_t7CU6-UPyp8I0KbadY8fu0SovRiqi-ZoEzoY7CcGw3fa6v8Dv1Kou6-slKz3o_-FV9Iw-c4XadFWnlsXd_sPpP3KGYpDcjmQGWl3jlkVatpu8SKl77BxtbGUa-PCLGanQSYwbnkU-cC5cUpH0eLnSt4wPsmuYZUQJEXEvYpLSQtpkvcLMP_0YruedzMZihfuz1G0S8SE_WzZpZjmf0XvSrAmYdqzOmWPJCL7K07hUaJkTIDSaJmW5j-R-4y4CLRo96HXfCA',NULL,'3110201400000'),
('c2bf766e-4bb6-11ea-837e-fb2fc4e64f2a','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyODQxODkxLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI5ZjhmZTdlMC0wNzI1LTQxY2YtYjRjZC0yMDllOGJjYmNkMDEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.R1AFbX6FFeL1TJLvxGcNwjLYMv-VOY6tk5hz8ghgHorDKlMJepTFZf9-W5NQ4dxr8k3xtD7yGfojXpFVWQERjBL1hTLA-UOdPLa5jMz0zAzMVjgpXMtXRox2u0dI5OX4CuUK3PBWj7iSb9pcdVTCg6G0CKNpwyt9_n-_t20BUgeWxKXb5WEE3T9D4DrGiC-J6Ktnzt2vFC4FzrCIBgRywFR1jSsKO88mMFWgoAmSFmcKF7lHq8ykw_ZR3NP3KaYkvyfIWqofnBs_WvuSOpbj2etfLMhQy1tS9K_iGmtmvCJ2jnihOkLSG1a29SZbg0m7Jshz4Ei0X8Bo2z7grARXzA','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI5ZjhmZTdlMC0wNzI1LTQxY2YtYjRjZC0yMDllOGJjYmNkMDEiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM1ODkxLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiIwZmY5ZGM0My0yMDVmLTQ1NDgtOWIxZS00NDIxOTNjNDU4YWYiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.KySIri8udgbzTLptNA0x7-14gqSYcgtJpvWNybHOhHeubJlKWmWhx2GdimTrQoxfHppyGMULK0l8-USYuLbFoz722d8dei7ve6ERqLGlqJQbXe3Z8NLsKURrwRR-vrRJqxnyEsXu7BE3h09MFpm4gVsIqa6NkXer0uRri5PqTsNZVtGcA86ydH4fs7XLDqgZzy0m-Q0jWrkYvsMz0ARSJWQAcGBaCB86VNsQyDih6jTHfp3ByVHnKbb0KnE4fDjD2_EOdtpEy5ItbLC1MZRda-d9iTekO0LS5kYUjov3M7pp9etVcJ1Jt8oHised1R4XuNRtG8cIwl2gdMKxq28gmA','2020-02-10 00:00:00','2021-02-09 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$9f8fe7e0-0725-41cf-b4cd-209e8bcbcd01x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0w��4�xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptseyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiI5ZjhmZTdlMC0wNzI1LTQxY2YtYjRjZC0yMDllOGJjYmNkMDEiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM1ODkxLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiIwZmY5ZGM0My0yMDVmLTQ1NDgtOWIxZS00NDIxOTNjNDU4YWYiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.KySIri8udgbzTLptNA0x7-14gqSYcgtJpvWNybHOhHeubJlKWmWhx2GdimTrQoxfHppyGMULK0l8-USYuLbFoz722d8dei7ve6ERqLGlqJQbXe3Z8NLsKURrwRR-vrRJqxnyEsXu7BE3h09MFpm4gVsIqa6NkXer0uRri5PqTsNZVtGcA86ydH4fs7XLDqgZzy0m-Q0jWrkYvsMz0ARSJWQAcGBaCB86VNsQyDih6jTHfp3ByVHnKbb0KnE4fDjD2_EOdtpEy5ItbLC1MZRda-d9iTekO0LS5kYUjov3M7pp9etVcJ1Jt8oHised1R4XuNRtG8cIwl2gdMKxq28gmAsq\0~\0w\0\0p.��wxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert7eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyODQxODkxLCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI5ZjhmZTdlMC0wNzI1LTQxY2YtYjRjZC0yMDllOGJjYmNkMDEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.R1AFbX6FFeL1TJLvxGcNwjLYMv-VOY6tk5hz8ghgHorDKlMJepTFZf9-W5NQ4dxr8k3xtD7yGfojXpFVWQERjBL1hTLA-UOdPLa5jMz0zAzMVjgpXMtXRox2u0dI5OX4CuUK3PBWj7iSb9pcdVTCg6G0CKNpwyt9_n-_t20BUgeWxKXb5WEE3T9D4DrGiC-J6Ktnzt2vFC4FzrCIBgRywFR1jSsKO88mMFWgoAmSFmcKF7lHq8ykw_ZR3NP3KaYkvyfIWqofnBs_WvuSOpbj2etfLMhQy1tS9K_iGmtmvCJ2jnihOkLSG1a29SZbg0m7Jshz4Ei0X8Bo2z7grARXzA',NULL,'3110201400000'),
('c2f0e6af-4bb6-11ea-837e-f5216734dbd4','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI4NDE4OTEsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjljYTUzNWIwLTVjNmYtNDY2NC1iYzVmLWY4ZjY4MDkwODgwNyIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.Up98xyOyqqboOyfwp924MEtxtkZIWMe2VfkKFUZd8zrCHTiePe7Cj9HJOnGorfRPVpmiz8PlD4wCJTegGg-47A-T8c429pyUhyl9dhllCqN7IDu0dpnraDlpP1hK_LVeg0lJbWMLThT7dI_jb8KpJPNG0ORBzhlm_jq0FTTSGNk0fG6zfE0PVYgVLmldFFr-SHgU_j3EmqBCOe1d-Q3TotsQXHksZRgogxs8vJIz_KvA0oqqMh9z50nUP3bXZ22pb4NefxKW_aaTsnL-aS_F1K1-ahCoBp5MdUF-hFUzQH3bLKqhLuf4lfdk_cOOEh0xEihd7X8oY1FCFogEfkUP9w','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI5Y2E1MzViMC01YzZmLTQ2NjQtYmM1Zi1mOGY2ODA5MDg4MDciLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM1ODkxLCJqdGkiOiI5MGIwYTJiMS1jMGNlLTRlMGUtYWNiOS00ZGJhM2NhZmI3NzEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.eXAcxg2j2f98ewVVNMwtnHddpGZ0qmyLeRq-tkQyh_ZCpd6G4jqYj-FSaHVVTQRzPJ5e0jJG7vdJ6jZhiNw8sjtQvytMftPVOSWBt5zlUMdYeYzmmSVWbwOkxMptBsjooR48U9C74GKNWtAa3Axmeh68jBvg3SSCeR--LojEQjmX8_b9HjBbFdzhezdwEFYdCWUnOjj5WvU94qf2F4ph3GOqSrHgOkg1TGHo-5ciBCteh9_RNudkjK4tCtIAnAcfa1gxqV2nEon8EGVX1loA3zcr0HNJoB5wLS27_KMRbE9a8lmXHmq5aE5ACxzH_D7zwKchdFox9zDbi_oKxU83uA','2020-02-10 00:00:00','2021-02-09 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$9ca535b0-5c6f-4664-bc5f-f8f680908807x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0w��6wxsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI5Y2E1MzViMC01YzZmLTQ2NjQtYmM1Zi1mOGY2ODA5MDg4MDciLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM1ODkxLCJqdGkiOiI5MGIwYTJiMS1jMGNlLTRlMGUtYWNiOS00ZGJhM2NhZmI3NzEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.eXAcxg2j2f98ewVVNMwtnHddpGZ0qmyLeRq-tkQyh_ZCpd6G4jqYj-FSaHVVTQRzPJ5e0jJG7vdJ6jZhiNw8sjtQvytMftPVOSWBt5zlUMdYeYzmmSVWbwOkxMptBsjooR48U9C74GKNWtAa3Axmeh68jBvg3SSCeR--LojEQjmX8_b9HjBbFdzhezdwEFYdCWUnOjj5WvU94qf2F4ph3GOqSrHgOkg1TGHo-5ciBCteh9_RNudkjK4tCtIAnAcfa1gxqV2nEon8EGVX1loA3zcr0HNJoB5wLS27_KMRbE9a8lmXHmq5aE5ACxzH_D7zwKchdFox9zDbi_oKxU83uAsq\0~\0w\0\0p.���xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI4NDE4OTEsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjljYTUzNWIwLTVjNmYtNDY2NC1iYzVmLWY4ZjY4MDkwODgwNyIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.Up98xyOyqqboOyfwp924MEtxtkZIWMe2VfkKFUZd8zrCHTiePe7Cj9HJOnGorfRPVpmiz8PlD4wCJTegGg-47A-T8c429pyUhyl9dhllCqN7IDu0dpnraDlpP1hK_LVeg0lJbWMLThT7dI_jb8KpJPNG0ORBzhlm_jq0FTTSGNk0fG6zfE0PVYgVLmldFFr-SHgU_j3EmqBCOe1d-Q3TotsQXHksZRgogxs8vJIz_KvA0oqqMh9z50nUP3bXZ22pb4NefxKW_aaTsnL-aS_F1K1-ahCoBp5MdUF-hFUzQH3bLKqhLuf4lfdk_cOOEh0xEihd7X8oY1FCFogEfkUP9w',NULL,'3110201400000'),
('fd141af3-4410-11ea-89f0-f7c552070465','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyMDAxMDM0LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImRlZjBkNzNlLTBhMjktNDJhMi04ZWM3LWM5YWI1YWU5YzhkOSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.BXatPsPZVFhINpdmxv2Rcn1WiamWuFmddcNBCBaB_U7RXRXqr2V8ezxMvOnS_kuC_uHQAHPw6tCpQVYULuqAE9bodxK4FEsqDCwLMe7Os72yVE68QoZnkAYsFSj3RMwRuVrQN3O6DARoJwnC7chLMGXn8T2mrLO2-xmh04UkFJhBLh1jKRZSqzawocVYx-6iZvVdEJeM1Jr24ISlaRfQsmjYFAXjW6jsICDxdTyokfvvdQQeiuChSHa8AQ8ex7eTE5L1-CzSsetJVAbzcnC3MNC_5tFWhDdqbXLzBq_gSQhakxE2QZlnx8RdlbuzL1m4-G88qGRR1Jn5AE7h9cNu4A','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiJkZWYwZDczZS0wYTI5LTQyYTItOGVjNy1jOWFiNWFlOWM4ZDkiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwNDk1MDM0LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImUxZjAyMTY0LWI2MTQtNGNmMi1iZGI2LWVjMmU1OWY2MzNjNCIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.Tlg2-UcZY5aHrlZ8AFwqmrO58UnyrU2364QwEI-jvkXE6Udp79pd89ycuW7gOKor_tchhV3GRMOGwzgpRI3SXecGlJh8_UPA-Zdf4x6DuCUoPaGgC_CCwjQdELeIxsdyiEyTq9x2qig-ZBDN97OiC6-2-uOPUEho-ouRj5hyJjNSCd3_kDbhlXug1CkSBF82XTv6ovkY-phSO52YnAfzPQVBSOQ0pm4pWXJvLNyAoUWysRHmXbQvZKnht4Np0CjBL5CMYpIAIxbmX6OyzKdAw31yUGSaGZ_5Xe3hRRFwiLuUqJGhib6Wrmm5IaAqczMIMVqqEwSb48KthJb1YD7yJg','2020-01-31 00:00:00','2021-01-30 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$def0d73e-0a29-42a2-8ec7-c9ab5ae9c8d9x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0wR���xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptbeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiJkZWYwZDczZS0wYTI5LTQyYTItOGVjNy1jOWFiNWFlOWM4ZDkiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgwNDk1MDM0LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImUxZjAyMTY0LWI2MTQtNGNmMi1iZGI2LWVjMmU1OWY2MzNjNCIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.Tlg2-UcZY5aHrlZ8AFwqmrO58UnyrU2364QwEI-jvkXE6Udp79pd89ycuW7gOKor_tchhV3GRMOGwzgpRI3SXecGlJh8_UPA-Zdf4x6DuCUoPaGgC_CCwjQdELeIxsdyiEyTq9x2qig-ZBDN97OiC6-2-uOPUEho-ouRj5hyJjNSCd3_kDbhlXug1CkSBF82XTv6ovkY-phSO52YnAfzPQVBSOQ0pm4pWXJvLNyAoUWysRHmXbQvZKnht4Np0CjBL5CMYpIAIxbmX6OyzKdAw31yUGSaGZ_5Xe3hRRFwiLuUqJGhib6Wrmm5IaAqczMIMVqqEwSb48KthJb1YD7yJgsq\0~\0w\0\0o��Xxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert&eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyMDAxMDM0LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6ImRlZjBkNzNlLTBhMjktNDJhMi04ZWM3LWM5YWI1YWU5YzhkOSIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNzby13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.BXatPsPZVFhINpdmxv2Rcn1WiamWuFmddcNBCBaB_U7RXRXqr2V8ezxMvOnS_kuC_uHQAHPw6tCpQVYULuqAE9bodxK4FEsqDCwLMe7Os72yVE68QoZnkAYsFSj3RMwRuVrQN3O6DARoJwnC7chLMGXn8T2mrLO2-xmh04UkFJhBLh1jKRZSqzawocVYx-6iZvVdEJeM1Jr24ISlaRfQsmjYFAXjW6jsICDxdTyokfvvdQQeiuChSHa8AQ8ex7eTE5L1-CzSsetJVAbzcnC3MNC_5tFWhDdqbXLzBq_gSQhakxE2QZlnx8RdlbuzL1m4-G88qGRR1Jn5AE7h9cNu4A',NULL,'3110201400000'),
('fdda1720-4bba-11ea-837e-9da6b00b5075','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyODQzNzA4LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiJmNWQzOTQyNy1mYjVhLTQwYTgtYWM0YS04NzMxZjU3YWMxYTYiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.NC-8boxJDwDIOueiWWjL8BUBnYJwNYwsGFpllUHM900CBzajEPC6Vda64DId6ovTPNhddJx5eWoMg_DQz2cuYnr5YqEEEKNK22Y8rc7sJpADp-Ew5-gQ4-SsSIJiJU-7_B8y6p2CDy37-1XSjHz47-z_yEDdVnrM5bHxWPvjAXVlAzFO0jrfxBr_7H2X4PEGkndd85dSMGQ5BMWfC1XDqLoFVkjTwm9Yk52SMmgdBQNX7v8sniCcmCDrK5-sMQKNMUqJJ_ehSynXANBub2KqxwPsDsH3IWYfd_-fwAIz50uDG9oL45-R4XkmuoRaeoe1DiBzG2jlR9TtZxAlKO5aeA','sso-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiJmNWQzOTQyNy1mYjVhLTQwYTgtYWM0YS04NzMxZjU3YWMxYTYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM3NzA4LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI0NTQ2MGVkNi1jOTMyLTQxMjYtOTFlNC03MzhjNWQ4MzA2NGQiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.hBl2AMyYT1-hi0s6dXf-CFO_H-iwU7U00lr9mpcxYU60BWI9eDdS078zJkkPRUF9hV382xHixN9bNeX_CQJSWPEaSx0FIH_VbFe0jM7ogngwggSvn4nR-2OscLM2LHEHe2T2jZRULbiqysd70Yajrs8n57EjdZsNUgBJoIh6hPVBREcpRvRHxcfeHGPdjS9OhyadcJuOCdGE8oFzkOxkf1uvEakBv59udCNYU8DMBp4opizEULn9uRFpKw3JzvNv8B0kEvErOdWGE_lUXJLEFIk98gSMmpHIBW51GmTi7XumRlGU6CEHs729pxvYpQaZCG5TwVCDaWs_rNeqqP0kGQ','2020-02-10 00:00:00','2021-02-09 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$f5d39427-fb5a-40a8-ac4a-8731f57ac1a6x\0sr\0java.util.Datehj�KYt\0\0xpw\0\0w���oxsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xptseyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJhdGkiOiJmNWQzOTQyNy1mYjVhLTQwYTgtYWM0YS04NzMxZjU3YWMxYTYiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM3NzA4LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiI0NTQ2MGVkNi1jOTMyLTQxMjYtOTFlNC03MzhjNWQ4MzA2NGQiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.hBl2AMyYT1-hi0s6dXf-CFO_H-iwU7U00lr9mpcxYU60BWI9eDdS078zJkkPRUF9hV382xHixN9bNeX_CQJSWPEaSx0FIH_VbFe0jM7ogngwggSvn4nR-2OscLM2LHEHe2T2jZRULbiqysd70Yajrs8n57EjdZsNUgBJoIh6hPVBREcpRvRHxcfeHGPdjS9OhyadcJuOCdGE8oFzkOxkf1uvEakBv59udCNYU8DMBp4opizEULn9uRFpKw3JzvNv8B0kEvErOdWGE_lUXJLEFIk98gSMmpHIBW51GmTi7XumRlGU6CEHs729pxvYpQaZCG5TwVCDaWs_rNeqqP0kGQsq\0~\0w\0\0p/��xsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0	hijr_corext\0bearert7eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMTUyMDgxOTA0MjU0NCIsImZ1bGxfbmFtZSI6IlN1cGVyIEFkbWluIiwidXNlcl9uYW1lIjoiMTUyMDgxOTA0MjU0NDo6MzExMDIwMTQwMDAwMCIsInNjb3BlIjpbImhpanJfY29yZSJdLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNjEyODQzNzA4LCJhdXRob3JpdGllcyI6WyJST0xFX1BLUFRfVVNFUiIsIlJPTEVfQUxIUF9VU0VSIiwiUk9MRV9BUFJfQURNSU4iLCJST0xFX1NBVFNETl9VU0VSIiwiUk9MRV9QVExfVVNFUiIsIlJPTEVfQURNSU4iLCJST0xFX0FQUl9PUEVSQVRPUiIsIlJPTEVfUEtSX1VTRVIiLCJST0xFX1NVUEVSX0FETUlOIiwiUk9MRV9BUFJfVVNFUiJdLCJqdGkiOiJmNWQzOTQyNy1mYjVhLTQwYTgtYWM0YS04NzMxZjU3YWMxYTYiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJjbGllbnRfaWQiOiJzc28td2ViIiwiYWNjb3VudF9hdmF0YXIiOiJodHRwOi8vbG9jYWxob3N0Ojk5MDAvc3NvL2ltYWdlcy91c2VyLnBuZz8xNTIwODE5MDQyNTQ0In0.NC-8boxJDwDIOueiWWjL8BUBnYJwNYwsGFpllUHM900CBzajEPC6Vda64DId6ovTPNhddJx5eWoMg_DQz2cuYnr5YqEEEKNK22Y8rc7sJpADp-Ew5-gQ4-SsSIJiJU-7_B8y6p2CDy37-1XSjHz47-z_yEDdVnrM5bHxWPvjAXVlAzFO0jrfxBr_7H2X4PEGkndd85dSMGQ5BMWfC1XDqLoFVkjTwm9Yk52SMmgdBQNX7v8sniCcmCDrK5-sMQKNMUqJJ_ehSynXANBub2KqxwPsDsH3IWYfd_-fwAIz50uDG9oL45-R4XkmuoRaeoe1DiBzG2jlR9TtZxAlKO5aeA',NULL,'3110201400000'),
('fdea43c1-4bba-11ea-837e-71b4470979a2','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI4NDM3MDgsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjY1NGYyOTE5LWY3OTktNDllMi1hMWEyLWNlZDI1NjI4YjcwZCIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.UMElh5btvdWvAn-sIOiU_v_VEGtw9m_YdQoIe6FTlwvolXHDr8tTS8ibIpxAfLLP7u_fi7dJdf4bXL4tv1yo7P9EtyacuZLeAbd6nHsKd-Tu5hhqefjbcurPc4PlIMMFgf4Rgzt6nHSs3cRBpHqYM1iauOU_9SwpitMQcVy7AY-5OycebSq1ndF9-Qi3xfThgSQ9CkvAlnTOxhEqgMt_MMqTVDQ2wj0gntiSmr_2iPXbAK4LolPjOm0PCE4awvg-kZskF-HBxOMAxfyxtPVq1bu3NBDEfrDriIvLa2A6IZTet4BmV432WYBFzLh1dlzV2QsSwmg58ejdpeuGSlqTHw','sistem-web','1520819042544::3110201400000','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI2NTRmMjkxOS1mNzk5LTQ5ZTItYTFhMi1jZWQyNTYyOGI3MGQiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM3NzA4LCJqdGkiOiI0ODI5OTBmMC03MDFmLTRmMzctYmZmOS03OTc2MzcwMGRlMGEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.IfscOUcZWgYPfEuJNg4njrWxchqFVoKmthlMdDVLPVHOjamp8_ok-jhyKkdYW9Gjm26YfJgHnYQgMdPysJO8s1IF_qwWtO0O71LOWmfU1yO3AMxLpnGIsb0EaLLfIa4rS5um3Rk8DYOfjE23KXMcCmlUwWP_KskIhUHP88VWGsDKme8xHmcmUo7S63HvfjwO1P9EpOOMHxz9TnooiP5AultJ1sFqMXzxoEg587D55YFDiJJE4TzoINZmoMIsnCt9umfx50ddQL7Cmovr5LJwktaMNHqdogzYOzQa973s7uV6wrE9jBPTLg8efvQRoG5EK6ktl2pky04KJGBB9NUUQQ','2020-02-10 00:00:00','2021-02-09 00:00:00','��\0sr\0Corg.springframework.security.oauth2.common.DefaultOAuth2AccessToken��6$��\0L\0additionalInformationt\0Ljava/util/Map;L\0\nexpirationt\0Ljava/util/Date;L\0refreshTokent\0?Lorg/springframework/security/oauth2/common/OAuth2RefreshToken;L\0scopet\0Ljava/util/Set;L\0	tokenTypet\0Ljava/lang/String;L\0valueq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\naccount_idt\0\r1520819042544t\0	full_namet\0Super Admint\0	source_idt\0\r3110201400000t\0source_nameq\0~\0t\0account_avatart\07http://localhost:9900/sso/images/user.png?1520819042544t\0jtit\0$654f2919-f799-49e2-a1a2-ced25628b70dx\0sr\0java.util.Datehj�KYt\0\0xpw\0\0w����xsr\0Lorg.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken/�Gc��ɷ\0L\0\nexpirationq\0~\0xr\0Dorg.springframework.security.oauth2.common.DefaultOAuth2RefreshTokens�\ncT�^\0L\0valueq\0~\0xpt�eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9QS1BUX1VTRVIiLCJST0xFX0FMSFBfVVNFUiIsIlJPTEVfQVBSX0FETUlOIiwiUk9MRV9TQVRTRE5fVVNFUiIsIlJPTEVfUFRMX1VTRVIiLCJST0xFX0FETUlOIiwiUk9MRV9BUFJfT1BFUkFUT1IiLCJST0xFX1BLUl9VU0VSIiwiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfQVBSX1VTRVIiXSwiY2xpZW50X2lkIjoic2lzdGVtLXdlYiIsImF1ZCI6WyJoaWpyX2NvcmUiXSwiYWNjb3VudF9pZCI6IjE1MjA4MTkwNDI1NDQiLCJmdWxsX25hbWUiOiJTdXBlciBBZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI2NTRmMjkxOS1mNzk5LTQ5ZTItYTFhMi1jZWQyNTYyOGI3MGQiLCJzb3VyY2VfaWQiOiIzMTEwMjAxNDAwMDAwIiwiZXhwIjoxNTgxMzM3NzA4LCJqdGkiOiI0ODI5OTBmMC03MDFmLTRmMzctYmZmOS03OTc2MzcwMGRlMGEiLCJzb3VyY2VfbmFtZSI6IjMxMTAyMDE0MDAwMDAiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.IfscOUcZWgYPfEuJNg4njrWxchqFVoKmthlMdDVLPVHOjamp8_ok-jhyKkdYW9Gjm26YfJgHnYQgMdPysJO8s1IF_qwWtO0O71LOWmfU1yO3AMxLpnGIsb0EaLLfIa4rS5um3Rk8DYOfjE23KXMcCmlUwWP_KskIhUHP88VWGsDKme8xHmcmUo7S63HvfjwO1P9EpOOMHxz9TnooiP5AultJ1sFqMXzxoEg587D55YFDiJJE4TzoINZmoMIsnCt9umfx50ddQL7Cmovr5LJwktaMNHqdogzYOzQa973s7uV6wrE9jBPTLg8efvQRoG5EK6ktl2pky04KJGBB9NUUQQsq\0~\0w\0\0p/�Yxsr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xr\0,java.util.Collections$UnmodifiableCollectionB\0��^�\0L\0ct\0Ljava/util/Collection;xpsr\0java.util.LinkedHashSet�l�Z��*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0t\0readt\0writext\0bearertZeyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiaGlqcl9jb3JlIl0sImFjY291bnRfaWQiOiIxNTIwODE5MDQyNTQ0IiwiZnVsbF9uYW1lIjoiU3VwZXIgQWRtaW4iLCJ1c2VyX25hbWUiOiIxNTIwODE5MDQyNTQ0OjozMTEwMjAxNDAwMDAwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sInNvdXJjZV9pZCI6IjMxMTAyMDE0MDAwMDAiLCJleHAiOjE2MTI4NDM3MDgsImF1dGhvcml0aWVzIjpbIlJPTEVfUEtQVF9VU0VSIiwiUk9MRV9BTEhQX1VTRVIiLCJST0xFX0FQUl9BRE1JTiIsIlJPTEVfU0FUU0ROX1VTRVIiLCJST0xFX1BUTF9VU0VSIiwiUk9MRV9BRE1JTiIsIlJPTEVfQVBSX09QRVJBVE9SIiwiUk9MRV9QS1JfVVNFUiIsIlJPTEVfU1VQRVJfQURNSU4iLCJST0xFX0FQUl9VU0VSIl0sImp0aSI6IjY1NGYyOTE5LWY3OTktNDllMi1hMWEyLWNlZDI1NjI4YjcwZCIsInNvdXJjZV9uYW1lIjoiMzExMDIwMTQwMDAwMCIsImNsaWVudF9pZCI6InNpc3RlbS13ZWIiLCJhY2NvdW50X2F2YXRhciI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTkwMC9zc28vaW1hZ2VzL3VzZXIucG5nPzE1MjA4MTkwNDI1NDQifQ.UMElh5btvdWvAn-sIOiU_v_VEGtw9m_YdQoIe6FTlwvolXHDr8tTS8ibIpxAfLLP7u_fi7dJdf4bXL4tv1yo7P9EtyacuZLeAbd6nHsKd-Tu5hhqefjbcurPc4PlIMMFgf4Rgzt6nHSs3cRBpHqYM1iauOU_9SwpitMQcVy7AY-5OycebSq1ndF9-Qi3xfThgSQ9CkvAlnTOxhEqgMt_MMqTVDQ2wj0gntiSmr_2iPXbAK4LolPjOm0PCE4awvg-kZskF-HBxOMAxfyxtPVq1bu3NBDEfrDriIvLa2A6IZTet4BmV432WYBFzLh1dlzV2QsSwmg58ejdpeuGSlqTHw',NULL,'3110201400000');

/*Table structure for table `hijr_login_archive` */

DROP TABLE IF EXISTS `hijr_login_archive`;

CREATE TABLE `hijr_login_archive` (
  `id_login` varchar(50) NOT NULL DEFAULT '',
  `access_token_login` text NOT NULL,
  `client_id_login` varchar(50) NOT NULL DEFAULT '',
  `username_login` varchar(50) NOT NULL DEFAULT '',
  `refresh_token_login` text NOT NULL,
  `created_time_login` datetime NOT NULL,
  `expire_time_login` datetime NOT NULL,
  `token_object_login` blob NOT NULL,
  `account_id_login` varchar(50) DEFAULT NULL,
  `source_id_login` varchar(50) DEFAULT NULL,
  `status_login` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_login_archive` */

/*Table structure for table `hijr_permission` */

DROP TABLE IF EXISTS `hijr_permission`;

CREATE TABLE `hijr_permission` (
  `id_permission` varchar(50) NOT NULL DEFAULT '',
  `entity_permission` varchar(50) DEFAULT NULL,
  `fields_permission` varchar(1000) DEFAULT NULL,
  `mode_permission` varchar(20) DEFAULT NULL,
  `role_permission` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_permission`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_permission` */

insert  into `hijr_permission`(`id_permission`,`entity_permission`,`fields_permission`,`mode_permission`,`role_permission`) values 
('001','pegawai','nama,jenis_kelamin,alamat_rumah,propinsi','WRITE','ROLE_USER'),
('002','pegawai','','DELETE','ROLE_USER'),
('003','akun_bank','','DELETE','ROLE_USER'),
('004','anggota_keluarga','','DELETE','ROLE_USER'),
('101','akun_bank','*','WRITE','ROLE_USER'),
('102','anggota_keluarga','nama,nomor_induk,status,tanggal_lahir,status_verifikasi=DRAFT,status_verifikasi=TEST','WRITE','ROLE_USER'),
('201','pegawai','upah_pokok','HIDDEN','ROLE_USER'),
('202','jabatan','upah_minimum,upah_maksimum','HIDDEN','*'),
('302','anggota_keluarga','status_verifikasi=VERIFIED','WRITE','ROLE_USERS'),
('401','pegawai','status_mutasi_jabatan=APPROVED','WRITE','ROLE_USERS'),
('501','pegawai','nama,jenis_kelamin,alamat_rumah,propinsi','WRITE','ROLE_ME');

/*Table structure for table `hijr_source` */

DROP TABLE IF EXISTS `hijr_source`;

CREATE TABLE `hijr_source` (
  `id_source` varchar(50) NOT NULL DEFAULT '',
  `name_source` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_source`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hijr_source` */

insert  into `hijr_source`(`id_source`,`name_source`) values 
('3110201400000','PT Hijr Global Solution'),
('3110201400001','Kementerian Lingkungan Hidup dan Kehutanan');

/*Table structure for table `oauth_client_details` */

DROP TABLE IF EXISTS `oauth_client_details`;

CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_client_details` */

insert  into `oauth_client_details`(`client_id`,`resource_ids`,`client_secret`,`scope`,`authorized_grant_types`,`web_server_redirect_uri`,`authorities`,`access_token_validity`,`refresh_token_validity`,`additional_information`,`autoapprove`) values 
('sistem-web','hijr_core','c5fa05675126f5a343a69a1657ff0e15','read,write','authorization_code,password,refresh_token','','ROLE_USER',31536000,30000,'{}','read,write'),
('sso-web','hijr_core','c5fa05675126f5a343a69a1657ff0e15','read,write','authorization_code,password,refresh_token','','ROLE_USER',31536000,30000,'{}','read,write');

/*Table structure for table `oauth_code` */

DROP TABLE IF EXISTS `oauth_code`;

CREATE TABLE `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_code` */

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(50) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `persistent_logins` */

/*Table structure for table `tbl_akun_bank` */

DROP TABLE IF EXISTS `tbl_akun_bank`;

CREATE TABLE `tbl_akun_bank` (
  `id_akun_bank` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pegawai_akun_bank` bigint(20) DEFAULT NULL,
  `nama_bank_akun_bank` varchar(100) DEFAULT NULL,
  `nomor_rekening_akun_bank` varchar(50) DEFAULT NULL,
  `id_source_akun_bank` varchar(50) DEFAULT NULL,
  `id_account_added_akun_bank` varchar(50) DEFAULT NULL,
  `timestamp_added_akun_bank` datetime DEFAULT NULL,
  `id_account_modified_akun_bank` varchar(50) DEFAULT NULL,
  `timestamp_modified_akun_bank` datetime DEFAULT NULL,
  PRIMARY KEY (`id_akun_bank`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_akun_bank` */

insert  into `tbl_akun_bank`(`id_akun_bank`,`id_pegawai_akun_bank`,`nama_bank_akun_bank`,`nomor_rekening_akun_bank`,`id_source_akun_bank`,`id_account_added_akun_bank`,`timestamp_added_akun_bank`,`id_account_modified_akun_bank`,`timestamp_modified_akun_bank`) values 
(1,2,'Bank Mandiri','11609283091823',NULL,NULL,NULL,NULL,NULL),
(2,2,'BCA','0092809381',NULL,NULL,NULL,NULL,NULL),
(3,3,'Muamalat','109820983232',NULL,NULL,NULL,NULL,NULL),
(6,7,'CIMB Niaga','1139809280398000','3110201400000','1520819042544','2020-01-15 10:21:20',NULL,NULL),
(26,27,'CIMB Niaga','1139809280398000','3110201400000','1520819042544','2020-01-16 21:53:58',NULL,NULL),
(27,28,'CIMB Niaga','1139809280398000','3110201400000','1520819042544','2020-01-29 12:09:48',NULL,NULL),
(57,53,'Bank Mandiri','11609283091823','3110201400000','1520819042544','2020-02-13 21:15:49',NULL,NULL),
(58,53,'Muamalat','109820983232','3110201400000','1520819042544','2020-02-13 21:15:49',NULL,NULL),
(63,59,'BRI','3252525','3110201400000','1520819042544','2020-02-18 22:05:21',NULL,NULL),
(64,57,'BSM','724117141','3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(65,57,'BNI','5467233525','3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(66,58,'MANDIRI','4412414141241','3110201400000','1520819042544','2020-02-18 23:38:17',NULL,NULL);

/*Table structure for table `tbl_anggota_keluarga` */

DROP TABLE IF EXISTS `tbl_anggota_keluarga`;

CREATE TABLE `tbl_anggota_keluarga` (
  `id_anggota_keluarga` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pegawai_anggota_keluarga` bigint(20) DEFAULT NULL,
  `nomor_induk_anggota_keluarga` varchar(50) DEFAULT NULL,
  `nama_anggota_keluarga` varchar(200) DEFAULT NULL,
  `status_anggota_keluarga` varchar(20) DEFAULT NULL,
  `tanggal_lahir_anggota_keluarga` date DEFAULT NULL,
  `status_verifikasi_anggota_keluarga` varchar(50) DEFAULT NULL,
  `id_source_anggota_keluarga` varchar(50) DEFAULT NULL,
  `id_account_added_anggota_keluarga` varchar(50) DEFAULT NULL,
  `timestamp_added_anggota_keluarga` datetime DEFAULT NULL,
  `id_account_modified_anggota_keluarga` varchar(50) DEFAULT NULL,
  `timestamp_modified_anggota_keluarga` datetime DEFAULT NULL,
  PRIMARY KEY (`id_anggota_keluarga`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_anggota_keluarga` */

insert  into `tbl_anggota_keluarga`(`id_anggota_keluarga`,`id_pegawai_anggota_keluarga`,`nomor_induk_anggota_keluarga`,`nama_anggota_keluarga`,`status_anggota_keluarga`,`tanggal_lahir_anggota_keluarga`,`status_verifikasi_anggota_keluarga`,`id_source_anggota_keluarga`,`id_account_added_anggota_keluarga`,`timestamp_added_anggota_keluarga`,`id_account_modified_anggota_keluarga`,`timestamp_modified_anggota_keluarga`) values 
(1,1,NULL,'Siti Aminah','Istri','1983-06-23',NULL,NULL,NULL,NULL,NULL,NULL),
(2,2,NULL,'Sarah','Istri','1978-12-17',NULL,NULL,NULL,NULL,NULL,NULL),
(3,2,NULL,'Fulan','Anak','1992-04-18',NULL,NULL,NULL,NULL,NULL,NULL),
(8,3,NULL,'Yati','Istri',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(9,3,NULL,'Budi','Anak',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,7,NULL,'Yuli','Istri',NULL,NULL,'3110201400000','1520819042544','2020-01-15 10:21:20',NULL,NULL),
(31,27,'1390009289302981092','Yuli','Istri','1984-04-15','VERIFIED','3110201400000','1520819042544','2020-01-16 21:53:58',NULL,NULL),
(32,28,'1390009289302981092','Yuli','Istri','1984-04-15','VERIFIED','3110201400000','1520819042544','2020-01-29 12:09:48',NULL,NULL),
(63,53,'141241241242','Otto','Istri','1983-06-23','VERIFIED','3110201400000','1520819042544','2020-02-13 21:15:49',NULL,NULL),
(67,57,'4874284812','Abdul Hajat','Adik','2005-06-23','NOT VERIFIED','3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(68,57,'8294928424','Muhammad Anif','Adik','2009-06-23','VERIFIED','3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL);

/*Table structure for table `tbl_beban_perusahaan` */

DROP TABLE IF EXISTS `tbl_beban_perusahaan`;

CREATE TABLE `tbl_beban_perusahaan` (
  `id_beban_perusahaan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pegawai_beban_perusahaan` bigint(20) DEFAULT NULL,
  `komponen_beban_perusahaan` varchar(200) DEFAULT NULL,
  `biaya_beban_perusahaan` bigint(20) DEFAULT NULL,
  `id_source_beban_perusahaan` varchar(50) DEFAULT NULL,
  `id_account_added_beban_perusahaan` varchar(50) DEFAULT NULL,
  `timestamp_added_beban_perusahaan` datetime DEFAULT NULL,
  `id_account_modified_beban_perusahaan` varchar(50) DEFAULT NULL,
  `timestamp_modified_beban_perusahaan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_beban_perusahaan`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_beban_perusahaan` */

insert  into `tbl_beban_perusahaan`(`id_beban_perusahaan`,`id_pegawai_beban_perusahaan`,`komponen_beban_perusahaan`,`biaya_beban_perusahaan`,`id_source_beban_perusahaan`,`id_account_added_beban_perusahaan`,`timestamp_added_beban_perusahaan`,`id_account_modified_beban_perusahaan`,`timestamp_modified_beban_perusahaan`) values 
(1,57,'BPJS Kesehatan',192139,'3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(2,57,'Jaminan Hari Tua(JHT)',125400,'3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(3,57,'Jaminan Kematian(JKM)',6600,'3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(4,57,'Jaminan Kecelakaan Kerja(JKK)',5280,'3110201400000','1520819042544','2020-02-18 23:17:38',NULL,NULL),
(5,58,'BPJS Kesehatan',250000,'3110201400000','1520819042544','2020-02-18 23:38:17',NULL,NULL),
(6,58,'Jaminan Hari Tua(JHT)',150000,'3110201400000','1520819042544','2020-02-18 23:38:17',NULL,NULL),
(7,58,'Jaminan Kematian(JKM)',10000,'3110201400000','1520819042544','2020-02-18 23:38:17',NULL,NULL),
(8,58,'Jaminan Kecelakaan Kerja(JKK)',20000,'3110201400000','1520819042544','2020-02-18 23:38:17',NULL,NULL);

/*Table structure for table `tbl_cuti_pegawai` */

DROP TABLE IF EXISTS `tbl_cuti_pegawai`;

CREATE TABLE `tbl_cuti_pegawai` (
  `id_cuti_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai_cuti_pegawai` int(11) DEFAULT NULL,
  `id_jenis_cuti_pegawai` int(11) DEFAULT NULL,
  `tanggal_mulai_cuti_pegawai` date DEFAULT NULL,
  `tanggal_selesai_cuti_pegawai` date DEFAULT NULL,
  `jumlah_cuti_pegawai` int(11) DEFAULT NULL,
  `keterangan_cuti_pegawai` text,
  `status_cuti_pegawai` varchar(100) DEFAULT NULL,
  `komentar_cuti_pegawai` varchar(500) DEFAULT NULL COMMENT 'will write/response from admin',
  `id_source_cuti_pegawai` varchar(50) DEFAULT NULL,
  `id_account_added_cuti_pegawai` varchar(50) DEFAULT NULL,
  `timestamp_added_cuti_pegawai` datetime DEFAULT NULL,
  `id_account_modified_cuti_pegawai` varchar(50) DEFAULT NULL,
  `timestamp_modified_cuti_pegawai` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cuti_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cuti_pegawai` */

/*Table structure for table `tbl_cuti_pengaturan` */

DROP TABLE IF EXISTS `tbl_cuti_pengaturan`;

CREATE TABLE `tbl_cuti_pengaturan` (
  `id_cuti_pengaturan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_cuti_pengaturan` varchar(100) DEFAULT NULL,
  `jumlah_cuti_pengaturan` int(11) DEFAULT NULL,
  `id_source_cuti_pengaturan` varchar(50) DEFAULT NULL,
  `id_account_added_cuti_pengaturan` varchar(50) DEFAULT NULL,
  `timestamp_added_cuti_pengaturan` datetime DEFAULT NULL,
  `id_account_modified_cuti_pengaturan` varchar(50) DEFAULT NULL,
  `timestamp_modified_cuti_pengaturan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cuti_pengaturan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cuti_pengaturan` */

insert  into `tbl_cuti_pengaturan`(`id_cuti_pengaturan`,`nama_cuti_pengaturan`,`jumlah_cuti_pengaturan`,`id_source_cuti_pengaturan`,`id_account_added_cuti_pengaturan`,`timestamp_added_cuti_pengaturan`,`id_account_modified_cuti_pengaturan`,`timestamp_modified_cuti_pengaturan`) values 
(1,'10 hari terakhir ramadhan',10,'3110201400000',NULL,NULL,NULL,NULL),
(2,'Keluarga Terdekat Meninggal',2,'3110201400000',NULL,NULL,NULL,NULL),
(3,'Cuti Tahunan',12,'3110201400000',NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_izin_pegawai` */

DROP TABLE IF EXISTS `tbl_izin_pegawai`;

CREATE TABLE `tbl_izin_pegawai` (
  `id_izin_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai_izin_pegawai` int(11) DEFAULT NULL,
  `jenis_izin_pegawai` varchar(100) DEFAULT NULL,
  `tanggal_mulai_izin_pegawai` date DEFAULT NULL,
  `tanggal_selesai_izin_pegawai` date DEFAULT NULL,
  `jumlah_izin_pegawai` int(11) DEFAULT NULL,
  `keterangan_izin_pegawai` text,
  `status_izin_pegawai` varchar(100) DEFAULT NULL,
  `komentar_izin_pegawai` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_izin_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_izin_pegawai` */

/*Table structure for table `tbl_izin_pegawai_ganti_hari` */

DROP TABLE IF EXISTS `tbl_izin_pegawai_ganti_hari`;

CREATE TABLE `tbl_izin_pegawai_ganti_hari` (
  `id_izin_pegawai_ganti_hari` int(11) NOT NULL AUTO_INCREMENT,
  `id_izin_pegawai_izin_pegawai_ganti_hari` int(11) DEFAULT NULL,
  `tanggal_izin_pegawai_ganti_hari` date DEFAULT NULL,
  PRIMARY KEY (`id_izin_pegawai_ganti_hari`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_izin_pegawai_ganti_hari` */

/*Table structure for table `tbl_jabatan` */

DROP TABLE IF EXISTS `tbl_jabatan`;

CREATE TABLE `tbl_jabatan` (
  `id_jabatan` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(200) DEFAULT NULL,
  `golongan_jabatan` varchar(20) DEFAULT NULL,
  `upah_minimum_jabatan` bigint(20) DEFAULT NULL,
  `upah_maksimum_jabatan` bigint(20) DEFAULT NULL,
  `id_source_jabatan` varchar(50) DEFAULT NULL,
  `id_account_added_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_added_jabatan` datetime DEFAULT NULL,
  `id_account_modified_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_modified_jabatan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jabatan` */

insert  into `tbl_jabatan`(`id_jabatan`,`nama_jabatan`,`golongan_jabatan`,`upah_minimum_jabatan`,`upah_maksimum_jabatan`,`id_source_jabatan`,`id_account_added_jabatan`,`timestamp_added_jabatan`,`id_account_modified_jabatan`,`timestamp_modified_jabatan`) values 
(1,'Direktur','10',8000000,20000000,'3110201400000',NULL,NULL,NULL,NULL),
(2,'Seinor Manager','09',4000000,10000000,'3110201400000',NULL,NULL,NULL,NULL),
(3,'Manager','08',2000000,5000000,'3110201400000',NULL,NULL,NULL,NULL),
(4,'Kepala Bagian','07',1000000,3000000,NULL,NULL,NULL,NULL,NULL),
(5,'Supervisor','06',500000,1500000,'3110201400000',NULL,NULL,NULL,NULL),
(6,'Junior Programmer ','07',1000000,1500000,'3110201400000','1520819042544','2020-02-14 01:14:34','1520819042544','2020-02-14 01:15:42');

/*Table structure for table `tbl_jadwal_kerja_pegawai` */

DROP TABLE IF EXISTS `tbl_jadwal_kerja_pegawai`;

CREATE TABLE `tbl_jadwal_kerja_pegawai` (
  `id_jadwal_kerja_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai_jadwal_kerja_pegawai` int(11) DEFAULT NULL,
  `hari_jadwal_kerja_pegawai` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_kerja_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jadwal_kerja_pegawai` */

/*Table structure for table `tbl_kalender_libur_nasional` */

DROP TABLE IF EXISTS `tbl_kalender_libur_nasional`;

CREATE TABLE `tbl_kalender_libur_nasional` (
  `id_kalender_libur_nasional` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_mulai_kalender_libur_nasional` datetime DEFAULT NULL,
  `tanggal_selesai_kalender_libur_nasional` datetime DEFAULT NULL,
  `tahun_kalender_libur_nasional` int(11) DEFAULT NULL,
  `nama_kalender_libur_nasional` varchar(50) DEFAULT NULL,
  `keterangan_kalender_libur_nasional` varchar(200) DEFAULT NULL,
  `id_source_kalender_libur_nasional` varchar(50) DEFAULT NULL,
  `id_account_added_kalender_libur_nasional` varchar(50) DEFAULT NULL,
  `timestamp_added_kalender_libur_nasional` datetime DEFAULT NULL,
  `id_account_modified_kalender_libur_nasional` varchar(50) DEFAULT NULL,
  `timestamp_modified_kalender_libur_nasional` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_kalender_libur_nasional`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kalender_libur_nasional` */

insert  into `tbl_kalender_libur_nasional`(`id_kalender_libur_nasional`,`tanggal_mulai_kalender_libur_nasional`,`tanggal_selesai_kalender_libur_nasional`,`tahun_kalender_libur_nasional`,`nama_kalender_libur_nasional`,`keterangan_kalender_libur_nasional`,`id_source_kalender_libur_nasional`,`id_account_added_kalender_libur_nasional`,`timestamp_added_kalender_libur_nasional`,`id_account_modified_kalender_libur_nasional`,`timestamp_modified_kalender_libur_nasional`) values 
(1,'2020-01-01 00:00:00','2020-01-01 00:00:00',NULL,'Tahun Baru 2020','Tahun baru masehi, bukan tahun baru islam','3110201400000','1520819042544','2020-02-28 19:27:13','1520819042544','2020-02-28 22:36:32'),
(3,'2020-03-01 00:00:00','2020-03-03 00:00:00',NULL,'Acara PTL','Hotel Salak Bogor','3110201400000','1520819042544','2020-02-28 19:10:49',NULL,NULL);

/*Table structure for table `tbl_kontrak` */

DROP TABLE IF EXISTS `tbl_kontrak`;

CREATE TABLE `tbl_kontrak` (
  `id_kontrak` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `periode_kontrak` varchar(50) DEFAULT NULL,
  `deskripsi_kontrak` varchar(500) DEFAULT NULL,
  `tanggal_mulai_kontrak` date DEFAULT NULL,
  `tanggal_akhir_kontrak` date DEFAULT NULL,
  `id_vendor_kontrak` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_kontrak`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kontrak` */

/*Table structure for table `tbl_pegawai` */

DROP TABLE IF EXISTS `tbl_pegawai`;

CREATE TABLE `tbl_pegawai` (
  `id_pegawai` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomor_pegawai` varchar(50) DEFAULT NULL,
  `nama_pegawai` varchar(200) DEFAULT NULL,
  `jenis_kelamin_pegawai` varchar(20) DEFAULT NULL,
  `alamat_rumah_pegawai` varchar(200) DEFAULT NULL,
  `id_propinsi_pegawai` bigint(20) DEFAULT NULL,
  `tanggal_penerimaan_pegawai` date DEFAULT NULL,
  `id_jabatan_pegawai` bigint(20) DEFAULT NULL,
  `tanggal_aktif_jabatan_pegawai` date DEFAULT NULL,
  `status_mutasi_jabatan_pegawai` varchar(20) DEFAULT NULL,
  `upah_pokok_pegawai` bigint(20) DEFAULT NULL,
  `id_account_pegawai` varchar(50) DEFAULT NULL,
  `id_source_pegawai` varchar(50) DEFAULT NULL,
  `id_account_added_pegawai` varchar(50) DEFAULT NULL,
  `timestamp_added_pegawai` datetime DEFAULT NULL,
  `id_account_modified_pegawai` varchar(50) DEFAULT NULL,
  `timestamp_modified_pegawai` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pegawai` */

insert  into `tbl_pegawai`(`id_pegawai`,`nomor_pegawai`,`nama_pegawai`,`jenis_kelamin_pegawai`,`alamat_rumah_pegawai`,`id_propinsi_pegawai`,`tanggal_penerimaan_pegawai`,`id_jabatan_pegawai`,`tanggal_aktif_jabatan_pegawai`,`status_mutasi_jabatan_pegawai`,`upah_pokok_pegawai`,`id_account_pegawai`,`id_source_pegawai`,`id_account_added_pegawai`,`timestamp_added_pegawai`,`id_account_modified_pegawai`,`timestamp_modified_pegawai`) values 
(1,'H001','Udin Bahrudin','Laki-laki','Jl. Lebak bulus no.5',13,'2019-04-20',1,'2019-04-20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(2,'H002','Asep Saefudin','Laki-laki','Jl. Pinang Perak No. 4',20,'2018-07-01',2,'2018-07-01',NULL,2000000,NULL,'3110201400000',NULL,NULL,NULL,NULL),
(3,'H003','Samsul Bahri','Laki-laki','Jl. Riau I No. 10',14,'2019-04-10',1,'2019-04-10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(4,'H004','Zaenab','Perempuan','Jl. Dara III No. 8',21,'2020-01-01',3,'2020-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(7,'H010','Rizal Bahri','Laki-laki','Jl. Salah alamat No.4',17,NULL,3,'2020-02-01',NULL,1500000,NULL,'3110201400000','1520819042544','2020-01-15 10:21:20','1579048263839648','2020-01-15 20:46:17'),
(28,'H005','Mahmud Soleh','Laki-laki','Jl. Sukasari No.15',18,NULL,4,'2019-05-01','APPROVED',NULL,NULL,'3110201400000','1520819042544','2020-01-29 12:09:48',NULL,NULL),
(53,'Zainab','Mahroh','Perempuan','Jl.Depok',32,'2020-02-13',3,'2020-02-13','APPROVED',1000000,NULL,'3110201400000','1520819042544','2020-02-13 19:08:00','1520819042544','2020-02-13 21:15:49'),
(57,'H100','KAYUM MUNAJIR','Laki-laki','Bogor',32,'2020-01-01',2,'2020-01-06','APPROVED',1500000,NULL,'3110201400000','1520819042544','2020-02-13 21:24:53','1520819042544','2020-02-18 23:17:38'),
(58,'H200','Ali','Laki-laki','Citerep',32,'2020-02-18',6,'2020-02-18','APPROVED',5000000,NULL,'3110201400000','1520819042544','2020-02-18 21:45:55','1520819042544','2020-02-18 23:38:17'),
(59,'H300','ULTRA','Laki-laki','Depok',32,'2020-02-18',5,'2020-02-18','APPROVED',4000000,NULL,'3110201400000','1520819042544','2020-02-18 22:05:21',NULL,NULL);

/*Table structure for table `tbl_pegawai_kuota_cuti` */

DROP TABLE IF EXISTS `tbl_pegawai_kuota_cuti`;

CREATE TABLE `tbl_pegawai_kuota_cuti` (
  `id_pegawai_kuota_cuti` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pegawai_pegawai_kuota_cuti` int(11) DEFAULT NULL,
  `nama_cuti_pegawai_kuota_cuti` varchar(50) DEFAULT NULL,
  `jumlah_pegawai_kuota_cuti` int(11) DEFAULT NULL,
  `id_account_pegawai_kuota_cuti` varchar(50) DEFAULT NULL,
  `id_source_pegawai_kuota_cuti` varchar(50) DEFAULT NULL,
  `id_account_added_pegawai_kuota_cuti` varchar(50) DEFAULT NULL,
  `timestamp_added_pegawai_kuota_cuti` datetime DEFAULT NULL,
  `id_account_modified_pegawai_kuota_cuti` varchar(50) DEFAULT NULL,
  `timestamp_modified_pegawai_kuota_cuti` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pegawai_kuota_cuti`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pegawai_kuota_cuti` */

insert  into `tbl_pegawai_kuota_cuti`(`id_pegawai_kuota_cuti`,`id_pegawai_pegawai_kuota_cuti`,`nama_cuti_pegawai_kuota_cuti`,`jumlah_pegawai_kuota_cuti`,`id_account_pegawai_kuota_cuti`,`id_source_pegawai_kuota_cuti`,`id_account_added_pegawai_kuota_cuti`,`timestamp_added_pegawai_kuota_cuti`,`id_account_modified_pegawai_kuota_cuti`,`timestamp_modified_pegawai_kuota_cuti`) values 
(1,57,'10 hari terakhir ramadhan',10,NULL,'3110201400000','1520819042544','2020-03-09 23:20:15',NULL,NULL),
(2,57,'Keluarga Terdekat Meninggal',2,NULL,'3110201400000','1520819042544','2020-03-09 23:20:15',NULL,NULL),
(3,57,'Cuti Tahunan',12,NULL,'3110201400000','1520819042544','2020-03-09 23:20:15',NULL,NULL),
(4,58,'10 hari terakhir ramadhan',10,NULL,'3110201400000','1520819042544','2020-03-09 23:45:15',NULL,NULL),
(5,58,'Keluarga Terdekat Meninggal',2,NULL,'3110201400000','1520819042544','2020-03-09 23:45:15',NULL,NULL),
(6,58,'Cuti Tahunan',12,NULL,'3110201400000','1520819042544','2020-03-09 23:45:15',NULL,NULL),
(7,58,'Pilihan 1',3,NULL,'3110201400000','1520819042544','2020-03-09 23:45:15',NULL,NULL),
(8,58,'Pilihan 2',4,NULL,'3110201400000','1520819042544','2020-03-09 23:45:15',NULL,NULL),
(10,59,'10 hari terakhir ramadhan',5,NULL,'3110201400000','1520819042544','2020-03-12 23:52:25',NULL,NULL);

/*Table structure for table `tbl_penggajian` */

DROP TABLE IF EXISTS `tbl_penggajian`;

CREATE TABLE `tbl_penggajian` (
  `id_penggajian` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pegawai_penggajian` bigint(20) DEFAULT NULL,
  `id_akun_bank_penggajian` bigint(20) DEFAULT NULL,
  `tanggal_penggajian` date DEFAULT NULL,
  `id_source_penggajian` varchar(50) DEFAULT NULL,
  `id_account_added_penggajian` varchar(50) DEFAULT NULL,
  `timestamp_added_penggajian` datetime DEFAULT NULL,
  `id_account_modified_penggajian` varchar(50) DEFAULT NULL,
  `timestamp_modified_penggajian` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penggajian`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_penggajian` */

insert  into `tbl_penggajian`(`id_penggajian`,`id_pegawai_penggajian`,`id_akun_bank_penggajian`,`tanggal_penggajian`,`id_source_penggajian`,`id_account_added_penggajian`,`timestamp_added_penggajian`,`id_account_modified_penggajian`,`timestamp_modified_penggajian`) values 
(6,58,66,'2020-03-01','3110201400000','1520819042544','2020-02-19 00:00:57','1520819042544','2020-02-24 11:25:29'),
(7,57,64,'2020-02-01','3110201400000','1520819042544','2020-02-19 00:23:29','1520819042544','2020-02-19 01:01:49'),
(8,57,65,'2019-12-01','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(14,59,63,'2020-03-12','3110201400000','1520819042544','2020-03-12 23:36:50',NULL,NULL),
(15,59,63,'2020-03-19','3110201400000','1520819042544','2020-03-19 15:10:29',NULL,NULL);

/*Table structure for table `tbl_penggajian_detil` */

DROP TABLE IF EXISTS `tbl_penggajian_detil`;

CREATE TABLE `tbl_penggajian_detil` (
  `id_penggajian_detil` bigint(11) NOT NULL AUTO_INCREMENT,
  `id_penggajian_penggajian_detil` bigint(11) DEFAULT NULL,
  `jenis_penggajian_detil` varchar(50) DEFAULT NULL,
  `komponen_penggajian_detil` varchar(100) DEFAULT NULL,
  `jumlah_penggajian_detil` bigint(11) DEFAULT NULL,
  `keterangan_penggajian_detil` varchar(200) DEFAULT NULL,
  `id_source_penggajian_detil` varchar(50) DEFAULT NULL,
  `id_account_added_penggajian_detil` varchar(50) DEFAULT NULL,
  `timestamp_added_penggajian_detil` datetime DEFAULT NULL,
  `id_account_modified_penggajian_detil` varchar(50) DEFAULT NULL,
  `timestamp_modified_penggajian_detil` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penggajian_detil`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_penggajian_detil` */

insert  into `tbl_penggajian_detil`(`id_penggajian_detil`,`id_penggajian_penggajian_detil`,`jenis_penggajian_detil`,`komponen_penggajian_detil`,`jumlah_penggajian_detil`,`keterangan_penggajian_detil`,`id_source_penggajian_detil`,`id_account_added_penggajian_detil`,`timestamp_added_penggajian_detil`,`id_account_modified_penggajian_detil`,`timestamp_modified_penggajian_detil`) values 
(55,7,'GAJI_UPAH','Upah Pokok',1500000,'','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(56,7,'GAJI_UPAH','Insentif Tahunan',12000000,'Periode-2020','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(57,7,'GAJI_POTONGAN','Hutang Pribadi(5.000.000)',500000,'cicilan ke-4','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(58,7,'GAJI_POTONGAN','Tes',100,'lorem','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(59,7,'BEBAN_PERUSAHAAN','BPJS Kesehatan',192139,'','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(60,7,'BEBAN_PERUSAHAAN','Jaminan Hari Tua(JHT)',125400,'','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(61,7,'BEBAN_PERUSAHAAN','Jaminan Kematian(JKM)',6600,'','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(62,7,'BEBAN_PERUSAHAAN','Jaminan Kecelakaan Kerja(JKK)',5280,'','3110201400000','1520819042544','2020-02-19 01:01:49',NULL,NULL),
(63,8,'GAJI_UPAH','Upah Pokok',1500000,'','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(64,8,'GAJI_UPAH','Bonus',5000000,'Goal Project','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(65,8,'GAJI_POTONGAN','Hutang Pribadi(5.000.000)',500000,'cicilan ke-2','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(66,8,'GAJI_POTONGAN','dll',20000,'pinjem beli kopi','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(67,8,'BEBAN_PERUSAHAAN','BPJS Kesehatan',192139,'','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(68,8,'BEBAN_PERUSAHAAN','Jaminan Hari Tua(JHT)',125400,'','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(69,8,'BEBAN_PERUSAHAAN','Jaminan Kematian(JKM)',6600,'','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(70,8,'BEBAN_PERUSAHAAN','Jaminan Kecelakaan Kerja(JKK)',5280,'','3110201400000','1520819042544','2020-02-19 01:23:40',NULL,NULL),
(71,6,'GAJI_UPAH','Upah Pokok',5000000,'','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(72,6,'GAJI_UPAH','Bonus',250000,'uang tip','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(73,6,'GAJI_POTONGAN','lorem ipsum dolor lorem ipsum dolor ',500000,'','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(74,6,'BEBAN_PERUSAHAAN','BPJS Kesehatan',250000,'','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(75,6,'BEBAN_PERUSAHAAN','Jaminan Hari Tua(JHT)',150000,'','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(76,6,'BEBAN_PERUSAHAAN','Jaminan Kematian(JKM)',10000,'','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(77,6,'BEBAN_PERUSAHAAN','Jaminan Kecelakaan Kerja(JKK)',20000,'','3110201400000','1520819042544','2020-02-24 11:25:29',NULL,NULL),
(99,14,'GAJI_UPAH','Upah Pokok',4000000,'','3110201400000','1520819042544','2020-03-12 23:36:50',NULL,NULL),
(100,15,'GAJI_UPAH','Upah Pokok',4000000,'','3110201400000','1520819042544','2020-03-19 15:10:29',NULL,NULL),
(101,15,'GAJI_UPAH','Uang Jalan',500000,'','3110201400000','1520819042544','2020-03-19 15:10:29',NULL,NULL);

/*Table structure for table `tbl_propinsi` */

DROP TABLE IF EXISTS `tbl_propinsi`;

CREATE TABLE `tbl_propinsi` (
  `id_propinsi` bigint(20) NOT NULL,
  `nama_propinsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `latitude_propinsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude_propinsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_source_propinsi` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_propinsi`),
  UNIQUE KEY `provinsi_id` (`id_propinsi`) USING BTREE,
  KEY `provinsi_nama` (`nama_propinsi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_propinsi` */

insert  into `tbl_propinsi`(`id_propinsi`,`nama_propinsi`,`latitude_propinsi`,`longitude_propinsi`,`id_source_propinsi`) values 
(11,'Aceh','4.695135','96.7493993','*'),
(12,'Sumatera Utara','2.0108563','98.9784887','*'),
(13,'Sumatera Barat','-0.7399397','100.8000051','*'),
(14,'Riau','0.2933469','101.7068294','*'),
(15,'Jambi','-1.6101229','103.6131203','*'),
(16,'Sumatera Selatan','-3.3194374','103.914399','*'),
(17,'Bengkulu','-3.7928451','102.2607641','*'),
(18,'Lampung','-4.5585849','105.4068079','*'),
(19,'Kepulauan Bangka Belitung','-2.7410513','106.4405872','*'),
(21,'Kepulauan Riau','3.9456514','108.1428669','*'),
(31,'Dki Jakarta','-6.1744651','106.822745','*'),
(32,'Jawa Barat','-7.090911','107.668887','*'),
(33,'Jawa Tengah','-7.150975','110.1402594','*'),
(34,'Di Yogyakarta','-7.7975915','110.3707141','*'),
(35,'Jawa Timur','-7.5360639','112.2384017','*'),
(36,'Banten','-6.4058172','106.0640179','*'),
(51,'Bali','-8.4095178','115.188916','*'),
(52,'Nusa Tenggara Barat','-8.6529334','117.3616476','*'),
(53,'Nusa Tenggara Timur','-8.6573819','121.0793705','*'),
(61,'Kalimantan Barat','-0.2787808','111.4752851','*'),
(62,'Kalimantan Tengah','-1.6814878','113.3823545','*'),
(63,'Kalimantan Selatan','-3.0926415','115.2837585','*'),
(64,'Kalimantan Timur','0.5386586','116.419389','*'),
(65,'Kalimantan Utara','3.0730929','116.0413889','*'),
(71,'Sulawesi Utara','0.6246932','123.9750018','*'),
(72,'Sulawesi Tengah','-1.4300254','121.4456179','*'),
(73,'Sulawesi Selatan','-3.6687994','119.9740534','*'),
(74,'Sulawesi Tenggara','-3.5562244','121.8020017','*'),
(75,'Gorontalo','0.5435442','123.0567693','*'),
(76,'Sulawesi Barat','-2.8441371','119.2320784','*'),
(81,'Maluku','-3.2384616','130.1452734','*'),
(82,'Maluku Utara','1.5709993','127.8087693','*'),
(91,'Papua Barat','-1.3361154','133.1747162','*'),
(94,'Papua','-4.269928','138.0803529','*');

/*Table structure for table `tbl_region` */

DROP TABLE IF EXISTS `tbl_region`;

CREATE TABLE `tbl_region` (
  `id_region` varchar(50) NOT NULL DEFAULT '',
  `name_region` varchar(50) DEFAULT NULL,
  `type_region` varchar(50) DEFAULT NULL,
  `description_region` text,
  `location_region` varchar(50) DEFAULT NULL,
  `location_desc_region` varchar(50) DEFAULT NULL,
  `organization_id_region` varchar(50) DEFAULT NULL,
  `refund_region` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_region` */

insert  into `tbl_region`(`id_region`,`name_region`,`type_region`,`description_region`,`location_region`,`location_desc_region`,`organization_id_region`,`refund_region`) values 
('1553051748240951','Wisata Gunung Bromo','LAUT',' Gunung Bromo adalah Wisata alam terbaik di Indonesia yang menjadi surga bagi pecinta wisata pegunungan dan para fotografer.  Selain itu juga dapat menikmati keindahan alam Bromo dan sajian sunrise-nya dari atas puncak, wisatawan juga bisa mengunjungi spot lain yang tidak kalah menarik, seperti Bukit Teletubbies dan Lautan Pasir Berbisik. \r\n#phan','2512512134','Jawa Timur','3110201400000',1),
('1553066489065932','Raja Ampat','LAUT','Raja Ampat Lautnya yg nampak eksotis nan indah.','5255252','Ambon','3110201400000',1),
('1553075693221742','Taman Nasional Komodo','DARAT','Kawasan yg dihiasi dengan satwa-satwa langkah.','353534346','Jawa','3110201400000',0),
('1553076020578140','Taman Nasional Bali Barat','DARAT','Pemandangan Alam, gunung, laut dan air terjun yg eksotis','','Bali','3110201400000',NULL),
('1553165877431282','The Famous Sunrise','DARAT','Mount Bromo is part of BromoTenggerSemeru National Park. Famous for exotic caldera or sea of sand creaters, as well as beautiful views of the sunrise (Bromo Sunrise Tour)','17478768732','Jawa Timur','3110201400000',NULL);

/*Table structure for table `tbl_region_facility` */

DROP TABLE IF EXISTS `tbl_region_facility`;

CREATE TABLE `tbl_region_facility` (
  `id_region_facility` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_facility` varchar(50) DEFAULT NULL,
  `item_region_facility` varchar(50) DEFAULT NULL,
  `price_region_facility` decimal(15,2) DEFAULT '0.00',
  `include_region_facility` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_region_facility`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_region_facility` */

insert  into `tbl_region_facility`(`id_region_facility`,`header_id_region_facility`,`item_region_facility`,`price_region_facility`,`include_region_facility`) values 
('1553061259686297','1553051748240951','Transportation',500000.00,'Y'),
('1553061274710640','1553051748240951','Acomdation 2 night hotel with breakfast',1500000.00,'Y'),
('1553061287991753','1553051748240951','Tour guide service',200000.00,'Y'),
('1553061312420108','1553051748240951','2 unit jeep (1-5) 2 point location',2000000.00,'Y'),
('1553061323040434','1553051748240951','parking & toll',50000.00,'Y'),
('1553061360210115','1553051748240951','ride horse in Bromo',50000.00,'N'),
('1553076525550133','1553076020578140','Transportation',NULL,'Y'),
('1553077372829630','1553076020578140','Acomodation 2 night hotel with breakfast',NULL,'Y'),
('1553077494918171','1553076020578140','tour guide service',NULL,'Y'),
('1553077613380524','1553076020578140','parking & toll',NULL,'Y'),
('1553077703321205','1553076020578140','2 unit jeep',NULL,'Y'),
('1553077794070115','1553076020578140','enterance free',100000.00,'N'),
('1553166232599768','1553165877431282','Transportation',NULL,'Y'),
('1553166251750969','1553165877431282','Acomodation 2 night hotel with breakfast',NULL,'Y'),
('1553166262444238','1553165877431282','tour guide service',NULL,'Y'),
('1553166285534556','1553165877431282','Entracee fee',NULL,'Y'),
('1553166313795179','1553165877431282','2 Unit Jeep(1-5) 2 point location',NULL,'Y'),
('1553166327744862','1553165877431282','daily mineral water 600ml',NULL,'Y'),
('1553166340861358','1553165877431282','parking & toll',NULL,'Y'),
('1553166352956216','1553165877431282','tip crew',NULL,'N'),
('1553166376552456','1553165877431282','airline tickets PP',NULL,'N'),
('1553166394586442','1553165877431282','airport tax, airport handling',NULL,'N'),
('1553166410060141','1553165877431282','ride horses in Bromo',NULL,'N'),
('1556070307974934','1553066489065932','Tour Guide',200000.00,'Y'),
('1556628404572607','1553075693221742','Tour Guide',200000.00,'Y'),
('1556628440909450','1553075693221742','Transportation',450000.00,'Y');

/*Table structure for table `tbl_region_photo` */

DROP TABLE IF EXISTS `tbl_region_photo`;

CREATE TABLE `tbl_region_photo` (
  `id_region_photo` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_photo` varchar(50) DEFAULT NULL,
  `file_id_region_photo` varchar(50) DEFAULT NULL,
  `active_region_photo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_region_photo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_region_photo` */

insert  into `tbl_region_photo`(`id_region_photo`,`header_id_region_photo`,`file_id_region_photo`,`active_region_photo`) values 
('1556068117536208','1553051748240951','1556068117477970',0),
('1556068257768964','1553051748240951','1556068257643559',0),
('1556068980789839','1553066489065932','1556068980512376',0),
('1556069009930917','1553066489065932','1556069009774368',0),
('1556069717357950','1553066489065932','1556069717285535',0),
('1556100307407161','1553075693221742','1556100306992468',0),
('1556100508652591','1553076020578140','1556100508537443',1),
('1556100522048435','1553165877431282','1556100521989192',1),
('1556531031363721','1553066489065932','1556531030963216',1),
('1556549582027284','1553066489065932','1556549581680246',0),
('1556556694067159','1553075693221742','1556556693663841',1),
('1556653148720288','1553051748240951','1556653147693142',1);

/*Table structure for table `tbl_region_todo` */

DROP TABLE IF EXISTS `tbl_region_todo`;

CREATE TABLE `tbl_region_todo` (
  `id_region_todo` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_todo` varchar(50) DEFAULT NULL,
  `day_region_todo` int(11) DEFAULT NULL,
  `time_region_todo` datetime DEFAULT NULL,
  `activity_region_todo` varchar(100) DEFAULT NULL,
  `parent_region_todo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_region_todo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_region_todo` */

insert  into `tbl_region_todo`(`id_region_todo`,`header_id_region_todo`,`day_region_todo`,`time_region_todo`,`activity_region_todo`,`parent_region_todo`) values 
('1','1553051748240951',1,'1970-01-01 10:09:00','Break again','1553052091348251'),
('1553052091348251','1553051748240951',1,'1970-01-01 09:09:00','Jelajahi Kota Malang',''),
('1553057187055646','1553051748240951',2,'1970-01-01 00:00:00','GO',''),
('1553057353610553','1553051748240951',2,'1970-01-01 07:00:00','Breakfast in resto local','1553057187055646'),
('1553057369918290','1553051748240951',2,'1970-01-01 08:00:00','GO to Somewhere','1553057187055646'),
('1553058420053147','1553051748240951',1,'1970-01-01 11:00:00','GO GO','1553052091348251'),
('1553058440047581','1553051748240951',2,'1970-01-01 09:00:00','Skiping','1553057187055646'),
('1553058449999983','1553051748240951',3,'1970-01-01 00:00:00','REHAT',''),
('1553062980819945','1553051748240951',3,'1970-01-01 07:00:00','Dhuha','1553058449999983'),
('1553096100376481','1553076020578140',1,'1970-01-01 00:00:00','Exploring A',''),
('1553155551274310','1553076020578140',2,'1970-01-01 08:00:00','Exploring B',''),
('1553155606739594','1553076020578140',3,'1970-01-01 00:00:00','END Exploring ',''),
('1553161676081790','1553076020578140',5,'1970-01-01 09:07:00','LIMA LIMA','1553155909656325'),
('1553162796260660','1553076020578140',1,'1970-01-01 06:00:00','Sarapan DI Resto Lokal','1553096100376481'),
('1553164778779787','1553076020578140',1,'1970-01-01 07:50:00','Preapere to GO!!','1553096100376481'),
('1553164995946210','1553076020578140',2,'1970-01-01 07:00:00','Breakfast In the Morning','1553155551274310'),
('1553165247656237','1553076020578140',2,'1970-01-01 08:00:00','Sholat Dhuha','1553155551274310'),
('1553165262755604','1553076020578140',3,'1970-01-01 07:00:00','Breakfast','1553155606739594'),
('1553166465026575','1553165877431282',1,'1970-01-01 06:00:00','GO',''),
('1553166481506140','1553165877431282',2,'1970-01-01 06:00:00','Explore Bromo',''),
('1553166509728574','1553165877431282',3,'1970-01-01 17:00:00','End Program',''),
('1553166525182269','1553165877431282',1,'1970-01-01 08:00:00','Arrival Surabaya','1553166465026575'),
('1553166549443553','1553165877431282',1,'1970-01-01 09:00:00','Breakfast at local resto','1553166465026575'),
('1553166574992165','1553165877431282',1,'1970-01-01 11:00:00','Transfer to bato visit jatim park 2','1553166465026575'),
('1553166593254689','1553165877431282',1,'1970-01-01 13:00:00','Lunch & Ishoma','1553166465026575'),
('1553166604889777','1553165877431282',1,'1970-01-01 14:00:00','Patikapel','1553166465026575'),
('1553166619500136','1553165877431282',1,'1970-01-01 16:00:00','Check in Hotel','1553166465026575'),
('1553166660736641','1553165877431282',2,'1970-01-01 03:00:00','Midhnight Bromo via cemoro lawang','1553166481506140'),
('1553168739658719','1553165877431282',2,'1970-01-01 07:00:00','Breakfast at camera indah hotel bromo','1553166481506140'),
('1553168806827826','1553165877431282',2,'1970-01-01 09:00:00','Back to malang','1553166481506140'),
('1553168823501638','1553165877431282',2,'1970-01-01 12:00:00','Lunch & Ishoma','1553166481506140'),
('1553168838786640','1553165877431282',2,'1970-01-01 13:00:00','Museum Angkut','1553166481506140'),
('1553168858722264','1553165877431282',2,'1970-01-01 16:00:00','Alun-alun Batu','1553166481506140'),
('1553169243729423','1553165877431282',2,'1970-01-01 18:00:00','BNS','1553166481506140'),
('1553169260265631','1553165877431282',2,'1970-01-01 19:00:00','Dinner at local resto','1553166481506140'),
('1553169272677651','1553165877431282',2,'1970-01-01 20:00:00','Check in hotel','1553166481506140'),
('1553169301026966','1553165877431282',3,'1970-01-01 07:00:00','Breakfast & Checkout in hotel','1553166509728574'),
('1553169390421718','1553165877431282',3,'1970-01-01 08:00:00','Shopping tour till time depature and than transfer to juanda airport Surabaya','1553166509728574'),
('1553423550922882','1553066489065932',1,'1970-01-01 08:00:00','Breakfast','1553166465026575'),
('2','1553051748240951',1,'1970-01-01 12:09:00','Ishoma','1553052091348251');

/*Table structure for table `tbl_region_todo_copy` */

DROP TABLE IF EXISTS `tbl_region_todo_copy`;

CREATE TABLE `tbl_region_todo_copy` (
  `id_region_todo` varchar(50) NOT NULL DEFAULT '',
  `header_id_region_todo` varchar(50) DEFAULT NULL,
  `day_region_todo` int(11) DEFAULT NULL,
  `title_region_todo` varchar(50) DEFAULT NULL,
  `desc_region_todo` varchar(100) DEFAULT NULL,
  `time_region_todo` datetime DEFAULT NULL,
  `activity_region_todo` varchar(50) DEFAULT NULL,
  `parent_region_todo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_region_todo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_region_todo_copy` */

/*Table structure for table `tbl_riwayat_alamat` */

DROP TABLE IF EXISTS `tbl_riwayat_alamat`;

CREATE TABLE `tbl_riwayat_alamat` (
  `id_riwayat_alamat` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_pegawai_riwayat_alamat` bigint(20) DEFAULT NULL,
  `alamat_rumah_riwayat_alamat` varchar(500) DEFAULT NULL,
  `id_propinsi_riwayat_alamat` bigint(20) DEFAULT NULL,
  `id_organization_riwayat_alamat` varchar(50) DEFAULT NULL,
  `id_account_added_riwayat_alamat` varchar(50) DEFAULT NULL,
  `timestamp_added_riwayat_alamat` datetime DEFAULT NULL,
  `id_account_modified_riwayat_alamat` varchar(50) DEFAULT NULL,
  `timestamp_modified_riwayat_alamat` datetime DEFAULT NULL,
  PRIMARY KEY (`id_riwayat_alamat`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_riwayat_alamat` */

insert  into `tbl_riwayat_alamat`(`id_riwayat_alamat`,`id_pegawai_riwayat_alamat`,`alamat_rumah_riwayat_alamat`,`id_propinsi_riwayat_alamat`,`id_organization_riwayat_alamat`,`id_account_added_riwayat_alamat`,`timestamp_added_riwayat_alamat`,`id_account_modified_riwayat_alamat`,`timestamp_modified_riwayat_alamat`) values 
(9,10,'Jl. Airlangga 3 No. 39',18,'3110201400000','1579048263839648','2020-01-15 18:03:32',NULL,NULL),
(10,7,'Jl. Airlangga 3 No. 39',18,'3110201400000','1579048263839648','2020-01-15 20:46:17',NULL,NULL),
(11,11,'Jl. Airlangga 3 No. 39',18,'3110201400000','1579048263839648','2020-01-15 20:46:28',NULL,NULL);

/*Table structure for table `tbl_riwayat_jabatan` */

DROP TABLE IF EXISTS `tbl_riwayat_jabatan`;

CREATE TABLE `tbl_riwayat_jabatan` (
  `id_riwayat_jabatan` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_pegawai_riwayat_jabatan` bigint(20) DEFAULT NULL,
  `id_jabatan_riwayat_jabatan` bigint(20) DEFAULT NULL,
  `tanggal_awal_riwayat_jabatan` date DEFAULT NULL,
  `tanggal_akhir_riwayat_jabatan` date DEFAULT NULL,
  `id_organization_riwayat_jabatan` varchar(50) DEFAULT NULL,
  `id_account_added_riwayat_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_added_riwayat_jabatan` datetime DEFAULT NULL,
  `id_account_modified_riwayat_jabatan` varchar(50) DEFAULT NULL,
  `timestamp_modified_riwayat_jabatan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_riwayat_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_riwayat_jabatan` */

insert  into `tbl_riwayat_jabatan`(`id_riwayat_jabatan`,`id_pegawai_riwayat_jabatan`,`id_jabatan_riwayat_jabatan`,`tanggal_awal_riwayat_jabatan`,`tanggal_akhir_riwayat_jabatan`,`id_organization_riwayat_jabatan`,`id_account_added_riwayat_jabatan`,`timestamp_added_riwayat_jabatan`,`id_account_modified_riwayat_jabatan`,`timestamp_modified_riwayat_jabatan`) values 
(9,10,4,'2019-03-01','2020-01-31','3110201400000','1579048263839648','2020-01-15 18:03:32',NULL,NULL),
(10,7,4,'2019-03-01','2020-01-31','3110201400000','1579048263839648','2020-01-15 20:46:17',NULL,NULL),
(11,11,4,'2019-03-01','2020-01-31','3110201400000','1579048263839648','2020-01-15 20:46:28',NULL,NULL);

/*Table structure for table `tbl_service_request` */

DROP TABLE IF EXISTS `tbl_service_request`;

CREATE TABLE `tbl_service_request` (
  `id_service_request` varchar(50) NOT NULL DEFAULT '',
  `module_service_request` varchar(20) NOT NULL DEFAULT '',
  `created_time_service_request` datetime NOT NULL,
  `status_service_request` varchar(20) DEFAULT '',
  `callback_time_service_request` datetime DEFAULT NULL,
  `reference_id_service_request` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_service_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_service_request` */

/*Table structure for table `tbl_tour_guide` */

DROP TABLE IF EXISTS `tbl_tour_guide`;

CREATE TABLE `tbl_tour_guide` (
  `id_tour_guide` varchar(50) NOT NULL DEFAULT '',
  `full_name_tour_guide` varchar(100) NOT NULL DEFAULT '',
  `gender_tour_guide` varchar(50) NOT NULL DEFAULT '',
  `photo_tour_guide` varchar(100) DEFAULT NULL,
  `description_tour_guide` text,
  `phone_number_tour_guide` varchar(50) DEFAULT NULL,
  `email_tour_guide` varchar(50) DEFAULT NULL,
  `cost_per_day_tour_guide` decimal(15,2) DEFAULT '0.00',
  `organization_id_tour_guide` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tour_guide`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tour_guide` */

insert  into `tbl_tour_guide`(`id_tour_guide`,`full_name_tour_guide`,`gender_tour_guide`,`photo_tour_guide`,`description_tour_guide`,`phone_number_tour_guide`,`email_tour_guide`,`cost_per_day_tour_guide`,`organization_id_tour_guide`) values 
('1551853116915302','Kit Harington','LAKI_LAKI',NULL,'What u want see,its my responsible to show u','082119260548','kit_harington@email.com',120000.00,'3110201400000'),
('1554028973074481','Yulie Rahmawati','PEREMPUAN','1556532372054235','Enjoyed with my tour','0852159588','yulie_rahmawati@email.com',250000.00,'3110201400000'),
('1554198612659471','Isaac Hempstead','LAKI_LAKI',NULL,'I will show you, what u want to see :D','082110806729','isaac@email.com',250000.00,'3110201400000'),
('1554732637306729','Peter Dinklage','LAKI_LAKI','1556558303706743','Simple For u','0849345453535','peter@emal.com',200000.00,'3110201400000'),
('1556532196181355','maher','LAKI_LAKI','1556532196181531','','847121241241','maher@email.com',300000.00,'3110201400000');

/*Table structure for table `tbl_type_region` */

DROP TABLE IF EXISTS `tbl_type_region`;

CREATE TABLE `tbl_type_region` (
  `id_type_region` varchar(50) NOT NULL DEFAULT '',
  `name_type_region` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_type_region`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_type_region` */

insert  into `tbl_type_region`(`id_type_region`,`name_type_region`) values 
('1556559558850986','LAUT'),
('1556559565383673','GUNUNG');

/*Table structure for table `tbl_type_vehicle` */

DROP TABLE IF EXISTS `tbl_type_vehicle`;

CREATE TABLE `tbl_type_vehicle` (
  `id_type_vehicle` varchar(50) NOT NULL DEFAULT '',
  `name_type_vehicle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_type_vehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_type_vehicle` */

insert  into `tbl_type_vehicle`(`id_type_vehicle`,`name_type_vehicle`) values 
('1','MINI MPV'),
('2','SUV'),
('3','Minivan'),
('4','Minibus'),
('5','City Car');

/*Table structure for table `tbl_vehicle` */

DROP TABLE IF EXISTS `tbl_vehicle`;

CREATE TABLE `tbl_vehicle` (
  `id_vehicle` varchar(50) NOT NULL DEFAULT '',
  `name_vehicle` varchar(50) NOT NULL DEFAULT '',
  `type_id_vehicle` varchar(50) NOT NULL DEFAULT '',
  `cost_vehicle` decimal(15,2) DEFAULT NULL,
  `photo_vehicle` varchar(100) DEFAULT NULL,
  `seat_capacity_vehicle` int(11) DEFAULT '0',
  `suitcase_vehicle` int(11) DEFAULT '0',
  `description_vehicle` text,
  `provided_vehicle` varchar(50) DEFAULT NULL,
  `organization_id_vehicle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_vehicle` */

insert  into `tbl_vehicle`(`id_vehicle`,`name_vehicle`,`type_id_vehicle`,`cost_vehicle`,`photo_vehicle`,`seat_capacity_vehicle`,`suitcase_vehicle`,`description_vehicle`,`provided_vehicle`,`organization_id_vehicle`) values 
('1551852673960765','Toyota Calya','5',367500.00,NULL,5,2,'','Qoyum','3110201400000'),
('1554142421514719','Suzuki Ertiga','1',367000.00,NULL,6,2,'','','3110201400000'),
('1554148313313636','Toyota Alphard','3',980000.00,NULL,6,2,'',NULL,'3110201400000'),
('1554185599696433','Daihatsu Xenia','1',367000.00,'1556558646157432',6,2,'','','3110201400000'),
('1555605643911329','Alphard Transformer','3',784000.00,'1556534441927466',2,6,'','Qoyum','3110201400000'),
('1556502810545868','Innova Super White','5',300000.00,'1556525639754372',4,6,'','C1000','3110201400000');

/*Table structure for table `tbl_vendor` */

DROP TABLE IF EXISTS `tbl_vendor`;

CREATE TABLE `tbl_vendor` (
  `id_vendor` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_vendor` varchar(100) DEFAULT NULL,
  `alamat_vendor` varchar(500) DEFAULT NULL,
  `jenis_vendor` varchar(50) DEFAULT NULL,
  `id_account_vendor` varchar(50) DEFAULT NULL,
  `id_source_vendor` varchar(50) DEFAULT NULL,
  `id_account_added_vendor` varchar(50) DEFAULT NULL,
  `timestamp_added_vendor` datetime DEFAULT NULL,
  `id_account_modified_vendor` varchar(50) DEFAULT NULL,
  `timestamp_modified_vendor` datetime DEFAULT NULL,
  PRIMARY KEY (`id_vendor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_vendor` */

insert  into `tbl_vendor`(`id_vendor`,`nama_vendor`,`alamat_vendor`,`jenis_vendor`,`id_account_vendor`,`id_source_vendor`,`id_account_added_vendor`,`timestamp_added_vendor`,`id_account_modified_vendor`,`timestamp_modified_vendor`) values 
(1,'Cloud Kilat','Jakarta Barat','SEWA',NULL,'3110201400000','1520819042544','2020-02-13 22:03:47','1520819042544','2020-02-14 01:00:30'),
(2,'Microsoft ','Jakarta Timur','BELI',NULL,'3110201400000','1520819042544','2020-02-13 22:04:40','1520819042544','2020-02-14 01:00:34'),
(3,'Adobe','Jakarta Pusat','BELI',NULL,'3110201400000','1520819042544','2020-02-13 22:04:53','1520819042544','2020-02-14 01:00:27');

/* Function  structure for function  `join_entity` */

/*!50003 DROP FUNCTION IF EXISTS `join_entity` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `join_entity`( _entity TEXT, _fields TEXT ) RETURNS text CHARSET latin1
BEGIN
DECLARE _next TEXT DEFAULT NULL;
DECLARE _nextlen INT DEFAULT NULL;
DECLARE _value TEXT DEFAULT NULL;
DECLARE _return TEXT DEFAULT '';
iterator:
LOOP
  IF LENGTH(TRIM(_fields)) = 0 OR _fields IS NULL THEN
    LEAVE iterator;
  END IF;
  SET _next = SUBSTRING_INDEX(_fields,',',1);
  SET _nextlen = LENGTH(_next);
  SET _value = TRIM(_next);
  SET _return = CONCAT(CONCAT(',',CONCAT(CONCAT(_entity,'.'),_value)),_return);
  SET _fields = INSERT(_fields,1,_nextlen + 1,'');
END LOOP;
   RETURN SUBSTRING(_return,2);
END */$$
DELIMITER ;

/*Table structure for table `oauth_account_login` */

DROP TABLE IF EXISTS `oauth_account_login`;

/*!50001 DROP VIEW IF EXISTS `oauth_account_login` */;
/*!50001 DROP TABLE IF EXISTS `oauth_account_login` */;

/*!50001 CREATE TABLE  `oauth_account_login`(
 `id_account` varchar(50) ,
 `username_account` varchar(30) ,
 `password_account` varchar(500) ,
 `email_account` varchar(100) ,
 `first_name_account` varchar(100) ,
 `last_name_account` varchar(100) ,
 `full_name_account` varchar(201) ,
 `mobile_account` varchar(30) ,
 `enabled_account` tinyint(1) ,
 `account_non_expired_account` tinyint(1) ,
 `credentials_non_expired_account` tinyint(1) ,
 `account_non_locked_account` tinyint(1) ,
 `admin_account` tinyint(1) ,
 `id_source` varchar(50) ,
 `name_source` varchar(200) 
)*/;

/*View structure for view oauth_account_login */

/*!50001 DROP TABLE IF EXISTS `oauth_account_login` */;
/*!50001 DROP VIEW IF EXISTS `oauth_account_login` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `oauth_account_login` AS select `a`.`id_account` AS `id_account`,`a`.`username_account` AS `username_account`,`a`.`password_account` AS `password_account`,`a`.`email_account` AS `email_account`,`a`.`first_name_account` AS `first_name_account`,`a`.`last_name_account` AS `last_name_account`,concat(concat(`a`.`first_name_account`,' '),`a`.`last_name_account`) AS `full_name_account`,`a`.`mobile_account` AS `mobile_account`,`a`.`enabled_account` AS `enabled_account`,`a`.`account_non_expired_account` AS `account_non_expired_account`,`a`.`credentials_non_expired_account` AS `credentials_non_expired_account`,`a`.`account_non_locked_account` AS `account_non_locked_account`,`b`.`is_admin_control` AS `admin_account`,`d`.`id_source` AS `id_source`,`d`.`name_source` AS `name_source` from ((`hijr_account` `a` join `hijr_control` `b` on((`a`.`id_account` = `b`.`account_id_control`))) join `hijr_source` `d` on((`d`.`id_source` = `b`.`source_id_control`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
